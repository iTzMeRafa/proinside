[![PROINSIDE](https://proinside.app/img/logo_temp.png)](https://proinside.app/)

# PROINSIDE APP

> Proinside ist ein System zur Verwaltung und Darstellung von Unternehmensprozessen. 

   
## Gliederung

> Die Aufteilung der `README`. Um zu einem bestimmten Topic zu springen einfach auf die Kategorie klicken.

- [Installation](#installation)
- [Lizenz](#lizenz)


## Installation

### Erforderlich
- Proinside wird mit PHP gebaut `>=PHP 7.0`

### Klonen

- Klone dieses Repository auf deine lokale Maschine `git clone https://iTzMeRafa@bitbucket.org/iTzMeRafa/proinside.git`

### Setup

- Es wird empfohlen einen virtuellen Host unter `proinside.test` einzurichten der zum Ordner `proinside/public/` zeigt.   
Andernfalls muss die `config.dev.php` angepasst werden.
- In der `.htaccess` im public Ordner muss je nach Einrichtung der RewriteBase auf `RewriteBase /` oder `RewriteBase /public` gesetzt werden.
- Die Ordner unter `proinside/public/uploads/` benötigen volle Read, Write, Execute Berechtigung.

> Um Dateien und Ordner erstellen und löschen zu können

```shell
$ chmod -R 777 ./public/uploads/files
$ chmod -R 777 ./public/uploads/business_logos
```


---

### Database Structure

Tables
- accepted_methods
- accepted_tasks
- archive_tree     
- archived_data           
- business_model          
- business_model_subtopics
- business_model_topics   
- checks_methods          
- checks_tasks            
- data                    
- flexible_questions      
- folder_tree             
- methods                 
- organs                  
- processes               
- qualifications          
- qualifications_tasks    
- qualifications_users    
- rbac_permissions        
- rbac_role_perms         
- rbac_roles              
- rbac_user_roles         
- read_data               
- reminders_methods       
- reminders_tasks         
- reports                 
- suborgans               
- tags                    
- tasks                   
- todo_lists              
- unaccepted_methods      
- unaccepted_tasks        
- unapproved_data         
- users                   
- ------------------------


### Code Syntax / Conventions

Proinside nutzt [PSR 1, 2](https://www.php-fig.org/psr/) als Codestandard.  
Use [Code Checker](https://cs.symfony.com/) to automate checking syntax.  
```shell
php-cs-fix fix /path/to/file/or/folder
```

## Lizenz

[![Lizenz](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2020 © PROINSIDE