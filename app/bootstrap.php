<?php
  //error_reporting(E_ALL);
  // Load Config
  require_once 'config/config.dev.php';

  // Load Helpers
  require_once 'helpers/system_helper.php';
  require_once 'helpers/convert_helper.php';
  require_once 'helpers/url_helper.php';
  require_once 'helpers/session_helper.php';
  require_once 'helpers/filetypes_helper.php';
  require_once 'helpers/role_helper.php';
  require_once 'helpers/date_time_helper.php';
  require_once 'helpers/password_helper.php';
  require_once 'helpers/mailer.php';

  // Load Role Based Access Control
  require_once 'role_based_access_control/Role.php';
  require_once 'role_based_access_control/PriviledgedUser.php';

  // Autoload Core Libraries
  spl_autoload_register(function ($className) {
      require_once 'libraries/' . $className . '.php';
  });
