<?php
  class Accounts extends Controller
  {
      public function __construct()
      {
          // Make sure user is logged in
          if (!isLoggedIn()) {
              redirect('users/login');
          }
          // Get the current user's permissions
          $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

          // Load the user model
          $this->userModel = $this->model('User');
      }

      /**********************
          METHOD: INDEX
       **********************/

      public function index()
      {
          // Get the current user
          $user = $this->userModel->getUserById($_SESSION['user_id']);

          $data = [
        'bodyClass' => 'indexAccounts',
        'privUser' => $this->privUser,
        'user' => $user
      ];

          $this->view('accounts/index', $data);
      }

      /**********************
          METHOD: EDIT
       **********************/

      public function edit()
      {
          // Check if there is a post request
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {
              // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              $data = [
          'bodyClass' => 'editAccounts',
          'privUser' => $this->privUser,
          'id' => $_SESSION['user_id'],
          'firstname' => $_POST['firstname'],
          'lastname' => $_POST['lastname'],
          'email' => $_POST['email'],
          'firstname_err' => '',
          'lastname_err' => '',
          'email_err' => ''
         ];

              // Validate firstname
              if (empty($data['firstname'])) {
                  $data['firstname_err'] = 'Bitte giben Sie einenen Vornamen an';
              }

              // Validate lastname
              if (empty($data['lastname'])) {
                  $data['lastname_err'] = 'Bitte giben Sie einenen Nachnamen an';
              }

              // Validate Email
              if (empty($data['email'])) {
                  $data['email_err'] = 'Bitte gültige E-Mail Adresse eingeben';
              // If input-email and session-email are not equal
              } elseif ($data['email'] != $_SESSION['user_email']) {
                  // Check if email already exists in the database
                  if ($this->userModel->findUserByEmail($data['email'])) {
                      $data['email_err'] = 'Diese E-Mail Adresse wird bereits verwendet';
                  }
              }

              // Make sure no errors
              if (empty($data['name_err']) && empty($data['email_err'])) {
                  // Validated
                  if ($this->userModel->updateUser($data)) {
                      // change email and name in the session variable so it matches the server regardless if changed or not
                      $_SESSION['user_email'] = $data['email'];
                      $_SESSION['user_name'] = $data['lastname'];
                      // Prepare the flash message
                      prepareFlash('user_message', 'Ihre Accountdetails wurden geändert.');
                      redirect('accounts');
                  } else {
                      die('Ein Fehler ist aufgetreten. Bitte melden Sie dies beim Support.');
                  }
              } else {
                  // Load the view with errors
                  $this->view('accounts/edit', $data);
              }
          } else {
              // Otherwise load the edit view, in order to be able to edit the account
              // Get existing account from model
              $user = $this->userModel->getUserById($_SESSION['user_id']);

              $data = [
          'bodyClass' => 'accounts accounts-edit',
          'privUser' => $this->privUser,
          'firstname' => $user->firstname,
          'lastname' => $user->lastname,
          'email' => $user->email
        ];

              $this->view('accounts/edit', $data);
          }
      }
  }
