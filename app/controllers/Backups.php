<?php

class Backups extends Controller
{
    private $privUser;
    private $helperController;
    private $backupModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->backupModel = $this->model('BackupModel');
    }

    /**
     * Loads the index page
     * @return {view}
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'backups',
            'privUser' => $this->privUser,
            'backups' => $this->backupModel->getBackups(),
        ];

        $this->view('backups/index', $data);
    }
}
