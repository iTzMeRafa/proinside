<?php

class BackupsCreate extends Controller
{
    private $privUser;
    private $helperController;
    private $backupModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->backupModel = $this->model('BackupModel');
    }

    /**
     * Loads the index page
     * @return {view}
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'backups',
            'privUser' => $this->privUser
        ];

        $this->view('backups/create', $data);
    }

    /**
     * Creates the system backup:
     *  - Copy file folder,
     *  - Make database backup
     * @return {view}
     */
    public function post()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            redirect('users/dashboard');
        }

        $backupName     = $_POST['backupName'];
        $backupFormat   = $_POST['backupFormat'];
        $backupFolder   = 'uploads/backups/'.getCurrentDate().'/';
        $zipFolder      = 'uploads/backups/'.getCurrentDate().'.zip';
        $backupDumpfile = $backupFolder . '/database.sql';


        switch ($backupFormat) {
            default:
            case 'fullBackup':
                $this->createFullbackup($backupName, $backupFolder, $backupDumpfile, $backupFormat, $zipFolder);
                break;

            case 'fileBackup':
                $this->createFileBackup($backupName, $backupFolder, $backupFormat, $zipFolder);
                break;

            case 'databaseBackup':
                $this->createDatabaseBackup($backupName, $backupFolder, $backupDumpfile, $backupFormat, $zipFolder);
                break;
        }

        prepareFlash('createBackupState', 'Das Backup wurde erfolgreich erstellt!');
        redirect('BackupsCreate');
    }

    /**
     * Creates a fullbackup via url: BackupsCreate/automatedBackup
     * @return void
     */
    public function automatedBackup() {
        $backupName     = 'Automated-Fullbackup';
        $backupFormat   = 'fullBackup';
        $backupFolder   = 'uploads/backups/'.getCurrentDate().'/';
        $zipFolder      = 'uploads/backups/'.getCurrentDate().'.zip';
        $backupDumpfile = $backupFolder . '/database.sql';

        $this->createFullbackup($backupName, $backupFolder, $backupDumpfile, $backupFormat, $zipFolder);
    }

    /**
     * Creates the backup folder
     * @param $backupFolder
     */
    private function createBackupFolder($backupFolder)
    {
        shell_exec("mkdir $backupFolder");
    }

    private function createDatabaseDump($backupDumpfile)
    {
        $dbUser         = DB_USER;
        $dbPassword     = DB_PASS;
        $dbHost         = DB_HOST_NO_PORT;
        $dbName         = DB_NAME;

        exec("mysqldump --user=$dbUser --password=$dbPassword --host=$dbHost $dbName > $backupDumpfile");
    }

    private function copyFilesToBackup($backupFolder)
    {
        shell_exec("cp -R uploads/files $backupFolder");
    }

    private function zipBackupFolder($zipFolder, $backupFolder)
    {
        shell_exec("zip -r $zipFolder $backupFolder");
    }

    /**
     * Creates a full backup of the system
     * @param $backupName
     * @param $backupFolder
     * @param $backupDumpfile
     * @param $backupFormat
     * @param $zipFolder
     */
    private function createFullbackup($backupName, $backupFolder, $backupDumpfile, $backupFormat, $zipFolder)
    {
        $this->createBackupFolder($backupFolder);
        $this->createDatabaseDump($backupDumpfile);
        $this->copyFilesToBackup($backupFolder);
        $this->zipBackupFolder($zipFolder, $backupFolder);

        $this->backupModel->createBackup($backupName, $backupFolder, $zipFolder, $backupFormat);
    }

    /**
     * Copies the file folder
     * @param $backupName
     * @param $backupFolder
     * @param $backupFormat
     * @param $zipFolder
     * @return void
     */
    private function createFileBackup($backupName, $backupFolder, $backupFormat, $zipFolder)
    {
        $this->createBackupFolder($backupFolder);
        $this->copyFilesToBackup($backupFolder);
        $this->zipBackupFolder($zipFolder, $backupFolder);

        $this->backupModel->createBackup($backupName, $backupFolder, $zipFolder, $backupFormat);
    }

    /**
     * Creates a backup of the database
     * @param $backupName
     * @param $backupFolder
     * @param $backupDumpfile
     * @param $backupFormat
     * @param $zipFolder
     * @return void
     */
    private function createDatabaseBackup($backupName, $backupFolder, $backupDumpfile, $backupFormat, $zipFolder)
    {
        $this->createBackupFolder($backupFolder);
        $this->createDatabaseDump($backupDumpfile);
        $this->zipBackupFolder($zipFolder, $backupFolder);

        $this->backupModel->createBackup($backupName, $backupFolder, $zipFolder, $backupFormat);
    }
}
