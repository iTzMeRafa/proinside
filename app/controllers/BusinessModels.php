<?php


class BusinessModels extends Controller
{
    protected $businessModel;
    protected $privUser;

    public function __construct()
    {
        /*
         * strict login check
         * redirect to login page
         */
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        /* Create Object of Business Model */
        $this->businessModel = $this->model('BusinessModel');

        /* Get current Users privilege */
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        if ($this->privUser->hasPrivilege('30') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt das Unternehmensmodell zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /***
     * Gets called on index page load, manages data and returns the view
     * @return view
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'businessmodels',
            'privUser' => $this->privUser,
            'businessModel' => $this->getBusinessModel(),
        ];

        // Load view
        $this->view('business_models/index', $data);
    }

    /***
     * Collects all business model data and merges them into one logic array
     * @return array
     */
    private function getBusinessModel()
    {

        /* Get active business model */
        $activeBusinessModel = $this->businessModel->getActiveBusinessModel();
        $activeBusinessModel->count = $this->businessModel->countBusinessModels()->count;

        /* Get all topics for the business model */
        $activeBusinessModelTopics = $this->businessModel->getBusinessModelTopicsByID($activeBusinessModel->id);

        /* Get all subtopics, filesExist for each topic */
        foreach ($activeBusinessModelTopics as $key => $topic) {
            $topic->subtopics = $this->businessModel->getBusinessModelTopicSubtopicsByID($topic->id);
            $topic->filesExist = $this->businessModel->checkIfFileIsAvailableByFolderID($topic->folder_id);
        }

        /* Get all filesExist for each subtopic */
        foreach ($activeBusinessModelTopics as $key => $topic) {
            foreach ($topic->subtopics as $subtopic) {
                $subtopic->filesExist = $this->businessModel->checkIfFileIsAvailableByFolderID($subtopic->folder_id);
            }
        }

        return [
            'businessModel' => $activeBusinessModel,
            'businessModelTopics' => $activeBusinessModelTopics
        ];
    }
}
