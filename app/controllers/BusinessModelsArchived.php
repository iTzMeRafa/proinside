<?php


class BusinessModelsArchived extends Controller
{
    protected $businessModel;
    protected $privUser;

    public function __construct()
    {
        /*
         * strict login check
         * redirect to login page
         */
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        /* Create Object of Business Model */
        $this->businessModel = $this->model('BusinessModel');

        /* Get current Users privilege */
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        if ($this->privUser->hasPrivilege('32') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die archivierten Unternehmensmodelle zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /***
     * Gets called on index page load, manages data and returns the view
     * @return view
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'businessmodels',
            'privUser' => $this->privUser,
            'businessModelArchived' => $this->businessModel->getAllArchivedBusinessModels(),
        ];

        // Load view
        $this->view('business_models/archived', $data);
    }

    /***
     * Gets called on show page load, shows the archived business model by given parameter
     * @param $id
     * @return view
     */
    public function show($id)
    {
        $data = [
            'bodyClass' => 'businessmodels',
            'privUser' => $this->privUser,
            'businessModelArchived' => $this->businessModel->getAllArchivedBusinessModels(),
            'businessModel' => $this->getBusinessModelArchivedByID($id),
        ];

        // Load view
        $this->view('business_models/archivedShow', $data);
    }

    /**
     * Sets the archived business model to the active business model by id
     * @param $id
     */
    public function setActive($id)
    {
        $this->businessModel->setActiveBusinessModelToArchived();
        $this->businessModel->setBusinessModelToActiveById($id);

        prepareFlash('business_model_edit_success', 'Das Unternehmensmodell wurde erfolgreich getauscht.');
        redirect('BusinessModels');
    }

    private function getBusinessModelArchivedByID($id)
    {

        /* Get the business model */
        $businessModel = $this->businessModel->getBusinessModelByID($id);
        $businessModel->count = $this->businessModel->countBusinessModels()->count;

        /* Get all topics for the business model */
        $businessModelTopics = $this->businessModel->getBusinessModelTopicsByID($businessModel->id);

        /* Get all subtopics for each topic */
        foreach ($businessModelTopics as $key => $topic) {
            $businessModelTopics[$key]->subtopics = $this->businessModel->getBusinessModelTopicSubtopicsByID($topic->id);
        }

        return [
            'businessModel' => $businessModel,
            'businessModelTopics' => $businessModelTopics
        ];
    }
}
