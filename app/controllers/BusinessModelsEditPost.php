<?php


class BusinessModelsEditPost extends Controller
{
    protected $businessModel;
    protected $privUser;
    protected $topics;
    protected $topicsCount;
    protected $topicsFolderID;
    protected $subtopics;
    protected $subtopicsFolderID;
    protected $businessLogo;
    protected $isValidBusinessLogoFileSize;
    protected $isValidBusinessLogoFileType;
    protected $businessLogoPath;
    protected $isNewLogo;

    public function __construct()
    {
        /* authentication check */
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        /* Create Object of Business Model */
        $this->businessModel = $this->model('BusinessModel');

        /* Get current Users privilege */
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
    }

    /***
     * Gets called on index page load, manages data and returns the view
     * @return void
     */
    public function index()
    {
        if (!isset($_POST['submitEdit'])) {
            return redirect('users/login');
        }

        $this->fetchTopicsAndCount();

        $this->fetchTopicsFolderID();

        $this->fetchSubtopics();

        $this->fetchSubtopicsFolderID();

        $this->fetchBusinessLogo();

        $this->validateBusinessLogo();

        $this->checkValidations();
    }

    /**
     * Fetches all topics, counts them and saves them in class variable
     * @return void
     */
    private function fetchTopicsAndCount()
    {
        $this->topics = $_POST['topicInput'];
        $this->topicsCount = count($this->topics);
    }

    /**
     * Fetches the folder id for each topic
     * @return void
     */
    private function fetchTopicsFolderID()
    {
        $this->topicsFolderID = $_POST['topicFolderID'];
    }

    /**
     * Fetches all subtopics and saves them in class variable
     * @return void
     */
    private function fetchSubtopics()
    {
        $subtopics = [];
        for ($i = 0; $i < $this->topicsCount; $i++) {
            $subtopics[$i] = $_POST['subtopics'.$i];
        }

        $this->subtopics = $subtopics;
    }

    /**
     * Fetches the folder id for each subtopic
     * @return void
     */
    private function fetchSubtopicsFolderID()
    {
        $subtopicsFolderID = [];
        for ($i = 0; $i < $this->topicsCount; $i++) {
            $subtopicsFolderID[$i] = $_POST['subtopicsFolderID'.$i];
        }

        $this->subtopicsFolderID = $subtopicsFolderID;
    }

    /**
     * Fetches the business logo and saves it
     * @return void
     */
    private function fetchBusinessLogo()
    {
        $this->businessLogo = $_FILES['businessLogo'];
    }

    /***
     * Validates: is new logo uploaded, filetype, filesize
     * Generates: business logo path
     * @return void
     */
    private function validateBusinessLogo()
    {
        /* Checks if new logo uploaded, otherwise we will continue to use the current one */
        if (!file_exists($this->businessLogo['tmp_name']) || !is_uploaded_file($this->businessLogo['tmp_name'])) {
            $this->isNewLogo = false;
            $this->isValidBusinessLogoFileSize = true;
            $this->isValidBusinessLogoFileType = true;
            $this->businessLogoPath = $this->businessModel->getCurrentBusinessLogoPath()->logoPath;
        } else {

            /* File Extension Validation */
            $allowedFileExt = ['image/jpeg', 'image/png', 'image/gif'];

            if (!in_array($this->businessLogo['type'], $allowedFileExt)) {
                $this->isValidBusinessLogoFileType = false;
            } else {
                $this->isValidBusinessLogoFileType = true;
            }

            /* File Size Validation */
            $allowedFileSize = 2;
            $sizeMB = $this->businessLogo['size'] / 1000000;

            if ($sizeMB > $allowedFileSize) {
                $this->isValidBusinessLogoFileSize = false;
            } else {
                $this->isValidBusinessLogoFileSize = true;
            }

            /* Generate Business Logo Path */
            $businessLogoName = $this->businessLogo['name'];
            $this->businessLogoPath = "uploads/business_logos/" . ($this->businessModel->countBusinessModels()->count + 1) . $businessLogoName;
            $this->isNewLogo = true;
        }
    }

    /**
     * Check if all validations passed or not
     * @return void
     */
    private function checkValidations()
    {
        if (
            !$this->isValidBusinessLogoFileSize ||
            !$this->isValidBusinessLogoFileType
        ) {
            prepareFlash('business_model_edit_failure', 'Es gab Probleme bei der bearbeitung des Unternehmensmodell. Überprüfen Sie Ihre Eingaben.', 'alert alert-danger');
            redirect('businessmodelsedit');
        } else {
            $this->createNewBusinessModel();
        }
    }

    /**
     * Creates a new business model with new data
     */
    private function createNewBusinessModel()
    {
        $this->moveLogoToFolder();
        $this->businessModel->setActiveBusinessModelToArchived();
        $this->businessModel->insertNewBusinessModel($this->businessLogoPath);

        $businessModelID = $this->businessModel->getIdOfActiveBusinessModel()->id;

        foreach ($this->topics as $keyTopic => $topic) {
            $this->businessModel->insertTopic($topic, $businessModelID, $this->topicsFolderID[$keyTopic]);
            $this->businessModel->updateFolderName($topic, $this->topicsFolderID[$keyTopic]);

            $topicID = $this->businessModel->getIdOfNewestTopic()->id;

            foreach ($this->subtopics[$keyTopic] as $keySubtopic => $subtopic) {
                // Update folder name for subtopics too
                if (is_null($subtopic) || empty($subtopic)) {
                    continue;
                }
                $subtopicFolderID = $this->subtopicsFolderID[$keyTopic][$keySubtopic];
                $subtopicFolderID = is_null($subtopicFolderID) || empty($subtopicFolderID) ? 0 : $subtopicFolderID;
                $this->businessModel->insertSubtopic($subtopic, $topicID, $subtopicFolderID);
                $this->businessModel->updateFolderName($subtopic, $subtopicFolderID);
            }
        }

        prepareFlash('business_model_edit_success', 'Ihr Unternehmensmodell wurde erfolgreich bearbeitet.');
        redirect('BusinessModels');
    }

    /**
     * Saves the uploaded logo to the folder
     * @return void
     */
    private function moveLogoToFolder()
    {
        move_uploaded_file($this->businessLogo['tmp_name'], APPROOT . '/../public/' . $this->businessLogoPath);
    }
}
