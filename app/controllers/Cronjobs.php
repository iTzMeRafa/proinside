<?php
class Cronjobs extends Controller
{
    private $privUser;
    private $cronjobModel;

    public function __construct()
    {
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->cronjobModel = $this->model('CronjobModel');
    }

    public function intervalReminder()
    {
        $reminder = $this->cronjobModel->getReminder();
        //Mailer::sendMail("info@btw-marketing.de", "cronjob@proinside.app", "Cronjob Test", "Diese Mail wurde automatisch mit Cronjobs gesendet. Diese erhalten Sie nun jede Minute ^-^");
    }
}
