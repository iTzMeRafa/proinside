<?php

class Folders extends Controller
{
    private $privUser;
    private $folderModel;
    private $helperController;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('tasks');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->folderModel = $this->model('Folder');
        $this->helperController = $this->newController('HelperController');
    }

    /**
     * Loads the index page
     * @return {view}
     */
    public function index()
    {
        // Validate access privilege
        if ($this->privUser->hasPrivilege('13') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt den Dateimanager zu betreten', 'alert alert-danger');
            redirect('users/dashboard/');
        }

        $data = [
            'bodyClass' => 'folders',
            'privUser' => $this->privUser,
            'organs' => $this->helperController->getFullUserListSortedInOrgans(),
        ];

        $this->view('folders/index', $data);
    }


    /**
     * Jumps to the folders by folder_id
     * @param $folder_id
     */
    public function jump($folder_id)
    {

        // Validate access
        if ($this->privUser->hasPrivilege('13') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt den Dateimanager zu betreten', 'alert alert-danger');
            redirect('users/dashboard/');
        }

        $data = [
            'bodyClass' => 'folders',
            'privUser' => $this->privUser,
            'folder_id' => $folder_id,
            'folder_name' => $this->folderModel->getFolderNameById($folder_id)->folder_name,
            'organs' => $this->helperController->getFullUserListSortedInOrgans(),
        ];

        $this->view('folders/index', $data);
    }
}
