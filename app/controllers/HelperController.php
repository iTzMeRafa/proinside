<?php
class HelperController extends Controller
{
    private $privUser;
    private $organModel;
    private $userModel;

    public function __construct()
    {
        // Load Models
        $this->privUser     = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->userModel    = $this->model('User');
        $this->organModel   = $this->model('Organ');
    }

    /**
     * Gets all users and their organ
     * @return mixed
     */
    public function getFullUserListSortedInOrgans()
    {
        $organs = $this->organModel->getOrgansWithoutUser();
        foreach ($organs as $index => $organ) {
            if ($organ->is_active == null) {
                unset($organs[$index]);
            }

            if ($organ->id == 2) {
                $suborgans = $this->organModel->getSubOrgansByParentId(2);
                if (!empty($suborgans)) {
                    foreach ($suborgans as $suborgan) {
                        $id_array =  $this->organModel->explodeIdString($suborgan->staff);
                        $users = $this->organModel->populateUserIdArray($id_array, $this->userModel);
                        $suborgan->users = $users;
                    }
                }
                $organ->suborgans = $suborgans;
            } else {
                $managers = explode(',', $organ->management_level);
                $staff = explode(',', $organ->staff);
                $users = array_merge($managers, $staff);
                $users = array_unique($users);
                $users = $this->organModel->populateUserIdArray($users, $this->userModel);
                $organ->users = $users;
            }
        }

        return $organs;
    }
}
