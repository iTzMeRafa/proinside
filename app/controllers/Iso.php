<?php

class Iso extends Controller
{
    private $privUser;
    private $helperController;
    private $isoModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->isoModel = $this->model('IsoModel');

        /*
        if ($this->privUser->hasPrivilege('37') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Kompetenz-Sektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }
        */
    }

    /**
     * Loads the index page
     * @return {view}
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'isos',
            'privUser' => $this->privUser,
            'organs' => $this->helperController->getFullUserListSortedInOrgans(),
            'isos' => $this->isoModel->getAllIsos(),
        ];

        $this->view('isos/index', $data);
    }

    public function show($id)
    {
        $data = [
            'bodyClass' => 'isos',
            'privUser' => $this->privUser,
            'iso' => $this->isoModel->getIsoById($id),
        ];

        $this->view('isos/show', $data);
    }
}
