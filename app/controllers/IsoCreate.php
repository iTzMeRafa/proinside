<?php

class IsoCreate extends Controller
{
    private $privUser;
    private $helperController;
    private $isoModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->isoModel = $this->model('IsoModel');

        /*
        if ($this->privUser->hasPrivilege('39') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Kompetenzen zu erstellen', 'alert alert-danger');
            redirect('users/dashboard');
        }
        */
    }

    /**
     * Loads the index page
     * @return {view}
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'isos',
            'privUser' => $this->privUser,
        ];

        $this->view('isos/create', $data);
    }

    /**
     * Creates a new iso and fetches/handles the data
     * @return {redirect}
     */
    public function post()
    {
        if (!isset($_POST['isoCreate'])) {
            return redirect('users/login');
        }


        $isoName          = $_POST['isoNorm'] ?? '';

        if ($this->validatePost($isoName)) {
            $this->isoModel->storeIso($isoName)
                ? prepareFlash('createIsoState', "Die ISO-Norm <strong>{$isoName}</strong> wurde erfolgreich erstellt!")
                : prepareFlash('createIsoState', 'Ein Fehler ist aufgetreten. Versuchen Sie es erneut.', 'alert alert-danger');

            redirect('IsoCreate');
        }
    }

    /**
     * Validates the data to create a new iso
     * @param $isoName
     * @return bool
     */
    public function validatePost($isoName)
    {
        return is_string($isoName);
    }
}
