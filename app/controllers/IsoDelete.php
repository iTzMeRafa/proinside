<?php

class IsoDelete extends Controller
{
    private $privUser;
    private $helperController;
    private $isoModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->isoModel = $this->model('IsoModel');

        /*
        if ($this->privUser->hasPrivilege('41') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Kompetenzen zu löschen', 'alert alert-danger');
            redirect('users/dashboard');
        }
        */
    }

    /**
     * Deletes a iso by given id and redirects to the index page
     * @return {redirect}
     */
    public function index($isoId)
    {
        $this->isoModel->removeIso($isoId)
            ? prepareFlash('deleteIsoState', "Die ISO-Norm wurde erfolgreich gelöscht!")
            : prepareFlash('deleteIsoState', 'Ein Fehler ist aufgetreten. Versuchen Sie es erneut.', 'alert alert-danger');

        redirect('Iso');
    }
}
