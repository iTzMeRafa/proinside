<?php

class IsoRename extends Controller
{
    private $privUser;
    private $helperController;
    private $isoModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->isoModel = $this->model('IsoModel');

        /*
        if ($this->privUser->hasPrivilege('42') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Kompetenzen umzubenennen', 'alert alert-danger');
            redirect('users/dashboard');
        }
        */
    }

    /**
     * Deletes a iso by given id and redirects to the index page
     * @return {redirect}
     */
    public function index()
    {
        if (!isset($_POST['renameIsoSubmit'])) {
            return redirect('users/login');
        }

        $isoId = $_POST['isoId'] ?? '';
        $isoName = $_POST['renamedIso'] ?? '';

        $this->isoModel->renameIso($isoId, $isoName)
            ? prepareFlash('renameIsoState', "Die ISO-Norm wurde erfolgreich umbenannt!")
            : prepareFlash('renameIsoState', 'Ein Fehler ist aufgetreten. Versuchen Sie es erneut.', 'alert alert-danger');

        redirect('Iso');
    }
}
