<?php

class Listings extends Controller
{
    private $privUser;
    private $taskModel;
    private $dataModel;
    private $userModel;

    public function __construct()
    {
        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Get the current user's permissions
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        // Load the user model
        $this->taskModel = $this->model('Task');
        // Load the data model
        $this->dataModel = $this->model('Data');
        // Load the user model
        $this->userModel = $this->model('User');
    }

    public function index($folderId = 2)
    {

        // Get the main directories
        $folders = $this->dataModel->getFolderByParentId(0);

        foreach ($folders as $key => $folder) {
            // Check if the current user has no folder access permission
            if (!$this->privUser->hasAccess($folder->id)) {
                // NO PERMISSION
                // remove the folder from the array
                unset($folders[$key]);
            } else {
                // PERMISSION
                if ($this->dataModel->getFolderByParentId($folder->id)) {
                    $folder->sub_directories = $this->dataModel->getFolderByParentId($folder->id);
                }
            }
        }
        // Reset the array keys
        $folders = array_values($folders);

        //die(var_dump($folders));

        if (!$this->privUser->hasAccess($folderId)) {
            $folderId = $folders[0]->id;
        }

        // Depending on the folderId -> get the sub directories
        $files = $this->dataModel->getDataByFolderId($folderId);


        // Init array for output
        $modifiedFiles = [];

        if (!empty($files)) {
            // FILES FOUND

            foreach ($files as $file) {
                $path = $file->file_path;
                $path = substr($file->file_path, 2);

                $modifiedFiles[] = array(
                    'name' => $file->file_name,
                    'path' => substr($file->file_path, 2),
                    'extension' => $file->extension,
                    'id' => $file->id,
                    'approved_by' => $file->approved_by,
                    'isAccepted' => $this->dataModel->getReadData($_SESSION['user_id'], $file->id)->isAccepted,
                );
            }
        } else {
            // NO FILES IN THIS FOLDER
            $modifiedFiles = '';
        }


        $data = [
            'bodyClass' => 'listings',
            'privUser' => $this->privUser,
            'mainDirectories' => $folders,
            'subDirectories' => $modifiedFiles,
            'currentFolder' => $folderId
        ];

        // Load view
        $this->view('listings/index', $data);
    }

    /**
     * Accepts a data by user who has to read it
     * @param $dataId
     */
    public function accept($folderId, $dataId, $approvedBy)
    {
        $approvedUser = $this->userModel->getUserById($approvedBy);
        $readUser = $this->userModel->getUserById($_SESSION['user_id']);
        $file = $this->dataModel->getFileById($dataId);

        if ($this->dataModel->updateReadData($_SESSION['user_id'], $dataId)) {
            Mailer::sendAcceptedReadFileMail($approvedUser->email, $approvedUser->firstname, $approvedUser->lastname, $readUser->firstname, $readUser->lastname, $file->file_name);
            prepareFlash('fileAccepted', 'Die Datei wurde von Ihnen zur Kenntnis genommen');
            redirect('Listings/index/'.$folderId);
        }
    }
}
