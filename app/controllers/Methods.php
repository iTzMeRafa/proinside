<?php
  class Methods extends Controller
  {
      public function __construct()
      {
          // Make sure user is logged in
          if (!isLoggedIn()) {
              redirect('users/login');
          }

          // Get the current user's permissions
          $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

          // Load the method model
          $this->methodModel = $this->model('Method');
          // Load the user model
          $this->userModel = $this->model('User');
          // Load the data model
          $this->dataModel = $this->model('Data');
          // Load the organ model
          $this->organModel = $this->model('Organ');
          // Load the question model
          $this->questionModel = $this->model('Question');
          // Load the reminder model
          $this->reminderModel = $this->model('Reminder');
          // Load the process model
          $this->processModel = $this->model('Process');
      }

      /* #####################################
          METHOD: INDEX
       ####################################### */
      public function index()
      {
      
      /////////////////////////////////////
          /////// Validate Privileges /////////
          /////////////////////////////////////

          if ($this->privUser->hasPrivilege('33') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Maßnahmesektion zu betreten', 'alert alert-danger');
              redirect('users/dashboard');
          }

          //////////////////////////////////////
          //////////////////////////////////////

          // Get all active single methods
          $methods = $this->methodModel->getActiveSingleMethods();

          // Limit description length in index view
          /*
          foreach($methods as $method){

           if(!empty($method->description))
           $method->description =
          }
          */

          $data = [
         'bodyClass' => 'methods',
         'privUser' => $this->privUser,
         'methods' => $methods
       ];

          // Add Checkdate MSG to methods
          foreach ($data['methods'] as $key => $method) {
              $data['methods'][$key]->startDate_msg = calculateNextCheckdate($method->reference_date);
          }

          // SQL JOIN - source: models/method.php
          // methods: id = methodId
          // users: id = userId
          // methods: name = methodName
          // users: name = userName
          // methods: created_at = methodCreated
          // users: created_at = userCreated

          // Load view
          $this->view('methods/index', $data);
      }

      /**********************
          METHOD: ARCHIVED
      **********************/
      public function archived()
      {

      /////////////////////////////////////
          /////// Validate Privileges /////////
          /////////////////////////////////////

          if ($this->privUser->hasPrivilege('33') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Maßnahmensektion zu betreten', 'alert alert-danger');
              redirect('users/dashboard');
          }
      
          //////////////////////////////////////
          //////////////////////////////////////

          // Get all methods
          $methods = $this->methodModel->getArchivedSingleMethods();

          $data = [
        'bodyClass' => 'methods',
        'privUser' => $this->privUser,
        'methods' => $methods
      ];
          // SQL JOIN - source: models/method.php
          // methods: id = methodId
          // users: id = userId
          // methods: name = methodName
          // users: name = userName
          // methods: created_at = methodCreated
          // users: created_at = userCreated

          // Load view
          $this->view('methods/archived', $data);
      }

      /**********************
          METHOD: ALL
      **********************/
      public function all()
      {

      /////////////////////////////////////
          /////// Validate Privileges /////////
          /////////////////////////////////////

          if ($this->privUser->hasPrivilege('33') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Maßnahmensektion zu betreten', 'alert alert-danger');
              redirect('users/dashboard');
          }
      
          //////////////////////////////////////
          //////////////////////////////////////

          // Get all methods
          $methods = $this->methodModel->getAllSingleMethods();

          $data = [
        'bodyClass' => 'methods',
        'privUser' => $this->privUser,
        'methods' => $methods
      ];
          // SQL JOIN - source: models/method.php
          // methods: id = methodId
          // users: id = userId
          // methods: name = methodName
          // users: name = userName
          // methods: created_at = methodCreated
          // users: created_at = userCreated

          // Load view
          $this->view('methods/all', $data);
      }

      /**********************
          METHOD: ADD TASK
       **********************/

      public function add()
      {


        /////////////////////////////////////
          /////// Validate Privileges /////////
          /////////////////////////////////////

          if ($this->privUser->hasPrivilege('35') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Maßnahmen zu erstellen', 'alert alert-danger');
              redirect('users/dashboard');
          }
        
          //////////////////////////////////////
          //////////////////////////////////////

          /* ########################################### */
          /* ################ GET ORGANS ############### */

          $organs = $this->organModel->getOrgansWithoutUser();

          foreach ($organs as $index => $organ) {

          // Filter out inactive organs
              if ($organ->is_active == null) {
                  // Remove it
                  unset($organs[$index]);
              }
          
              // Check if the current organ is the staff_functions organ
              if ($organ->id == 2) {
                  // STAFF FUNCTIONS

                  // Get the suborgans (which are the actual staff functions)
                  $suborgans = $this->organModel->getSubOrgansByParentId(2);

                  if (!empty($suborgans)) {
                      foreach ($suborgans as $suborgan) {
                
                // Turn id string into array of ids
                          $id_array =  $this->organModel->explodeIdString($suborgan->staff);

                          // Populate the array
                          $users = $this->organModel->populateUserIdArray($id_array, $this->userModel);

                          // Attach the populated array to the suborgan
                          $suborgan->users = $users;
                      }
                  }
                  // Attach the populated array to the organ
                  $organ->suborgans = $suborgans;
              } else {
                  // REGULAR ORGANS
            
                  // Turn id strings into id arrays
                  $managers = explode(',', $organ->management_level);
                  $staff = explode(',', $organ->staff);

                  // Merge both arrays
                  $users = array_merge($managers, $staff);
                  // Remove duplicate ids
                  $users = array_unique($users);

                  // Populate the array
                  $users = $this->organModel->populateUserIdArray($users, $this->userModel);

                  // Attach the populated array to the organ
                  $organ->users = $users;
              }
          }

          /* ################################################ */

          $processes = $this->processModel->getAllProcesses();

          // Check if there is a post request
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {
              // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              $data = [
            'bodyClass' => 'methods addMethod',
            'privUser' => $this->privUser,
            'user_id' => $_SESSION['user_id'],
            'name' => trim($_POST['name']),
            'description' => trim($_POST['description']),
            'occurrence' => intval($_POST['occurrence']),
            'process_id' => trim($_POST['process_id']),
            //
            'organs' => $organs,
            'processes' => $processes,
            //
            'reason' => trim($_POST['reason']),
            'solution' => trim($_POST['solution']),
            'goal' => trim($_POST['goal']),
            'status' => trim($_POST['status']),
            'priority' => intval($_POST['priority']),
            //
            'required_knowledge_text' => trim($_POST['required_knowledge_text']),
            'required_knowledge' => trim($_POST['required_knowledge']),
            'required_knowledge_folders' => trim($_POST['required_knowledge_folders']),
            'required_knowledge_UI' => '',
            'required_knowledge_folders_UI' => '',
            // Roles
            'roles_responsible' => trim($_POST['roles_responsible']),
            'roles_concerned' => trim($_POST['roles_concerned']),
            'roles_contributing' => trim($_POST['roles_contributing']),
            'roles_information' => trim($_POST['roles_information']),
            'iso_relevant' => trim($_POST['iso_relevant']),
            // Recurring Check Intervals
            'check_intervals' => '',
            'interval_count' => '',
            'interval_format' => '',
            'activate_intervalsMethod' => '',
            'check_date' => '',
            'reference_day' => '',
            'reference_month' => '',
            'reference_year' => '',
            // Check Reminders
            'reminders_activated' => '',
            'remind_before' => '',
            'remind_at' => '',
            'remind_after' => '',
            //
            'required_resources_text' => trim($_POST['required_resources_text']),
            'required_resources' => trim($_POST['required_resources']),
            'required_resources_folders' => trim($_POST['required_resources_folders']),
            'required_resources_UI' => '',
            'required_resources_folders_UI' => '',
            'existing_compentencies' => trim($_POST['existing_compentencies']),
            'required_education' => trim($_POST['required_education']),
            // Tags
            'tags' => trim($_POST['tags']),
            'taglist' => '',
            'tags_array' => '',
            // Flexible Questions
            'flexible_questions' => '',
            //
            'is_approved' => 'true',
            ///////////////////////////
            //////////// Errors
            ///////////////////////////
            'name_err' => '',
            // description is optional
            'occurrence_err' => '',
            'process_id_err' => '',
            'reason_err' => '',
            'solution_err' => '',
            'goal_err' => '',
            'status_err' => '',
            'priority_err' => '',
            'required_knowledge_err' => '',
            'roles_err' => '',
            'iso_relevant_err' => '',
            'check_intervals_err' => '',
            'reminders_err' => '',
            'reference_date_err' => '',
            'required_resources_err' => '',
            'existing_compentencies_err' => '',
            'required_education_err' => '',
            // tags are optional
          ];

              // Validate data
              if (empty($data['name'])) {
                  $data['name_err'] = 'Bitte Namen eintragen';
              }
              if (empty($data['occurrence'])) {
                  $data['occurrence_err'] = 'Bitte Ereignis auswählen';
              }
              if (empty($data['process_id'])) {
                  $data['process_id_err'] = 'Bitte Prozess auswählen';
              }
              if (empty($data['reason'])) {
                  $data['reason_err'] = 'Bitte Ursache der Feststellung eintragen';
              }

              if (empty($data['solution'])) {
                  $data['solution_err'] = 'Bitte Aktion/Maßnahme eintragen';
              }

              if ($data['goal'] == 'empty') {
                  $data['goal_err'] = 'Bitte Kategorie auswählen';
              }
              if ($data['status'] == 'empty') {
                  $data['status_err'] = 'Bitte Status auswählen';
              }

              if ($data['priority'] == 'empty') {
                  $data['priority_err'] = 'Bitte Priorität auswählen';
              }

              if (empty($data['existing_compentencies'])) {
                  //$data['existing_compentencies_err'] = 'Bitte bestehende Kompetenzen angeben';
              }
              if (empty($data['required_education'])) {
                  //$data['required_education_err'] = 'Bitte erforderlichen Weiterbildungsbedarf angeben';
              }

              /* ################################################# */
              /* ################ ROLES VALIDATION ############### */

              $r_responsible = $this->processRole($data['roles_responsible']);
              $r_concerned = $this->processRole($data['roles_concerned']);
              $r_contributing = $this->processRole($data['roles_contributing']);
              $r_information = $this->processRole($data['roles_information']);

              /* ##################### REQUIRED KNOWLEDGE ###################### */

              $knowledgeData = $this->processRequiredKnowledge($data['required_knowledge']);
              $knowledgeFoldersData = $this->processRequiredKnowledgeData($data['required_knowledge_folders']);

              // Update the UI data
              $data['required_knowledge_UI'] = $knowledgeData;
              $data['required_knowledge_folders_UI'] = $knowledgeFoldersData;

              /* ##################### REQUIRED RESOURCES ##################### */

              $resourcesData = $this->processRequiredResources($data['required_resources']);
              $resourceFoldersData = $this->processRequiredResourcesData($data['required_resources_folders']);
        
              // Update the UI data
              $data['required_resources_UI'] = $resourcesData;
              $data['required_resources_folders_UI'] = $resourceFoldersData;

              /* ##################### CHECK INTERVALS ##################### */

              // Check if the activate_intervalsMethod Checkbox is set
              if (isset($_POST['activate_intervalsMethod'])) {
                  // ACTIVATED
                  // Get the reference date
                  $data['reference_day'] = trim($_POST['reference_day']);
                  $data['reference_month'] = trim($_POST['reference_month']);
                  $data['reference_year'] = trim($_POST['reference_year']);
                  // Get the interval data
                  $data['interval_count'] = trim($_POST['interval_count']);
                  $data['interval_format']= trim($_POST['interval_format']);

                  // Check if intervall fields are empty
                  if (empty($data['interval_count']) && $data['interval_format'] == 'empty') {
                      // Add the appropriate error message to the corresponding variable
                      $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl und ein Intervallformat aus.';
                  } elseif (empty($data['interval_count'])) {
                      // Add the appropriate error message to the corresponding variable
                      $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl aus';
                  } elseif ($data['interval_format'] == 'empty') {
                      // Add the appropriate error message to the corresponding variable
                      $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallformat aus.';
                  } elseif (empty($data['reference_day']) || empty($data['reference_month']) || empty($data['reference_year'])) {
                      $data['reference_date_err'] = 'Bitte füllen Sie alle Felder des Referenzdatums aus.';
                  } else {
                      // Construct the reference date
                      $data['reference_date'] = $data['reference_year'] .'-'. $data['reference_month'] .'-'. $data['reference_day'];
                      // Calculate the checkdate
                      // Get the reference date from the DB
                      $timestamp = strtotime($data['reference_date']);
                      // Calculate the next check date
                      // source: helpers/date_time_helper.php
                      $checkdate = calculateCheckdate($timestamp, $data['interval_count'], $data['interval_format']);
                      $data['check_date'] = $checkdate['year'] .'-'. $checkdate['month'] .'-'. $checkdate['day'];
                      // Construct the check interval text value
                      $data['check_intervals'] = $data['interval_count'] .' '. $data['interval_format'];
                  }
              } else {
                  // UNACTIVATED
                  $data['check_intervals'] = 'Kein Check Intervall';
                  $data['activate_intervalsMethod'] = false;
                  // Set the variables to NULL
                  $data['reference_date'] = null;
                  $data['interval_count'] = null;
                  $data['interval_format'] = null;
                  $data['check_date'] = null;
                  $data['check_intervals_err'] = '';
              }

          

              /* ############################# REMINDERS ############################ */
              // Check if the reminders and check intervals both have been activated
              if (isset($_POST['activate_reminders']) && isset($_POST['activate_intervalsMethod'])) {
                  // ACTIVE
                  // Fill the variables with the respective POST values
                  $data['reminders_activated'] = true;

                  // If the value is 0 set it to NULL
                  // Otherwise set it to the value
                  if (trim($_POST['remind_before']) == 0) {
                      $data['remind_before'] = null;
                  } else {
                      $data['remind_before'] = trim($_POST['remind_before']);
                  }

                  // If the value is 0 set it to NULL
                  // Otherwise set it to the value
                  if (trim($_POST['remind_at']) != 'activated') {
                      $data['remind_at'] = null;
                  } else {
                      $data['remind_at'] = trim($_POST['remind_at']);
                  }

                  // If the value is 0 set it to NULL
                  // Otherwise set it to the value
                  if (trim($_POST['remind_after']) == 0) {
                      $data['remind_after'] = null;
                  } else {
                      $data['remind_after'] = trim($_POST['remind_after']);
                  }

                  // Check if all fields are empty or unset
                  if (empty($data['remind_before']) && empty($data['remind_at']) && empty($data['remind_after'])) {
                      // ALL EMPTY
                      $data['reminders_err'] = 'Bitte wählen Sie mindestens eine der drei Erinnerungsmöglichkeiten aus oder deaktivieren Sie die Erinnerungsfunktion.';
                  }
              } elseif (isset($_POST['activate_reminders']) && !isset($_POST['activate_intervalsMethod'])) {
                  // ERROR: CHECK INTERVALS NOT ACTIVATED
                  $data['reminders_err'] = 'Die Erinnerungsfunktion kann nur aktiviert werden, sofern zunächst ein Check-Intervall definiert wurde.';
              } else {
                  // INACTIVE
                  $data['reminders_activated'] = false;
                  $data['remind_before'] = null;
                  $data['remind_at'] = null;
                  $data['remind_after'] = null;
              }

              /* ########### TAGS ########## */

              $data['taglist'] = $data['tags'];
              // Check if the tags array is filled
              if (!empty($data['tags'])) {
                  // Validated
                  // Create an array of tags
                  $data['tags_array'] = explode(',', $data['tags']);
              } else {
                  // Invalid
                  // Set the tags array to empty
                  $data['tags_array'] = [];
              }

              /* ######################################### */
              /* ########### FLEXIBLE QUESTIONS ########## */

              // Make sure there is a flexible question
              if (isset($_POST['flexible_questions'])) {
                  $data['flexible_questions'] = $_POST['flexible_questions'];

                  // Explode the flexible_questions string
                  $questionsArr = explode('||', $_POST['flexible_questions']);

                  // Init variable for later
                  $questionData = [];

                  foreach ($questionsArr as $questionObj) {
                      // Make sure the current item is not empty
                      if (!empty($questionObj)) {
                          // Explode it into an array
                          $parts = explode('/!/', $questionObj);
                          // Add each part to a correspondig variable
                          $question = $parts[0];
                          $yesNo = substr($parts[1], 3);
                          $notify = substr($parts[2], 2);
                          $remind = substr($parts[3], 2);

                          // Prepare $answers_UI for output
                          $answers_UI = '';

                          if ($yesNo == 'true') {
                              $answers_UI .= 'Ja/Nein, ';
                          }

                          if ($notify == 'true') {
                              $answers_UI .= 'Mitarbeiterbenachrichtigung, ';
                          }

                          if ($remind == 'true') {
                              $answers_UI .= 'Erinnerung, ';
                          }
                          // Remove the space and the comma
                          $answers_UI = substr($answers_UI, 0, -2);

                          $questionData[] = array(
                  'question' => $question,
                  'yesNo' => $yesNo,
                  'notify' => $notify,
                  'remind' => $remind,
                  'answers_UI' => $answers_UI,
                  'string_UI' => $questionObj
                );
                      }
                  }
                  // Store questionData for UI
                  $data['flexible_questions_UI'] = $questionData;
              }


              // Make sure no errors
              if (empty($data['name_err']) &&
              empty($data['occurrence_err']) &&
              empty($data['process_id_err']) &&
              empty($data['reason_err']) &&
              empty($data['solution_err']) &&
              empty($data['goal_err']) &&
              empty($data['status_err']) &&
              empty($data['priority_err']) &&
              empty($data['required_knowledge_err']) &&
              empty($data['iso_relevant_err']) &&
              empty($data['check_intervals_err']) &&
              empty($data['reminders_err']) &&
              empty($data['reference_date_err']) &&
              empty($data['required_resources_err']) &&
              empty($data['tags_err'])) {

                // Validated
                  // Add the method to the database and get its id
                  $methodId = $this->methodModel->addMethod($data);

                  // Check if the database request was successful
                  if ($methodId != false) {
                      // SUCCESS
                      // ASSIGN THE TASK TO THE USERS
                      // role: responsible
                      $this->assignMethodToUsers($r_responsible, 'responsible', $methodId);
                      // role: concerned
                      $this->assignMethodToUsers($r_concerned, 'concerned', $methodId);
                      // role: contributing
                      $this->assignMethodToUsers($r_contributing, 'contributing', $methodId);
                      // role: information
                      $this->assignMethodToUsers($r_information, 'information', $methodId);

                      // SUBMIT FLEXIBLE QUESTIONS TO DB
                      // Loop through all questions
                      foreach ($questionData as $questItem) {
                          // Prepare data
                          $data =  [
                      'question' => $questItem['question'],
                      'yesNo' => $questItem['yesNo'],
                      'notify' => $questItem['notify'],
                      'remind' => $questItem['remind'],
                      'task_id' => null,
                      'method_id' => $methodId,
                      'document_id' => null
                    ];
                          // Submit the question to the DB
                          $this->questionModel->addFlexibleQuestion($data);
                      }

                      // SUMBIT TAGS

                  
                  
                  

                      // Prepare the flash messages and redirect to methods
                      prepareFlash('method_message', 'Die Maßnahme wurde erstellt.');
                      redirect('methods/show/'.$methodId);
                  } else {
                      // Error
                      die('Ein Fehler ist aufgetreten. Bitte kontaktieren Sie den Entwickler');
                  }
              } else {

                /* ######## ROLES ######### */

                  // Update the fields for the UI
                  $data['roles_responsible_id'] = $data['roles_responsible'];
                  $data['roles_responsible'] = $r_responsible;
                  // Update the fields for the UI
                  $data['roles_concerned_id'] = $data['roles_concerned'];
                  $data['roles_concerned'] = $r_concerned;
                  // Update the fields for the UI
                  $data['roles_contributing_id'] = $data['roles_contributing'];
                  $data['roles_contributing'] = $r_contributing;
                  // Update the fields for the UI
                  $data['roles_information_id'] = $data['roles_information'];
                  $data['roles_information'] = $r_information;
                
                  //die(var_dump( $r_responsible )  );
                
                  // Load the view with Errors
                  $this->view('methods/add', $data);
              }
          } else {
              // Otherwise load the view, in order to be able to add a method

              // Init data
              $data = [
          'bodyClass' => 'methods addMethod',
          'privUser' => $this->privUser,
          'user_id' => '',
          'name' => '',
          'description' => '',
          'occurrence' => '',
          'process_id' => '',
          //
          'organs' => $organs,
          'processes' => $processes,
          //
          'reason' => '',
          'solution' => '',
          'startDate_msg' => '',
          'goal' => '',
          'status' => '',
          'priority' => '',
          'required_knowledge' => '',
          'required_knowledge_folders' => '',
          // Roles
          'roles_responsible' => '',
          'roles_concerned' => '',
          'roles_contributing' => '',
          'roles_information' => '',
          'roles_responsible_id' => '',
          'roles_concerned_id' => '',
          'roles_contributing_id' => '',
          'roles_information_id' => '',
          //
          'iso_relevant' => '',
          // Recurring Check Intervals
          'interval_count' => '',
          'interval_format' => '',
          'activate_intervalsMethod' => '',
          'check_date' => '',
          'reference_day' => '',
          'reference_month' => '',
          'reference_year' => '',
          // Check Reminders
          'reminders_activated' => '',
          'remind_before' => '',
          'remind_at' => '',
          'remind_after' => '',
          //
          'required_resources' => '',
          'required_resources_folders' => '',
          'existing_compentencies' => '',
          'required_education' => '',
          'required_education_folders' => '',
          'tags' => '',
          'taglist' => '',
          'flexible_questions' => '',
          // ##########################
          // ######## Errors ##########
          'name_err' => '',
          // description is optional
          'occurrence_err' => '',
          'process_id_err' => '',
          'reason_err' => '',
          'solution_err' => '',
          'goal_err' => '',
          'status_err' => '',
          'priority_err' => '',
          'required_knowledge_err' => '',
          'roles_err' => '',
          'iso_relevant_err' => '',
          'check_intervals_err' => '',
          'reminders_err' => '',
          'reference_date_err' => '',
          'required_resources_err' => '',
          'existing_compentencies_err' => '',
          'required_education_err' => '',
          'tags_err' => ''
        ];

              // Load view
              $this->view('methods/add', $data);
          }
      }

      /*****************************************
         PRIVATE METHOD: ASSIGN TASK TO USERS
      ******************************************/

      private function assignMethodToUsers($userArray, $role, $methodId)
      {
          // Check if $userArray is filled
          if (!empty($userArray)) {
              // Loop through it and process each user
              foreach ($userArray as $user) {

              // Prepare the data array for the model
                  $modelData = [
                  'method_id' => $methodId,
                  'user_id' => $user['user']->id ?? 0, // TODO: null excess overflow
                  'role' => $role,
                  'organ_id' => $user['organ']->id ?? 0  // TODO: null excess overflow
              ];

                  if (!empty($user['suborgan'])) {
                      $modelData['suborgan_id'] = $user['suborgan']->id;
                  } else {
                      $modelData['suborgan_id'] = null;
                  }

                  // Assign the method to user
                  $this->methodModel->addUnacceptedAssignment($modelData);
              }
          }
      }

      /*****************************************
         PRIVATE METHOD: PROCESS ROLE
      ******************************************/

      private function processRole($role)
      {
          // Check if user(s) are assigned to the role
          if (empty($role)) {
              // no user found
              // set variable to false
              return $roleArray = false;
          } else {
              // Get the roles string and turn each user (user_id) into an array item of $roleArray
              $roleArray = explode(',', $role);
              // Loop through the array and get each user as an object
              foreach ($roleArray as $index => $role_ids) {
                  if (substr_count($role_ids, '_') == 1) {
                      // Explode the string
                      $role_organ_ids = explode('_', $role_ids);

                      // Get the user
                      $user = $this->userModel->getUserById($role_organ_ids[0]);

                      // Get the organ
                      $organ = $this->organModel->getOrganById($role_organ_ids[1]);

              
                      // Add both objects to the $roleArray
                      $roleArray[$index] = array(
                'user' => $user,
                'organ' => $organ
              );
                  } elseif (substr_count($role_ids, '_') == 2) {
                      // Explode the string
                      $role_organ_suborgan_ids = explode('_', $role_ids);

                      // Get the user
                      $user = $this->userModel->getUserById($role_organ_suborgan_ids[0]);

                      // Get the organ
                      $organ = $this->organModel->getOrganById($role_organ_suborgan_ids[1]);

                      // Get the organ
                      $suborgan = $this->organModel->getSuborganById($role_organ_suborgan_ids[2]);

              
                      // Add both objects to the $roleArray
                      $roleArray[$index] = array(
                'user' => $user,
                'organ' => $organ,
                'suborgan' => $suborgan
              );
                  }
              }
              return $roleArray;
          }
      }
      
      /***********************************************************
         PRIVATE METHOD: PROCESS A PROCESS ROLE (ONLY FOR PROCESS TASKS)
      ********************************************* **************/

      private function processProcessRole($role)
      {
          // Check if user(s) are assigned to the role
          if (empty($role)) {
              // no user found
              // set variable to false
              return $roleArray = false;
          } else {
              // Get the roles string and turn each user (user_id) into an array item of $roleArray
              $roleArray = explode(',', $role);
              // Loop through the array and get each user as an object
              foreach ($roleArray as $key => $item) {
                  $userAndOrgan = explode('_', $item);
                  // Get the user from DB
                  $userObj = $this->userModel->getUserById($userAndOrgan[0]);
                  // Get the organ from DB
                  $organObj = $this->organModel->getOrganById($userAndOrgan[1]);

                  // Add the user and organ objects to the $roleArray
                  $roleArray[$key] = array(
              'user' => $userObj,
              'organ' => $organObj
            );
              }
              return $roleArray;
          }
      }

      /******************************
         PRIVATE VARIABLE: FILETYPES
      *******************************/

      private $fileTypes = [
        // Documents
        'csv'   => 'fa-file-csv',
        'doc'   => 'fa-file-word',
        'docx'  => 'fa-file-word',
        'odt'   => 'fa-file-alt',
        'ppt'   => 'fa-powerpoint',
        'pdf'   => 'fa-file-pdf',
        'xls'   => 'fa-file-alt',
        'xlsx'  => 'fa-file-excel',
        // Blank
        'blank' => 'fa-file'
      ];

      /***************************************************
         PRIVATE METHOD: PROCESS THE REQUIRED KNOWLEDGE
      ****************************************************/

      private function processRequiredKnowledge($data)
      {
          $fileTypes = $this->fileTypes;
          $dataModel = $this->dataModel;

          // Get the required resources (files) and process them
          if (!empty($data)) {
              $knowledgeItems = explode('|', $data);

              // Init variabls
              $knowledgeData = [];

              // Loop through the $resources array
              foreach ($knowledgeItems as $resource) {

            // If the $resource is empty skip it
                  if (empty($resource)) {
                      continue;
                  }

                  // Get the file from DB
                  $fileObj = $dataModel->getFileById($resource);

                  // Get the name of the file
                  $name = $fileObj->file_name;

                  // Get the path
                  $path = $fileObj->file_path;
                  // Remove '..' from the path
                  $path = substr($path, 2);

                  // Get the file extension
                  $fileExt =  substr(strrchr($name, '.'), 1);

                  // Determine the respective font-awesome css icon-class
                  if (isset($fileTypes[$fileExt])) {
                      $iconClass = $fileTypes[$fileExt];
                  } else {
                      $iconClass = $fileTypes['blank'];
                  }

                  array_push($knowledgeData, array(
              'id' => $resource,
              'name' => $name,
              'path' => $path,
              'extension' => $fileExt,
              'icon_class' => $iconClass
            ));
              }
          
              // Return the data
              return $knowledgeData;
          } else {
              // Set it to empty
              $knowledgeData = [];
              // Return the data
              return $knowledgeData;
          }
      }

      /********************************************************
         PRIVATE METHOD: PROCESS THE REQUIRED KNOWLEDGE DATA
      **********************************************************/

      private function processRequiredKnowledgeData($data)
      {

        // Get the required model for further action
          $dataModel = $this->dataModel;

          // Make sure the input data is not empty
          if (!empty($data)) {
              $knowledgeFolders = explode('|', $data);

              // Init variable
              $knowledgeFoldersData = [];

              // Loop through the $resourceFolders array
              foreach ($knowledgeFolders as $key => $folder) {
                  // If the $folder is empty skip it
                  if (empty($folder)) {
                      continue;
                  }

                  // Get the folder from DB
                  $folderObj = $dataModel->getFolderById($folder);

                  // Get the name of the folder or file
                  $name = $folderObj->folder_name;

                  // Add the data to the $resourceFoldersData array
                  $knowledgeFoldersData[$key] = [
              'id' => $folder,
              'folderName' => $name,
              // create an empty array for the files
              'files' => []
            ];

                  // Get the data within the folder
                  $containedData = $dataModel->getDataByFolderId($folder);
            
                  // Loop through containedData and process the data
                  foreach ($containedData as $file) {
                      $props = [
                'file_name' => $file->file_name,
                'file_path' => substr($file->file_path, 2)
              ];

                      // Add the array of the file data to the files array
                      array_push($knowledgeFoldersData[$key]['files'], $props);
                  }
              }

              // Return the data
              return $knowledgeFoldersData;
          } else {
              // Set it to emptyr
              $knowledgeFoldersData = [];
              // Return the data
              return $knowledgeFoldersData;
          }
      }

      /****************************************************
         PRIVATE METHOD: PROCESS THE REQUIRED RESOURCES
      *****************************************************/

      private function processRequiredResources($data)
      {
          $fileTypes = $this->fileTypes;
          $dataModel = $this->dataModel;

          // Get the required resources (files) and process them
          if (!empty($data)) {
              $resources = explode('|', $data);

              // Init variable
              $resourcesData = [];

              // Loop through the $resources array
              foreach ($resources as $resource) {

            // If the $resource is empty skip it
                  if (empty($resource)) {
                      continue;
                  }

                  // Get the file from DB
                  $fileObj = $dataModel->getFileById($resource);

                  // Get the name of the file
                  $name = $fileObj->file_name;

                  // Get the path
                  $path = $fileObj->file_path;
                  // Remove '..' from the path
                  $path = substr($path, 2);

                  // Get the file extension
                  $fileExt = substr(strrchr($name, '.'), 1);

                  // Determine the respective font-awesome css icon-class
                  if (isset($fileTypes[$fileExt])) {
                      $iconClass = $fileTypes[$fileExt];
                  } else {
                      $iconClass = $fileTypes['blank'];
                  }

                  // Add all the data to the $resourcesData
                  array_push($resourcesData, array(
              'id' => $resource,
              'name' => $name,
              'path' => $path,
              'extension' => $fileExt,
              'icon_class' => $iconClass
            ));
              }

              return $resourcesData;
          } else {
              // Set it to empty
              $resourcesData = [];

              return $resourcesData;
          }
      }
      
      private function processRequiredResourcesData($data)
      {

        // Get the required model for further action
          $dataModel = $this->dataModel;

          if (!empty($data)) {
              $resourceFolders = explode('|', $data);

              // Init variable
              $resourceFoldersData = [];

              // Loop through the $resourceFolders array
              foreach ($resourceFolders as $key => $folder) {
                  // If the $folder is empty skip it
                  if (empty($folder)) {
                      continue;
                  }

                  // Get the folder from DB
                  $folderObj = $dataModel->getFolderById($folder);

                  // Get the name of the folder or file
                  $name = $folderObj->folder_name;

                  // Add the data to the $resourceFoldersData array
                  $resourceFoldersData[$key] = [
              'id' => $folder,
              'folderName' => $name,
              // create an empty array for the files
              'files' => []
            ];
            
                  // Get the data within the folder
                  $containedData = $dataModel->getDataByFolderId($folder);

                  // Loop through containedData and process the data
                  foreach ($containedData as $file) {
                      $props = [
                'file_name' => $file->file_name,
                'file_path' => substr($file->file_path, 2)
              ];

                      // Add the array of the file data to the files array
                      array_push($resourceFoldersData[$key]['files'], $props);
                  }
              }

              return $resourceFoldersData;
          } else {
              // Set it to empty
              $resourceFoldersData = [];

              return $resourceFoldersData;
          }
      }


      /* ##################################################### */
      /* ################# METHOD: SHOW TASK ################# */
      /* ##################################################### */

      public function show($id)
      {
          if ($this->privUser->hasPrivilege('33') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Maßnahmesektion zu betreten', 'alert alert-danger');
              redirect('users/dashboard');
          }

          // Get the method
          // $id is coming from the URL and is passed to the postModel
          $method = $this->methodModel->getAnyMethodById($id);

          //die(var_dump($method));

          // Check if method is archived(empty)
          if (empty($method)) {
              $archivedMethod = $this->methodModel->getArchivedMethodById($id);
          
              if (empty($archivedMethod)) {
                  redirect('methods');
              } else {
                  // replace method with the archived method
                  $method = $archivedMethod;
              }
          }
        
          // Get the creator of the method
          $user = $this->userModel->getUserById($method->user_id);

          /* ################################################## */
          /* #################### PROCESS ##################### */
        
          $process = '';

          if (!empty($method->process_id)) {
              // Get the process
              $process = $this->processModel->getProcessById($method->process_id);
          }

          /* ############################################### */
          /* ################## DEPARTMENT ################# */

          $method->occurrence = $this->organModel->getOrganById($method->occurrence)->name;

          /* ############################################### */
          /* #################### ROLES #################### */

          // string to array of objects conversion

          $r_responsible = $this->processRole($method->roles_responsible);
          $r_concerned = $this->processRole($method->roles_concerned);
          $r_contributing = $this->processRole($method->roles_contributing);
          $r_information = $this->processRole($method->roles_information);

          /* ############################################### */
        

        
          /* ############################################### */
          /* ################ CHECK INTERVALS ############## */

          // Make sure there is a reference date before procceeding
          if (!empty($method->reference_date)) {
              // ######## REFERENCE DATE ############
              // Get the date and convert it
              // source: helpers/date_time_helper.php
              $dateArray = convert_date_time($method->reference_date);
              // Prepare variable for output
              $referenceDate = $dateArray['day'] .'. '. $dateArray['month'] .' '. $dateArray['year'];
          } else {
              // Set referenceDate to an error message
              $referenceDate = 'nicht angegeben';
          }

          /* ############################################### */
          /* ################## CHECKDATE ################## */

          if (!empty($method->check_date)) {
              $check_date = convert_date_time($method->check_date);
          } else {
              $check_date = false;
          }
          $nextCheckdate = calculateNextCheckdate($method->check_date);

          //die( var_dump($nextCheckdate) );

          /* ############################################### */
          /* ################## CHECKS ##################### */

          $checks = $this->methodModel->getChecksByMethodId($id);

          if (!empty($checks)) {
              // Init output array
              $checksOutput = [];
              foreach ($checks as $check) {
                  // Get the user
                  $user = $this->userModel->getUserById($check->user_id);
                  // Get the timestamp and convert it
                  $checkedAt = convert_date_time($check->checked_at);
                  // Add the data to the outputArray
                  $checksOutput[] = array(
              'user' => $user,
              'checked_at' => $checkedAt,
              'comment' => $check->comment
            );
              }
          } else {
              $checksOutput = '';
          }

          /* ############################################### */
          /* ################# REMINDERS ################### */

          $reminders = [
          'before' => '',
          'at' => '',
          'after' => ''
        ];

          if ($method->remind_before == 0) {
              $reminders['before'] = false;
          } else {
              $reminders['before'] = $method->remind_before;
          }

          if ($method->remind_at == 0) {
              $reminders['at'] = false;
          } else {
              $reminders['at'] = $method->remind_at;
          }

          if ($method->remind_after == 0) {
              $reminders['after'] = false;
          } else {
              $reminders['after'] = $method->remind_after;
          }

          /* ##################################################### */
          /* ############### UNACCEPTED ASSIGNMENTS ############## */

          // Get all unaccepted assignments
          $unacceptedAssignments = $this->methodModel->getUnacceptedAssignmentsByMethodId($id);

          // Check if this method has unaccepted assignments
          if (!empty($unacceptedAssignments)) {
              // Init variable
              $nonconfirmingUsers = [];
              // Loop through unacceptedAssignments
              foreach ($unacceptedAssignments as $assignment) {
                  // Get the role
                  // source: helpers/role_helper.php
                  $role = translateRoleLanguage($assignment->role);
                  // Prepare the array for output
                  $nonconfirmingUsers[] = array(
              'user_id' => $assignment->user_id,
              'role' => $role,
              'role_js' => $assignment->role // for the javascript acceptance process
            );
              }
          } else {
              //empty
              // set the output array to false
              $nonconfirmingUsers = false;
          }

          /* ############################################### */
          /* #################### TAGS ##################### */

          // Split the tags string into an array of tags
          $tags = explode(',', $method->tags);

          /* ########################################################### */
          /* ################## FLEXIBLE QUESTIONS ##################### */

          $questions = $this->questionModel->getQuestionsByMethodId($id);
          // Init variable
          $questionData = [];

          if (!empty($questions)) {
              foreach ($questions as $questItem) {
                  // Fill the questionData array with values
                  $questionData[] = $questItem;
              }
          }
        

          /* ####### TESTING ######## */

          //require_once '../app/cronjobs/reminders.php';

          //$reminders = remind($this->methodModel, $this->userModel);

          // die(var_dump($reminders));


          // Add Checkdate MSG to methods
          $method->startDate_msg = calculateNextCheckdate($method->reference_date);


          $data = [
          'bodyClass' => 'methods showMethod',
          'privUser' => $this->privUser,
          'method' => $method,
          'process' => $process,
          'tags' => $tags,
          'roles_responsible' => $r_responsible,
          'roles_concerned' => $r_concerned,
          'roles_contributing' => $r_contributing,
          'roles_information' => $r_information,
          'non_confirming_users' => $nonconfirmingUsers,
          'reference_date' => $referenceDate,
          'checkdate' => $check_date,
          'next_checkdate' => $nextCheckdate,
          'checks' => $checksOutput,
          'reminders' => $reminders,
          'required_knowledge' => $this->processRequiredKnowledge($method->required_knowledge),
          'required_knowledge_folders' => $this->processRequiredKnowledgeData($method->required_knowledge_folders),
          'required_resources' => $this->processRequiredResources($method->required_resources),
          'required_resources_folders' => $this->processRequiredResourcesData($method->required_resources_folders),
          'user' => $user,
          'questions' => $questionData
        ];

          $this->view('methods/show', $data);
      }

      /**********************
         METHOD: EDIT TASK
       **********************/

      public function edit($id)
      {


        /////////////////////////////////////
          /////// Validate Privileges /////////
          /////////////////////////////////////

          if ($this->privUser->hasPrivilege('34') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Maßnahmen zu bearbeiten', 'alert alert-danger');
              redirect('methods/show/' .$id);
          }
        
          //////////////////////////////////////
          //////////////////////////////////////

          // Get existing method from model
          $method = $this->methodModel->getUnarchivedMethodById($id);

          if ($method->is_approved == 'false') {
              // TASK = UNAPPROVED PROCESSD TASK
              // Validate the users priviledge
              if ($this->privUser->hasPrivilege('23') != true) {
                  prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt nicht-freigegebene Prozessmaßnahmen zu bearbeiten', 'alert alert-danger');
                  redirect('methods/show/' .$id);
              }
          }

          /* ########################################### */
          /* ################ GET ORGANS ############### */

          $organs = $this->organModel->getOrgansWithoutUser();

          foreach ($organs as $index => $organ) {

          // Filter out inactive organs
              if ($organ->is_active == null) {
                  // Remove it
                  unset($organs[$index]);
              }
          
              // Check if the current organ is the staff_functions organ
              if ($organ->id == 2) {
                  // STAFF FUNCTIONS

                  // Get the suborgans (which are the actual staff functions)
                  $suborgans = $this->organModel->getSubOrgansByParentId(2);

                  if (!empty($suborgans)) {
                      foreach ($suborgans as $suborgan) {
                
                // Turn id string into array of ids
                          $id_array =  $this->organModel->explodeIdString($suborgan->staff);

                          // Populate the array
                          $users = $this->organModel->populateUserIdArray($id_array, $this->userModel);

                          // Attach the populated array to the suborgan
                          $suborgan->users = $users;
                      }
                  }
                  // Attach the populated array to the organ
                  $organ->suborgans = $suborgans;
              } else {
                  // REGULAR ORGANS
            
                  // Turn id strings into id arrays
                  $managers = explode(',', $organ->management_level);
                  $staff = explode(',', $organ->staff);

                  // Merge both arrays
                  $users = array_merge($managers, $staff);
                  // Remove duplicate ids
                  $users = array_unique($users);

                  // Populate the array
                  $users = $this->organModel->populateUserIdArray($users, $this->userModel);

                  // Attach the populated array to the organ
                  $organ->users = $users;
              }
          }

          /* ############################################# */



          // Check if there is a post request
          // So this block only runs if the permission in the else codeblock is already validated
          // Otherwise a post request would not be possible to make
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {

          // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              // Get organs
              $organs = $this->organModel->getOrgans();
              $processes = $this->processModel->getAllProcesses();

              $data = [
            'bodyClass' => 'methods editMethod',
            'privUser' => $this->privUser,
            'id' => $id,
            'user_id' => $_SESSION['user_id'],
            'name' => trim($_POST['name']),
            'description' => trim($_POST['description']),
            'occurrence' => intval($_POST['occurrence']),
            'process_id' => trim($_POST['process_id']),
            //
            'organs' => $organs,
            'processes' => $processes,
            //
            'reason' => trim($_POST['reason']),
            'solution' => trim($_POST['solution']),
            'goal' => trim($_POST['goal']),
            'status' => trim($_POST['status']),
            'priority' => intval($_POST['priority']),
            'required_knowledge_text' => trim($_POST['required_knowledge_text']),
            'required_knowledge' => trim($_POST['required_knowledge']),
            'required_knowledge_folders' => trim($_POST['required_knowledge_folders']),
            'required_knowledge_UI' => '',
            'required_knowledge_folders_UI' => '',
            // Roles
            'roles_responsible' => trim($_POST['roles_responsible']),
            'roles_concerned' => trim($_POST['roles_concerned']),
            'roles_contributing' => trim($_POST['roles_contributing']),
            'roles_information' => trim($_POST['roles_information']),
            'iso_relevant' => trim($_POST['iso_relevant']),
            // Recurring Check Intervals
            'check_intervals' => '',
            'reference_date' => '',
            'activate_intervalsMethod' => '',
            'interval_count' => '',
            'interval_format' => '',
            'check_date' => '',
            // Check Reminders
            'reminders_activated' => '',
            'remind_before' => '',
            'remind_at' => '',
            'remind_after' => '',
            //
            'required_resources_text' => trim($_POST['required_resources_text']),
            'required_resources' => trim($_POST['required_resources']),
            'required_resources_folders' => trim($_POST['required_resources_folders']),
            'required_resources_UI' => '',
            'required_resources_folders_UI' => '',
            'existing_compentencies' => trim($_POST['existing_compentencies']),
            'required_education' => trim($_POST['required_education']),
            'tags' => trim($_POST['tags']),
            'taglist' => '',
            'tags_array' => '',
            ////////////////////////
            // Errors
            'name_err' => '',
            // description is optional
            'reason_err' => '',
            'solution_err' => '',
            'goal_err' => '',
            'status_err' => '',
            'priority_err' => '',
            'required_knowledge_err' => '',
            'roles_err' => '',
            'iso_relevant_err' => '',
            'check_intervals_err' => '',
            'reminders_err' => '',
            'reference_date_err' => '',
            'required_resources_err' => '',
            'existing_compentencies_err' => '',
            'required_education_err' => '',
            'tags_err' => ''
          ];

              // Validate data
              if (empty($data['name'])) {
                  $data['name_err'] = 'Bitte Namen eintragen';
              }

              if (empty($data['occurrence'])) {
                  $data['occurrence'] = null;
              }

              if (empty($data['process_id'])) {
                  $data['process_id'] = null;
              }

              if (empty($data['reason'])) {
                  $data['reason_err'] = 'Bitte Ursache für die Feststellung eintragen';
              }

              if (empty($data['solution'])) {
                  $data['solution_err'] = 'Bitte Aktion / Maßnahme eintragen';
              }

              if (empty($data['goal'])) {
                  $data['goal'] = null;
                  $data['goal_err'] = 'Bitte Kategorie zuteilen';
              }
              if (empty($data['status'])) {
                  $data['status'] = null;
                  $data['status_err'] = 'Bitte Status einstufen';
              }

              if (empty($data['priority'])) {
                  $data['priority'] = null;
                  $data['priority_err'] = 'Bitte Priorität einstufen';
              }

              if (empty($data['existing_compentencies'])) {
                  //$data['existing_compentencies_err'] = 'Bitte bestehende Kompetenzen angeben';
              }
              if (empty($data['required_education'])) {
                  //$data['required_education_err'] = 'Bitte erforderlichen Weiterbildungsbedarf angeben';
              }
          
              /* ##################################### */
              /* ######### ROLES VALIDATION ########## */

              $r_responsible = $this->processRole($data['roles_responsible']);
              $r_concerned = $this->processRole($data['roles_concerned']);
              $r_contributing = $this->processRole($data['roles_contributing']);
              $r_information = $this->processRole($data['roles_information']);

              // Get the stored method from the database
              $storedMethod = $this->methodModel->getMethodById($id);
              // Get the user ids of the roles and store them
              $storedData = [
            'roles_responsible' => $storedMethod->roles_responsible,
            'roles_concerned' => $storedMethod->roles_concerned,
            'roles_contributing' => $storedMethod->roles_contributing,
            'roles_information' => $storedMethod->roles_information
          ];

              function compareUsersInRole($existingRoleData, $newRoleData)
              {
                  // Split the strings of ids into arrays of ids
                  $existingUserIds = explode(',', $existingRoleData);
                  $newUserIds = explode(',', $newRoleData);
                  // Init array for output
                  $toBeNotified = [];
                  // Loop through the new users and filter out the new ones
                  foreach ($newUserIds as $userId) {
                      if (!in_array($userId, $existingUserIds)) {
                          // Store the new users in the $toBeNotified array
                          $toBeNotified[] = $userId;
                      }
                  }
                  // Output the array
                  return $toBeNotified;
              }

              $notify = [
            'roles_responsible' => compareUsersInRole($storedData['roles_responsible'], $data['roles_responsible']),
            'roles_concerned' => compareUsersInRole($storedData['roles_concerned'], $data['roles_concerned']),
            'roles_contributing' => compareUsersInRole($storedData['roles_contributing'], $data['roles_contributing']),
            'roles_information' => compareUsersInRole($storedData['roles_information'], $data['roles_information'])
          ];

              /* ############################################################### */
              /* ##################### REQUIRED KNOWLEDGE ###################### */

              $knowledgeData = $this->processRequiredKnowledge($data['required_knowledge']);
              $knowledgeFoldersData = $this->processRequiredKnowledgeData($data['required_knowledge_folders']);

              // Update the UI data
              $data['required_knowledge_UI'] = $knowledgeData;
              $data['required_knowledge_folders_UI'] = $knowledgeFoldersData;

              /* ############################################################### */
              /* ##################### REQUIRED RESOURCES ###################### */

              $resourcesData = $this->processRequiredResources($data['required_resources']);
              $resourceFoldersData = $this->processRequiredResourcesData($data['required_resources_folders']);
        
              // Update the UI data
              $data['required_resources_UI'] = $resourcesData;
              $data['required_resources_folders_UI'] = $resourceFoldersData;

              /* ############################################################ */
              /* ##################### CHECK INTERVALS ###################### */

              // Check if the activate_intervalsMethod Checkbox is set
              if (isset($_POST['activate_intervalsMethod'])) {
                  // Validated
                  // Get the post data
                  $data['interval_count'] = trim($_POST['interval_count']);
                  $data['interval_format']= trim($_POST['interval_format']);
                  // Set the data variables
                  $data['reference_day'] = trim($_POST['reference_day']);
                  $data['reference_month'] = trim($_POST['reference_month']);
                  $data['reference_year'] = trim($_POST['reference_year']);

                  $day = trim($_POST['reference_day']);
                  $month = trim($_POST['reference_month']);
                  $year = trim($_POST['reference_year']);

                  // Validated - Check if intervall fields are empty
                  if (empty($data['interval_count']) && $data['interval_format'] == 'empty') {
                      // Add the appropriate error message to the corresponding variable
                      $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl und ein Intervallformat aus.';
                  } elseif (empty($data['interval_count'])) {
                      // Add the appropriate error message to the corresponding variable
                      $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl aus.';
                  } elseif ($data['interval_format'] == 'empty') {
                      // Add the appropriate error message to the corresponding variable
                      $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallformat aus.';
                  } elseif (empty($day) || empty($month) || empty($year)) {
                      // Add the error message
                      $data['reference_date_err'] = 'Bitte füllen Sie alle Felder des Referenzdatums aus.';
                  } else {
                      // Construct the reference date
                      $data['reference_date'] = $year .'-'. $month .'-'. $day;
                      // Calculate the checkdate
                      // Get the reference date from the DB
                      $timestamp = strtotime($data['reference_date']);
                      // Calculate the next check date
                      // source: helpers/date_time_helper.php
                      $checkdate = calculateCheckdate($timestamp, $data['interval_count'], $data['interval_format']);

                      $data['check_date'] = $checkdate['year'] .'-'. $checkdate['month'] .'-'. $checkdate['day'];
                      // Construct the check interval text value
                      $data['check_intervals'] = $data['interval_count'] .' '. $data['interval_format'];
                  }
              } else {
                  // Invalid
                  $data['check_intervals'] = 'Kein Check Intervall';
                  $data['activate_intervalsMethod'] = false;
                  // Set the variables to NULL
                  $data['reference_date'] = null;
                  $data['interval_count'] = null;
                  $data['interval_format'] = null;
                  $data['check_date'] = null;
                  $data['check_intervals_err'] = '';
              }

          
              /* #################################################################### */
              /* ############################# REMINDERS ############################ */

              // Check if the reminders and check intervals both have been activated
              if (isset($_POST['activate_reminders']) && isset($_POST['activate_intervalsMethod'])) {
                  // ACTIVE
                  // Fill the variables with the respective POST values
                  $data['reminders_activated'] = true;

                  // If the value is 0 set it to NULL
                  // Otherwise set it to the value
                  if (trim($_POST['remind_before']) == 0) {
                      $data['remind_before'] = null;
                  } else {
                      $data['remind_before'] = trim($_POST['remind_before']);
                  }

                  // If the value is 0 set it to NULL
                  // Otherwise set it to the value
                  if (trim($_POST['remind_at']) != 'activated') {
                      $data['remind_at'] = null;
                  } else {
                      $data['remind_at'] = trim($_POST['remind_at']);
                  }

                  // If the value is 0 set it to NULL
                  // Otherwise set it to the value
                  if (trim($_POST['remind_after']) == 0) {
                      $data['remind_after'] = null;
                  } else {
                      $data['remind_after'] = trim($_POST['remind_after']);
                  }

                  // Check if all fields are empty or unset
                  if (empty($data['remind_before']) && empty($data['remind_at']) && empty($data['remind_after'])) {
                      // ALL EMPTY
                      $data['reminders_err'] = 'Bitte wählen Sie mindestens eine der drei Erinnerungsmöglichkeiten aus oder deaktivieren Sie die Erinnerungsfunktion.';
                  }
              } elseif (isset($_POST['activate_reminders']) && !isset($_POST['activate_intervalsMethod'])) {
                  // ERROR: CHECK INTERVALS NOT ACTIVATED
                  $data['reminders_err'] = 'Die Erinnerungsfunktion kann nur aktiviert werden, sofern zunächst ein Check-Intervall definiert wurde.';
              } else {
                  // INACTIVE
                  $data['reminders_activated'] = false;
                  $data['remind_before'] = null;
                  $data['remind_at'] = null;
                  $data['remind_after'] = null;
              }

              /* ##################################### */
              /* ################ TAGS ############### */

              $data['taglist'] = $data['tags'];
              if (!empty($data['tags'])) {
                  // Validated
                  // Create an array of tags
                  $data['tags_array'] = explode(',', $data['tags']);
              } else {
                  // Invalid
                  // Set the tags array to empty
                  $data['tags_array'] = [];
              }

              /* ######################################### */
              /* ########### FLEXIBLE QUESTIONS ########## */

              // Make sure there is a flexible question
              if (isset($_POST['flexible_questions'])) {
                  $data['flexible_questions'] = $_POST['flexible_questions'];

                  // Explode the flexible_questions string
                  $questionsArr = explode('||', $_POST['flexible_questions']);

                  // Init variable for later
                  $questionData = [];

                  foreach ($questionsArr as $questionObj) {
                      // Make sure the current item is not empty
                      if (!empty($questionObj)) {
                          // Explode it into an array
                          $parts = explode('/!/', $questionObj);
                          // Add each part to a correspondig variable
                          $question = $parts[0];
                          $yesNo = substr($parts[1], 3);
                          $notify = substr($parts[2], 2);
                          $remind = substr($parts[3], 2);

                          // Prepare $answers_UI for output
                          $answers_UI = '';

                          if ($yesNo == 'true') {
                              $answers_UI .= 'Ja/Nein, ';
                          }

                          if ($notify == 'true') {
                              $answers_UI .= 'Mitarbeiterbenachrichtigung, ';
                          }

                          if ($remind == 'true') {
                              $answers_UI .= 'Erinnerung, ';
                          }
                          // Remove the space and the comma
                          $answers_UI = substr($answers_UI, 0, -2);

                          $questionData[] = array(
                  'question' => $question,
                  'yesNo' => $yesNo,
                  'notify' => $notify,
                  'remind' => $remind,
                  'answers_UI' => $answers_UI,
                  'string_UI' => $questionObj
                );
                      }
                  }
                  // Store questionData for UI
                  $data['flexible_questions_UI'] = $questionData;
              }

              //die(var_dump($questionData));

              // Make sure no errors
              if (empty($data['name_err']) &&
              empty($data['reason_err']) &&
              empty($data['solution_err']) &&
              empty($data['goal_err']) &&
              empty($data['status_err']) &&
              empty($data['priority_err']) &&
              empty($data['required_knowledge_err']) &&
              empty($data['roles_err']) &&
              empty($data['iso_relevant_err']) &&
              empty($data['check_intervals_err']) &&
              empty($data['reminders_err']) &&
              empty($data['reference_date_err']) &&
              empty($data['required_resources_err']) &&
              empty($data['tags_err'])) {
                  // Validated
                  if ($this->methodModel->updateMethod($data)) {
                      // TASKUPDATE: SUCCESSFULL
                      // SKIP ROLE ASSIGNMENTS FOR PROCESSTASKS
                      // IS SINGLETASK
                      // ASSIGN THE TASK TO THE USERS
                      // role: responsible
                      $this->assignMethodToUsers($notify['roles_responsible'], 'responsible', $id);
                      // role: concerned
                      $this->assignMethodToUsers($notify['roles_concerned'], 'concerned', $id);
                      // role: contributing
                      $this->assignMethodToUsers($notify['roles_contributing'], 'contributing', $id);
                      // role: information
                      $this->assignMethodToUsers($notify['roles_information'], 'information', $id);


                      // Remove all questions pertaining to this method
                      $this->questionModel->removeQuestionsByMethodId($id);
                      // Submit all of them
                      // Loop through all questions
                      foreach ($questionData as $questItem) {
                          // Prepare data
                          $data =  [
                      'question' => $questItem['question'],
                      'yesNo' => $questItem['yesNo'],
                      'notify' => $questItem['notify'],
                      'remind' => $questItem['remind'],
                      'task_id' => null,
                      'method_id' => $id,
                      'document_id' => null
                    ];
                          // Submit the question to the DB
                          $this->questionModel->addFlexibleQuestion($data);
                      }

                      prepareFlash('method_message', 'Die Maßnahme wurde aktualisiert');
                      // Redirect to the show method view
                      redirect('methods/show/'.$id);
                  } else {
                      // methodupdate unsuccessful
                      die('Ein Fehler ist aufgetreten. Bitte kontaktieren Sie den Entwickler');
                  }
              } else {
                  /* ######## ROLES ######### */

                  // Update the fields for the UI
                  $data['roles_responsible_id'] = $data['roles_responsible'];
                  $data['roles_responsible'] = $r_responsible;
                  // Update the fields for the UI
                  $data['roles_concerned_id'] = $data['roles_concerned'];
                  $data['roles_concerned'] = $r_concerned;
                  // Update the fields for the UI
                  $data['roles_contributing_id'] = $data['roles_contributing'];
                  $data['roles_contributing'] = $r_contributing;
                  // Update the fields for the UI
                  $data['roles_information_id'] = $data['roles_information'];
                  $data['roles_information'] = $r_information;

                  // Load the view with Errors
                  $this->view('methods/edit', $data);

                  //die(var_dump($data));
              }
          } else {
              // Otherwise load the view, in order for the user to be able to edit the method

        
        

              /* ############### ROLES ############## */
              // string to array of objects conversion

              $r_responsible = $this->processRole($method->roles_responsible);
              $r_concerned = $this->processRole($method->roles_concerned);
              $r_contributing = $this->processRole($method->roles_contributing);
              $r_information = $this->processRole($method->roles_information);

              /* ##################### REQUIRED KNOWLEDGE ###################### */

              $knowledgeData = $this->processRequiredKnowledge($method->required_knowledge);
              $knowledgeFoldersData = $this->processRequiredKnowledgeData($method->required_knowledge_folders);

              /* ##################### REQUIRED RESOURCES ##################### */

              $resourcesData = $this->processRequiredResources($method->required_resources);
              $resourceFoldersData = $this->processRequiredResourcesData($method->required_resources_folders);


              /* ########### CHECK INTERVALS ########## */

              // Check if a check_interval is set
              if ($method->check_intervals == 'Kein Check Intervall' || empty($method->check_intervals)) {
                  // NOT SET
                  // create $activate_intervalsMethod var and set it to false
                  $activate_intervalsMethod = false;
              } else {
                  // SET
                  // create $activate_intervalsMethod var and set it to true
                  $activate_intervalsMethod = true;
              }

              /* ##################### REFERENCE DATE ####################### */

              // Make sure there is a reference date before procceeding
              if (!empty($method->reference_date)) {
                  // Get the date and convert it
                  // source: helpers/date_time_helper.php
                  $dateArray = convert_date_time($method->reference_date);
                  // Prepare variables for output
                  $reference_day = $dateArray['day'];
                  $reference_year = $dateArray['year'];
                  // Get the month as a number
                  $timestamp = strtotime($method->reference_date);
                  $reference_month = date("n", $timestamp);
              } else {
                  // NO REFERENCE DATE
                  // Prepare empty variables for output
                  $reference_day = '';
                  $reference_year = '';
                  $reference_month = '';
              }

              /* ################## REMINDERS ##################### */

              $remind_before = $method->remind_before;
              $remind_at = $method->remind_at;
              $remind_after = $method->remind_after;
              $reminders_activated = false;


              if ($remind_before != null || $remind_at != null || $remind_after != null) {
                  $reminders_activated = true;
              }

              /* ################## FLEXIBLE QUESTIONS ##################### */

              $questions = $this->questionModel->getQuestionsByMethodId($id);
              // Init variable
              $questionData = [];
              $questionsValue = '';

              if (!empty($questions)) {
                  foreach ($questions as $questItem) {
                      // Prepare variables for output
                      $answers_UI = '';
                      $string_UI = $questItem->question;

                      // Fill them
                      if ($questItem->yes_no == 'true') {
                          $answers_UI .= 'Ja/Nein, ';
                          $string_UI .= '/!/YN:true';
                      } else {
                          $string_UI .= '/!/YN:false';
                      }

                      if ($questItem->notify == 'true') {
                          $answers_UI .= 'Mitarbeiterbenachrichtigung, ';
                          $string_UI .= '/!/N:true';
                      } else {
                          $string_UI .= '/!/N:false';
                      }

                      if ($questItem->remind == 'true') {
                          $answers_UI .= 'Erinnerung, ';
                          $string_UI .= '/!/R:true';
                      } else {
                          $string_UI .= '/!/R:false';
                      }

                      // Remove the space and the comma
                      $answers_UI = substr($answers_UI, 0, -2);

                      // Fill the questionData array with values
                      $questionData[] = array(
              'question' => $questItem->question,
              'answers_UI' => $answers_UI,
              'string_UI' => $string_UI
            );

                      $questionsValue .= $string_UI.'||';
                  }
              }

              $processes = $this->processModel->getAllProcesses();

              $data = [
          'bodyClass' => 'methods editMethod',
          'privUser' => $this->privUser,
          'id' => $id,
          'user_id' => $method->user_id,
          'name' => $method->name,
          'description' => $method->description,
          'occurrence' => $method->occurrence,
          'process_id' => $method->process_id,
          //
          'organs' => $organs,
          'processes' => $processes,
          //
          'reason' => $method->reason,
          'solution' => $method->solution,
          'startDate_msg' => $method->startDate_msg,
          'goal' => $method->goal,
          'status' => $method->status,
          'priority' => $method->priority,
          // Required Knowledge
          'required_knowledge_text' => $method->required_knowledge_text,
          'required_knowledge' => $method->required_knowledge,
          'required_knowledge_folders' => $method->required_knowledge_folders,
          'required_knowledge_UI' => $knowledgeData,
          'required_knowledge_folders_UI' => $knowledgeFoldersData,
          // Roles
          'roles_responsible' => $r_responsible,
          'roles_responsible_id' => $method->roles_responsible,
          'roles_concerned' => $r_concerned,
          'roles_concerned_id' => $method->roles_concerned,
          'roles_contributing' => $r_contributing,
          'roles_contributing_id' => $method->roles_contributing,
          'roles_information' => $r_information,
          'roles_information_id' => $method->roles_information,
          //
          'iso_relevant' => $method->iso_relevant,
          // Check Intervals
          'check_intervals' => '',
          'reference_date' => '',
          'interval_count' => $method->interval_count,
          'interval_format' => $method->interval_format,
          'activate_intervalsMethod' => $activate_intervalsMethod,
          'reference_day' => $reference_day,
          'reference_month' => $reference_month,
          'reference_year' => $reference_year,
          // Check Reminders
          'reminders_activated' => $reminders_activated,
          'remind_before' => $remind_before,
          'remind_at' => $remind_at,
          'remind_after' => $remind_after,
          // Required Resources
          'required_resources_text' => $method->required_resources_text,
          'required_resources' => $method->required_resources,
          'required_resources_folders' => $method->required_resources_folders,
          'required_resources_UI' => $resourcesData,
          'required_resources_folders_UI' => $resourceFoldersData,
          //
          'existing_compentencies' => $method->existing_compentencies,
          'required_education' => $method->required_education,
          'taglist' => $method->tags,
          'tags_array' => explode(',', $method->tags),
          'flexible_questions' => $questionsValue,
          'flexible_questions_UI' => $questionData,
          // ##########################
          // ######## Errors ##########
          'name_err' => '',
          // description is optional
          'occurrence_err' => '',
          'process_id_err' => '',
          'reason_err' => '',
          'solution_err' => '',
          'goal_err' => '',
          'status_err' => '',
          'priority_err' => '',
          'required_knowledge_err' => '',
          'roles_err' => '',
          'iso_relevant_err' => '',
          'check_intervals_err' => '',
          'reminders_err' => '',
          'reference_date_err' => '',
          'required_resources_err' => '',
          'existing_compentencies_err' => '',
          'required_education_err' => '',
          'tags_err' => ''
        ];

              //die( var_dump( $r_responsible ));
        
              // Load view
              $this->view('methods/edit', $data);
          }
      }

      /*************************
         METHOD: DELETE TASK
       *************************/

      public function delete($id)
      {
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {

          /////////////////////////////////////
              /////// Validate Privileges /////////
              /////////////////////////////////////

              // Validate delete privilege
              if ($this->privUser->hasPrivilege('3') != true) {
                  prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt diese Maßnahme zu löschen', 'alert alert-danger');
                  redirect('methods/show/' .$id);
              }
          
              //////////////////////////////////////
              //////////////////////////////////////

              // Get the method
              $method = $this->methodModel->getMethodById($id);

              // IS A SINGLE TASK
              // Archive the method and check for success
              if ($this->methodModel->archiveMethod($id)) {
                  // SUCCESS
                  // Remove all connected notifications
                  $this->reminderModel->deactivateRemindersByMethodId($id);
                  $this->methodModel->removeUnacceptedAssignmentsByMethodId($id);
                  $this->methodModel->removeAcceptedMethodsByMethodId($id);

                  prepareFlash('method_message', 'Die Maßnahme wurde ins Archiv verschoben.');
                  redirect('methods');
              } else {
                  // ERROR
                  die('Etwas ist schief gelaufen. Bitte kontakieren Sie den Entwickler');
              }
          } else {
              redirect('methods');
          }
      }




      public function archiveMethodAndRemoveReminders($id)
      {
          $errors = [];

          if ($this->methodModel->archiveMethod($id) != true) {
              $errors[] = "Die Maßnahme A: $id konnte nicht archiviert werden.";
          }

          if ($this->reminderModel->removeRemindersByMethodId($id) != true) {
              $errors[] = "Die Erinnerungen der Maßnahme A: $id konnte nicht deaktiviert werden.";
          }


          if (!empty($errors)) {
              return false;
          } else {
              return true;
          }
      }
  }
