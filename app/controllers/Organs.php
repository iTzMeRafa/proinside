<?php

class Organs extends Controller
{
    public function __construct()
    {
        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Get the current user's permissions
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        // Load the organ model
        $this->organModel = $this->model('Organ');
        // Load the user model
        $this->userModel = $this->model('User');
        // Load the task model
        $this->taskModel = $this->model('Task');
    }

    /**********************
     * METHOD: INDEX
     **********************/
    public function index()
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        // Validate delete privilege
        if ($this->privUser->hasPrivilege('10') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Organsektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard/');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get all organs
        $organs = $this->organModel->getOrgansWithoutUser();

        $data = [
            'bodyClass' => 'organs',
            'privUser' => $this->privUser,
            'organs' => $organs
        ];
        // SQL JOIN - source: models/task.php
        // organs: id = organId
        // users: id = userId
        // organs: name = organName
        // users: name = userName
        // organs: created_at = organCreated
        // users: created_at = userCreated

        // Load view
        $this->view('organs/index', $data);
    }

    /**********************
     * METHOD: ADD ORGAN
     **********************/

    public function add()
    {
        // Check if there is a post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [
                'bodyClass' => 'organs addOrgan',
                'privUser' => $this->privUser,
                'user_id' => $_SESSION['user_id'],
                'name' => trim($_POST['name']),
                'abbreviation' => trim($_POST['abbreviation']),
                'description' => trim($_POST['description']),
                'management_level' => trim($_POST['management_level']),
                'staff' => trim($_POST['staff']),
                'level_id' => trim($_POST['level_id']),
                // Errors
                'name_err' => '',
                'abbreviation_err' => '',
                'level_err' => '',
                // description is optional
                'completion_err' => '',
            ];

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Bitte Namen eintragen';
            }

            // Validate data
            if (strlen($data['abbreviation']) > 3) {
                $data['abbreviation_err'] = 'Die maximale Länge des Kürzels sind drei Zeichen';
            }

            if (empty($data['management_level']) && empty($data['staff'])) {
                $data['completion_err'] = 'Bitte fügen Sie zu mindestens einer Ebene Nutzer hinzu.';
            }

            //die( var_dump($data) );

            // Make sure no errors
            if (empty($data['name_err']) &&
                empty($data['completion_err']) &&
                empty($data['abbreviation_err'])) {
                // Validated
                if ($this->organModel->addOrgan($data)) {

                    // Prepare the flash messages and redirect to tasks
                    prepareFlash('organ_message', 'Das Organ wurde erstellt');
                    redirect('organs');
                } else {
                    die('Ein Fehler ist aufgetreten. Bitte kontaktieren Sie den Entwickler');
                }
            } else {

                // Explode the strings with the ids
                $management_ids = $this->explodeIdString($data['management_level']);
                $staff_ids = $this->explodeIdString($data['staff']);

                // Get the user objects for each id
                $managers = $this->populateUserIdArray($management_ids);
                $staff = $this->populateUserIdArray($staff_ids);

                // Make them accessible in the UI
                $data['management_level_UI'] = $managers;
                $data['staff_UI'] = $staff;


                // Load the view with Errors
                $this->view('organs/add', $data);
            }
        } else {
            // Otherwise load the view, in order to be able to add a task

            /////////////////////////////////////
            /////// Validate Privileges /////////
            /////////////////////////////////////

            // Validate delete privilege
            if ($this->privUser->hasPrivilege('6') != true) {
                prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt neue Organe hinzuzufügen', 'alert alert-danger');
                redirect('users/dashboard/');
            }

            //////////////////////////////////////
            //////////////////////////////////////

            // Init data
            $data = [
                'bodyClass' => 'organs addOrgan',
                'privUser' => $this->privUser,
                'user_id' => '',
                'name' => '',
                'abbreviation' => '',
                'description' => '',
                'management_level' => '',
                'staff' => '',
                // Errors
                'name_err' => '',
                'completion_err' => '',
                'abbreviation_err' => ''
            ];

            // Load view
            $this->view('organs/add', $data);
        }
    }

    /**********************
     * METHOD: SHOW ORGAN
     **********************/

    public function show($id)
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        // Validate delete privilege
        if ($this->privUser->hasPrivilege('10') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Organsektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard/');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // $id is coming from the URL and is passed to the postModel
        $organ = $this->organModel->getOrganById($id);
        $user = $this->userModel->getUserById($organ->user_id);

        // Init empty managers array
        $managers = [];
        // Check if management_level has users
        if (!empty($organ->management_level)) {
            // Get the management_level string from the database and turn each manager (user_id) into an array object of $managers
            $managers = explode(',', $organ->management_level);
            // Loop through the $managers array and get each user as an object
            for ($i = 0; $i < count($managers); $i++) {
                $managers[$i] = $this->userModel->getUserById($managers[$i]);
            }
        }

        // Init empty staff array
        $staff = [];
        // Check if staff has users
        if (!empty($organ->staff)) {
            // Same thing as above but for the staff
            $staff = explode(',', $organ->staff);
            // Loop through the $managers array and get each user as an object
            for ($i = 0; $i < count($staff); $i++) {
                $staff[$i] = $this->userModel->getUserById($staff[$i]);
            }
        }

        // Init empty staff array
        $tasks = [];
        // Check if tasks has tasks
        if (!empty($organ->tasks)) {
            // Same thing as above but for the tasks
            $tasks = explode(',', $organ->tasks);
            // Loop through the $tasks array and get each task as an object
            for ($i = 0; $i < count($tasks); $i++) {
                $tasks[$i] = $this->taskModel->getTaskById($tasks[$i]);
                if (!is_object($tasks[$i])) {
                    array_splice($tasks, $i, 1);
                }
            }
        }

        /* ###################################### */
        /* ############# SUB ORGANS ############# */

        $suborgans = $this->organModel->getSubOrgansByParentId($id);
        // Init output array
        $subOrgansData = [];

        foreach ($suborgans as $suborgan) {

            // Init empty subStaff array
            $subStaff = [];
            // Check if subStaff is empty
            if (!empty($suborgan->staff)) {
                $subStaff = explode(',', $suborgan->staff);
                // Loop through the $staff array and get each user as an object
                for ($i = 0; $i < count($subStaff); $i++) {
                    $subUser = $this->userModel->getUserById($subStaff[$i]);
                    if (is_object($subUser)) {
                        $subStaff[$i] = $subUser;
                    }
                }
            }

            // Same thing as above but for the tasks
            $sub_tasks = explode(',', $suborgan->tasks);
            // Loop through the $tasks array and get each task as an object
            for ($i = 0; $i < count($sub_tasks); $i++) {
                $sub_tasks[$i] = $this->taskModel->getTaskById($sub_tasks[$i]);
                if (!is_object($sub_tasks[$i])) {
                    array_splice($sub_tasks, $i, 1);
                }
            }

            $subOrgansData[] = array(
                'name' => $suborgan->name,
                'staff' => $subStaff,
                'tasks' => $sub_tasks
            );
        }


        $data = [
            'bodyClass' => 'organs showOrgan',
            'privUser' => $this->privUser,
            'user' => $user,
            'organ' => $organ,
            'managers' => $managers,
            'staff' => $staff,
            'tasks' => $tasks,
            'methods' => '',
            'suborgans' => $subOrgansData
        ];

        $this->view('organs/show', $data);
    }

    /* #############################
        METHOD: EDIT ORGAN
     ############################## */

    public function edit($id)
    {


        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        // Validate delete privilege
        if ($this->privUser->hasPrivilege('7') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Organe zu bearbeiten', 'alert alert-danger');
            redirect('users/dashboard/');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get existing organ from model
        $organ = $this->organModel->getOrganById($id);

        /* ######################################### */
        /* ############### Main Organ ############## */

        // Get the management_level string from the database and turn each manager (user_id) into an array object of $managers
        if (!empty($organ->management_level)) {
            $managers = explode(',', $organ->management_level);
            // Loop through the $managers array and get each user as an object
            for ($i = 0; $i < count($managers); $i++) {
                $managers[$i] = $this->userModel->getUserById($managers[$i]);
            }
        } else {
            $managers = [];
        }

        if (!empty($organ->staff)) {
            // Same thing as above but for the staff
            $staff = explode(',', $organ->staff);
            // Loop through the $staff array and get each user as an object
            for ($i = 0; $i < count($staff); $i++) {
                $staff[$i] = $this->userModel->getUserById($staff[$i]);
            }
        } else {
            $staff = [];
        }


        // Check if there is a post request
        // So this block only runs if the Organ Owner in the else codeblock is already validated
        // Otherwise a post request would not be possible to make
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'bodyClass' => 'organs editOrgan',
                'privUser' => $this->privUser,
                'id' => $id,
                'user_id' => $_SESSION['user_id'],
                'name' => trim($_POST['name']),
                'abbreviation' => trim($_POST['abbreviation']),
                'description' => trim($_POST['description']),
                'management_level' => trim($_POST['management_level']),
                'staff' => trim($_POST['staff']),
                // Errors
                'name_err' => '',
                'abbreviation_err' => '',
                // description is optional
                'completion_err' => ''
            ];

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Bitte Namen eintragen';
            }

            // Validate abbreviaition
            if (strlen($data['abbreviation']) > 3) {
                $data['abbreviation_err'] = 'Die maximale Länge des Kürzels sind drei Zeichen';
            }

            if (empty($data['management_level']) && empty($data['staff'])) {
                $data['completion_err'] = 'Bitte fügen Sie zu mindestens einer Ebene Nutzer hinzu.';
            }


            // Make sure no errors
            if (empty($data['name_err']) &&
                empty($data['completion_err']) &&
                empty($data['abbreviation_err'])) {
                // Validated
                if ($this->organModel->updateOrgan($data)) {

                    // Prepare the flash messages and redirect to tasks
                    prepareFlash('organ_message', 'Das Organ wurde aktualisiert');
                    redirect('organs/show/' . $id);
                } else {
                    die('Ein Fehler ist aufgetreten. Bitte kontaktieren Sie den Entwickler');
                }
            } else {

                // Explode the strings with the ids
                $management_ids = $this->explodeIdString($data['management_level']);
                $staff_ids = $this->explodeIdString($data['staff']);

                // Get the user objects for each id
                $managers = $this->populateUserIdArray($management_ids);
                $staff = $this->populateUserIdArray($staff_ids);

                // Make them accessible in the UI
                $data['management_level_UI'] = $managers;
                $data['staff_UI'] = $staff;

                // Load the view with Errors
                $this->view('organs/edit', $data);
            }
        } else {

            // Load the view, in order for the user to be able to edit the organ

            $data = [
                'bodyClass' => 'organs editOrgan',
                'privUser' => $this->privUser,
                'id' => $id,
                'user_id' => $organ->user_id,
                'name' => $organ->name,
                'abbreviation' => $organ->abbreviation,
                'description' => $organ->description,
                'management_level' => $organ->management_level,
                'management_level_UI' => $managers,
                'staff' => $organ->staff,
                'staff_UI' => $staff
            ];

            // Load view
            $this->view('organs/edit', $data);
        }
    }

    /* ######################################### */
    /* ######### METHOD: ADD SUBORGANS ######### */
    /* ######################################### */

    public function addsub($id)
    {
        /* ############################################### */
        /* ################ GET BASIC DATA ############### */

        $parent = $this->organModel->getOrganById($id);
        $suborgans = $this->organModel->getSubOrgansByParentId($id);

        $allUsers = $this->userModel->getUsers();

        /* ################## SUB ORGANS ################# */

        // Init output array
        $subOrgansData = [];

        // Create array to store all the initial ids of the suborgans is
        $initialIds = [];

        foreach ($suborgans as $suborgan) {

            // Init empty subStaff array
            $subStaff = [];
            // Check if subStaff is empty
            if (!empty($suborgan->staff)) {
                $subStaff = explode(',', $suborgan->staff);
                // Loop through the $staff array and get each user as an object
                for ($i = 0; $i < count($subStaff); $i++) {
                    $subUser = $this->userModel->getUserById($subStaff[$i]);
                    if (is_object($subUser)) {
                        $subStaff[$i] = $subUser;
                    }
                }
            }

            // Same thing as above but for the tasks
            $sub_tasks = explode(',', $suborgan->tasks);
            // Loop through the $tasks array and get each task as an object
            for ($i = 0; $i < count($sub_tasks); $i++) {
                $sub_tasks[$i] = $this->taskModel->getTaskById($sub_tasks[$i]);
                if (!is_object($sub_tasks[$i])) {
                    array_splice($sub_tasks, $i, 1);
                }
            }

            $subOrgansData[] = array(
                'id' => $suborgan->id,
                'name' => $suborgan->name,
                'abbreviation' => $suborgan->abbreviation,
                'staff' => $subStaff,
                'tasks' => $sub_tasks,
                'workers' => $suborgan->staff
            );

            $initialIds[] = $suborgan->id;
        }


        /* ############### GET ALL WORKERS ############## */

        // Check if management_level has users
        if (!empty($parent->management_level)) {
            // Same thing as above but for the staff
            $managers = explode(',', $parent->management_level);
        }
        // Check if staff has users
        if (!empty($parent->staff)) {
            // Same thing as above but for the staff
            $staff = explode(',', $parent->staff);
        } else {
            $staff = [];
        }

        // Merge both arrays
        $merged_users = array_merge($managers, $staff);
        // Make sure every user is contained only once
        $merged_users = array_unique($merged_users);

        $workers = [];

        foreach ($merged_users as $userId) {
            $workers[] = $this->userModel->getUserById($userId);
        }


        // Check for POST REQUEST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            // Create array to store the organ sets in
            $organ_sets = [];
            // Init count variable
            $count = 1;
            $remainingIds = [];

            foreach ($_POST as $key => $field) {
                // Get the position of the first dash in the $key
                $pos = strpos($key, '-');
                // Extract the number of the organ from the $key via $pos
                $number = substr($key, 0, $pos);
                // Extract the field name from the $key via $pos
                $fieldName = substr($key, $pos + 1);

                if ($number == $count) {
                    // MATCHING
                    // If no array with the current $number exists
                    if (!array_key_exists($number, $organ_sets)) {
                        // Create it inside $organ_sets
                        $organ_sets[$number] = array();
                    }

                    // Store all the corresponding fields in that array
                    $organ_sets[$number][$fieldName] = $field;
                } else {
                    // NOT MATCHING
                    // Augment the count by one
                    $count = $count + 1;

                    // If no array with the current $number exists
                    if (!array_key_exists($number, $organ_sets)) {
                        // Create it
                        $organ_sets[$number] = array();
                    }


                    // Store all corresponding fields within that array
                    $organ_sets[$number][$fieldName] = $field;
                }

                if ($fieldName == 'id') {
                    $remainingIds[] = $field;
                }
            }

            // die(var_dump($organ_sets));

            // Reindex the arrays (starting from zero)
            $organ_sets = array_values($organ_sets);

            // Create a log array to catch possible errors
            $log = [];

            foreach ($organ_sets as $key => $set) {

                // Check if the organ already exists in the DB
                if (array_key_exists('id', $set)) {
                    // ALREADY EXISTING
                    // prepare data for model
                    $suborgan_data = [
                        'id' => $set['id'],
                        'name' => $set['name'],
                        'abbreviation' => $set['abbreviation'],
                        'staff' => $set['workers']
                    ];

                    // Update the suborgan and check for errors
                    if ($this->organModel->editSubOrgan($suborgan_data) == false) {
                        // Log error if exists
                        $log[] = 'Fehler bei der Aktualisierung des bestehnen Suborgans: ' . $set['name'];
                    }
                } else {
                    // NOT EXISTING
                    // prepare data for model
                    $suborgan_data = [
                        'parent_id' => $id,
                        'name' => $set['name'],
                        'abbreviation' => $set['abbreviation'],
                        'staff' => $set['workers'],
                        'tasks' => '',
                        'methods' => ''
                    ];

                    // Create new suborgan in DB and check for errors
                    if ($this->organModel->addSubOrgan($suborgan_data) == false) {
                        // Log error if exists
                        $log[] = 'Fehler bei der Erstellung eines Suborgans: ' . $set['name'];
                    }
                }
            }

            /* ################################################### */
            /* ###############  REMOVED SUBORGANS ################ */

            // Loop through all initialIds
            foreach ($initialIds as $iId) {
                // Compare the initialIds with the remainingIds
                // if the initial Id is not remaining
                if (in_array($iId, $remainingIds) == false) {
                    // Remove the element and check for errors
                    if ($this->organModel->deleteSubOrgan($iId) == false) {
                        // Log error if exists
                        $log[] = 'Fehler bei der Löschung eines Suborgans ' . $set['name'];
                    }
                }
            }


            if (!empty($log)) {
                // Prepare the flash messages and redirect to the process show view
                prepareFlash('organ_message', 'Bei der Erstellung der Suborgane ist ein Fehler aufgetreten.');
            } else {
                // Prepare the flash messages and redirect to the process show view
                prepareFlash('organ_message', 'Die Suborgane wurden erfolgreich aktualisiert.');
            }


            redirect('organs/show/' . $id);
        } else {

            // die(var_dump($workers));

            $data = [
                'bodyClass' => 'organs addSub',
                'privUser' => $this->privUser,
                'allUsers' => $allUsers,
                'parent' => $parent,
                'workers' => $workers,
                'suborgans' => $subOrgansData
            ];

            // Load view
            $this->view('organs/addsub', $data);
        }
    }

    /*************************
     * METHOD: DELETE ORGAN
     *************************/

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            /////////////////////////////////////
            /////// Validate Privileges /////////
            /////////////////////////////////////

            // Validate delete privilege
            if ($this->privUser->hasPrivilege('8') != true) {
                prepareFlash('organ_permission', 'Sie sind nicht dazu berechtigt dieses Organ zu löschen', 'alert alert-danger');
                redirect('organs/show/' . $id);
            }

            // Make sure static organs cannot be removed
            if ($id == 1 || $id == 2 || $id == 3) {
                redirect('organs/index');
            }

            //////////////////////////////////////
            //////////////////////////////////////

            // Get existing organ from model
            $organ = $this->organModel->getOrganById($id);

            if ($this->organModel->deleteOrgan($id)) {
                prepareFlash('organ_message', 'Das Organ wurde gelöscht');
                redirect('organs');
            } else {
                die('Etwas ist schief gelaufen. Bitte kontakieren Sie den Entwickler');
            }
        } else {
            redirect('organs');
        }
    }


    /* ############################################################ */
    /* #########  METHOD: ORGANIGRAM ############################## */
    /* ############################################################ */

    public function organigram()
    {
        if ($this->privUser->hasPrivilege('10') != true) {
            prepareFlash('organ_permission', 'Sie sind nicht dazu berechtigt das Organigram zu sehen', 'alert alert-danger');
            redirect('users/dashboard');
        }

        // Get all organs
        $organs = $this->organModel->getOrgansWithoutUser();


        // Loop through all organs
        foreach ($organs as $index => $organ) {
            ///////////////////////
            ////// GET USERS //////

            $explodedDepartments = $this->explodeUserDepartments($organ->management_level, $organ->staff);

            $managers = $this->populateUserIdArray($explodedDepartments['management_level_ids']);
            $staff = $this->populateUserIdArray($explodedDepartments['staff_ids']);


            if (!empty($managers)) {
                $organ->management_level = $managers;
            } else {
                $organ->management_level = [];
            }
            if (!empty($staff)) {
                $organ->staff = $staff;
            } else {
                $organ->staff = [];
            }

            ///////////////////////////
            ////// GET SUBORGANS //////

            $suborgans = $this->organModel->getSubOrgansByParentId($organ->id);

            if (!empty($suborgans)) {
                // FILLED


                foreach ($suborgans as $index => $suborgan) {

                    /////// GET USERS ///////
                    $explodedDepartments = $this->explodeUserDepartments($suborgan->staff, $empty);

                    $users = $this->populateUserIdArray($explodedDepartments['management_level_ids']);

                    if (!empty($users)) {
                        $suborgan->staff = $users;
                    }
                }
                $organ->suborgans = $suborgans;
            }
        }


        /* ############### GET STAFF FUNCTIONS ################# */

        $staff_functions = $organs[1]->suborgans;
        $numOfFuncs = count($staff_functions);

        $staff_functions_output = [
            'numOfFuncs' => $numOfFuncs,
            'staff_functions' => $staff_functions
        ];

        /*
        switch( $numOfFuncs ) {

          case '1':
            // One item left
            $column_1 = [
              'is_full' => false,
              'items' => $staff_functions
            ];
            break;
          case '2':
            // One item left - one item right
            $column_1 = [
              'is_full' => false,
              'items' => $staff_functions[0]
            ];

            $column_2 = [
              'is_full' => false,
              'items' => $staff_functions[1]
            ];

            $staff_functions_output[] = $column_1;
            $staff_functions_output[] = $column_2;



            break;
          case '3':
            // One colunn left - one item right

            $column_1 = [
              'is_full' => false,
              'items' => $staff_functions[0]
            ];

            $column_2 = [
              'is_full' => false,
              'items' => $staff_functions[1]
            ];

            $staff_functions_output[] = $column_1;
            $staff_functions_output[] = $column_2;

            break;
          case '4':
            // One colunn left - one column right
            die( 'four' );
            break;
          case '5':
            // One colunn left and one item left - one column right
            die( 'five' );
            break;
          case '6':
            // One colunn left and one item left - One colunn left and one item right
            die( 'six' );
            break;
          case '7':
            // Two columns left  - One colunn left and one item right
            die( 'seven' );
            break;
          case '8':
            // Two columns left - two columns right
            die( 'eight' );
            break;

        }
        */


        //die(var_dump($staff_functions_output));


        $data = [
            'bodyClass' => 'organs organigram',
            'privUser' => $this->privUser,
            'organs' => $organs,
            'staff_functions' => $staff_functions_output
        ];

        // Load view
        $this->view('organs/organigram', $data);
    }


    /* ################# EXPLODE USER DEPARTMENTS #################### */

    public function explodeUserDepartments($management_level, $staff)
    {
        if (!empty($management_level)) {
            // FILLED
            // Turn string of ids into array of ids
            $manager_ids = explode(',', $management_level);
        } else {
            // EMPTY
            // Set $manager_ids to an empty array
            $manager_ids = [];
        }

        if (!empty($staff)) {
            // FILLED
            // Turn string of ids into array of ids
            $staff_ids = explode(',', $staff);
        } else {
            // EMPTY
            // Set $staff_ids to an empty array
            $staff_ids = [];
        }

        $output = [
            'management_level_ids' => $manager_ids,
            'staff_ids' => $staff_ids
        ];

        return $output;
    }

    /* ################# EXPLODE USER ID STRING #################### */

    public function explodeIdString($id_string)
    {
        if (!empty($id_string)) {
            // FILLED
            // Turn string of ids into array of ids
            $output = explode(',', $id_string);
        } else {
            // EMPTY
            // Set $output an empty array
            $output = [];
        }

        return $output;
    }

    /* ################# POPULATE USER-ID-ARRAY #################### */

    public function populateUserIdArray($user_id_array)
    {
        if (!empty($user_id_array)) {
            // FILLED
            // Create output array
            $outputArray = [];

            // Loop through the array
            foreach ($user_id_array as $user_id) {

                // Get user object from DB
                $user = $this->userModel->getUserById($user_id);

                // Make sure it is filled
                if (is_object($user)) {
                    // Add it to the output array
                    $outputArray[] = $user;
                }
            }
        } else {
            // EMPTY
            // Create empty output array
            $outputArray = [];
        }

        return $outputArray;
    }
}
