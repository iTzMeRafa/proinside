<?php
  class Pages extends Controller
  {
      public function __construct()
      {
      }

      /********************
          METHOD: INDEX
       ********************/

      public function index()
      {
          if (isLoggedIn()) {
              redirect('users/dashboard');
          }

          $data = [
        'bodyClass' => 'indexPage',
        'title' => 'PROINSIDE App',
        'description' => 'Eine Webapplikation für Qualitätsmanagement von PROERGEBNIS'
      ];

          $this->view('pages/index', $data);
      }

      /********************
          METHOD: ABOUT
       ********************/

      public function about()
      {
          $data = [
        'bodyClass' => 'pageAbout',
        'title' => 'About PRO INSIDE App',
        'description' => 'Eine QM-Webapplikation für die Verbesserung Ihres Unternehmens.'
      ];

          $this->view('pages/about', $data);
      }
  }
