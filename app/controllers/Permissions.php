<?php

class Permissions extends Controller
{
    private $privUser;

    public function __construct()
    {
        // Make sure user is logged in
        if (!isLoggedIn()) {
            // Helper Function - source: "helpers/url_helper.php"
            redirect('users/login');
        }

        // Get the current user's permissions
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        // Load the required models
        $this->userModel = $this->model('User');
        $this->permModel = $this->model('Permission');
        $this->organModel = $this->model('Organ');
        $this->dataModel = $this->model('Data');
    }

    /**********************
     * METHOD: INDEX
     **********************/
    public function index()
    {
        if ($this->privUser->hasPrivilege('14') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Rechtesektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        // Get all roles
        $roles = $this->permModel->getRoles();
        // Init output array
        $roleData = [];

        foreach ($roles as $role) {
            $permissions = $this->permModel->getPermIdsByRoleId($role->role_id);
            $roleData[$role->role_id] = array(
                'name' => $role->role_name,
                'perms' => $permissions
            );
        }

        $data = [
            'bodyClass' => 'permissions permissions-index',
            'privUser' => $this->privUser,
            'roles' => $roleData
        ];

        // Load view
        $this->view('permissions/index', $data);
    }

    /**********************
     * METHOD: ADD ROLE
     **********************/

    public function add()
    {
        if ($this->privUser->hasPrivilege('26') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Rechterollen hinzuzufügen', 'alert alert-danger');
            redirect('users/dashboard');
        }

        // Check if there is a post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Get the active folders for folder access permissions
            $folders = $this->dataModel->getFolders();

            $data = [
                'bodyClass' => 'permissions permissions-add',
                'privUser' => $this->privUser,
                'folders' => $folders,
                //////////////////////////////////
                'name' => trim($_POST['name']),
                'description' => trim($_POST['description']),
                // Process Permissions
                'createProcessMatrixes' => isset($_POST['createProcessMatrixes']) ? $_POST['createProcessMatrixes'] : false,
                'approveProcessMatrixes' => isset($_POST['approveProcessMatrixes']) ? $_POST['approveProcessMatrixes'] : false,
                'editProcessMatrixes' => isset($_POST['editProcessMatrixes']) ? $_POST['editProcessMatrixes'] : false,
                'deleteProcessMatrixes' => isset($_POST['deleteProcessMatrixes']) ? $_POST['deleteProcessMatrixes'] : false,
                // Task Permissions
                'accessTaskSection' => isset($_POST['accessTaskSection']) ? $_POST['accessTaskSection'] : false,
                'addTasks' => isset($_POST['addTasks']) ? $_POST['addTasks'] : false,
                'editTasks' => isset($_POST['editTasks']) ? $_POST['editTasks'] : false,
                'deleteTasks' => isset($_POST['deleteTasks']) ? $_POST['deleteTasks'] : false,
                'checkTasks' => isset($_POST['checkTasks']) ? $_POST['checkTasks'] : false,
                // Organs
                'accessOrganSection' => isset($_POST['accessOrganSection']) ? $_POST['accessOrganSection'] : false,
                'addOrgans' => isset($_POST['addOrgans']) ? $_POST['addOrgans'] : false,
                'editOrgans' => isset($_POST['editOrgans']) ? $_POST['editOrgans'] : false,
                'deleteOrgans' => isset($_POST['deleteOrgans']) ? $_POST['deleteOrgans'] : false,
                // Datamanager
                'accessDatamanager' => isset($_POST['accessDatamanager']) ? $_POST['accessDatamanager'] : false,
                'createFolders' => isset($_POST['createFolders']) ? $_POST['createFolders'] : false,
                'deleteFolders' => isset($_POST['deleteFolders']) ? $_POST['deleteFolders'] : false,
                'renameFolders' => isset($_POST['renameFolders']) ? $_POST['renameFolders'] : false,
                'uploadFiles' => isset($_POST['uploadFiles']) ? $_POST['uploadFiles'] : false,
                'approveFiles' => isset($_POST['approveFiles']) ? $_POST['approveFiles'] : false,
                'deleteFiles' => isset($_POST['deleteFiles']) ? $_POST['deleteFiles'] : false,
                'renameFiles' => isset($_POST['renameFiles']) ? $_POST['renameFiles'] : false,
                'folderAccessPerms' => trim($_POST['folderAccessPerms']),
                'globalAccessPerms' => trim($_POST['globalAccess']),
                // Coworkers
                'accessUserSection' => isset($_POST['accessUserSection']) ? $_POST['accessUserSection'] : false,
                'registerUsers' => isset($_POST['registerUsers']) ? $_POST['registerUsers'] : false,
                // Reports
                'accessReportSection' => isset($_POST['accessReportSection']) ? $_POST['accessReportSection'] : false,
                // Qualifications
                'createQualifications' => isset($_POST['createQualifications']) ? $_POST['createQualifications'] : false,
                'accessQualifications' => isset($_POST['accessQualifications']) ? $_POST['accessQualifications'] : false,
                'editQualifications' => isset($_POST['editQualifications']) ? $_POST['editQualifications'] : false,
                'assignQualifications' => isset($_POST['assignQualifications']) ? $_POST['assignQualifications'] : false,
                'deleteQualifications' => isset($_POST['deleteQualifications']) ? $_POST['deleteQualifications'] : false,
                'renameQualifications' => isset($_POST['renameQualifications']) ? $_POST['renameQualifications'] : false,
                // Permissions
                'accessPermissionSection' => isset($_POST['accessPermissionSection']) ? $_POST['accessPermissionSection'] : false,
                'assignRoles' => isset($_POST['assignRoles']) ? $_POST['assignRoles'] : false,
                'addRole' => isset($_POST['addRole']) ? $_POST['addRole'] : false,
                'editRole' => isset($_POST['editRole']) ? $_POST['editRole'] : false,
                // BusinessModel
                'accessBusinessModel' => isset($_POST['accessBusinessModel']) ? $_POST['accessBusinessModel'] : false,
                'editBusinessModel' => isset($_POST['editBusinessModel']) ? $_POST['editBusinessModel'] : false,
                'accessArchivedBusinessModel' => isset($_POST['accessArchivedBusinessModel']) ? $_POST['accessArchivedBusinessModel'] : false,
                // Methods
                'accessMethods' => isset($_POST['accessMethods']) ? $_POST['accessMethods'] : false,
                'editMethods' => isset($_POST['editMethods']) ? $_POST['editMethods'] : false,
                'createMethods' => isset($_POST['createMethods']) ? $_POST['createMethods'] : false,
                /////////////
                // Errors //
                'name_err' => '',
            ];


            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Bitte geben Sie einen Rollennamen ein.';
            }

            /* ######################################################## */
            /* ################## FOLDER ACCESS PERMS ################# */

            $folderAccess;

            // Check if global Access is granted
            if ($data['globalAccessPerms'] == 'true') {
                // GLOBAL ACCESS GRANTED
                $folderAccess = 'global';
            } else {
                // GLOBAL ACCESS DENIED
                // Validate Folder Access Permissions
                if (!empty($data['folderAccessPerms'])) {
                    // Split the string of folderIds into an array
                    $folderPerms = explode(',', $data['folderAccessPerms']);
                    // Sort it numerically
                    sort($folderPerms, SORT_NUMERIC);
                    // Turn it back into a string
                    $data['folderAccessPerms'] = implode(',', $folderPerms);

                    $folderAccess = $data['folderAccessPerms'];
                }
            }

            //die(print_r($folderAccess));

            /* ######################################################## */
            /* ###################### PERMISSIONS ##################### */


            // Init array
            $perm_ids = [];
            // Loop through the data array to get the permission ids
            foreach ($data as $key => $perm) {
                // Skip irrelevant items


                if (
                    $key === 'bodyClass' ||
                    $key === 'privUser' ||
                    $key === 'folders' ||
                    $key === 'name' ||
                    $key === 'description' ||
                    $key === 'name_err' ||
                    $key === 'globalAccessPerms'
                ) {
                    continue;
                }
                // Filter out the empty ones
                if ($perm != false) {

                    // Add the perm id to the array
                    $perm_ids[] = $perm;
                }
            }

            //die( var_dump($data) );

            // Turn the $perm_ids array into a comma seperated string

            $perm_ids_string = implode(',', $perm_ids);

            /* ######################################################## */
            /* ####################### EXECUTION ###################### */

            // Make sure no errors
            if (empty($data['name_err'])) {


                // VALIDATED
                // Create the role
                $roleId = $this->permModel->addRole($data['name'], $data['description'], $folderAccess);
                // Check if the role has been created
                if ($roleId != false) {
                    // SUCCESS
                    if ($this->permModel->addRolePerms($roleId, $perm_ids_string)) {
                        // SUCCESS
                        prepareFlash('perm_message', 'Die Rolle wurde erfolgreich erstellt.');
                        // Helper Function - source: "helpers/url_helper.php"
                        redirect('permissions/show/' . $roleId);
                    } else {
                        die('Ein Fehler ist aufgetreten. Bitte kontaktieren sie den Support.');
                    }
                } else {
                    die('Ein Fehler ist aufgetreten. Bitte kontaktieren sie den Support.');
                }
            } else {
                // Load the view with errors
                $this->view('permissions/add', $data);
            }
        } else {
            // Otherwise load the view, in order to be able to add a post

            // Get the active folders for folder access permissions
            $folders = $this->dataModel->getFolders();

            // Init data
            $data = [
                'bodyClass' => 'permissions permissions-add',
                'privUser' => $this->privUser,
                'folders' => $folders,
                ///////////////////////
                'name' => '',
                'description' => '',
                // Process Permissions
                'createProcessMatrixes' => '',
                'approveProcessMatrixes' => '',
                'editProcessMatrixes' => '',
                'deleteProcessMatrixes' => '',
                // Task Permissions
                'accessTaskSection' => '',
                'addTasks' => '',
                'editTasks' => '',
                'deleteTasks' => '',
                'checkTasks' => '',
                // Organs
                'accessOrganSection' => '',
                'addOrgans' => '',
                'editOrgans' => '',
                'deleteOrgans' => '',
                // Datamanager
                'accessDatamanager' => '',
                'createFolders' => '',
                'deleteFolders' => '',
                'renameFolders' => '',
                'uploadFiles' => '',
                'approveFiles' => '',
                'deleteFiles' => '',
                'renameFiles' => '',
                'folderAccessPerms' => '',
                'globalAccessPerms' => 'false',
                // Coworkers
                'accessUserSection' => '',
                'registerUsers' => '',
                // Reports
                'accessReportSection' => '',
                // Qualifications
                'createQualifications' => '',
                'accessQualifications' => '',
                'editQualifications' => '',
                'assignQualifications' => '',
                'deleteQualifications' => '',
                'renameQualifications' => '',
                // Permissions
                'accessPermissionSection' => '',
                'assignRoles' => '',
                'addRole' => '',
                'editRole' => '',
                // BusinessModel
                'accessBusinessModel' => '',
                'editBusinessModel' => '',
                'accessArchivedBusinessModel' => '',
                // Methods
                'accessMethods' => '',
                'editMethods' => '',
                'createMethods' => '',
                /////////////
                // Errors //
                'name_err' => '',
            ];

            $this->view('permissions/add', $data);
        }
    }

    /************************
     * METHOD: SHOW ROLE
     ************************/

    public function show($id)
    {
        if ($this->privUser->hasPrivilege('14') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Rechtesektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        // Get role
        $role = $this->permModel->getSingleRole($id);
        // Get perms
        $perms = $this->permModel->getPermsByRoleId($id);
        // Init output array
        $perm_ids = [];

        // Loop through the perms
        foreach ($perms as $perm) {
            // Store the perm ids in the output array
            $perm_ids[] = $perm->perm_id;
        }


        /* ##############  FOLDER ACCESS PERMISSIONS  ############## */

        // Make sure there are folder access perm ids
        if ($role->folder_perms == 'global') {
            $accessibleFolders = 'global';
        } elseif (!empty($role->folder_perms) && $role->folder_perms != 'global') {
            // Turn the string of ids into an array
            $folderIds = explode(',', $role->folder_perms);
            // Sort the array
            asort($folderIds);

            // Init output array
            $accessibleFolders = [];

            // Loop through all the folder ids
            foreach ($folderIds as $folderId) {
                // Get each folder as  an object
                $accessibleFolders[] = $this->dataModel->getFolderById($folderId);
            }
        } else {
            // NO ACCESS PERM IDS SET
            // Create an empty array
            $accessibleFolders = [];
        }


        $data = [
            'bodyClass' => 'permissions permissions-show',
            'privUser' => $this->privUser,
            'id' => $id,
            'name' => $role->role_name,
            'description' => $role->role_description,
            // Process Permissions
            'createProcessMatrixes' => in_array(22, $perm_ids) ? true : false,
            'approveProcessMatrixes' => in_array(29, $perm_ids) ? true : false,
            'editProcessMatrixes' => in_array(23, $perm_ids) ? true : false,
            'deleteProcessMatrixes' => in_array(24, $perm_ids) ? true : false,
            // Task Permissions
            'accessTaskSection' => in_array(9, $perm_ids) ? true : false,
            'addTasks' => in_array(1, $perm_ids) ? true : false,
            'editTasks' => in_array(2, $perm_ids) ? true : false,
            'deleteTasks' => in_array(3, $perm_ids) ? true : false,
            'checkTasks' => in_array(15, $perm_ids) ? true : false,
            // Organs
            'accessOrganSection' => in_array(10, $perm_ids) ? true : false,
            'addOrgans' => in_array(6, $perm_ids) ? true : false,
            'editOrgans' => in_array(7, $perm_ids) ? true : false,
            'deleteOrgans' => in_array(8, $perm_ids) ? true : false,
            // Datamanager
            'accessDatamanager' => in_array(13, $perm_ids) ? true : false,
            'createFolders' => in_array(16, $perm_ids) ? true : false,
            'deleteFolders' => in_array(17, $perm_ids) ? true : false,
            'renameFolders' => in_array(36, $perm_ids) ? true : false,
            'uploadFiles' => in_array(18, $perm_ids) ? true : false,
            'approveFiles' => in_array(19, $perm_ids) ? true : false,
            'deleteFiles' => in_array(20, $perm_ids) ? true : false,
            'renameFiles' => in_array(28, $perm_ids) ? true : false,
            // Listings
            'folderAccessPerms' => $accessibleFolders,
            // Coworkers
            'accessUserSection' => in_array(11, $perm_ids) ? true : false,
            'registerUsers' => in_array(21, $perm_ids) ? true : false,
            // Reports
            'accessReportSection' => in_array(12, $perm_ids) ? true : false,
            // Qualifications
            'createQualifications' => in_array(39, $perm_ids) ? true : false,
            'accessQualifications' => in_array(37, $perm_ids) ? true : false,
            'editQualifications' => in_array(38, $perm_ids) ? true : false,
            'assignQualifications' => in_array(40, $perm_ids) ? true : false,
            'deleteQualifications' => in_array(41, $perm_ids) ? true : false,
            'renameQualifications' => in_array(42, $perm_ids) ? true : false,
            // Permissions
            'accessPermissionSection' => in_array(14, $perm_ids) ? true : false,
            'assignRoles' => in_array(25, $perm_ids) ? true : false,
            'addRole' => in_array(26, $perm_ids) ? true : false,
            'editRole' => in_array(27, $perm_ids) ? true : false,
            // BusinessModel
            'accessBusinessModel' => in_array(30, $perm_ids) ? true : false,
            'editBusinessModel' => in_array(31, $perm_ids) ? true : false,
            'accessArchivedBusinessModel' => in_array(32, $perm_ids) ? true : false,
            // Methods
            'accessMethods' => in_array(33, $perm_ids) ? true : false,
            'editMethods' => in_array(34, $perm_ids) ? true : false,
            'createMethods' => in_array(35, $perm_ids) ? true : false,
        ];

        // Load view
        $this->view('permissions/show', $data);
    }

    /**********************
     * METHOD: EDIT ROLE
     **********************/

    public function edit($id)
    {
        if ($this->privUser->hasPrivilege('27') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Rechte zu bearbeiten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        // Check if there is a post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'bodyClass' => 'permissions permissions-add',
                'privUser' => $this->privUser,
                'id' => $id,
                ////////////////////////////////
                'name' => trim($_POST['name']),
                'description' => trim($_POST['description']),
                // Process Permissions
                'createProcessMatrixes' => isset($_POST['createProcessMatrixes']) ? $_POST['createProcessMatrixes'] : false,
                'approveProcessMatrixes' => isset($_POST['approveProcessMatrixes']) ? $_POST['approveProcessMatrixes'] : false,
                'editProcessMatrixes' => isset($_POST['editProcessMatrixes']) ? $_POST['editProcessMatrixes'] : false,
                'deleteProcessMatrixes' => isset($_POST['deleteProcessMatrixes']) ? $_POST['deleteProcessMatrixes'] : false,
                // Task Permissions
                'accessTaskSection' => isset($_POST['accessTaskSection']) ? $_POST['accessTaskSection'] : false,
                'addTasks' => isset($_POST['addTasks']) ? $_POST['addTasks'] : false,
                'editTasks' => isset($_POST['editTasks']) ? $_POST['editTasks'] : false,
                'deleteTasks' => isset($_POST['deleteTasks']) ? $_POST['deleteTasks'] : false,
                'checkTasks' => isset($_POST['checkTasks']) ? $_POST['checkTasks'] : false,
                // Organs
                'accessOrganSection' => isset($_POST['accessOrganSection']) ? $_POST['accessOrganSection'] : false,
                'addOrgans' => isset($_POST['addOrgans']) ? $_POST['addOrgans'] : false,
                'editOrgans' => isset($_POST['editOrgans']) ? $_POST['editOrgans'] : false,
                'deleteOrgans' => isset($_POST['deleteOrgans']) ? $_POST['deleteOrgans'] : false,
                // Datamanager
                'accessDatamanager' => isset($_POST['accessDatamanager']) ? $_POST['accessDatamanager'] : false,
                'createFolders' => isset($_POST['createFolders']) ? $_POST['createFolders'] : false,
                'deleteFolders' => isset($_POST['deleteFolders']) ? $_POST['deleteFolders'] : false,
                'renameFolders' => isset($_POST['renameFolders']) ? $_POST['renameFolders'] : false,
                'uploadFiles' => isset($_POST['uploadFiles']) ? $_POST['uploadFiles'] : false,
                'approveFiles' => isset($_POST['approveFiles']) ? $_POST['approveFiles'] : false,
                'deleteFiles' => isset($_POST['deleteFiles']) ? $_POST['deleteFiles'] : false,
                'renameFiles' => isset($_POST['renameFiles']) ? $_POST['renameFiles'] : false,
                'folderAccessPerms' => trim($_POST['folderAccessPerms']),
                'globalAccessPerms' => trim($_POST['globalAccess']),
                // Coworkers
                'accessUserSection' => isset($_POST['accessUserSection']) ? $_POST['accessUserSection'] : false,
                'registerUsers' => isset($_POST['registerUsers']) ? $_POST['registerUsers'] : false,
                // Reports
                'accessReportSection' => isset($_POST['accessReportSection']) ? $_POST['accessReportSection'] : false,
                // Qualifications
                'createQualifications' => isset($_POST['createQualifications']) ? $_POST['createQualifications'] : false,
                'accessQualifications' => isset($_POST['accessQualifications']) ? $_POST['accessQualifications'] : false,
                'editQualifications' => isset($_POST['editQualifications']) ? $_POST['editQualifications'] : false,
                'assignQualifications' => isset($_POST['assignQualifications']) ? $_POST['assignQualifications'] : false,
                'deleteQualifications' => isset($_POST['deleteQualifications']) ? $_POST['deleteQualifications'] : false,
                'renameQualifications' => isset($_POST['renameQualifications']) ? $_POST['renameQualifications'] : false,
                // Permissions
                'accessPermissionSection' => isset($_POST['accessPermissionSection']) ? $_POST['accessPermissionSection'] : false,
                'assignRoles' => isset($_POST['assignRoles']) ? $_POST['assignRoles'] : false,
                'addRole' => isset($_POST['addRole']) ? $_POST['addRole'] : false,
                'editRole' => isset($_POST['editRole']) ? $_POST['editRole'] : false,
                // BusinessModel
                'accessBusinessModel' => isset($_POST['accessBusinessModel']) ? $_POST['accessBusinessModel'] : false,
                'editBusinessModel' => isset($_POST['editBusinessModel']) ? $_POST['editBusinessModel'] : false,
                'accessArchivedBusinessModel' => isset($_POST['accessArchivedBusinessModel']) ? $_POST['accessArchivedBusinessModel'] : false,
                // Methods
                'accessMethods' => isset($_POST['accessMethods']) ? $_POST['accessMethods'] : false,
                'editMethods' => isset($_POST['editMethods']) ? $_POST['editMethods'] : false,
                'createMethods' => isset($_POST['createMethods']) ? $_POST['createMethods'] : false,
                /////////////
                // Errors //
                'name_err' => '',
            ];

            /* ###################################################### */
            /* ##################### VALIDATION ##################### */

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Bitte geben Sie einen Rollennamen ein.';
            }

            /* ################## FOLDER ACCESS PERMS ################# */

            $folderAccess = 'global';

            // Check if global Access is granted
            if ($data['globalAccessPerms'] == 'true') {
                // GLOBAL ACCESS GRANTED
                $folderAccess = 'global';
            } else {
                // GLOBAL ACCESS DENIED
                // Validate Folder Access Permissions
                if (!empty($data['folderAccessPerms'])) {
                    // Split the string of folderIds into an array
                    $folderPerms = explode(',', $data['folderAccessPerms']);
                    // Sort it numerically
                    sort($folderPerms, SORT_NUMERIC);
                    // Turn it back into a string
                    $data['folderAccessPerms'] = implode(',', $folderPerms);

                    $folderAccess = $data['folderAccessPerms'];
                }
            }

            //die(print_r($folderAccess));

            /* ###################### PERMISSIONS ##################### */

            // Init array for storage
            $perm_ids = [];
            // Loop through the data array to get the permission ids
            foreach ($data as $key => $perm) {
                // Skip irrelevant items
                if (
                    $key === 'bodyClass' ||
                    $key === 'privUser' ||
                    $key === 'folders' ||
                    $key === 'name' ||
                    $key === 'description' ||
                    $key === 'name_err' ||
                    $key === 'globalAccessPerms'
                ) {
                    continue;
                }

                // Filter out the empty ones
                if ($perm != false) {
                    // Add the perm id to the array
                    $perm_ids[] = $perm;
                }
            }

            // Turn the $perm_ids array into a comma seperated string
            $perm_ids_string = implode(',', $perm_ids);


            //die(print_r($perm_ids_string));

            /* ##################################################### */
            /* ##################### EXECUTION ##################### */

            // Make sure no errors
            if (empty($data['name_err'])) {
                // VALIDATED
                // Update the role and check for success
                if ($this->permModel->updateRole($id, $data['name'], $data['description'], $folderAccess)) {
                    // SUCCESSFUL
                    // Update the Role's Permissions
                    if ($this->permModel->updateRolePerms($id, $perm_ids_string)) {
                        // SUCCESS
                        prepareFlash('perm_message', 'Die Rolle wurde erfolgreich aktualisiert.');
                        // Helper Function - source: "helpers/url_helper.php"
                        redirect('permissions/show/' . $id);
                    } else {
                        // FAILURE
                        prepareFlash('perm_message', 'Die Berechtigungen der Rolle konnte nicht erfolgreich aktualisiert werden. Bitte versuchen Sie es erneut oder kontaktieren den Support', 'alert alert-danger');
                        // Helper Function - source: "helpers/url_helper.php"
                        redirect('permissions/show/' . $id);
                    }
                } else {
                    // UNSUCCESSFUL
                    prepareFlash('perm_message', 'Die Rolle konnte nicht erfolgreich aktualisiert werden. Bitte versuchen Sie es erneut oder kontaktieren den Support', 'alert alert-danger');
                    // Helper Function - source: "helpers/url_helper.php"
                    redirect('permissions/show/' . $id);
                }
            } else {
                // INVALID
                // Load the view with errors
                $this->view('permissions/edit', $data);
            }
        } else {
            // Otherwise load the view, in order to be able to add a post

            // Get role
            $role = $this->permModel->getSingleRole($id);
            // Get perms
            $perms = $this->permModel->getPermsByRoleId($id);
            // Init output array
            $perm_ids = [];

            // Loop through the perms
            foreach ($perms as $perm) {
                // Store the perm ids in the output array
                $perm_ids[] = $perm->perm_id;
            }

            /* ##################################################### */
            /* ############### FOLDER ACCESS PERMS ################# */

            $folderAccessPerms = $role->folder_perms;

            // Init a global access variable
            $hasGlobalAccess = 'false';
            // Init output array for the accessible folders
            $accessibleFolders = [];

            // Check if global access is granted
            if ($role->folder_perms == 'global' && !empty($role->folder_perms)) {
                // GLOBAL ACCESS
                $hasGlobalAccess = 'true';
                $folderAccessPerms = '';
            } else {
                // RESTRICTED ACCESS
                // Turn the string of ids into an array
                $folderIds = explode(',', $role->folder_perms);
                // Sort the array
                asort($folderIds);

                // Loop through all the folder ids
                foreach ($folderIds as $folderId) {
                    // Get each folder as  an object
                    $accessibleFolders[] = $this->dataModel->getFolderById($folderId);
                }
            }

            // Get the active folders for folder access permissions
            $folders = $this->dataModel->getFolders();

            // Init data
            $data = [
                'bodyClass' => 'permissions permissions-add',
                'privUser' => $this->privUser,
                'folders' => $folders,
                'id' => $id,
                ///////////////////////
                'name' => $role->role_name,
                'description' => $role->role_description,
                // Process Permissions
                'createProcessMatrixes' => in_array(22, $perm_ids) ? true : false,
                'approveProcessMatrixes' => in_array(29, $perm_ids) ? true : false,
                'editProcessMatrixes' => in_array(23, $perm_ids) ? true : false,
                'deleteProcessMatrixes' => in_array(24, $perm_ids) ? true : false,
                // Task Permissions
                'accessTaskSection' => in_array(9, $perm_ids) ? true : false,
                'addTasks' => in_array(1, $perm_ids) ? true : false,
                'editTasks' => in_array(2, $perm_ids) ? true : false,
                'deleteTasks' => in_array(3, $perm_ids) ? true : false,
                'checkTasks' => in_array(15, $perm_ids) ? true : false,
                // Organs
                'accessOrganSection' => in_array(10, $perm_ids) ? true : false,
                'addOrgans' => in_array(6, $perm_ids) ? true : false,
                'editOrgans' => in_array(7, $perm_ids) ? true : false,
                'deleteOrgans' => in_array(8, $perm_ids) ? true : false,
                // Datamanager
                'accessDatamanager' => in_array(13, $perm_ids) ? true : false,
                'createFolders' => in_array(16, $perm_ids) ? true : false,
                'deleteFolders' => in_array(17, $perm_ids) ? true : false,
                'renameFolders' => in_array(36, $perm_ids) ? true : false,
                'uploadFiles' => in_array(18, $perm_ids) ? true : false,
                'approveFiles' => in_array(19, $perm_ids) ? true : false,
                'deleteFiles' => in_array(20, $perm_ids) ? true : false,
                'renameFiles' => in_array(28, $perm_ids) ? true : false,
                // Listings
                'folderAccessPerms' => $folderAccessPerms,
                'accessibleFolders' => $accessibleFolders,
                'globalAccessPerms' => $hasGlobalAccess,
                // Coworkers
                'accessUserSection' => in_array(11, $perm_ids) ? true : false,
                'registerUsers' => in_array(21, $perm_ids) ? true : false,
                // Reports
                'accessReportSection' => in_array(12, $perm_ids) ? true : false,
                // Qualifications
                'createQualifications' => in_array(39, $perm_ids) ? true : false,
                'accessQualifications' => in_array(37, $perm_ids) ? true : false,
                'editQualifications' => in_array(38, $perm_ids) ? true : false,
                'assignQualifications' => in_array(40, $perm_ids) ? true : false,
                'deleteQualifications' => in_array(41, $perm_ids) ? true : false,
                'renameQualifications' => in_array(42, $perm_ids) ? true : false,
                // Permissions
                'accessPermissionSection' => in_array(14, $perm_ids) ? true : false,
                'assignRoles' => in_array(25, $perm_ids) ? true : false,
                'addRole' => in_array(26, $perm_ids) ? true : false,
                'editRole' => in_array(27, $perm_ids) ? true : false,
                // BusinessModel
                'accessBusinessModel' => in_array(30, $perm_ids) ? true : false,
                'editBusinessModel' => in_array(31, $perm_ids) ? true : false,
                'accessArchivedBusinessModel' => in_array(32, $perm_ids) ? true : false,
                // Methods
                'accessMethods' => in_array(33, $perm_ids) ? true : false,
                'editMethods' => in_array(34, $perm_ids) ? true : false,
                'createMethods' => in_array(35, $perm_ids) ? true : false,
                /////////////
                // Errors //
                'name_err' => '',
            ];

            //die( var_dump($accessibleFolders) );

            $this->view('permissions/edit', $data);
        }
    }

    /***********************************
     * METHOD: ASSIGN ROLES TO USERS
     ***********************************/

    public function assign()
    {

        // Check if there is a post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // Validate Privilege
            if ($this->privUser->hasPrivilege('25') != true) {
                prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Rechte-Rollen zu vergeben', 'alert alert-danger');
                redirect('users/dashboard');
            }

            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [
                'bodyClass' => 'permissions permissions-assign',
                'privUser' => $this->privUser
            ];

            $err_log = [];

            // Loop through the $_POST array
            foreach ($_POST as $key => $role_ids) {
                // Make sure only the user inputs are targeted
                if ($key != 'submit') {

                    // Filter out the user id from the $key
                    $user_id = substr($key, 5);

                    //die(print_r($role_ids));

                    // Check if there is an error
                    if ($this->permModel->updateRolesOfUser($user_id, $role_ids) != true) {
                        // Error -> log the error
                        $err_log[] = "Funktionszuteilungsfehler - User: $key und Rollen: $role_ids";
                    }
                }
            }

            // Check if the error log is empty
            if (empty($err_log)) {
                // NO ERRORS

                // Prepare the flash messages and redirect to assign view
                prepareFlash('assign_message', 'Die Nutzerrechte wurden erfolgreich aktualisiert.');
                // Helper Function - source: "helpers/url_helper.php"
                redirect('permissions/assign');
            } else {
                // ERRORS

                // Prepare a special flash
                prepareFlash('assign_message', 'Bei der Vergabe der Nutzerrechte ist ein Fehler aufgetreten.');
                // Helper Function - source: "helpers/url_helper.php"
                redirect('permissions/assign');
            }
        } else {
            // Otherwise load the view, in order to be assign the roles


            /* ############### GET ORGANS ############### */
            $organs = $this->organModel->getOrgansWithoutUser();

            foreach ($organs as $organ) {
                $management_level = explode(',', $organ->management_level);
                $staff = explode(',', $organ->staff);

                $merged = array_merge($management_level, $staff);
                $organ->users = array_unique($merged);
            }


            // Get all users with their assigned roles
            $users = $this->userModel->getUsersWithRoles();

            foreach ($users as $user) {
                if (!empty($user->role_ids)) {
                    // Inti output object
                    $role_objs = [];
                    // Turn the string of role_ids to an array
                    $role_ids = explode(',', $user->role_ids);
                    foreach ($role_ids as $role) {
                        // Get each role object and add it to the output array
                        $role_objs[] = $this->permModel->getSingleRole($role)->role_name;
                    }
                    $user->roles = $role_objs;
                    $user->role_names = implode(', ', $role_objs);
                }
            }

            // Get all roles for modal
            $roles = $this->permModel->getRoles();


            $data = [
                'bodyClass' => 'permissions permissions-assign',
                'privUser' => $this->privUser,
                'users' => $users,
                'roles' => $roles,
                'organs' => $organs
            ];

            // Load view
            $this->view('permissions/assign', $data);
        }
    }

    /* ########################################### */
    /* ########### METHOD: DELETE ROLE ########### */
    /* ########################################### */

    //public function delete($id){

    /////////////////////////////////////
    /////// Validate Privileges /////////
    /////////////////////////////////////
    /*
    if($this->privUser->hasPrivilege('9') != true) {
      prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Prozesse zu erstellen.', 'alert alert-danger');
      redirect('users/dashboard');
    }
    */
    //////////////////////////////////////
    ///////////////  ORGANS  /////////////
    //////////////////////////////////////
}
