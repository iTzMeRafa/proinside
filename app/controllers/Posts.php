<?php
  class Posts extends Controller
  {
      public function __construct()
      {
          if (!isLoggedIn()) {
              redirect('users/login');
          }

          $this->postModel = $this->model('Post');
          $this->userModel = $this->model('User');
      }

      public function index()
      {
          // Get posts
          $posts = $this->postModel->getPosts();

          $data = [
        'posts' => $posts
      ];

          $this->view('posts/index', $data);
      }

      /**********************
          METHOD: ADD POST
       **********************/

      public function add()
      {
          // Check if there is a post request
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {
              // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              $data = [
          'title' => trim($_POST['title']),
          'body' => trim($_POST['body']),
          'user_id' => $_SESSION['user_id'],
          'title_err' => '',
          'body_err' => ''
        ];

              // Validate data
              if (empty($data['title'])) {
                  $data['title_err'] = 'Please enter title';
              }
              if (empty($data['body'])) {
                  $data['body_err'] = 'Please enter body text';
              }

              // Make sure no errors
              if (empty($data['title_err']) && empty($data['body_err'])) {
                  // Validated
                  if ($this->postModel->addPost($data)) {
                      prepareFlash('post_message', 'The post was added');
                      redirect('posts');
                  } else {
                      die('Something went wrong');
                  }
              } else {
                  // Load the view with errors
                  $this->view('posts/add', $data);
              }
          } else {
              // Otherwise load the view, in order to be able to add a post
              // Init data
              $data = [
          'title' => '',
          'body' => ''
        ];

              $this->view('posts/add', $data);
          }
      }

      /**********************
          METHOD: EDIT POST
       **********************/

      public function edit($id)
      {
          // Check if there is a post request
          // So this block only runs if the Post Owner in the else codeblock is already validated
          // Otherwise a post request would not be possible to make
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {
              // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              $data = [
          'id' => $id,
          'title' => trim($_POST['title']),
          'body' => trim($_POST['body']),
          'user_id' => $_SESSION['user_id'],
          'title_err' => '',
          'body_err' => ''
        ];

              // Validate data
              if (empty($data['title'])) {
                  $data['title_err'] = 'Please enter title';
              }
              if (empty($data['body'])) {
                  $data['body_err'] = 'Please enter body text';
              }

              // Make sure no errors
              if (empty($data['title_err']) && empty($data['body_err'])) {
                  // Validated
                  if ($this->postModel->updatePost($data)) {
                      prepareFlash('post_message', 'The post was updated');
                      redirect('posts');
                  } else {
                      die('Something went wrong');
                  }
              } else {
                  // Load the view with errors
                  $this->view('posts/edit', $data);
              }
          } else {
              // Otherwise load the view, in order for the user to be able to edit a post
              // Get existing post from model
              $post = $this->postModel->getPostById($id);

              // Check for owner
              if ($post->user_id != $_SESSION['user_id']) {
                  redirect('posts');
              }

              $data = [
          'id' => $id,
          'title' => $post->title,
          'body' => $post->body
        ];


              $this->view('posts/edit', $data);
          }
      }

      public function show($id)
      {
          // $id is coming from the URL and is passed to the postModel
          $post = $this->postModel->getPostById($id);
          $user = $this->userModel->getUserById($post->user_id);

          $data = [
        'id' => $post->id,
        'title' => $post->title,
        'body' => $post->body,
        'created_at' => $post->created_at,
        'user_id' => $post->user_id,
        'user_name' => $user->name
      ];

          $this->view('posts/show', $data);
      }

      public function delete($id)
      {
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        // Get existing post from model
              $post = $this->postModel->getPostById($id);

              // Check for owner
              if ($post->user_id != $_SESSION['user_id']) {
                  redirect('posts');
              }

              if ($this->postModel->deletePost($id)) {
                  prepareFlash('post_message', 'The Post was deleted');
                  redirect('posts');
              } else {
                  die('Something went wrong');
              }
          } else {
              redirect('posts');
          }
      }
  }
