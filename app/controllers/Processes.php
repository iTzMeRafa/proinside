<?php
  class Processes extends Controller
  {
      public function __construct()
      {
          // Make sure user is logged in
          if (!isLoggedIn()) {
              redirect('users/login');
          }

          // Get the current user's permissions
          $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

          // Load the task model
          $this->taskModel = $this->model('Task');
          // Load the user model
          $this->userModel = $this->model('User');
          // Load the data model
          $this->dataModel = $this->model('Data');
          // Load the organ model
          $this->organModel = $this->model('Organ');
          // Load the process model
          $this->processModel = $this->model('Process');
      }

      /**********************
          METHOD: ADD
       **********************/
      public function add($id = 0)
      {

      /////////////////////////////////////
          /////// Validate Privileges /////////
          /////////////////////////////////////

          if ($this->privUser->hasPrivilege('9') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Prozesse zu erstellen.', 'alert alert-danger');
              redirect('users/dashboard');
          }

          //////////////////////////////////////
          ///////////////  ORGANS  /////////////
          //////////////////////////////////////

      
          // Get all the organs
          $organs = $this->organModel->getOrgansWithoutUser();

          // Init arrays for output
          $organArray = [];
          $user_count = 0;
      
          // Loop through all the organs
          foreach ($organs as $organ) {
              $managers = $organ->management_level;
              $staff = $organ->staff;

              $managerArr = explode(',', $managers);
              $staffArr = explode(',', $staff);

              if (!empty($managerArr) && !empty($staffArr)) {
                  // BOTH FILLED
                  // Merge arrays
                  $userIds = array_merge($managerArr, $staffArr);
                  // Filter values
                  $userIds = array_unique($userIds);
              } elseif (!empty($managerArr) && empty($staffArr)) {
                  // ONLY MANAGERS
                  // Set userIds to $managerArr
                  $userIds = $managerArr;
              } elseif (empty($managerArr) && !empty($staffArr)) {
                  // ONLY STAFF
                  // Set userIds to $staffArr
                  $userIds = $staffArr;
              } else {
                  // BOTH EMPTY
                  $userIds = '';
              }

              $users = [];

              foreach ($userIds as $userId) {
                  if (is_Object($this->userModel->getUserById($userId))) {
                      $users[] = $this->userModel->getUserById($userId);
                      $user_count = $user_count+1;
                  }
              }

              $organArray[] = array(
          'name' => $organ->name,
          'organ_id' => $organ->organId,
          'abbreviation' => $organ->abbreviation,
          'users' => $users
        );
          }

          //////////////////////////////////////
          //////////////  FOLDERS  /////////////
          //////////////////////////////////////
      
          // Get the root folders
          $folders = $this->dataModel->getFolderByParentId(0);
      

          foreach ($folders as $key => $folder) {
              // Filter out the archive
              if ($folder->folder_name == 'Archiv') {
                  unset($folders[$key]);
              } else {
                  // Get sub directories if existing
                  if ($this->dataModel->getFolderByParentId($folder->id)) {
                      $folder->sub_directories = $this->dataModel->getFolderByParentId($folder->id);
                  }
              }
          }

          //die( var_dump($folders) );


          //////////////////////////////////////
          ///////////  POST REQUEST  ///////////
          //////////////////////////////////////

          // Check if there is a post request
          // So this block only runs if the permission in the else codeblock is already validated
          // Otherwise a post request would not be possible to make
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {
              // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              // Create array for the legend values
              $legend = [];
              // Create array to store the task sets in
              $task_sets = [];
              // Init count variable
              $count = 1;

              $err_sets = [];

              // Loop through the $_POST array
              foreach ($_POST as $key => $field) {

          ////////////////////////////////////
                  //  Filter out the legend values  //
                  ////////////////////////////////////
                  if (substr($key, 0, 6) == 'legend') {

            // Get the position of this special char to get the user id in the $key
                      $pos = strpos($key, '_');

                      // Store the values in the $legend array
                      $legend[] = array(
              'user_id' => substr($key, $pos+1),
              'organ_id' => $field,
            );
                      continue;
                  }
                  ////////////////////////////////////////////////////////
                  // Skip the fields that are not part of the task_sets //
                  ////////////////////////////////////////////////////////
                  elseif ($key == 'process_name' || $key == 'target_folder') {
                      continue;
                  }

                  // Get the position of the first dash
                  $pos = strpos($key, '-');
                  // Extract the number of the task from the $key via $pos
                  $number = substr($key, 0, $pos);
                  // Extract the field name from the $key via $pos
                  $fieldName = substr($key, $pos+1);

                  // Check if the $count and the current $number match
                  if ($number == $count) {
                      // MATCHING
                      // If no array with the current $number exists
                      if (!array_key_exists($number, $task_sets)) {
                          // Create it inside $task_sets
                          $task_sets[$number] = array(
                'roles_responsible' => array(),
                'roles_concerned' => array(),
                'roles_contributing' => array(),
                'roles_information' => array(),
              );
                          // Create it inside $err_sets as well
                          $err_sets[$number] = array();
                      }

                      // Store all the corresponding fields in $err_sets for err outputs
                      $err_sets[$number][$fieldName] = $field;

                      // Check if the field is a roles field
                      if (substr($fieldName, 0, 11) == 'user-select') {
                          // VALIDATED

                          // Get the position of the last dash in $fieldName
                          $lastDash = strrpos($fieldName, '-');
                          // Extract the user number from $fieldName
                          $userNumber = substr($fieldName, $lastDash+1);
                          // Get the user id from the legend array via the user number
                          $userId = $legend[$userNumber-1]['user_id'];
                          // Get the organ id from the legend array via the user number
                          $organId = $legend[$userNumber-1]['organ_id'];

                          // Check which role is the array value of the current iteration
                          // Then add it to the respective array within $tasksets
                          if ($field == 'responsible') {
                              $task_sets[$number]['roles_responsible'][] = $userId.'_'.$organId;
                          } elseif (($field == 'concerned')) {
                              $task_sets[$number]['roles_concerned'][] = $userId.'_'.$organId;
                          } elseif (($field == 'contributing')) {
                              $task_sets[$number]['roles_contributing'][] = $userId.'_'.$organId;
                          } elseif (($field == 'information')) {
                              $task_sets[$number]['roles_information'][] = $userId.'_'.$organId;
                          }
                          // Skip the rest of the current iteration and move on to the next
                          continue;
                      }

            

                      // Store all the corresponding fields in that array
                      $task_sets[$number][$fieldName] = $field;
                  } else {
                      // NOT MATCHING
                      // Augment the count by one
                      $count = $count + 1;

                      // If no array with the current $number exists
                      if (!array_key_exists($number, $task_sets)) {
                          // Create it
                          $task_sets[$number] = array(
                'roles_responsible' => array(),
                'roles_concerned' => array(),
                'roles_contributing' => array(),
                'roles_information' => array(),
              );
                          // Create it inside $err_sets as well
                          $err_sets[$number] = array();
                      }

                      // Store all the corresponding fields in $err_sets for err outputs
                      $err_sets[$number][$fieldName] = $field;

                      // Check if the field is a roles field
                      if (substr($fieldName, 0, 11) == 'user-select') {
                          // VALIDATED

                          // Get the position of the last dash in $fieldName
                          $lastDash = strrpos($fieldName, '-');
                          // Extract the user number from $fieldName
                          $userNumber = substr($fieldName, $lastDash+1);
                          // Get the user id from the legend array via the user number
                          $userId = $legend[$userNumber-1]['user_id'];
                          // Get the organ id from the legend array via the user number
                          $organId = $legend[$userNumber-1]['organ_id'];

                          // Check which role is the array value of the current iteration
                          // Then add it to the respective array within $tasksets
                          if ($field == 'responsible') {
                              $task_sets[$number]['roles_responsible'][] = $userId;
                          } elseif (($field == 'concerned')) {
                              $task_sets[$number]['roles_concerned'][] = $userId;
                          } elseif (($field == 'contributing')) {
                              $task_sets[$number]['roles_contributing'][] = $userId;
                          } elseif (($field == 'information')) {
                              $task_sets[$number]['roles_information'][] = $userId;
                          }
                          // Skip the rest of the current iteration and move on to the next
                          continue;
                      }

                      // Store all corresponding fields within that array
                      $task_sets[$number][$fieldName] = $field;
                  }
              }

              //die( var_dump($task_sets) );

              /* ######################################################## */
              /* ############## SUBMIT DATA TO DATABASE ################# */

              if (!empty($_POST['process_name']) &&
           !empty($_POST['target_folder'])) {

          // Init array to store ids of the created tasks
                  $createdTaskIds = [];

                  // Create the individual user tasks

                  foreach ($task_sets as $task_set) {

            // Remove duplicate ids from roles arrays
                      $cleaned_responsible = array_unique($task_set['roles_responsible']);
                      $cleaned_concerned = array_unique($task_set['roles_concerned']);
                      $cleaned_contributing = array_unique($task_set['roles_contributing']);
                      $cleaned_information =array_unique($task_set['roles_information']);

                      //die(var_dump($task_set));


                      /* ################## CHECK INTERVALS ################### */

                      if (!empty(trim($task_set['ref-date'])) &&
                !empty(trim($task_set['int-count'])) &&
                !empty(trim($task_set['int-format'])) &&
                !empty(trim($task_set['remind-before'])) &&
                !empty(trim($task_set['remind-after'])) &&
                !empty(trim($task_set['remind-at']))) {
                          $refDate = trim($task_set['ref-date']);
                          $intCount = trim($task_set['int-count']);
                          $intFormat = trim($task_set['int-format']);
                          // Construct check_intervals field
                          $check_intervals = "$intCount $intFormat";

              
                          // Calculate the next checkdate
                          $timestamp = strtotime($refDate);
                          $checkdate = calculateCheckdate($timestamp, $intCount, $intFormat);

                          /* ################## REMINDERS ################### */
              
                          // Make sure remind-before value is not empty or zero
                          if (!empty(trim($task_set['remind-before'])) || trim($task_set['remind-before']) != 0) {
                              $remindBefore = trim($task_set['remind-before']);
                          } else {
                              // EMPTY
                              $remindBefore = null;
                          }

                          // Make sure remind-after value is not empty or zero
                          if (!empty(trim($task_set['remind-after'])) || trim($task_set['remind-after']) != 0) {
                              $remindAfter = trim($task_set['remind-after']);
                          } else {
                              // EMPTY
                              $remindAfter = null;
                          }

                          // Make sure remind-at value is not false or zero
                          if (trim($task_set['remind-at']) != false || trim($task_set['remind-after']) != 0) {
                              $remindAt = 'activated';
                          } else {
                              // EMPTY
                              $remindAt = null;
                          }
                      } else {
                          $refDate = null;
                          $intCount = null;
                          $check_intervals = '';

                          $intFormat = null;
                          $remindBefore = null;
                          $remindAfter = null;
                          $remindAt = null;
                      }


                      $taskData = [
              'process_id' => null,
              'user_id' => $_SESSION['user_id'],
              'description' => null,
              'department' => null,
              'input' => trim($task_set['input']),
              'input_source' => trim($task_set['input_source']),
              'category' => trim($task_set['category']),
              'name' => trim($task_set['name']),
              'output' => trim($task_set['output']),
              'output_target' => trim($task_set['output_target']),
              'risk' => trim($task_set['risk']),
              'status' => trim($task_set['status']),
              'required_knowledge' => null,
              'required_knowledge_folders' => '',
              'required_knowledge' => trim($task_set['required_knowledge']),
              // Roles
              'roles_responsible' => implode(',', $cleaned_responsible),
              'roles_concerned' => implode(',', $cleaned_concerned),
              'roles_contributing' => implode(',', $cleaned_contributing),
              'roles_information' => implode(',', $cleaned_information),
              //
              'iso_value' => trim($task_set['iso_value']),
              'check_intervals' => $check_intervals,
              'reference_date' => $refDate,
              'interval_count' => $intCount,
              'interval_format' => $intFormat,
              'check_date' => $checkdate['year'] .'-'. $checkdate['month'] .'-'. $checkdate['day'],
              'remind_before' => $remindBefore,
              'remind_at' => $remindAt,
              'remind_after' => $remindAfter,
              'required_resources' => null,
              'required_resources_folders' => '',
              'existing_compentencies' => null,
              'required_education' => null,
              'tags' => trim($task_set['tags']),
              'is_approved' => 'false',
              'is_singletask' => 'false'
            ];

                      // Create the task and get its ID
                      $taskId = $this->taskModel->addTask($taskData);

                      if ($taskId != false) {
                          $createdTaskIds[] = $taskId;
                      }
                  }
          

                  // Create the process

                  $processData = [
            'process_name' => $_POST['process_name'],
            'folder_id' => $_POST['target_folder'],
            'task_ids' => implode(',', $createdTaskIds)
          ];

                  $processId = $this->processModel->createProcess($processData);

                  if ($processId != false) {
                      // PROCESS CREATED

                      foreach ($createdTaskIds as $taskId) {
                          // connect the process_id to the tasks in the DB
                          $this->taskModel->addProcessIdToTask($processId, $taskId);
                      }

                      // Prepare the flash messages and redirect to the process show view
                      prepareFlash('process_message', 'Der Prozess wurde erstellt.');
                      redirect('processes/show/'.$processId);
                  }
              } else {
                  $data = [
            'bodyClass' => 'processes processes-add',
            'privUser' => $this->privUser,
            'organs' => $organArray,
            'user_count' => $user_count,
            'target_id' => $_POST['target_folder'],
            'folders' => $folders,
            'tasks' => $err_sets,
            'error' => true
          ];


                  // Load the view with Errors
                  $this->view('processes/add', $data);
              }
          } else {
              $data = [
          'bodyClass' => 'processes processes-add',
          'privUser' => $this->privUser,
          'organs' => $organArray,
          'user_count' => $user_count,
          'target_id' => $id,
          'folders' => $folders,
          'tasks' => ''
        ];

              // Load view
              $this->view('processes/add', $data);
          }
      }


      /* #################################################### */
      /* ################# METHOD: SHOW ##################### */
      /* #################################################### */

      public function show($id)
      {

      /* ########################################## */
          /* ############ GET THE PROCESS ############# */

          $process = $this->processModel->getProcessById($id);
      
          $target_folder = $this->dataModel->getFolderById($process->folder_id);

          /* ############################################## */
          /* ############# VALIDATE PRIVILEGE ############# */

          // Check if the process is awaiting approval
          if ($process->is_approved == 'false') {
              // UNAPPROVED
              // Check if the current user has the privilege to approve it
              if ($this->privUser->hasPrivilege('29') != true) {
                  // NO PRIVILEGE
                  // Redirect him and show an error message
                  prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Prozesse freizugeben.', 'alert alert-danger');
                  redirect('users/dashboard');
              }
          }

          /* ################################# */
          /* ############ ORGANS ############# */
      
          // Get all the organs
          $organs = $this->organModel->getOrgansWithoutUser();

          // Init arrays for output
          $organArray = [];
          $user_count = 0;
          $activeOrganIds = [];

          $organRoleArray = [];
      
          // Loop through all the organs
          foreach ($organs as $organ) {
              $managers = $organ->management_level;
              $staff = $organ->staff;

              $managerArr = explode(',', $managers);
              $staffArr = explode(',', $staff);

              if (!empty($managerArr) && !empty($staffArr)) {
                  // BOTH FILLED
                  // Merge arrays
                  $userIds = array_merge($managerArr, $staffArr);
                  // Filter values
                  $userIds = array_unique($userIds);
              } elseif (!empty($managerArr) && empty($staffArr)) {
                  // ONLY MANAGERS
                  // Set userIds to $managerArr
                  $userIds = $managerArr;
              } elseif (empty($managerArr) && !empty($staffArr)) {
                  // ONLY STAFF
                  // Set userIds to $staffArr
                  $userIds = $staffArr;
              } else {
                  // BOTH EMPTY
                  $userIds = '';
              }

              // Create arrays for the loop
              $users = [];
              $organRoleKeys = [];

              foreach ($userIds as $userId) {
                  // Check if user exists
                  if (is_Object($this->userModel->getUserById($userId))) {
                      // Get the user object from DB
                      $users[] = $this->userModel->getUserById($userId);
                      // Increase the user_count
                      $user_count = $user_count+1;

                      $organRoleKeys[] = $userId.'_'.isset($organ->organId) && !empty($organ->organId) ? $organ->organID : null;
                  }
              }

              $organRoleArray =  array_merge($organRoleArray, $organRoleKeys);

              $organArray[] = array(
          'name' => $organ->name,
          'organ_id' => isset($organ->organId) && !empty($organ->organId) ? $organ->organID : null,
          'users' => $users
        );

              $activeOrganIds[] = isset($organ->organId) && !empty($organ->organId) ? $organ->organID : null;
          }

          /* ############################### */
          /* ############ TASKS ############ */
      

          $taskIds = explode(',', $process->task_ids);

          $tasks = [];

          foreach ($taskIds as $key => $taskId) {
              // Get the task
              $task = $this->taskModel->getProcessTaskById($taskId);

        
        
              $roles_ordered = [];

              $r_responsible = explode(',', $task->roles_responsible);
              $r_concerned = explode(',', $task->roles_concerned);
              $r_contributing = explode(',', $task->roles_contributing);
              $r_information = explode(',', $task->roles_information);

              $ordered_roles = $this->orderRoles($r_responsible, $r_concerned, $r_contributing, $r_information, $organRoleArray);


              $task->roles_ordered = $ordered_roles;

              $tasks[] = $task;
          }

          /* ################################## */
          /* ########## OUTPUT DATA ########### */

          $data = [
        'bodyClass' => 'processes processes-show',
        'privUser' => $this->privUser,
        'id' => $process->id,
        'process_name' => $process->process_name,
        'target_folder' => $target_folder->folder_name,
        'folder_id' => $process->folder_id,
        'is_approved' => $process->is_approved,
        'is_archived' => $process->is_archived,
        'tasks' => $tasks,
        'task_ids' => $process->task_ids,
        'organs' => $organArray
      ];

          //die( var_dump($data['tasks']) );

          // Load view
          $this->view('processes/show', $data);
      }

      /* ####################################################### */
      /* ################# METHOD: APPROVE ##################### */
      /* ####################################################### */

      public function approve($id)
      {

      /* ###################################### */
          /* ######## Validate Privileges ######### */

          if ($this->privUser->hasPrivilege('29') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Prozesse freizugeben.', 'alert alert-danger');
              redirect('users/dashboard');
          }
      
          /* ########################################## */
          /* ############ GET THE PROCESS ############# */

          $process = $this->processModel->getProcessById($id);

          if ($process->is_approved != 'false' || $process->is_archived != 'false') {
              prepareFlash('no_permisssion', 'Dieser Prozess ist entweder schon aktiv oder bereits archiviert und kann deshalb nicht mehr freigegeben werden.', 'alert alert-danger');
              redirect('users/dashboard');
          }

          /* ############################### */
          /* ############ TASKS ############ */
      
          // Get the taskIds as an array
          $taskIds = explode(',', $process->task_ids);

          $tasksErrLog = [];

          // Loop through all taskIds
          foreach ($taskIds as $key => $taskId) {
              // Get the current task
              $task = $this->taskModel->getProcessTaskById($taskId);

              // Explode all the roles
              $r_responsible = explode(',', $task->roles_responsible);
              $r_concerned = explode(',', $task->roles_concerned);
              $r_contributing = explode(',', $task->roles_contributing);
              $r_information = explode(',', $task->roles_information);

              if (!empty($task->roles_responsible)) {
                  $r_responsible = explode(',', $task->roles_responsible);
                  $r_responsible = $this->assignRolesArray($r_responsible, 'responsible', $taskId);
              }
              if (!empty($task->roles_concerned)) {
                  $r_concerned = explode(',', $task->roles_concerned);
                  $r_concerned = $this->assignRolesArray($r_concerned, 'concerned', $taskId);
              }
              if (!empty($task->roles_contributing)) {
                  $r_contributing = explode(',', $task->roles_contributing);
                  $r_contributing = $this->assignRolesArray($r_contributing, 'contributing', $taskId);
              }
              if (!empty($task->roles_information)) {
                  $r_information = explode(',', $task->roles_information);
                  $r_information = $this->assignRolesArray($r_information, 'information', $taskId);
              }

              // Check if all role assignments were successful
              if ($r_responsible == true && $r_concerned == true && $r_contributing == true && $r_information == true) {
                  // ALL USERS SUCCESSFULLY ASSIGNED
                  // Approve the task and check if successful
                  if ($this->taskModel->approveTask($taskId) == false) {
                      // ERROR
                      // Return an appropriate error message
                      return $tasksErrLog[] = "Die Aufgabe A:$taskId wurde nicht erfolgreich freigegeben";
                  }
              }
          }

          // Check if all tasks were successfully approved
          if (empty($tasksErrLog)) {
              // SUCCESS
              // Approve the whole process
              $this->processModel->approveProcess($id);
              prepareFlash('process_message', 'Der Prozess wurde erfolgreich freigegeben.');
              redirect('processes/show/'.$id);
          } else {
              // ERROR
              prepareFlash('process_message', 'Bei der Freigabe des Prozesses ist ein Fehler aufgetreten. Es wurden nicht alle Nutzer der Aufgabe korrekt zugeteilt.', 'alert alert-danger');
              redirect('processes/show/'.$id);
          }
      }

    

      private function assignRolesArray($rolesArray, $role, $taskId)
      {

        // ROLES ARRAY FILLED
        
          $log = [];
        
          // Loop through the whole roles array
          foreach ($rolesArray as $key => $item) {
              // Extract the user id and the organ id
              $userAndOrgan = explode('_', $item);

              // Prepare data for model
              $roleData = [
            'task_id' => $taskId,
            'user_id' => $userAndOrgan[0],
            'role' => $role,
            'organ_id' => $userAndOrgan[1]
          ];

              // Add the unaccepted assignment via the model and log errors if the occur
              if ($this->taskModel->addUnacceptedAssignment($roleData) == false) {
                  // ERROR
                  // log it
                  $log[] = "Die Nutzer ID:$userAndOrgan[0] mit der Rolle $role und dem OrganID:$userAndOrgan[1]  konnte der Aufgabe A:$taskId nicht zugeteilt werden.";
              }
          }

          // Check if all role assignments were successful
          if (empty($log)) {
              // SUCCESSFUL
              return true;
          } else {
              // ERRORS FOUND
              return $log;
          }
      }

    





      /* #################################################### */
      /* ################# METHOD: EDIT ##################### */
      /* #################################################### */

      public function edit($id)
      {

      /* ###################################### */
          /* ######## Validate Privileges ######### */

          if ($this->privUser->hasPrivilege('23') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Prozesse zu bearbeiten.', 'alert alert-danger');
              redirect('users/dashboard');
          }

          /* ########################################## */
          /* ############ GET THE PROCESS ############# */

          $process = $this->processModel->getProcessById($id);

          if ($process->is_approved != 'false' || $process->is_archived != 'false') {
              prepareFlash('no_permisssion', 'Dieser Prozess ist entweder schon aktiv oder bereits archiviert und kann deshalb nicht mehr bearbeitet werden.', 'alert alert-danger');
              redirect('processes/show/'. $id);
          }


          /* ########################################## */
          /* ################# FOLDERS ################ */
      
          // Get the root folders
          $folders = $this->dataModel->getFolderByParentId(0);
      

          foreach ($folders as $key => $folder) {
              // Filter out the archive
              if ($folder->folder_name == 'Archiv') {
                  unset($folders[$key]);
              } else {
                  // Get sub directories if existing
                  if ($this->dataModel->getFolderByParentId($folder->id)) {
                      $folder->sub_directories = $this->dataModel->getFolderByParentId($folder->id);
                  }
              }
          }

          /* ################################# */
          /* ############ ORGANS ############# */
      
          // Get all the organs
          $organs = $this->organModel->getOrgans();

          // Init arrays for output
          $organArray = [];
          $user_count = 0;
          $activeOrganIds = [];

          $organRoleArray = [];
      
          // Loop through all the organs
          foreach ($organs as $organ) {
              $managers = $organ->management_level;
              $staff = $organ->staff;

              $managerArr = explode(',', $managers);
              $staffArr = explode(',', $staff);

              if (!empty($managerArr) && !empty($staffArr)) {
                  // BOTH FILLED
                  // Merge arrays
                  $userIds = array_merge($managerArr, $staffArr);
                  // Filter values
                  $userIds = array_unique($userIds);
              } elseif (!empty($managerArr) && empty($staffArr)) {
                  // ONLY MANAGERS
                  // Set userIds to $managerArr
                  $userIds = $managerArr;
              } elseif (empty($managerArr) && !empty($staffArr)) {
                  // ONLY STAFF
                  // Set userIds to $staffArr
                  $userIds = $staffArr;
              } else {
                  // BOTH EMPTY
                  $userIds = '';
              }


              // Create arrays for the loop
              $users = [];
              $organRoleKeys = [];

              foreach ($userIds as $userId) {
                  // Check if user exists
                  if (is_Object($this->userModel->getUserById($userId))) {
                      // Get the user object from DB
                      $users[] = $this->userModel->getUserById($userId);
                      // Increase the user_count
                      $user_count = $user_count+1;

                      $organRoleKeys[] = $userId.'_'.$organ->organId;
                  }
              }

              $organRoleArray =  array_merge($organRoleArray, $organRoleKeys);

              $organArray[] = array(
          'name' => $organ->name,
          'organ_id' => $organ->organId,
          'users' => $users
        );

              $activeOrganIds[] = $organ->organId;
          }

          /* ############################### */
          /* ############ TASKS ############ */
      

          $taskIds = explode(',', $process->task_ids);

          $tasks = [];

          foreach ($taskIds as $key => $taskId) {
              // Get the task
              $task = $this->taskModel->getProcessTaskById($taskId);
        
              $roles_ordered = [];

              $r_responsible = explode(',', $task->roles_responsible);
              $r_concerned = explode(',', $task->roles_concerned);
              $r_contributing = explode(',', $task->roles_contributing);
              $r_information = explode(',', $task->roles_information);

              $ordered_roles = $this->orderRoles($r_responsible, $r_concerned, $r_contributing, $r_information, $organRoleArray);


              $task->roles_ordered = $ordered_roles;

              $tasks[] = $task;
          }

          /* ###################################### */
          /* ###################################### */
          /* ############ POST REQUEST ############ */

          // Check if there is a post request
          // So this block only runs if the permission in the else codeblock is already validated
          // Otherwise a post request would not be possible to make
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {
              // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              // Create array for the legend values
              $legend = [];
              // Create array to store the task sets in
              $task_sets = [];
              // Init count variable
              $count = 1;

              $err_sets = [];

              // Loop through the $_POST array
              foreach ($_POST as $key => $field) {

          ////////////////////////////////////
                  //  Filter out the legend values  //
                  ////////////////////////////////////
                  if (substr($key, 0, 6) == 'legend') {

            // Get the position of this special char to get the user id in the $key
                      $pos = strpos($key, '_');

                      // Store the values in the $legend array
                      $legend[] = array(
              'user_id' => substr($key, $pos+1),
              'organ_id' => $field,
            );
                      continue;
                  }
                  ////////////////////////////////////////////////////////
                  // Skip the fields that are not part of the task_sets //
                  ////////////////////////////////////////////////////////
                  elseif ($key == 'process_name' || $key == 'target_folder') {
                      continue;
                  }

                  // Get the position of the first dash
                  $pos = strpos($key, '-');
                  // Extract the number of the task from the $key via $pos
                  $number = substr($key, 0, $pos);
                  // Extract the field name from the $key via $pos
                  $fieldName = substr($key, $pos+1);

                  // Check if the $count and the current $number match
                  if ($number == $count) {
                      // MATCHING
                      // If no array with the current $number exists
                      if (!array_key_exists($number, $task_sets)) {
                          // Create it inside $task_sets
                          $task_sets[$number] = array(
                'roles_responsible' => array(),
                'roles_concerned' => array(),
                'roles_contributing' => array(),
                'roles_information' => array(),
              );
                          // Create it inside $err_sets as well
                          $err_sets[$number] = array();
                      }

                      // Store all the corresponding fields in $err_sets for err outputs
                      $err_sets[$number][$fieldName] = $field;

                      // Check if the field is a roles field
                      if (substr($fieldName, 0, 11) == 'user-select') {
                          // VALIDATED

                          // Get the position of the last dash in $fieldName
                          $lastDash = strrpos($fieldName, '-');
                          // Extract the user number from $fieldName
                          $userNumber = substr($fieldName, $lastDash+1);
                          // Get the user id from the legend array via the user number
                          $userId = $legend[$userNumber-1]['user_id'];
                          // Get the organ id from the legend array via the user number
                          $organId = $legend[$userNumber-1]['organ_id'];

                          // Check which role is the array value of the current iteration
                          // Then add it to the respective array within $tasksets
                          if ($field == 'responsible') {
                              $task_sets[$number]['roles_responsible'][] = $userId.'_'.$organId;
                          } elseif (($field == 'concerned')) {
                              $task_sets[$number]['roles_concerned'][] = $userId.'_'.$organId;
                          } elseif (($field == 'contributing')) {
                              $task_sets[$number]['roles_contributing'][] = $userId.'_'.$organId;
                          } elseif (($field == 'information')) {
                              $task_sets[$number]['roles_information'][] = $userId.'_'.$organId;
                          }
                          // Skip the rest of the current iteration and move on to the next
                          continue;
                      }

            

                      // Store all the corresponding fields in that array
                      $task_sets[$number][$fieldName] = $field;
                  } else {
                      // NOT MATCHING
                      // Augment the count by one
                      $count = $count + 1;

                      // If no array with the current $number exists
                      if (!array_key_exists($number, $task_sets)) {
                          // Create it
                          $task_sets[$number] = array(
                'roles_responsible' => array(),
                'roles_concerned' => array(),
                'roles_contributing' => array(),
                'roles_information' => array(),
              );
                          // Create it inside $err_sets as well
                          $err_sets[$number] = array();
                      }

                      // Store all the corresponding fields in $err_sets for err outputs
                      $err_sets[$number][$fieldName] = $field;

                      // Check if the field is a roles field
                      if (substr($fieldName, 0, 11) == 'user-select') {
                          // VALIDATED

                          // Get the position of the last dash in $fieldName
                          $lastDash = strrpos($fieldName, '-');
                          // Extract the user number from $fieldName
                          $userNumber = substr($fieldName, $lastDash+1);
                          // Get the user id from the legend array via the user number
                          $userId = $legend[$userNumber-1]['user_id'];
                          // Get the organ id from the legend array via the user number
                          $organId = $legend[$userNumber-1]['organ_id'];

                          // Check which role is the array value of the current iteration
                          // Then add it to the respective array within $tasksets
                          if ($field == 'responsible') {
                              $task_sets[$number]['roles_responsible'][] = $userId;
                          } elseif (($field == 'concerned')) {
                              $task_sets[$number]['roles_concerned'][] = $userId;
                          } elseif (($field == 'contributing')) {
                              $task_sets[$number]['roles_contributing'][] = $userId;
                          } elseif (($field == 'information')) {
                              $task_sets[$number]['roles_information'][] = $userId;
                          }
                          // Skip the rest of the current iteration and move on to the next
                          continue;
                      }

                      // Store all corresponding fields within that array
                      $task_sets[$number][$fieldName] = $field;
                  }
              }

              //die(var_dump($task_sets));

              /* ########################################################## */
              /* ################ SUBMIT DATA TO DATABASE ################# */

              if (!empty($_POST['process_name']) &&
           !empty($_POST['target_folder'])) {

          // Init array to store ids of the connected tasks
                  $connectedTaskIds = [];
                  // Create array to store all remaining preexisting tasks in
                  $remainingTasks = [];

                  // Create the individual user tasks

                  foreach ($task_sets as $task_set) {

            // Remove duplicate ids from roles arrays
                      $cleaned_responsible = array_unique($task_set['roles_responsible']);
                      $cleaned_concerned = array_unique($task_set['roles_concerned']);
                      $cleaned_contributing = array_unique($task_set['roles_contributing']);
                      $cleaned_information =array_unique($task_set['roles_information']);

                      $taskData = [
              'user_id' => $_SESSION['user_id'],
              'description' => null,
              'department' => null,
              'input' => trim($task_set['input']),
              'input_source' => trim($task_set['input_source']),
              'category' => trim($task_set['category']),
              'name' => trim($task_set['name']),
              'output' => trim($task_set['output']),
              'output_target' => trim($task_set['output_target']),
              'risk' => trim($task_set['risk']),
              'status' => trim($task_set['status']),
              'required_knowledge' => null,
              'required_knowledge_folders' => '',
              'required_knowledge' => trim($task_set['required_knowledge']),
              // Roles
              'roles_responsible' => implode(',', $cleaned_responsible),
              'roles_concerned' => implode(',', $cleaned_concerned),
              'roles_contributing' => implode(',', $cleaned_contributing),
              'roles_information' => implode(',', $cleaned_information),
              //
              'iso_value' => trim($task_set['iso_value']),
              'check_intervals' => '',
              'reference_date' => null,
              'interval_count' => null,
              'interval_format' => null,
              'check_date' => null,
              'remind_before' => null,
              'remind_at' => null,
              'remind_after' => null,
              'required_resources' => null,
              'required_resources_folders' => '',
              'existing_compentencies' => null,
              'required_education' => null,
              'tags' => null,
              'is_singletask' => 'false',
              'is_approved' => 'false'
            ];

                      if ($task_set['existing_id']) {
                          // TASK EXISTS ALREADY


                          // Add its ID to the remainingTasks array
                          $remainingTasks[] = $task_set['existing_id'];

                          // Add its id to the data array
                          $taskData['id'] = $task_set['existing_id'];
                          // Update it
                          $this->taskModel->updateTask($taskData);
                          // Add it to the connectedTasksIds Array
                          $connectedTaskIds[] = $task_set['existing_id'];
                      } else {
                          // TASK DOES NOT EXIST YET

                          // Create it
                          $taskId = $this->taskModel->addTask($taskData);

                          if ($taskId != false) {
                              $connectedTaskIds[] = $taskId;
                          }
                      }
                  }


                  // Compare the exisiting task ids with the remaining task ids
                  foreach ($taskIds as $task_id) {
                      if (!in_array($task_id, $remainingTasks)) {
                          // NOT IN THE ARRAY
              // DELETE ?? ARCHIVE???
                      }
                  }


                  // Update the process

                  $processData = [
            'id' => $id,
            'process_name' => $_POST['process_name'],
            'folder_id' => $_POST['target_folder'],
            'task_ids' => implode(',', $connectedTaskIds)
          ];

                  if ($this->processModel->updateProcess($processData)) {
                      // PROCESS UPDATED SUCCESSFULLY

                      foreach ($connectedTaskIds as $taskId) {
                          // connect the process_id to the tasks in the DB
                          $this->taskModel->addProcessIdToTask($id, $taskId);
                      }

                      // Prepare the flash messages and redirect to the process show view
                      prepareFlash('process_message', 'Der Prozess wurde erfolgreich aktualisiert.');
                      redirect('processes/show/'.$id);
                  }
              } else {
                  $data = [
            'bodyClass' => 'processes processes-add',
            'privUser' => $this->privUser,
            'organs' => $organArray,
            'user_count' => $user_count,
            'target_id' => $_POST['target_folder'],
            'folders' => $folders,
            'tasks' => $err_sets,
            'error' => true
          ];


                  // Load the view with Errors
                  $this->view('processes/add', $data);
              }
          } else {
              // Load the view in order to edit the existing process


              $data = [
          'bodyClass' => 'processes processes-edit',
          'privUser' => $this->privUser,
          'id' => $id,
          'process' => $process,
          'target_id' => $process->folder_id,
          'folders' => $folders,
          'organs' => $organArray,
          'user_count' => $user_count,
          'tasks' => $tasks,
        ];

              // Load view
              $this->view('processes/edit', $data);
          }
      }

      /* ####################################################### */
      /* ################# METHOD: RECYCLE ##################### */
      /* ####################################################### */

      public function recycle($id)
      {

      /* ###################################### */
          /* ######## Validate Privileges ######### */

          if ($this->privUser->hasPrivilege('23') != true) {
              prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Prozesse zu bearbeiten.', 'alert alert-danger');
              redirect('users/dashboard');
          }

          /* ########################################## */
          /* ############ GET THE PROCESS ############# */

          $process = $this->processModel->getProcessById($id);

          /* ########################################## */
          /* ################# FOLDERS ################ */
      
          // Get the root folders
          $folders = $this->dataModel->getFolderByParentId(0);
      

          foreach ($folders as $key => $folder) {
              // Filter out the archive
              if ($folder->folder_name == 'Archiv') {
                  unset($folders[$key]);
              } else {
                  // Get sub directories if existing
                  if ($this->dataModel->getFolderByParentId($folder->id)) {
                      $folder->sub_directories = $this->dataModel->getFolderByParentId($folder->id);
                  }
              }
          }

          /* ################################# */
          /* ############ ORGANS ############# */
      
          // Get all the organs
          $organs = $this->organModel->getOrgans();

          // Init arrays for output
          $organArray = [];
          $user_count = 0;
          $activeOrganIds = [];

          $organRoleArray = [];
      
          // Loop through all the organs
          foreach ($organs as $organ) {
              $managers = $organ->management_level;
              $staff = $organ->staff;

              $managerArr = explode(',', $managers);
              $staffArr = explode(',', $staff);

              if (!empty($managerArr) && !empty($staffArr)) {
                  // BOTH FILLED
                  // Merge arrays
                  $userIds = array_merge($managerArr, $staffArr);
                  // Filter values
                  $userIds = array_unique($userIds);
              } elseif (!empty($managerArr) && empty($staffArr)) {
                  // ONLY MANAGERS
                  // Set userIds to $managerArr
                  $userIds = $managerArr;
              } elseif (empty($managerArr) && !empty($staffArr)) {
                  // ONLY STAFF
                  // Set userIds to $staffArr
                  $userIds = $staffArr;
              } else {
                  // BOTH EMPTY
                  $userIds = '';
              }


              // Create arrays for the loop
              $users = [];
              $organRoleKeys = [];

              foreach ($userIds as $userId) {
                  // Check if user exists
                  if (is_Object($this->userModel->getUserById($userId))) {
                      // Get the user object from DB
                      $users[] = $this->userModel->getUserById($userId);
                      // Increase the user_count
                      $user_count = $user_count+1;

                      $organRoleKeys[] = $userId.'_'.$organ->organId;
                  }
              }

              $organRoleArray =  array_merge($organRoleArray, $organRoleKeys);

              $organArray[] = array(
          'name' => $organ->name,
          'organ_id' => $organ->organId,
          'users' => $users
        );

              $activeOrganIds[] = $organ->organId;
          }

          /* ############################### */
          /* ############ TASKS ############ */
      

          $taskIds = explode(',', $process->task_ids);

          $tasks = [];

          foreach ($taskIds as $key => $taskId) {
              // Get the task
              $task = $this->taskModel->getProcessTaskById($taskId);
              //die(var_dump($taskIds));
        
              $roles_ordered = [];

              $r_responsible = explode(',', $task->roles_responsible);
              $r_concerned = explode(',', $task->roles_concerned);
              $r_contributing = explode(',', $task->roles_contributing);
              $r_information = explode(',', $task->roles_information);

              $ordered_roles = $this->orderRoles($r_responsible, $r_concerned, $r_contributing, $r_information, $organRoleArray);


              $task->roles_ordered = $ordered_roles;

              $tasks[] = $task;
          }

          /* ###################################### */
          /* ###################################### */
          /* ############ POST REQUEST ############ */

          // Check if there is a post request
          // So this block only runs if the permission in the else codeblock is already validated
          // Otherwise a post request would not be possible to make
          if ($_SERVER['REQUEST_METHOD'] == 'POST') {
              // Sanitize POST array
              $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

              // Create array for the legend values
              $legend = [];
              // Create array to store the task sets in
              $task_sets = [];
              // Init count variable
              $count = 1;

              $err_sets = [];

              // Loop through the $_POST array
              foreach ($_POST as $key => $field) {

          ////////////////////////////////////
                  //  Filter out the legend values  //
                  ////////////////////////////////////
                  if (substr($key, 0, 6) == 'legend') {

            // Get the position of this special char to get the user id in the $key
                      $pos = strpos($key, '_');

                      // Store the values in the $legend array
                      $legend[] = array(
              'user_id' => substr($key, $pos+1),
              'organ_id' => $field,
            );
                      continue;
                  }
                  ////////////////////////////////////////////////////////
                  // Skip the fields that are not part of the task_sets //
                  ////////////////////////////////////////////////////////
                  elseif ($key == 'process_name' || $key == 'target_folder') {
                      continue;
                  }

                  // Get the position of the first dash
                  $pos = strpos($key, '-');
                  // Extract the number of the task from the $key via $pos
                  $number = substr($key, 0, $pos);
                  // Extract the field name from the $key via $pos
                  $fieldName = substr($key, $pos+1);

                  // Check if the $count and the current $number match
                  if ($number == $count) {
                      // MATCHING
                      // If no array with the current $number exists
                      if (!array_key_exists($number, $task_sets)) {
                          // Create it inside $task_sets
                          $task_sets[$number] = array(
                'roles_responsible' => array(),
                'roles_concerned' => array(),
                'roles_contributing' => array(),
                'roles_information' => array(),
              );
                          // Create it inside $err_sets as well
                          $err_sets[$number] = array();
                      }

                      // Store all the corresponding fields in $err_sets for err outputs
                      $err_sets[$number][$fieldName] = $field;

                      // Check if the field is a roles field
                      if (substr($fieldName, 0, 11) == 'user-select') {
                          // VALIDATED

                          // Get the position of the last dash in $fieldName
                          $lastDash = strrpos($fieldName, '-');
                          // Extract the user number from $fieldName
                          $userNumber = substr($fieldName, $lastDash+1);
                          // Get the user id from the legend array via the user number
                          $userId = $legend[$userNumber-1]['user_id'];
                          // Get the organ id from the legend array via the user number
                          $organId = $legend[$userNumber-1]['organ_id'];

                          // Check which role is the array value of the current iteration
                          // Then add it to the respective array within $tasksets
                          if ($field == 'responsible') {
                              $task_sets[$number]['roles_responsible'][] = $userId.'_'.$organId;
                          } elseif (($field == 'concerned')) {
                              $task_sets[$number]['roles_concerned'][] = $userId.'_'.$organId;
                          } elseif (($field == 'contributing')) {
                              $task_sets[$number]['roles_contributing'][] = $userId.'_'.$organId;
                          } elseif (($field == 'information')) {
                              $task_sets[$number]['roles_information'][] = $userId.'_'.$organId;
                          }
                          // Skip the rest of the current iteration and move on to the next
                          continue;
                      }

            

                      // Store all the corresponding fields in that array
                      $task_sets[$number][$fieldName] = $field;
                  } else {
                      // NOT MATCHING
                      // Augment the count by one
                      $count = $count + 1;

                      // If no array with the current $number exists
                      if (!array_key_exists($number, $task_sets)) {
                          // Create it
                          $task_sets[$number] = array(
                'roles_responsible' => array(),
                'roles_concerned' => array(),
                'roles_contributing' => array(),
                'roles_information' => array(),
              );
                          // Create it inside $err_sets as well
                          $err_sets[$number] = array();
                      }

                      // Store all the corresponding fields in $err_sets for err outputs
                      $err_sets[$number][$fieldName] = $field;

                      // Check if the field is a roles field
                      if (substr($fieldName, 0, 11) == 'user-select') {
                          // VALIDATED

                          // Get the position of the last dash in $fieldName
                          $lastDash = strrpos($fieldName, '-');
                          // Extract the user number from $fieldName
                          $userNumber = substr($fieldName, $lastDash+1);
                          // Get the user id from the legend array via the user number
                          $userId = $legend[$userNumber-1]['user_id'];
                          // Get the organ id from the legend array via the user number
                          $organId = $legend[$userNumber-1]['organ_id'];

                          // Check which role is the array value of the current iteration
                          // Then add it to the respective array within $tasksets
                          if ($field == 'responsible') {
                              $task_sets[$number]['roles_responsible'][] = $userId;
                          } elseif (($field == 'concerned')) {
                              $task_sets[$number]['roles_concerned'][] = $userId;
                          } elseif (($field == 'contributing')) {
                              $task_sets[$number]['roles_contributing'][] = $userId;
                          } elseif (($field == 'information')) {
                              $task_sets[$number]['roles_information'][] = $userId;
                          }
                          // Skip the rest of the current iteration and move on to the next
                          continue;
                      }

                      // Store all corresponding fields within that array
                      $task_sets[$number][$fieldName] = $field;
                  }
              }

              //die(var_dump($task_sets));

              /* ########################################################## */
              /* ################ SUBMIT DATA TO DATABASE ################# */

              if (!empty($_POST['process_name']) &&
           !empty($_POST['target_folder'])) {

          // Init array to store ids of the created tasks
                  $createdTaskIds = [];

                  // Create the individual user tasks

                  foreach ($task_sets as $task_set) {

            // Remove duplicate ids from roles arrays
                      $cleaned_responsible = array_unique($task_set['roles_responsible']);
                      $cleaned_concerned = array_unique($task_set['roles_concerned']);
                      $cleaned_contributing = array_unique($task_set['roles_contributing']);
                      $cleaned_information =array_unique($task_set['roles_information']);

                      $taskData = [
              'user_id' => $_SESSION['user_id'],
              'description' => null,
              'department' => null,
              'input' => trim($task_set['input']),
              'input_source' => trim($task_set['input_source']),
              'category' => trim($task_set['category']),
              'name' => trim($task_set['name']),
              'output' => trim($task_set['output']),
              'output_target' => trim($task_set['output_target']),
              'risk' => trim($task_set['risk']),
              'status' => trim($task_set['status']),
              'required_knowledge' => null,
              'required_knowledge_folders' => '',
              'required_knowledge' => trim($task_set['required_knowledge']),
              // Roles
              'roles_responsible' => implode(',', $cleaned_responsible),
              'roles_concerned' => implode(',', $cleaned_concerned),
              'roles_contributing' => implode(',', $cleaned_contributing),
              'roles_information' => implode(',', $cleaned_information),
              //
              'iso_value' => trim($task_set['iso_value']),
              'check_intervals' => '',
              'reference_date' => null,
              'interval_count' => null,
              'interval_format' => null,
              'check_date' => null,
              'remind_before' => null,
              'remind_at' => null,
              'remind_after' => null,
              'required_resources' => null,
              'required_resources_folders' => '',
              'existing_compentencies' => null,
              'required_education' => null,
              'tags' => null,
              'is_singletask' => 'false',
              'is_approved' => 'false'
            ];

                      // Create the task and get its ID
                      $taskId = $this->taskModel->addTask($taskData);

                      if ($taskId != false) {
                          $createdTaskIds[] = $taskId;
                      }
                  }

                  // Create the process

                  $processData = [
            'process_name' => $_POST['process_name'],
            'folder_id' => $_POST['target_folder'],
            'task_ids' => implode(',', $createdTaskIds)
          ];

                  $processId = $this->processModel->createProcess($processData);

                  if ($processId != false) {
                      // PROCESS CREATED
            
                      foreach ($createdTaskIds as $taskId) {
                          // connect the process_id to the tasks in the DB
                          $this->taskModel->addProcessIdToTask($processId, $taskId);
                      }

                      // Prepare the flash messages and redirect to the process show view
                      prepareFlash('process_message', 'Der Prozess wurde erfolgreich erstellt.');
                      redirect('processes/show/'.$processId);
                  }
              } else {
                  $data = [
            'bodyClass' => 'processes processes-add',
            'privUser' => $this->privUser,
            'id' => $id,
            'organs' => $organArray,
            'folders' => $folders,
            'user_count' => $user_count,
            'target_id' => $_POST['target_folder'],
            'process_name' => $_POST['process_name'],
            'tasks' => $err_sets,
            'user_count' => $user_count,
            'error' => true
           ];

                  //die(var_dump($_POST['target_folder']));

                  // Load the view with Errors
                  $this->view('processes/recycle', $data);
              }
          } else {
              // Load the view in order to edit the existing process

              $data = [
          'bodyClass' => 'processes processes-recycle',
          'privUser' => $this->privUser,
          'id' => $id,
          'process' => $process,
          'target_id' => $process->folder_id,
          'folders' => $folders,
          'organs' => $organArray,
          'tasks' => $tasks
        ];

              // Load view
              $this->view('processes/recycle', $data);
          }
      }


      /* ##################################################################### */
      /* ##################################################################### */
      /* ########################## FUNCTIONS ################################ */
      /* ##################################################################### */
      /* ##################################################################### */


      public function orderRoles($roleIdArray_1, $roleIdArray_2, $roleIdArray_3, $roleIdArray_4, $organRoleArray)
      {
          $arraysOfIds = [
        'responsible' => $roleIdArray_1,
        'concerned' => $roleIdArray_2,
        'contributing' => $roleIdArray_3,
        'information' => $roleIdArray_4
      ];

          $result = [];

          foreach ($organRoleArray as $idRoleKey) {
              if (in_array($idRoleKey, $roleIdArray_1)) {
                  $result[$idRoleKey] = 'responsible';
              } elseif (in_array($idRoleKey, $roleIdArray_2)) {
                  $result[$idRoleKey] = 'concerned';
              } elseif (in_array($idRoleKey, $roleIdArray_3)) {
                  $result[$idRoleKey] = 'contributing';
              } elseif (in_array($idRoleKey, $roleIdArray_4)) {
                  $result[$idRoleKey] = 'information';
              } else {
                  $result[$idRoleKey] = '';
              }
          }
      
          return $result;
      }
  }
