<?php

class Qualifications extends Controller
{
    private $privUser;
    private $helperController;
    private $qualificationModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->qualificationModel = $this->model('QualificationModel');

        if ($this->privUser->hasPrivilege('37') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Kompetenz-Sektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /**
     * Loads the index page
     * @return {view}
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'qualifications',
            'privUser' => $this->privUser,
            'organs' => $this->helperController->getFullUserListSortedInOrgans(),
            'qualifications' => $this->qualificationModel->getAllQualifications(),
        ];

        $this->view('qualifications/index', $data);
    }

    public function show($id)
    {
        $data = [
            'bodyClass' => 'qualifications',
            'privUser' => $this->privUser,
            'qualification' => $this->qualificationModel->getQualificationById($id),
            'userList' => $this->qualificationModel->getUsersWithQualificationById($id),
        ];

        $this->view('qualifications/show', $data);
    }
}
