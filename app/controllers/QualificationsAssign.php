<?php

class QualificationsAssign extends Controller
{
    private $privUser;
    private $helperController;
    private $qualificationModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->qualificationModel = $this->model('QualificationModel');

        if ($this->privUser->hasPrivilege('40') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Mitarbeitern Kompetenzen zuzuweisen', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /**
     * Deletes a qualification by given id and redirects to the index page
     * @return {redirect}
     */
    public function index()
    {
        if (!isset($_POST['assignQualificationSubmit'])) {
            return redirect('users/login');
        }

        $qualificationId = $_POST['qualificationId'] ?? '';
        $qualificationUserList = $_POST['qualificationUserList'] ?? '';
        $userListArr = removeOrgansFromUserArrayList($qualificationUserList);

        foreach ($userListArr as $userId) {
            $this->qualificationModel->assignUserQualification($qualificationId, $userId)
                ? prepareFlash('assignQualificationState', "Die Kompetenz wurde erfolgreich allen ausgewählten Mitarbeitern zu gewiesen")
                : prepareFlash('assignQualificationState', 'Ein Fehler ist aufgetreten. Versuchen Sie es erneut.', 'alert alert-danger');
        }

        redirect('Qualifications');
    }
}
