<?php

class QualificationsCreate extends Controller
{
    private $privUser;
    private $helperController;
    private $qualificationModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->qualificationModel = $this->model('QualificationModel');

        if ($this->privUser->hasPrivilege('39') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Kompetenzen zu erstellen', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /**
     * Loads the index page
     * @return {view}
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'qualifications',
            'privUser' => $this->privUser,
        ];

        $this->view('qualifications/create', $data);
    }

    /**
     * Creates a new qualification and fetches/handles the data
     * @return {redirect}
     */
    public function post()
    {
        if (!isset($_POST['qualificationCreate'])) {
            return redirect('users/login');
        }


        $qualificationName          = $_POST['qualificationName'] ?? '';
        $qualificationExpireTime    = (int)$_POST['qualificationExpireTime'] ?? '';

        if ($this->validatePost($qualificationName, $qualificationExpireTime)) {
            $this->qualificationModel->storeQualification($qualificationName, $qualificationExpireTime)
                ? prepareFlash('createQualificationState', "Die Kompetenz <strong>{$qualificationName}</strong> wurde erfolgreich erstellt!")
                : prepareFlash('createQualificationState', 'Ein Fehler ist aufgetreten. Versuchen Sie es erneut.', 'alert alert-danger');

            redirect('QualificationsCreate');
        }
    }

    /**
     * Validates the data to create a new qualification
     * @param $qualiName
     * @param $qualiExpireTime
     * @return bool
     */
    public function validatePost($qualiName, $qualiExpireTime)
    {
        return is_string($qualiName) && is_int($qualiExpireTime);
    }
}
