<?php

class QualificationsDelete extends Controller
{
    private $privUser;
    private $helperController;
    private $qualificationModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->qualificationModel = $this->model('QualificationModel');

        if ($this->privUser->hasPrivilege('41') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Kompetenzen zu löschen', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /**
     * Deletes a qualification by given id and redirects to the index page
     * @return {redirect}
     */
    public function index($qualificationId)
    {
        $this->qualificationModel->removeQualification($qualificationId)
            ? prepareFlash('deleteQualificationState', "Die Kompetenz wurde erfolgreich gelöscht!")
            : prepareFlash('deleteQualificationState', 'Ein Fehler ist aufgetreten. Versuchen Sie es erneut.', 'alert alert-danger');

        redirect('Qualifications');
    }
}
