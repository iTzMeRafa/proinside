<?php

class QualificationsRename extends Controller
{
    private $privUser;
    private $helperController;
    private $qualificationModel;

    public function __construct()
    {
        if (!isLoggedIn()) {
            redirect('users/dashboard');
        }

        // Load Models
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        $this->helperController = $this->newController('HelperController');
        $this->qualificationModel = $this->model('QualificationModel');

        if ($this->privUser->hasPrivilege('42') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Kompetenzen umzubenennen', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /**
     * Deletes a qualification by given id and redirects to the index page
     * @return {redirect}
     */
    public function index()
    {
        if (!isset($_POST['renameQualificationSubmit'])) {
            return redirect('users/login');
        }

        $qualificationId = $_POST['qualificationId'] ?? '';
        $qualificationName = $_POST['renamedQualification'] ?? '';

        $this->qualificationModel->renameQualification($qualificationId, $qualificationName)
            ? prepareFlash('renameQualificationState', "Die Kompetenz wurde erfolgreich umbenannt!")
            : prepareFlash('renameQualificationState', 'Ein Fehler ist aufgetreten. Versuchen Sie es erneut.', 'alert alert-danger');

        redirect('Qualifications');
    }
}
