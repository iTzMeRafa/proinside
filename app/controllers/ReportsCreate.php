<?php
class ReportsCreate extends Controller
{
    public function __construct()
    {
        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Get the current user's permissions
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        // Load the report model
        $this->reportModel = $this->model('Report');
        // Load the task model
        $this->taskModel = $this->model('Task');
        // Load the user model
        $this->userModel = $this->model('User');
    }

    /**********************
    METHOD: INDEX
     **********************/
    public function post()
    {

        if (!isset($_POST['reportCreate'])) {
            return redirect('users/login');
        }

        return var_dump($_POST);

        $data = [
            'bodyClass' => 'reports',
            'privUser' => $this->privUser,
        ];

        // Load the view
        $this->view('reports/index', $data);
    }
}
