<?php


class Search extends Controller
{
    protected $privUser;
    protected $searchModel;

    public function __construct()
    {
        /* authentication check */
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Load the report model
        $this->searchModel = $this->model('SearchModel');

        /* Get current Users privilege */
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
    }

    /***
     * Gets called on index page load, manages data and returns the view
     * @return view
     */
    public function index()
    {
        $data = [
            'bodyClass' => 'searchmodels',
            'privUser' => $this->privUser,
        ];

        // Load view
        $this->view('search/index', $data);
    }

    public function fetch($query)
    {
        $searchData = $this->getSearchData($query);

        $data = [
            'bodyClass' => 'searchmodels',
            'privUser' => $this->privUser,
            'query' => $query,
            'searchData' => $searchData,
        ];

        // Load view
        $this->view('search/index', $data);
    }

    private function getSearchData($query)
    {
        $tasks      = null;
        $methods    = null;
        $documents  = null;

        // Get Tasks
        $this->privUser->hasPrivilege('9') != true
            ? prepareFlash('no_permisssionTask', 'Sie sind nicht dazu berechtigt die Aufgaben zu sehen', 'alert alert-danger')
            : $tasks = $this->searchModel->getTasksByTags($query);

        // Get Methods
        $this->privUser->hasPrivilege('33') != true
            ? prepareFlash('no_permisssionMethod', 'Sie sind nicht dazu berechtigt die Maßnahmen zu sehen', 'alert alert-danger')
            : $methods = $this->searchModel->getMethodsByTags($query);

        // Get Documents
        $this->privUser->hasPrivilege('13') != true
            ? prepareFlash('no_permisssionDocuments', 'Sie sind nicht dazu berechtigt die Dokumente zu sehen', 'alert alert-danger')
            : $documents = $this->searchModel->getDocumentsByTags($query);

        if ($methods !== null) {
            foreach ($methods as $key => $method) {
                $methods[$key]->startDate_msg = calculateNextCheckdate($method->reference_date);
            }
        }

        return [
            'tasks' => $tasks,
            'methods' => $methods,
            'documents' => $documents,
            'countResults' => count($tasks) + count($methods) + count($documents),
        ];
    }
}
