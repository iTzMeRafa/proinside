<?php

class Tasks extends Controller
{
    private $privUser;
    private $taskModel;
    private $userModel;
    private $dataModel;
    private $organModel;
    private $questionModel;
    private $reminderModel;
    private $processModel;
    private $qualificationModel;
    private $isoModel;

    public function __construct()
    {
        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Get the current user's permissions
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        // Load the task model
        $this->taskModel = $this->model('Task');
        // Load the user model
        $this->userModel = $this->model('User');
        // Load the data model
        $this->dataModel = $this->model('Data');
        // Load the organ model
        $this->organModel = $this->model('Organ');
        // Load the question model
        $this->questionModel = $this->model('Question');
        // Load the reminder model
        $this->reminderModel = $this->model('Reminder');
        // Load the process model
        $this->processModel = $this->model('Process');
        // Load the qualification model
        $this->qualificationModel = $this->model('QualificationModel');
        // Load the iso model
        $this->isoModel = $this->model('IsoModel');
    }

    /* #####################################
        METHOD: INDEX
     ####################################### */
    public function index()
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        if ($this->privUser->hasPrivilege('9') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Aufgabensektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get all active single tasks
        $tasks = $this->taskModel->getActiveSingleTasks();

        // Limit description length in index view
        /*
        foreach($tasks as $task){

         if(!empty($task->description))
         $task->description =
        }
        */

        $data = [
            'bodyClass' => 'tasks',
            'privUser' => $this->privUser,
            'tasks' => $tasks
        ];
        // SQL JOIN - source: models/task.php
        // tasks: id = taskId
        // users: id = userId
        // tasks: name = taskName
        // users: name = userName
        // tasks: created_at = taskCreated
        // users: created_at = userCreated

        // Load view
        $this->view('tasks/index', $data);
    }

    /**********************
     * METHOD: ARCHIVED
     **********************/
    public function archived()
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        if ($this->privUser->hasPrivilege('9') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Aufgabensektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get all tasks
        $tasks = $this->taskModel->getArchivedSingleTasks();

        $data = [
            'bodyClass' => 'tasks',
            'privUser' => $this->privUser,
            'tasks' => $tasks
        ];
        // SQL JOIN - source: models/task.php
        // tasks: id = taskId
        // users: id = userId
        // tasks: name = taskName
        // users: name = userName
        // tasks: created_at = taskCreated
        // users: created_at = userCreated

        // Load view
        $this->view('tasks/archived', $data);
    }

    /**********************
     * METHOD: ALL
     **********************/
    public function all()
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        if ($this->privUser->hasPrivilege('9') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Aufgabensektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get all tasks
        $tasks = $this->taskModel->getAllTasks();

        $data = [
            'bodyClass' => 'tasks',
            'privUser' => $this->privUser,
            'tasks' => $tasks
        ];
        // SQL JOIN - source: models/task.php
        // tasks: id = taskId
        // users: id = userId
        // tasks: name = taskName
        // users: name = userName
        // tasks: created_at = taskCreated
        // users: created_at = userCreated

        // Load view
        $this->view('tasks/all', $data);
    }

    /* #####################################
        METHOD: Processes
     ####################################### */
    public function processes()
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        if ($this->privUser->hasPrivilege('9') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Aufgabensektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get all active single tasks
        $tasks = $this->taskModel->getAllTasksOfProcesses();

        // Limit description length in index view
        /*
        foreach($tasks as $task){

         if(!empty($task->description))
         $task->description =
        }
        */

        $data = [
            'bodyClass' => 'tasks',
            'privUser' => $this->privUser,
            'tasks' => $tasks
        ];
        // SQL JOIN - source: models/task.php
        // tasks: id = taskId
        // users: id = userId
        // tasks: name = taskName
        // users: name = userName
        // tasks: created_at = taskCreated
        // users: created_at = userCreated

        // Load view
        $this->view('tasks/processes', $data);
    }

    /* #####################################
          METHOD: Single
    ####################################### */
    public function single()
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        if ($this->privUser->hasPrivilege('9') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Aufgabensektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get all active single tasks
        $tasks = $this->taskModel->getAllSingleTasks();

        // Limit description length in index view
        /*
        foreach($tasks as $task){

         if(!empty($task->description))
         $task->description =
        }
        */

        $data = [
            'bodyClass' => 'tasks',
            'privUser' => $this->privUser,
            'tasks' => $tasks
        ];
        // SQL JOIN - source: models/task.php
        // tasks: id = taskId
        // users: id = userId
        // tasks: name = taskName
        // users: name = userName
        // tasks: created_at = taskCreated
        // users: created_at = userCreated

        // Load view
        $this->view('tasks/single', $data);
    }

    /**********************
     * METHOD: ADD TASK
     **********************/

    public function add()
    {


        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        if ($this->privUser->hasPrivilege('9') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt die Aufgabensektion zu betreten', 'alert alert-danger');
            redirect('users/dashboard');
        }

        $qualificationList = $this->qualificationModel->getAllQualifications();
        $isoList = $this->isoModel->getAllIsos();

        //////////////////////////////////////
        //////////////////////////////////////

        /* ########################################### */
        /* ################ GET ORGANS ############### */

        $organs = $this->organModel->getOrgansWithoutUser();

        foreach ($organs as $index => $organ) {

            // Filter out inactive organs
            if ($organ->is_active == null) {
                // Remove it
                unset($organs[$index]);
            }

            // Check if the current organ is the staff_functions organ
            if ($organ->id == 2) {
                // STAFF FUNCTIONS

                // Get the suborgans (which are the actual staff functions)
                $suborgans = $this->organModel->getSubOrgansByParentId(2);

                if (!empty($suborgans)) {
                    foreach ($suborgans as $suborgan) {

                        // Turn id string into array of ids
                        $id_array = $this->organModel->explodeIdString($suborgan->staff);

                        // Populate the array
                        $users = $this->organModel->populateUserIdArray($id_array, $this->userModel);

                        // Attach the populated array to the suborgan
                        $suborgan->users = $users;
                    }
                }
                // Attach the populated array to the organ
                $organ->suborgans = $suborgans;
            } else {
                // REGULAR ORGANS

                // Turn id strings into id arrays
                $managers = explode(',', $organ->management_level);
                $staff = explode(',', $organ->staff);

                // Merge both arrays
                $users = array_merge($managers, $staff);
                // Remove duplicate ids
                $users = array_unique($users);

                // Populate the array
                $users = $this->organModel->populateUserIdArray($users, $this->userModel);

                // Attach the populated array to the organ
                $organ->users = $users;
            }
        }

        /* ################################################ */


        // Check if there is a post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'bodyClass' => 'tasks addTask',
                'privUser' => $this->privUser,
                'user_id' => $_SESSION['user_id'],
                'name' => trim($_POST['name']),
                'description' => trim($_POST['description']),
                'department' => trim($_POST['department']),
                'qualifications' => $_POST['qualification'],
                'isos' => $_POST['iso'],
                'qualificationList' => $qualificationList,
                'isoList' => $isoList,
                //
                'organs' => $organs,
                //
                'input' => trim($_POST['input']),
                'input_source' => trim($_POST['input_source']),
                'output' => trim($_POST['output']),
                'output_target' => trim($_POST['output_target']),
                'category' => trim($_POST['category']),
                'risk' => trim($_POST['risk']),
                'status' => trim($_POST['status']),
                //
                'required_knowledge_text' => '',
                'required_knowledge' => trim($_POST['required_knowledge']),
                'required_knowledge_folders' => trim($_POST['required_knowledge_folders']),
                'required_knowledge_UI' => '',
                'required_knowledge_folders_UI' => '',
                // Roles
                'roles_responsible' => trim($_POST['roles_responsible']),
                'roles_concerned' => trim($_POST['roles_concerned']),
                'roles_contributing' => trim($_POST['roles_contributing']),
                'roles_information' => trim($_POST['roles_information']),
                // Recurring Check Intervals
                'check_intervals' => '',
                'interval_count' => '',
                'interval_format' => '',
                'activate_intervals' => '',
                'check_date' => '',
                'reference_day' => '',
                'reference_month' => '',
                'reference_year' => '',
                // Check Reminders
                'reminders_activated' => '',
                'remind_before' => '',
                'remind_at' => '',
                'remind_after' => '',
                //
                'required_resources_text' => trim($_POST['required_resources_text']),
                'required_resources' => trim($_POST['required_resources']),
                'required_resources_folders' => trim($_POST['required_resources_folders']),
                'required_resources_UI' => '',
                'required_resources_folders_UI' => '',
                'existing_compentencies' => trim($_POST['existing_compentencies']),
                'required_education' => trim($_POST['required_education']),
                // Tags
                'tags' => trim($_POST['tags']),
                'taglist' => '',
                'tags_array' => '',
                // Flexible Questions
                'flexible_questions' => '',
                //
                'is_singletask' => 'true',
                'is_approved' => 'true',
                ///////////////////////////
                //////////// Errors
                ///////////////////////////
                'name_err' => '',
                // description is optional
                'department_err' => '',
                'input_err' => '',
                'input_source_err' => '',
                'output_err' => '',
                'output_target_err' => '',
                'category_err' => '',
                'risk_err' => '',
                'status_err' => '',
                'required_knowledge_err' => '',
                'roles_err' => '',
                'check_intervals_err' => '',
                'reminders_err' => '',
                'reference_date_err' => '',
                'required_resources_err' => '',
                'existing_compentencies_err' => '',
                'required_education_err' => '',
                // tags are optional
            ];

            if (in_array('none', $data['qualifications'])) {
                $data['qualifications'] = null;
            }

            if (in_array('none', $data['isos'])) {
                $data['isos'] = null;
            }

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Bitte Namen eintragen';
            }
            if (empty($data['department'])) {
                $data['department_err'] = 'Bitte Unternehmensbereich auswählen';
            }
            if (empty($data['input'])) {
                $data['input_err'] = 'Bitte Input eintragen';
            }

            if (empty($data['input_source'])) {
                $data['input_source_err'] = 'Bitte Namen eintragen';
            }
            if (empty($data['output'])) {
                $data['output_err'] = 'Bitte Unternehmensbereich eintragen';
            }
            if (empty($data['output_target'])) {
                $data['output_target_err'] = 'Bitte Input eintragen';
            }

            if ($data['category'] == 'empty') {
                $data['category_err'] = 'Bitte Kategorie auswählen';
            }
            if ($data['risk'] == 'empty') {
                $data['risk_err'] = 'Bitte Risikoeinstufung eintragen';
            }
            if ($data['status'] == 'empty') {
                $data['status_err'] = 'Bitte Status auswählen';
            }

            if (empty($data['existing_compentencies'])) {
                //$data['existing_compentencies_err'] = 'Bitte bestehende Kompetenzen angeben';
            }
            if (empty($data['required_education'])) {
                //$data['required_education_err'] = 'Bitte erforderlichen Weiterbildungsbedarf angeben';
            }

            /* ################################################# */
            /* ################ ROLES VALIDATION ############### */

            $r_responsible = $this->processRole($data['roles_responsible']);
            $r_concerned = $this->processRole($data['roles_concerned']);
            $r_contributing = $this->processRole($data['roles_contributing']);
            $r_information = $this->processRole($data['roles_information']);

            /* ##################### REQUIRED KNOWLEDGE ###################### */

            $knowledgeData = $this->processRequiredKnowledge($data['required_knowledge']);
            $knowledgeFoldersData = $this->processRequiredKnowledgeData($data['required_knowledge_folders']);

            // Update the UI data
            $data['required_knowledge_UI'] = $knowledgeData;
            $data['required_knowledge_folders_UI'] = $knowledgeFoldersData;

            /* ##################### REQUIRED RESOURCES ##################### */

            $resourcesData = $this->processRequiredResources($data['required_resources']);
            $resourceFoldersData = $this->processRequiredResourcesData($data['required_resources_folders']);

            // Update the UI data
            $data['required_resources_UI'] = $resourcesData;
            $data['required_resources_folders_UI'] = $resourceFoldersData;


            /* ##################### CHECK INTERVALS ##################### */

            // Check if the activate_intervals Checkbox is set
            if (isset($_POST['activate_intervals'])) {
                // ACTIVATED
                // Get the reference date
                $data['reference_day'] = trim($_POST['reference_day']);
                $data['reference_month'] = trim($_POST['reference_month']);
                $data['reference_year'] = trim($_POST['reference_year']);
                // Get the interval data
                $data['interval_count'] = trim($_POST['interval_count']);
                $data['interval_format'] = trim($_POST['interval_format']);

                // Check if intervall fields are empty
                if (empty($data['interval_count']) && $data['interval_format'] == 'empty') {
                    // Add the appropriate error message to the corresponding variable
                    $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl und ein Intervallformat aus.';
                } elseif (empty($data['interval_count'])) {
                    // Add the appropriate error message to the corresponding variable
                    $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl aus';
                } elseif ($data['interval_format'] == 'empty') {
                    // Add the appropriate error message to the corresponding variable
                    $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallformat aus.';
                } elseif (empty($data['reference_day']) || empty($data['reference_month']) || empty($data['reference_year'])) {
                    $data['reference_date_err'] = 'Bitte füllen Sie alle Felder des Referenzdatums aus.';
                } else {
                    // Construct the reference date
                    $data['reference_date'] = $data['reference_year'] . '-' . $data['reference_month'] . '-' . $data['reference_day'];
                    // Calculate the checkdate
                    // Get the reference date from the DB
                    $timestamp = strtotime($data['reference_date']);
                    // Calculate the next check date
                    // source: helpers/date_time_helper.php
                    $checkdate = calculateCheckdate($timestamp, $data['interval_count'], $data['interval_format']);
                    $data['check_date'] = $checkdate['year'] . '-' . $checkdate['month'] . '-' . $checkdate['day'];
                    // Construct the check interval text value
                    $data['check_intervals'] = $data['interval_count'] . ' ' . $data['interval_format'];
                }
            } else {
                // UNACTIVATED
                $data['check_intervals'] = 'Kein Check Intervall';
                $data['activate_intervals'] = false;
                // Set the variables to NULL
                $data['reference_date'] = null;
                $data['interval_count'] = null;
                $data['interval_format'] = null;
                $data['check_date'] = null;
                $data['check_intervals_err'] = '';
            }


            /* ############################# REMINDERS ############################ */
            // Check if the reminders and check intervals both have been activated
            if (isset($_POST['activate_reminders']) && isset($_POST['activate_intervals'])) {
                // ACTIVE
                // Fill the variables with the respective POST values
                $data['reminders_activated'] = true;

                // If the value is 0 set it to NULL
                // Otherwise set it to the value
                if (trim($_POST['remind_before']) == 0) {
                    $data['remind_before'] = null;
                } else {
                    $data['remind_before'] = trim($_POST['remind_before']);
                }

                // If the value is 0 set it to NULL
                // Otherwise set it to the value
                if (trim($_POST['remind_at']) != 'activated') {
                    $data['remind_at'] = null;
                } else {
                    $data['remind_at'] = trim($_POST['remind_at']);
                }

                // If the value is 0 set it to NULL
                // Otherwise set it to the value
                if (trim($_POST['remind_after']) == 0) {
                    $data['remind_after'] = null;
                } else {
                    $data['remind_after'] = trim($_POST['remind_after']);
                }

                // Check if all fields are empty or unset
                if (empty($data['remind_before']) && empty($data['remind_at']) && empty($data['remind_after'])) {
                    // ALL EMPTY
                    $data['reminders_err'] = 'Bitte wählen Sie mindestens eine der drei Erinnerungsmöglichkeiten aus oder deaktivieren Sie die Erinnerungsfunktion.';
                }
            } elseif (isset($_POST['activate_reminders']) && !isset($_POST['activate_intervals'])) {
                // ERROR: CHECK INTERVALS NOT ACTIVATED
                $data['reminders_err'] = 'Die Erinnerungsfunktion kann nur aktiviert werden, sofern zunächst ein Check-Intervall definiert wurde.';
            } else {
                // INACTIVE
                $data['reminders_activated'] = false;
                $data['remind_before'] = null;
                $data['remind_at'] = null;
                $data['remind_after'] = null;
            }

            /* ########### TAGS ########## */

            $data['taglist'] = $data['tags'];
            // Check if the tags array is filled
            if (!empty($data['tags'])) {
                // Validated
                // Create an array of tags
                $data['tags_array'] = explode(',', $data['tags']);
            } else {
                // Invalid
                // Set the tags array to empty
                $data['tags_array'] = [];
            }

            /* ######################################### */
            /* ########### FLEXIBLE QUESTIONS ########## */

            // Make sure there is a flexible question
            if (isset($_POST['flexible_questions'])) {
                $data['flexible_questions'] = $_POST['flexible_questions'];

                // Explode the flexible_questions string
                $questionsArr = explode('||', $_POST['flexible_questions']);

                // Init variable for later
                $questionData = [];

                foreach ($questionsArr as $questionObj) {
                    // Make sure the current item is not empty
                    if (!empty($questionObj)) {
                        // Explode it into an array
                        $parts = explode('/!/', $questionObj);
                        // Add each part to a correspondig variable
                        $question = $parts[0];
                        $yesNo = substr($parts[1], 3);
                        $notify = substr($parts[2], 2);
                        $remind = substr($parts[3], 2);

                        // Prepare $answers_UI for output
                        $answers_UI = '';

                        if ($yesNo == 'true') {
                            $answers_UI .= 'Ja/Nein, ';
                        }

                        if ($notify == 'true') {
                            $answers_UI .= 'Mitarbeiterbenachrichtigung, ';
                        }

                        if ($remind == 'true') {
                            $answers_UI .= 'Erinnerung, ';
                        }
                        // Remove the space and the comma
                        $answers_UI = substr($answers_UI, 0, -2);

                        $questionData[] = array(
                            'question' => $question,
                            'yesNo' => $yesNo,
                            'notify' => $notify,
                            'remind' => $remind,
                            'answers_UI' => $answers_UI,
                            'string_UI' => $questionObj
                        );
                    }
                }
                // Store questionData for UI
                $data['flexible_questions_UI'] = $questionData;
            }


            // Make sure no errors
            if (empty($data['name_err']) &&
                empty($data['department_err']) &&
                empty($data['input_err']) &&
                empty($data['input_source_err']) &&
                empty($data['output_err']) &&
                empty($data['output_target_err']) &&
                empty($data['category_err']) &&
                empty($data['risk_err']) &&
                empty($data['status_err']) &&
                empty($data['required_knowledge_err']) &&
                empty($data['check_intervals_err']) &&
                empty($data['reminders_err']) &&
                empty($data['reference_date_err']) &&
                empty($data['required_resources_err']) &&
                empty($data['tags_err'])) {
                // Validated
                // Add the task to the database and get its id
                $taskId = $this->taskModel->addTask($data);


                // Check if the database request was successful
                if ($taskId != false) {
                    // SUCCESS
                    // ASSIGN THE TASK TO THE USERS
                    // role: responsible
                    $this->assignTaskToUsers($r_responsible, 'responsible', $taskId);
                    // role: concerned
                    $this->assignTaskToUsers($r_concerned, 'concerned', $taskId);
                    // role: contributing
                    $this->assignTaskToUsers($r_contributing, 'contributing', $taskId);
                    // role: information
                    $this->assignTaskToUsers($r_information, 'information', $taskId);

                    // Add qualifications
                    if (!is_null($data['qualifications'])) {
                        foreach ($data['qualifications'] as $qualification) {
                            $this->qualificationModel->assignTaskQualification($qualification, $taskId);
                        }
                    }

                    // Add isos
                    if (!is_null($data['isos'])) {
                        foreach ($data['isos'] as $iso) {
                            $this->isoModel->assignTaskIso($iso, $taskId);
                        }
                    }

                    // SUBMIT FLEXIBLE QUESTIONS TO DB
                    // Loop through all questions
                    foreach ($questionData as $questItem) {
                        // Prepare data
                        $data = [
                            'question' => $questItem['question'],
                            'yesNo' => $questItem['yesNo'],
                            'notify' => $questItem['notify'],
                            'remind' => $questItem['remind'],
                            'task_id' => $taskId,
                            'method_id' => null,
                            'document_id' => null
                        ];
                        // Submit the question to the DB
                        $this->questionModel->addFlexibleQuestion($data);
                    }

                    // SUMBIT TAGS


                    // Prepare the flash messages and redirect to tasks
                    prepareFlash('task_message', 'Die Aufgabe wurde erstellt.');
                    redirect('tasks/show/' . $taskId);
                } else {
                    // Error
                    die('Ein Fehler ist aufgetreten. Bitte kontaktieren Sie den Entwickler');
                }
            } else {

                /* ######## ROLES ######### */

                // Update the fields for the UI
                $data['roles_responsible_id'] = $data['roles_responsible'];
                $data['roles_responsible'] = $r_responsible;
                // Update the fields for the UI
                $data['roles_concerned_id'] = $data['roles_concerned'];
                $data['roles_concerned'] = $r_concerned;
                // Update the fields for the UI
                $data['roles_contributing_id'] = $data['roles_contributing'];
                $data['roles_contributing'] = $r_contributing;
                // Update the fields for the UI
                $data['roles_information_id'] = $data['roles_information'];
                $data['roles_information'] = $r_information;

                //die(var_dump( $r_responsible )  );

                // Load the view with Errors
                $this->view('tasks/add', $data);
            }
        } else {
            // Otherwise load the view, in order to be able to add a task

            // Init data
            $data = [
                'bodyClass' => 'tasks addTask',
                'privUser' => $this->privUser,
                'user_id' => '',
                'name' => '',
                'description' => '',
                'department' => '',
                'qualificationList' => $qualificationList,
                'isoList' => $isoList,
                //
                'organs' => $organs,
                //
                'input' => '',
                'input_source' => '',
                'output' => '',
                'output_target' => '',
                'category' => '',
                'risk' => '',
                'status' => '',
                'required_knowledge' => '',
                'required_knowledge_folders' => '',
                // Roles
                'roles_responsible' => '',
                'roles_concerned' => '',
                'roles_contributing' => '',
                'roles_information' => '',
                'roles_responsible_id' => '',
                'roles_concerned_id' => '',
                'roles_contributing_id' => '',
                'roles_information_id' => '',
                //
                // Recurring Check Intervals
                'interval_count' => '',
                'interval_format' => '',
                'activate_intervals' => '',
                'check_date' => '',
                'reference_day' => '',
                'reference_month' => '',
                'reference_year' => '',
                // Check Reminders
                'reminders_activated' => '',
                'remind_before' => '',
                'remind_at' => '',
                'remind_after' => '',
                //
                'required_resources' => '',
                'required_resources_folders' => '',
                'existing_compentencies' => '',
                'required_education' => '',
                'required_education_folders' => '',
                'tags' => '',
                'taglist' => '',
                'flexible_questions' => '',
                // ##########################
                // ######## Errors ##########
                'name_err' => '',
                // description is optional
                'department_err' => '',
                'input_err' => '',
                'input_source_err' => '',
                'output_err' => '',
                'output_target_err' => '',
                'category_err' => '',
                'risk_err' => '',
                'status_err' => '',
                'required_knowledge_err' => '',
                'roles_err' => '',
                'check_intervals_err' => '',
                'reminders_err' => '',
                'reference_date_err' => '',
                'required_resources_err' => '',
                'existing_compentencies_err' => '',
                'required_education_err' => '',
                'tags_err' => ''
            ];

            // Load view
            $this->view('tasks/add', $data);
        }
    }

    /*****************************************
     * PRIVATE METHOD: ASSIGN TASK TO USERS
     ******************************************/

    private function assignTaskToUsers($userArray, $role, $taskId)
    {
        // Check if $userArray is filled
        if (!empty($userArray)) {
            // Loop through it and process each user

            foreach ($userArray as $user) {

                // Prepare the data array for the model
                $modelData = [
                    'task_id' => $taskId,
                    'user_id' => $user['user']->id ?? 0,
                    'role' => $role,
                    'organ_id' => $user['organ']->id ?? 0,
                ];

                if (!empty($user['suborgan'])) {
                    $modelData['suborgan_id'] = $user['suborgan']->id;
                } else {
                    $modelData['suborgan_id'] = null;
                }

                // Assign the task to user
                $this->taskModel->addUnacceptedAssignment($modelData);
            }
        }
    }

    /*****************************************
     * PRIVATE METHOD: PROCESS ROLE
     ******************************************/

    private function processRole($role)
    {
        // Check if user(s) are assigned to the role
        if (empty($role)) {
            // no user found
            // set variable to false
            return $roleArray = false;
        } else {
            // Get the roles string and turn each user (user_id) into an array item of $roleArray
            $roleArray = explode(',', $role);
            // Loop through the array and get each user as an object
            foreach ($roleArray as $index => $role_ids) {
                if (substr_count($role_ids, '_') == 1) {
                    // Explode the string
                    $role_organ_ids = explode('_', $role_ids);

                    // Get the user
                    $user = $this->userModel->getUserById($role_organ_ids[0]);

                    // Get the organ
                    $organ = $this->organModel->getOrganById($role_organ_ids[1]);


                    // Add both objects to the $roleArray
                    $roleArray[$index] = array(
                        'user' => $user,
                        'organ' => $organ
                    );
                } elseif (substr_count($role_ids, '_') == 2) {
                    // Explode the string
                    $role_organ_suborgan_ids = explode('_', $role_ids);

                    // Get the user
                    $user = $this->userModel->getUserById($role_organ_suborgan_ids[0]);

                    // Get the organ
                    $organ = $this->organModel->getOrganById($role_organ_suborgan_ids[1]);

                    // Get the organ
                    $suborgan = $this->organModel->getSuborganById($role_organ_suborgan_ids[2]);


                    // Add both objects to the $roleArray
                    $roleArray[$index] = array(
                        'user' => $user,
                        'organ' => $organ,
                        'suborgan' => $suborgan
                    );
                } else {
                    $user = $this->userModel->getUserById($role_ids);
                    $organ = 'unbekannt';
                    $roleArray[$index] = array(
                        'user' => $user,
                        'organ' => $organ
                    );
                }
            }
            return $roleArray;
        }
    }

    /***********************************************************
     * PRIVATE METHOD: PROCESS A PROCESS ROLE (ONLY FOR PROCESS TASKS)
     ********************************************* **************/

    private function processProcessRole($role)
    {
        // Check if user(s) are assigned to the role
        if (empty($role)) {
            // no user found
            // set variable to false
            return $roleArray = false;
        } else {
            // Get the roles string and turn each user (user_id) into an array item of $roleArray
            $roleArray = explode(',', $role);
            // Loop through the array and get each user as an object
            foreach ($roleArray as $key => $item) {
                $userAndOrgan = explode('_', $item);
                // Get the user from DB
                $userObj = $this->userModel->getUserById($userAndOrgan[0]);
                // Get the organ from DB
                $organObj = $this->organModel->getOrganById($userAndOrgan[1]);

                // Add the user and organ objects to the $roleArray
                $roleArray[$key] = array(
                    'user' => $userObj,
                    'organ' => $organObj
                );
            }
            return $roleArray;
        }
    }

    /******************************
     * PRIVATE VARIABLE: FILETYPES
     *******************************/

    private $fileTypes = [
        // Documents
        'csv' => 'fa-file-csv',
        'doc' => 'fa-file-word',
        'docx' => 'fa-file-word',
        'odt' => 'fa-file-alt',
        'ppt' => 'fa-powerpoint',
        'pdf' => 'fa-file-pdf',
        'xls' => 'fa-file-alt',
        'xlsx' => 'fa-file-excel',
        // Blank
        'blank' => 'fa-file'
    ];

    /***************************************************
     * PRIVATE METHOD: PROCESS THE REQUIRED KNOWLEDGE
     ****************************************************/

    private function processRequiredKnowledge($data)
    {
        $fileTypes = $this->fileTypes;
        $dataModel = $this->dataModel;

        // Get the required resources (files) and process them
        if (!empty($data)) {
            $knowledgeItems = explode('|', $data);

            // Init variabls
            $knowledgeData = [];

            // Loop through the $resources array
            foreach ($knowledgeItems as $resource) {

                // If the $resource is empty skip it
                if (empty($resource)) {
                    continue;
                }

                // Get the file from DB
                $fileObj = $dataModel->getFileById($resource);

                // Get the name of the file
                $name = $fileObj->file_name;

                // Get the path
                $path = $fileObj->file_path;
                // Remove '..' from the path
                $path = substr($path, 2);

                // Get the file extension
                $fileExt = substr(strrchr($name, '.'), 1);

                // Determine the respective font-awesome css icon-class
                if (isset($fileTypes[$fileExt])) {
                    $iconClass = $fileTypes[$fileExt];
                } else {
                    $iconClass = $fileTypes['blank'];
                }

                array_push($knowledgeData, array(
                    'id' => $resource,
                    'name' => $name,
                    'path' => $path,
                    'extension' => $fileExt,
                    'icon_class' => $iconClass
                ));
            }

            // Return the data
            return $knowledgeData;
        } else {
            // Set it to empty
            $knowledgeData = [];
            // Return the data
            return $knowledgeData;
        }
    }

    /********************************************************
     * PRIVATE METHOD: PROCESS THE REQUIRED KNOWLEDGE DATA
     **********************************************************/

    private function processRequiredKnowledgeData($data)
    {

        // Get the required model for further action
        $dataModel = $this->dataModel;

        // Make sure the input data is not empty
        if (!empty($data)) {
            $knowledgeFolders = explode('|', $data);

            // Init variable
            $knowledgeFoldersData = [];

            // Loop through the $resourceFolders array
            foreach ($knowledgeFolders as $key => $folder) {
                // If the $folder is empty skip it
                if (empty($folder)) {
                    continue;
                }

                // Get the folder from DB
                $folderObj = $dataModel->getFolderById($folder);

                // Get the name of the folder or file
                $name = $folderObj->folder_name;

                // Add the data to the $resourceFoldersData array
                $knowledgeFoldersData[$key] = [
                    'id' => $folder,
                    'folderName' => $name,
                    // create an empty array for the files
                    'files' => []
                ];

                // Get the data within the folder
                $containedData = $dataModel->getDataByFolderId($folder);

                // Loop through containedData and process the data
                foreach ($containedData as $file) {
                    $props = [
                        'file_name' => $file->file_name,
                        'file_path' => substr($file->file_path, 2)
                    ];

                    // Add the array of the file data to the files array
                    array_push($knowledgeFoldersData[$key]['files'], $props);
                }
            }

            // Return the data
            return $knowledgeFoldersData;
        } else {
            // Set it to empty
            $knowledgeFoldersData = [];
            // Return the data
            return $knowledgeFoldersData;
        }
    }

    /****************************************************
     * PRIVATE METHOD: PROCESS THE REQUIRED RESOURCES
     *****************************************************/

    private function processRequiredResources($data)
    {
        $fileTypes = $this->fileTypes;
        $dataModel = $this->dataModel;

        // Get the required resources (files) and process them
        if (!empty($data)) {
            $resources = explode('|', $data);

            // Init variable
            $resourcesData = [];

            // Loop through the $resources array
            foreach ($resources as $resource) {

                // If the $resource is empty skip it
                if (empty($resource)) {
                    continue;
                }

                // Get the file from DB
                $fileObj = $dataModel->getFileById($resource);

                // Get the name of the file
                $name = $fileObj->file_name;

                // Get the path
                $path = $fileObj->file_path;
                // Remove '..' from the path
                $path = substr($path, 2);

                // Get the file extension
                $fileExt = substr(strrchr($name, '.'), 1);

                // Determine the respective font-awesome css icon-class
                if (isset($fileTypes[$fileExt])) {
                    $iconClass = $fileTypes[$fileExt];
                } else {
                    $iconClass = $fileTypes['blank'];
                }

                // Add all the data to the $resourcesData
                array_push($resourcesData, array(
                    'id' => $resource,
                    'name' => $name,
                    'path' => $path,
                    'extension' => $fileExt,
                    'icon_class' => $iconClass
                ));
            }

            return $resourcesData;
        } else {
            // Set it to empty
            $resourcesData = [];

            return $resourcesData;
        }
    }

    private function processRequiredResourcesData($data)
    {

        // Get the required model for further action
        $dataModel = $this->dataModel;

        if (!empty($data)) {
            $resourceFolders = explode('|', $data);

            // Init variable
            $resourceFoldersData = [];

            // Loop through the $resourceFolders array
            foreach ($resourceFolders as $key => $folder) {
                // If the $folder is empty skip it
                if (empty($folder)) {
                    continue;
                }

                // Get the folder from DB
                $folderObj = $dataModel->getFolderById($folder);

                // Get the name of the folder or file
                $name = $folderObj->folder_name;

                // Add the data to the $resourceFoldersData array
                $resourceFoldersData[$key] = [
                    'id' => $folder,
                    'folderName' => $name,
                    // create an empty array for the files
                    'files' => []
                ];

                // Get the data within the folder
                $containedData = $dataModel->getDataByFolderId($folder);

                // Loop through containedData and process the data
                foreach ($containedData as $file) {
                    $props = [
                        'file_name' => $file->file_name,
                        'file_path' => substr($file->file_path, 2)
                    ];

                    // Add the array of the file data to the files array
                    array_push($resourceFoldersData[$key]['files'], $props);
                }
            }

            return $resourceFoldersData;
        } else {
            // Set it to empty
            $resourceFoldersData = [];

            return $resourceFoldersData;
        }
    }


    /* ##################################################### */
    /* ################# METHOD: SHOW TASK ################# */
    /* ##################################################### */

    public function show($id)
    {

        // Get the task
        // $id is coming from the URL and is passed to the postModel
        $task = $this->taskModel->getAnyTaskById($id);

        //die(var_dump($task));

        // Check if task is archived(empty)
        if (empty($task)) {
            $archivedTask = $this->taskModel->getArchivedTaskById($id);

            if (empty($archivedTask)) {
                redirect('tasks');
            } else {
                // replace task with the archived task
                $task = $archivedTask;
            }
        }

        // Get the creator of the task
        $user = $this->userModel->getUserById($task->user_id);

        /* ################################################## */
        /* #################### PROCESS ##################### */

        $process = '';

        if (!empty($task->process_id) && $task->is_singletask == false) {
            // Get the process
            $process = $this->processModel->getProcessById($task->process_id);
        }

        /* ############################################### */
        /* ################## DEPARTMENT ################# */

        $task->department = $this->organModel->getOrganById($task->department)->name;

        /* ############################################### */
        /* #################### ROLES #################### */

        // string to array of objects conversion

        $r_responsible = $this->processRole($task->roles_responsible);
        $r_concerned = $this->processRole($task->roles_concerned);
        $r_contributing = $this->processRole($task->roles_contributing);
        $r_information = $this->processRole($task->roles_information);

        /* ############################################### */


        /* ############################################### */
        /* ################ CHECK INTERVALS ############## */

        // Make sure there is a reference date before procceeding
        if (!empty($task->reference_date)) {
            // ######## REFERENCE DATE ############
            // Get the date and convert it
            // source: helpers/date_time_helper.php
            $dateArray = convert_date_time($task->reference_date);
            // Prepare variable for output
            $referenceDate = $dateArray['day'] . '. ' . $dateArray['month'] . ' ' . $dateArray['year'];
        } else {
            // Set referenceDate to an error message
            $referenceDate = 'nicht angegeben';
        }

        /* ############################################### */
        /* ################## CHECKDATE ################## */

        if (!empty($task->check_date)) {
            $check_date = convert_date_time($task->check_date);
        } else {
            $check_date = false;
        }
        $nextCheckdate = calculateNextCheckdate($task->check_date);

        //die( var_dump($nextCheckdate) );

        /* ############################################### */
        /* ################## CHECKS ##################### */

        $checks = $this->taskModel->getChecksByTaskId($id);

        if (!empty($checks)) {
            // Init output array
            $checksOutput = [];
            foreach ($checks as $check) {
                // Get the user
                $user = $this->userModel->getUserById($check->user_id);
                // Get the timestamp and convert it
                $checkedAt = convert_date_time($check->checked_at);
                // Add the data to the outputArray
                $checksOutput[] = array(
                    'user' => $user,
                    'checked_at' => $checkedAt,
                    'comment' => $check->comment
                );
            }
        } else {
            $checksOutput = '';
        }

        /* ############################################### */
        /* ################# REMINDERS ################### */

        $reminders = [
            'before' => '',
            'at' => '',
            'after' => ''
        ];

        if ($task->remind_before == 0) {
            $reminders['before'] = false;
        } else {
            $reminders['before'] = $task->remind_before;
        }

        if ($task->remind_at == 0) {
            $reminders['at'] = false;
        } else {
            $reminders['at'] = $task->remind_at;
        }

        if ($task->remind_after == 0) {
            $reminders['after'] = false;
        } else {
            $reminders['after'] = $task->remind_after;
        }

        /* ##################################################### */
        /* ############### UNACCEPTED ASSIGNMENTS ############## */

        // Get all unaccepted assignments
        $unacceptedAssignments = $this->taskModel->getUnacceptedAssignmentsByTaskId($id);

        // Check if this task has unaccepted assignments
        if (!empty($unacceptedAssignments)) {
            // Init variable
            $nonconfirmingUsers = [];
            // Loop through unacceptedAssignments
            foreach ($unacceptedAssignments as $assignment) {
                // Get the role
                // source: helpers/role_helper.php
                $role = translateRoleLanguage($assignment->role);
                // Prepare the array for output
                $nonconfirmingUsers[] = array(
                    'user_id' => $assignment->user_id,
                    'role' => $role,
                    'role_js' => $assignment->role // for the javascript acceptance process
                );
            }
        } else {
            //empty
            // set the output array to false
            $nonconfirmingUsers = false;
        }

        /* ############################################### */
        /* #################### TAGS ##################### */

        // Split the tags string into an array of tags
        $tags = explode(',', $task->tags);

        /* ########################################################### */
        /* ################## FLEXIBLE QUESTIONS ##################### */

        $questions = $this->questionModel->getQuestionsByTaskId($id);
        // Init variable
        $questionData = [];

        if (!empty($questions)) {
            foreach ($questions as $questItem) {
                // Fill the questionData array with values
                $questionData[] = $questItem;
            }
        }


        $data = [
            'bodyClass' => 'tasks showTask',
            'privUser' => $this->privUser,
            'task' => $task,
            'process' => $process,
            'tags' => $tags,
            'roles_responsible' => $r_responsible,
            'roles_concerned' => $r_concerned,
            'roles_contributing' => $r_contributing,
            'roles_information' => $r_information,
            'non_confirming_users' => $nonconfirmingUsers,
            'reference_date' => $referenceDate,
            'checkdate' => $check_date,
            'next_checkdate' => $nextCheckdate,
            'checks' => $checksOutput,
            'reminders' => $reminders,
            'required_knowledge' => $this->processRequiredKnowledge($task->required_knowledge),
            'required_knowledge_folders' => $this->processRequiredKnowledgeData($task->required_knowledge_folders),
            'required_resources' => $this->processRequiredResources($task->required_resources),
            'required_resources_folders' => $this->processRequiredResourcesData($task->required_resources_folders),
            'user' => $user,
            'questions' => $questionData
        ];

        $this->view('tasks/show', $data);
    }

    /**********************
     * METHOD: EDIT TASK
     **********************/

    public function edit($id)
    {

        /////////////////////////////////////
        /////// Validate Privileges /////////
        /////////////////////////////////////

        if ($this->privUser->hasPrivilege('2') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt Aufgaben zu bearbeiten', 'alert alert-danger');
            redirect('tasks/show/' . $id);
        }

        //////////////////////////////////////
        //////////////////////////////////////

        // Get existing task from model
        $task = $this->taskModel->getUnarchivedTaskById($id);
        $qualifications = $this->qualificationModel->getAllQualifications();
        $isos = $this->isoModel->getAllIsos();

        if ($task->is_approved == 'false' && $task->is_singletask == false) {
            // TASK = UNAPPROVED PROCESSD TASK
            // Validate the users priviledge
            if ($this->privUser->hasPrivilege('23') != true) {
                prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt nicht-freigegebene Prozessaufgaben zu bearbeiten', 'alert alert-danger');
                redirect('tasks/show/' . $id);
            }
        }

        /* ########################################### */
        /* ################ GET ORGANS ############### */

        $organs = $this->organModel->getOrgansWithoutUser();

        foreach ($organs as $index => $organ) {

            // Filter out inactive organs
            if ($organ->is_active == null) {
                // Remove it
                unset($organs[$index]);
            }

            // Check if the current organ is the staff_functions organ
            if ($organ->id == 2) {
                // STAFF FUNCTIONS

                // Get the suborgans (which are the actual staff functions)
                $suborgans = $this->organModel->getSubOrgansByParentId(2);

                if (!empty($suborgans)) {
                    foreach ($suborgans as $suborgan) {

                        // Turn id string into array of ids
                        $id_array = $this->organModel->explodeIdString($suborgan->staff);

                        // Populate the array
                        $users = $this->organModel->populateUserIdArray($id_array, $this->userModel);

                        // Attach the populated array to the suborgan
                        $suborgan->users = $users;
                    }
                }
                // Attach the populated array to the organ
                $organ->suborgans = $suborgans;
            } else {
                // REGULAR ORGANS

                // Turn id strings into id arrays
                $managers = explode(',', $organ->management_level);
                $staff = explode(',', $organ->staff);

                // Merge both arrays
                $users = array_merge($managers, $staff);
                // Remove duplicate ids
                $users = array_unique($users);

                // Populate the array
                $users = $this->organModel->populateUserIdArray($users, $this->userModel);

                // Attach the populated array to the organ
                $organ->users = $users;
            }
        }

        /* ############################################# */


        // Check if there is a post request
        // So this block only runs if the permission in the else codeblock is already validated
        // Otherwise a post request would not be possible to make
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitize POST array
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Get organs
            $organs = $this->organModel->getOrgans();

            $data = [
                'bodyClass' => 'tasks editTask',
                'privUser' => $this->privUser,
                'id' => $id,
                'user_id' => $_SESSION['user_id'],
                'name' => trim($_POST['name']),
                'description' => trim($_POST['description']),
                'department' => trim($_POST['department']),
                'qualifications' => $_POST['qualification'],
                'isos' => $_POST['iso'],
                //
                'organs' => $organs,
                //
                'input' => trim($_POST['input']),
                'input_source' => trim($_POST['input_source']),
                'output' => trim($_POST['output']),
                'output_target' => trim($_POST['output_target']),
                'category' => trim($_POST['category']),
                'risk' => trim($_POST['risk']),
                'status' => trim($_POST['status']),
                'required_knowledge_text' => '',
                'required_knowledge' => trim($_POST['required_knowledge']),
                'required_knowledge_folders' => trim($_POST['required_knowledge_folders']),
                'required_knowledge_UI' => '',
                'required_knowledge_folders_UI' => '',
                // Roles
                'roles_responsible' => trim($_POST['roles_responsible']),
                'roles_concerned' => trim($_POST['roles_concerned']),
                'roles_contributing' => trim($_POST['roles_contributing']),
                'roles_information' => trim($_POST['roles_information']),
                // Recurring Check Intervals
                'check_intervals' => '',
                'reference_date' => '',
                'activate_intervals' => '',
                'interval_count' => '',
                'interval_format' => '',
                'check_date' => '',
                // Check Reminders
                'reminders_activated' => '',
                'remind_before' => '',
                'remind_at' => '',
                'remind_after' => '',
                //
                'required_resources_text' => trim($_POST['required_resources_text']),
                'required_resources' => trim($_POST['required_resources']),
                'required_resources_folders' => trim($_POST['required_resources_folders']),
                'required_resources_UI' => '',
                'required_resources_folders_UI' => '',
                'existing_compentencies' => trim($_POST['existing_compentencies']),
                'required_education' => trim($_POST['required_education']),
                'tags' => trim($_POST['tags']),
                'taglist' => '',
                'tags_array' => '',
                ////////////////////////
                // Errors
                'name_err' => '',
                // description is optional
                'input_err' => '',
                'input_source_err' => '',
                'output_err' => '',
                'output_target_err' => '',
                'category_err' => '',
                'risk_err' => '',
                'status_err' => '',
                'required_knowledge_err' => '',
                'roles_err' => '',
                'check_intervals_err' => '',
                'reminders_err' => '',
                'reference_date_err' => '',
                'required_resources_err' => '',
                'existing_compentencies_err' => '',
                'required_education_err' => '',
                'tags_err' => ''
            ];

            if (in_array('none', $data['qualifications'])) {
                $data['qualifications'] = null;
            }

            if (in_array('none', $data['isos'])) {
                $data['isos'] = null;
            }

            // Validate data
            if (empty($data['name'])) {
                $data['name_err'] = 'Bitte Namen eintragen';
            }

            if (empty($data['department'])) {
                $data['department'] = null;
            }

            if (empty($data['input'])) {
                $data['input_err'] = 'Bitte Input eintragen';
            }

            if (empty($data['input_source'])) {
                $data['input_source_err'] = 'Bitte Namen eintragen';
            }
            if (empty($data['output'])) {
                $data['output_err'] = 'Bitte Unternehmensbereich eintragen';
            }
            if (empty($data['output_target'])) {
                $data['output_target_err'] = 'Bitte Input eintragen';
            }

            if (empty($data['category'])) {
                $data['category'] = null;
                $data['category_err'] = 'Bitte Kategorie zuteilen';
            }

            if (empty($data['risk'])) {
                $data['risk'] = null;
                $data['risk_err'] = 'Bitte Risiko einstufen';
            }

            if (empty($data['status'])) {
                $data['status'] = null;
                $data['status_err'] = 'Bitte Status einstufen';
            }

            if (empty($data['existing_compentencies'])) {
                //$data['existing_compentencies_err'] = 'Bitte bestehende Kompetenzen angeben';
            }
            if (empty($data['required_education'])) {
                //$data['required_education_err'] = 'Bitte erforderlichen Weiterbildungsbedarf angeben';
            }

            /* ##################################### */
            /* ######### ROLES VALIDATION ########## */

            $r_responsible = $this->processRole($data['roles_responsible']);
            $r_concerned = $this->processRole($data['roles_concerned']);
            $r_contributing = $this->processRole($data['roles_contributing']);
            $r_information = $this->processRole($data['roles_information']);

            // Get the stored task from the database
            $storedTask = $this->taskModel->getTaskById($id);
            // Get the user ids of the roles and store them
            $storedData = [
                'roles_responsible' => $storedTask->roles_responsible,
                'roles_concerned' => $storedTask->roles_concerned,
                'roles_contributing' => $storedTask->roles_contributing,
                'roles_information' => $storedTask->roles_information
            ];

            function compareUsersInRole($existingRoleData, $newRoleData)
            {
                // Split the strings of ids into arrays of ids
                $existingUserIds = explode(',', $existingRoleData);
                $newUserIds = explode(',', $newRoleData);
                // Init array for output
                $toBeNotified = [];
                // Loop through the new users and filter out the new ones
                foreach ($newUserIds as $userId) {
                    if (!in_array($userId, $existingUserIds)) {
                        // Store the new users in the $toBeNotified array
                        $toBeNotified[] = $userId;
                    }
                }
                // Output the array
                return $toBeNotified;
            }

            $notify = [
                'roles_responsible' => compareUsersInRole($storedData['roles_responsible'], $data['roles_responsible']),
                'roles_concerned' => compareUsersInRole($storedData['roles_concerned'], $data['roles_concerned']),
                'roles_contributing' => compareUsersInRole($storedData['roles_contributing'], $data['roles_contributing']),
                'roles_information' => compareUsersInRole($storedData['roles_information'], $data['roles_information'])
            ];

            /* ############################################################### */
            /* ##################### REQUIRED KNOWLEDGE ###################### */

            $knowledgeData = $this->processRequiredKnowledge($data['required_knowledge']);
            $knowledgeFoldersData = $this->processRequiredKnowledgeData($data['required_knowledge_folders']);

            // Update the UI data
            $data['required_knowledge_UI'] = $knowledgeData;
            $data['required_knowledge_folders_UI'] = $knowledgeFoldersData;

            /* ############################################################### */
            /* ##################### REQUIRED RESOURCES ###################### */

            $resourcesData = $this->processRequiredResources($data['required_resources']);
            $resourceFoldersData = $this->processRequiredResourcesData($data['required_resources_folders']);

            // Update the UI data
            $data['required_resources_UI'] = $resourcesData;
            $data['required_resources_folders_UI'] = $resourceFoldersData;

            /* ############################################################ */
            /* ##################### CHECK INTERVALS ###################### */

            // Check if the activate_intervals Checkbox is set
            if (isset($_POST['activate_intervals'])) {
                // Validated
                // Get the post data
                $data['interval_count'] = trim($_POST['interval_count']);
                $data['interval_format'] = trim($_POST['interval_format']);
                // Set the data variables
                $data['reference_day'] = trim($_POST['reference_day']);
                $data['reference_month'] = trim($_POST['reference_month']);
                $data['reference_year'] = trim($_POST['reference_year']);

                $day = trim($_POST['reference_day']);
                $month = trim($_POST['reference_month']);
                $year = trim($_POST['reference_year']);

                // Validated - Check if intervall fields are empty
                if (empty($data['interval_count']) && $data['interval_format'] == 'empty') {
                    // Add the appropriate error message to the corresponding variable
                    $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl und ein Intervallformat aus.';
                } elseif (empty($data['interval_count'])) {
                    // Add the appropriate error message to the corresponding variable
                    $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallzahl aus.';
                } elseif ($data['interval_format'] == 'empty') {
                    // Add the appropriate error message to the corresponding variable
                    $data['check_intervals_err'] = 'Bitte wählen Sie eine Intervallformat aus.';
                } elseif (empty($day) || empty($month) || empty($year)) {
                    // Add the error message
                    $data['reference_date_err'] = 'Bitte füllen Sie alle Felder des Referenzdatums aus.';
                } else {
                    // Construct the reference date
                    $data['reference_date'] = $year . '-' . $month . '-' . $day;
                    // Calculate the checkdate
                    // Get the reference date from the DB
                    $timestamp = strtotime($data['reference_date']);
                    // Calculate the next check date
                    // source: helpers/date_time_helper.php
                    $checkdate = calculateCheckdate($timestamp, $data['interval_count'], $data['interval_format']);

                    $data['check_date'] = $checkdate['year'] . '-' . $checkdate['month'] . '-' . $checkdate['day'];
                    // Construct the check interval text value
                    $data['check_intervals'] = $data['interval_count'] . ' ' . $data['interval_format'];
                }
            } else {
                // Invalid
                $data['check_intervals'] = 'Kein Check Intervall';
                $data['activate_intervals'] = false;
                // Set the variables to NULL
                $data['reference_date'] = null;
                $data['interval_count'] = null;
                $data['interval_format'] = null;
                $data['check_date'] = null;
                $data['check_intervals_err'] = '';
            }


            /* #################################################################### */
            /* ############################# REMINDERS ############################ */

            // Check if the reminders and check intervals both have been activated
            if (isset($_POST['activate_reminders']) && isset($_POST['activate_intervals'])) {
                // ACTIVE
                // Fill the variables with the respective POST values
                $data['reminders_activated'] = true;

                // If the value is 0 set it to NULL
                // Otherwise set it to the value
                if (trim($_POST['remind_before']) == 0) {
                    $data['remind_before'] = null;
                } else {
                    $data['remind_before'] = trim($_POST['remind_before']);
                }

                // If the value is 0 set it to NULL
                // Otherwise set it to the value
                if (trim($_POST['remind_at']) != 'activated') {
                    $data['remind_at'] = null;
                } else {
                    $data['remind_at'] = trim($_POST['remind_at']);
                }

                // If the value is 0 set it to NULL
                // Otherwise set it to the value
                if (trim($_POST['remind_after']) == 0) {
                    $data['remind_after'] = null;
                } else {
                    $data['remind_after'] = trim($_POST['remind_after']);
                }

                // Check if all fields are empty or unset
                if (empty($data['remind_before']) && empty($data['remind_at']) && empty($data['remind_after'])) {
                    // ALL EMPTY
                    $data['reminders_err'] = 'Bitte wählen Sie mindestens eine der drei Erinnerungsmöglichkeiten aus oder deaktivieren Sie die Erinnerungsfunktion.';
                }
            } elseif (isset($_POST['activate_reminders']) && !isset($_POST['activate_intervals'])) {
                // ERROR: CHECK INTERVALS NOT ACTIVATED
                $data['reminders_err'] = 'Die Erinnerungsfunktion kann nur aktiviert werden, sofern zunächst ein Check-Intervall definiert wurde.';
            } else {
                // INACTIVE
                $data['reminders_activated'] = false;
                $data['remind_before'] = null;
                $data['remind_at'] = null;
                $data['remind_after'] = null;
            }

            /* ##################################### */
            /* ################ TAGS ############### */

            $data['taglist'] = $data['tags'];
            if (!empty($data['tags'])) {
                // Validated
                // Create an array of tags
                $data['tags_array'] = explode(',', $data['tags']);
            } else {
                // Invalid
                // Set the tags array to empty
                $data['tags_array'] = [];
            }

            /* ######################################### */
            /* ########### FLEXIBLE QUESTIONS ########## */

            // Make sure there is a flexible question
            if (isset($_POST['flexible_questions'])) {
                $data['flexible_questions'] = $_POST['flexible_questions'];

                // Explode the flexible_questions string
                $questionsArr = explode('||', $_POST['flexible_questions']);

                // Init variable for later
                $questionData = [];

                foreach ($questionsArr as $questionObj) {
                    // Make sure the current item is not empty
                    if (!empty($questionObj)) {
                        // Explode it into an array
                        $parts = explode('/!/', $questionObj);
                        // Add each part to a correspondig variable
                        $question = $parts[0];
                        $yesNo = substr($parts[1], 3);
                        $notify = substr($parts[2], 2);
                        $remind = substr($parts[3], 2);

                        // Prepare $answers_UI for output
                        $answers_UI = '';

                        if ($yesNo == 'true') {
                            $answers_UI .= 'Ja/Nein, ';
                        }

                        if ($notify == 'true') {
                            $answers_UI .= 'Mitarbeiterbenachrichtigung, ';
                        }

                        if ($remind == 'true') {
                            $answers_UI .= 'Erinnerung, ';
                        }
                        // Remove the space and the comma
                        $answers_UI = substr($answers_UI, 0, -2);

                        $questionData[] = array(
                            'question' => $question,
                            'yesNo' => $yesNo,
                            'notify' => $notify,
                            'remind' => $remind,
                            'answers_UI' => $answers_UI,
                            'string_UI' => $questionObj
                        );
                    }
                }
                // Store questionData for UI
                $data['flexible_questions_UI'] = $questionData;
            }

            //die(var_dump($questionData));

            // Make sure no errors
            if (empty($data['name_err']) &&
                empty($data['input_err']) &&
                empty($data['input_source_err']) &&
                empty($data['output_err']) &&
                empty($data['output_target_err']) &&
                empty($data['category_err']) &&
                empty($data['risk_err']) &&
                empty($data['status_err']) &&
                empty($data['required_knowledge_err']) &&
                empty($data['roles_err']) &&
                empty($data['check_intervals_err']) &&
                empty($data['reminders_err']) &&
                empty($data['reference_date_err']) &&
                empty($data['required_resources_err']) &&
                empty($data['tags_err'])) {
                // Validated
                if ($this->taskModel->updateTask($data)) {
                    // TASKUPDATE: SUCCESSFULL
                    // SKIP ROLE ASSIGNMENTS FOR PROCESSTASKS

                    // Remove previous qualifications
                    $this->qualificationModel->removeTaskQualification($id);

                    // Remove previous isos
                    $this->isoModel->removeTaskIso($id);

                    // Add qualifications
                    if (!is_null($data['qualifications'])) {
                        foreach ($data['qualifications'] as $qualification) {
                            $this->qualificationModel->assignTaskQualification($qualification, $id);
                        }
                    }

                    // Add isos
                    if (!is_null($data['isos'])) {
                        foreach ($data['isos'] as $iso) {
                            $this->isoModel->assignTaskIso($iso, $id);
                        }
                    }

                    if ($task->is_singletask) {
                        // IS SINGLETASK
                        // ASSIGN THE TASK TO THE USERS
                        // role: responsible
                        $this->assignTaskToUsers($notify['roles_responsible'], 'responsible', $id);
                        // role: concerned
                        $this->assignTaskToUsers($notify['roles_concerned'], 'concerned', $id);
                        // role: contributing
                        $this->assignTaskToUsers($notify['roles_contributing'], 'contributing', $id);
                        // role: information
                        $this->assignTaskToUsers($notify['roles_information'], 'information', $id);
                    }

                    // Remove all questions pertaining to this task
                    $this->questionModel->removeQuestionsByTaskId($id);
                    // Submit all of them
                    // Loop through all questions
                    foreach ($questionData as $questItem) {
                        // Prepare data
                        $data = [
                            'question' => $questItem['question'],
                            'yesNo' => $questItem['yesNo'],
                            'notify' => $questItem['notify'],
                            'remind' => $questItem['remind'],
                            'task_id' => $id,
                            'method_id' => null,
                            'document_id' => null
                        ];
                        // Submit the question to the DB
                        $this->questionModel->addFlexibleQuestion($data);
                    }

                    prepareFlash('task_message', 'Die Aufgabe wurde aktualisiert');
                    // Redirect to the show task view
                    redirect('tasks/show/' . $id);
                } else {
                    // taskupdate unsuccessful
                    die('Ein Fehler ist aufgetreten. Bitte kontaktieren Sie den Entwickler');
                }
            } else {
                /* ######## ROLES ######### */

                // Update the fields for the UI
                $data['roles_responsible_id'] = $data['roles_responsible'];
                $data['roles_responsible'] = $r_responsible;
                // Update the fields for the UI
                $data['roles_concerned_id'] = $data['roles_concerned'];
                $data['roles_concerned'] = $r_concerned;
                // Update the fields for the UI
                $data['roles_contributing_id'] = $data['roles_contributing'];
                $data['roles_contributing'] = $r_contributing;
                // Update the fields for the UI
                $data['roles_information_id'] = $data['roles_information'];
                $data['roles_information'] = $r_information;

                // Load the view with Errors
                $this->view('tasks/edit', $data);

                //die(var_dump($data));
            }
        } else {
            // Otherwise load the view, in order for the user to be able to edit the task


            /* ############### ROLES ############## */
            // string to array of objects conversion

            $r_responsible = $this->processRole($task->roles_responsible);
            $r_concerned = $this->processRole($task->roles_concerned);
            $r_contributing = $this->processRole($task->roles_contributing);
            $r_information = $this->processRole($task->roles_information);

            /* ##################### REQUIRED KNOWLEDGE ###################### */

            $knowledgeData = $this->processRequiredKnowledge($task->required_knowledge);
            $knowledgeFoldersData = $this->processRequiredKnowledgeData($task->required_knowledge_folders);

            /* ##################### REQUIRED RESOURCES ##################### */

            $resourcesData = $this->processRequiredResources($task->required_resources);
            $resourceFoldersData = $this->processRequiredResourcesData($task->required_resources_folders);


            /* ########### CHECK INTERVALS ########## */

            // Check if a check_interval is set
            if ($task->check_intervals == 'Kein Check Intervall' || empty($task->check_intervals)) {
                // NOT SET
                // create $activate_intervals var and set it to false
                $activate_intervals = false;
            } else {
                // SET
                // create $activate_intervals var and set it to true
                $activate_intervals = true;
            }

            /* ##################### REFERENCE DATE ####################### */

            // Make sure there is a reference date before procceeding
            if (!empty($task->reference_date)) {
                // Get the date and convert it
                // source: helpers/date_time_helper.php
                $dateArray = convert_date_time($task->reference_date);
                // Prepare variables for output
                $reference_day = $dateArray['day'];
                $reference_year = $dateArray['year'];
                // Get the month as a number
                $timestamp = strtotime($task->reference_date);
                $reference_month = date("n", $timestamp);
            } else {
                // NO REFERENCE DATE
                // Prepare empty variables for output
                $reference_day = '';
                $reference_year = '';
                $reference_month = '';
            }

            /* ################## REMINDERS ##################### */

            $remind_before = $task->remind_before;
            $remind_at = $task->remind_at;
            $remind_after = $task->remind_after;
            $reminders_activated = false;


            if ($remind_before != null || $remind_at != null || $remind_after != null) {
                $reminders_activated = true;
            }

            /* ################## FLEXIBLE QUESTIONS ##################### */

            $questions = $this->questionModel->getQuestionsByTaskId($id);
            // Init variable
            $questionData = [];
            $questionsValue = '';

            if (!empty($questions)) {
                foreach ($questions as $questItem) {
                    // Prepare variables for output
                    $answers_UI = '';
                    $string_UI = $questItem->question;

                    // Fill them
                    if ($questItem->yes_no == 'true') {
                        $answers_UI .= 'Ja/Nein, ';
                        $string_UI .= '/!/YN:true';
                    } else {
                        $string_UI .= '/!/YN:false';
                    }

                    if ($questItem->notify == 'true') {
                        $answers_UI .= 'Mitarbeiterbenachrichtigung, ';
                        $string_UI .= '/!/N:true';
                    } else {
                        $string_UI .= '/!/N:false';
                    }

                    if ($questItem->remind == 'true') {
                        $answers_UI .= 'Erinnerung, ';
                        $string_UI .= '/!/R:true';
                    } else {
                        $string_UI .= '/!/R:false';
                    }

                    // Remove the space and the comma
                    $answers_UI = substr($answers_UI, 0, -2);

                    // Fill the questionData array with values
                    $questionData[] = array(
                        'question' => $questItem->question,
                        'answers_UI' => $answers_UI,
                        'string_UI' => $string_UI
                    );

                    $questionsValue .= $string_UI . '||';
                }
            }


            $data = [
                'bodyClass' => 'tasks editTask',
                'privUser' => $this->privUser,
                'id' => $id,
                'user_id' => $task->user_id,
                'name' => $task->name,
                'description' => $task->description,
                'department' => $task->department,
                'taskQualifications' => $task->qualifications,
                'taskIsos' => $task->isos,
                'qualificationList' => $qualifications,
                'isoList' => $isos,
                //
                'organs' => $organs,
                //
                'input' => $task->input,
                'input_source' => $task->input_source,
                'output' => $task->output,
                'output_target' => $task->output_target,
                'category' => $task->category,
                'risk' => $task->risk,
                'status' => $task->status,
                // Required Knowledge
                'required_knowledge_text' => '',
                'required_knowledge' => $task->required_knowledge,
                'required_knowledge_folders' => $task->required_knowledge_folders,
                'required_knowledge_UI' => $knowledgeData,
                'required_knowledge_folders_UI' => $knowledgeFoldersData,
                // Roles
                'roles_responsible' => $r_responsible,
                'roles_responsible_id' => $task->roles_responsible,
                'roles_concerned' => $r_concerned,
                'roles_concerned_id' => $task->roles_concerned,
                'roles_contributing' => $r_contributing,
                'roles_contributing_id' => $task->roles_contributing,
                'roles_information' => $r_information,
                'roles_information_id' => $task->roles_information,
                //
                // Check Intervals
                'check_intervals' => '',
                'reference_date' => '',
                'interval_count' => $task->interval_count,
                'interval_format' => $task->interval_format,
                'activate_intervals' => $activate_intervals,
                'reference_day' => $reference_day,
                'reference_month' => $reference_month,
                'reference_year' => $reference_year,
                // Check Reminders
                'reminders_activated' => $reminders_activated,
                'remind_before' => $remind_before,
                'remind_at' => $remind_at,
                'remind_after' => $remind_after,
                // Required Resources
                'required_resources_text' => $task->required_resources_text,
                'required_resources' => $task->required_resources,
                'required_resources_folders' => $task->required_resources_folders,
                'required_resources_UI' => $resourcesData,
                'required_resources_folders_UI' => $resourceFoldersData,
                //
                'existing_compentencies' => $task->existing_compentencies,
                'required_education' => $task->required_education,
                'taglist' => $task->tags,
                'tags_array' => explode(',', $task->tags),
                'flexible_questions' => $questionsValue,
                'flexible_questions_UI' => $questionData,
                'is_singletask' => $task->is_singletask,
                // ##########################
                // ######## Errors ##########
                'name_err' => '',
                // description is optional
                'department_err' => '',
                'input_err' => '',
                'input_source_err' => '',
                'output_err' => '',
                'output_target_err' => '',
                'category_err' => '',
                'risk_err' => '',
                'status_err' => '',
                'required_knowledge_err' => '',
                'roles_err' => '',
                'check_intervals_err' => '',
                'reminders_err' => '',
                'reference_date_err' => '',
                'required_resources_err' => '',
                'existing_compentencies_err' => '',
                'required_education_err' => '',
                'tags_err' => ''
            ];

            //die( var_dump( $r_responsible ));

            // Load view
            $this->view('tasks/edit', $data);
        }
    }

    /*************************
     * METHOD: DELETE TASK
     *************************/

    public function delete($id)
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            /////////////////////////////////////
            /////// Validate Privileges /////////
            /////////////////////////////////////

            // Validate delete privilege
            if ($this->privUser->hasPrivilege('3') != true) {
                prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt diese Aufgabe zu löschen', 'alert alert-danger');
                redirect('tasks/show/' . $id);
            }

            //////////////////////////////////////
            //////////////////////////////////////

            // Get the task
            $task = $this->taskModel->getTaskById($id);

            // CHECK IF THIS TASK IS A PROCESSTASK
            if ($task->is_singletask == 'false') {
                // IS PROCESS TASK
                redirect('tasks/show/' . $id);
            } else {
                // IS A SINGLE TASK
                // Archive the task and check for success
                if ($this->taskModel->archiveTask($id)) {
                    // SUCCESS
                    // Remove all connected notifications
                    $this->reminderModel->deactivateRemindersByTaskId($id);
                    $this->taskModel->removeUnacceptedAssignmentsByTaskId($id);
                    $this->taskModel->removeAcceptedTasksByTaskId($id);

                    prepareFlash('task_message', 'Die Aufgabe wurde ins Archiv verschoben.');
                    redirect('tasks');
                } else {
                    // ERROR
                    die('Etwas ist schief gelaufen. Bitte kontakieren Sie den Entwickler');
                }
            }
        } else {
            redirect('tasks');
        }
    }

    public function archiveTaskAndRemoveReminders($id)
    {
        $errors = [];

        if ($this->taskModel->archiveTask($id) != true) {
            $errors[] = "Die Aufgabe A: $id konnte nicht archiviert werden.";
        }

        if ($this->reminderModel->removeRemindersByTaskId($id) != true) {
            $errors[] = "Die Erinnerungen der Aufgabe A: $id konnte nicht deaktiviert werden.";
        }


        if (!empty($errors)) {
            return false;
        } else {
            return true;
        }
    }
}
