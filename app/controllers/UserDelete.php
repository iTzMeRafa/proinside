<?php


class UserDelete extends Controller
{
    protected $privUser;
    private $users;

    public function __construct()
    {
        /* authentication check */
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        $this->users = $this->model('User');

        /* Get current Users privilege */
        $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

        if ($this->privUser->hasPrivilege('21') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt ein Mitarbeiterprofil zu entfernen', 'alert alert-danger');
            redirect('users/dashboard');
        }
    }

    /***
     * Deletes a user by user id
     * @return view
     */
    public function index($userId)
    {
        if ($this->users->deleteUser($userId)) {
            prepareFlash('user_message', 'Das Mitarbeiterprofil wurde erfolgreich entfernt', 'alert alert-success');
            redirect('users/dashboard');
        }
    }
}
