<?php

class Users extends Controller
{
    private $userModel;
    private $taskModel;
    private $methodModel;
    private $dataModel;
    private $organModel;
    private $permModel;
    private $reminderModel;
    private $qualificationModel;

    public function __construct()
    {

        // Load the required models
        $this->userModel = $this->model('User');
        $this->taskModel = $this->model('Task');
        $this->methodModel = $this->model('Method');
        $this->dataModel = $this->model('Data');
        $this->organModel = $this->model('Organ');
        $this->permModel = $this->model('Permission');
        $this->reminderModel = $this->model('Reminder');
        $this->qualificationModel = $this->model('QualificationModel');

        if (isLoggedIn()) {
            // Get the current user's permissions
            $this->privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);
        }
    }


    /**
     * Handles the URL: /users/
     * @return {view}
     */
    public function index()
    {

        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Get users
        $users = $this->userModel->getUsers();

        $data = [
            'bodyClass' => 'users',
            'privUser' => $this->privUser,
            'users' => $users
        ];

        // Load view
        $this->view('users/index', $data);
    }

    /**
     * Handles the URL: /register/
     * @return {view}
     */
    public function register()
    {
        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Validate Privilege
        if ($this->privUser->hasPrivilege('11') != true || $this->privUser->hasPrivilege('21') != true) {
            prepareFlash('no_permisssion', 'Sie sind nicht dazu berechtigt neue Nutzer zu registrieren', 'alert alert-danger');
            redirect('users/dashboard');
        }

        // Check for POST Request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Process form

            // Sanatize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $randomPassword = randomPassword();
            // Init data
            // The trim function trims off the whitespace
            $data = [
                'bodyClass' => 'users users-register',
                'privUser' => $this->privUser,
                'firstname' => trim($_POST['firstname']),
                'lastname' => trim($_POST['lastname']),
                'email' => trim($_POST['email']),
                'password' => $randomPassword,
                'firstname_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'role' => trim($_POST['userPermissionRole']),
            ];

            // Validate Email
            if (empty($data['email'])) {
                $data['email_err'] = 'Bitte gültige E-Mail Adresse eingeben';
            } else {
                // Check email
                if ($this->userModel->findUserByEmail($data['email'])) {
                    $data['email_err'] = 'Diese E-Mail Adresse wird bereits verwendet';
                }
            }

            // Validate Firstname
            if (empty($data['firstname'])) {
                $data['firstname_err'] = 'Bitte Vornamen eingeben';
            }
            // Validate Lastname
            if (empty($data['lastname'])) {
                $data['lastname_err'] = 'Bitte Nachnamen eingeben';
            }

            // Make sure errors are empty
            if (empty($data['email_err']) && empty($data['name_err'])) {
                // Validated

                // Hash Password
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                // Register User and get his id on successful registration
                $userId = $this->userModel->register($data);
                // Check if the registration was successful
                if ($userId != false) {
                    // SUCCESS
                    // Give the user the default role
                    $this->permModel->giveUserDefaultRole($userId, $data['role']);

                    // Send User Register Email with data
                    if (Mailer::sendRegisterMail($data['email'], $data['firstname'], $data['lastname'], $randomPassword)) {
                        prepareFlash('register_email_success', 'Dem Nutzer wurden erfolgreich die Anmeldedaten zugesendet.');
                    } else {
                        prepareFlash('register_email_failure', 'Dem User konnte keine E-Mail zugestellt werden. Bitte versuchen Sie es erneut.');
                    }

                    // Prepare the flash message, which will be displayed in the login view after the redirect
                    // Helper Function - source: "helpers/session_helper.php"
                    prepareFlash('register_success', 'Der Nutzer wurde erfolgreich registriert.');
                    // Redirect to the login view
                    // Helper Function - source: "helpers/url_helper.php"
                    redirect('users/index');
                } else {
                    // ERRROR
                    die('Something went wrong. Please contact the developer.');
                }
            } else {
                // Load view with errors
                $this->view('users/register', $data);
            }
        } else {

            // Get all roles
            $roles = $this->permModel->getRoles();

            // Init data
            $data = [
                'bodyClass' => 'users users-register',
                'privUser' => $this->privUser,
                'firstname' => '',
                'lastname' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'firstname_err' => '',
                'lastname_err' => '',
                'email_err' => '',
                'roles' => $roles,
            ];

            // Load view
            $this->view('users/register', $data);
        }
    }

    /********************
     * METHOD: LOGIN
     ********************/

    public function login()
    {
        // Check for POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Process form

            // Sanatize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Init data
            // The trim function trims off the whitespace
            $data = [
                'bodyClass' => 'users users-login',
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => ''
            ];

            // Validate Email
            if (empty($data['email'])) {
                $data['email_err'] = 'Bitte gültige E-Mail Adresse eingeben';
            }

            // Validate Password
            if (empty($data['password'])) {
                $data['password_err'] = 'Bitte Passwort eingeben';
            }

            // Check if the user/email does exist
            if ($this->userModel->findUserByEmail($data['email']) == false) {
                $data['email_err'] = 'Die eingegeben E-Mail Adresse existiert nicht';
            }

            // Make sure errors are empty
            if (empty($data['email_err']) && empty($data['password_err'])) {
                // Validated
                // Check and set logged in user
                $loggedInUser = $this->userModel->login($data['email'], $data['password']);

                // Check if loggedInUser is not false
                if ($loggedInUser) {
                    // Create Session
                    $this->createUserSession($loggedInUser);
                } else {
                    $data['password_err'] = 'Das eingegebene Passwort is inkorrekt.';

                    // Load the view
                    $this->view('users/login', $data);
                }
            } else {
                // Load view with errors
                $this->view('users/login', $data);
            }
        } else {
            // Init data
            $data = [
                'bodyClass' => 'users users-login',
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''
            ];

            // Load view
            $this->view('users/login', $data);
        }
    }

    /********************
     * METHOD: FORGOTPASS
     ********************/

    public function forgotPass()
    {
        // Check for POST
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Process form

            // Sanatize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Init data
            // The trim function trims off the whitespace
            $data = [
                'bodyClass' => 'users users-forgotpass',
                'privUser' => $this->privUser,
                'email' => trim($_POST['email']),
                'email_err' => '',
                'id' => '',
                'passwordcode' => ''
            ];

            // Validate Email
            if (empty($data['email'])) {
                $data['email_err'] = 'Bitte gültige E-Mail Adresse eingeben';
            }

            // Check if the user/email does exist
            if ($this->userModel->findUserByEmail($data['email']) == false) {
                $data['email_err'] = 'Die eingegeben E-Mail Adresse gehört zu keinem registrierten Account';
            }

            // Make sure errors are empty
            if (empty($data['email_err'])) {
                // Validated
                // Get the user
                $user = $this->userModel->getUserByEmail($data['email']);

                // Generate a unique passwordcode from binary to hex
                $bytes = openssl_random_pseudo_bytes(16);
                $passwordcode = bin2hex($bytes);

                // Prepare the data for the database
                $data = [
                    'id' => $user->id,
                    'passwordcode' => sha1($passwordcode) // hash the passwordcode before submit
                ];

                // Store the passwordcode and a autogenerated timestamp in the database
                if ($this->userModel->updateUserPasswordcode($data) !== true) {
                    die('There was an error updating your unique Passworcode. Please try again or contact the developer.');
                }

                // Send resetcode to email
                if (Mailer::sendForgotPasswordMail($user->email, $user->firstname, $user->lastname, $user->id, $passwordcode)) {
                    prepareFlash('forgotpass_success', 'Ein Link, um Ihr Passwort zurückzusetzen wurde an Ihre E-Mail Adresse gesendet.');
                } else {
                    prepareFlash('forgotpass_failure', 'Ein Link, um Ihr Passwort zurückzusetzen konnte nicht an Ihre E-Mail Adresse versendet werden.');
                }

                // Redirect to the forgotpass view
                // Helper Function - source: "helpers/url_helper.php"
                redirect('users/login');
            } else {
                // Load view with errors
                $this->view('users/forgotpass', $data);
            }
        } else {
            // Init data
            $data = [
                'bodyClass' => 'forgotpass',
                'privUser' => $this->privUser,
                'email' => '',
                'email_err' => '',
            ];

            // Load view
            $this->view('users/forgotpass', $data);
        }
    }

    /********************
     * METHOD: RESETPASS
     ********************/
    public function resetPass($userId, $passwordcode)
    {

        // Get the user
        $user = $this->userModel->getUserById($userId);

        // Init data
        $data = [
            'bodyClass' => 'resetpass',
            'privUser' => $this->privUser,
            'id' => $user->id,
            'passwordcode' => $passwordcode,
            'password' => '',
            'confirm_password' => '',
            'password_err' => '',
            'confirm_password_err' => ''
        ];

        // Check if there was a user found and if the user has a passwordcode
        if ($user === null || $user->passwordcode === null) {
            $data['user_err'] = 'Es wurde keine passender Benutzer gefunden.';
            // Prepare the flash message, which will be displayed in the view after the redirect
            // Helper Function - source: "helpers/session_helper.php"
            prepareFlash('resetpass_error', 'Es wurde kein passender Benutzer gefunden.', 'alert alert-danger');
            // Redirect to the forgotpass view
            // Helper Function - source: "helpers/url_helper.php"
            redirect('users/forgotpass');
        }

        // Check if the users forgotPass request is older than 24hours
        if ($user->passwordcode_time === null || strtotime($user->passwordcode_time) < (time() - 24 * 3600)) {
            // Prepare the flash message, which will be displayed in the view after the redirect
            // Helper Function - source: "helpers/session_helper.php"
            prepareFlash('resetpass_error', 'Ihre Anfrage um ihr Passwort zurückzusetzen ist älter als 24 Stunden. Bitte stellen Sie eine erneute Anfrage.', 'alert alert-danger');
            // Redirect to the forgotpass view
            // Helper Function - source: "helpers/url_helper.php"
            redirect('users/forgotpass');
        }

        // Validate the users passwordcode
        if (sha1($passwordcode) != $user->passwordcode) {
            // Prepare the flash message, which will be displayed in the view after the redirect
            // Helper Function - source: "helpers/session_helper.php"
            prepareFlash('resetpass_error', 'Der übergebene Code war ungültig. Bitte stellen Sie sicher, dass Sie den genauen Link in der URL aufgerufen haben.', 'alert alert-danger');
            // Redirect to the forgotpass view
            // Helper Function - source: "helpers/url_helper.php"
            redirect('users/forgotpass');
        }

        // Validated - the user is now allowed to change his password
        // Check for post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanatize POST data
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data['password'] = trim($_POST['password']);
            $data['confirm_password'] = trim($_POST['confirm_password']);

            // Validate Password
            if (empty($data['password'])) {
                $data['password_err'] = 'Bitte Passwort eingeben';
            } elseif (strlen($data['password']) < 6) {
                $data['password_err'] = 'Das Passwort muss mindestens 6 Zeichen lang sein';
            }

            // Validate Confirm Password
            if (empty($data['confirm_password'])) {
                $data['confirm_password_err'] = 'Bitte Passwort bestätigen';
            } else {
                if ($data['password'] != $data['confirm_password']) {
                    $data['confirm_password_err'] = 'Die Passwörter stimmen nicht überein';
                }
            }

            // Make sure all errors are empty
            if (empty($data['password_err']) && empty($data['confirm_password_err'])) {
                // Validated

                // Hash Password
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                // Update the user
                if ($this->userModel->updateUserPassword($data)) {
                    // Prepare the flash message, which will be displayed in the login view after the redirect
                    // Helper Function - source: "helpers/session_helper.php"
                    prepareFlash('resetpass_success', 'Ihr Passwort wurde erfolgreich geändert.');
                    // Redirect to the login view
                    // Helper Function - source: "helpers/url_helper.php"
                    redirect('users/login');
                }
            } else {
                // Load view with errors
                $this->view('users/resetpass', $data);
            }
        } else {
            // Load view
            $this->view('users/resetpass', $data);
        }
    }

    /********************
     * METHOD: SHOW
     ********************/

    public function show($id)
    {
        // Get the user
        $user = $this->userModel->getUserById($id);

        // Get the user's roles
        $privUser = PrivilegedUser::getByEmail($user->email);
        if (!empty($privUser)) {
            $roles = $privUser->roles;
        } else {
            $roles = [];
        }

        /*
         * Get accepted tasks
         */
        $acceptedTasks = $this->taskModel->getAcceptedTasksByUserId($id);
        $tasks = [];
        foreach ($acceptedTasks as $task) {
            // Check if the current task_id is not already tasks(array) as a key
            if (empty($tasks[$task->task_id])) {
                $tasks[$task->task_id] = $this->taskModel->getTaskById($task->task_id);
            }
            if ($tasks[$task->task_id] == false) {
                unset($tasks[$task->task_id]);
            }
        }

        /*
         * Get unaccepted tasks
         */
        $unacceptedTaskAssignments = $this->taskModel->getUnacceptedAssignmentsByUserId($id);
        if (empty($unacceptedTaskAssignments)) {
            $unacceptedTasks = false;
        } else {
            foreach ($unacceptedTaskAssignments as $assignment) {
                $task = $this->taskModel->getTaskById($assignment->task_id);
                $date_time = convert_date_time($assignment->assigned_at);

                $unacceptedTasks[] = [
                    'task' => $task,
                    'role' => translateRoleLanguage($assignment->role),
                    'date_time' => $date_time,
                ];
            }
        }

        /*
         * Get accepted methods
         */
        $acceptedMethods = $this->methodModel->getAcceptedMethodsByUserId($id);

        $methods = [];
        foreach ($acceptedMethods as $method) {
            if (empty($methods[$method->method_id])) {
                $methods[$method->method_id] = $this->methodModel->getMethodById($method->method_id);
                $methods[$method->method_id]->id = $method->method_id;
            }
            if ($methods[$method->method_id] == false) {
                unset($methods[$method->method_id]);
            }
        }
        /*
         * Get unaccepted methods
         */
        $unacceptedMethodAssignments = $this->methodModel->getUnacceptedAssignmentsByUserId($id);
        if (empty($unacceptedMethodAssignments)) {
            $unacceptedMethods = false;
        } else {
            foreach ($unacceptedMethodAssignments as $assignment) {
                $method = $this->methodModel->getMethodById($assignment->method_id);
                $date_time = convert_date_time($assignment->assigned_at);

                $unacceptedMethods[] = [
                    'method' => $method,
                    'role' => translateRoleLanguage($assignment->role),
                    'date_time' => $date_time,
                ];
            }
        }

        $userQualifications = $this->userModel->getUsersQualifications($id);

        // Add qualifications to task
        foreach ($tasks as $task) {
            $qualifications     = [];
            $qualificationsArr  = explode(', ', $task->qualiName);
            $userID             = $id;

            // Get qualifications by id
            foreach ($qualificationsArr as $key => $qualification) {
                $fullQualification = $this->qualificationModel->getQualificationById($qualification);
                if (is_object($fullQualification)) {
                    $fullQualification->assigned_at = $this->qualificationModel->getAssignedAtUserQualification($userID, $qualification)->assigned_at;
                }
                $qualifications[$key] = $fullQualification;
            }

            $task->qualifications = $qualifications;
        }

        // Add qualifications to unacceptedTasks
        foreach ($unacceptedTasks as $unacceptedTask) {
            $qualifications     = [];
            $qualificationsArr  = explode(', ', $unacceptedTask['task']->qualiName);
            $userID             = $id;

            // Get qualifications by id
            foreach ($qualificationsArr as $key => $qualification) {
                $fullQualification = $this->qualificationModel->getQualificationById($qualification);
                if (is_object($fullQualification)) {
                    $fullQualification->assigned_at = $this->qualificationModel->getAssignedAtUserQualification($userID, $qualification)->assigned_at;
                }
                $qualifications[$key] = $fullQualification;
            }

            $unacceptedTask['task']->qualifications = $qualifications;
        }

        $data = [
            'bodyClass' => 'users showUser',
            'privUser' => $this->privUser,
            'user' => $user,
            'roles' => $roles,
            'tasks' => $tasks,
            'methods' => $methods,
            'unaccepted_tasks' => $unacceptedTasks,
            'unaccepted_methods' => $unacceptedMethods,
            'qualifications' => $userQualifications,
        ];

        // Load the view
        $this->view('users/show', $data);
    }


    /**
     * Gets called on route: /users/dashboard
     *
     * @param null $param
     * @return {view}
     */
    public function dashboard($param = null)
    {
        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        if ($param == "sendMail") {
            Mailer::sendMail();
        }

        // Get the user from the current Session
        $user = $this->userModel->getUserById($_SESSION['user_id']);

        $allUnapprovedData = $this->dataModel->getAllUnapprovedData();

        /* ################## GET LEADERS ################## */

        $leadOrgan = $this->organModel->getOrganById(1);

        $management_level = explode(',', $leadOrgan->management_level);
        $staff = explode(',', $leadOrgan->staff);

        $leadUsers = array_merge($management_level, $staff);
        $leadUsers = array_unique($leadUsers);

        /* ################################################ */
        /* ################### ESCALATION ################# */
        /* ################################################ */

        // Make sure this user is a leader
        if (in_array($_SESSION['user_id'], $leadUsers)) {
            // IS LEADER

            $activeTasks = $this->taskModel->getActiveTasks();
            $activeMethods = $this->methodModel->getActiveMethods();

            /**
             * Looping over Tasks
             */
            foreach ($activeTasks as $task) {
                if ($task->id == 147) {
                    //
                }

                // If this task has a checkdate assigned
                if (!empty($task->check_date)) {
                    $nextCheckdate = calculateNextCheckdate($task->check_date);

                    if ($nextCheckdate['isWhen'] == 'past') {

                        // CHECKDATE IN PAST
                        // ESCALATION
                        if ($nextCheckdate['diffInDays'] >= 5) {

                            // SEE IF ESCALATION-REMINDER EXISTS ALREADY

                            $escalation_data = [
                                'task_id' => $task->id,
                                'user_id' => $_SESSION['user_id'],
                                'check_date' => $task->check_date,
                                'reminder_type' => 'escalation'
                            ];

                            $escalation_reminder = $this->reminderModel->getTaskReminder($escalation_data);

                            // IF NO ESCALATION-REMINDER EXISTS
                            if (empty($escalation_reminder)) {
                                // ADD ESCALATION-REMINDER
                                $this->reminderModel->addTaskReminder($escalation_data);
                            }
                        }
                    }
                }
            }

            /**
             * Looping over Methods
             */
            foreach ($activeMethods as $method) {


                // If this task has a checkdate assigned
                if (!empty($method->check_date)) {
                    $nextCheckdate = calculateNextCheckdate($method->check_date);

                    if ($nextCheckdate['isWhen'] == 'past') {

                        // CHECKDATE IN PAST
                        // ESCALATION
                        if ($nextCheckdate['diffInDays'] >= 5) {

                            // SEE IF ESCALATION-REMINDER EXISTS ALREADY

                            $escalation_data = [
                                'method_id' => $method->id,
                                'user_id' => $_SESSION['user_id'],
                                'check_date' => $method->check_date,
                                'reminder_type' => 'escalation'
                            ];

                            $escalation_reminder = $this->reminderModel->getTaskReminder($escalation_data);

                            // IF NO ESCALATION-REMINDER EXISTS
                            if (empty($escalation_reminder)) {
                                // ADD ESCALATION-REMINDER
                                $this->reminderModel->addTaskReminder($escalation_data);
                            }
                        }
                    }
                }
            }

            /* ############# ESCALATION TASK REMINDERS ################ */

            $escalationsTasks = $this->reminderModel->getUnseenTaskEscalationsByUserId($_SESSION['user_id']);

            // GET CORRESPONDING TASK OBJECT AND AT IT TO THE ESCALATION
            foreach ($escalationsTasks as $escalationTask) {
                // Get the task object and add it to the reminder
                $task = $this->taskModel->getTaskById($escalationTask->task_id);
                $escalationTask->task = $task;

                $escalationTask->nextCheckdate = calculateNextCheckdate($escalationTask->check_date);
            }

            /* ############# ESCALATION MAßNAHME REMINDERS ################ */

            $escalationsMethods = $this->reminderModel->getUnseenMethodEscalationsByUserId($_SESSION['user_id']);

            // GET CORRESPONDING TASK OBJECT AND AT IT TO THE ESCALATION
            foreach ($escalationsMethods as $escalationMethod) {
                // Get the task object and add it to the reminder
                $method = $this->methodModel->getMethodById($escalationMethod->method_id);
                $escalationMethod->method = $method;

                $escalationMethod->nextCheckdate = calculateNextCheckdate($escalationMethod->check_date);
            }
        }


        /* #################################################### */
        /* ################### ACCEPTED TASKS ################# */
        /* #################################################### */

        // Get accepted tasks
        $acceptedTasks = $this->taskModel->getAcceptedTasksByUserId($_SESSION['user_id']);
        // Init task array for output
        $tasks = [];
        foreach ($acceptedTasks as $task) {
            // Check if the current task_id is not already tasks(array) as a key
            if (empty($tasks[$task->task_id])) {
                // add it
                $tasks[$task->task_id] = $this->taskModel->getTaskById($task->task_id);
            }

            // If the current task is empty or archived
            if ($tasks[$task->task_id] == false) {
                // EMPTY/ ARCHIVED
                // unset it
                unset($tasks[$task->task_id]);
            }
        }


        $tasks_AW = [];
        $tasks_SW = [];
        $tasks_ZW = [];
        $tasks_RW = [];


        foreach ($tasks as $task) {

            /////////////////////////////////
            /////////// REMINDERS ///////////

            // If this task has a checkdate assigned
            if (!empty($task->check_date)) {

                // Get the
                $remind_before = $task->remind_before;
                $remind_at = $task->remind_at;
                $remind_after = $task->remind_after;

                $nextCheckdate = calculateNextCheckdate($task->check_date);

                if ($nextCheckdate['isWhen'] == 'past') {
                    // CHECKDATE IN PAST

                    if (!empty($remind_after) && $nextCheckdate['diffInDays'] >= $remind_after) {
                        // SEE IF AFTER-REMINDER EXISTS ALREADY

                        $rAf_data = [
                            'task_id' => $task->id,
                            'user_id' => $_SESSION['user_id'],
                            'check_date' => $task->check_date,
                            'reminder_type' => 'after'
                        ];

                        $after_reminder = $this->reminderModel->getTaskReminder($rAf_data);

                        // IF NO AFTER-REMINDER EXISTS
                        if (empty($after_reminder)) {
                            // ADD AFTER-REMINDER
                            $this->reminderModel->addTaskReminder($rAf_data);
                        }
                    }
                } elseif ($nextCheckdate['isWhen'] == 'today') {
                    // CHECKDATE TODAY

                    if (!empty($remind_at)) {
                        // SEE IF AT-REMINDER EXISTS ALREADY

                        $rAt_data = [
                            'task_id' => $task->id,
                            'user_id' => $_SESSION['user_id'],
                            'check_date' => $task->check_date,
                            'reminder_type' => 'at'
                        ];

                        $at_reminder = $this->reminderModel->getTaskReminder($rAt_data);

                        // IF NO AT-REMINDER EXISTS
                        if (empty($at_reminder)) {
                            // ADD AT-REMINDER
                            $this->reminderModel->addTaskReminder($rAt_data);
                        }
                    }
                } else {
                    // CHECKDATE IN FUTURE

                    if (!empty($remind_before) && $nextCheckdate['diffInDays'] <= $remind_before) {
                        // SEE IF BEFORE-REMINDER EXISTS ALREADY

                        $rBe_data = [
                            'task_id' => $task->id,
                            'user_id' => $_SESSION['user_id'],
                            'check_date' => $task->check_date,
                            'reminder_type' => 'before'
                        ];

                        $before_reminder = $this->reminderModel->getTaskReminder($rBe_data);

                        // IF NO BEFORE-REMINDER EXISTS
                        if (empty($before_reminder)) {
                            // ADD BEFORE-REMINDER
                            $this->reminderModel->addTaskReminder($rBe_data);
                        }
                    }
                }
            }


            /////////////////////////////////
            //////////// MY TASKS ///////////

            // Get all the roles_strings and converte them into arrays
            $r_resposible = explode(',', $task->roles_responsible);
            $r_concerned = explode(',', $task->roles_concerned);
            $r_contributing = explode(',', $task->roles_contributing);
            $r_information = explode(',', $task->roles_information);

            // Init roles array
            $roles = [];

            // Check if the user id is in any role array
            // If so, add the UI role character to the array
            if (in_array($_SESSION['user_id'], $r_resposible)) {
                $roles['responsible'] = 'V';
            }
            if (in_array($_SESSION['user_id'], $r_concerned)) {
                $roles['concerned'] = 'Z';
            }
            if (in_array($_SESSION['user_id'], $r_contributing)) {
                $roles['contributing'] = 'M';
            }
            if (in_array($_SESSION['user_id'], $r_information)) {
                $roles['information'] = 'I';
            }

            // Turn the array into a string for the UI
            $task->roles = implode(', ', $roles);

            if ($task->category == 'Auftragswelt') {
                $tasks_AW[] = $task;
            }
            if ($task->category == 'Strukturwelt') {
                $tasks_SW[] = $task;
            }
            if ($task->category == 'Zielwelt') {
                $tasks_ZW[] = $task;
            }
            if ($task->category == 'Rechtswelt') {
                $tasks_RW[] = $task;
            }
            if ($task->category == 'Maßnahmenswelt') {
                $tasks_MW[] = $task;
            }
        }


        // GET ALL UNSEEN & ACTIVE REMINDERS
        $reminders = $this->reminderModel->getUnseenTaskRemindersByUserId($_SESSION['user_id']);

        foreach ($reminders as $reminder) {
            // Get the task object and add it to the reminder
            $task = $this->taskModel->getTaskById($reminder->task_id);
            $reminder->task = $task;

            $reminder->nextCheckdate = calculateNextCheckdate($reminder->check_date);
        }


        /* #################################################### */
        /* ################# ACCEPTED METHODS ################# */
        /* #################################################### */

        // Get accepted methods
        $acceptedMethods = $this->methodModel->getAcceptedMethodsByUserId($_SESSION['user_id']);
        // Init method array for output
        $methods = [];
        foreach ($acceptedMethods as $method) {
            // Check if the current task_id is not already tasks(array) as a key
            if (empty($methods[$method->method_id])) {
                // add it
                $methods[$method->method_id] = $this->methodModel->getMethodById($method->method_id);
            }

            // If the current task is empty or archived
            if ($methods[$method->method_id] == false) {
                // EMPTY/ ARCHIVED
                // unset it
                unset($methods[$method->method_id]);
            }
        }


        $methods_AW = [];
        $methods_SW = [];
        $methods_ZW = [];
        $methods_RW = [];


        foreach ($methods as $method) {

            /////////////////////////////////
            /////////// METHOD REMINDERS ///////////

            // If this task has a checkdate assigned
            if (!empty($method->check_date)) {

                // Get the
                $remind_before = $method->remind_before;
                $remind_at = $method->remind_at;
                $remind_after = $method->remind_after;

                $nextCheckdate = calculateNextCheckdate($method->check_date);

                if ($nextCheckdate['isWhen'] == 'past') {
                    // CHECKDATE IN PAST

                    if (!empty($remind_after) && $nextCheckdate['diffInDays'] >= $remind_after) {
                        // SEE IF AFTER-REMINDER EXISTS ALREADY

                        $rAf_data = [
                            'method_id' => $method->id,
                            'user_id' => $_SESSION['user_id'],
                            'check_date' => $method->check_date,
                            'reminder_type' => 'after'
                        ];

                        $after_reminder = $this->reminderModel->getTaskReminder($rAf_data);

                        // IF NO AFTER-REMINDER EXISTS
                        if (empty($after_reminder)) {
                            // ADD AFTER-REMINDER
                            $this->reminderModel->addTaskReminder($rAf_data);
                        }
                    }
                } elseif ($nextCheckdate['isWhen'] == 'today') {
                    // CHECKDATE TODAY

                    if (!empty($remind_at)) {
                        // SEE IF AT-REMINDER EXISTS ALREADY

                        $rAt_data = [
                            'method_id' => $method->id,
                            'user_id' => $_SESSION['user_id'],
                            'check_date' => $method->check_date,
                            'reminder_type' => 'at'
                        ];

                        $at_reminder = $this->reminderModel->getTaskReminder($rAt_data);

                        // IF NO AT-REMINDER EXISTS
                        if (empty($at_reminder)) {
                            // ADD AT-REMINDER
                            $this->reminderModel->addTaskReminder($rAt_data);
                        }
                    }
                } else {
                    // CHECKDATE IN FUTURE

                    if (!empty($remind_before) && $nextCheckdate['diffInDays'] <= $remind_before) {
                        // SEE IF BEFORE-REMINDER EXISTS ALREADY

                        $rBe_data = [
                            'method_id' => $method->id,
                            'user_id' => $_SESSION['user_id'],
                            'check_date' => $method->check_date,
                            'reminder_type' => 'before'
                        ];

                        $before_reminder = $this->reminderModel->getTaskReminder($rBe_data);

                        // IF NO BEFORE-REMINDER EXISTS
                        if (empty($before_reminder)) {
                            // ADD BEFORE-REMINDER
                            $this->reminderModel->addTaskReminder($rBe_data);
                        }
                    }
                }
            }


            /////////////////////////////////
            //////////// MY METHODS ///////////

            // Get all the roles_strings and converte them into arrays
            $r_resposible = explode(',', $method->roles_responsible);
            $r_concerned = explode(',', $method->roles_concerned);
            $r_contributing = explode(',', $method->roles_contributing);
            $r_information = explode(',', $method->roles_information);

            // Init roles array
            $roles = [];

            // Check if the user id is in any role array
            // If so, add the UI role character to the array
            if (in_array($_SESSION['user_id'], $r_resposible)) {
                $roles['responsible'] = 'V';
            }
            if (in_array($_SESSION['user_id'], $r_concerned)) {
                $roles['concerned'] = 'Z';
            }
            if (in_array($_SESSION['user_id'], $r_contributing)) {
                $roles['contributing'] = 'M';
            }
            if (in_array($_SESSION['user_id'], $r_information)) {
                $roles['information'] = 'I';
            }

            // Turn the array into a string for the UI
            $method->roles = implode(', ', $roles);

            if ($method->category == 'Auftragswelt') {
                $methods_AW[] = $method;
            }
            if ($method->category == 'Strukturwelt') {
                $methods_SW[] = $method;
            }
            if ($method->category == 'Zielwelt') {
                $methods_ZW[] = $method;
            }
            if ($method->category == 'Rechtswelt') {
                $methods_RW[] = $method;
            }
            if ($method->category == 'Maßnahmenswelt') {
                $methods_MW[] = $method;
            }
        }


        // GET ALL UNSEEN & ACTIVE REMINDERS
        $reminders = $this->reminderModel->getUnseenTaskRemindersByUserId($_SESSION['user_id']);

        foreach ($reminders as $reminder) {
            // Get the task object and add it to the reminder
            $method = $this->methodModel->getMethodById($reminder->method_id);
            $reminder->method = $method;

            $reminder->nextCheckdate = calculateNextCheckdate($reminder->check_date);
        }


        /* #################################### */
        /* ###### Get unaccepted tasks ######## */
        /* #################################### */

        $unacceptedTasks = $this->taskModel->getUnacceptedAssignmentsByUserId($user->id);

        // Make sure there are unaccepted tasks before proceeding
        if (!empty($unacceptedTasks)) {
            // Init variable
            $unacceptedTasksArray = [];
            // Loop through the array
            foreach ($unacceptedTasks as $task) {
                // Get the full task
                $taskObj = $this->taskModel->getTaskById($task->task_id);
                // Get the role name
                // source: helpers/role_helper.php
                $role = translateRoleLanguage($task->role);


                // Store the time of assignment
                $timestamp = strtotime($task->assigned_at);
                $date = date("d.m.Y", $timestamp);
                $time = date("H:i", $timestamp);

                // Add all values to the previously created variable
                $unacceptedTasksArray[] = array(
                    'task' => $taskObj,
                    'role' => $role,
                    'date' => $date,
                    'time' => $time
                );
            }

            $numOfTasks = count($unacceptedTasks);
        } else {
            $unacceptedTasksArray = [];
            $numOfTasks = '';
        }

        /* #################################### */
        /* ###### Get unaccepted methods ###### */
        /* #################################### */

        $unacceptedMethods = $this->methodModel->getUnacceptedAssignmentsByUserId($user->id);

        // Make sure there are unaccepted tasks before proceeding
        if (!empty($unacceptedMethods)) {
            // Init variable
            $unacceptedMethodsArray = [];
            // Loop through the array
            foreach ($unacceptedMethods as $method) {
                // Get the full task
                $methodObj = $this->methodModel->getMethodById($method->method_id);
                // Get the role name
                // source: helpers/role_helper
                $role = translateRoleLanguage($method->role);


                // Store the time of assignment
                $timestamp = strtotime($method->assigned_at);
                $date = date("d.m.Y", $timestamp);
                $time = date("H:i", $timestamp);

                // Add all values to the previously created variable
                $unacceptedMethodsArray[] = array(
                    'method' => $methodObj,
                    'role' => $role,
                    'date' => $date,
                    'time' => $time
                );
            }

            $numOfMethods = count($unacceptedMethods);
        } else {
            $unacceptedMethodsArray = [];
            $numOfMethods = '';
        }

        /* #################################### */
        /* ########## Get Todo List ########### */
        /* #################################### */

        $todo_list_tasks = $this->dataModel->getTodoListByUserId($_SESSION['user_id']);

        // Make sure there are todo_list_tasks before proceeding
        if (!empty($todo_list_tasks)) {
            // Split the database string into multiple parts
            $todo_tasks = explode('||', $todo_list_tasks->tasks);
            // Loop through the array
            foreach ($todo_tasks as $key => $task) {
                // If the current iteration is empty
                if (empty($task)) {
                    // remove it from the array
                    unset($todo_tasks[$key]);
                }
            }
        } else {
            // Otherwise create an empty array
            $todo_tasks = [];
        }

        // Add Checkdate MSG to methods
        foreach ($methods as $key => $method) {
            $methods[$key]->startDate_msg = calculateNextCheckdate($method->reference_date);
        }

        $userUnreadData = $this->dataModel->getUnreadData($_SESSION['user_id']);

        $data = [
            'bodyClass' => 'dashboard',
            'privUser' => $this->privUser,
            'user' => $user,
            'accepted_tasks' => $tasks,
            'accepted_methods' => $methods,
            'angebotswelt' => $tasks_AW,
            'strukturwelt' => $tasks_SW,
            'zielwelt' => $tasks_ZW,
            'rechtswelt' => $tasks_RW,
            'maßnahmenswelt' => $tasks_MW,
            'angebotswelt_method' => $methods_AW,
            'strukturwelt_method' => $methods_SW,
            'zielwelt_method' => $methods_ZW,
            'rechtswelt_method' => $methods_RW,
            'maßnahmenswelt_method' => $methods_MW,
            'unreadData' => $userUnreadData,

            'unaccepted_tasks' => array(
                'tasks' => $unacceptedTasksArray,
                'numOfTasks' => $numOfTasks
            ),
            'unaccepted_methods' => array(
                'methods' => $unacceptedMethodsArray,
                'numOfMethods' => $numOfMethods
            ),
            'unapproved_data' => $allUnapprovedData,
            'todo_tasks' => $todo_tasks,
            'reminders' => $reminders,
            'escalationsTasks' => $escalationsTasks,
            'escalationsMethods' => $escalationsMethods,
            'approvedFilesByUser' => $this->dataModel->getApprovedFilesByUserId($_SESSION['user_id']),
        ];

        // Load the view
        $this->view('users/dashboard', $data);
    }


    // Create a session for the user, that is passed in
    public function createUserSession($user)
    {
        $_SESSION['user_id'] = $user->id;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->lastname;
        $_SESSION['show_disclaimer'] = $user->show_disclaimer;
        redirect('users/dashboard');
    }

    // unset all user session variables
    // destroy the session
    // redirect to the login view
    public function logout()
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['user_name']);
        unset($_SESSION['user_email']);
        unset($_SESSION['show_disclaimer']);
        session_destroy();
        redirect('users/login');
    }

    /* ################################################################### */
    /* ################# METHOD: ASSIGN ABBREVIATION ##################### */
    /* ################################################################### */

    public function abbr()
    {

        // Make sure user is logged in
        if (!isLoggedIn()) {
            redirect('users/login');
        }

        // Check if there is a post request
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $errLog = [];

            foreach ($_POST as $key => $abbr) {

                // Get the position of the first dash
                $pos = strpos($key, '-');
                // Extract the user id of the task from the $key via $pos
                $userId = substr($key, 0, $pos);

                // Update the user's abbreviation and log errors if they occur
                if ($this->userModel->updateUserAbbr($userId, $abbr) != true) {
                    $errLog[] = "Der Nutzer mit der ID:$userId konnte nicht aktualisiert werden.";
                }
            }


            if (empty($errLog)) {
                // Prepare the flash messages and redirect to the index view
                prepareFlash('user_message', 'Die Kürzel der Mitarbeiter wurden erfolgreich aktualisiert');
                redirect('users/index');
            } else {
                // Prepare an error message
                prepareFlash('user_message', 'Die Kürzel der Mitarbeiterkonnte nicht aktualisiert werden', 'alert alert-danger');
                redirect('users/index');
            }
        } else {
            // Load the view to edit the abbreviations

            // Get users
            $users = $this->userModel->getUsers();

            $data = [
                'bodyClass' => 'users',
                'privUser' => $this->privUser,
                'users' => $users
            ];

            // Load view
            $this->view('users/abbr', $data);
        }
    }
}
