<?php
// Require the task model file
require_once '../app/bootstrap.php';
// Require the task model file
//require_once '../app/models/task.php';
// instantiate the data model class
//$taskModel = new Task;


/* ####################################
 A reminder function to get all active tasks, filter them according to whether they have a checkdate and reminders activated and lastly send out emails to all responsible users assigned to the task.
#################################### */

function remind($taskModel, $userModel)
{
    // Get all active tasks
    $tasks = $taskModel->getTasks();

    // Init output array
    $output = [];

    // Loop through them
    foreach ($tasks as $task) {


    // Skip all task rows without a set checkdate
        if (empty($task->check_date)) {
            continue;
        } elseif (empty($task->remind_before) && empty($task->remind_at) && empty($task->remind_after)) {
            // Skip all task rows without any reminders
            continue;
        }

        // Get the task id
        // task->id = taskId (reason: inner join on sql)
        $id = $task->taskId;
        // Get task name
        $taskname = $task->name;

        // Get the checkdate
        $checkdate = $task->check_date;
        // Convert the checkdate to a timestamp
        $checkstamp = strtotime($checkdate);
        // Create formatted checkdate
        $checkdate_formatted = date("d.m.Y", $checkstamp);

        // Get the reminder values
        $remind_before = $task->remind_before;
        $remind_at = $task->remind_at;
        $remind_after = $task->remind_after;
        // Get the current date
        $currentDate = date("Y-m-d");

        // Check if remind_before has a value
        if (!empty($remind_before)) {

      // Subtract the days before
            $beforestamp = $checkstamp - (86400 * $remind_before);
            // Convert the beforestamp into a date
            $before = date("Y-m-d", $beforestamp);
        } else {
            // Create an empty before variable
            $before = '';
        }

        // Check if remind_after has a value
        if (!empty($remind_after)) {

      // Subtract the days before
            $afterstamp = $checkstamp + (86400 * $remind_after);
            // Convert the beforestamp into a date
            $after = date("Y-m-d", $afterstamp);
        }


        /* ################################################ */
        /* ###### Check if today a reminder is due ######## */
        /* ################################################ */

        if (!empty($task->roles_responsible)) {
            // NOT EMPTY
            $users = $task->roles_responsible;
            // Init array to store email adresses in
            $recipients = [];

            // Check if $users is actually an array/object
            if (is_array($users) || is_object($users)) {
                // ARRAY/OBJECT
                // Loop through it
                foreach ($users as $user) {
                    // Get the user as an object from the DB
                    $userObj = $userModel->getUserById($user);
                    // Add the users email to the array
                    $recipients[] = $userObj;
                }
            } else {
                // SINGLE VALUE
                // add it to the array
                $userObj = $userModel->getUserById($users);
                $recipients[] = $userObj;
            }

            // Before date
            if ($currentDate == $before && !empty($remind_before)) {
                foreach ($recipients as $recipient) {
                    $subject = 'Erinnerung: Aufgabe '.$id.' soll am '.$checkdate_formatted.' gecheckt werden.';

                    $body = 'Guten Tag '.$recipient->firstname.' '.$recipient->lastname.',<br> die Aufgabe '.$id.': '.$taskname.' wird voraussichtlich am '.$checkdate_formatted.' gecheckt. Sie erhalten diese Erinnerung, weil sie bei dieser Aufgabe zur Rolle "Verantwortlich" zugeteilt sind. Der nachfolgende Link führt sie direkt zur Aufgabe.<br> <a href="'.URLROOT.'/tasks/show/'.$id.'">Zur Aufgabe</a><br><br> Diese E-Mail ist vom System generiert. Bitte antworten sie nicht auf diese E-Mail.';

                    //sendEmail($recipient->email, $subject, $body)
                }
            }
            // Checkdate
            if ($currentDate == $checkdate && !empty($remind_at)) {
                foreach ($recipients as $recipient) {
                    $subject = 'Erinnerung: Aufgabe '.$id.' soll heute gecheckt werden.';

                    $body = 'Guten Tag '.$recipient->firstname.' '.$recipient->lastname.',<br> der Check der Aufgabe '.$id.': '.$taskname.' steht heute an. Sie erhalten diese Erinnerung, weil sie bei dieser Aufgabe zur Rolle "Verantwortlich" zugeteilt sind. Der nachfolgende Link führt sie direkt zur Aufgabe.<br> <a href="'.URLROOT.'/tasks/show/'.$id.'">Zur Aufgabe</a><br><br> Diese E-Mail ist vom System generiert. Bitte antworten sie nicht auf diese E-Mail.';

                    //sendEmail($recipient->email, $subject, $body)
                }
            }
            // After date
            if ($currentDate == $after && !empty($remind_after)) {
                foreach ($recipients as $recipient) {
                    $subject = 'Aufgaben Check überfällig! Die Aufgabe '.$id.' sollte vor '.$remind_after.' Tagen gecheckt werden.';

                    $body = 'Guten Tag '.$recipient->firstname.' '.$recipient->lastname.',<br> der Check der Aufgabe '.$id.': '.$taskname.' ist seit '.$remind_after.' Tage(n) überfällig. Sie erhalten diese Erinnerung, weil sie bei dieser Aufgabe zur Rolle "Verantwortlich" zugeteilt sind. Der nachfolgende Link führt sie direkt zur Aufgabe. Bitte führen sie umgehend den Check aus.<br> <a href="'.URLROOT.'/tasks/show/'.$id.'">Zur Aufgabe</a><br><br> Diese E-Mail ist vom System generiert. Bitte antworten sie nicht auf diese E-Mail.';

                    //sendEmail($recipient->email, $subject, $body)
                }
            }
        }

    

        $output[] = array(
      'task_id' => $id,
      'checkdate' => $checkdate,
      'remind_before' => $remind_before,
      'remind_at' => $remind_at,
      'remind_after' => $remind_after,
      'before' => $before,
      'after' => $after,
      'recipients' => $recipients
    );
    }

    return $output;
}
