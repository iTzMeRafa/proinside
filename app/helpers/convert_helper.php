<?php

/**
 * @param $stringBool
 * @return int
 */
function stringBoolToInt($stringBool)
{
    if ($stringBool === 'true') {
        return 1;
    } elseif ($stringBool === 'false') {
        return 0;
    } else {
        return undefined;
    }
}

/**
 * @param $userlist
 * @return array
 */
function removeOrgansFromUserCommaSeperatedList($userList)
{
    $userListArr = explode(',', $userList);
    foreach ($userListArr as $key =>$user) {
        $userListArr[$key] = strtok($user, '_');
    }
    $trimedArr = array_filter(array_map('trim', $userListArr));
    return array_unique($trimedArr);
}

/**
 * @param $userlist
 * @return array
 */
function removeOrgansFromUserArrayList($userList)
{
    foreach ($userList as $key =>$user) {
        $userList[$key] = strtok($user, '_');
    }
    $trimedArr = array_filter(array_map('trim', $userList));
    return array_unique($trimedArr);
}

/**
 * Returns the active class or not depending on the body needle
 * @param $bodyClass
 * @param $needle
 * @return string
 */
function getActiveClassByNeedle($bodyClass, $needle) {
    return (strpos($bodyClass, $needle) !== false) ? 'active' : '';
}

/**
 * Merges the first and lastname to a full name string
 * @param $firstName
 * @param $lastName
 * @return string
 */
function mergeFullName($firstName, $lastName) {
        return $firstName . ' ' . $lastName;
}
