<?php


/* #################################################### */
/* ################ CONVERT DATE TIME ################# */
/* #################################################### */

function convert_date_time($input)
{
    // Check if $input is a timestamp
    if (is_int($input)) {
        $timestamp = $input;
    } else {
        // Convert database date-time value into timestamp
        $timestamp = strtotime($input);
    }

    // Split the date and time into two variables and format them
    $weekday = date("D", $timestamp);
    $month = date("m", $timestamp);

    // Convert the month into text
    switch ($month) {
        case "01":
            $month = 'Januar';
            break;
        case "02":
            $month = 'Februar';
            break;
        case "03":
            $month = 'März';
            break;
        case "04":
            $month = 'April';
            break;
        case "05":
            $month = 'Mai';
            break;
        case "06":
            $month = 'Juni';
            break;
        case "07":
            $month = 'Juli';
            break;
        case "08":
            $month = 'August';
            break;
        case "09":
            $month = 'September';
            break;
        case "10":
            $month = 'Oktober';
            break;
        case "11":
            $month = 'November';
            break;
        case "12":
            $month = 'Dezember';
            break;
    }

    // Convert the weekday into text
    switch ($weekday) {
        case "Mon":
            $weekday = 'Montag';
            break;
        case "Tue":
            $weekday = 'Dienstag';
            break;
        case "Wed":
            $weekday = 'Mittwoch';
            break;
        case "Thu":
            $weekday = 'Donnerstag';
            break;
        case "Fri":
            $weekday = 'Freitag';
            break;
        case "Sat":
            $weekday = 'Samstag';
            break;
        case "Sun":
            $weekday = 'Sonntag';
            break;
    }

    // Prepare data array for output
    $data = [
        'weekday' => $weekday,
        'day' => $day = date("d", $timestamp),
        'month' => $month,
        'year' => $year = date("Y", $timestamp),
        'hour' => $hour = date("H", $timestamp),
        'min' => $min = date("i", $timestamp)
    ];

    return $data;
}

/* ############################################################ */
/* ################ CONVERT DATE TIME NUMBERS ################# */
/* ############################################################ */

function convert_date_time_numbers($input)
{
    // Check if $input is a timestamp
    if (is_int($input)) {
        $timestamp = $input;
    } else {
        // Convert database date-time value into timestamp
        $timestamp = strtotime($input);
    }

    // Prepare data array for output
    $data = [
        'day' => $day = date("d", $timestamp),
        'month' => $month = date("m", $timestamp),
        'year' => $year = date("Y", $timestamp),
        'hour' => $hour = date("H", $timestamp),
        'min' => $min = date("i", $timestamp)
    ];

    return $data;
}

/* ###################################################### */
// Calculate the checkdate based upon the reference date
/* ###################################################### */

function calculateCheckdate($timestamp, $count, $interval_format)
{
    // Convert the interval formats into seconds
    $format = [
        'Tages-Rhythmus' => 86400,
        'Wochen-Rhythmus' => 604800,
        'Monats-Rhythmus' => 2592000,
        'Jahres-Rhythmus' => 31536000,
        'Niemals' => 999999999999999999999999999999999999999999999999999999999999999,
    ];

    $nextCheckstamp = $timestamp + $count * $format[$interval_format];

    return $outputArray = convert_date_time_numbers($nextCheckstamp);
}


// ##########################################
// Calculate the the time until the checkdate

function calculateNextCheckdate($input)
{
    // Check if $input is a timestamp
    if (is_int($input)) {
        $dateTimestamp = $input;
    } else {
        // Convert database date-time value into timestamp
        $dateTimestamp = strtotime($input);
    }

    $currentTimestamp = time();

    // If the Checkdate is still in the future
    if ($currentTimestamp < $dateTimestamp) {
        $diff = $dateTimestamp - $currentTimestamp;
        $isWhen = 'future';
        $prefix = 'in';;
    } else {
        // Checkdate is not in the future
        $diff = $currentTimestamp - $dateTimestamp;
        $prefix = 'seit';

        // Check if the checkdate is today and set the variables accordingly
        if ($diff < 86400) {
            $isWhen = 'today';
        } else {
            $isWhen = 'past';
        }
    }


    // Time difference in seconds
    $sec = $diff;

    // Convert time difference in minutes
    $min = round($diff / 60);

    // Convert time difference in hours
    $hrs = round($diff / 3600);

    // Convert time difference in days
    $days = round($diff / 86400);

    // Convert time difference in weeks
    $weeks = round($diff / 604800);

    // Convert time difference in months
    $mnths = round($diff / 2592000);

    // Convert time difference in years
    $yrs = round($diff / 31536000);

    // Check for seconds
    if ($sec <= 60) {
        return "$prefix $sec Sekunden";
    } // Check for minutes
    elseif ($min <= 60) {
        if ($min == 1) {
            $msg = "$prefix einer Minute";
        } else {
            $msg = "$prefix $min Minuten";
        }
    } // Check for hours
    elseif ($hrs <= 24) {
        if ($hrs == 1) {
            $msg = "$prefix einer Stunde";
        } else {
            $msg = "$prefix $hrs Stunden";
        }
    } // Check for days
    elseif ($days <= 7) {
        if ($days == 1 && $isWhen == 'future') {
            $msg = "morgen";
        } elseif ($days == 1 && $isWhen == 'past') {
            $msg = "gestern";
        } else {
            $msg = "$prefix $days Tagen";
        }
    } // Check for weeks
    elseif ($weeks <= 4.3) {
        if ($weeks == 1 && $isWhen == 'future') {
            $msg = "$prefix einer Woche";
        } else {
            $msg = "$prefix $weeks Wochen";
        }
    } // Check for months
    elseif ($mnths <= 12) {
        if ($mnths == 1) {
            $msg = "$prefix einem Monat";
        } else {
            $msg = "$prefix $mnths Monaten";
        }
    } // Check for years
    else {
        if ($yrs == 1) {
            $msg = "$prefix einem Jahr";
        } else {
            $msg = "$prefix $yrs Jahren";
        }
    }

    $output = [
        'isWhen' => $isWhen,
        'msg' => $msg,
        'diffInDays' => $days
    ];

    return $output;
}

function getCurrentTimestamp()
{
    $date = date_create();
    return date_timestamp_get($date);
}

function getCurrentDate($format = 'time')
{
    switch ($format) {
        default:
        case 'time':
            return date("Y-m-d_H-i-s");
        case 'noTime':
            return date("Y-m-d");
    }
}

/**
 * Returns a String formatted
 * @param $date
 * @return string
 * @throws Exception
 */
function differenceBetweenNowAndDateFormatted($date)
{
    $date = new DateTime($date);
    $now = new DateTime();

    return $date->diff($now)->format("%a Tage, %h Stunden und %i Minuten");
}

/**
 * Returns an Object {y: 1, days: 389 ...}
 * @param $date
 * @return bool|DateInterval
 * @throws Exception
 */
function differenceBetweenNowAndDate($date)
{
    $date = new DateTime($date);
    $now = new DateTime();

    return $date->diff($now);
}
