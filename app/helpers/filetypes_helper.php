<?php

// Set up the required font-awesome css-classes
  $fileTypes = array(
    // Archives
    '7z'    => 'fa-file-archive',
    'bz'    => 'fa-file-archive',
    'gz'    => 'fa-file-archive',
    'rar'   => 'fa-file-archive',
    'tar'   => 'fa-file-archive',
    'zip'   => 'fa-file-archive',

    // Text
    'cfg'   => 'fa-file-alt',
    'ini'   => 'fa-file-alt',
    'log'   => 'fa-file-alt',
    'md'    => 'fa-file-alt',
    'rtf'   => 'fa-file-alt',
    'txt'   => 'fa-file-alt',

    // Video
    'avi'   => 'fa-file-video',
    'flv'   => 'fa-file-video',
    'mkv'   => 'fa-file-video',
    'mov'   => 'fa-file-video',
    'mp4'   => 'fa-file-video',
    'mpg'   => 'fa-file-video',
    'ogv'   => 'fa-file-video',
    'webm'  => 'fa-file-video',
    'wmv'   => 'fa-file-video',
    'swf'   => 'fa-file-video',

    // Images
    'bmp'   => 'fa-file-image',
    'gif'   => 'fa-file-image',
    'jpg'   => 'fa-file-image',
    'jpeg'  => 'fa-file-image',
    'png'   => 'fa-file-image',
    'psd'   => 'fa-file-image',
    'tga'   => 'fa-file-image',
    'tif'   => 'fa-file-image',

    // Audio
    'aac'   => 'fa-file-audio',
    'flac'  => 'fa-file-audio',
    'mid'   => 'fa-file-audio',
    'midi'  => 'fa-file-audio',
    'mp3'   => 'fa-file-audio',
    'ogg'   => 'fa-file-audio',
    'wma'   => 'fa-file-audio',
    'wav'   => 'fa-file-audio',

    // Documents
    'csv'   => 'fa-file-csv',
    'doc'   => 'fa-file-word',
    'docx'  => 'fa-file-word',
    'odt'   => 'fa-file-alt',
    'ppt'   => 'fa-powerpoint',
    'pdf'   => 'fa-file-pdf',
    'xls'   => 'fa-file-alt',
    'xlsx'  => 'fa-file-excel',
    // Blank
    'blank' => 'fa-file'
  );
