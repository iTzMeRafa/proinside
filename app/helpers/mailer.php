<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require APPROOT . '/vendor/PHPMailer/Exception.php';
require APPROOT . '/vendor/PHPMailer/PHPMailer.php';
require APPROOT . '/vendor/PHPMailer/SMTP.php';

/* Configurate PHPMailer SMTP
$mailer->SMTPAuth = true;
$mailer->Username = EMAIL_USER;
$mailer->Password = EMAIL_PASS;
$mailer->isMail();
$mailer->SMTPDebug = 2; // 0 = off (for production use) - 1 = client messages - 2 = client and server messages
$mailer->Host = gethostbyname(EMAIL_SERVER); // use $mailer->Host = gethostbyname('smtp.gmail.com'); // if your network does not support SMTP over IPv6
$mailer->Port = 465; // TLS only
$mailer->SMTPSecure = 'tls'; // ssl is depracated
$mailer->msgHTML("Email Test much wow Email Test much wow Email Test much wow"); //$mailer->msgHTML(file_get_contents('contents.html'), __DIR__); //Read an HTML message body from an external file, convert referenced images to embedded,
$mailer->addAttachment('images/phpmailer_mini.png'); //Attach an image file
 */

class Mailer
{
    public static function sendMail($toEmail, $toReply, $subject, $body)
    {
        try {
            $mailer = new PHPMailer;

            //From email address and name
            $mailer->From = "noreply@proinside.app";
            $mailer->FromName = "Proinside";

            //To address and name
            $mailer->addAddress($toEmail); //Recipient name is optional

            //Address to which recipient will reply
            $mailer->addReplyTo($toReply, "NoReply");

            //Send HTML or Plain Text email
            $mailer->isHTML(true);

            $mailer->Subject = $subject;
            $mailer->Body = $body;
            $mailer->AltBody = "Sie können leider keine HTML E-Mails empfangen.";

            if (!$mailer->send()) {
                return false;
            } else {
                return true;
            }
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function sendRegisterMail($toEmail, $firstname, $lastname, $generatedPassword)
    {
        $logoPath = URLROOT . '/img/logo_temp.png';

        try {
            $mailer = new PHPMailer;

            //From email address and name
            $mailer->From = "noreply@proinside.app";
            $mailer->FromName = "PROINSIDE";
            $mailer->CharSet = 'UTF-8';

            //To address and name
            $mailer->addAddress($toEmail, $firstname . ' ' . $lastname); //Recipient name is optional

            //Address to which recipient will reply
            $mailer->addReplyTo(EMAIL_NOREPLY, "NoReply");

            //Send HTML or Plain Text email
            $mailer->isHTML(true);

            $mailer->Subject = 'Ihre Registrierung bei Proinside!';
            $mailer->Body = 'Hallo ' . $firstname . ' ' . $lastname . ', <br /> Sie wurden erfolgreich bei Proinside registriert und können sich mit folgenden Daten anmelden: <br /> <br /> E-Mail: ' . $toEmail . '<br /> Passwort: <strong>' . $generatedPassword . '</strong><br /> <br /> Mit freundlichen Grüßen, <br /> das PRO<strong>INSIDE</strong> Team <br /> <br /><img id="logo" src="'.$logoPath.'" style="width: 300px;" alt="Das ProInside App Logo">';
            $mailer->AltBody = "Sie können leider keine HTML E-Mails empfangen.";

            if (!$mailer->send()) {
                return false;
            } else {
                return true;
            }
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function sendApprovedFileMail($toEmail, $firstname, $lastname, $filename, $folderid)
    {
        $logoPath = URLROOT . '/img/logo_temp.png';
        $fileLink = URLROOT. '/listings/index/'.$folderid;

        try {
            $mailer = new PHPMailer;

            //From email address and name
            $mailer->From = "noreply@proinside.app";
            $mailer->FromName = "PROINSIDE";
            $mailer->CharSet = 'UTF-8';

            //To address and name
            $mailer->addAddress($toEmail, $firstname . ' ' . $lastname); //Recipient name is optional

            //Address to which recipient will reply
            $mailer->addReplyTo(EMAIL_NOREPLY, "NoReply");

            //Send HTML or Plain Text email
            $mailer->isHTML(true);

            $mailer->Subject = 'Proinside - Genehmigte Datei: Bitte lesen und akzeptieren';
            $mailer->Body = 'Hallo ' . $firstname . ' ' . $lastname . ', <br /><br /> Die Datei <strong>'.$filename.'</strong> wurde soeben genehmigt! <br /><br /> Bitte lesen Sie diese und akzeptieren Sie die zur Kenntnisnahme. <br />Zur Datei: <a href="'.$fileLink.'">'.$fileLink.'</a> <br /> <br /> Mit freundlichen Grüßen, <br /> das PRO<strong>INSIDE</strong> Team <br /> <br /><img id="logo" src="'.$logoPath.'" style="width: 300px;" alt="Das ProInside App Logo">';
            $mailer->AltBody = "Sie können leider keine HTML E-Mails empfangen.";

            if (!$mailer->send()) {
                return false;
            } else {
                return true;
            }
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function sendAcceptedReadFileMail($toEmail, $firstname, $lastname, $readUserFirstname, $readUserLastname, $filename)
    {
        $logoPath = URLROOT . '/img/logo_temp.png';
        $readUserFullname = $readUserFirstname . ' ' . $readUserLastname;
        try {
            $mailer = new PHPMailer;

            //From email address and name
            $mailer->From = "noreply@proinside.app";
            $mailer->FromName = "PROINSIDE";
            $mailer->CharSet = 'UTF-8';

            //To address and name
            $mailer->addAddress($toEmail, $firstname . ' ' . $lastname); //Recipient name is optional

            //Address to which recipient will reply
            $mailer->addReplyTo(EMAIL_NOREPLY, "NoReply");

            //Send HTML or Plain Text email
            $mailer->isHTML(true);

            $mailer->Subject = 'Proinside - Eine Datei wurde von einem Mitarbeiter akzeptiert.';
            $mailer->Body = 'Hallo ' . $firstname . ' ' . $lastname . ', <br /><br /> Die Datei <strong>'.$filename.'</strong> wurde soeben von dem Mitarbeiter <strong>'.$readUserFullname.' </strong> gelesen und akzeptiert! <br /> <br /> Mit freundlichen Grüßen, <br /> das PRO<strong>INSIDE</strong> Team <br /> <br /><img id="logo" src="'.$logoPath.'" style="width: 300px;" alt="Das ProInside App Logo">';
            $mailer->AltBody = "Sie können leider keine HTML E-Mails empfangen.";

            if (!$mailer->send()) {
                return false;
            } else {
                return true;
            }
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function sendForgotPasswordMail($toEmail, $firstname, $lastname, $userID, $passwordCode)
    {
        $logoPath = URLROOT . '/img/logo_temp.png';
        $url_passwordcode = URLROOT .'/users/resetpass/' .$userID. '/' .$passwordCode;
        $fullname = $firstname.' '.$lastname;

        try {
            $mailer = new PHPMailer;

            //From email address and name
            $mailer->From = "noreply@proinside.app";
            $mailer->FromName = "PROINSIDE";
            $mailer->CharSet = 'UTF-8';

            //To address and name
            $mailer->addAddress($toEmail, $fullname); //Recipient name is optional

            //Address to which recipient will reply
            $mailer->addReplyTo(EMAIL_NOREPLY, "NoReply");

            //Send HTML or Plain Text email
            $mailer->isHTML(true);

            $mailer->Subject = 'Ihr Passwort kann zurückgesetzt werden - PROINSIDE';
            $mailer->Body =
                'Guten Tag '.$fullname.', <br /> <br /> 
                für Ihr Account bei PROINSIDE wurde ein neues Passwort angefordert. <br /> 
                Um ein neues Passwort zu vergeben, rufen Sie innerhalb der nächsten 24 Stunden den folgenden Link auf: <br /> <br />
                <strong>'.$url_passwordcode.'</strong>
                <br /> <br /> Mit freundlichen Grüßen, <br /> das PRO<strong>INSIDE</strong> Team <br /> <br /><img id="logo" src="'.$logoPath.'" style="width: 300px;" alt="Das ProInside App Logo">';
            $mailer->AltBody = "Sie können leider keine HTML E-Mails empfangen.";

            if (!$mailer->send()) {
                return false;
            } else {
                return true;
            }
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
