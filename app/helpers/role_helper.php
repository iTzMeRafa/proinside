<?php
  // Translate the role name and return it
  function translateRoleLanguage($db_value)
  {
      switch ($db_value) {
      case "responsible":
        return $role = 'Verantwortlich';
        break;
      case "concerned":
        return $role = 'Zuständig';
        break;
      case "contributing":
        return $role = 'Mitwirkend';
        break;
      case "information":
        return $role = 'Informationen';
        break;
    }
  }
