<?php
  session_start();

  // Flash message helpers
  // EXAMPLE - flash('register_success', 'You are now registered');
  // DISPLAY IN VIEW - echo flash('register_success');
  function prepareFlash($name = '', $message = '', $class = 'alert alert-success')
  {
      if (!empty($name)) {
          if (!empty($message) && empty($_SESSION[$name])) {

        // Make sure the session "$name_class" is empty
              if (!empty($_SESSION[$name. '_class'])) {
                  unset($_SESSION[$name. '_class']);
              }

              // Set the two sessions according to the given variables
              $_SESSION[$name] = $message;
              $_SESSION[$name. '_class'] = $class;

              // If $message is empty and $_SESSION[$name] is set
          }
      }
  }

  function displayFlash($name = '', $message = '')
  {
      // Check if the flash Message is already prepared
      // If $message is empty and $_SESSION[$name] is set
      if (empty($message) && !empty($_SESSION[$name])) {

    // If the session "$name_class" is not empty, set $class to Session "$name_class"
          // If session "$name_class" is empty, set $class to an empty string
          $class = !empty($_SESSION[$name. '_class']) ? $_SESSION[$name. '_class'] : '';

          echo '<div class="' .$class. '" id="msg-flash">' .$_SESSION[$name]. '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    </div>';

          // Unset the used sessions for the next message
          unset($_SESSION[$name]);
          unset($_SESSION[$name. '_class']);
      }
  }


  // Check if the user is logged in

  function isLoggedIn()
  {
      if (isset($_SESSION['user_id'])) {
          return true;
      } else {
          return false;
      }
  }
