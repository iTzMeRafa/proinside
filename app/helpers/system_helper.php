<?php

/**
 * Checks if a function is callable and enabled by the system
 * @param $func
 * @return bool
 */
function isEnabled($func) {
    return is_callable($func) && false === stripos(ini_get('disable_functions'), $func);
}