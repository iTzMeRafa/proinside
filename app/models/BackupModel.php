<?php

class BackupModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Gets all backups created
     */
    public function getBackups()
    {
        $this->db->query('SELECT * FROM backups');

        return $this->db->resultSet();
    }

    /**
     * Returns a backup by id
     * @param $backupID
     * @return mixed
     */
    public function getBackupByID($backupID) {
        $this->db->query('SELECT * FROM backups WHERE id = :id');
        $this->db->bind(':id', $backupID);

        return $this->db->single();
    }

    /**
     * Creates a new backup database entry
     * @param $name
     * @param $folder_path
     * @param $zip_path
     * @param $format
     */
    public function createBackup($name, $folder_path, $zip_path, $format)
    {
        $this->db->query('INSERT into backups (name, created_at, folder_path, zip_path, format) VALUES (:name, NOW(), :folder_path, :zip_path, :format)');

        $this->db->bind(':name', $name);
        $this->db->bind(':folder_path', $folder_path);
        $this->db->bind(':zip_path', $zip_path);
        $this->db->bind(':format', $format);

        $this->db->execute();
    }
}
