<?php


class BusinessModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Gets the current and active business model
     * @return mixed
     */
    public function getActiveBusinessModel()
    {
        $this->db->query('SELECT * FROM business_model WHERE isArchived = :isArchived');

        $this->db->bind(':isArchived', 0);

        return $this->db->single();
    }

    /**
     * Gets a business model by id
     * @param $id
     * @return mixed
     */
    public function getBusinessModelByID($id)
    {
        $this->db->query('SELECT * FROM business_model WHERE id = :id');

        $this->db->bind(':id', $id);

        return $this->db->single();
    }

    /**
     * Gets topics for business model by businessmodel id
     * @param int $id
     * @return mixed
     */
    public function getBusinessModelTopicsByID(int $id)
    {
        $this->db->query('SELECT bmt.* 
                            FROM business_model_topics as bmt
                            INNER JOIN folder_tree as ft
                            ON bmt.folder_id = ft.id
                            WHERE business_id = :id 
                            ORDER BY ft.sortID DESC');

        $this->db->bind(':id', $id);

        return $this->db->resultSet();
    }

    /**
     * Gets subtopics for a topic of an business model by topic id
     * @param int $id
     * @return mixed
     */
    public function getBusinessModelTopicSubtopicsByID(int $id)
    {
        $this->db->query('SELECT bms.* 
                            FROM business_model_subtopics as bms
                            INNER JOIN folder_tree as ft 
                            ON bms.folder_id = ft.id
                            WHERE topic_id = :id
                            ORDER BY ft.sortID DESC');

        $this->db->bind(':id', $id);

        return $this->db->resultSet();
    }

    /**
     * Counts business models (revisions)
     * @return mixed
     */
    public function countBusinessModels()
    {
        $this->db->query('SELECT max(id) as "count" FROM business_model');

        return $this->db->single();
    }

    public function getCurrentBusinessLogoPath()
    {
        $this->db->query('SELECT logoPath FROM business_model ORDER BY id DESC LIMIT 1');

        return $this->db->single();
    }

    public function setActiveBusinessModelToArchived()
    {
        $this->db->query('UPDATE business_model SET isArchived = :isArchivedSet WHERE isArchived = :isArchivedCurrent');

        $this->db->bind(':isArchivedSet', 1);
        $this->db->bind(':isArchivedCurrent', 0);

        $this->db->execute();
    }

    public function insertNewBusinessModel($logoPath)
    {
        $this->db->query('INSERT into business_model (created_at, isArchived, logoPath) VALUES (NOW(), :isArchived, :logoPath) ');

        // Bind values
        $this->db->bind(':isArchived', 0);
        $this->db->bind(':logoPath', $logoPath);

        $this->db->execute();
    }

    public function getIdOfActiveBusinessModel()
    {
        $this->db->query('SELECT id FROM business_model WHERE isArchived = :isArchived ORDER BY id DESC LIMIT 1');

        $this->db->bind(':isArchived', 0);

        return $this->db->single();
    }

    public function insertTopic($name, $business_id, $folderID)
    {
        $this->db->query('INSERT into business_model_topics (name, business_id, folder_id) VALUES (:name, :business_id, :folder_id) ');

        // Bind values
        $this->db->bind(':name', $name);
        $this->db->bind(':business_id', $business_id);
        $this->db->bind(':folder_id', $folderID);

        $this->db->execute();
    }

    public function getIdOfNewestTopic()
    {
        $this->db->query('SELECT id FROM business_model_topics ORDER BY id DESC LIMIT 1');

        return $this->db->single();
    }

    public function insertSubtopic($name, $topic_id, $folderID)
    {
        $topic_id = is_null($topic_id) ? 0 : $topic_id;
        $this->db->query('INSERT into business_model_subtopics (name, topic_id, created_at, folder_id) VALUES (:name, :topic_id, NOW(), :folder_id) ');

        $this->db->bind(':name', $name);
        $this->db->bind(':topic_id', $topic_id);
        $this->db->bind(':folder_id', $folderID);

        $this->db->execute();
    }

    public function getTopicIdByFolderId($folder_id)
    {
        $this->db->query('SELECT id FROM business_model_topics WHERE folder_id = :folder_id ORDER BY id DESC');

        // Bind values
        $this->db->bind(':folder_id', $folder_id);

        return $this->db->single();
    }

    public function deleteTopicByFolderID($folder_id)
    {
        $this->db->query('DELETE FROM business_model_topics WHERE folder_id = :folder_id');

        // Bind values
        $this->db->bind(':folder_id', $folder_id);

        // Execute
        $this->db->execute();
    }

    public function deleteSubtopicByFolderID($folder_id)
    {
        $this->db->query('DELETE FROM business_model_subtopics WHERE folder_id = :folder_id');

        // Bind values
        $this->db->bind(':folder_id', $folder_id);

        // Execute
        $this->db->execute();
    }

    public function getAllArchivedBusinessModels()
    {
        $this->db->query("SELECT * FROM business_model WHERE isArchived = :isArchived ORDER BY id ASC");

        $this->db->bind(":isArchived", 1);

        return $this->db->resultSet();
    }

    public function setBusinessModelToActiveById($id)
    {
        $this->db->query('UPDATE business_model SET isArchived = :isArchivedSet WHERE id = :id');

        $this->db->bind(':isArchivedSet', 0);
        $this->db->bind(':id', $id);

        $this->db->execute();



        $this->db->query("UPDATE business_model SET isArchived = :isArchived WHERE id = :id");

        $this->db->bind(":isArchived", 0);
        $this->db->bind(":id", $id);

        // Execute
        $this->db->execute();
    }

    public function updateFolderName($name, $id)
    {
        $this->db->query('UPDATE folder_tree SET folder_name = :folderName WHERE id = :id');

        $this->db->bind(":folderName", $name);
        $this->db->bind(":id", $id);

        $this->db->execute();
    }

    public function checkIfFileIsAvailableByFolderID($folder_id)
    {
        $this->db->query('SELECT id FROM data WHERE parent_id = :folder_id');

        $this->db->bind(":folder_id", $folder_id);

        return $this->db->single();
    }

    public function updateActiveTopic($name, $folder_id)
    {
        $this->db->query('UPDATE business_model_topics SET name = :name WHERE folder_id = :folder_id AND business_id = (SELECT max(business_model.id) FROM business_model)');

        $this->db->bind(":name", $name);
        $this->db->bind(":folder_id", $folder_id);

        $this->db->execute();
    }

    public function updateActiveSubtopic($name, $folder_id)
    {
        $this->db->query('UPDATE business_model_subtopics SET name = :name WHERE folder_id = :folder_id');

        $this->db->bind(":name", $name);
        $this->db->bind(":folder_id", $folder_id);

        $this->db->execute();
    }
}
