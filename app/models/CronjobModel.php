<?php

class CronjobModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Get Reminders which are due
     */
    public function getReminder()
    {
        $this->db->query('SELECT * FROM tasks WHERE id = :id');
        $this->db->bind(':id', 143);

        return $this->db->single();
    }
}
