<?php

class Data
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /* ############################################################# */
    /* ################## DB-TABLE: ARCHIVE_TREE ################### */
    /* ############################################################# */

    /**************************************
     * METHOD: CREATE ARCHIVE FOLDER
     **************************************/

    public function createArchiveFolder($data)
    {
        $this->db->query('INSERT INTO archive_tree ( id ,parent_id , folder_name, folder_path ) VALUES ( :id, :parent_id, :folder_name, :folder_path )');

        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':parent_id', $data['parent_id']);
        $this->db->bind(':folder_name', $data['folder_name']);
        $this->db->bind(':folder_path', $data['folder_path']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**************************************
     * METHOD: GET ARCHIVE FOLDERS
     **************************************/

    public function getArchiveFoldersByParentId($parent_id)
    {
        $this->db->query('SELECT * FROM archive_tree WHERE parent_id = :parent_id ORDER BY folder_name ASC');

        // Bind value
        $this->db->bind(':parent_id', $parent_id);

        $results = $this->db->resultSet();

        return $results;
    }

    /**************************************
     * METHOD: GET ARCHIVE FOLDER BY ID
     **************************************/

    public function getArchiveFolderById($id)
    {
        $this->db->query('SELECT * FROM archive_tree WHERE id = :id');

        // Bind value
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    /* ############################################################# */
    /* ################# DB-TABLE: ARCHIVED_DATA ################### */
    /* ############################################################# */

    /**********************************
     * METHOD: ARCHIVE DATA
     **********************************/

    public function archiveFile($data)
    {
        $this->db->query('INSERT INTO archived_data (file_name, file_path, parent_id, archived_by ) VALUES (:file_name, :file_path, :parent_id, :archived_by)');

        // Bind values
        $this->db->bind(':file_name', $data['file_name']);
        $this->db->bind(':file_path', $data['file_path']);
        $this->db->bind(':parent_id', $data['parent_id']);
        $this->db->bind(':archived_by', $_SESSION['user_id']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /*******************************************
     * METHOD: GET ARCHIVED DATA BY PARENT ID
     *******************************************/

    public function getArchivedDataByParentId($id)
    {
        $this->db->query('SELECT * FROM archived_data WHERE parent_id = :parent_id ORDER BY file_name ASC');

        // Bind value
        $this->db->bind(':parent_id', $id);

        $results = $this->db->resultSet();

        return $results;
    }

    /* ########################################################### */
    /* ################# DB-TABLE: FOLDER_TREE ################### */
    /* ########################################################### */

    /*************************************
     * METHOD: ADD FOLDER TO DATABASE
     *************************************/

    public function addFolder($data)
    {
        $this->db->query('INSERT INTO folder_tree (folder_name, folder_path, parent_id, isTopic, isSubtopic, sortID ) VALUES (:folder_name, :folder_path, :parent_id, :isTopic, :isSubtopic, :sortAfterFolderID)');

        // Bind values
        $this->db->bind(':folder_name', $data['folder_name']);
        $this->db->bind(':folder_path', $data['folder_path']);
        $this->db->bind(':parent_id', $data['parent_id']);
        $this->db->bind(':isTopic', $data['businessModelType'] == 'isTopic' ? 1 : 0);
        $this->db->bind(':isSubtopic', $data['businessModelType'] == 'isSubtopic' ? 1 : 0);
        $this->db->bind(':sortAfterFolderID', $data['sortAfterFolderID'] + 1);

        // Execute
        $last_id = $this->db->executeAndGetId();

        $this->updateSortID($data['sortAfterFolderID']);

        if (@copy(APPROOT . '/../public/uploads/files/init.htaccess', $data['folder_path'] . "/init.htaccess")) {
            $this->addInitFile('init', $data['folder_path'] . "/init.htaccess", 'htaccess', 'init', $last_id, '1');

        }

        return true;
    }

    /**
     * Updates the sort id of folders starting from param id
     * @param $sortAfterFolderID
     * @return bool
     */
    private function updateSortID($sortAfterFolderID)
    {
        $this->db->query('UPDATE folder_tree SET sortID = sortID + 1 WHERE sortID > :sortAfterFolderID');
        $this->db->bind(':sortAfterFolderID', $sortAfterFolderID + 1);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * updates exisiting folders sortid to a give one and updateSort after
     * @param $folderID
     * @param $sortAfterFolderID
     * @return bool
     */
    public function updateFoldersSortID($folderID, $sortAfterFolderID) {
        $this->db->query('UPDATE folder_tree SET sortID = :sortAfterFolderID WHERE id = :folderID');
        $this->db->bind(':sortAfterFolderID', $sortAfterFolderID + 1);
        $this->db->bind(':folderID', $folderID);

        if ($this->db->execute()) {
            $this->updateSortID($sortAfterFolderID);
        }

        return true;
    }

    /*************************************
     * METHOD: GET ALL FOLDERS
     *************************************/

    public function getFolders()
    {
        $this->db->query('SELECT * FROM folder_tree');

        $results = $this->db->resultSet();

        return $results;
    }


    /*************************************
     * METHOD: GET FOLDER BY PARENT ID
     *************************************/

    public function getFolderByParentId($parent_id)
    {
        $this->db->query('SELECT * FROM folder_tree WHERE parent_id = :parent_id ORDER BY sortID DESC');

        // Bind value
        $this->db->bind(':parent_id', $parent_id);

        $results = $this->db->resultSet();

        return $results;
    }

    /**************************************
     * METHOD: GET FOLDER BY ID
     **************************************/

    public function getFolderById($id)
    {
        $this->db->query('SELECT * FROM folder_tree WHERE id = :id');

        // Bind value
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    /**********************************
     * METHOD: GET FOLDER BY NAME
     **********************************/

    public function getFolderByName($name)
    {
        $this->db->query('SELECT * FROM folder_tree WHERE folder_name = :folder_name');

        // Bind value
        $this->db->bind(':folder_name', $name);

        $row = $this->db->single();

        return $row;
    }

    /**********************************
     * METHOD: REMOVE FOLDER BY ID
     **********************************/

    public function removeFolderById($id)
    {
        $this->db->query('DELETE FROM folder_tree WHERE id = :id');
        // Bind value
        $this->db->bind(':id', $id);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /*************************************
     * METHOD: GET ID OF NEWEST FOLDER
     *************************************/
    public function getIdOfNewestFolder()
    {
        $this->db->query('SELECT id FROM folder_tree ORDER BY id DESC LIMIT 1');

        return $this->db->single();
    }

    /* ########################################################### */
    /* ##################### DB-TABLE: DATA ###################### */
    /* ########################################################### */

    /*********************************************
     * METHOD: APPROVE DATA
     *********************************************/

    public function approveData($data)
    {
        $this->db->query('INSERT INTO data (file_name, file_path, extension, tags, parent_id, approved_by) VALUES (:file_name, :file_path, :extension, :tags, :parent_id, :approved_by)');

        // Bind values
        $this->db->bind(':file_name', $data['file_name']);
        $this->db->bind(':file_path', $data['file_path']);
        $this->db->bind(':extension', $data['extension']);
        $this->db->bind(':tags', $data['tags']);
        $this->db->bind(':parent_id', $data['parent_id']);
        $this->db->bind(':approved_by', $data['approved_by']);

        return $this->db->executeAndGetId();
    }

    /**
     * INSERT READ DATA
     * Keeps track of data which has to be read and accepted by users
     *
     * @param $userId
     * @param $dataId
     * @param $isAccepted
     * @return boolean
     */
    public function addReadData($userId, $dataId, $isAccepted)
    {
        $this->db->query('INSERT INTO read_data (userId, dataId, isAccepted) VALUES (:userId, :dataId, :isAccepted)');
        $this->db->bind(':userId', $userId);
        $this->db->bind(':dataId', $dataId);
        $this->db->bind(':isAccepted', $isAccepted);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * GET READ DATA
     * Gets isAccepted from the read data selected by user id and data id
     * @param $userId
     * @param $dataId
     * @return mixed
     */
    public function getReadData($userId, $dataId)
    {
        $this->db->query('SELECT isAccepted FROM read_data WHERE userId = :userId AND dataId = :dataId');
        $this->db->bind('userId', $userId);
        $this->db->bind('dataId', $dataId);

        return $this->db->single();
    }

    /**
     * Update a unread data to read data: isAccepted 0 => 1
     * @param $userId
     * @param $dataId
     * @return bool
     */
    public function updateReadData($userId, $dataId)
    {
        $this->db->query('UPDATE read_data SET isAccepted = :isAccepted WHERE userId = :userId AND dataId = :dataId');
        $this->db->bind(':isAccepted', 1);
        $this->db->bind(':userId', $userId);
        $this->db->bind(':dataId', $dataId);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Get all unread data for a user by id
     * @param $userId
     * @return mixed
     */
    public function getUnreadData($userId)
    {
        $this->db->query('SELECT t1.*, t2.* 
                          FROM read_data t1
                          INNER JOIN data t2
                          ON t1.dataId = t2.id
                          WHERE userId = :userId 
                          AND isAccepted = :isAccepted');
        $this->db->bind(':userId', $userId);
        $this->db->bind(':isAccepted', 0);

        return $this->db->resultSet();
    }

    /**
     * Returns a list of users who have not accepted the file yet by fileid
     * @param $dataId
     * @return mixed
     */
    public function getUnacceptedUsersByDataId($dataId) {
        $this->db->query('SELECT t2.firstname, t2.lastname
                            FROM read_data t1
                            LEFT JOIN users t2
                            ON t1.userId = t2.id
                            WHERE t1.dataId = :dataId 
                            AND t1.isAccepted = :isAccepted');
        $this->db->bind('dataId', $dataId);
        $this->db->bind('isAccepted', 0);
        $userObj = $this->db->resultSet();
        $userList = [];
        foreach ($userObj as $key => $user) {
            $userList[$key] = $user->firstname . ' ' . $user->lastname;
        }

        return $userList;
    }

    /**
     * Returns all approved files by given user id and also
     * the names of the users who have to read the file
     * @param $userId
     * @return mixed
     */
    public function getApprovedFilesByUserId($userId) {
        $this->db->query('SELECT t1.* FROM data t1 INNER JOIN read_data t2 ON t1.id = t2.dataId WHERE approved_by = :userId');
        $this->db->bind(':userId', $userId);

        $files = $this->db->resultSet();
        foreach ($files as $file) {
            $file->users = $this->getUsernamesForFileByFileId($file->id);
        }
        return $files;
    }

    /**
     * Returns usernames and isAccepted state for a given file
     * @param $fileId
     * @return mixed
     */
    public function getUsernamesForFileByFileId($fileId) {
        $this->db->query('SELECT t1.isAccepted, t2.firstname, t2.lastname 
                          FROM read_data t1 
                          LEFT JOIN users t2
                          ON t1.userId = t2.id
                          WHERE t1.dataId = :fileId');
        $this->db->bind(':fileId', $fileId);

        $usersObj = $this->db->resultSet();

        $users = [
          'acceptedUser' => [],
          'unacceptedUser' => [],
        ];

        foreach ($usersObj as $key => $user) {
            $user->fullname = $user->firstname . ' ' . $user->lastname;
            $user->isAccepted
                ? $users['acceptedUser'][$key] = $user->fullname
                : $users['unacceptedUser'][$key] = $user->fullname;
        }

        return $users;
    }

    /*********************************************
     * METHOD: GET APPROVED DATA BY FOLDER ID
     *********************************************/

    public function getDataByFolderId($id)
    {
        $this->db->query('SELECT t1.*, t2.firstname, t2.lastname FROM data t1 
                               INNER JOIN users t2
                               ON t1.approved_by = t2.id
                               WHERE parent_id = :parent_id 
                               ORDER BY file_name ASC');

        // Bind value
        $this->db->bind(':parent_id', $id);

        $results = $this->db->resultSet();

        return $results;
    }



    /*********************************************
     * METHOD: GET APPROVED FILE BY ID
     *********************************************/

    public function getFileById($id)
    {
        $this->db->query('SELECT * FROM data WHERE id = :id');

        // Bind value
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    /*********************************************
     * METHOD: GET APPROVED FILE BY PATH
     *********************************************/

    public function getFileByPath($path)
    {
        $this->db->query('SELECT * FROM data WHERE file_path = :file_path');

        // Bind value
        $this->db->bind(':file_path', $path);

        return $this->db->single();
    }

    /*********************************************
     * METHOD: CHANGE FILENAME
     *********************************************/

    public function changeFileName($data)
    {
        $this->db->query('UPDATE data SET file_name = :file_name, file_path = :file_path WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':file_name', $data['file_name']);
        $this->db->bind(':file_path', $data['file_path']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        };
    }

    /*********************************************
     * METHOD: ADD REPORT FOR RENAMING FILES
     *********************************************/

    public function addReportOnFileRenaming($user_id, $file_id)
    {
        $this->db->query('INSERT INTO reports (report_date, report_type, user_id, data_id) VALUES (NOW(), :report_type, :user_id, :data_id)');
        // Bind values
        $this->db->bind(':report_type', 'data_rename');
        $this->db->bind(':user_id', $user_id);
        $this->db->bind(':data_id', $file_id);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /*********************************************
     * METHOD: REMOVE APPROVED FILE BY ID
     *********************************************/

    public function removeFile($id)
    {
        $this->db->query('DELETE FROM data WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $id);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /***********************************************
     * METHOD: REMOVE APPROVED DATA BY FOLDER ID
     ***********************************************/

    public function removeDataByFolderId($id)
    {
        $this->db->query('DELETE FROM data WHERE parent_id = :parent_id');
        // Bind values
        $this->db->bind(':parent_id', $id);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /* ###################################################################### */
    /* ##################### DB-TABLE: UNAPPROVED DATA ###################### */
    /* ###################################################################### */

    /**********************************
     * METHOD: ADD UNAPPROVED DATA
     **********************************/

    public function addUnapprovedData($data)
    {
        $this->db->query('INSERT INTO unapproved_data (target_name, path, extension, tags, user_id, parent_id, userNotificationList ) VALUES (:target_name, :path, :extension, :tags, :user_id, :parent_id, :userNotificationList)');

        // Bind values
        $this->db->bind(':target_name', $data['target_name']);
        $this->db->bind(':path', $data['path']);
        $this->db->bind(':extension', $data['extension']);
        $this->db->bind(':tags', $data['tags']);
        $this->db->bind(':user_id', $data['user_id']);
        $this->db->bind(':parent_id', $data['parent_id']);
        $this->db->bind(':userNotificationList', $data['userNotificationList']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Update an unapproved data
     *
     * @param $userNotificationList, $fileID
     * @return boolean
     */
    public function updateUnapprovedData($userNotificationList, $fileID)
    {
        $this->db->query('UPDATE unapproved_data SET userNotificationList = :userNotificationList WHERE id = :fileID');

        $this->db->bind(':userNotificationList', $userNotificationList);
        $this->db->bind(':fileID', $fileID);
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /*********************************************
     * METHOD: GET UNAPPROVED DATA BY FOLDER ID
     *********************************************/

    public function getUnapprovedDataByFolderId($id)
    {
        $this->db->query('SELECT * FROM unapproved_data WHERE parent_id = :parent_id AND is_active = 1  ORDER BY target_name ASC');

        // Bind value
        $this->db->bind(':parent_id', $id);

        $results = $this->db->resultSet();

        return $results;
    }

    /**
     * Gets all unapproved data
     * @return mixed
     */
    public function getAllUnapprovedData()
    {
        $this->db->query('SELECT * FROM unapproved_data WHERE is_active = 1 ORDER BY id DESC');
        return $this->db->resultSet();
    }

    /*********************************************
     * METHOD: GET APPROVED FILE BY ID
     *********************************************/

    public function getUnapprovedFileById($id)
    {
        $this->db->query('SELECT * FROM unapproved_data WHERE id = :id');

        // Bind value
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    /*********************************************
     * METHOD: SET UNAPPROVED FILE INACTIVE
     *********************************************/

    public function setUnapprovedFileInactive($id)
    {
        $this->db->query('UPDATE unapproved_data SET is_active = :is_active WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $id);
        $this->db->bind(':is_active', 0);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /* ########################################################### */
    /* ################## DB-TABLE: TODO LISTs ################### */
    /* ########################################################### */

    /*********************************************
     * METHOD: CREATE TODO LIST
     *********************************************/

    public function createTodoList($tasks)
    {
        $this->db->query('INSERT INTO todo_lists (user_id, tasks) VALUES (:user_id, :tasks) ');
        // Bind values
        $this->db->bind(':user_id', $_SESSION['user_id']);
        $this->db->bind(':tasks', $tasks);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        };
    }

    /*********************************************
     * METHOD: SAVE TODO LIST
     *********************************************/

    public function saveTodoList($tasks)
    {
        $this->db->query('UPDATE todo_lists SET tasks = :tasks WHERE user_id = :user_id');
        // Bind values
        $this->db->bind(':user_id', $_SESSION['user_id']);
        $this->db->bind(':tasks', $tasks);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        };
    }

    /*********************************************
     * METHOD: SAVE TODO LIST
     *********************************************/

    public function getTodoListByUserId($id)
    {
        $this->db->query('SELECT * FROM todo_lists WHERE user_id = :user_id');
        // Bind values
        $this->db->bind(':user_id', $id);

        $row = $this->db->single();

        return $row;
    }

    /* ########################################################### */
    /* ##################### DB-TABLE: TAGS ###################### */
    /* ########################################################### */

    /*********************************************
     * METHOD: ADD TAG
     *********************************************/

    public function addTag($data)
    {
        $this->db->query('INSERT INTO tags (tag, task_id, method_id, file_id) VALUES (:tag, :task_id, :method_id, :file_id)');
        // Bind values
        $this->db->bind(':tag', $data['tag']);
        $this->db->bind(':task_id', $data['task_id']);
        $this->db->bind(':method_id', $data['method_id']);
        $this->db->bind(':file_id', $$data['file_id']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /************************************
     * METHOD: GET TAGS
     ************************************/

    public function getTags($tag)
    {
        $this->db->query('SELECT * FROM tags WHERE tags = :tag');
        // Bind values
        $this->db->bind(':tag', $tag);

        $results = $this->db->resultSet();

        return $results;
    }

    /************************************
     * METHOD: GET TAGS BY TASK ID
     ************************************/

    public function getTagsByTaskId($task_id)
    {
        $this->db->query('SELECT * FROM tags WHERE task_id = :task_id');
        // Bind values
        $this->db->bind(':task_id', $task_id);

        $results = $this->db->resultSet();

        return $results;
    }

    /************************************
     * METHOD: GET TAGS BY METHOD ID
     ************************************/

    public function getTagsByMethodId($method_id)
    {
        $this->db->query('SELECT * FROM tags WHERE method_id = :method_id');
        // Bind values
        $this->db->bind(':method_id', $method_id);

        $results = $this->db->resultSet();

        return $results;
    }

    /************************************
     * METHOD: GET TAGS BY FILE ID
     ************************************/

    public function getTagsByFileId($file_id)
    {
        $this->db->query('SELECT * FROM tags WHERE file_id = :file_id');
        // Bind values
        $this->db->bind(':file_id', $file_id);

        $results = $this->db->resultSet();

        return $results;
    }

    /*********************************************
     * METHOD: ADD INIT FILE
     *********************************************/

    public function addInitFile($file_name, $file_path, $extension, $tags, $parent_id, $approved_by)
    {
        $this->db->query('INSERT INTO data (file_name, file_path, extension, tags, parent_id, approved_by, approved_at) VALUES (:file_name, :file_path, :extension, :tags, :parent_id, :approved_by, NOW())');
        // Bind values
        $this->db->bind(':file_name', $file_name);
        $this->db->bind(':file_path', $file_path);
        $this->db->bind(':extension', $extension);
        $this->db->bind(':tags', $tags);
        $this->db->bind(':parent_id', $parent_id);
        $this->db->bind(':approved_by', $approved_by);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
