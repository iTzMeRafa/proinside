<?php
class Folder
{
    private $db;

    /**
     * Folder constructor.
     * Initialize database connection
     */
    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Returns the folder name by folder id
     * @param $parent_id
     * @return mixed
     */
    public function getFolderNameById($folderID)
    {
        $this->db->query('SELECT folder_name FROM folder_tree WHERE id = :folderID');
        $this->db->bind(':folderID', $folderID);

        return $this->db->single();
    }
}
