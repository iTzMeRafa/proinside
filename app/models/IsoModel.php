<?php

class IsoModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Fetches all isos
     * @return mixed
     */
    public function getAllIsos() {
        $this->db->query('SELECT * FROM isos');

        return $this->db->resultSet();
    }

    /**
     * Returns a iso by id
     * @param $iso_id
     * @return mixed
     */
    public function getIsoById($iso_id) {
        $this->db->query('SELECT * FROM isos WHERE id = :iso_id');
        $this->db->bind(':iso_id', $iso_id);

        return $this->db->single();
    }

    /**
     * Stores a new iso
     * @param $isoName
     * @return bool
     */
    public function storeIso($isoName) {
        $this->db->query('INSERT INTO isos (norm, created_at) VALUES (:name, NOW())');
        $this->db->bind(':name', $isoName);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Deletes a iso by Id
     * @param $iso_id
     * @return bool
     */
    public function removeIso($iso_id) {
        $this->db->query('DELETE FROM isos WHERE id = :id');
        $this->db->bind(':id', $iso_id);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Renames a iso by id
     * @param $iso_id
     * @param $iso_name
     * @return bool
     */
    public function renameIso($iso_id, $iso_name) {
        $this->db->query('UPDATE isos SET norm = :name WHERE id = :id');
        $this->db->bind(':name', $iso_name);
        $this->db->bind(':id', $iso_id);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Assign isos to tasks
     * @param $iso_id
     * @param $taskId
     * @return bool
     */
    public function assignTaskIso($iso_id, $taskId) {
        $this->db->query('INSERT INTO isos_tasks (iso_id, task_id, assigned_at) VALUES (:iso_id, :task_id, NOW()) ');
        $this->db->bind(':iso_id', $iso_id);
        $this->db->bind(':task_id', $taskId);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Remove isos from task
     * @param $taskId
     * @return bool
     */
    public function removeTaskIso($taskId) {
        $this->db->query('DELETE FROM isos_tasks WHERE task_id = :task_id');
        $this->db->bind(':task_id', $taskId);

        return $this->db->execute()
            ? true
            : false;
    }
}