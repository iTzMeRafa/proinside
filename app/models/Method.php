<?php
  class Method
  {
      private $db;

      public function __construct()
      {
          $this->db = new Database;
      }

      /* ############################################################# */
      /* ################ GET MULTIPLE TASKS AT ONCE ################# */
      /* ############################################################# */

      /*************************************
          METHOD: GET ALL ACTIVE TASKS
       *************************************/

      public function getActiveMethods()
      {
          $this->db->query('SELECT *
                        FROM methods
                        WHERE is_archived = :is_archived 
                        AND is_approved = :is_approved
                        ORDER BY methods.created_at DESC
                        ');
          // Bind value
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_approved', 'true');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /*************************************
          METHOD: GET ALL UNARCHIVED TASKS
       *************************************/

      public function getMethods()
      {
          $this->db->query('SELECT *,
                          methods.id as methodId,
                          users.id as userId,
                          methods.created_at as methodCreated,
                          users.created_at as userCreated,
                          methods.name as methodName,
                          users.lastname as userName
                          FROM methods 
                          INNER JOIN users
                          ON methods.user_id = users.id
                          WHERE is_archived = :is_archived
                          ORDER BY methods.created_at DESC
                          ');
          // Bind value
          $this->db->bind(':is_archived', 'false');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /**********************************
          METHOD: GET ALL ACTIVE SINGLE TASKS
       **********************************/

      public function getActiveSingleMethods()
      {
          $this->db->query('SELECT *,
                        methods.id as methodId,
                        users.id as userId,
                        methods.created_at as methodCreated,
                        users.created_at as userCreated,
                        methods.name as methodName,
                        users.lastname as userName,
                        processes.process_name as process_name
                        FROM methods 
                        INNER JOIN users
                        ON methods.user_id = users.id
                        INNER JOIN processes
                        ON methods.process_id = processes.id
                        WHERE methods.is_archived = :is_archived
                        ORDER BY methods.created_at DESC
                        ');
          // Bind value
          $this->db->bind(':is_archived', 'false');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }
    
      /**********************************
          METHOD: GET ALL ARCHIVED SINGLE TASKS
       **********************************/

      public function getArchivedSingleMethods()
      {
          $this->db->query('SELECT *,
                        methods.id as methodId,
                        users.id as userId,
                        methods.created_at as methodCreated,
                        users.created_at as userCreated,
                        methods.name as methodName,
                        users.lastname as userName,
                        processes.process_name as process_name
                        FROM methods 
                        INNER JOIN users
                        ON methods.user_id = users.id
                        INNER JOIN processes
                        ON methods.process_id = processes.id
                        WHERE methods.is_archived = :is_archived
                        ORDER BY methods.created_at DESC
                        ');
          // Bind values
          $this->db->bind(':is_archived', 'true');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /**********************************
          METHOD: GET ALL SINGLE TASKS
       **********************************/

      public function getAllSingleMethods()
      {
          $this->db->query('SELECT *,
                        methods.id as methodId,
                        users.id as userId,
                        methods.created_at as methodCreated,
                        users.created_at as userCreated,
                        methods.name as methodName,
                        users.lastname as userName,
                        processes.process_name as process_name
                        FROM methods 
                        INNER JOIN users
                        ON methods.user_id = users.id
                        INNER JOIN processes
                        ON methods.process_id = processes.id
                        ORDER BY methods.created_at DESC
                        ');


          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }


      /* ############################################################# */
      /* ##################### GET ONLY ONE TASK ##################### */
      /* ############################################################# */

      /**************************************
          METHOD: GET (ACTIVE) TASK BY ID
       **************************************/

      public function getMethodById($id)
      {
          $this->db->query('SELECT *,
                  methods.id as methodId,
                  users.id as userId,
                  methods.created_at as methodCreated,
                  users.created_at as userCreated,
                  methods.name as methodName,
                  users.lastname as userName,
                  processes.process_name as process_name
                  FROM methods 
                  INNER JOIN users
                  ON methods.user_id = users.id
                  INNER JOIN processes
                  ON methods.process_id = processes.id
                  WHERE methods.id = :id AND methods.is_archived = :is_archived AND methods.is_approved = :is_approved
                  ORDER BY methods.created_at DESC
');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_approved', 'true');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /**************************************
         METHOD: GET SINGLE TASK BY ID
      **************************************/

      public function getSingleMethodById($id)
      {
          $this->db->query('SELECT * FROM methods WHERE id = :id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /**************************************
         METHOD: GET ARCHIVED TASK BY ID
      **************************************/

      public function getArchivedMethodById($id)
      {
          $this->db->query('SELECT * FROM methods WHERE id = :id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'true');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /******************************************
        METHOD: GET PROCESS TASK BY ID
      ********************************************/

      public function getProcessMethodById($id)
      {
          $this->db->query('SELECT * FROM methods WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /**************************************
        METHOD: GET UNARCHIVED TASK BY ID

        So Single and Process Methods, Unapproved and Approved Methods
     **************************************/

      public function getUnarchivedMethodById($id)
      {
          $this->db->query('SELECT * FROM methods WHERE id = :id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /******************************************
        METHOD: GET (ANY KIND OF) TASK BY ID
      ********************************************/

      public function getAnyMethodById($id)
      {
          $this->db->query('SELECT * FROM methods WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /*********************************
          METHOD: ADD (ACTIVE) TASK
       *********************************/

      public function addMethod($data)
      {
          $this->db->query('INSERT INTO methods (created_at, user_id, name, description, occurrence, reason, solution, goal, status, priority, required_knowledge_text, required_knowledge, required_knowledge_folders, roles_responsible, roles_concerned, roles_contributing, roles_information, iso_relevant, check_intervals, reference_date, interval_count, interval_format, check_date, remind_before, remind_at, remind_after, required_resources_text, required_resources, required_resources_folders, existing_compentencies, required_education, tags, is_archived, process_id, is_approved) VALUES (:created_at, :user_id, :name, :description, :occurrence, :reason, :solution, :goal, :status, :priority, :required_knowledge_text, :required_knowledge, :required_knowledge_folders, :roles_responsible, :roles_concerned, :roles_contributing, :roles_information, :iso_relevant, :check_intervals, :reference_date, :interval_count, :interval_format, :check_date, :remind_before, :remind_at, :remind_after, :required_resources_text, :required_resources, :required_resources_folders, :existing_compentencies, :required_education, :tags, :is_archived, :process_id, :is_approved )');

          // Bind values
          $this->db->bind(':created_at', date('Y-m-d H:i:s'));
          $this->db->bind(':user_id', $data['user_id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':description', $data['description']);
          $this->db->bind(':occurrence', $data['occurrence']);
          $this->db->bind(':reason', $data['reason']);
          $this->db->bind(':solution', $data['solution']);
          $this->db->bind(':goal', $data['goal']);
          $this->db->bind(':status', $data['status']);
          $this->db->bind(':priority', $data['priority']);
          $this->db->bind(':required_knowledge_text', $data['required_knowledge_text']);
          $this->db->bind(':required_knowledge', $data['required_knowledge']);
          $this->db->bind(':required_knowledge_folders', $data['required_knowledge_folders']);
          $this->db->bind(':roles_responsible', $data['roles_responsible']);
          $this->db->bind(':roles_concerned', $data['roles_concerned']);
          $this->db->bind(':roles_contributing', $data['roles_contributing']);
          $this->db->bind(':roles_information', $data['roles_information']);
          $this->db->bind(':iso_relevant', $data['iso_relevant']);
          $this->db->bind(':check_intervals', $data['check_intervals']);
          $this->db->bind(':reference_date', $data['reference_date']);
          $this->db->bind(':interval_count', $data['interval_count']);
          $this->db->bind(':interval_format', $data['interval_format']);
          $this->db->bind(':check_date', $data['check_date']);
          $this->db->bind(':remind_before', $data['remind_before']);
          $this->db->bind(':remind_at', $data['remind_at']);
          $this->db->bind(':remind_after', $data['remind_after']);
          $this->db->bind(':required_resources_text', $data['required_resources_text']);
          $this->db->bind(':required_resources', $data['required_resources']);
          $this->db->bind(':required_resources_folders', $data['required_resources_folders']);
          $this->db->bind(':existing_compentencies', $data['existing_compentencies']);
          $this->db->bind(':required_education', $data['required_education']);
          $this->db->bind(':tags', $data['tags']);
          $this->db->bind(':is_approved', $data['is_approved']);
          // Make it active by default
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':process_id', $data['process_id']);

          // Execute
          $last_id = $this->db->executeAndGetId();

          if ($last_id != false) {
              return $last_id;
          } else {
              return false;
          }
      }

      /***************************
          METHOD: UPDATE TASK
       ***************************/

      public function updateMethod($data)
      {
          $this->db->query('UPDATE methods SET name = :name, description = :description, occurrence = :occurrence, reason = :reason, solution = :solution, goal = :goal, status = :status, priority = :priority, required_knowledge_text = :required_knowledge_text, required_knowledge = :required_knowledge, roles_responsible = :roles_responsible, roles_concerned = :roles_concerned, roles_contributing = :roles_contributing, roles_information = :roles_information, iso_relevant = :iso_relevant, check_intervals = :check_intervals, reference_date = :reference_date, interval_count = :interval_count, interval_format = :interval_format, check_date = :check_date, remind_before = :remind_before, remind_at = :remind_at, remind_after = :remind_after, required_resources_text = :required_resources_text, required_resources = :required_resources, existing_compentencies = :existing_compentencies, required_education = :required_education, tags = :tags, process_id = :process_id WHERE id = :id');

          // Bind values
          $this->db->bind(':id', $data['id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':description', $data['description']);
          $this->db->bind(':occurrence', $data['occurrence']);
          $this->db->bind(':reason', $data['reason']);
          $this->db->bind(':solution', $data['solution']);
          $this->db->bind(':goal', $data['goal']);
          $this->db->bind(':status', $data['status']);
          $this->db->bind(':priority', $data['priority']);
          $this->db->bind(':required_knowledge_text', $data['required_knowledge_text']);
          $this->db->bind(':required_knowledge', $data['required_knowledge']);
          $this->db->bind(':roles_responsible', $data['roles_responsible']);
          $this->db->bind(':roles_concerned', $data['roles_concerned']);
          $this->db->bind(':roles_contributing', $data['roles_contributing']);
          $this->db->bind(':roles_information', $data['roles_information']);
          $this->db->bind(':iso_relevant', $data['iso_relevant']);
          $this->db->bind(':check_intervals', $data['check_intervals']);
          $this->db->bind(':reference_date', $data['reference_date']);
          $this->db->bind(':interval_count', $data['interval_count']);
          $this->db->bind(':interval_format', $data['interval_format']);
          $this->db->bind(':check_date', $data['check_date']);
          $this->db->bind(':remind_before', $data['remind_before']);
          $this->db->bind(':remind_at', $data['remind_at']);
          $this->db->bind(':remind_after', $data['remind_after']);
          $this->db->bind(':required_resources_text', $data['required_resources_text']);
          $this->db->bind(':required_resources', $data['required_resources']);
          $this->db->bind(':existing_compentencies', $data['existing_compentencies']);
          $this->db->bind(':required_education', $data['required_education']);
          $this->db->bind(':tags', $data['tags']);
          $this->db->bind(':process_id', $data['process_id']);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /****************************************
           METHOD: ADD PROCESS ID TO TASK
      ****************************************/

      public function addProcessIdToMethod($process_id, $method_id)
      {
          $this->db->query('UPDATE methods SET process_id = :process_id WHERE id = :id');

          // Bind values
          $this->db->bind(':id', $method_id);
          $this->db->bind(':process_id', $process_id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }




      /**************************************************
          METHOD: APPROVE TASK (FOR APPROVED PROCESSES)
       **************************************************/

      public function approveMethod($id)
      {
          $this->db->query('UPDATE methods SET is_approved = :is_approved WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_approved', 'true');

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /***************************
         METHOD: ARCHIVE TASK
      ***************************/

      public function archiveMethod($id)
      {
          $this->db->query('UPDATE methods SET is_archived = :is_archived WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'true');

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /* ###################################################################### */
      /* ##################### DB-TABLE: UNACCEPTED TASKS ##################### */
      /* ###################################################################### */

      /*************************************************
          METHOD: ADD AN UNACCEPTED ASSIGNMENT
       *************************************************/

      public function addUnacceptedAssignment($data)
      {
          $this->db->query('INSERT INTO unaccepted_methods (method_id, user_id, role, organ, suborgan, assigned_at) VALUES (:method_id, :user_id, :role, :organ, :suborgan, :assigned_at)');

          // Bind values
          $this->db->bind(':method_id', $data['method_id']);
          $this->db->bind(':user_id', $data['user_id']);
          $this->db->bind(':role', $data['role']);
          $this->db->bind(':organ', $data['organ_id']);
          $this->db->bind(':assigned_at', date('Y-m-d H:i:s'));

          // If suborgan id is provided
          if (!empty($data['suborgan_id'])) {
              // PROVIDED
              // Bind value
              $this->db->bind(':suborgan', $data['organ_id']);
          } else {
              // EMPTY
              // Make it null
              $this->db->bind(':suborgan', null);
          }

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }
    
      /****************************************************
          METHOD: GET UNACCEPTED ASSIGNMENTS BY USER ID
       ****************************************************/

      public function getUnacceptedAssignmentsByUserId($id)
      {
          $this->db->query('SELECT * FROM unaccepted_methods WHERE user_id = :user_id');
          // Bind value
          $this->db->bind(':user_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /*************************************************
          METHOD: GET UNACCEPTED ASSIGNMENTS BY TASK ID
       *************************************************/

      public function getUnacceptedAssignmentsByMethodId($id)
      {
          $this->db->query('SELECT * FROM unaccepted_methods WHERE method_id = :method_id');
          // Bind value
          $this->db->bind(':method_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /*****************************************************
          METHOD: REMOVE UNACCEPTED ASSIGNMENTS BY TASK ID
       ******************************************************/

      public function removeUnacceptedAssignmentsByMethodId($id)
      {
          $this->db->query('DELETE FROM unaccepted_methods WHERE method_id = :method_id');
          // Bind value
          $this->db->bind(':method_id', $id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /* ###################################################################### */
      /* ###################### DB-TABLE: ACCEPTED TASKS ###################### */
      /* ###################################################################### */

      /*******************************************
          METHOD: GET ACCEPTED TASKS BY USER ID
       *******************************************/

      public function getAcceptedMethodsByUserId($id)
      {
          $this->db->query('SELECT * FROM accepted_methods WHERE user_id = :user_id');
          // Bind value
          $this->db->bind(':user_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /************************************************
          METHOD: REMOVE ACCEPTED TASKS BY TASK ID
       ************************************************/

      public function removeAcceptedMethodsByMethodId($id)
      {
          $this->db->query('DELETE FROM accepted_methods WHERE method_id = :method_id');
          // Bind value
          $this->db->bind(':method_id', $id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }


      /* ###################################################################### */
      /* ######################### DB-TABLE: CHECKS ########################### */
      /* ###################################################################### */

      /*************************************************
          METHOD: GET CHECK BY ID
       *************************************************/

      public function getCheckById($id)
      {
          $this->db->query('SELECT * FROM checks_methods WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /*************************************************
          METHOD: GET CHECKS BY TASKID
       *************************************************/

      public function getChecksByMethodId($id)
      {
          $this->db->query('SELECT * FROM checks_methods WHERE method_id = :method_id');
          // Bind value
          $this->db->bind(':method_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }
  }
