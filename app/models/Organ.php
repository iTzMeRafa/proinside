<?php
  class Organ
  {
      private $db;

      public function __construct()
      {
          $this->db = new Database;
      }

      /**********************
          METHOD: GET ORGANS
       **********************/

      public function getOrgans()
      {
          $this->db->query('SELECT * FROM organs ORDER BY id ASC ');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /********************************************
          METHOD: GET ORGANS WITHOUT INNER JOIN
       ********************************************/

      public function getOrgansWithoutUser()
      {
          $this->db->query('SELECT * FROM organs ORDER BY id ASC ');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /***************************
          METHOD: GET ORGAN BY ID
       ***************************/

      public function getOrganById($id)
      {
          $this->db->query('SELECT * FROM organs WHERE id = :id');

          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /***********************************
         METHOD: GET ORGANS BY LEVEL ID
      ************************************/

      public function getOrgansByLevelId($id)
      {
          $this->db->query('SELECT * FROM organs WHERE level_id = :level_id');
          // Bind value
          $this->db->bind(':level_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /***************************
          METHOD: ADD ORGAN
       ***************************/

      public function addOrgan($data)
      {
          $this->db->query('INSERT INTO organs (name, description, management_level, staff, abbreviation, level_id) VALUES ( :name, :description, :management_level, :staff, :abbreviation, :level_id)');

          // Bind values
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':description', $data['description']);
          $this->db->bind(':management_level', $data['management_level']);
          $this->db->bind(':staff', $data['staff']);
          $this->db->bind(':abbreviation', $data['abbreviation']);
          $this->db->bind(':level_id', $data['level_id']);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /***************************
          METHOD: UPDATE ORGAN
       ***************************/

      public function updateOrgan($data)
      {
          $this->db->query('UPDATE organs SET name = :name, abbreviation = :abbreviation, description = :description, management_level = :management_level, staff = :staff WHERE id = :id');

          // Bind values
          $this->db->bind(':id', $data['id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':abbreviation', $data['abbreviation']);
          $this->db->bind(':description', $data['description']);
          $this->db->bind(':management_level', $data['management_level']);
          $this->db->bind(':staff', $data['staff']);
       

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /***************************
         METHOD: DELETE ORGAN
      ***************************/

      public function deleteOrgan($id)
      {
          $this->db->query('DELETE FROM organs WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /*************************************
         METHOD: UPDATE ACTIVE STATUS
      **************************************/

      public function updateOrganActiveStatus($data)
      {
          $this->db->query('UPDATE organs SET is_active = :is_active WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $data['id']);
          $this->db->bind(':is_active', $data['is_active']);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }
    
      /* ############################################# */
      /* ################ SUB ORGANS ################# */
      /* ############################################# */

      /****************************************
        METHOD: GET SUBORGANS BY PARENT ID
      *****************************************/

      public function getSubOrgansByParentId($parent_id)
      {
          $this->db->query('SELECT * FROM suborgans WHERE parent_id = :parent_id');

          // Bind value
          $this->db->bind(':parent_id', $parent_id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /******************************
        METHOD: GET SUBORGAN BY ID
      *******************************/

      public function getSubOrganById($id)
      {
          $this->db->query('SELECT * FROM suborgans WHERE id = :id');

          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the results
          return $row;
      }
    
      /***************************
        METHOD: ADD SUBORGAN
      ***************************/

      public function addSubOrgan($data)
      {
          $this->db->query('INSERT INTO suborgans (parent_id, name, abbreviation, staff, tasks, methods) VALUES (:parent_id, :name, :abbreviation, :staff, :tasks, :methods)');

          // Bind values
          $this->db->bind(':parent_id', $data['parent_id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':abbreviation', $data['abbreviation']);
          $this->db->bind(':staff', $data['staff']);
          $this->db->bind(':tasks', $data['tasks']);
          $this->db->bind(':methods', $data['methods']);

          // Execute
          $last_id = $this->db->executeAndGetId();

          if ($last_id != false) {
              return $last_id;
          } else {
              return false;
          }
      }

      /******************************
        METHOD: EDIT SUBORGAN
      *******************************/

      public function editSubOrgan($data)
      {
          $this->db->query('UPDATE suborgans SET name = :name, abbreviation = :abbreviation, staff = :staff  WHERE id = :id');

          // Bind values
          $this->db->bind(':id', $data['id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':abbreviation', $data['abbreviation']);
          $this->db->bind(':staff', $data['staff']);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /******************************
        METHOD: DELETE SUBORGAN
      *******************************/

      public function deleteSubOrgan($id)
      {
          $this->db->query('DELETE FROM suborgans WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /* ############################################# */
      /* ################# FUNCTIONS ################# */
      /* ############################################# */

      /* ################# EXPLODE USER ID STRING #################### */

      public function explodeIdString($id_string)
      {
          if (!empty($id_string)) {
              // FILLED
              // Turn string of ids into array of ids
              $output = explode(',', $id_string);
          } else {
              // EMPTY
              // Set $output an empty array
              $output = [];
          }

          return $output;
      }

      /* ################# POPULATE USER-ID-ARRAY #################### */

      public function populateUserIdArray($user_id_array, $userModel)
      {
          if (!empty($user_id_array)) {
              // FILLED
              // Create output array
              $outputArray = [];

              // Loop through the array
              foreach ($user_id_array as $user_id) {

          // Get user object from DB
                  $user = $userModel->getUserById($user_id);

                  // Make sure it is filled
                  if (is_object($user)) {
                      // Add it to the output array
                      $outputArray[] = $user;
                  }
              }
          } else {
              // EMPTY
              // Create empty output array
              $outputArray = [];
          }
      
          return $outputArray;
      }
  }
