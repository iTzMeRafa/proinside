<?php
  class Permission
  {
      private $db;

      public function __construct()
      {
          $this->db = new Database;
      }

      /* ###################################################################### */
      /* ######################## DB-TABLE: RBAC_ROLES ######################## */
      /* ###################################################################### */

      /***************************
          METHOD: GET ALL ROLES
       ***************************/

      public function getRoles()
      {
          $this->db->query('SELECT * FROM rbac_roles');

          $results = $this->db->resultSet();

          return $results;
      }

      /*****************************
          METHOD: GET SINGLE ROLE
       *****************************/

      public function getSingleRole($role_id)
      {
          $this->db->query('SELECT * FROM rbac_roles WHERE role_id = :role_id');

          // Bind value
          $this->db->bind(':role_id', $role_id);

          $row = $this->db->single();

          return $row;
      }

      /*****************************
          METHOD: ADD ROLE
       *****************************/

      public function addRole($role_name, $role_description, $folder_perms = 'global')
      {
          $this->db->query('INSERT INTO rbac_roles (role_name, role_description, folder_perms) VALUES (:role_name, :role_description, :folder_perms)');
          // Bind values
          $this->db->bind(':role_name', $role_name);
          $this->db->bind(':role_description', $role_description);
          $this->db->bind(':folder_perms', $folder_perms);

          // Execute
          $last_id = $this->db->executeAndGetId();

          return $last_id;
      }

      /*****************************
          METHOD: UPDATE ROLE
       *****************************/

      public function updateRole($role_id, $role_name, $role_description, $folder_perms)
      {
          $this->db->query('UPDATE rbac_roles SET role_name = :role_name, role_description = :role_description, folder_perms = :folder_perms WHERE role_id = :role_id');
          // Bind values
          $this->db->bind(':role_id', $role_id);
          $this->db->bind(':role_name', $role_name);
          $this->db->bind(':role_description', $role_description);
          $this->db->bind(':folder_perms', $folder_perms);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /* ########################################################################### */
      /* ######################## DB-TABLE: RBAC_ROLE_PERMS ######################## */
      /* ########################################################################### */

      /**********************************
          METHOD: ADD ROLE PERMISSIONS
       **********************************/

      public function addRolePerms($role_id, $perm_ids)
      {
          $this->db->query('INSERT INTO rbac_role_perms (role_id, perm_ids) VALUES (:role_id, :perm_ids)');
          // Bind values
          $this->db->bind(':role_id', $role_id);
          $this->db->bind(':perm_ids', $perm_ids);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /**********************************
          METHOD: UPDATE ROLE PERMISSIONS
       **********************************/

      public function updateRolePerms($role_id, $perm_ids)
      {
          $this->db->query('UPDATE rbac_role_perms SET perm_ids = :perm_ids WHERE role_id = :role_id');
          // Bind values
          $this->db->bind(':role_id', $role_id);
          $this->db->bind(':perm_ids', $perm_ids);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /************************************************
          METHOD: GET THE PERMISSION ID's OF A ROLE
       ************************************************/

      public function getPermIdsByRoleId($role_id)
      {
          $this->db->query('SELECT * FROM rbac_role_perms WHERE role_id = :role_id');
          // Bind value
          $this->db->bind(':role_id', $role_id);
          // Get the row
          $row = $this->db->single();

          $perm_ids = explode(',', $row->perm_ids);

          return $perm_ids;
      }

      /* ###################################################################### */
      /* ##################### DB-TABLE: RBAC_PERMISSIONS ##################### */
      /* ###################################################################### */

      /**********************************
          METHOD: GET ALL PERMISSIONS
       **********************************/

      public function getAllPermissions()
      {
          $this->db->query('SELECT * FROM rbac_permissions');

          $results = $this->db->resultSet();

          return $results;
      }

      /********************************************
          METHOD: GET THE PERMISSIONS OF A ROLE
       ********************************************/

      public function getPermsByRoleId($role_id)
      {

      // Get the perm_ids
          $perm_ids =  $this->getPermIdsByRoleId($role_id);

          //die(var_dump($perm_ids));

          $output = [];

          foreach ($perm_ids as $perm_id) {
              $this->db->query('SELECT * FROM rbac_permissions WHERE perm_id = :perm_id');
              // Bind value
              $this->db->bind(':perm_id', $perm_id);
              // Get results
              $row = $this->db->single();

              $output[$perm_id] = $row;

              if ($output[$perm_id] == false) {
                  unset($output[$perm_id]);
              }
          }
      
          return $output;
      }

      /* ###################################################################### */
      /* ###################### DB-TABLE: RBAC_USER_ROLES ##################### */
      /* ###################################################################### */

      /********************************************
          METHOD: GET THE PERMISSIONS OF A ROLE
       ********************************************/

      public function giveUserDefaultRole($user_id, $role_id)
      {
          $this->db->query('INSERT INTO rbac_user_roles (user_id, role_ids) VALUES (:user_id, :role_ids)');
          // Bind values
          $this->db->bind(':user_id', $user_id);
          $this->db->bind(':role_ids', $role_id);
      
          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /********************************************
          METHOD: GET THE PERMISSIONS OF A ROLE
       ********************************************/

      public function updateRolesOfUser($user_id, $role_ids)
      {
          $this->db->query('UPDATE rbac_user_roles SET role_ids = :role_ids WHERE user_id = :user_id');
          // Bind values
          $this->db->bind(':user_id', $user_id);
          $this->db->bind(':role_ids', $role_ids);
      
          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }
  }
