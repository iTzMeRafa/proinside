<?php
  class Process
  {
      private $db;

      public function __construct()
      {
          $this->db = new Database;
      }

      /* ###################################################################### */
      /* ######################### DB-TABLE:  PROCESSES ####################### */
      /* ###################################################################### */

      /**********************************
          METHOD: CREATE PROCESS
       **********************************/

      public function createProcess($data)
      {
          $this->db->query('INSERT INTO processes (process_name, folder_id, task_ids) VALUE (:process_name, :folder_id, :task_ids)');

          // Bind values
          $this->db->bind(':process_name', $data['process_name']);
          $this->db->bind(':folder_id', $data['folder_id']);
          $this->db->bind(':task_ids', $data['task_ids']);

          // Execute
          $last_id = $this->db->executeAndGetId();

          if ($last_id != false) {
              return $last_id;
          } else {
              return false;
          }
      }

      /**********************************
          METHOD: GET PROCESS BY ID
       **********************************/

      public function getProcessById($id)
      {
          $this->db->query('SELECT * FROM processes WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /**********************************
          METHOD: GET ALL PROCESSES
      **********************************/

      public function getAllProcesses()
      {
          $this->db->query('SELECT * FROM processes');

          // Get the row
          $row = $this->db->resultSet();

          // Return the row
          return $row;
      }
      
      /****************************************
          METHOD: GET PROCESS(ES) BY FOLDER ID
       ****************************************/

      public function getProcessByFolderId($id)
      {
          $this->db->query('SELECT * FROM processes WHERE folder_id = :folder_id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':folder_id', $id);
          $this->db->bind(':is_archived', 'false');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /**********************************
          METHOD: UPDATE PROCESS
       **********************************/
    
      public function updateProcess($data)
      {
          $this->db->query('UPDATE processes SET process_name = :process_name, folder_id = :folder_id, task_ids = :task_ids WHERE id = :id AND is_approved = :is_approved');
          // Bind value
          $this->db->bind(':id', $data['id']);
          $this->db->bind(':process_name', $data['process_name']);
          $this->db->bind(':folder_id', $data['folder_id']);
          $this->db->bind(':task_ids', $data['task_ids']);
          $this->db->bind(':is_approved', 'false');

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /**********************************
          METHOD: APPROVE PROCESS
       **********************************/
    
      public function approveProcess($id)
      {
          $this->db->query('UPDATE processes SET is_approved = :is_approved WHERE id = :id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_approved', 'true');

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /**********************************
          METHOD: ARCHIVE PROCESS
       **********************************/

      public function archiveProcess($id)
      {
          $this->db->query('UPDATE processes SET is_archived = :is_archived, archived_by = :archived_by, archived_at = :archived_at WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'true');
          $this->db->bind(':archived_by', $_SESSION['user_id']);
          // Create the current timestamp
          $currentDate = date("Y-m-d");
          $currentTime = date("H:i");
          $this->db->bind(':archived_at', "$currentDate $currentTime");

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /***********************************************
          METHOD: GET ARCHIVED PROCESSES BY FOLDER ID
       ***********************************************/

      public function getArchivedProcessesByFolderId($id)
      {
          $this->db->query('SELECT * FROM processes WHERE folder_id = :folder_id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':folder_id', $id);
          $this->db->bind(':is_archived', 'true');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /* ###################################################################### */
      /* ####################### DB-TABLE: PROCESS TASKS ###################### */
      /* ###################################################################### */

      /*********************************
         METHOD: ADD PROCESS TASK
      *********************************/

      public function addProcessTask($data)
      {
          $this->db->query('INSERT INTO process_tasks (process_id, user_id, name, description, department, input, input_source, output, output_target, category, risk, status, required_knowledge, required_knowledge_folders, roles_responsible, roles_concerned, roles_contributing, roles_information, iso_value, check_intervals, reference_date, interval_count, interval_format, check_date, remind_before, remind_at, remind_after, required_resources, required_resources_folders, existing_compentencies, required_education, tags, is_archived, is_approved) VALUES (:process_id, user_id, :name, :description, :department, :input, :input_source, :output, :output_target, :category, :risk, :status, :required_knowledge, :required_knowledge_folders, :roles_responsible, :roles_concerned, :roles_contributing, :roles_information, :iso_value, :check_intervals, :reference_date, :interval_count, :interval_format, :check_date, :remind_before, :remind_at, :remind_after, :required_resources, :required_resources_folders, :existing_compentencies, :required_education, :tags, :is_archived, :is_approved )');

          // Bind values
          $this->db->bind(':process_id', $data['process_id']);
          $this->db->bind(':user_id', $data['user_id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':description', $data['description']);
          $this->db->bind(':department', $data['department']);
          $this->db->bind(':input', $data['input']);
          $this->db->bind(':input_source', $data['input_source']);
          $this->db->bind(':output', $data['output']);
          $this->db->bind(':output_target', $data['output_target']);
          $this->db->bind(':category', $data['category']);
          $this->db->bind(':risk', $data['risk']);
          $this->db->bind(':status', $data['status']);
          $this->db->bind(':required_knowledge', $data['required_knowledge']);
          $this->db->bind(':required_knowledge_folders', $data['required_knowledge_folders']);
          $this->db->bind(':roles_responsible', $data['roles_responsible']);
          $this->db->bind(':roles_concerned', $data['roles_concerned']);
          $this->db->bind(':roles_contributing', $data['roles_contributing']);
          $this->db->bind(':roles_information', $data['roles_information']);
          $this->db->bind(':iso_value', $data['iso_value']);
          $this->db->bind(':check_intervals', $data['check_intervals']);
          $this->db->bind(':reference_date', $data['reference_date']);
          $this->db->bind(':interval_count', $data['interval_count']);
          $this->db->bind(':interval_format', $data['interval_format']);
          $this->db->bind(':check_date', $data['check_date']);
          $this->db->bind(':remind_before', $data['remind_before']);
          $this->db->bind(':remind_at', $data['remind_at']);
          $this->db->bind(':remind_after', $data['remind_after']);
          $this->db->bind(':required_resources', $data['required_resources']);
          $this->db->bind(':required_resources_folders', $data['required_resources_folders']);
          $this->db->bind(':existing_compentencies', $data['existing_compentencies']);
          $this->db->bind(':required_education', $data['required_education']);
          $this->db->bind(':tags', $data['tags']);
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_approved', $data['is_approved']);
      

          // Execute
          $last_id = $this->db->executeAndGetId();

          if ($last_id != false) {
              return $last_id;
          } else {
              return false;
          }
      }

      /**************************************************
          METHOD: ADD PROCESS ID TO PROCESS TASKS
       **************************************************/

      public function addProcessIdToProcessTasks($taskIds, $process_id)
      {
          foreach ($taskIds as $taskId) {
              $this->db->query('UPDATE process_tasks SET process_id = :process_id  WHERE id = :id');
              // Bind value
              $this->db->bind(':process_id', $process_id);
              $this->db->bind(':id', $id);
        
              // Execute
              if ($this->db->execute()) {
                  return true;
              } else {
                  return false;
              }
          }
      }

      /**************************************
          METHOD: GET PROCESS TASK BY ID
       **************************************/

      public function getProcessTaskById($id)
      {
          $this->db->query('SELECT * FROM process_tasks WHERE id = :id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }


      /* ###################################################################### */
      /* ##################### DB-TABLE: APPROVED PROCESSES ################### */
      /* ###################################################################### */

      /**********************************
          METHOD: APPROVE PROCESS
       **********************************/

      public function logApprovedProcess($id)
      {
          $this->db->query('INSERT INTO approved_processes (process_id, approved_by) VALUE (:process_id, :approved_by)');

          // Bind values
          $this->db->bind(':process_id', $id);
          $this->db->bind(':approved_by', $_SESSION['user_id']);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }
  }
