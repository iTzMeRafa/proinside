<?php

class QualificationModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Fetches all qualifications
     * @return mixed
     */
    public function getAllQualifications() {
        $this->db->query('SELECT * FROM qualifications');

        return $this->db->resultSet();
    }

    /**
     * Returns a qualification by id
     * @param $qualification_id
     * @return mixed
     */
    public function getQualificationById($qualification_id) {
        $this->db->query('SELECT * FROM qualifications WHERE id = :qualification_id');
        $this->db->bind(':qualification_id', $qualification_id);

        return $this->db->single();
    }

    /**
     * Gets assigned at value of a user and a qualification
     * @param $user_id
     * @param $qualification_id
     * @return mixed
     */
    public function getAssignedAtUserQualification($user_id, $qualification_id) {
        $this->db->query('SELECT assigned_at FROM qualifications_users WHERE qualification_id = :qualification_id AND user_id = :user_id');
        $this->db->bind(':qualification_id', $qualification_id);
        $this->db->bind(':user_id', $user_id);

        return $this->db->single();
    }

    /**
     * Stores a new qualification
     * @param $qualificationName
     * @param $qualificationExpireTime
     * @return bool
     */
    public function storeQualification($qualificationName, $qualificationExpireTime) {
        $this->db->query('INSERT INTO qualifications (name, expire_in_days, created_at) VALUES (:name, :expire_in_days, NOW())');
        $this->db->bind(':name', $qualificationName);
        $this->db->bind(':expire_in_days', $qualificationExpireTime);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Deletes a qualification by Id
     * @param $qualificationId
     * @return bool
     */
    public function removeQualification($qualificationId) {
        $this->db->query('DELETE FROM qualifications WHERE id = :id');
        $this->db->bind(':id', $qualificationId);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Renames a qualification by id
     * @param $qualificationId
     * @param $qualificationName
     * @return bool
     */
    public function renameQualification($qualificationId, $qualificationName) {
        $this->db->query('UPDATE qualifications SET name = :name WHERE id = :id');
        $this->db->bind(':name', $qualificationName);
        $this->db->bind(':id', $qualificationId);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Assignes Qualifications to Users
     * @param $qualificationId
     * @param $userId
     * @return bool
     */
    public function assignUserQualification($qualificationId, $userId) {
        $this->db->query('INSERT INTO qualifications_users (qualification_id, user_id, assigned_at) VALUES (:qualification_id, :user_id, NOW()) ');
        $this->db->bind(':qualification_id', $qualificationId);
        $this->db->bind(':user_id', $userId);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Assign qualifications to tasks
     * @param $qualificationId
     * @param $taskId
     * @return bool
     */
    public function assignTaskQualification($qualificationId, $taskId) {
        $this->db->query('INSERT INTO qualifications_tasks (qualification_id, task_id, assigned_at) VALUES (:qualification_id, :task_id, NOW()) ');
        $this->db->bind(':qualification_id', $qualificationId);
        $this->db->bind(':task_id', $taskId);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Remove qualifications from task
     * @param $taskId
     * @return bool
     */
    public function removeTaskQualification($taskId) {
        $this->db->query('DELETE FROM qualifications_tasks WHERE task_id = :task_id');
        $this->db->bind(':task_id', $taskId);

        return $this->db->execute()
            ? true
            : false;
    }

    /**
     * Gets all users who have a specific qualification (by qualification id)
     * @param $qualification_id
     * @return mixed
     */
    public function getUsersWithQualificationById($qualification_id) {
        $this->db->query('SELECT t2.*, t1.assigned_at
                              FROM qualifications_users t1
                              LEFT JOIN users t2
                              ON t1.user_id = t2.id
                              WHERE t1.qualification_id = :qualification_id');
        $this->db->bind(':qualification_id', $qualification_id);

        return $this->db->resultSet();
    }
}