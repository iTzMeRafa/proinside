<?php
  class Question
  {
      private $db;

      public function __construct()
      {
          $this->db =  new Database;
      }

      /* ################################
           METHOD: ADD QUESTION
       ################################## */

      public function addFlexibleQuestion($data)
      {
          $this->db->query('INSERT INTO flexible_questions (question, yes_no, notify, remind, created_by, task_id, method_id, document_id) VALUES (:question, :yes_no, :notify, :remind, :created_by,  :task_id, :method_id, :document_id)');
          // Bind values
          $this->db->bind(':question', $data['question']);
          $this->db->bind(':yes_no', $data['yesNo']);
          $this->db->bind(':notify', $data['notify']);
          $this->db->bind(':remind', $data['remind']);
          $this->db->bind(':created_by', $_SESSION['user_id']);
          $this->db->bind(':task_id', $data['task_id']);
          $this->db->bind(':method_id', $data['method_id']);
          $this->db->bind(':document_id', $data['document_id']);
      
          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /* #########################################
          METHOD: GET QUESTIONS BY TASK ID
       ########################################### */

      public function getQuestionsByTaskId($id)
      {
          $this->db->query('SELECT * FROM flexible_questions WHERE task_id = :task_id ');
          // Bind value
          $this->db->bind(':task_id', $id);

          $results = $this->db->resultSet();

          return $results;
      }

      /* #########################################
          METHOD: GET QUESTIONS BY METHOD ID
       ########################################### */

      public function getQuestionsByMethodId($id)
      {
          $this->db->query('SELECT * FROM flexible_questions WHERE method_id = :method_id ');
          // Bind value
          $this->db->bind(':method_id', $id);

          $results = $this->db->resultSet();

          return $results;
      }




      /* ##############################################
          METHOD: REMOVE ALL TASK QUESTIONS BY METHOD ID
        ################################################ */

      public function removeQuestionsByTaskId($id)
      {
          $this->db->query('DELETE FROM flexible_questions WHERE method_id = :method_id');
          // Bind values
          $this->db->bind(':method_id', $id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      public function removeQuestionsByMethodId($id)
      {
          $this->db->query('DELETE FROM flexible_questions WHERE method_id = :method_id');
          // Bind values
          $this->db->bind(':method_id', $id);
  
          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }
  }
