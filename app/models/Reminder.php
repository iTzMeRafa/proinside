<?php
class Reminder
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /* ############################################################# */
    /* #################### DB-TABLE: REMINDERS #################### */
    /* ############################################################# */

    /*******************************************************************
    METHOD: GET UNSEEN & ACTIVE ESCALATAIONS REMINDERS BY USER ID
     *******************************************************************/

    public function getUnseenTaskEscalationsByUserId($id)
    {
        $this->db->query('SELECT * FROM reminders_tasks WHERE user_id = :user_id AND status = :status AND reminder_type = :reminder_type AND is_active = :is_active');

        // Bind values
        $this->db->bind(':user_id', $id);
        $this->db->bind(':status', 'unseen');
        $this->db->bind(':reminder_type', 'escalation');
        $this->db->bind(':is_active', 1);

        // Get the results
        $results = $this->db->resultSet();

        // Return the results
        return $results;
    }

    public function getUnseenMethodEscalationsByUserId($id)
    {
        $this->db->query('SELECT *, t2.name FROM reminders_methods t1 INNER JOIN methods t2 ON t1.method_id = t2.id WHERE t1.user_id = :user_id AND t1.status = :status AND t1.reminder_type = :reminder_type AND t1.is_active = :is_active');

        // Bind values
        $this->db->bind(':user_id', $id);
        $this->db->bind(':status', 'unseen');
        $this->db->bind(':reminder_type', 'escalation');
        $this->db->bind(':is_active', 1);

        // Get the results
        $results = $this->db->resultSet();

        // Return the results
        return $results;
    }

    /*************************************************************
    METHOD: GET UNSEEN & ACTIVE REMINDERS BY USER ID
     *************************************************************/

    public function getUnseenTaskRemindersByUserId($id)
    {
        $this->db->query('SELECT *, t2.name FROM reminders_tasks t1 INNER JOIN tasks t2 ON t1.task_id = t2.id WHERE t1.user_id = :user_id AND t1.status = :status AND t1.reminder_type != :reminder_type AND t1.is_active = :is_active');

        // Bind values
        $this->db->bind(':user_id', $id);
        $this->db->bind(':status', 'unseen');
        $this->db->bind(':reminder_type', 'escalation');
        $this->db->bind(':is_active', 1);

        // Get the results
        $results = $this->db->resultSet();

        // Return the results
        return $results;
    }

    public function getUnseenMethodRemindersByUserId($id)
    {
        $this->db->query('SELECT * FROM reminders_methods WHERE user_id = :user_id AND status = :status AND reminder_type != :reminder_type AND is_active = :is_active');

        // Bind values
        $this->db->bind(':user_id', $id);
        $this->db->bind(':status', 'unseen');
        $this->db->bind(':reminder_type', 'escalation');
        $this->db->bind(':is_active', 1);

        // Get the results
        $results = $this->db->resultSet();

        // Return the results
        return $results;
    }

    /**************************************
    METHOD: ADD REMINDER
     **************************************/

    public function addTaskReminder($data)
    {
        if (isset($data['task_id']) && !empty($data['task_id']) && $data['task_id'] != null) {
            $this->db->query('INSERT INTO reminders_tasks ( task_id , user_id, check_date, reminder_type ) VALUES ( :task_id, :user_id, :check_date, :reminder_type )');

            // Bind values
            $this->db->bind(':task_id', $data['task_id']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':check_date', $data['check_date']);
            $this->db->bind(':reminder_type', $data['reminder_type']);

            // Execute
            if ($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function addMethodReminder($data)
    {
        $this->db->query('INSERT INTO reminders_methods ( method_id , user_id, check_date, reminder_type ) VALUES ( :method_id, :user_id, :check_date, :reminder_type )');

        // Bind values
        $this->db->bind(':method_id', $data['method_id']);
        $this->db->bind(':user_id', $data['user_id']);
        $this->db->bind(':check_date', $data['check_date']);
        $this->db->bind(':reminder_type', $data['reminder_type']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**************************************
    METHOD: GET REMINDER
     **************************************/

    public function getTaskReminder($data)
    {
        if (isset($data['task_id']) && !empty($data['task_id']) && $data['task_id'] != null) {
            $this->db->query('SELECT * FROM reminders_tasks WHERE task_id = :task_id AND user_id = :user_id AND check_date = :check_date AND reminder_type = :reminder_type');

            // Bind values
            $this->db->bind(':task_id', $data['task_id']);
            $this->db->bind(':user_id', $data['user_id']);
            $this->db->bind(':check_date', $data['check_date']);
            $this->db->bind(':reminder_type', $data['reminder_type']);

            $row = $this->db->single();

            return $row;
        }
    }

    public function getMethodReminder($data)
    {
        $this->db->query('SELECT * FROM reminders_methods WHERE method_id = :method_id AND user_id = :user_id AND check_date = :check_date AND reminder_type = :reminder_type');

        // Bind values
        $this->db->bind(':method_id', $data['method_id']);
        $this->db->bind(':user_id', $data['user_id']);
        $this->db->bind(':check_date', $data['check_date']);
        $this->db->bind(':reminder_type', $data['reminder_type']);

        $row = $this->db->single();

        return $row;
    }

    /********************************************
    METHOD: DEACTIVATE REMINDERS BY TASK ID
     ********************************************/

    public function deactivateTaskRemindersByTaskId($task_id)
    {
        $this->db->query('UPDATE reminders_tasks SET is_active = :is_active WHERE task_id = :task_id ');

        // Bind values
        $this->db->bind(':task_id', $task_id);
        $this->db->bind(':is_active', 0);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    public function deactivateMethodRemindersByMethodId($method_id)
    {
        $this->db->query('UPDATE reminders_methods SET is_active = :is_active WHERE method_id = :method_id ');

        // Bind values
        $this->db->bind(':method_id', $method_id);
        $this->db->bind(':is_active', 0);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
}
