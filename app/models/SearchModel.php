<?php


class SearchModel
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Gets all tasks by tags
     * @return mixed
     */
    public function getTasksByTags($query)
    {
        $this->db->query('SELECT tasks.*, organs.name as organname,
                        tasks.id as taskId,
                        users.id as userId,
                        tasks.created_at as taskCreated,
                        users.created_at as userCreated,
                        tasks.name as taskName,
                        users.lastname as userName
                        FROM tasks 
                        INNER JOIN users
                        ON tasks.user_id = users.id
                        LEFT JOIN organs
                        ON tasks.department = organs.id
                        WHERE tags LIKE :query
                        ORDER BY tasks.created_at DESC');

        $this->db->bind(':query', '%'.$query.'%');

        return $this->db->resultSet();
    }

    /**
     * Gets all methods by tags
     * @return mixed
     */
    public function getMethodsByTags($query)
    {
        $this->db->query('SELECT *,
                        methods.id as methodId,
                        users.id as userId,
                        methods.created_at as methodCreated,
                        users.created_at as userCreated,
                        methods.name as methodName,
                        users.lastname as userName,
                        processes.process_name as process_name
                        FROM methods 
                        INNER JOIN users
                        ON methods.user_id = users.id
                        INNER JOIN processes
                        ON methods.process_id = processes.id
                        WHERE tags LIKE :query
                        ORDER BY methods.created_at DESC');

        $this->db->bind(':query', '%'.$query.'%');

        return $this->db->resultSet();
    }

    /**
     * Gets all documents by tags
     * @return mixed
     */
    public function getDocumentsByTags($query)
    {
        $this->db->query('SELECT * FROM data WHERE tags LIKE :query');

        $this->db->bind(':query', '%'.$query.'%');

        return $this->db->resultSet();
    }
}
