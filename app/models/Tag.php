<?php
  class Tag {
    private $db;

    public function __construct(){
      $this->db = new Database;
    }

    /* ############################################################# */
    /* ################ GET MULTIPLE TASKS AT ONCE ################# */
    /* ############################################################# */

    /****************************************************
        METHOD: ADD TAG
     ****************************************************/

    public function addTag($data){
      $this->db->query('INSERT INTO tags ( tag, task_id, method_id, file_id ) VALUES ( :tag, :task_id, :method_id, :file_id )');

      // Bind values
      $this->db->bind(':tag', $data['tag']);
      $this->db->bind(':task_id', $data['task_id']);
      $this->db->bind(':method_id', $data['method_id']);
      $this->db->bind(':file_id', $data['file_id']);

      // Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }

    /****************************************************
        METHOD: GET TAGS BY KEYWORD
     ****************************************************/

    public function getTagsByKeyword($keyword){
      $this->db->query('SELECT * FROM tags WHERE tag LIKE "%:tag%"');

      // Bind values
      $this->db->bind(':tag', $keyword);

      // Get the results
      $results = $this->db->resultSet();

       // Return the results
      return $results;
    }

    /****************************************************
        METHOD: GET TAGS BY TASK ID AND TAG
     ****************************************************/

    public function getTagsByTaskIdAndTag($data){
      $this->db->query('SELECT * FROM tags WHERE tag = :tag AND task_id = :task_id');

      // Bind values
      $this->db->bind(':tag', $data['tag']);
      $this->db->bind(':task_id', $data['task_id']);

      // Get the results
      $results = $this->db->resultSet();

       // Return the results
      return $results;
    }

    /****************************************************
        METHOD: REMOVE TAGS BY TASK ID
     ****************************************************/

    public function removeTagsByTaskId($id){
      $this->db->query('DELETE * FROM tags WHERE task_id = :task_id');

      // Bind values
      $this->db->bind(':task_id', $id);

      // Execute
      if($this->db->execute()){
        return true;
      } else {
        return false;
      }
    }


    /****************************************************
        FUNCTION: SUBMIT TAGS
     ****************************************************/

    function sumbitTags( $data ){

      if( !empty( $data['tag_array'] ) && $data['task_id'] ||
          !empty( $data['tag_array'] ) && $data['method_id'] ||
          !empty( $data['tag_array'] ) && $data['file_id'] ||
      ){

        foreach( $data['tag_array'] as $tag ){

          $this->addTag()
        }

      }
    } 

  }