<?php
  class Task
  {
      private $db;

      public function __construct()
      {
          $this->db = new Database;
      }

      /* ############################################################# */
      /* ################ GET MULTIPLE TASKS AT ONCE ################# */
      /* ############################################################# */

      /*************************************
          METHOD: GET ALL ACTIVE TASKS
       *************************************/

      public function getActiveTasks()
      {
          $this->db->query('SELECT *
                        FROM tasks
                        WHERE is_archived = :is_archived 
                        AND is_approved = :is_approved
                        ORDER BY tasks.created_at DESC
                        ');
          // Bind value
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_approved', 'true');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /*************************************
          METHOD: GET ALL UNARCHIVED TASKS
       *************************************/

      public function getTasks()
      {
          $this->db->query('SELECT *,
                          tasks.id as taskId,
                          users.id as userId,
                          tasks.created_at as taskCreated,
                          users.created_at as userCreated,
                          tasks.name as taskName,
                          users.lastname as userName
                          FROM tasks 
                          INNER JOIN users
                          ON tasks.user_id = users.id
                          WHERE is_archived = :is_archived
                          ORDER BY tasks.created_at DESC
                          ');
          // Bind value
          $this->db->bind(':is_archived', 'false');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /**********************************
          METHOD: GET ALL ACTIVE SINGLE TASKS
       **********************************/

      public function getActiveSingleTasks()
      {
          $this->db->query('SELECT tasks.*, organs.name as organname,
                        tasks.id as taskId,
                        users.id as userId,
                        tasks.created_at as taskCreated,
                        users.created_at as userCreated,
                        tasks.name as taskName,
                        users.lastname as userName
                        FROM tasks 
                        INNER JOIN users
                        ON tasks.user_id = users.id
                        LEFT JOIN organs
                        ON tasks.department = organs.id
                        WHERE tasks.is_archived = :is_archived AND tasks.is_singletask = :is_singletask
                        ORDER BY tasks.created_at DESC
                        ');
          // Bind value
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_singletask', true);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /**********************************
      METHOD: GET ALL TASKS OF PROCESSES
       **********************************/

      public function getAllTasksOfProcesses()
      {
          $this->db->query('SELECT *,
                        tasks.id as taskId,
                        users.id as userId,
                        tasks.created_at as taskCreated,
                        users.created_at as userCreated,
                        tasks.name as taskName,
                        users.lastname as userName,
                        organs.name as organname
                        FROM tasks 
                        INNER JOIN users
                        ON tasks.user_id = users.id
                        LEFT JOIN processes
                        ON tasks.process_id = processes.id
                        LEFT JOIN organs
                        ON tasks.department = organs.id
                        WHERE tasks.is_archived = :is_archived AND tasks.process_id IS NOT NULL
                        ORDER BY tasks.created_at DESC
                        ');
          // Bind value
          $this->db->bind(':is_archived', 'false');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }
    
      /**********************************
          METHOD: GET ALL ARCHIVED SINGLE TASKS
       **********************************/

      public function getArchivedSingleTasks()
      {
          $this->db->query('SELECT tasks.*, organs.name as organname,
                        tasks.id as taskId,
                        users.id as userId,
                        tasks.created_at as taskCreated,
                        users.created_at as userCreated,
                        tasks.name as taskName,
                        users.lastname as userName
                        FROM tasks 
                        INNER JOIN users
                        ON tasks.user_id = users.id
                        LEFT JOIN organs
                        ON tasks.department = organs.id
                        WHERE tasks.is_archived = :is_archived AND tasks.is_singletask = :is_singletask
                        ORDER BY tasks.created_at DESC
                        ');
          // Bind values
          $this->db->bind(':is_archived', 'true');
          $this->db->bind(':is_singletask', true) ;

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /**********************************
          METHOD: GET ALL SINGLE TASKS
       **********************************/

      public function getAllSingleTasks()
      {
          $this->db->query('SELECT *,
                        tasks.id as taskId,
                        users.id as userId,
                        tasks.created_at as taskCreated,
                        users.created_at as userCreated,
                        tasks.name as taskName,
                        users.lastname as userName,
                        organs.name as organname
                        FROM tasks 
                        INNER JOIN users
                        ON tasks.user_id = users.id
                        LEFT JOIN organs
                        ON tasks.department = organs.id
                        WHERE is_singletask = :is_singletask AND tasks.is_archived = :is_archived
                        ORDER BY tasks.created_at DESC
                        ');
      
          // Bind value
          $this->db->bind(':is_singletask', true);
          $this->db->bind(':is_archived', 'false');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /**********************************
      METHOD: GET ALL TASKS
       **********************************/

      public function getAllTasks()
      {
          $this->db->query('SELECT *,
                        tasks.id as taskId,
                        users.id as userId,
                        tasks.created_at as taskCreated,
                        users.created_at as userCreated,
                        tasks.name as taskName,
                        users.lastname as userName,
                        organs.name as organname
                        FROM tasks 
                        INNER JOIN users
                        ON tasks.user_id = users.id
                        LEFT JOIN processes
                        ON tasks.process_id = processes.id
                        LEFT JOIN organs
                        ON tasks.department = organs.id
                        ORDER BY tasks.created_at DESC
                        ');

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }


      /* ############################################################# */
      /* ##################### GET ONLY ONE TASK ##################### */
      /* ############################################################# */

      /**************************************
          METHOD: GET (ACTIVE) TASK BY ID
       **************************************/

      public function getTaskById($id)
      {
          $this->db->query('SELECT t1.*, t2.process_name, GROUP_CONCAT(t4.id SEPARATOR \', \') as qualiName
                                FROM tasks t1
                                LEFT JOIN processes t2
                                ON t1.process_id = t2.id 
                                LEFT JOIN qualifications_tasks t3
                                ON t1.id = t3.task_id
                                LEFT JOIN qualifications t4
                                ON t3.qualification_id = t4.id
                                WHERE t1.id = :id AND t1.is_archived = :is_archived AND t1.is_approved = :is_approved');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_approved', 'true');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /**************************************
         METHOD: GET SINGLE TASK BY ID
      **************************************/

      public function getSingleTaskById($id)
      {
          $this->db->query('SELECT * FROM tasks WHERE id = :id AND is_archived = :is_archived AND is_singletask = :is_singletask');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');
          $this->db->bind(':is_singletask', true);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /**************************************
         METHOD: GET ARCHIVED TASK BY ID
      **************************************/

      public function getArchivedTaskById($id)
      {
          $this->db->query('SELECT * FROM tasks WHERE id = :id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'true');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /******************************************
        METHOD: GET PROCESS TASK BY ID
      ********************************************/

      public function getProcessTaskById($id)
      {
          $this->db->query('SELECT * FROM tasks WHERE id = :id AND is_singletask = :is_singletask');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_singletask', false);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /**************************************
        METHOD: GET UNARCHIVED TASK BY ID

        So Single and Process Tasks, Unapproved and Approved Tasks
     **************************************/

      public function getUnarchivedTaskById($id)
      {
          $this->db->query('SELECT t1.*, GROUP_CONCAT(t3.name SEPARATOR \', \') as qualifications, GROUP_CONCAT(t5.norm SEPARATOR \', \') as isos
                                FROM tasks t1 
                                LEFT JOIN qualifications_tasks t2 
                                ON t2.task_id = :id 
                                INNER JOIN qualifications t3
                                ON t2.qualification_id = t3.id
                                LEFT JOIN isos_tasks t4
                                ON t4.task_id = :id 
                                INNER JOIN isos t5
                                ON t4.iso_id = t5.id
                                WHERE t1.id = :id AND is_archived = :is_archived');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'false');

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /******************************************
        METHOD: GET (ANY KIND OF) TASK BY ID
      ********************************************/

      public function getAnyTaskById($id)
      {
          $this->db->query('SELECT t1.*, GROUP_CONCAT(DISTINCT t3.name SEPARATOR \', \') as qualifications, GROUP_CONCAT(DISTINCT t5.norm SEPARATOR \', \') as isos
                                FROM tasks t1 
                                LEFT JOIN qualifications_tasks t2 
                                ON t2.task_id = :id 
                                INNER JOIN qualifications t3
                                ON t2.qualification_id = t3.id
                                LEFT JOIN isos_tasks t4 
                                ON t4.task_id = :id 
                                INNER JOIN isos t5
                                ON t4.iso_id = t5.id
                                WHERE t1.id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /*********************************
          METHOD: ADD (ACTIVE) TASK
       *********************************/

      public function addTask($data)
      {
          $taskBool = stringBoolToInt($data['is_singletask']);

          $this->db->query('INSERT INTO tasks (created_at, user_id, name, description, department, input, input_source, output, output_target, category, risk, status, required_knowledge_text, required_knowledge, required_knowledge_folders, roles_responsible, roles_concerned, roles_contributing, roles_information, check_intervals, reference_date, interval_count, interval_format, check_date, remind_before, remind_at, remind_after, required_resources_text, required_resources, required_resources_folders, existing_compentencies, required_education, tags, is_archived, is_singletask, is_approved) VALUES (:created_at, :user_id, :name, :description, :department, :input, :input_source, :output, :output_target, :category, :risk, :status, :required_knowledge_text, :required_knowledge, :required_knowledge_folders, :roles_responsible, :roles_concerned, :roles_contributing, :roles_information, :check_intervals, :reference_date, :interval_count, :interval_format, :check_date, :remind_before, :remind_at, :remind_after, :required_resources_text, :required_resources, :required_resources_folders, :existing_compentencies, :required_education, :tags, :is_archived, :is_singletask, :is_approved )');

          // Bind values
          $this->db->bind(':created_at', date('Y-m-d H:i:s'));
          $this->db->bind(':user_id', $data['user_id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':description', $data['description']);
          $this->db->bind(':department', $data['department']);
          $this->db->bind(':input', $data['input']);
          $this->db->bind(':input_source', $data['input_source']);
          $this->db->bind(':output', $data['output']);
          $this->db->bind(':output_target', $data['output_target']);
          $this->db->bind(':category', $data['category']);
          $this->db->bind(':risk', $data['risk']);
          $this->db->bind(':status', $data['status']);
          $this->db->bind(':required_knowledge_text', $data['required_knowledge_text']);
          $this->db->bind(':required_knowledge', $data['required_knowledge']);
          $this->db->bind(':required_knowledge_folders', $data['required_knowledge_folders']);
          $this->db->bind(':roles_responsible', $data['roles_responsible']);
          $this->db->bind(':roles_concerned', $data['roles_concerned']);
          $this->db->bind(':roles_contributing', $data['roles_contributing']);
          $this->db->bind(':roles_information', $data['roles_information']);
          $this->db->bind(':check_intervals', $data['check_intervals']);
          $this->db->bind(':reference_date', $data['reference_date']);
          $this->db->bind(':interval_count', $data['interval_count']);
          $this->db->bind(':interval_format', $data['interval_format']);
          $this->db->bind(':check_date', $data['check_date']);
          $this->db->bind(':remind_before', $data['remind_before']);
          $this->db->bind(':remind_at', $data['remind_at']);
          $this->db->bind(':remind_after', $data['remind_after']);
          $this->db->bind(':required_resources_text', $data['required_resources_text']);
          $this->db->bind(':required_resources', $data['required_resources']);
          $this->db->bind(':required_resources_folders', $data['required_resources_folders']);
          $this->db->bind(':existing_compentencies', $data['existing_compentencies']);
          $this->db->bind(':required_education', $data['required_education']);
          $this->db->bind(':tags', $data['tags']);
          $this->db->bind(':is_singletask', $taskBool);
          $this->db->bind(':is_approved', $data['is_approved']);
          // Make it active by default
          $this->db->bind(':is_archived', 'false');

          // Execute
          $last_id = $this->db->executeAndGetId();

          if ($last_id != false) {
              return $last_id;
          } else {
              return false;
          }
      }

      /***************************
          METHOD: UPDATE TASK
       ***************************/

      public function updateTask($data)
      {
          $this->db->query('UPDATE tasks SET name = :name, description = :description, department = :department, input = :input, input_source = :input_source, output = :output, output_target = :output_target, category = :category, risk = :risk, status = :status, required_knowledge_text = :required_knowledge_text, required_knowledge = :required_knowledge, roles_responsible = :roles_responsible, roles_concerned = :roles_concerned, roles_contributing = :roles_contributing, roles_information = :roles_information, check_intervals = :check_intervals, reference_date = :reference_date, interval_count = :interval_count, interval_format = :interval_format, check_date = :check_date, remind_before = :remind_before, remind_at = :remind_at, remind_after = :remind_after, required_resources_text = :required_resources_text, required_resources = :required_resources, existing_compentencies = :existing_compentencies, required_education = :required_education, tags = :tags WHERE id = :id');

          // Bind values
          $this->db->bind(':id', $data['id']);
          $this->db->bind(':name', $data['name']);
          $this->db->bind(':description', $data['description']);
          $this->db->bind(':department', $data['department']);
          $this->db->bind(':input', $data['input']);
          $this->db->bind(':input_source', $data['input_source']);
          $this->db->bind(':output', $data['output']);
          $this->db->bind(':output_target', $data['output_target']);
          $this->db->bind(':category', $data['category']);
          $this->db->bind(':risk', $data['risk']);
          $this->db->bind(':status', $data['status']);
          $this->db->bind(':required_knowledge_text', $data['required_knowledge_text']);
          $this->db->bind(':required_knowledge', $data['required_knowledge']);
          $this->db->bind(':roles_responsible', $data['roles_responsible']);
          $this->db->bind(':roles_concerned', $data['roles_concerned']);
          $this->db->bind(':roles_contributing', $data['roles_contributing']);
          $this->db->bind(':roles_information', $data['roles_information']);
          $this->db->bind(':check_intervals', $data['check_intervals']);
          $this->db->bind(':reference_date', $data['reference_date']);
          $this->db->bind(':interval_count', $data['interval_count']);
          $this->db->bind(':interval_format', $data['interval_format']);
          $this->db->bind(':check_date', $data['check_date']);
          $this->db->bind(':remind_before', $data['remind_before']);
          $this->db->bind(':remind_at', $data['remind_at']);
          $this->db->bind(':remind_after', $data['remind_after']);
          $this->db->bind(':required_resources_text', $data['required_resources_text']);
          $this->db->bind(':required_resources', $data['required_resources']);
          $this->db->bind(':existing_compentencies', $data['existing_compentencies']);
          $this->db->bind(':required_education', $data['required_education']);
          $this->db->bind(':tags', $data['tags']);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /****************************************
           METHOD: ADD PROCESS ID TO TASK
      ****************************************/

      public function addProcessIdToTask($process_id, $task_id)
      {
          $this->db->query('UPDATE tasks SET process_id = :process_id WHERE id = :id');

          // Bind values
          $this->db->bind(':id', $task_id);
          $this->db->bind(':process_id', $process_id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }




      /**************************************************
          METHOD: APPROVE TASK (FOR APPROVED PROCESSES)
       **************************************************/

      public function approveTask($id)
      {
          $this->db->query('UPDATE tasks SET is_approved = :is_approved WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_approved', 'true');

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /***************************
         METHOD: ARCHIVE TASK
      ***************************/

      public function archiveTask($id)
      {
          $this->db->query('UPDATE tasks SET is_archived = :is_archived WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);
          $this->db->bind(':is_archived', 'true');

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /* ###################################################################### */
      /* ##################### DB-TABLE: UNACCEPTED TASKS ##################### */
      /* ###################################################################### */

      /*************************************************
          METHOD: ADD AN UNACCEPTED ASSIGNMENT
       *************************************************/

      public function addUnacceptedAssignment($data)
      {
          $this->db->query('INSERT INTO unaccepted_tasks (task_id, user_id, role, organ, suborgan, assigned_at) VALUES (:task_id, :user_id, :role, :organ, :suborgan, :assigned_at)');

          // Bind values
          $this->db->bind(':task_id', $data['task_id']);
          $this->db->bind(':user_id', $data['user_id']);
          $this->db->bind(':role', $data['role']);
          $this->db->bind(':organ', (int)$data['organ_id'] ?? null);
          $this->db->bind(':assigned_at', date('Y-m-d H:i:s'));

          // If suborgan id is provided
          if (!empty($data['suborgan_id'])) {
              // PROVIDED
              // Bind value
              $this->db->bind(':suborgan', $data['organ_id']);
          } else {
              // EMPTY
              // Make it null
              $this->db->bind(':suborgan', null);
          }

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }
    
      /****************************************************
          METHOD: GET UNACCEPTED ASSIGNMENTS BY USER ID
       ****************************************************/

      public function getUnacceptedAssignmentsByUserId($id)
      {
          $this->db->query('SELECT * FROM unaccepted_tasks WHERE user_id = :user_id');
          // Bind value
          $this->db->bind(':user_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /*************************************************
          METHOD: GET UNACCEPTED ASSIGNMENTS BY TASK ID
       *************************************************/

      public function getUnacceptedAssignmentsByTaskId($id)
      {
          $this->db->query('SELECT * FROM unaccepted_tasks WHERE task_id = :task_id');
          // Bind value
          $this->db->bind(':task_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /*****************************************************
          METHOD: REMOVE UNACCEPTED ASSIGNMENTS BY TASK ID
       ******************************************************/

      public function removeUnacceptedAssignmentsByTaskId($id)
      {
          $this->db->query('DELETE FROM unaccepted_tasks WHERE task_id = :task_id');
          // Bind value
          $this->db->bind(':task_id', $id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }

      /* ###################################################################### */
      /* ###################### DB-TABLE: ACCEPTED TASKS ###################### */
      /* ###################################################################### */

      /*******************************************
          METHOD: GET ACCEPTED TASKS BY USER ID
       *******************************************/

      public function getAcceptedTasksByUserId($id)
      {
          $this->db->query('SELECT * FROM accepted_tasks WHERE user_id = :user_id');
          // Bind value
          $this->db->bind(':user_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }

      /************************************************
          METHOD: REMOVE ACCEPTED TASKS BY TASK ID
       ************************************************/

      public function removeAcceptedTasksByTaskId($id)
      {
          $this->db->query('DELETE FROM accepted_tasks WHERE task_id = :task_id');
          // Bind value
          $this->db->bind(':task_id', $id);

          // Execute
          if ($this->db->execute()) {
              return true;
          } else {
              return false;
          }
      }


      /* ###################################################################### */
      /* ######################### DB-TABLE: CHECKS ########################### */
      /* ###################################################################### */

      /*************************************************
          METHOD: GET CHECK BY ID
       *************************************************/

      public function getCheckById($id)
      {
          $this->db->query('SELECT * FROM checks_tasks WHERE id = :id');
          // Bind value
          $this->db->bind(':id', $id);

          // Get the row
          $row = $this->db->single();

          // Return the row
          return $row;
      }

      /*************************************************
          METHOD: GET CHECKS BY TASKID
       *************************************************/

      public function getChecksByTaskId($id)
      {
          $this->db->query('SELECT * FROM checks_tasks WHERE task_id = :task_id');
          // Bind value
          $this->db->bind(':task_id', $id);

          // Get the results
          $results = $this->db->resultSet();

          // Return the results
          return $results;
      }
  }
