<?php

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /*******************************
     * METHOD: REGISTER USER
     ********************************/
    public function register($data)
    {
        $this->db->query('INSERT INTO users (firstname, lastname, email, password) VALUES (:firstname,:lastname, :email, :password)');
        // Bind values
        $this->db->bind(':firstname', $data['firstname']);
        $this->db->bind(':lastname', $data['lastname']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', $data['password']);

        // Execute
        $last_id = $this->db->executeAndGetId();

        if ($last_id != false) {
            return $last_id;
        } else {
            return false;
        }
    }

    /*******************************
     * METHOD: LOGIN USER
     ********************************/
    public function login($email, $password)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        $hashed_password = $row->password;

        // Verify that the hashed password from the database and the unhashed password from the input match
        if (password_verify($password, $hashed_password)) {
            return $row;
        } else {
            return false;
        }
    }

    /***********************************
     * METHOD: FIND USER BY EMAIL
     ************************************/
    public function findUserByEmail($email)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        // Bind value
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        // Check row
        if ($this->db->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /***********************************
     * METHOD: GET USERS BY USER IDS
     ************************************/
    public function getUsersByCommaSeperatedIds($userlist)
    {
        $this->db->query('SELECT id, email, firstname, lastname FROM `users` WHERE FIND_IN_SET(`id`, :userlist)');
        $this->db->bind(':userlist', $userlist);

        return $this->db->resultSet();
    }

    /***********************************
     * METHOD: GET USERS
     ************************************/
    public function getUsers()
    {
        $this->db->query('SELECT * FROM users');

        $results = $this->db->resultSet();

        return $results;
    }

    /**********************************************
     * METHOD: GET USERS WITH ROLES (LASTNAME ASC)
     **********************************************/

    public function getUsersWithRoles()
    {
        $this->db->query('SELECT * FROM users as t1 LEFT JOIN rbac_user_roles as t2 ON t1.id = t2.user_id ORDER BY lastname ASC');

        $results = $this->db->resultSet();

        return $results;
    }

    /***********************************
     * METHOD: GET USER BY ID
     ************************************/
    public function getUserById($id)
    {
        $this->db->query('SELECT * FROM users WHERE id = :id');
        // Bind value
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    /***********************************
     * METHOD: GET USER BY EMAIL
     ************************************/
    public function getUserByEmail($email)
    {
        $this->db->query('SELECT * FROM users WHERE email = :email');
        // Bind value
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        return $row;
    }

    /***********************************
     * METHOD: UPDATE USER
     ************************************/
    public function updateUser($data)
    {
        $this->db->query('UPDATE users SET firstname = :firstname, lastname = :lastname, email = :email WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':firstname', $data['firstname']);
        $this->db->bind(':lastname', $data['lastname']);
        $this->db->bind(':email', $data['email']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /****************************************
     * METHOD: UPDATE USER - ABBREVIATION
     *****************************************/
    public function updateUserAbbr($id, $abbr)
    {
        $this->db->query('UPDATE users SET abbreviation = :abbreviation WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $id);
        $this->db->bind(':abbreviation', $abbr);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /****************************************************
     * METHOD: UPDATE USER PASSWORDCODE & TIMESTAMP
     *****************************************************/
    public function updateUserPasswordcode($data)
    {
        $this->db->query('UPDATE users SET passwordcode = :passwordcode, passwordcode_time = NOW() WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':passwordcode', $data['passwordcode']);
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /***********************************
     * METHOD: UPDATE USER PASSWORD
     ************************************/
    public function updateUserPassword($data)
    {
        $this->db->query('UPDATE users SET password = :password WHERE id = :id');
        // Bind values
        $this->db->bind(':id', $data['id']);
        $this->db->bind(':password', $data['password']);
        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Delete user by user id
     */
    public function deleteUser($id)
    {
        $this->db->query('DELETE FROM users WHERE id = :id');
        $this->db->bind(':id', $id);

        return $this->db->execute()
            ? true
            : false;
    }

    public function getUsersQualifications($user_id)
    {
        $this->db->query('SELECT t1.*, t2.name, t2.id, t2.expire_in_days
                            FROM qualifications_users t1
                            LEFT JOIN qualifications t2
                            ON t1.qualification_id = t2.id
                            WHERE user_id = :user_id');
        $this->db->bind(':user_id', $user_id);

        return $this->db->resultSet();
    }
}
