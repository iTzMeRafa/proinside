<?php
class PrivilegedUser
{
    private $user_id;
    private $email;
    private $password;

    public $roles = [];
    public $folderPerms = [];
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    // override User method
    public static function getByEmail($email)
    {
        $db = new Database;
        $db->query('SELECT * FROM users WHERE email = :email');
        // Bind value
        $db->bind(':email', $email);
        $result = $db->single();
    
        if (!empty($result)) {
            $privUser = new PrivilegedUser;
            $privUser->user_id = $result->id;
            $privUser->email = $email;
            $privUser->password = $result->password;
        
            // Get the user's role_ids(string)
            $privUser->db->query('SELECT role_ids FROM rbac_user_roles WHERE user_id = :user_id');
            // Bind value
            $privUser->db->bind(':user_id', $privUser->user_id);
            // Get the row
            $row = $privUser->db->single();

            if (!empty($row)) {
                // Put each role id into the role_ids array as the value
                $role_ids = explode(',', $row->role_ids);
                    
                // Loop through the role_ids
                foreach ($role_ids as $role_id) {
                            
                            // Get the role_name to the role_id
                    $db->query('SELECT * FROM rbac_roles WHERE role_id = :role_id');
                    // Bind value
                    $db->bind(':role_id', $role_id);
                    // Get the row
                    $row = $db->single();

                    // Fill the user's roles array
                    // For the array item's key: use the role name
                    // For the array item's value: get role's array of permissions
                    $privUser->roles[$row->role_name] = Role::getRolePerms($role_id);
                            
                    $privUser->folderPerms[$row->role_name] = explode(',', $row->folder_perms);
                }
            }
            unset($privUser->db);
            return $privUser;
        } else {
            return false;
        }
    }

    // check if user has a specific privilege
    public function hasPrivilege($perm)
    {
        foreach ($this->roles as $role) {
            if ($role->hasPerm($perm)) {
                return true;
            }
        }
        return false;
    }


    public function getPrivileges()
    {
        $output = [];

        foreach ($this->roles as $role) {
            // die(var_dump($role));
            foreach ($role as $key => $perm) {
                $output[] = $key;
            }
        }

        return $output;

        //die(var_dump($output));
    }

    public function hasAccess($folder_id)
    {
        foreach ($this->folderPerms as $role) {
            if (in_array('global', $role)) {
                return true;
            } elseif (in_array($folder_id, $role)) {
                return true;
            }
        }
        return false;
    }
}
