<?php
  class Role
  {
      protected $permissions;

      protected function __construct()
      {
          $this->permissions = array();
      }
    

      // return a role object with associated permissions
      public static function getRolePerms($role_id)
      {
          $db = new Database;
          $role = new Role;

          // Get the string of permission ids for the current role
          $db->query('SELECT perm_ids FROM rbac_role_perms WHERE role_id = :role_id');
          // Bind value
          $db->bind(':role_id', $role_id);
          // Get the row
          $row = $db->single();

          $perm_ids = explode(',', $row->perm_ids);

          // Fill the permissions array with the perm ids
          foreach ($perm_ids as $perm_id) {
              $role->permissions[$perm_id] = true;
          }

          return $role;
      }

      // check if a permission is set
      public function hasPerm($perm_id)
      {
          return isset($this->permissions[$perm_id]);
      }
  }
