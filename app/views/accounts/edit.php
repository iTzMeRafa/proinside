<?php require APPROOT . '/views/inc/header.php'; ?>
<a href="javascript:history.go(-1)" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

<h1>Account bearbeiten</h1>
<form action="<?php echo URLROOT; ?>/accounts/edit" method="post">
  <div class="form-group">
    <label for="Name">Vorname:</label>
    <input type="text" name="firstname" class="form-control form-control-lg <?php echo (!empty($data['firstname_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['firstname']; ?>">
    <span class="invalid-feedback"><?php echo $data['firstname_err']; ?></span>
  </div>
  <div class="form-group">
    <label for="Name">Nachname:</label>
    <input type="text" name="lastname" class="form-control form-control-lg <?php echo (!empty($data['lastname_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['lastname']; ?>">
    <span class="invalid-feedback"><?php echo $data['lastname_err']; ?></span>
  </div>
  <div class="form-group">
    <label for="email">E-Mail:</label>
    <input type="email" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>">
    <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
  </div>
  <input class="btn btn-success" type="submit" value="Aktualisieren">
</form>


<?php require APPROOT . '/views/inc/footer.php'; ?>
