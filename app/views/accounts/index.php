<?php require APPROOT . '/views/inc/header.php'; ?>
<?php // Echo out the flash Message in case there is one
  displayFlash('user_message'); ?>
<h1>Mein Account</h1>
<div class="card">
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Vorname: <?php echo $data['user']->firstname; ?></li>
    <li class="list-group-item">Nachname: <?php echo $data['user']->lastname; ?></li>
    <li class="list-group-item">E-Mail: <?php echo $data['user']->email; ?></li>
  </ul>
</div>
<a href="<?php echo URLROOT; ?>/accounts/edit" class="btn btn-pe-lightgreen mt-3">Accountdetails bearbeiten</a>
<a href="<?php echo URLROOT; ?>/users/forgotpass" class="btn btn-pe-darkgreen mt-3">Passwort zurücksetzen</a>

<?php require APPROOT . '/views/inc/footer.php'; ?>
