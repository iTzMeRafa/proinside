<!-- Sortable Table -->
<div class="col-md-12">
    <table id="backupList" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">ID</th>
            <th style="border: none;">Name</th>
            <th style="border: none;">Erstellt am</th>
            <th style="border: none;">Aktion</th>
        </tr>
        </thead>
        <tbody>

        <?php

        ?>

        <?php foreach ($data['backups'] as $backup) : ?>
            <tr>
                <td><?= $backup->id; ?></td>
                <td><?= $backup->name; ?></td>
                <td><?= $backup->created_at; ?></td>
                <td>
                    <a href="<?= URLROOT; ?>/<?= $backup->zip_path; ?>" download class="btn btn-pe-lightgreen">
                        <i class="fas fa-download mr-2"></i> Download
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->