<div class="row">
    <div class="col-md-6 offset-md-3">

        <?php displayFlash('createBackupState'); ?>

        <form method="post" action="<?= URLROOT; ?>/BackupsCreate/post">
            <div class="form-group">
                <label for="backupName">Name des Backups</label>
                <input
                    type="text"
                    name="backupName"
                    class="form-control"
                    id="backupName"
                    placeholder="Name"
                />
            </div>
            <div class="form-group">
                <label for="backupName">Format</label>
                <div class="custom-control custom-radio">
                    <input type="radio" id="fullBackup" value="fullBackup" name="backupFormat" class="custom-control-input" checked>
                    <label class="custom-control-label" for="fullBackup">Vollbackup (Hochgeladene Dateien + Datenbankeinträge)</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="fileBackup" value="fileBackup" name="backupFormat" class="custom-control-input">
                    <label class="custom-control-label" for="fileBackup">File Backup (Hochgeladene Dateien)</label>
                </div>
                <div class="custom-control custom-radio">
                    <input type="radio" id="databaseBackup" value="databaseBackup" name="backupFormat" class="custom-control-input">
                    <label class="custom-control-label" for="databaseBackup">Database Backup (Datenbankeinträge)</label>
                </div>
            </div>
            <div class="text-center">
                <button
                    type="submit"
                    name="backupCreate"
                    class="btn btn-primary btn-lg float-right"
                >
                    Backup Erstellen
                </button>
            </div>
        </form>

    </div>
</div>

