
<!-- Initialization -->
<?php
$pageTitle = 'Archivierte';
?>

<?php require APPROOT . '/views/inc/header.php'; ?>

<?php require APPROOT . '/views/business_models/templates/title.php'; ?>

<!-- Load Archived Listing -->
<?php require APPROOT . '/views/business_models/templates/archivedListing.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
