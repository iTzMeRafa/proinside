<!-- Form beginn -->
<form method="POST" action="<?= URLROOT; ?>/BusinessModelsEditPost" enctype="multipart/form-data">

    <!-- Initialization -->
    <?php
        $pageTitle = 'Bearbeiten';
        $businessData = $data['businessModel'];
        $businessDataMain = $data['businessModel']['businessModel'];
        $businessDataTopics = $data['businessModel']['businessModelTopics'];
    ?>

    <?php require APPROOT . '/views/inc/header.php'; ?>

    <?php require APPROOT . '/views/business_models/templates/title.php'; ?>

    <?php require APPROOT . '/views/business_models/templates/headBarEdit.php'; ?>



        <?php require APPROOT . '/views/business_models/templates/headerEdit.php'; ?>

        <!-- Show Error Message -->
        <?php displayFlash('business_model_edit_failure'); ?>

        <!-- Load TopicsListing -->
        <?php require APPROOT . '/views/business_models/templates/topicsListingEdit.php'; ?>

</form>

<?php require APPROOT . '/views/inc/footer.php'; ?>
