
    <!-- Initialization -->
    <?php
        $pageTitle = 'Ansehen';
        $businessData = $data['businessModel'];
        $businessDataMain = $data['businessModel']['businessModel'];
        $businessDataTopics = $data['businessModel']['businessModelTopics'];
    ?>

    <?php require APPROOT . '/views/inc/header.php'; ?>

    <?php require APPROOT . '/views/business_models/templates/title.php'; ?>

    <?php require APPROOT . '/views/business_models/templates/headBarIndex.php'; ?>



    <?php require APPROOT . '/views/business_models/templates/headerIndex.php'; ?>

    <!-- Show Success Message -->
    <?php displayFlash('business_model_edit_success'); ?>

    <!-- Load TopicsListing -->
    <?php require APPROOT . '/views/business_models/templates/topicsListingIndex.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
