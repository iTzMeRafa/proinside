<!-- Sortable Table -->
<div class="mt-5">
<table id="businessModelsArchived" class="table table-striped table-bordered" style="width: 100%">
    <thead>
    <tr style="background-color: #0B676E; color: white; border: none;">
        <th style="border: none;">ID/Revision</th>
        <th style="border: none;">Erstellt am</th>
        <th style="border: none;">Logo</th>
        <th style="border: none;">Aktionen</th>
    </tr>
    </thead>
    <tbody>

    <?php foreach ($data['businessModelArchived'] as $businessModel) : ?>
        <tr>
            <td><a href="<?php echo URLROOT; ?>/BusinessModelsArchived/show/<?php echo $businessModel->id; ?>">R<?= $businessModel->id; ?></a></td>
            <td><?= $businessModel->created_at; ?></td>
            <td>
                <?php if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/' .$businessModel->logoPath)): ?>
                    <img src="<?= URLROOT. '/'. $businessModel->logoPath; ?>" class="img-fluid" style="max-height: 100px;"/>
                <?php else: ?>
                    Kein Logo archiviert.
                <?php endif; ?>
            </td>
            <td>
                <a href="<?php echo URLROOT; ?>/BusinessModelsArchived/show/<?php echo $businessModel->id; ?>" class="btn btn-pe-lightgreen">Anzeigen</a>
                <a href="<?php echo URLROOT; ?>/BusinessModelsArchived/setActive/<?php echo $businessModel->id; ?>" class="btn btn-warning">Als Aktives Festlegen</a>
            </td>
        </tr>
    <?php endforeach; ?>

    </tbody>
</table>
</div>
<!-- /Sortable Table -->