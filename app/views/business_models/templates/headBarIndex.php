<?php
/***
 * Header Bar for the business models
 */
?>
<div class="row">
    <div class="col-md-12">
        <a href="<?php echo URLROOT; ?>/BusinessModelsEdit" class="h1 btn btn-pe-lightgreen">
            <i class="fas fa-pencil-alt mr-2"></i>Unternehmensmodell bearbeiten
        </a>
        <a href="<?= URLROOT; ?>/BusinessModelsArchived" class="h1 btn btn-dark"><i class="fas fa-trash mr-2"></i>Archivierte</a>
    </div>
</div>
