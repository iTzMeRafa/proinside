<!-- Logo - New Logo -->
<div class="row mb-5">

    <div class="col-md-4">

        <div class="card">
            <img class="card-img- p-3 img-fluid" src="<?= URLROOT . '/' . $businessDataMain->logoPath; ?>" alt="Card image cap" id="businessLogoPreview" style="max-height: 300px;">
            <div class="card-body">
                <h5 class="card-title">Unternehmens Logo</h5>
                <p class="card-text">Wenn das Logo beibehalten werden soll, nehmen Sie keine Änderungen vor.</p>

                <div class="custom-file">
                    <input name="businessLogo" id="businessLogoInput" type="file" class="custom-file-input">
                    <label class="custom-file-label" for="businessLogoInput">Datei auswählen...</label>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-8">

        <!-- Infotext for adding/removing subtopics -->
        <div class="card">
            <div class="card-header">
                Informationen
            </div>
            <div class="card-body">

                <!-- Logo Bedingungen -->
                <h5 class="card-title">Logo Bedingungen</h5>
                <ul class="list-group">
                    <li class="list-group-item">Erlaubte Dateiformate: <strong>PNG, JPG, JPEG, GIF</strong></li>
                    <li class="list-group-item">Maximale Dateigröße: <strong>2 MB</strong></li>
                </ul>
                <hr>
                <!-- Themen/Unterthemen Info -->
                <h5 class="card-title">Themen & Unterthemen</h5>
                <p class="card-text">Unterthemen werden derzeit im Dateimanager hinzugefügt oder gelöscht. <br /> Erstellen Sie dazu im Hauptordner einen Unterordner mit gewünschtem Themennamen.</p>
                <a href="<?php echo URLROOT; ?>/folders" class="btn btn-primary">Zum Dateimanager</a>
            </div>
        </div>

    </div>

</div>