<div class="row align-items-center mb-5">

    <div class="col-md-6">

        <!-- Jumbotron -->
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Revision Nr.: <?= $businessDataMain->id; ?> / <?= $businessDataMain->count; ?></h5>
                <h6 class="card-subtitle mb-2 text-muted"><?= $businessDataMain->created_at; ?></h6>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <!-- Logo -->
        <img src="<?= URLROOT. '/'. $businessDataMain->logoPath; ?>" class="img-fluid" style="max-height: 300px;"/>
    </div>

</div>