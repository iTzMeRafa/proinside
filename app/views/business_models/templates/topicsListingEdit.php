<div class="row">

    <!-- Topics Looping -->
    <?php foreach ($businessDataTopics as $topicKey => $topic): ?>
        <div class="col-md-3">

            <!-- Topics Card Style -->
            <div class="card mb-3 text-white bg-primary">
                <div class="card-header">
                    <input class="form-control" type="text" value="<?= $topic->name; ?>" name="topicInput[]" />
                    <input type="hidden" name="topicFolderID[]" value="<?= $topic->folder_id; ?>" />
                </div>
            </div>

            <!-- Subtopics Accordion Style -->
            <div class="row mb-5">
                <div class="col-md-12">

                    <!-- Display hint when no subtopics available -->
                    <?php if (count((array)$topic->subtopics) <= 0): ?>
                        <input type="hidden" name="subtopics<?= $topicKey; ?>[]" />
                        <div class="alert alert-info" role="alert">
                            Es existieren keine Unterthemen für dieses Hauptthema
                        </div>
                    <?php endif; ?>



                    <!-- Accordion -->
                    <div class="accordion" id="accordion<?= $topicKey; ?>">

                        <?php foreach ($topic->subtopics as $subtopicKey => $subtopic): $primaryKey = $topicKey . $subtopicKey; ?>
                            <div class="card">
                                <div class="card-header" id="heading<?= $primaryKey; ?>">
                                    <h5 class="mb-0">
                                        <button type="button" class="btn btn-link topicsCollapseButton" data-toggle="collapse" data-target="#collapse<?= $primaryKey; ?>" aria-expanded="<?= $subtopicKey == 0 ? 'true' : 'false'; ?>" aria-controls="collapse<?= $primaryKey; ?>" style="width: 100%;">
                                            <input class="form-control" type="text" value="<?= $subtopic->name; ?>" name="subtopics<?= $topicKey; ?>[]" />
                                            <input type="hidden" name="subtopicsFolderID<?= $topicKey; ?>[]" value="<?= $subtopic->folder_id; ?>" />
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse<?= $primaryKey; ?>" class="collapse" aria-labelledby="heading<?= $primaryKey; ?>" data-parent="#accordion<?= $topicKey; ?>">
                                    <div class="card-body">
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/folders">Willst Du ein Dokument hochladen?</a>
                                            </li>
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/processes/add/<?= $subtopic->folder_id; ?>">Willst Du einen Prozess erstellen?</a>
                                            </li>
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/tasks/add">Willst du eine Aufgabe erstellen?</a>
                                            </li>
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/methods/add">Willst du eine Maßnahme erstellen?</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>

        </div>
    <?php endforeach; ?>

</div>