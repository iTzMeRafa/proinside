<div class="row">

    <!-- Topics Looping -->
    <?php foreach ($businessDataTopics as $topicKey => $topic): ?>
        <div class="col-md-3">

            <!-- Topics Style -->
            <div class="card mb-3 text-white bg-primary">
                <div class="card-header">
                    <a href="<?= URLROOT; ?>/folders/jump/<?= $topic->folder_id; ?>">
                        <?= $topic->name; ?>
                    </a>
                        <!-- Show PDF Button if folder has files -->
                        <?php if (is_object($topic->filesExist)): ?>
                            <a href="<?= URLROOT; ?>/listings/index/<?= $topic->folder_id; ?>"><i class="text-white float-right fas fa-file-pdf fa-2x"></i></a>
                        <?php endif; ?>
                </div>
            </div>

            <!-- Subtopics Style -->
            <div class="row mb-5">
                <div class="col-md-12">

                    <!-- Display hint when no subtopics available -->
                    <?php if (count((array)$topic->subtopics) <= 0): ?>
                        <div class="alert alert-info" role="alert">
                            Es existieren keine Unterthemen für dieses Hauptthema
                        </div>
                    <?php endif; ?>

                    <!-- Accordion -->
                    <div class="accordion" id="accordion<?= $topicKey; ?>">
                        <?php foreach ($topic->subtopics as $subtopicKey => $subtopic): $primaryKey = $topicKey . $subtopicKey; ?>
                            <div class="card">
                                <div class="card-header" id="heading<?= $primaryKey; ?>">



                                    <h5 class="mb-0">
                                        <button class="btn btn-link <?= $subtopicKey != 0 ? 'collapsed' : ''; ?> topicsCollapseButton" data-toggle="collapse" data-target="#collapse<?= $primaryKey; ?>" aria-expanded="<?= $subtopicKey == 0 ? 'true' : 'false'; ?>" aria-controls="collapse<?= $primaryKey; ?>">
                                            <?= $subtopic->name; ?>
                                        </button>
                                    </h5>
                                </div>

                                <div id="collapse<?= $primaryKey; ?>" class="collapse <?= $subtopicKey == 0 ? 'show' : ''; ?>" aria-labelledby="heading<?= $primaryKey; ?>" data-parent="#accordion<?= $topicKey; ?>">
                                    <div class="card-body">
                                        <ul class="list-group list-group-flush">

                                            <!-- Jump to Folder -->
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/folders/jump/<?= $subtopic->folder_id; ?>"><i class="fas fa-folder-open mr-2"></i> Zum Ordner springen</a>
                                            </li>

                                            <!-- Show PDF Button if folder has files -->
                                            <?php if (is_object($subtopic->filesExist)): ?>
                                                <li class="list-group-item">
                                                    <a href="<?= URLROOT; ?>/listings/index/<?= $subtopic->folder_id; ?>"><i class="fas fa-file-pdf mr-2"></i> Zu den Dokumenten</a>
                                                </li>
                                            <?php endif; ?>

                                            <!-- Upload Document -->
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/folders/jump/<?= $subtopic->folder_id; ?>?action=openUploadModal"><i class="fas fa-file-upload mr-2"></i> Dokument hochladen</a>
                                            </li>

                                            <!-- Create Process -->
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/processes/add/<?= $subtopic->folder_id; ?>"><i class="fas fa-cogs mr-2"></i> Prozess erstellen</a>
                                            </li>

                                            <!-- Create Task -->
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/tasks/add"><i class="fas fa-tasks mr-2"></i> Aufgabe erstellen</a>
                                            </li>

                                            <!-- Create Method -->
                                            <li class="list-group-item">
                                                <a href="<?= URLROOT; ?>/methods/add"><i class="fas fa-thumbtack mr-2"></i> Maßnahme erstellen</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>

        </div>
    <?php endforeach; ?>

</div>