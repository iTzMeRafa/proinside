<?php
require APPROOT . '/views/inc/header.php';
$startDir = !empty($data['folder_id']) ? $data['folder_id'] : 0;
$startDirName = !empty($data['folder_name']) ? $data['folder_name'] : 'Hauptordner';
?>

<div id="alertbox"></div>
<h1 class="dateimananger">
    <i class="fas fa-folder mr-2"></i>Dateimanager
</h1>
<p>Hier können Sie das Dateisystem bearbeiten.</p>
<h2>
    <i class="fa fa-folder"></i> <span id="currentDirectory">Hauptordner</span>
</h2>

<?php if ($data['privUser']->hasPrivilege('16') == true) : ?>
    <button id="create_folder" type="button" name="create_folder" class="btn btn-pe-darkgreen">Ordner erstellen</button>
<?php endif; ?>

<?php if ($data['privUser']->hasPrivilege('18') == true) : ?>
    <button id="upload_data" type="button" name="upload_data" class="btn btn-pe-lightgreen">Datei hochladen</button>
<?php endif; ?>

<?php if ($data['privUser']->hasPrivilege('22') == true) : ?>
    <a href="<?php echo URLROOT; ?>/processes/add/12" id="create_process" class="btn btn-pe-darkgreen d-none"> Prozess
        erstellen
    </a>
<?php endif; ?>

<a href="<?php echo URLROOT; ?>/listings/index/1" id="show_infomaterial" class="btn btn-pe-darkgreen d-none">
    Infomaterial ansehen
</a>

<div
        id="folder_table"
        data-startDir="<?= $startDir; ?>"
        data-startDirName="<?= $startDirName; ?>"
        class="container-fluid mt-4"
>
</div>

<!--- FILE-INFO MODAL -->
<?php require APPROOT . '/views/folders/templates/fileInfoModal/index.php'; ?>

<!--- FILE MODAL -->
<?php require APPROOT . '/views/folders/templates/fileModal/index.php'; ?>

<!--- FOLDER MODAL -->
<?php require APPROOT . '/views/folders/templates/folderModal/index.php'; ?>

<!--- UPLOAD MODAL -->
<?php require APPROOT . '/views/folders/templates/uploadModal/index.php'; ?>

<!--- CONFIRM MODAL -->
<?php require APPROOT . '/views/folders/templates/confirmModal/index.php'; ?>

<!-- FILE USER NOTIFICATION MODAL -->
<?php require APPROOT . '/views/folders/templates/fileUserNotificationModal/index.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
