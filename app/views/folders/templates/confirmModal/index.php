<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title"><span id="change_title">Bestätigung erforderlich!</span></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p class="h6">Sind Sie sicher, dass Sie diese Datei löschen möchten?</p>
                <p class="data">Ausgewählte Datei: <strong id="selectedFile"></strong></p>
            </div>
            <div class="modal-footer">
                <button id="acceptDel" type="button" class="btn btn-danger">Löschen &#40;Archivieren&#41;</button>
                <button type="button" class="btn btn-dark" data-dismiss="modal">Schließen</button>
            </div>

        </div>
    </div>
</div>


<!-- Breaks have to be here! Code overflow error on modals, code will stop beeing executed otherwise. Weird bug -->
<br />
<br />
<br />
<br />
<br />