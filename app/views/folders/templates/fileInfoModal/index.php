<div id="infoModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span id="change_title">Informationen</span></h4>
                <button type="button" class="close closeInfo" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p class="h5">Datei:</p>
                <p id="file_info_name"></p>
                <p class="h5">Erstellt von:</p>
                <p id="file_info_approved_by"></p>
                <p class="h5">Freigegeben am:</p>
                <p id="file_info_approved_at"></p>
                <p class="font-weight-bold mb-0">Tags:</p>
                <div id="tag-list">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="closeInfo btn btn-default" data-dismiss="modal">Schließen</button>
            </div>
        </div>
    </div>
</div>