<div id="fileModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span id="change_title">Datei umbenennen</span></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p>Dateinamen eingeben</p>
                <input type="text" name="file_name" id="file_name" class="form-control">
                <br>
                <input type="hidden" name="file_id" id="file_id">
                <input type="hidden" name="file_extension" id="file_extension">
                <input type="hidden" name="old_file_name" id="old_file_name">
                <input type="button" name="file_button" id="file_button" class="btn btn-info" value="Umbenennen">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
            </div>
        </div>
    </div>
</div>