<form id="user_notification_form" method="post">
    <div class="modal fade" id="approve_file_notification" tabindex="-1" role="dialog" aria-labelledby="fileUserNotificationLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="fileUserNotificationLabel">Benachrichtigung einstellen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php require APPROOT . '/views/folders/templates/fileUserNotificationModal/userList.php'; ?>
                </div>
                <div class="modal-footer">
                    <?php require APPROOT . '/views/folders/templates/fileUserNotificationModal/footer.php'; ?>
                </div>
            </div>
        </div>
    </div>
    <?php require APPROOT . '/views/folders/templates/fileUserNotificationModal/hiddenInputs.php'; ?>
</form>