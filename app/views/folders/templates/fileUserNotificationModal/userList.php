<h6>Hier können Sie nochmals die Benachrichtigungseinstellungen für diese Datei überprüfen und ändern.</h6>
<div id="notifiedUsersList"></div>

<div class="alert alert-success" id="fileUserNotificationListAlert" role="alert" style="display: none;">
    Die Änderungen wurden erfolgreich gespeichert!
</div>

<div class="alert alert-danger" id="fileUserNotificationListAlertDanger" role="alert" style="display: none;">
    Etwas ist schief gelaufen.
</div>

<div class="modal-list mt-3" id="fileUploadUserList">

    <?php if (!empty($data['organs'])) : ?>

        <div class="accordion" id="accordionExample">

            <?php foreach ($data['organs'] as $organ) : ?>

                <div class="card">
                    <div class="card-header" id="headingOne">
                        <h2 class="mb-0">
                            <button class="btn btn-link w-100 text-left" type="button" data-toggle="collapse" data-target="#responsible-<?php echo $organ->id; ?>" aria-expanded="true" aria-controls="responsible-<?php echo $organ->id; ?>">
                                <?php echo $organ->name; ?>
                            </button>
                        </h2>
                    </div>

                    <div id="responsible-<?php echo $organ->id; ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">

                            <?php if (!empty($organ->id == 2)) : ?>
                                <?php foreach ($organ->suborgans as $suborgan) : ?>
                                    <?php foreach ($suborgan->users as $user) : ?>
                                        <div class="form-check">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   value="<?php echo $user->id.'_'.$organ->id.'_'.$suborgan->id; ?>"
                                                   name="notificationUserList[]"
                                                   data-name="<?php echo $user->firstname.' '.$user->lastname; ?>"
                                                   data-organ="<?php echo $suborgan->name;
                                                   ?>">
                                            <label class="form-check-label" for="<?php echo $user->lastname.''.$id; ?>"><?php echo $user->firstname.' '.$user->lastname.' ('.$suborgan->name.')'; ?></label>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <?php foreach ($organ->users as $user) : ?>
                                    <div class="form-check">
                                        <input class="form-check-input"
                                               type="checkbox"
                                               value="<?php echo $user->id.'_'.$organ->id; ?>"
                                               name="notificationUserList[]"
                                               data-name="<?php echo $user->firstname.' '.$user->lastname; ?>"
                                               data-organ="<?php echo $organ->name;
                                               ?>">
                                        <label class="form-check-label" for="<?php echo $user->lastname.''.$id; ?>"><?php echo $user->firstname.' '.$user->lastname; ?></label>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>

    <?php endif; ?>

</div>