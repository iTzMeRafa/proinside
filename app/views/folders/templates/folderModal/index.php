<div id="folderModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span id="change_title">Ordner erstellen</span></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning" role="alert">
                    Bitte achten Sie auf eine korrekte Bezeichnung. Die Ordner können im nachhinein nicht mehr umbenannt
                    werden.
                </div>
                <p>Ordnernamen eingeben</p>
                <input type="text" name="folder_name" id="folder_name" class="form-control">
                <br>

                <!-- Default, Topic, Subtopic Radios -->
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="businessModelType" id="noModel" value="noModel"
                           checked>
                    <label class="form-check-label" for="noModel">
                        Standard (Normaler Ordner)
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="businessModelType" id="isTopic" value="isTopic">
                    <label class="form-check-label" for="isTopic">
                        Ist Hauptthema
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="businessModelType" id="isSubtopic"
                           value="isSubtopic">
                    <label class="form-check-label" for="isSubtopic">
                        Ist Unterthema von dem Ordner <strong id="subtopicOfTopicName">Hauptordner</strong>
                    </label>
                </div>

                <br/>

                <!-- Set sortposition for folder -->
                <label class="form-check-label" for="sortPositionFolder">
                    Ordner Position festlegen
                </label>
                <select id="sortPositionFolder" name="sortPositionFolder" class="form-control">
                    <!-- Optionfelder hier -->
                </select>


                <br/>

                <input type="hidden" name="action" id="action">
                <input type="hidden" name="old_name" id="old_name">
                <input type="hidden" name="folder_id" id="folder_id">
                <input type="hidden" name="istopicFolder" id="istopicFolder">
                <input type="hidden" name="issubtopicFolder" id="issubtopicFolder">
                <input type="button" name="folder_button" id="folder_button" class="btn btn-pe-darkgreen"
                       value="Erstellen">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
            </div>
        </div>
    </div>
</div>