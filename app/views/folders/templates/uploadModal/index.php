<form id="upload_form" method="post" enctype="multipart/form-data">
    <div id="uploadModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span id="change_title">Datei hochladen</span></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">

                    <div class="row">

                        <!-- Left -->
                        <div class="col-md-6">

                            <!-- Headline -->
                            <?php require APPROOT . '/views/folders/templates/uploadModal/headline.php'; ?>

                            <!-- File Select -->
                            <?php require APPROOT . '/views/folders/templates/uploadModal/fileSelect.php'; ?>

                            <!-- Tags -->
                            <?php require APPROOT . '/views/folders/templates/uploadModal/tags.php'; ?>

                            <!-- Needed hidden inputs -->
                            <?php require APPROOT . '/views/folders/templates/uploadModal/hiddenInputs.php'; ?>
                        </div>

                        <!-- Right -->
                        <div class="col-md-6">

                            <!-- Checkbox -->
                            <?php require APPROOT . '/views/folders/templates/uploadModal/showCheckbox.php'; ?>

                            <!-- User Listing -->
                            <?php require APPROOT . '/views/folders/templates/uploadModal/userListing.php'; ?>

                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <?php require APPROOT . '/views/folders/templates/uploadModal/footer.php'; ?>
                </div>
            </div>
        </div>
    </div>
</form>