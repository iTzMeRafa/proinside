<div class="mt-5">
    <p class="mb-1"><strong>Schlagwörter</strong> - bitte mit Komma trennen.</p>
    <div class="tags-input w-100" data-name="tags-input">
        <?php if (!empty($data['tags_array'])) : ?>
            <?php foreach ($data['tags_array'] as $tag): ?>
                <span class="tag"><?php echo $tag; ?> <i class="close fas fa-times"></i></span>
            <?php endforeach; ?>
        <?php endif; ?>
        <input type="text" class="mainInput w-100">
        <input name="tags" type="hidden" class="hiddenInput" value="">
    </div>
</div>