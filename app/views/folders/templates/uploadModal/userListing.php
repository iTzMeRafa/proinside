<div class="modal-list mt-3" id="fileUploadUserList" style="display: none;">

    <?php if (!empty($data['organs'])) : ?>

        <div class="accordion" id="userListingAccordion">

            <?php foreach ($data['organs'] as $key => $organ) : ?>

                <div class="card">
                    <div class="card-header" id="heading<?= $key; ?>">
                        <h2 class="mb-0">
                            <button class="btn btn-link w-100 text-left" type="button" data-toggle="collapse" data-target="#userListing<?= $key; ?>" aria-expanded="<?= $key == 0 ? 'true' : ''; ?>" aria-controls="userListing<?= $key; ?>">
                                <?php echo $organ->name; ?>
                            </button>
                        </h2>
                    </div>

                    <div id="userListing<?= $key; ?>" class="collapse <?= $key == 0 ? 'show' : ''; ?>" aria-labelledby="heading<?= $key; ?>" data-parent="#userListingAccordion">
                        <div class="card-body">

                            <?php if (!empty($organ->id == 2)) : ?>
                                <?php foreach ($organ->suborgans as $suborgan) : ?>
                                    <?php foreach ($suborgan->users as $user) : ?>
                                        <div class="form-check">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   value="<?php echo $user->id.'_'.$organ->id.'_'.$suborgan->id; ?>"
                                                   name="notificationUserList[]"
                                                   data-name="<?php echo $user->firstname.' '.$user->lastname; ?>"
                                                   data-organ="<?php echo $suborgan->name;
                                                   ?>"
                                            />
                                            <label class="form-check-label" for="notificationUserList[]"><?php echo $user->firstname.' '.$user->lastname.' ('.$suborgan->name.')'; ?></label>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <?php foreach ($organ->users as $user) : ?>
                                    <div class="form-check">
                                            <input class="form-check-input"
                                                   type="checkbox"
                                                   value="<?php echo $user->id.'_'.$organ->id; ?>"
                                                   name="notificationUserList[]"
                                                   data-name="<?php echo $user->firstname.' '.$user->lastname; ?>"
                                                   data-organ="<?php echo $organ->name;
                                                   ?>"
                                            />
                                        <label class="form-check-label" for="notificationUserList[]"><?php echo $user->firstname.' '.$user->lastname; ?></label>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>

                        </div>
                    </div>
                </div>

            <?php endforeach; ?>

        </div>

    <?php endif; ?>

</div>