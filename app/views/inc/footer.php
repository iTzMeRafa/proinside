</main>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="search_results_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="search-results" class="border">
                    <ul class="list-group">
                        <li class="list-group-item">Dapibus ac facilisis in</li>
                        <li class="list-group-item">Morbi leo risus</li>
                        <li class="list-group-item">Porta ac consectetur ac</li>
                        <li class="list-group-item">Vestibulum at eros</li>
                    </ul>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- Global Config -->
<script src="<?= URLROOT; ?>/js/config.dev.js"></script>

<!-- JavaScript CNDS -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= URLROOT; ?>/js/bootstrap-multiselect.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- SortTables -->
<script src="<?= URLROOT; ?>/js/sortTables.js"></script>

<!-- Multiselect -->
<script src="<?= URLROOT; ?>/js/multiSelect.js"></script>

<!-- Datepicker -->
<script src="<?= URLROOT; ?>/js/datepicker.js"></script>

<script src="<?php echo URLROOT; ?>/js/main.js"></script>
<script src="<?php echo URLROOT; ?>/js/search.js"></script>
<script src="<?php echo URLROOT; ?>/js/helper.js"></script>
<script>
  $(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
      $('#sidebar').toggleClass('active');
      $('#sidebarCollapse').toggleClass('active');
      $('#sideBarCollapseArrowLeft').toggleClass('d-none');
      $('#sideBarCollapseArrowRight').toggleClass('d-none');
    });
  });

  $(document).ready(function () {
    $('#globalSearchForm').on('click', function () {

      if ($("#sidebar").hasClass("active")) {
        $('#sidebar').toggleClass('active');
        $('#sideBarCollapseArrowLeft').toggleClass('d-none');
        $('#sideBarCollapseArrowRight').toggleClass('d-none');
      }
    });
  });
</script>

<script>
    jQuery(document).ready(function() {
        $("#org1").jOrgChart({
            chartElement : '#chart1',
            dragAndDrop  : true
        });
    });
    jQuery(document).ready(function() {
        $("#org2").jOrgChart({
            chartElement : '#chart2',
            dragAndDrop  : true
        });
    });
    jQuery(document).ready(function() {
        $("#org3").jOrgChart({
            chartElement : '#chart3',
            dragAndDrop  : true
        });
    });
    jQuery(document).ready(function() {
        $("#org4").jOrgChart({
            chartElement : '#chart4',
            dragAndDrop  : true
        });
    });
</script>

<?php
switch ($data['bodyClass']) {
    ///// BUSINESSMODEL
    case 'businessmodels':
        echo '<script src="' . URLROOT . '/js/businessmodels/addRemoveFields.js"></script>';
        echo '<script src="' . URLROOT . '/js/businessmodels/logoPreview.js"></script>';
        break;
    //////////////////
    ///// TASKS
    case 'tasks addTask':
        echo '<script src="' . URLROOT . '/js/tasks/add.js"></script>';
        break;
    case 'tasks showTask':
        echo '<script src="' . URLROOT . '/js/tasks/show.js"></script>';
        break;
    case 'tasks editTask':
        echo '<script src="' . URLROOT . '/js/tasks/edit.js"></script>';
        break;
    /////////////////
    ///// METHODS
    case 'methods addMethod':
        echo '<script src="' . URLROOT . '/js/methods/add.js"></script>';
        break;
    case 'methods showMethod':
        echo '<script src="' . URLROOT . '/js/methods/show.js"></script>';
        break;
    case 'methods editMethod':
        echo '<script src="' . URLROOT . '/js/methods/edit.js"></script>';
        break;
    /////////////////
    ///// ORGANS
    case 'organs addOrgan':
        echo '<script src="' . URLROOT . '/js/organs/add.js"></script>';
        break;
    case 'organs editOrgan':
        echo '<script src="' . URLROOT . '/js/organs/edit.js"></script>';
        break;
    case 'organs showOrgan':
        echo '<script src="' . URLROOT . '/js/organs/show.js"></script>';
        break;
    case 'organs addSub':
        echo '<script src="' . URLROOT . '/js/organs/addsub.js"></script>';
        break;
    case 'organs organigram':
        echo '<script src="' . URLROOT . '/js/organs/jquery.jOrgChart.js"></script>';
        echo '<script src="' . URLROOT . '/js/organs/organigram.js"></script>';
        break;
    ////////////////////
    ///// PERMISSIONS
    case 'permissions permissions-assign':
        echo '<script src="' . URLROOT . '/js/permissions/assign.js"></script>';
        break;
    case 'permissions permissions-add':
        echo '<script src="' . URLROOT . '/js/permissions/add.js"></script>';
        break;
    /////////////////
    //// PROCESSES
    case 'processes processes-add':
        echo '<script src="' . URLROOT . '/js/processes/add.js"></script>';
        break;
    case 'processes processes-show':
        echo '<script src="' . URLROOT . '/js/processes/show.js"></script>';
        break;
    case 'processes processes-edit':
        echo '<script src="' . URLROOT . '/js/processes/edit.js"></script>';
        break;
    case 'processes processes-recycle':
        echo '<script src="' . URLROOT . '/js/processes/recycle.js"></script>';
        break;
    ////////////////
    //// LISTINGS
    case 'listings':
        echo '<script src="' . URLROOT . '/js/listings.js"></script>';
        break;
    ////////////////
    // FOLDERS
    case 'folders':
        echo '<script src="' . URLROOT . '/js/folders.js"></script>';
        break;


} ?>
</body>
</html>
