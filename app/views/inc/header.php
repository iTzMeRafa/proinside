<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
    integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
    integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="<?= URLROOT; ?>/css/bootstrap-multiselect.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/sidebar.css">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/pageHeaders.css">
    <link rel="stylesheet" href="<?= URLROOT; ?>/css/organigramm.css">
    <title><?php echo SITENAME; ?></title>
</head>
<body class="<?php echo $data['bodyClass']; ?>">

<?php if (
    $data['bodyClass'] == 'indexPage' ||
    $data['bodyClass'] == 'pageAbout' ||
    $data['bodyClass'] == 'users users-login'
) : ?>

    <?php require APPROOT . '/views/inc/navbar_unregistered.php'; ?>
    <div class="container-fluid">
        <div class="row">

<?php else : ?>
        <div class="container-fluid">
            <!--<div class="row">-->
            <div class="wrapper">
                <?php require APPROOT . '/views/inc/navbar.php'; ?>

                <?php require APPROOT . '/views/inc/navbarCollapseButton.php'; ?>

                <main id="content" role="main">


<?php endif; ?>
    
      
