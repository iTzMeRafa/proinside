
<!-- Sidebar  -->
<nav id="sidebar">

    <div id="fixedSidebar">


        <!-- Header -->
        <div class="sidebar-header">
            <h3>
                <a class="navbar-brand" href="<?= URLROOT; ?>/users/dashboard">
                    <img id="logo" src="<?= URLROOT; ?>/img/logo_temp.png" alt="Das ProInside App Logo">
                </a>
            </h3>
            <strong>
                <a href="<?= URLROOT; ?>/users/dashboard">
                    <img class="w-100" style="margin-left: -10px;" src="<?= URLROOT; ?>/img/logo_small.png" alt="Das ProInside App Logo">
                </a>
            </strong>
        </div>

        <ul class="list-unstyled components">

            <!-- If User is Logged In -->
            <?php if (isset($_SESSION['user_id'])) : ?>

                <!-- Global Searchbar -->
                <?php require APPROOT . '/views/inc/searchbar.php'; ?>

                <!-- Dashboard -->
                <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'dashboard'); ?>">
                    <a href="<?php echo URLROOT; ?>/users/dashboard">
                        <i class="fas fa-chart-line mr-2"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <!-- Unternehmensmodell -->
                <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'businessmodels'); ?>">
                    <a href="<?php echo URLROOT; ?>/BusinessModels">
                        <i class="fas fa-table mr-2"></i>
                        <span>Unternehmensmodell</span>
                    </a>
                </li>

                <!-- Organigramm -->
                <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'organs'); ?>">
                    <a href="<?php echo URLROOT; ?>/organs">
                        <i class="fas fa-sitemap mr-2"></i>
                        <span>Organigramm</span>
                    </a>
                </li>

                <!-- Dateimanager -->
                <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'folders'); ?>">
                    <a href="<?php echo URLROOT; ?>/folders">
                        <i class="fas fa-folder mr-2"></i>
                        <span>Dateimanager</span>
                    </a>
                </li>

                <!-- Reports -->
                <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'reports'); ?>">
                    <a href="<?php echo URLROOT; ?>/reports">
                        <i class="fas fa-eye mr-2"></i>
                        <span>Reports</span>
                    </a>
                </li>

                <!-- Aufgaben -->
                <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'tasks'); ?>">
                    <a href="<?php echo URLROOT; ?>/tasks">
                        <i class="fas fa-tasks mr-2"></i>
                        <span>Aufgaben</span>
                    </a>
                </li>

                <!-- Maßnahmen -->
                <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'methods'); ?>">
                    <a href="<?php echo URLROOT; ?>/methods">
                        <i class="fas fa-thumbtack mr-2"></i>
                        <span>Maßnahmen</span>
                    </a>
                </li>



        </ul>


            <?php
                if (
                    $data['privUser']->hasPrivilege('11') == true ||
                    $data['privUser']->hasPrivilege('14') == true
                ) :
            ?>
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 text-muted">
                    <span>Admin</span>
                    <a class="d-flex align-items-center text-muted" href="#">
                    </a>
                </h6>

                <ul class="list-unstyled components" style="padding-top: 0;">
                    <?php if ($data['privUser']->hasPrivilege('11') == true) : ?>
                        <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'users'); ?>">
                            <a class="nav-link" href="<?php echo URLROOT; ?>/users">
                                <i class="fas fa-users mr-2"></i>
                                <span>Mitarbeiter</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <?php if ($data['privUser']->hasPrivilege('14') == true) : ?>
                        <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'permissions'); ?>">
                            <a class="nav-link" href="<?php echo URLROOT; ?>/permissions">
                                <i class="fas fa-gavel mr-2"></i>
                                <span>Rechte</span>
                            </a>
                        </li>
                    <?php endif; ?>

                    <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'qualifications'); ?>">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/qualifications">
                            <i class="fas fa-ribbon mr-2"></i>
                            <span>Kompetenzen</span>
                        </a>
                    </li>

                    <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'iso'); ?>">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/iso">
                            <i class="fas fa-globe mr-2"></i>
                            <span>ISO Normen</span>
                        </a>
                    </li>

                    <li class="<?= getActiveClassByNeedle($data['bodyClass'], 'backups'); ?>">
                        <a class="nav-link" href="<?php echo URLROOT; ?>/Backups">
                            <i class="fas fa-database mr-2"></i>
                            <span>Backups</span>
                        </a>
                    </li>
                </ul>

            <?php endif; ?>

        <?php endif; ?>

        <ul class="list-unstyled components">

                <li class="nav-item">
                    <a
                        class="nav-link <?php echo (strpos($data['bodyClass'], 'accounts') !== false) ? 'active' : ''; ?>"
                        href="<?php echo URLROOT; ?>/accounts"
                    >
                        <i class="fas fa-user-circle mr-2"></i>
                        <span>Mein Account</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a
                        class="nav-link"
                        href="<?php echo URLROOT; ?>/users/logout"
                    >
                        <i class="fas fa-sign-out-alt mr-2"></i>
                        <span>Abmelden</span>
                    </a>
                </li>

                <!-- Simulates and handles click in public/js/fileManagerMagic.js -->
                <li class="nav-item">
                    <a
                        href="<?php echo URLROOT; ?>/folders?action=archiveClick"
                        id="archiveNavbar"
                        class="archive nav-link"
                    >
                        <i class="fas fa-archive mr-2"></i>
                        <span>Archiv</span>
                    </a>
                </li>
        </ul>

            <!-- Navigation mit dropdown
            <li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">
                    <i class="fas fa-home"></i>
                    Home
                </a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li>
                        <a href="#">Home 1</a>
                    </li>
                    <li>
                        <a href="#">Home 2</a>
                    </li>
                    <li>
                        <a href="#">Home 3</a>
                    </li>
                </ul>
            </li>
            -->

            <!-- Single Navitem
            <li>
                <a href="#">
                    <i class="fas fa-briefcase"></i>
                    About
                </a>
            </li>
            -->

    </div>


</nav>


