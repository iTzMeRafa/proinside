<div class="outerCollapseButton">
    <div class="innerCollapseButton">

        <button type="button" id="sidebarCollapse" class="btn">

            <!-- Show when sidebar is closed -->
            <i id="sideBarCollapseArrowRight" class="fas fa-chevron-right d-none"></i>

            <!-- Show when sidebar is open -->
            <i id="sideBarCollapseArrowLeft" class="fas fa-chevron-left"></i>
        </button>

    </div>
</div>
