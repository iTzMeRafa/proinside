
<nav class="col-md-2 d-none d-md-block sidebar position-fixed p-0 h-100">
    <div class="d-flex flex-column h-100">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="navbar-brand" href="<?= URLROOT; ?>/users/dashboard">
                    <img id="logo" src="<?= URLROOT; ?>/img/logo_temp.png" alt="Das ProInside App Logo">
                </a>

                <span id="minimize" class=""><i class="fas fa-long-arrow-alt-left"></i></span>

            </li>

            <?php /*** If user is logged in ***/
            if (isset($_SESSION['user_id'])) : ?>

            <!-- Global Searchbar -->
            <?php require APPROOT . '/views/inc/searchbar.php'; ?>

            <li class="nav-item">
                <a class="nav-link <?php echo (strpos($data['bodyClass'], 'dashboard') !== false) ? 'active' : ''; ?>"
                   href="<?php echo URLROOT; ?>/users/dashboard">
                    <i class="fas fa-chart-line mr-2"></i>Dashboard
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link <?php echo (strpos($data['bodyClass'], 'businessmodels') !== false) ? 'active' : ''; ?>"
                   href="<?php echo URLROOT; ?>/BusinessModels"><i class="fas fa-table mr-2"></i>Unternehmensmodell</a>
            </li>

            <?php if ($data['privUser']->hasPrivilege('10') == true) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'organs') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/organs"><i class="fas fa-sitemap mr-2"></i>Organigramm</a>
                </li>
            <?php endif; ?>

            <?php if ($data['privUser']->hasPrivilege('13') == true) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'folders') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/folders"><i class="fas fa-folder mr-2"></i>Dateimananger</a>
                </li>
            <?php endif; ?>

            <?php if ($data['privUser']->hasPrivilege('12') == true) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'reports') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/reports"><i class="fas fa-eye mr-2"></i>Reports</a>
                </li>
            <?php endif; ?>

            <?php if ($data['privUser']->hasPrivilege('9') == true) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'tasks') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/tasks"><i class="fas fa-tasks mr-2"></i>Aufgaben</a>
                </li>
            <?php endif; ?>

            <?php if ($data['privUser']->hasPrivilege('9') == true) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'methods') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/methods"><i class="fas fa-thumbtack"></i>Maßnahmen</a>
                </li>
            <?php endif; ?>
        </ul>

        <?php
            if (
                $data['privUser']->hasPrivilege('11') == true ||
                $data['privUser']->hasPrivilege('14') == true
            ) :
        ?>
            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>Adminbereich</span>
                <a class="d-flex align-items-center text-muted" href="#">
                    <span data-feather="plus-circle"></span>
                </a>
            </h6>
        <?php endif; ?>

        <ul class="nav flex-column mb-2">

            <?php if ($data['privUser']->hasPrivilege('11') == true) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'users') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/users"><i class="fas fa-users mr-2"></i>Mitarbeiter</a>
                </li>
            <?php endif; ?>

            <?php if ($data['privUser']->hasPrivilege('14') == true) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'permissions') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/permissions"><i class="fas fa-gavel mr-2"></i>Rechte</a>
                </li>
            <?php endif; ?>


            <?php endif; ?>
        </ul>

        <ul class="nav flex-column mt-auto mb-2">
            <?php /*** If user is logged in ***/
            if (isset($_SESSION['user_id'])) : ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo (strpos($data['bodyClass'], 'accounts') !== false) ? 'active' : ''; ?>"
                       href="<?php echo URLROOT; ?>/accounts"><i class="fas fa-user-circle mr-2"></i>Mein Account</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout"><i
                                class="fas fa-sign-out-alt mr-2"></i>Abmelden</a>
                </li>
                <!-- Simulates and handles click in public/js/fileManagerMagic.js -->
                <li class="nav-item">
                    <a href="<?php echo URLROOT; ?>/folders?action=archiveClick" id="archiveNavbar"
                       class="archive nav-link"><i class="fas fa-archive mr-2"></i>Archiv</a>
                </li>
            <?php endif; ?>
        </ul>


    </div>
</nav>
