<nav class="navbar navbar-expand-lg bg-white mb-3">
  <div class="container-fluid">
     <a class="navbar-brand" href="<?php echo URLROOT; ?>">
       <img id="logo" src="<?php echo URLROOT; ?>/img/logo_temp.png" alt="Das ProInside App Logo">
     </a>
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
       <span class="navbar-toggler-icon"></span>
     </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
       <ul class="navbar-nav mr-auto">
         <li class="nav-item">
           <a class="nav-link" href="<?php echo URLROOT; ?>">Home</a>
         </li>
         <?php  /*** If user is not logged in ***/
          if (!isset($_SESSION['user_id'])) : ?>
           <li class="nav-item">
             <a class="nav-link" href="<?php echo URLROOT; ?>/pages/about">About</a>
           </li>
         <?php endif;?>


         
       </ul>

       <ul class="navbar-nav ml-auto">
         <?php  /*** If user is logged in ***/
          if (isset($_SESSION['user_id'])) : ?>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/accounts">Mein Account</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo URLROOT; ?>/users/logout">Abmelden</a>
          </li>

         <?php  /*** if user is not logged in display the following ***/
          else : ?>
           <li class="nav-item">
             <a class="nav-link" href="<?php echo URLROOT; ?>/users/login">Login</a>
           </li>
         <?php endif; ?>
       </ul>
      </div>
    </div>
   </nav>