<form role="search" class="mb-4" id="globalSearchForm">
    <li style="display: inline-flex" class="ml-3">
        <i class="fas fa-search mr-2" style="color: #0B676E"></i>
        <input type="text" class="globalSearchbar" id="globalSearchbarInput"/>
    </li>
</form>