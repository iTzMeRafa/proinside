
<?php require APPROOT . '/views/inc/header.php'; ?>

<?php
    $pageTitle = 'Erstellen'
?>

<?php require APPROOT . '/views/isos/templates/title.php'; ?>

<?php require APPROOT . '/views/isos/templates/navigation.php'; ?>

<?php require APPROOT . '/views/isos/templates/createForm.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
