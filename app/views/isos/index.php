
<?php require APPROOT . '/views/inc/header.php'; ?>

<?php
$pageTitle = 'Ansehen'
?>

    <?php require APPROOT . '/views/isos/templates/title.php'; ?>

    <?php require APPROOT . '/views/isos/templates/navigation.php'; ?>

    <?php displayFlash('deleteIsoState'); ?>
    <?php displayFlash('renameIsoState'); ?>
    <?php displayFlash('assignIsoState'); ?>


    <?php require APPROOT . '/views/isos/templates/isoList.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
