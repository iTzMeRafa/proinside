<div class="row">
    <div class="col-md-6 offset-md-3">

        <?php displayFlash('createIsoState'); ?>

        <form method="post" action="<?= URLROOT; ?>/IsoCreate/post">
            <div class="form-group">
                <label for="isoNorm">Iso Norm:</label>
                <input
                        type="text"
                        name="isoNorm"
                        class="form-control"
                        id="isoNorm"
                        placeholder="Norm"
                />
            </div>
            <button type="submit" name="isoCreate" class="btn btn-primary float-right">Erstellen</button>
        </form>

    </div>
</div>

