<!-- Sortable Table -->
<div class="row">
    <div class="col-md-12">
        <table id="isoList" class="table table-striped table-bordered" style="width: 100%">
            <thead>
            <tr style="background-color: #0B676E; color: white; border: none;">
                <th style="border: none;">ID</th>
                <th style="border: none;">Norm</th>
                <th style="border: none;">Aktionen</th>
                <th style="border: none;">Hinzugefügt Am</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($data['isos'] as $iso) : ?>
                <tr>
                    <td>
                        <a href="<?= URLROOT; ?>/Iso/show/<?= $iso->id; ?>">
                            <?= $iso->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?= URLROOT; ?>/Iso/show/<?= $iso->id; ?>">
                            <?= $iso->norm; ?>
                        </a>
                    </td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">

                            <!-- Delete Button -->
                            <button
                                    type="button"
                                    data-toggle="modal"
                                    data-target="#isoDeleteModal<?= $iso->id; ?>"
                                    class="btn btn-danger"
                            >
                                <i class="fa fa-trash mr-2"></i> Löschen
                            </button>

                            <!-- Rename Iso Button -->
                            <button
                                    type="button"
                                    data-toggle="modal"
                                    data-target="#isoRenameModal<?= $iso->id; ?>"
                                    class="btn btn-pe-darkgreen"
                            >
                                <i class="fas fa-pen-square mr-2"></i> Umbenennen
                            </button>

                        </div>
                    </td>
                    <td><?= $iso->created_at; ?></td>

                </tr>

                <!-- Modals for each entry with correct values -->
                <?php require APPROOT . '/views/isos/templates/isoModals.php'; ?>

            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
<!-- /Sortable Table -->