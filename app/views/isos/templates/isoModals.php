<!-- Iso Delete Modal -->
<div id="isoDeleteModal<?= $iso->id; ?>" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ISO Löschen: <strong><?= $iso->name; ?></strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Sind Sie sicher, dass Sie die ISO-Norm "<?= $iso->norm; ?>" löschen wollen? <br />
                    Dieser Vorgang kann nicht rückgängig gemacht werden, auch verlieren alle Aufgaben und Maßnahmen diese ISO-Norm.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <a href="<?= URLROOT; ?>/IsoDelete/index/<?= $iso->id; ?>" type="button" class="saveChanges btn btn-danger">Löschen</a>
            </div>
        </div>
    </div>
</div>

<!-- Iso Rename Modal -->
<div id="isoRenameModal<?= $iso->id; ?>" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <form id="isosRenameForm<?= $iso->id; ?>" method="POST" action="<?= URLROOT; ?>/IsoRename">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">ISO Umbenennen: <strong><?= $iso->name; ?></strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="renamedIso" value="<?= $iso->name; ?>" placeholder="<?= $iso->name; ?>" />
                    <input type="hidden" name="isoId" value="<?= $iso->id; ?>" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="submit" name="renameIsoSubmit" class="saveChanges btn btn-primary">Umbenennen</button>
                </div>
            </div>
        </form>
    </div>
</div>


