<?php
$userList = $data['userList'];
$qualification = $data['qualification'];
?>
<h1 class="mb-5">Kompetenz <span class="badge badge-secondary"><?= $qualification->name; ?></span></h1>

<!-- Sortable Table -->
<div class="col-md-12">
    <table id="qualificationUserList" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">ID</th>
            <th style="border: none;">Name</th>
            <th style="border: none;">E-Mail</th>
            <th style="border: none;">Kompetenz</th>
            <th style="border: none;">Status</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($userList as $user) : ?>
            <tr>
                <td><?= $user->id; ?></td>
                <td><?= $user->firstname.' '.$user->lastname; ?></td>
                <td><?= $user->email; ?></td>
                <td>Erhalten vor: <br /> <?= differenceBetweenNowAndDateFormatted($user->assigned_at); ?></td>
                <td>
                    <?php
                       $passedDaysHavingQualification = differenceBetweenNowAndDate($user->assigned_at)->days;
                       $allowedDaysHavingQualification = $qualification->expire_in_days;
                    ?>

                    <?php if ($passedDaysHavingQualification > $allowedDaysHavingQualification): ?>
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fas fa-frown fa-3x color-darkgreen"></i>
                        </div>
                        <div class="col-md-10">
                            Muss erneut geschult werden <br />
                            Abgelaufen seit: <?= $passedDaysHavingQualification - $allowedDaysHavingQualification; ?> Tagen
                        </div>
                    </div>
                    <?php else: ?>
                    <div class="row">
                        <div class="col-md-2">
                            <i class="fas fa-smile-beam fa-3x color-lightgreen"></i>
                        </div>
                        <div class="col-md-10">
                            Alles gut! <br />
                            Läuft ab in: <?= $allowedDaysHavingQualification - $passedDaysHavingQualification; ?> Tagen
                        </div>
                    </div>
                    <?php endif; ?>
                </td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->
