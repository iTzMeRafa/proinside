<div class="row mb-5">
    <div class="col-12">

        <a href="<?= URLROOT; ?>/Iso" class="h1 btn btn-pe-lightgreen">
            <i class="fas fa-eye mr-2"></i> Ansehen
        </a>

        <a href="<?= URLROOT; ?>/IsoCreate" class="h1 btn btn-pe-lightgreen">
            <i class="fas fa-plus-square mr-2"></i> Erstellen
        </a>

    </div>
</div>