<?php require APPROOT . '/views/inc/header.php'; ?>



<?php displayFlash('listing_message'); ?>
<?php displayFlash('fileAccepted'); ?>

<div class="container-fluid m-0 p-0">

    <div class="row">
        <nav class="col-md-3 bg-light sidebar">

            <a
                    href="<?= URLROOT; ?>/folders/jump/<?= $data['currentFolder']; ?>"
                    class="btn btn-light mb-3">
                <i class="fa fa-chevron-left"></i> Zurück
            </a>

            <h2 class="h4 mt-3">Primäre Ordner</h2>
            <ul class="nav flex-column">
                <?php foreach ($data['mainDirectories'] as $directory) : ?>
                    <li class="nav-item">

                        <div class="d-flex">
                            <a class="nav-link main-link w-100 <?php echo ($directory->id == $data['currentFolder']) ? 'active' : ''; ?>"
                               href="<?php echo URLROOT; ?>/listings/index/<?php echo $directory->id; ?>">
                                <?php echo $directory->folder_name; ?>
                            </a>
                            <?php if (isset($directory->sub_directories)) : ?>
                                <div class="expand-sub d-flex justify-content-center">
                                    <i class="fas fa-plus align-self-center"></i>
                                </div>
                            <?php endif; ?>
                        </div>

                        <?php if (isset($directory->sub_directories)) : ?>
                            <div class="sub-folders d-none">
                                <ul class="nav flex-column">
                                    <?php foreach ($directory->sub_directories as $sub) : ?>
                                        <li class="nav-item">
                                            <a class="nav-link sub-link <?php echo ($sub->id == $data['currentFolder']) ? 'active' : ''; ?>"
                                               href="<?php echo URLROOT; ?>/listings/index/<?php echo $sub->id; ?>"><?php echo $sub->folder_name; ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>

                    </li>
                <?php endforeach; ?>
            </ul>
        </nav>


        <div id="mainArea" class="col-md-7 px-0 h-100">
            <div class="border-0 bg-dark d-none mainAreaFileNotFoundHint" id="mainAreaFileNotFoundHint">
                <p class="h1 mainAreaFileNotFoundHint__content">Die Datei konnte nicht gefunden werden. <br/>Bitte laden
                    Sie diese erneut hoch!</p>
            </div>
            <iframe src="" width="100%" height="100%" class="border-0 bg-dark"></iframe>
        </div>

        <div id="sidebar" class="col-md-2 bg-light">
            <h2 class="h4 mt-3">Dateien</h2>
            <ul class="list-group">
                <?php if (!empty($data['subDirectories'])) : ?>
                    <?php foreach ($data['subDirectories'] as $directory) :
                        $dataPath = URLROOT . $directory['path']; // e.g proinside.test/public/uploads/files/Test/test.pdf
                        ?>

                        <!-- Single Button if just a upload with no notification -->
                        <?php if (is_null($directory['isAccepted'])): ?>
                            <button
                                    type="button"
                                    class="showDataListGroup list-group-item list-group-item-action mb-1"
                                    data-contentType="<?= get_headers($dataPath)[3]; ?>>"
                                    data-path="<?= $dataPath; ?> "
                                    style="overflow: auto; text-overflow: ellipsis;"
                            >
                                <?php echo $directory['name']; ?>
                            </button>
                        <?php else: ?>
                            <!-- Button Group only for file notification enabled -->
                            <form method="POST" action="<?= URLROOT; ?>/Listings/accept/<?= $data['currentFolder']; ?>/<?= $directory['id']; ?>/<?= $directory['approved_by']; ?>">
                                <div class="btn-group mb-1" role="group" aria-label="Basic example">
                                    <button
                                            type="button"
                                            class="showDataListGroup list-group-item list-group-item-action"
                                            data-contentType="<?= get_headers($dataPath)[3]; ?>>"
                                            data-path="<?= $dataPath; ?> "
                                            style="overflow: auto; text-overflow: ellipsis;"
                                    >
                                        <?php echo $directory['name']; ?>
                                    </button>

                                    <!-- Show Accept buttons color depending on acceptance -->
                                    <?php if ($directory['isAccepted'] == '1'): ?>
                                        <button type="button" class="btn btn-info"><i class="fas fa-check"></i></button>
                                    <?php elseif ($directory['isAccepted'] == '0'): ?>
                                            <button type="submit" class="btn btn-success">Akzeptieren</button>
                                    <?php endif; ?>
                                </div>
                            </form>
                        <?php endif; ?>

                    <?php endforeach; ?>
                <?php else : ?>
                    <button type="button" class="list-group-item list-group-item-action" data-path="empty"
                            style="overflow: auto; text-overflow: ellipsis;">
                        In diesem Ordner befinden sich keine Dateien.
                    </button>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <?php require APPROOT . '/views/inc/footer.php'; ?>
