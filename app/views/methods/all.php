<?php require APPROOT . '/views/inc/header.php'; ?>
  <?php displayFlash('method_message'); ?>
  <div class="row mb-3">
    <div class="col-md-6">
      <h1 class="btn btn-pe-lightgreen"><i class="fas fa-methods mr-2"></i>Maßnahmen</h1>
    </div>
    <div class="col-md-6">
      <a href="<?php echo URLROOT; ?>/methods/add" class="btn btn-pe-lightgreen float-right">
        <i class="fas fa-pencil-alt mr-2"></i> Neue Maßnahme erstellen
      </a>
    </div>
    <div class="col-12 mt-2 mb-3">
      <ul class="nav nav-pills">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URLROOT; ?>/methods">Aktiv</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo URLROOT; ?>/methods/archived">Archiviert</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" href="<?php echo URLROOT; ?>/methods/all">Alle</a>
        </li>
      </ul>
    </div>
  </div>

<div class="row">
    <div class="col-md-12">

        <!-- Sortable Table -->
        <table id="methodsActiveTable" class="table table-striped table-bordered" style="width: 100%">
            <thead>
            <tr style="background-color: #0B676E; color: white; border: none;">
                <th style="border: none;">ID</th>
                <th style="border: none;">Bezeichnung</th>
                <th style="border: none;">Erstellt am</th>
                <th style="border: none;">Zu Prozess</th>
                <th style="border: none;">Priorität</th>
                <th style="border: none;">Status</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($data['methods'] as $method) : ?>
                <tr>
                    <td><a href="<?php echo URLROOT; ?>/methods/show/<?php echo $method->methodId; ?>">M<?= $method->methodId; ?></a></td>
                    <td><a href="<?php echo URLROOT; ?>/methods/show/<?php echo $method->methodId; ?>"><?php echo $method->methodName; ?></a></td>
                    <td><?= $method->created_at; ?></td>
                    <td><a href="<?= URLROOT; ?>/processes/show/<?= $method->process_id; ?>"><?= $method->process_name; ?></a></td>
                    <td>
                        <?php
                        switch ($method->priority) {
                            case 0:
                                echo 'Keine Priorität festgelegt';
                                break;
                            case 1:
                                echo '1 - Wichtig / Dringend';
                                break;
                            case 2:
                                echo '2 - Nicht Wichtig / Dringend';
                                break;
                            case 3:
                                echo '3 - Wichtig / Nicht Dringend';
                                break;
                            case 4:
                                echo '4 - Nicht Wichtig / Nicht Dringend';
                                break;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        switch ($method->status) {
                            case 'nichtBegonnen':
                                echo 'Nicht Begonnen';
                                break;
                            case 'inBearbeitung':
                                echo 'In Bearbeitung';
                                break;
                            case 'umgesetzt':
                                echo 'Umgesetzt';
                                break;
                            case 'wirksamkeitGeprüft':
                                echo 'Wirksamkeit geprüft';
                                break;
                        }
                        ?>
                    </td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
        <!-- /Sortable Table -->

    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
