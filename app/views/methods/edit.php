<?php require APPROOT . '/views/inc/header.php'; ?>
<a href="<?php echo URLROOT . '/methods/show/' . $data['id'] ?>'" class="btn btn-light mb-3"><i
            class="fa fa-chevron-left"></i> Zurück</a>

<form action="<?php echo URLROOT; ?>/methods/edit/<?php echo $data['id']; ?>" method="post">
    <div class="row mb-5">
        <div class="col-md-12">
            <div class="card">
                <h5 class="card-header"><i class="fas fa-edit"></i> Maßnahme M<?php echo $data['id']; ?> bearbeiten
                    <input style="margin-top: 0px !important;" type="submit"
                           class="btn btn-pe-darkgreen mt-3 float-right" value="Aktualisieren"></h5>
                <div class="card-body">
                    <!--<h5 class="card-title">Füllen Sie die nachfolgenden Felder aus, um die Maßnahme zu bearbeiten</h5>-->
                    <p class="card-text">Füllen Sie die nachfolgenden Felder aus, um die Maßnahme zu bearbeiten.</p>


                    <!-- Input Felder -->
                    <div class="row mb-4">

                        <!-- Left Side -->
                        <div class="form-group col-md-4">
                            <p class="h6">Name: <sup>*</sup></p>
                            <input name="name" type="text"
                                   class="form-control <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>"
                                   value="<?php echo $data['name']; ?>" placeholder="Name">
                            <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>

                            <p class="h6 mt-3">Auslösendes Ereignis:</p>
                            <select id="occurrence" name="occurrence"
                                    class="form-control <?php echo (!empty($data['occurrence_err'])) ? 'is-invalid' : ''; ?>">
                                <option value="">Ereignis auswählen</option>
                                <option value="1" <?php echo $data['occurrence'] == 1 ? 'selected' : ''; ?>>
                                    Arbeitsschutz
                                </option>
                                <option value="2" <?php echo $data['occurrence'] == 2 ? 'selected' : ''; ?>>Interne
                                    Audits
                                </option>
                                <option value="3" <?php echo $data['occurrence'] == 3 ? 'selected' : ''; ?>>Externe
                                    Audits
                                </option>
                                <option value="5" <?php echo $data['occurrence'] == 5 ? 'selected' : ''; ?>>Abgelaufene Kompetenz
                                </option>
                                <option value="4" <?php echo $data['occurrence'] == 4 ? 'selected' : ''; ?>>Sonstiges
                                </option>
                            </select>
                            <span class="invalid-feedback"><?php echo $data['occurrence_err']; ?></span>

                            <!-- Prozess -->
                            <h2 class="h6 mt-3">Zu Prozess hinzufügen*: <sup>*</sup></h2>
                            <select id="process_id" name="process_id"
                                    class="form-control <?php echo (!empty($data['process_id_err'])) ? 'is-invalid' : ''; ?>">
                                <option value="">Prozess auswählen</option>
                                <?php foreach ($data['processes'] as $process): ?>
                                    <option value="<?= $process->id; ?>" <?php echo $data['process_id'] == $process->id ? 'selected' : ''; ?>><?= $process->process_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <span class="invalid-feedback"><?php echo $data['process_id_err']; ?></span>

                            <!-- Zielvorgabe -->
                            <h2 class="h6 mt-3">Zielvorgabe: <sup>*</sup></h2>
                            <input name="goal" type="text"
                                   class="form-control <?php echo (!empty($data['goal_err'])) ? 'is-invalid' : ''; ?>"
                                   value="<?php echo $data['goal']; ?>" placeholder="Zielvorgabe">
                            <span class="invalid-feedback"><?php echo $data['goal_err']; ?></span>
                            <!-- /Zielvorgabe -->

                            <!-- Status -->
                            <h2 class="h6 mt-3">Status: <sup>*</sup></h2>
                            <select name="status"
                                    class="form-control <?php echo (!empty($data['status_err'])) ? 'is-invalid' : ''; ?>">
                                <option value="">Status auswählen</option>
                                <option value="nichtBegonnen" <?php echo $data['status'] == 'nichtBegonnen' ? 'selected' : ''; ?>>
                                    Nicht Begonnen
                                </option>
                                <option value="inBearbeitung" <?php echo $data['status'] == 'inBearbeitung' ? 'selected' : ''; ?>>
                                    In Bearbeitung
                                </option>
                                <option value="umgesetzt" <?php echo $data['status'] == 'umgesetzt' ? 'selected' : ''; ?>>
                                    Umgesetzt
                                </option>
                                <option value="wirksamkeitGeprüft" <?php echo $data['status'] == 'wirksamkeitGeprüft' ? 'selected' : ''; ?>>
                                    Wirksamkeit Geprüft
                                </option>
                            </select>
                            <span class="invalid-feedback"><?php echo $data['status_err']; ?></span>
                            <!-- /Status -->

                            <!-- Priorität -->
                            <h2 class="h6 mt-3">Priorität: <sup>*</sup></h2>
                            <select name="priority"
                                    class="form-control <?php echo (!empty($data['priority_err'])) ? 'is-invalid' : ''; ?>">
                                <option value="empty" selected>Priorität auswählen</option>
                                <option value="1" <?php echo $data['priority'] == 1 ? 'selected' : ''; ?>>1) Wichtig /
                                    Dringend
                                </option>
                                <option value="2" <?php echo $data['priority'] == 2 ? 'selected' : ''; ?>>2) Nicht
                                    Wichtig / Dringend
                                </option>
                                <option value="3" <?php echo $data['priority'] == 3 ? 'selected' : ''; ?>>3) Wichtig /
                                    Nicht Dringend
                                </option>
                                <option value="4" <?php echo $data['priority'] == 4 ? 'selected' : ''; ?>>4) Nicht
                                    Wichtig / Nicht Dringend
                                </option>
                            </select>
                            <span class="invalid-feedback"><?php echo $data['priority_err']; ?></span>
                            <!-- /Priorität -->

                        </div>
                        <!-- /Left Side -->

                        <!-- Right Side -->
                        <div class="form-group col-md-8">

                            <!-- Beschreibung -->
                            <h2 class="h6">Beschreibung: <sup>*</sup></h2>
                            <textarea name="description" class="form-control" rows="4"
                                      cols="80"><?php echo $data['description']; ?></textarea>
                            <!-- Beschreibung -->

                            <!-- Ursache -->
                            <h2 class="h6 mt-3">Ursache für die Feststellung: <sup>*</sup></h2>
                            <!--<input name="reason" type="text" class="form-control <?php echo (!empty($data['reason_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['reason']; ?>" placeholder="Ursache für die Feststellung">-->
                            <textarea name="reason" id="reason" class="form-control mb-3" cols="80"
                                      rows="4"><?php echo $data['reason']; ?></textarea>
                            <span class="invalid-feedback"><?php echo $data['reason_err']; ?></span>
                            <!-- /Ursache -->

                            <!-- Aktion -->
                            <h2 class="h6 mt-3">Aktion/Maßnahme: <sup>*</sup></h2>
                            <!--<input name="solution" type="text" class="form-control <?php echo (!empty($data['solution_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['solution']; ?>" placeholder="Aktion/Maßnahme">-->
                            <textarea name="solution" id="solution" class="form-control mb-3" cols="80"
                                      rows="4"><?php echo $data['solution']; ?></textarea>
                            <span class="invalid-feedback"><?php echo $data['solution_err']; ?></span>
                            <!-- /Aktion -->

                        </div>
                        <!-- /Right Side -->
                    </div>
                    <!-- /Input Felder -->

                    <!-- Funktionszuteilungen -->
                    <div class="row">
                        <div class="col-md-12 ">
                            <p class="h4 px-3 py-2 bg-pe-darkgreen text-white">Funktionszuteilung: <sup>*</sup></p>
                            <div class="row">

                                <div class="col-md-3">
                                    <button id="open_roles_responsible" type="button"
                                            class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                            data-target="#roles_responsible_modal">Verantwortlichkeiten definieren
                                    </button>
                                    <input id="roles_responsible" name="roles_responsible" type="hidden"
                                           class="form-control" value="<?php echo $data['roles_responsible_id']; ?>">
                                    <ul id="responsibleUI" class="list-group">
                                        <?php if ($data['roles_responsible']) : ?>

                                            <?php foreach ($data['roles_responsible'] as $role) : ?>

                                                <?php if (!empty($role['suborgan'])) : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['suborgan']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php else : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['organ']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php endif; ?>

                                            <?php endforeach; ?>


                                        <?php else : ?>
                                            <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i>
                                                Keine Zuteilung
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>

                                <div class="col-md-3">
                                    <button id="open_roles_concerned" type="button"
                                            class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                            data-target="#roles_concerned_modal">Zuständigkeiten definieren
                                    </button>
                                    <input id="roles_concerned" name="roles_concerned" type="hidden"
                                           class="form-control" value="<?php echo $data['roles_concerned_id']; ?>">
                                    <ul id="concernedUI" class="list-group">
                                        <?php if ($data['roles_concerned']) : ?>

                                            <?php foreach ($data['roles_concerned'] as $role) : ?>

                                                <?php if (!empty($role['suborgan'])) : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['suborgan']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php else : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['organ']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php endif; ?>

                                            <?php endforeach; ?>

                                        <?php else : ?>
                                            <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i>
                                                Keine Zuteilung
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>

                                <div class="col-md-3">
                                    <button id="open_roles_contributing" type="button"
                                            class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                            data-target="#roles_contributing_modal">Mitwirkungen definieren
                                    </button>
                                    <input id="roles_contributing" name="roles_contributing" type="hidden"
                                           class="form-control" value="<?php echo $data['roles_contributing_id']; ?>">
                                    <ul id="contributingUI" class="list-group">
                                        <?php if ($data['roles_contributing']) : ?>

                                            <?php foreach ($data['roles_contributing'] as $role) : ?>

                                                <?php if (!empty($role['suborgan'])) : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['suborgan']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php else : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['organ']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php endif; ?>

                                            <?php endforeach; ?>

                                        <?php else : ?>
                                            <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i>
                                                Keine Zuteilung
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>

                                <div class="col-md-3">
                                    <button id="open_roles_information" type="button"
                                            class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                            data-target="#roles_information_modal">Information definieren
                                    </button>
                                    <input id="roles_information" name="roles_information" type="hidden"
                                           class="form-control" value="<?php echo $data['roles_information_id']; ?>">
                                    <ul id="informationUI" class="list-group">
                                        <?php if ($data['roles_information']) : ?>

                                            <?php foreach ($data['roles_information'] as $role) : ?>

                                                <?php if (!empty($role['suborgan'])) : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['suborgan']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php else : ?>

                                                    <li class="list-group-item d-flex"
                                                        data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                                        <p class="mb-0">
                                                            <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                            (<?php echo $role['organ']->name; ?>)
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto align-self-center"></i>
                                                    </li>
                                                    </li>

                                                <?php endif; ?>

                                            <?php endforeach; ?>

                                        <?php else : ?>
                                            <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i>
                                                Keine Zuteilung
                                            </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="form-group col-md-4 mt-3">
                            <h2 class="h6">ISO-Relevanz: <sup>*</sup></h2>
                            <select name="iso_relevant"
                                    class="form-control <?php echo (!empty($data['iso_relevant_err'])) ? 'is-invalid' : ''; ?>">
                                <option value="empty" selected>Relevanz auswählen</option>
                                <option value="1" <?php echo $data['iso_relevant'] == 1 ? 'selected' : ''; ?>>Ja
                                </option>
                                <option value="0" <?php echo $data['iso_relevant'] == 0 ? 'selected' : ''; ?>>Nein
                                </option>
                            </select>
                            <span class="invalid-feedback"><?php echo $data['iso_relevant_err']; ?></span>
                        </div>

                        <div class="form-group col-md-12 my-3">
                            <div class="row">

                                <div class="col-md-6">
                                    <p class="h4">Erforderliches Wissen:</p>
                                    <textarea name="required_knowledge_text" id="required_knowledge_text"
                                              class="form-control mb-3" cols="30"
                                              rows="3"><?php echo $data['required_knowledge_text']; ?></textarea>
                                </div>

                                <div id="stored_knowledge" class="col-md-6">

                                    <input id="required_knowledge" name="required_knowledge" type="hidden"
                                           class="form-control" value="<?php echo $data['required_knowledge']; ?>">
                                    <input id="required_knowledge_folders" name="required_knowledge_folders"
                                           type="hidden" class="form-control"
                                           value="<?php echo $data['required_knowledge_folders']; ?>">
                                    <span class="invalid-feedback"><?php echo $data['required_knowledge_err']; ?></span>
                                    </button>

                                    <p class="h5">Zugeteiltes Wissen
                                        <button id="open_knowledge_browser" type="button"
                                                class="btn btn-pe-darkgreen ml-2" data-toggle="modal"
                                                data-target="#knowledge_browser">
                                            Dateien anhängen
                                        </button>
                                    </p>
                                    <ul class="list-group">
                                        <?php if (!empty($data['required_knowledge_UI'])) : ?>

                                            <?php foreach ($data['required_knowledge_UI'] as $item) : ?>
                                                <li class="list-group-item list-group-item-secondary d-flex align-items-center file"
                                                    data-path="<?php echo $item['path']; ?>">
                                                    <i class="fas fa-file mr-2"></i>
                                                    <?php echo $item['name']; ?>
                                                    <i class="close-btn fas fa-times ml-auto"></i>
                                                </li>
                                            <?php endforeach; ?>

                                        <?php endif; ?>

                                        <?php if (!empty($data['required_knowledge_folders_UI'])) : ?>

                                            <?php foreach ($data['required_knowledge_folders_UI'] as $item) : ?>
                                                <li class="list-group-item list-group-item-secondary d-flex align-items-center folder"
                                                    data-path="<?php echo $item['folderPath']; ?>">
                                                    <i class="fas fa-folder mr-2"></i>
                                                    <?php echo $item['folderName']; ?>
                                                    <i class="close-btn fas fa-times ml-auto"></i>
                                                </li>
                                            <?php endforeach; ?>

                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12 mt-4">
                            <div class="row">

                                <div class="col-md-6">
                                    <p class="h4">Erforderliche Ressourcen:</p>
                                    <textarea name="required_resources_text" id="required_resources_text"
                                              class="form-control mb-3" cols="30"
                                              rows="3"><?php echo $data['required_resources_text']; ?></textarea>
                                </div>

                                <div id="stored_resources" class="col-md-6">

                                    <input id="required_resources" name="required_resources" type="hidden"
                                           class="form-control" value="<?php echo $data['required_resources']; ?>">
                                    <input id="required_resources_folders" name="required_resources_folders"
                                           type="hidden" class="form-control"
                                           value="<?php echo $data['required_resources_folders']; ?>">
                                    <span class="invalid-feedback"><?php echo $data['required_resources_err']; ?></span>


                                    <p class="h5">Zugeteilte Ressourcen
                                        <button id="open_resource_browser" type="button"
                                                class="btn btn-pe-darkgreen ml-2" data-toggle="modal"
                                                data-target="#resource_browser">
                                            Dateien anhängen
                                        </button>
                                    </p>
                                    <ul class="list-group">
                                        <?php if (!empty($data['required_resources_UI'])) : ?>

                                            <?php foreach ($data['required_resources_UI'] as $item) : ?>
                                                <li class="list-group-item list-group-item-secondary d-flex align-items-center file"
                                                    data-path="<?php echo $item['path']; ?>">
                                                    <i class="fas fa-file mr-2"></i>
                                                    <?php echo $item['name']; ?>
                                                    <i class="close-btn fas fa-times ml-auto"></i>
                                                </li>
                                            <?php endforeach; ?>

                                        <?php endif; ?>

                                        <?php if (!empty($data['required_resources_folders_UI'])) : ?>

                                            <?php foreach ($data['required_resources_folders_UI'] as $item) : ?>
                                                <li class="list-group-item list-group-item-secondary d-flex align-items-center folder"
                                                    data-path="<?php echo $item['folderPath']; ?>">
                                                    <i class="fas fa-folder mr-2"></i>
                                                    <?php echo $item['folderName']; ?>
                                                    <i class="close-btn fas fa-times ml-auto"></i>
                                                </li>
                                            <?php endforeach; ?>

                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="form-group col-md-12 mt-3">
                            <div class="row">
                                <div class="col-12">
                                    <p class="h4 px-3 py-2 bg-pe-darkgreen text-white">Wiederkehrende Check-Intervalle:
                                        <sup>*</sup></p>
                                </div>

                            </div>
                        </div>


                        <div class="form-group col-md-12">
                            <div class="row">

                                <div class="col-md-4">
                                    <div class="form-check pl-0">
                                        <input id="activate_intervalsMethod" name="activate_intervalsMethod"
                                               type="checkbox" value="none" class="mr-1" checked
                                               style="visibility: hidden;">
                                    </div>

                                    <fieldset id="set_intervals" class="mt-3">
                                        <label for="reference_date">Start Datum: *</label>
                                        <div class="form-row">
                                            <div class="col-4">
                                                <label for="reference_day">Tag</label>
                                                <input name="reference_day" type="number"
                                                       class="form-control <?php echo (!empty($data['reference_date_err'])) ? 'is-invalid' : ''; ?>"
                                                       min="1" max="31" value="<?php echo $data['reference_day']; ?>">
                                            </div>
                                            <div class="col-4">
                                                <label for="reference_month">Monat</label>
                                                <input name="reference_month" type="number"
                                                       class="form-control <?php echo (!empty($data['reference_date_err'])) ? 'is-invalid' : ''; ?>"
                                                       min="1" max="12" value="<?php echo $data['reference_month']; ?>">
                                            </div>
                                            <div class="col-4">
                                                <label for="reference_year">Jahr</label>
                                                <input name="reference_year" type="number"
                                                       class="form-control <?php echo (!empty($data['reference_date_err'])) ? 'is-invalid' : ''; ?>"
                                                       min="1900" max="3000"
                                                       value="<?php echo $data['reference_year']; ?>">
                                            </div>
                                            <span class="invalid-feedback"><?php echo $data['reference_date_err']; ?></span>
                                        </div>

                                        <label class="mt-2"> Intervallzahl</label>
                                        <input id="interval_count" name="interval_count" type="number" min="1" max="365"
                                               class="form-control mb-2 <?php echo (!empty($data['check_intervals_err'])) ? 'is-invalid' : ''; ?>"
                                               value="<?php echo $data['interval_count']; ?>"
                                               placeholder="Intervallzahl auswählen">

                                        <select id="interval_format" name="interval_format"
                                                class="form-control <?php echo (!empty($data['check_intervals_err'])) ? 'is-invalid' : ''; ?>">
                                            <option value="empty">Intervallformat auswählen</option>
                                            <option <?php echo $data['interval_format'] == 'Tages-Rhythmus' ? 'selected' : ''; ?>>
                                                Tages-Rhythmus
                                            </option>
                                            <option <?php echo $data['interval_format'] == 'Wochen-Rhythmus' ? 'selected' : ''; ?>>
                                                Wochen-Rhythmus
                                            </option>
                                            <option <?php echo $data['interval_format'] == 'Monats-Rhythmus' ? 'selected' : ''; ?>>
                                                Monats-Rhythmus
                                            </option>
                                            <option <?php echo $data['interval_format'] == 'Jahres-Rhythmus' ? 'selected' : ''; ?>>
                                                Jahres-Rhythmus
                                            </option>
                                            <option <?php echo $data['interval_format'] == 'Niemals' ? 'selected' : ''; ?>>
                                                Niemals
                                            </option>
                                        </select>
                                        <span class="invalid-feedback"><?php echo $data['check_intervals_err']; ?></span>
                                    </fieldset>
                                </div>

                                <div class="col-md-8">
                                    <h2 class="h5">Erinnerungsfunktion</h2>
                                    <p><strong>Vorraussetzung:</strong> ein Check-Intervall muss definiert sein.</p>
                                    <input id="activate_reminders" name="activate_reminders" type="checkbox"
                                           value="activated" <?php echo ($data['reminders_activated']) ? 'checked' : ''; ?>>
                                    <label for="activate_reminders" class="form-check-label">Erinnerungen
                                        aktivieren</label>

                                    <fieldset id="set_reminders"
                                              class="mt-2 <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                              disabled="disabled">
                                        <div class="form-row">
                                            <div class="col-2 my-3">
                                                <input id="remind_before" name="remind_before" type="number"
                                                       class="form-control <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                                       min='0' max='100' value="<?php echo $data['remind_before']; ?>">
                                            </div>
                                            <div class="col-10 my-3">
                                                <label for="remind_before" class="form-check-label">Tage vor Stichtag
                                                    erinnern</label>
                                            </div>

                                            <div class="col-md-12">
                                                <input id="remind_at" name="remind_at" type="checkbox"
                                                       class="mt-1 <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                                       value="activated" <?php echo ($data['remind_at']) ? 'checked' : ''; ?>>
                                                <label for="remind_at" class="ml-2 form-check-label">Zum Stichtag
                                                    erinnern</label>
                                            </div>

                                            <div class="col-2 my-3">
                                                <input id="remind_after" name="remind_after" type="number"
                                                       class="form-control <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                                       min='0' max='100' value="<?php echo $data['remind_after']; ?>">
                                            </div>
                                            <div class="col-10 my-3">
                                                <label for="remind_after" class="form-check-label" min='1' max='100'>Tage
                                                    nach Stichtag erinnern</label>
                                            </div>
                                        </div>
                                        <span class="invalid-feedback"><?php echo $data['reminders_err']; ?></span>
                                    </fieldset>
                                </div>


                                <div class="form-group col-md-4 mt-3">
                                    <p class="h5">bestehende Kompetenzen:</p>
                                    <input name="existing_compentencies" type="text"
                                           class="form-control <?php echo (!empty($data['existing_compentencies_err'])) ? 'is-invalid' : ''; ?>"
                                           value="<?php echo $data['existing_compentencies']; ?>"
                                           placeholder="bestehende Kompetenzen">
                                    <span class="invalid-feedback"><?php echo $data['existing_compentencies_err']; ?></span>
                                </div>
                                <div class="form-group col-md-4 mt-3">
                                    <p class="h5">erforderlicher Weiterbildungsbedarf:</p>
                                    <input name="required_education" type="text"
                                           class="form-control <?php echo (!empty($data['required_education_err'])) ? 'is-invalid' : ''; ?>"
                                           value="<?php echo $data['required_education']; ?>"
                                           placeholder="erforderlicher Weiterbildungsbedarf">
                                    <span class="invalid-feedback"><?php echo $data['required_education_err']; ?></span>
                                </div>

                                <div class="form-group col-md-4 mt-3">
                                    <p class="h5">Schlagwörter (Komma-Trennung):</p>
                                    <div class="tags-input" data-name="tags-input">
                                        <?php foreach ($data['tags_array'] as $tag): ?>
                                            <?php if (!empty($tag)) : ?>
                                                <span class="tag"><?php echo $tag; ?> <i class="close fas fa-times"></i></span>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                        <input type="text" class="mainInput">
                                        <input name="tags" type="hidden" class="hiddenInput"
                                               value="<?php echo $data['taglist']; ?>">
                                    </div>
                                </div>

                                <!--<?php /*
                                <div class="col-md-12">
                                    <h2 class="h4 px-3 py-2 bg-pe-darkgreen text-white">Flexibles Abfragesystem</h2>
                                    <h5>Angewendete Fragen</h5>
                                    <input id="flexible_questions" name="flexible_questions" type="hidden"
                                           value="<?php echo $data['flexible_questions']; ?>">
                                    <div id="added_questions">
                                        <ul class="list-group my-3">

                                            <?php if (!empty($data['flexible_questions_UI'])) : ?>

                                                <?php foreach ($data['flexible_questions_UI'] as $question) : ?>

                                                    <li class="list-group-item list-group-item-secondary d-flex align-items-center folder"
                                                        data-qstring="<?php echo $question['string_UI']; ?>">
                                                        <p class="mb-0">
                                                            <strong>Frage:</strong> <?php echo $question['question']; ?>
                                                        </p>
                                                        <p class="mb-0 ml-2">
                                                            <strong>Antwortmöglichkeiten:</strong> <?php echo $question['answers_UI']; ?>
                                                        </p>
                                                        <i class="close-btn fas fa-times ml-auto"></i>
                                                    </li>

                                                <?php endforeach; ?>
                                            <?php endif; ?>

                                        </ul>
                                        <button id="open_question_modal" type="button" class="btn btn-pe-lightgreen"
                                                data-toggle="modal" data-target="#question_modal">
                                            Abfrage hinzufügen
                                        </button>
                                        </button>
                                    </div>
                                </div>
                                */?>-->

                            </div> <!-- row -->
</form>


</div>
</div>
</div>
</div>

<!--<?php /*
<!-- Modal: Flexible Questions -->
<div id="question_modal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Flexibles Abfragesystem</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="question">Frage:</label>
                <input id="question" name="question" type="text" class="form-control" value=""
                       placeholder="Bsp: Willst du geschult werden?">

                <p class="h5 mt-3">Antwortmöglichkeiten auswählen</p>

                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="yes_no_checkbox">
                    <label class="form-check-label" for="yes_no_checkbox">Ja oder Nein</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="notify_checkbox">
                    <label class="form-check-label" for="notify_checkbox">Benachrichtigung von Mitarbeitern mit
                        Verfassungsmöglichkeit einer Nachricht</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="checkbox" id="remind_checkbox">
                    <label class="form-check-label" for="remind_checkbox">Erinnerungsfunktion mit Datums- bzw.
                        Intervallauswahl</label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>
*/?>-->

<!-- Modal: Knowledge Browser -->
<div id="knowledge_browser" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Dateibrowser</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="folders"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal: Resource Browser -->
<div id="resource_browser" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Dateibrowser</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="folders"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal: Roles - Responsible  -->
<div id="roles_responsible_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nutzer der Rolle "Verantwortlichkeit" bearbeiten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                <div class="modal-list">

                    <?php if (!empty($data['organs'])) : ?>

                        <div class="accordion" id="accordionExample">

                            <?php foreach ($data['organs'] as $organ) : ?>

                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link w-100 text-left" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#responsible-<?php echo $organ->id; ?>"
                                                    aria-expanded="true"
                                                    aria-controls="responsible-<?php echo $organ->id; ?>">
                                                <?php echo $organ->name; ?>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                         aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">

                                            <?php /* ############################################### */ ?>
                                            <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                            <?php if (!empty($organ->id == 2)) : ?>

                                                <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                    <?php foreach ($suborgan->users as $user) : ?>

                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $suborgan->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endforeach; ?>

                                            <?php else : ?>

                                                <?php /* ############################################### */ ?>
                                                <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                <?php foreach ($organ->users as $user) : ?>
                                                    <div class="form-check">
                                                        <input class="form-check-input"
                                                               type="checkbox"
                                                               value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                               name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                               data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                               data-organ="<?php echo $organ->name;
                                                               ?>">
                                                        <label class="form-check-label"
                                                               for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                    </div>

                                                <?php endforeach; ?>

                                            <?php endif; ?>


                                        </div>
                                    </div>
                                </div><!-- end of card -->

                            <?php endforeach; ?>

                        </div><!-- end of accordion -->

                    <?php endif; ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal: Roles - Concerned -->
<div id="roles_concerned_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nutzer der Rolle "Zuständigkeit" bearbeiten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                <div class="modal-list">

                    <?php if (!empty($data['organs'])) : ?>

                        <div class="accordion" id="accordionExample">

                            <?php foreach ($data['organs'] as $organ) : ?>

                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link w-100 text-left" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#responsible-<?php echo $organ->id; ?>"
                                                    aria-expanded="true"
                                                    aria-controls="responsible-<?php echo $organ->id; ?>">
                                                <?php echo $organ->name; ?>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                         aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">

                                            <?php /* ############################################### */ ?>
                                            <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                            <?php if (!empty($organ->id == 2)) : ?>

                                                <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                    <?php foreach ($suborgan->users as $user) : ?>

                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $suborgan->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endforeach; ?>

                                            <?php else : ?>

                                                <?php /* ############################################### */ ?>
                                                <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                <?php foreach ($organ->users as $user) : ?>
                                                    <div class="form-check">
                                                        <input class="form-check-input"
                                                               type="checkbox"
                                                               value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                               name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                               data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                               data-organ="<?php echo $organ->name;
                                                               ?>">
                                                        <label class="form-check-label"
                                                               for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                    </div>

                                                <?php endforeach; ?>

                                            <?php endif; ?>


                                        </div>
                                    </div>
                                </div><!-- end of card -->

                            <?php endforeach; ?>

                        </div><!-- end of accordion -->

                    <?php endif; ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal: Roles - Contributing -->
<div id="roles_contributing_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nutzer der Rolle "Mitwirkung" bearbeiten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                <div class="modal-list">

                    <?php if (!empty($data['organs'])) : ?>

                        <div class="accordion" id="accordionExample">

                            <?php foreach ($data['organs'] as $organ) : ?>

                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link w-100 text-left" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#responsible-<?php echo $organ->id; ?>"
                                                    aria-expanded="true"
                                                    aria-controls="responsible-<?php echo $organ->id; ?>">
                                                <?php echo $organ->name; ?>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                         aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">

                                            <?php /* ############################################### */ ?>
                                            <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                            <?php if (!empty($organ->id == 2)) : ?>

                                                <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                    <?php foreach ($suborgan->users as $user) : ?>

                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $suborgan->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endforeach; ?>

                                            <?php else : ?>

                                                <?php /* ############################################### */ ?>
                                                <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                <?php foreach ($organ->users as $user) : ?>
                                                    <div class="form-check">
                                                        <input class="form-check-input"
                                                               type="checkbox"
                                                               value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                               name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                               data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                               data-organ="<?php echo $organ->name;
                                                               ?>">
                                                        <label class="form-check-label"
                                                               for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                    </div>

                                                <?php endforeach; ?>

                                            <?php endif; ?>


                                        </div>
                                    </div>
                                </div><!-- end of card -->

                            <?php endforeach; ?>

                        </div><!-- end of accordion -->

                    <?php endif; ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal: Roles - Information -->
<div id="roles_information_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nutzer der Rolle "Information" bearbeiten</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                <div class="modal-list">

                    <?php if (!empty($data['organs'])) : ?>

                        <div class="accordion" id="accordionExample">

                            <?php foreach ($data['organs'] as $organ) : ?>

                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link w-100 text-left" type="button"
                                                    data-toggle="collapse"
                                                    data-target="#responsible-<?php echo $organ->id; ?>"
                                                    aria-expanded="true"
                                                    aria-controls="responsible-<?php echo $organ->id; ?>">
                                                <?php echo $organ->name; ?>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                         aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body">

                                            <?php /* ############################################### */ ?>
                                            <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                            <?php if (!empty($organ->id == 2)) : ?>

                                                <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                    <?php foreach ($suborgan->users as $user) : ?>

                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $suborgan->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endforeach; ?>

                                            <?php else : ?>

                                                <?php /* ############################################### */ ?>
                                                <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                <?php foreach ($organ->users as $user) : ?>
                                                    <div class="form-check">
                                                        <input class="form-check-input"
                                                               type="checkbox"
                                                               value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                               name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                               data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                               data-organ="<?php echo $organ->name;
                                                               ?>">
                                                        <label class="form-check-label"
                                                               for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                    </div>

                                                <?php endforeach; ?>

                                            <?php endif; ?>


                                        </div>
                                    </div>
                                </div><!-- end of card -->

                            <?php endforeach; ?>

                        </div><!-- end of accordion -->

                    <?php endif; ?>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>


<?php require APPROOT . '/views/inc/footer.php'; ?>
