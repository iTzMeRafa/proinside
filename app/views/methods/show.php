<?php require APPROOT . '/views/inc/header.php'; ?>
	<?php displayFlash('method_message'); ?>
	<?php displayFlash('no_permisssion'); ?>
<a href="javascript:history.go(-1)" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

	<?php if ($data['privUser']->hasPrivilege('2') && $data['method']->is_archived != 'true') : ?>
		<a href="<?php echo URLROOT; ?>/methods/edit/<?php echo $data['method']->id; ?>" class="btn btn-dark mb-3 float-right"><i class="fa fa-edit"></i> Bearbeiten</a>
	<?php endif; ?>

    <!-- Notifications -->
	<?php if ($data['method']->is_approved == 'false') : ?>
		<div class="alert alert-warning" role="alert">
			Diese Prozessmaßnahme ist noch nicht freigegeben.
		</div>
	<?php endif; ?>


	<?php if ($data['non_confirming_users']) : ?>
		<?php foreach ($data['non_confirming_users'] as $user) : ?>
			<?php if ($user['user_id'] == $_SESSION['user_id']) : ?>
				<div class="alert alert-warning" role="alert">
					<h4 class="alert-heading">Unakzeptierte Zuteilung!</h4>
					<p>Sie habe Ihre Zuteilung zu dieser Maßnahme in der Rolle <strong>"<?php echo $user['role']; ?>"</strong> noch nicht akzeptiert. Bitte schauen Sie sich die Maßnahme an und entscheiden sich, ob Sie diese akzeptieren wollen.</p>
					<hr>
					<button data-user="<?php echo $user['user_id']; ?>" data-method="<?php echo $data['method']->id; ?>" data-role="<?php echo $user['role_js']; ?>" type="button" class="confirm_assignment btn btn-success"><i class="fas fa-check"></i> Maßnahme akzeptieren</button>
				</div>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php if ($data['checkdate'] != false && $data['next_checkdate']['isWhen'] == 'past' && $data['method']->is_archived != 'true') : ?>
		<div class="alert alert-danger" role="alert">
			<h4 class="alert-heading">Maßnahmen Check überfällig!</h4>
			<p>Der Check dieser Maßnahme ist <?php echo $data['next_checkdate']['msg']; ?> überfällig.<br> Das planmäßige Checkdatum war am <?php echo $data['checkdate']['day'] .'. '. $data['checkdate']['month'] .' '. $data['checkdate']['year']; ?></p>
		</div>
	<?php endif; ?>

    <?php if ($data['checkdate'] != false && $data['next_checkdate']['isWhen'] == 'future') : ?>
        <div class="alert alert-success" role="alert">
            Der nächste Check dieser Maßnahme steht <?php echo $data['next_checkdate']['msg']; ?> an.<br> Checkdatum: <?php echo $data['checkdate']['day'] .'. '. $data['checkdate']['month'] .' '. $data['checkdate']['year']; ?>
            <div class="mt-2">
                <p class="mb-0"><strong>Erinnerungen:</strong>
                    <?php echo ($data['method']->remind_before != null) ? $data['method']->remind_before.' Tage im Vorraus,' : ''; ?>
                    <?php echo ($data['method']->remind_at != null) ? ' am Stichtag,' : ''; ?>
                    <?php echo ($data['method']->remind_after != null) ? $data['method']->remind_after.' Tage im Nachhinein' : ''; ?>
                </p>
            </div>
        </div>
    <?php endif; ?>

	<!-- CHECK WINDOW -->

	<?php if ($data['checkdate'] != false && $data['privUser']->hasPrivilege('15') && $data['method']->is_archived != 'true') : ?> 

		<?php if ($data['next_checkdate']['isWhen'] == 'past' || $data['next_checkdate']['isWhen'] == 'today') : ?>
			<div class="alert alert-success" role="alert">
				<h4 class="alert-heading">Diese Maßnahme muss heute gecheckt werden.</h4>
				<p>Bitte schauen Sie sich die Maßnahme genau an und schreiben einen angemessenen Kommentar zu Ihrem Check.</p>
				<div class="form-group">
					<textarea id="check_comment" class="form-control <?php echo (!empty($data['check_intervals_err'])) ? 'is-invalid' : ''; ?>" rows="3"></textarea>
					<span class="invalid-feedback"><?php echo $data['check_comment_err']; ?></span>
				</div>
				<button id="check_method" data-user="<?php echo $_SESSION['user_id']; ?>" data-method="<?php echo $data['method']->id; ?>" data-check-date="<?php echo $data['method']->check_date; ?>" data-format="<?php echo $data['method']->interval_format; ?>" data-count="<?php echo $data['method']->interval_count; ?>" type="button" class="btn btn-success"><i class="fas fa-check"></i> Check bestätigen</button>
			</div>
		<?php endif; ?>
	<?php endif; ?>

    <?php if ($data['method']->is_archived == 'true') : ?>
        <div class="alert alert-danger" role="alert">
            Die Maßnahme ist inaktiv und befindet sich im Archiv.
        </div>
    <?php endif; ?>
	<!-- /Notifications -->

    <div class="row mt-5 mb-5">

        <!-- Left Side -->
        <div class="col-md-6">

            <!-- Card -->
            <div class="card mb-5">
                <div class="card-body">
                    <h5 class="card-title"><?= $data['method']->name; ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted">
                        Erstellt von <a href="<?= URLROOT; ?>/users/show/<?php echo $data['user']->id; ?>" class="text-success"><?= $data['user']->lastname; ?></a> am <?= $data['method']->created_at; ?>
                    </h6>
                    <p class="card-text"><?= $data['method']->description; ?></p>

                    <!-- Bearbeiten Button -->
                    <?php if ($data['privUser']->hasPrivilege('2') && $data['method']->is_archived != 'true') : ?>
                        <a href="<?php echo URLROOT; ?>/methods/edit/<?php echo $data['method']->id; ?>" class="card-link"><i class="fa fa-edit"></i> Bearbeiten</a>
                    <?php endif; ?>
                    <!-- /Bearbeiten Button -->

                    <!-- Archivieren Button -->
                    <?php if ($data['privUser']->hasPrivilege('3') == true && $data['method']->is_archived != 'true') : ?>
                        <a href="#" class="card-link" data-toggle="modal" data-target="#archiveModal"><i class="fa fa-trash"></i> Archivieren</a>
                    <?php endif; ?>
                    <!-- /Archivieren Button -->
                </div>
            </div>
            <!-- /Card -->

            <h3>Funktionszuteilung</h3>
            <!-- Funktionszuteilung -->
            <div class="row mb-4">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Verantwortlich
                        </div>
                        <div class="card-body">
                            <?php if ($data['roles_responsible']) : ?>
                                <?php foreach ($data['roles_responsible'] as $item) : ?>
                                    <?php if ($item['user']) : ?>
                                        <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>">
                                            <h5 class="card-title"><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></h5>

                                            <?php if (!empty($item['suborgan'])) : ?>
                                                <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                            <?php else : ?>
                                                <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                            <?php endif; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                            <?php else : ?>
                               <i class="fas fa-exclamation-triangle"></i> Keine Zuteilung
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Zuständig
                        </div>
                        <div class="card-body">
                            <?php if ($data['roles_concerned']) : ?>
                                <?php foreach ($data['roles_concerned'] as $item) : ?>
                                    <?php if ($item['user']) : ?>
                                        <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>">
                                            <h5 class="card-title"><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></h5>

                                            <?php if (!empty($item['suborgan'])) : ?>
                                                <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                            <?php else : ?>
                                                <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                            <?php endif; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                            <?php else : ?>
                               <i class="fas fa-exclamation-triangle"></i> Keine Zuteilung
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Mitwirkend
                        </div>
                        <div class="card-body">
                            <?php if ($data['roles_contributing']) : ?>
                                <?php foreach ($data['roles_contributing'] as $item) : ?>
                                    <?php if ($item['user']) : ?>
                                        <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>">
                                            <h5 class="card-title"><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></h5>

                                            <?php if (!empty($item['suborgan'])) : ?>
                                                <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                            <?php else : ?>
                                                <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                            <?php endif; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                            <?php else : ?>
                               <i class="fas fa-exclamation-triangle"></i> Keine Zuteilung
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Informationen
                        </div>
                        <div class="card-body">
                            <?php if ($data['roles_information']) : ?>
                                <?php foreach ($data['roles_information'] as $item) : ?>
                                    <?php if ($item['user']) : ?>
                                        <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>">
                                            <h5 class="card-title"><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></h5>

                                            <?php if (!empty($item['suborgan'])) : ?>
                                                <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                            <?php else : ?>
                                                <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                            <?php endif; ?>
                                        </a>
                                    <?php endif; ?>
                                <?php endforeach; ?>

                            <?php else : ?>
                                <i class="fas fa-exclamation-triangle"></i> Keine Zuteilung
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /Funktionszuteilung -->

            <!-- Erforderliches Wissen -->
            <h3>Erforderliches Wissen</h3>
            <div class="row">
                <div class="col-md-12">
                    
                    <?php if (!empty($data['method']->required_knowledge_text)) : ?>
                        <h6 class="card-subtitle mb-2 text-muted mb-3"><?= $data['method']->required_knowledge_text; ?></h6>
                    <?php endif; ?>

                    <?php if (!empty($data['required_knowledge']) || !empty($data['required_knowledge_folders'])) : ?>

                        <?php if (!empty($data['required_knowledge'])) : ?>
                            <ul class="list-group">
                                <?php foreach ($data['required_knowledge'] as $resource) : ?>
                                    <a href="<?php echo URLROOT.''.$resource['path']; ?>" class="list-group-item list-group-item-action" target="_blank">
                                        <i class="fa fa-file"></i>
                                        <?php echo $resource['name']; ?>
                                    </a>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <?php if (!empty($data['required_knowledge_folders'])) : ?>
                            <h3 class="h4 d-block mt-3">Ordner</h3>
                            <div class="row">

                                <?php foreach ($data['required_knowledge_folders'] as $resource) : ?>
                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title"><?php echo $resource['folderName']; ?></h5>
                                            </div>
                                            <ul class="list-group list-group-flush">

                                                <?php foreach ($resource['files'] as $file) : ?>
                                                    <a href="<?php echo URLROOT .'/'. $file['file_path']; ?>" class="list-group-item list-group-item-action" target="_blank"><i class="fa fa-file"></i> <?php echo $file['file_name']; ?></a>
                                                <?php endforeach; ?>

                                            </ul>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>

                    <?php else : ?>
                        <p><i class="fas fa-exclamation-triangle"></i> Es wurden keine Dateien angehangen.</p>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /Erforderliches Wissen -->

            <hr>

            <!-- Erforderliche Resourcen -->
            <h3>Erforderliches Ressourcen</h3>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <?php if (!empty($data['method']->required_resources_text)) : ?>
                        <h6 class="card-subtitle mb-2 text-muted mb-3"><?= $data['method']->required_resources_text; ?></h6>
                    <?php endif; ?>

                    <?php if (!empty($data['required_resources']) || !empty($data['required_resources_folders'])) : ?>

                        <?php if (!empty($data['required_resources'])) : ?>
                            <ul class="list-group">
                                <?php foreach ($data['required_resources'] as $resource) : ?>
                                    <a href="<?php echo URLROOT.''.$resource['path']; ?>" class="list-group-item list-group-item-action" target="_blank">
                                        <i class="fa fa-file"></i>
                                        <?php echo $resource['name']; ?>
                                    </a>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                        <?php if (!empty($data['required_resources_folders'])) : ?>
                            <h3 class="h4 d-block mt-3">Ordner</h3>
                            <div class="row">

                                <?php foreach ($data['required_resources_folders'] as $resource) : ?>

                                    <div class="col-md-4">
                                        <div class="card">
                                            <div class="card-body">
                                                <h5 class="card-title"><?php echo $resource['folderName']; ?></h5>
                                            </div>
                                            <ul class="list-group list-group-flush">

                                                <?php foreach ($resource['files'] as $file) : ?>
                                                    <a href="<?php echo URLROOT .'/'. $file['file_path']; ?>" class="list-group-item list-group-item-action" target="_blank"><i class="fa fa-file"></i> <?php echo $file['file_name']; ?></a>
                                                <?php endforeach; ?>

                                            </ul>
                                        </div>
                                    </div>

                                <?php endforeach; ?>
                            </div>

                        <?php endif; ?>

                    <?php else : ?>
                        <p><i class="fas fa-exclamation-triangle"></i> Es wurden keine Dateien angehangen.</p>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /Erforderliche Resourcen -->

            <hr>

            <!-- Checks -->
            <h3>Checks</h3>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <?php if (!empty($data['checks'])) : ?>
                        <?php foreach ($data['checks'] as $check) : ?>
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title"><?php echo $check['checked_at']['day'] .'. '. $check['checked_at']['month'] .' '. $check['checked_at']['year'] .' um '. $check['checked_at']['hour'] .':'. $check['checked_at']['min'] .' Uhr'; ?></h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Gecheckt von
                                        <a href="<?php echo URLROOT; ?>/users/show/<?php echo $check['user']->id; ?>" class="text-success"><?php echo $check['user']->firstname .' '. $check['user']->name; ?></a>
                                    </h6>
                                    <p class="card-text"><?php echo $check['comment']; ?></p>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p><i class="fas fa-exclamation-triangle"></i> Es wurden bisher noch keine Checks durchgeführt.</p>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /Checks -->

            <hr>

            <!-- Flexible Abfragen -->
            <h3>Flexible Abfragen</h3>
            <div class="row">
                <div class="col-md-12 mt-3">
                    <?php if (!empty($data['questions'])) : ?>
                        <?php foreach ($data['questions'] as $question) : ?>
                            <div class="alert alert-secondary" role="alert">
                                <p class="h4"><?php echo $question->question; ?></p>

                                <?php if ($question->yes_no == 'true') : ?>
                                    <div class="yes_no answer mt-3">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="question<?php echo $question->id; ?>" id="question<?php echo $question->id; ?>yes" value="true">
                                            <label class="form-check-label" for="question<?php echo $question->id; ?>yes">Ja</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="question<?php echo $question->id; ?>" id="question<?php echo $question->id; ?>no" value="false">
                                            <label class="form-check-label" for="question<?php echo $question->id; ?>no">Nein</label>
                                        </div>
                                        <span class="yesno-error d-block"></span>
                                    </div>
                                <?php endif; ?>

                                <?php if ($question->notify == 'true') : ?>
                                    <div class="notify answer mt-3">
                                        <p class="h6">Welche Mitarbeiter möchten Sie benachrichtigen?</p>
                                        <button id="open_coworkers_modal" type="button" class="coworkers-btn btn btn-primary my-3" data-toggle="modal" data-target="#coworkers_browser" data-id="<?php echo $question->id; ?>">Mitarbeiter auswählen</button>
                                        <span class="coworkers-error d-block"></span>
                                        <div class="stored-coworkers">
                                            <input id="notifyUsersQ-<?php echo $question->id; ?>" class="userIds" type="hidden">
                                            <ul id="q-<?php echo $question->id; ?>-notifyUsers" class="list-group my-3">
                                            </ul>
                                        </div>
                                        <p class="h6">Ihre Nachricht</p>
                                        <textarea class="message form-control" id="textarea-q-<?php echo $question->id; ?>" rows="3"></textarea>
                                        <span class="msg-error d-block"></span>
                                    </div>
                                <?php endif; ?>

                                <?php if ($question->remind == 'true') : ?>
                                    <div class="remind answer mt-3">
                                        <p class="h6">Zu welchem Datum möchten Sie eine Erinnerung schalten?</p>
                                        <div class="remind-date form-row">
                                            <div class="col-4">
                                                <label for="remind_day">Tag</label>
                                                <input name="remind_day" type="number" class="remind_day form-control" min="1" max="31" value="">
                                            </div>
                                            <div class="col-4">
                                                <label for="remind_month">Monat</label>
                                                <input name="remind_month" type="number" class="remind_month form-control" min="1" max="12" value="">
                                            </div>
                                            <div class="col-4">
                                                <label for="remind_year">Jahr</label>
                                                <input name="remind_year" type="number" class="remind_year form-control" min="2019" max="3000" value="">
                                            </div>
                                        </div>
                                        <span class="remind-error d-block"></span>
                                    </div>
                                <?php endif; ?>
                                <button type="button" class="submit-answer btn btn-outline-info mt-3">Antwort absenden</button>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p><i class="fas fa-exclamation-triangle"></i> Es wurden keine individuellen Fragen gestellt.</p>
                    <?php endif; ?>
                </div>
            </div>
            <!-- /Flexible Abfragen -->

        </div>
        <!-- /Left Side -->

        <!-- Right Side -->
        <div class="col-md-6">
            <table class="table table-hover">
                <thead style="background-color: #0B676E; color: white;">
                <tr>
                    <th scope="col"><strong>Attribut</strong></th>
                    <th scope="col"><strong>Wert</strong></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">ID</th>
                    <td>M<?= $data['method']->id; ?></td>
                </tr>
                <tr>
                    <th scope="row">Schlagwörter</th>
                    <td>
                        <?php if (!empty($data['tags'])) : ?>
                            <?php foreach ($data['tags'] as $tag) : ?>
                                <?= '<span class="badge badge-secondary mx-1">'.$tag.'</span>'; ?>
                            <?php endforeach; ?>
                        <?php else : ?>
                            <p>-</p>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Teil des Prozesses</th>
                    <td><a href="<?php echo URLROOT; ?>/processes/show/<?php echo $data['method']->process_id; ?>"><?php echo $data['process']->id.' - '.$data['process']->process_name; ?></a></td>
                </tr>
                <tr>
                    <th scope="row">Unternehmensbereich</th>
                    <td><?php echo $data['method']->occurrence; ?></td>
                </tr>
                <tr>
                    <th scope="row">Aktion/Maßnahme</th>
                    <td><?php echo $data['method']->solution; ?></td>
                </tr>
                <tr>
                    <th scope="row">Zielvorgabe</th>
                    <td><?php echo $data['method']->goal; ?></td>
                </tr>
                <tr>
                    <th scope="row">Ursache für die Feststellung</th>
                    <td><?php echo $data['method']->reason; ?></td>
                </tr>
                <tr>
                    <th scope="row">Status</th>
                    <td><?php echo $data['method']->status; ?></td>
                </tr>
                <tr>
                    <th scope="row">ISO-Relevant</th>
                    <td><?= $data['method']->iso_relevant == 1 ? 'Ja' : 'Nein'; ?></td>
                </tr>
                <tr>
                    <th scope="row">Bestehende Kompetenzen</th>
                    <td><?php echo $data['method']->existing_compentencies; ?></td>
                </tr>
                <tr>
                    <th scope="row">Erforderlicher Weiterbildungsbedarf</th>
                    <td><?php echo $data['method']->required_education; ?></td>
                </tr>
                <tr>
                    <th scope="row">Wiederkehrende Check-Intervalle</th>
                    <td>
                        <?=
                            empty($data['method']->check_intervals) || $data['method']->check_intervals == 'Kein Check Intervall'
                                ? 'Kein Check Intervall'
                                : 'im '.$data['method']->check_intervals;
                        ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Startdatum</th>
                    <td>
                        <?=
                            empty($data['method']->check_intervals) || $data['method']->check_intervals == 'Kein Check Intervall'
                                ? 'Kein Startdatum'
                                : $data['reference_date'];
                        ?>
                            <br />
                        <!-- StartDate MSG -->
                        <?php
                        if (isset($data['method']->startDate_msg) && $data['method']->startDate_msg['isWhen'] == 'future') : ?>
                            Die Maßnahme fängt <strong><?= $data['method']->startDate_msg['msg']; ?></strong> an. <br />
                            <small class="text-muted">(<?= $data['method']->startDate_msg['diffInDays']; ?> Tage ab heute)</small>
                        <?php endif; ?>

                        <?php
                        if (isset($data['method']->startDate_msg) && $data['method']->startDate_msg['isWhen'] == 'past') : ?>
                            Die Maßnahme ist <strong><?= $data['method']->startDate_msg['msg']; ?></strong> überfällig. <br />
                            <small class="text-muted">(Seit <?= $data['method']->startDate_msg['diffInDays']; ?> Tagen)</small>
                        <?php endif; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- /Right Side -->

    </div>




	<!-- Modal: Coworkers Browser -->
	<div id="coworkers_browser" class="modal" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Zugeteilte Mitarbeiter auswählen</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
						<ul class="list-group">
							<?php if ($data['roles_responsible']) : ?>
								<li class="list-group-item active">Rolle: Verantwortlich</li>
								<?php foreach ($data['roles_responsible'] as $role) : ?>
									<li class="list-group-item">
										<?php echo $role->firstname.' '.$role->name; ?>
										<input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
									</li>
								<?php endforeach; ?>

							<?php else : ?>
								<li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
							<?php endif; ?>

							<?php if ($data['roles_concerned']) : ?>
								<li class="list-group-item active">Rolle: Zuständig</li>
								<?php foreach ($data['roles_concerned'] as $role) : ?>
									<li class="list-group-item">
										<?php echo $role->firstname.' '.$role->name; ?>
										<input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
									</li>
								<?php endforeach; ?>

							<?php else : ?>
								<li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
							<?php endif; ?>

							<?php if ($data['roles_contributing']) : ?>
								<li class="list-group-item active">Rolle: Mitwirkend</li>
								<?php foreach ($data['roles_contributing'] as $role) : ?>
									<li class="list-group-item">
										<?php echo $role->firstname.' '.$role->name; ?>
										<input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
									</li>
								<?php endforeach; ?>

							<?php else : ?>
								<li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
							<?php endif; ?>

							<?php if ($data['roles_information']) : ?>
								<li class="list-group-item active">Rolle: Information</li>
								<?php foreach ($data['roles_information'] as $role) : ?>
									<li class="list-group-item">
										<?php echo $role->firstname.' '.$role->name; ?>
										<input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
									</li>
								<?php endforeach; ?>

							<?php else : ?>
								<li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
							<?php endif; ?>
						</ul>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
            <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Archivieren Modal -->
    <div class="modal fade" id="archiveModal" tabindex="-1" role="dialog" aria-labelledby="archiveModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="archiveModalLabel">Maßnahme archivieren</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Wollen Sie diese Maßnahme wirklich archivieren?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form class="float-right"action="<?php echo URLROOT; ?>/methods/delete/<?php echo $data['method']->id; ?>" method="post">
                        <input type="submit" class="btn btn-danger" value="Löschen 	&#40;Archivieren&#41;">
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php require APPROOT . '/views/inc/footer.php'; ?>
