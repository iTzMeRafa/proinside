<?php require APPROOT . '/views/inc/header.php'; ?>
<a href="javascript:history.go(-1)" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

<div class="row">
    <div class="col-12">
        <h1 class="h2">Neues Organ erstellen</h1>
        <p>Füllen Sie die nachfolgenden Felder aus, um eine neues Organ zu erstellen</p>
    </div>

    <div class="col-12">
        <form action="<?php echo URLROOT; ?>/organs/add" method="post">
            <div class="row">

                <div class="form-group col-md-4">
                    <p class="h5">Name: <sup>*</sup></p>
                    <input name="name" type="text"
                           class="form-control <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['name']; ?>" placeholder="Name des Organs">
                    <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
                </div>
                <div class="form-group col-md-2">
                    <p class="h5">Kürzel:</p>
                    <input name="abbreviation" type="text"
                           class="form-control <?php echo (!empty($data['abbreviation_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['abbreviation']; ?>" placeholder="Kürzel">
                    <span class="invalid-feedback"><?php echo $data['abbreviation_err']; ?></span>
                </div>
                <div class="form-group col-md-6">

                    <div class="row">

                        <!-- Zweck -->
                        <div class="col-md-6">
                            <p class="h5">Zweck:</p>
                            <textarea
                                placeholder="Zweck"
                                name="description"
                                class="form-control"
                                rows="1"
                                cols="80"
                            >
                            <?= $data['description']; ?>
                        </textarea>
                        </div>

                        <!-- Levels -->
                        <div class="col-md-6">
                            <p class="h5">Ebene (1-4):</p>
                            <input min="1" max="4" type="number" class="form-control" name="level_id" placeholder="3"/>
                        </div>

                    </div>

                </div>

                <div class="form-group col-md-6">
                    <h2>Führungsebene:</h2>
                    <button id="open_management_modal" type="button" class="btn btn-pe-darkgreen d-block w-100"
                            data-toggle="modal" data-target="#management_modal">Nutzer auswählen
                    </button>
                    <input id="management_level" name="management_level"
                           class="form-control <?php echo (!empty($data['management_level_err'])) ? 'is-invalid' : ''; ?>"
                           type="hidden" value="<?php echo $data['management_level']; ?>">
                    <ul id="managementUI" class="list-group">

                        <?php if (!empty($data['management_level_UI'])) : ?>

                            <?php foreach ($data['management_level_UI'] as $user) : ?>
                                <li class="list-group-item d-flex align-items-center"
                                    data-id="<?php echo $user->id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?>
                                    <i class="close-btn fas fa-times ml-auto"></i></li>
                            <?php endforeach; ?>

                        <?php endif; ?>

                    </ul>
                    <span class="invalid-feedback"><?php echo $data['management_level_err']; ?></span>
                </div>

                <div class="form-group col-md-6">
                    <h2>Mitarbeiter:</h2>
                    <button id="open_staff_modal" type="button" class="btn btn-pe-darkgreen d-block w-100"
                            data-toggle="modal" data-target="#staff_modal">Nutzer auswählen
                    </button>
                    <input id="staff" name="staff" type="hidden"
                           class="form-control <?php echo (!empty($data['staff_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['staff']; ?>">
                    <ul id="staffUI" class="list-group">

                        <?php if (!empty($data['staff_UI'])) : ?>

                            <?php foreach ($data['staff_UI'] as $user) : ?>
                                <li class="list-group-item d-flex align-items-center"
                                    data-id="<?php echo $user->id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?>
                                    <i class="close-btn fas fa-times ml-auto"></i></li>
                            <?php endforeach; ?>

                        <?php endif; ?>

                    </ul>
                    <span class="invalid-feedback"><?php echo $data['staff_err']; ?></span>
                </div>

            </div>
            <input type="submit" class="btn btn-pe-lightgreen float-right" value="Erstellen">
        </form>
    </div>

    <!-- Management Modal -->
    <div id="management_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nutzer der Führungsebene bearbeiten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Wählen Sie Nutzer aus der Liste aus, um Sie der Führungsebene hinzuzufügen.</p>
                    <div class="modal-list">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button id="saveChanges" type="button" class="btn btn-pe-lightgreen">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Staff Modal -->
    <div id="staff_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nutzer der Mitarbeiterebene bearbeiten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                    <div class="modal-list">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button id="saveChanges" type="button" class="btn btn-pe-lightgreen">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
