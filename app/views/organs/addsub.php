<?php require APPROOT . '/views/inc/header.php'; ?>
<a href="javascript:history.go(-1)" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

  <?php if ($data['parent']->id != 2) : ?>

    <div class="row">
      <div class="col-12">
        <h1 class="h2">Sub Organe bearbeiten</h1>
        <p>Bearbeiten Sie die nachfolgenden Felder, um Suborgane zum bestehend Organ hinzuzufügen oder zu entfernen</p>
      </div>

      <div class="col-12">
        <form action="<?php echo URLROOT; ?>/organs/addsub/<?php echo $data['parent']->id; ?>" method="post">
          <div class="row">

            

          </div><!-- end of row -->
          <h2 class="mt-2">Suborgane:</h2>

            <div class="col-12 mb-3">
              

              <div id="added_suborgans" class="row">

              <?php foreach ($data['suborgans'] as $key => $suborgan) : ?>

              <div id="sub-<?php echo $key+1; ?>" class="suborgan col-md-4 mb-3">
                <div class="border p-3 d-flex flex-column">

                  <span class="remove_organ ml-auto"><i class="fas fa-times"></i></span>
                  
                  <input name="<?php echo $key+1; ?>-id" type="hidden" value="<?php echo $suborgan['id']; ?>">
                  <p class="h5">Name: <span class="nameUI font-weight-normal"><?php echo $suborgan['name']; ?></span></p>
                  <input class="name" name="<?php echo $key+1; ?>-name" type="hidden" value="<?php echo $suborgan['name']; ?>">

                  <p class="h5">Kürzel: <span class="abbrUI font-weight-normal"><?php echo $suborgan['abbreviation']; ?></span></p>
                  <input class="abbreviation" name="<?php echo $key+1; ?>-abbreviation" type="hidden" value="<?php echo $suborgan['abbreviation']; ?>">

                  <ul class="workersUI list-group mt-2">
                    <li class="list-group-item active">Mitarbeiter</li>
                    <?php foreach ($suborgan['staff'] as $subKey => $user) : ?>
                    <li class="list-group-item"><?php echo $user->firstname.' '.$user->lastname; ?></li>
                    <?php endforeach; ?>
                  </ul>
                  <input class="workers" name="<?php echo $key+1; ?>-workers" type="hidden" value="<?php echo $suborgan['workers']; ?>">
                  
                  <button type="button" class="editSubOrgan btn btn-pe-darkgreen d-block w-100 mt-3" data-toggle="modal" data-target="#suborgan_modal">Suborgan bearbeiten</button>
                </div>

              </div>


              <?php endforeach; ?>
                  
              </div>
            </div>

            <button id="open_suborgan_modal" type="button" class="btn btn-pe-lightgreen d-block w-100 mb-2" data-toggle="modal" data-target="#suborgan_modal">Suborgan hinzufügen</button>

            

            

          
          <input type="submit" class="btn btn-pe-darkgreen float-right mt-3" value="Aktualisieren">
        </form>
      </div>

      <!-- Suborgan Modal -->
      <div id="suborgan_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Suborgan hinzufügen</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <div class="row">

                <div class="col-md-6">
                    <input id="existing_id" type="hidden">
                    <p class="font-weight-bold" >Name des Suborgans</p>
                    <input id="sub_name" class="form-control mb-2" type="text" placeholder="Name des Suborgans">
                    <p class="font-weight-bold mt-2">Abkürzung des Suborgans</p>
                    <input id="sub_abbr" class="form-control" type="text" placeholder="Kürzel">
                </div>

                <div id="user_list" class="col-md-6">
                  <p class="mb-0 font-weight-bold">Mitarbeiter</p>
                  <p>Setzen Sie einen Haken bei einem Nutzer um ihn zum Suborgan hinzuzufügen.</p>
                  <?php foreach ($data['workers'] as $worker) : ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="<?php echo $worker->id; ?>">
                      <label class="form-check-label"><?php echo $worker->firstname.' '.$worker->lastname; ?></label>
                    </div>
                  <?php endforeach; ?>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
              <button id="saveChanges" type="button" class="btn btn-pe-darkgreen">Organ hinzufügen</button>
            </div>
          </div>
        </div>
      </div>

    </div>

  <?php else : ?>

    <?php /* ########### CASE: STAFF FUNCTIONS ########### */ ?>
    <div id="staff-functions" class="row">
      <div class="col-12">
        <h1 class="h2">Stabsfunktionen bearbeiten</h1>
        <p>Bearbeiten Sie die nachfolgenden Felder, um Stabsfunktionen zum Organigramm hinzuzufügen oder zu entfernen.<br>Es können <strong>maximal 8 Stabsfunktionen</strong> hinzugefügt werden.</p>
      </div>

      <div class="col-12">
        <form action="<?php echo URLROOT; ?>/organs/addsub/<?php echo $data['parent']->id; ?>" method="post">
          <div class="row">

            

          </div><!-- end of row -->
          <h2 class="mt-2">Stabsfunktionen:</h2>

            <div class="col-12 mb-3">
              

              <div id="added_suborgans" class="row">

              <?php foreach ($data['suborgans'] as $key => $suborgan) : ?>

              <div id="sub-<?php echo $key+1; ?>" class="suborgan col-md-4 mb-3">
                <div class="border p-3 d-flex flex-column">

                  <span class="remove_organ ml-auto"><i class="fas fa-times"></i></span>
                  
                  <input name="<?php echo $key+1; ?>-id" type="hidden" value="<?php echo $suborgan['id']; ?>">
                  <p class="h5">Name:</p>
                  <p class="h5"><span class="nameUI font-weight-normal"><?php echo $suborgan['name']; ?></span></p>
                  <input class="name" name="<?php echo $key+1; ?>-name" type="hidden" value="<?php echo $suborgan['name']; ?>">

                  <p class="h5">Kürzel:</p>
                  <p class="h5"><span class="abbrUI font-weight-normal"><?php echo $suborgan['abbreviation']; ?></span></p>
                  <input class="abbreviation" name="<?php echo $key+1; ?>-abbreviation" type="hidden" value="<?php echo $suborgan['abbreviation']; ?>">

                  <ul class="workersUI list-group mt-2">
                    <li class="list-group-item active">Mitarbeiter</li>
                    <?php foreach ($suborgan['staff'] as $subKey => $user) : ?>
                    <li class="list-group-item"><?php echo $user->firstname.' '.$user->lastname; ?></li>
                    <?php endforeach; ?>
                  </ul>
                  <input class="workers" name="<?php echo $key+1; ?>-workers" type="hidden" value="<?php echo $suborgan['workers']; ?>">
                  
                  <button type="button" class="editSubOrgan btn btn-pe-darkgreen d-block w-100 mt-3" data-toggle="modal" data-target="#suborgan_modal">Stabsfunktion bearbeiten</button>
                </div>

              </div>


              <?php endforeach; ?>
                  
              </div>
            </div>

            <?php if (count($data['suborgans']) < 8) : ?>

              <button id="open_suborgan_modal" type="button" class="btn btn-pe-lightgreen d-block w-100 mb-2" data-toggle="modal" data-target="#suborgan_modal">Stabsfunktion hinzufügen</button>

            <?php endif; ?>
            

          
          <input type="submit" class="btn btn-pe-darkgreen float-right mt-3" value="Aktualisieren">
        </form>
      </div>

      <!-- Staff Functions Modal -->
      <div id="suborgan_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Stabsfunktion hinzufügen</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              
              <div class="row">

                <div class="col-md-6">
                    <input id="existing_id" type="hidden">
                    <p class="font-weight-bold" >Name der Stabsfunktion</p>
                    <input id="sub_name" class="form-control mb-2" type="text" placeholder="Name des Suborgans">
                    <p class="font-weight-bold mt-2">Abkürzung der Stabsfunktion</p>
                    <input id="sub_abbr" class="form-control" type="text" placeholder="Kürzel">
                </div>

                <div id="user_list" class="col-md-6 mb-3">
                  <p class="mb-0 font-weight-bold">Mitarbeiter</p>
                  <p>Setzen Sie einen Haken bei einem Nutzer, um ihn zur Stabsfunktion hinzuzufügen.</p>
                  <?php foreach ($data['allUsers'] as $worker) : ?>
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="<?php echo $worker->id; ?>">
                      <label class="form-check-label"><?php echo $worker->firstname.' '.$worker->lastname; ?></label>
                    </div>
                  <?php endforeach; ?>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
              <button id="saveChanges" type="button" class="btn btn-pe-darkgreen">Organ hinzufügen</button>
            </div>
          </div>
        </div>
      </div>

    </div>

  <?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
