<?php require APPROOT . '/views/inc/header.php'; ?>
  <a href="<?php echo URLROOT; ?>/organs/show/<?php echo $data['id']; ?>" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

  <div class="row">
    <div class="col-12">
      <h1 class="h2">Organ bearbeiten</h1>
      <p>Bearbeiten Sie die nachfolgenden Felder aus, um das bestehende Organ anzupassen</p>
    </div>

    <div class="col-12">
      <form action="<?php echo URLROOT; ?>/organs/edit/<?php echo $data['id']; ?>" method="post">
        <div class="row">

          <div class="form-group col-md-4">
            <h2 class="h5">Name: <sup>*</sup></h2>
            <input name="name" type="text" class="form-control <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['name']; ?>" placeholder="Name">
            <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
          </div>

          <div class="form-group col-md-2">
            <h2 class="h5">Kürzel: <sup>*</sup></h2>
            <input name="abbreviation" type="text" class="form-control <?php echo (!empty($data['abbreviation_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['abbreviation']; ?>" placeholder="Kürzel">
            <span class="invalid-feedback"><?php echo $data['abbreviation_err']; ?></span>
          </div>

          <div class="form-group col-md-6">
            <h2 class="h5">Zweck:</h2>
            <textarea name="description" class="form-control" rows="3" cols="80"><?php echo $data['description']; ?></textarea>
          </div>

          <div class="form-group col-md-6">
            <h2>Führungsebene:</h2>
            <button id="open_management_modal" type="button" class="open_management btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal" data-target="#management_modal">Nutzer auswählen</button>
            <input id="management_level" name="management_level" class="hiddenInput form-control" type="hidden" value="<?php echo $data['management_level']; ?>">
            <ul id="" class="managementUI list-group">
              <?php if ($data['management_level_UI']) : ?>

                <?php foreach ($data['management_level_UI'] as $user) : ?>
                  <li class="list-group-item d-flex align-items-center" data-id="<?php echo $user->id; ?>"><?php echo $user->firstname.' '.$user->lastname; ?><i class="close-btn fas fa-times ml-auto"></i></li>
                <?php endforeach; ?>

              <?php else : ?>
                <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
              <?php endif; ?>
            </ul>
            <span class="invalid-feedback"><?php echo $data['management_level_err']; ?></span>
          </div>

          <div class="form-group col-md-6">
            <h2>Mitarbeiter:</h2>
            <button id="open_staff_modal" type="button" class="open_staff btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal" data-target="#staff_modal">Nutzer auswählen</button>
            <input id="staff" name="staff" type="hidden" class="hiddenInput form-control" value="<?php echo $data['staff']; ?>">
            <ul id="" class="staffUI list-group">
              <?php if ($data['staff']) : ?>

                <?php foreach ($data['staff_UI'] as $user) : ?>
                  <li class="list-group-item d-flex align-items-center" data-id="<?php echo $user->id; ?>"><?php echo $user->firstname.' '.$user->lastname; ?><i class="close-btn fas fa-times ml-auto"></i></li>
                <?php endforeach; ?>

              <?php else : ?>
                <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
              <?php endif; ?>
            </ul>
            <span class="invalid-feedback"><?php echo $data['staff_err']; ?></span>
          </div>

        </div><!-- end of row -->

        
        <input type="submit" class="btn btn-pe-lightgreen float-right" value="Aktualisieren">
      </form>
    </div>




    <!-- Management Modal -->
    <div id="management_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nutzer der Führungsebene bearbeiten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Wählen Sie Nutzer aus der Liste aus, um Sie der Führungsebene hinzuzufügen.</p>
            <div class="modal-list">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
            <button id="saveChanges" type="button" class="btn btn-pe-lightgreen">Änderungen speichern</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Staff Modal -->
    <div id="staff_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Nutzer der Mitarbeiterebene bearbeiten</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
            <div class="modal-list">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
            <button id="saveChanges" type="button" class="btn btn-pe-lightgreen">Änderungen speichern</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Confirmation Modal -->
    <div id="confirmation_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Bestätigung erforderlich</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Bitte stellen Sie sicher, dass alle anderen Daten gespeichert sind.<br><strong>Ansonsten gehen noch nicht gespeicherte Daten bei der Weiterleitung verloren.</strong></p>
            <div class="modal-list">

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
            <a href="<?php echo URLROOT; ?>/organs/addsub/<?php echo $data['id']; ?>" class="btn btn-pe-lightgreen">Suborgane bearbeiten</a>
          </div>
        </div>
      </div>
    </div>

  </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
