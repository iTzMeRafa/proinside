<!-- Main Header -->
<?php require APPROOT . '/views/inc/header.php'; ?>

    <!-- Flashes -->
    <?php require APPROOT . '/views/organs/templates/indexListing/displayFlashs.php'; ?>

    <!-- Header -->
    <?php require APPROOT . '/views/organs/templates/indexListing/header.php'; ?>

    <!-- Organ Listing -->
    <?php require APPROOT . '/views/organs/templates/indexListing/organListing.php'; ?>

<!-- Footer -->
<?php require APPROOT . '/views/inc/footer.php'; ?>
