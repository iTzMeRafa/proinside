<?php require APPROOT . '/views/inc/header.php'; ?>
  <?php displayFlash('organ_message'); ?>

  <div class="row mb-3">
    <div class="col-md-6 mb-3">
      <h1 class="h1"><i class="fas fa-sitemap mr-2"></i>Organigramm Ansicht</h1>
    </div>

    <div class="col-md-6 mt-2">
      <a href="<?php echo URLROOT; ?>/organs/index" class="btn btn-pe-lightgreen w-100">
        <i class="fas fa-pencil-alt mr-2"></i> Stammdaten bearbeiten
      </a>
    </div>

    <div id="organigram" class="px-4">

      <div id="inner">

        <?php /* #################################### */
              /* ############# LEVEL 1 ############## */ ?>

        <div id="level-1" class="d-flex justify-content-center">

            <?php ######################## ONE ######################?>
            <?php if ($data['staff_functions']['numOfFuncs'] == 1) : ?>
              <div class="staff-column position-relative d-flex align-items-center"> 

                <div class="horizontal left single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>

                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column position-relative d-flex align-items-center order-1 invisible"> 

                <div class="horizontal right single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                    </div>
                  </div>
                </div>

              </div>
            <?php ######################## TWO ######################?>
            <?php elseif ($data['staff_functions']['numOfFuncs'] == 2) : ?>
              <div class="staff-column position-relative d-flex align-items-center">

                <div class="horizontal left single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column position-relative d-flex align-items-center order-1"> 

                <div class="horizontal right single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][1]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][1];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

            <?php ######################## THREE ######################?>
            <?php elseif ($data['staff_functions']['numOfFuncs'] == 3) : ?>
              <div class="staff-column filled">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>
                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="horizontal left">
                  <div class="connector"></div>
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][1]->name; ?></h6>
                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][1];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column position-relative d-flex align-items-center order-1"> 

                <div class="horizontal right single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][2]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][2];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div> 

            <?php ######################## FOUR ######################?>
            <?php elseif ($data['staff_functions']['numOfFuncs'] == 4) : ?>
              <div class="staff-column filled">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="horizontal left">
                  <div class="connector"></div>
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][1]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][1];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled order-1">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][2]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][2];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>
                
                <div class="horizontal right">
                  <div class="connector"></div>
                </div>
                

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][3]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][3];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

            <?php ######################## FIVE ######################?>
            <?php elseif ($data['staff_functions']['numOfFuncs'] == 5) : ?>
              <div class="staff-column position-relative d-flex align-items-center">

                <div class="horizontal left single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled">

              <div class="staff-function">
                <div class="card bg-pe-lightgreen" style="width: 18rem;">
                  <div class="card-body text-white text-center">
                    <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][1]->name; ?></h6>

                      <!-- Show Employee -->
                      <?php
                      $dataStaffFunction = $data['staff_functions']['staff_functions'][1];
                      require APPROOT . '/views/organs/templates/showEmployee.php';
                      ?>
                  </div>
                </div>
              </div>

              <div class="connector single"></div>

              <div class="staff-function">
                <div class="card bg-pe-lightgreen" style="width: 18rem;">
                  <div class="card-body text-white text-center">
                    <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][2]->name; ?></h6>

                      <!-- Show Employee -->
                      <?php
                      $dataStaffFunction = $data['staff_functions']['staff_functions'][2];
                      require APPROOT . '/views/organs/templates/showEmployee.php';
                      ?>
                  </div>
                </div>
              </div>

              </div>

              <div class="staff-column filled order-1">

              <div class="staff-function">
                <div class="card bg-pe-lightgreen" style="width: 18rem;">
                  <div class="card-body text-white text-center">
                    <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][3]->name; ?></h6>

                      <!-- Show Employee -->
                      <?php
                      $dataStaffFunction = $data['staff_functions']['staff_functions'][3];
                      require APPROOT . '/views/organs/templates/showEmployee.php';
                      ?>
                  </div>
                </div>
              </div>

              <div class="connector single"></div>

              <div class="staff-function">
                <div class="card bg-pe-lightgreen" style="width: 18rem;">
                  <div class="card-body text-white text-center">
                    <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][4]->name; ?></h6>

                      <!-- Show Employee -->
                      <?php
                      $dataStaffFunction = $data['staff_functions']['staff_functions'][4];
                      require APPROOT . '/views/organs/templates/showEmployee.php';
                      ?>
                  </div>
                </div>
              </div>

              </div>

              <div class="staff-column position-relative d-flex align-items-center order-1 invisible"> 

                <div class="horizontal right single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                    </div>
                  </div>
                </div>

              </div>

              

            <?php ######################## SIX ######################?>
            <?php elseif ($data['staff_functions']['numOfFuncs'] == 6) : ?>
              <div class="staff-column position-relative d-flex align-items-center">

                <div class="horizontal left single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][1]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][1];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="connector single"></div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][2]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][2];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled order-1">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][3]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][3];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="connector single"></div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][4]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][4];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column position-relative d-flex align-items-center order-1"> 

                <div class="horizontal right single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

            <?php ######################## SEVEN ######################?>
            <?php elseif ($data['staff_functions']['numOfFuncs'] == 7) : ?>
              <div class="staff-column filled">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                            $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                            require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>

                    </div>
                  </div>
                </div>

                <div class="horizontal left">
                  <div class="connector"></div>
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][1]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][1];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][2]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][2];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="horizontal left">
                  <div class="connector"></div>
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][3]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][3];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>

                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled order-1">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][4]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][4];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="connector single"></div>
                

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][5]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][5];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column position-relative d-flex align-items-center order-1"> 

                <div class="horizontal right single">
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][6]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][6];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>            
            
            <?php ######################## EIGHT ######################?>
            <?php elseif ($data['staff_functions']['numOfFuncs'] == 8) : ?>
              <div class="staff-column filled">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][0]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][0];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="horizontal left">
                  <div class="connector"></div>
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][1]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][1];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][2]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][2];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="horizontal left">
                  <div class="connector"></div>
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][3]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][3];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled order-1">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][4]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][4];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="horizontal right">
                  <div class="connector"></div>
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][5]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][5];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

              <div class="staff-column filled order-1">

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][6]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][6];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

                <div class="horizontal right">
                  <div class="connector"></div> 
                </div>

                <div class="staff-function">
                  <div class="card bg-pe-lightgreen" style="width: 18rem;">
                    <div class="card-body text-white text-center">
                      <h6 class="card-title"><?php echo $data['staff_functions']['staff_functions'][7]->name; ?></h6>

                        <!-- Show Employee -->
                        <?php
                        $dataStaffFunction = $data['staff_functions']['staff_functions'][7];
                        require APPROOT . '/views/organs/templates/showEmployee.php';
                        ?>
                    </div>
                  </div>
                </div>

              </div>

            <?php endif; ?>

          <?php foreach ($data['organs'] as $organ) : ?>

            <?php if ($organ->id != 1) {
                            continue;
                        } ?>

            <div id="lead-organ" class="card bg-pe-lightgreen" style="width: 18rem;">
              <div class="card-body text-white text-center">
                <h6 class="card-title"><?php echo $organ->name; ?></h6>

                  <!-- Show Employee -->
                  <?php
                  $dataStaffFunction = $organ;
                  require APPROOT . '/views/organs/templates/showEmployee.php';
                  ?>
                <p class="mt-2 mb-0"><?php echo $organ->description; ?></p>
              </div>
            </div>

          <?php endforeach; ?>

        </div>

        <?php /* #################################### */
              /* ############# LEVEL 2 ############## */ ?>

        <?php if (!empty($data['organs'][2]->is_active)) : ?>

          <div id="level-2" class="d-flex justify-content-center">

            <div id="privacy" class="card bg-warning" style="width: 18rem;">
                <div class="line"></div>
                <div class="card-body text-white text-center">
                  <h6><?php echo $data['organs'][2]->name; ?> (<?php echo $data['organs'][2]->abbreviation; ?>)</h6>
                  <p><?php echo $data['organs'][2]->description; ?></p>
                </div>
            </div>

          </div>  

        <?php endif; ?>

        <?php /* #################################### */
              /* ############# LEVEL 3 ############## */ ?>

        <div id="level-3" class="d-flex justify-content-center">

          <?php foreach ($data['organs'] as $index => $organ) : ?>

            <?php if ($organ->level_id != 3) {
                  continue;
              } ?>

            <div class="wrapper mx-2 mt-4">

              <div class="card bg-dark" style="width: 18rem;">
                <div class="line">
                  <?php if ($index == 0) : ?>
                    <div class="horizontal-line"></div>
                  <?php endif; ?>
                </div>
                <div class="card-body">
                  <h6 class="card-title text-center text-white"><?php echo $organ->name; ?></h6>

                  <?php if (!empty($organ->management_level)) : ?>

                    <button class="btn btn-white w-100" type="button" data-toggle="collapse" data-target="#org-<?php echo $organ->id; ?>-management-collapse" aria-expanded="false" aria-controls="organ-<?php echo $organ->id; ?>-collapse">
                      Führungsebene ansehen
                    </button>
                    </p>
                    <div class="collapse" id="org-<?php echo $organ->id; ?>-management-collapse">
                      <div class="card card-body">
                        <ul class="list-group">
    
                          <?php foreach ($organ->management_level as $user) : ?>
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $user->id; ?>" class="list-group-item list-group-item-action"><?php echo substr($user->firstname, 0, 1).'. '.$user->lastname; ?></a>
                          <?php endforeach; ?>
                        
                        </ul>
                      </div>
                    </div>

                  <?php endif; ?>

                  <?php if (!empty($organ->staff)) : ?>

                    <button class="btn btn-white w-100 mt-2" type="button" data-toggle="collapse" data-target="#org-<?php echo $organ->id; ?>-staff-collapse" aria-expanded="false" aria-controls="organ-<?php echo $organ->id; ?>-collapse">
                      Mitarbeiter ansehen
                    </button>
                    </p>
                    <div class="collapse" id="org-<?php echo $organ->id; ?>-staff-collapse">
                      <div class="card card-body">
                        <ul class="list-group">
    
                          <?php foreach ($organ->staff as $user) : ?>
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $user->id; ?>" class="list-group-item list-group-item-action"><?php echo substr($user->firstname, 0, 1).'. '.$user->lastname; ?></a>
                          <?php endforeach; ?>
                        
                        </ul>
                      </div>
                    </div>

                  <?php endif; ?>

                </div>
              </div>

              <?php if (!empty($organ->suborgans)) : ?>

                <?php foreach ($organ->suborgans as $suborgan) : ?>

                <div class="card bg-pe-darkgreen mt-4" style="width: 18rem;">
                  <div class="card-body">
                    <h6 class="card-title text-center text-white"><?php echo $suborgan->name; ?></h6>

                    <button class="btn btn-white w-100" type="button" data-toggle="collapse" data-target="#org<?php echo $organ->id; ?>-sub<?php echo $suborgan->id; ?>-collapse" aria-expanded="false" aria-controls="org<?php echo $organ->id; ?>-sub<?php echo $suborgan->id; ?>-collapse">
                    Mitarbeiter ansehen
                    </button>
                    </p>
                    <div class="collapse" id="org<?php echo $organ->id; ?>-sub<?php echo $suborgan->id; ?>-collapse">
                      <div class="card card-body">
                        <ul class="list-group">
    
                          <?php foreach ($suborgan->staff as $user) : ?>
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $user->id; ?>" class="list-group-item list-group-item-action"><?php echo substr($user->firstname, 0, 1).'. '.$user->lastname; ?></a>
                          <?php endforeach; ?>
                        
                        </ul>
                      </div>
                    </div>


                  </div>
                </div>

                <?php endforeach; ?>



              <?php endif; ?>

              
            </div>

            

          <?php endforeach; ?>

        </div>

      </div><!-- end of #inner -->

    </div><!-- end of #organigram -->

  </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
