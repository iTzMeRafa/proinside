<?php require APPROOT . '/views/inc/header.php'; ?>
<?php
$pageTitle = 'Ansehen'
?>

<?php require APPROOT . '/views/organs/templates/title.php'; ?>

<?php require APPROOT . '/views/organs/templates/navigation.php'; ?>

<?php displayFlash('organ_message'); ?>

<?php
    $organsSize = count($data['organs']);
    $levels = [1 => [], 2 => [], 3 => [], 4 => []];

    foreach ($data['organs'] as $organ) {
        if ($organ->level_id == 1){ array_push($levels[1], $organ); }
        if ($organ->level_id == 2){ array_push($levels[2], $organ); }
        if ($organ->level_id == 3){ array_push($levels[3], $organ); }
        if ($organ->level_id == 4){ array_push($levels[4], $organ); }
    }
?>

<?php for ($i = 1; $i <= 4; $i++): ?>

    <!-- Skip empty levels -->
    <?php if (empty($levels[$i])){ continue; } ?>

    <ul id="org<?= $i; ?>" style="display:none">
        <li>
            <?php
                switch ($i) {
                    case 1:
                        echo '<h6 class="text-white">Führungsebene</h6>';
                        break;
                    case 2:
                        echo '<h6 class="text-white">Vertriebsebene</h6>';
                        break;
                    case 3:
                    case 4:
                        echo '<h6 class="text-white">Sonstiges</h6>';
                }
            ?>

            <!-- Organs -->
            <ul>
                <?php foreach ($levels[$i] as $keyOrgan => $organ): ?>
                <?php
                    $primaryKeyOrgan = $i . $keyOrgan;
                    $managersOrgan = [];
                    foreach ($organ->management_level as $manager) {
                        array_push($managersOrgan, mergeFullName($manager->firstname, $manager->lastname));
                    }
                ?>

                    <li>
                        <h6 class="organTitle text-white"><?= $organ->name; ?> <br /> (<?= empty($organ->abbreviation) ? '-' : $organ->abbreviation; ?>)</h6>

                        <button type="button" id="showOrganManagersButton" data-managers="<?= implode(', ', $managersOrgan); ?>" class="btn btn-white  mt-2" data-toggle="modal" data-target="#showManagersModal">
                            Mitarbeiter ansehen (<?= count($managersOrgan); ?>)
                        </button>

                        <!-- Suborgans -->
                        <?php if (isset($organ->suborgans) && !empty($organ->suborgans)): ?>
                            <ul>
                                <?php foreach ($organ->suborgans as $keySubOrgan => $subOrgan): ?>

                                <?php
                                    $primaryKeySubOrgan = $keyOrgan . $keySubOrgan . $i;
                                    $managersSubOrgan = [];
                                    foreach ($subOrgan->staff as $manager) {
                                        array_push($managersSubOrgan, mergeFullName($manager->firstname, $manager->lastname));
                                    }
                                ?>
                                    <li>
                                        <h6 class="organTitle text-white"><?= $subOrgan->name; ?> <br />(<?= empty($subOrgan->abbreviation) ? '-' : $subOrgan->abbreviation; ?>)</h6>

                                        <button type="button" id="showSubOrganManagersButton" data-managers="<?= implode(', ', $managersSubOrgan); ?>" class="btn btn-white  mt-2" data-toggle="modal" data-target="#showManagersModal">
                                            Mitarbeiter ansehen (<?= count($managersSubOrgan); ?>)
                                        </button>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>

                    </li>

                <?php endforeach; ?>
            </ul>

        </li>
    </ul>

    <div id="chart<?= $i; ?>" class="orgChart"></div>

    <!-- Modal -->
    <div class="modal fade" id="showManagersModal" tabindex="-1" role="dialog" aria-labelledby="showManagersTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="showManagersTitle">Mitarbeiterliste</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="managersList"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                </div>
            </div>
        </div>
    </div>

<?php endfor; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
