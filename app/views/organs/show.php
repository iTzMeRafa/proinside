<?php require APPROOT . '/views/inc/header.php'; ?>
  <?php displayFlash('organ_message'); ?>
  <?php displayFlash('organ_permission'); ?>

<a href="javascript:history.go(-1)" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

  <?php if ($data['organ']->id != 2) : ?>

    <?php if ($data['privUser']->hasPrivilege('7') == true) : ?>
      <a href="<?php echo URLROOT; ?>/organs/edit/<?php echo $data['organ']->id; ?>" class="btn btn-dark float-right">Bearbeiten</a>
    <?php endif; ?>

    <div class="row">
      <div class="col-12">
        <h1 class="h2"><?php echo $data['organ']->name; ?> (<?php echo $data['organ']->abbreviation; ?>)</h1>
        <?php if ($data['organ']->id != 1) : ?>
          <p class="h3">Zweck:</p>
        <?php endif; ?>
        <p><?php echo $data['organ']->description; ?></p>
      </div>
      <div id="management_level" class="col-md-6 mb-3">
        <h2>Führungsebene</h2>

        <?php if (count($data['managers']) > 0) : ?>
          <ul class="list-group">
          <?php foreach ($data['managers'] as $manager) : ?>
              <a href="<?php echo URLROOT; ?>/users/show/<?php echo $manager->id; ?>" class="list-group-item list-group-item-action"><?php echo $manager->firstname .' '. $manager->lastname; ?></a>
          <?php endforeach; ?>
          </ul>
        <?php else : ?>
          <p>In diesem Organ sind noch keine Nutzer der Führungsebene zugeordnet.</p>
        <?php endif; ?>

      </div>

      <div id="staff" class="col-md-6 mb-3">
        <h2>Mitarbeiter</h2>
        <?php if ($data['staff']) : ?>
          <ul class="list-group">
          <?php foreach ($data['staff'] as $staff) : ?>
              <a href="<?php echo URLROOT; ?>/users/show/<?php echo $staff->id; ?>" class="list-group-item list-group-item-action"><?php echo $staff->firstname .' '. $staff->lastname; ?></a>
          <?php endforeach; ?>
          </ul>
        <?php else : ?>
          <p>In diesem Organ sind noch keine Nutzer der Mitarbeiterebene zugeordnet.</p>
        <?php endif; ?>
      </div>
        
      <?php if ($data['organ']->id != 1 && $data['organ']->id != 3) : ?>

        <div class="col-12">
          <h2>Suborgane <a href="<?php echo URLROOT; ?>/organs/addsub/<?php echo $data['organ']->id; ?>" class="btn btn-pe-lightgreen">Suborgane bearbeiten</a></h2>

            <div class="accordion w-100" id="accordionExample">
              <?php foreach ($data['suborgans'] as $key => $organ) : ?>
                <div class="card">
                  <div class="card-header" id="heading<?php echo $key; ?>">
                    <h2 class="mb-0">
                      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="collapseOne">
                          <?php echo $organ['name']; ?>
                      </button>
                    </h2>
                  </div>
                  <div id="collapse<?php echo $key; ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      <div class="row">

                        <div class="col-12 mb-3">
                          <h3>Mitarbeiter</h3>
                          <ul class="list-group">
                            <?php if (count($organ['staff']) > 0) : ?>
                              <?php foreach ($organ['staff'] as $staff) : ?>
                                <a href="<?php echo URLROOT; ?>/users/show/<?php echo $staff->id; ?>" class="list-group-item list-group-item-action"><?php echo $staff->firstname .' '. $staff->lastname; ?></a>
                              <?php endforeach; ?>
                            <?php else : ?>
                              <p>In diesem Organ sind momentan keine Nutzer der Mitarbeiterebene zugeordnet.</p>
                            <?php endif; ?>
                          </ul>
                        </div>

                      </div>
                      
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
        </div> <!-- end of col-12 -->

      <?php endif; ?>

    </div> <!-- end of row -->

    <?php if ($data['privUser']->hasPrivilege('8') == true &&
              $data['organ']->id != 1 &&
              $data['organ']->id != 3) : ?>
      <form class="" action="<?php echo URLROOT; ?>/organs/delete/<?php echo $data['organ']->id; ?>" method="post">
        <input type="submit" class="btn btn-danger mt-3" value="Organ löschen">
      </form>
    <?php endif; ?>

    <?php /* ################### ORGAN ID:3 - DEACTIVATE #################### */ ?>

    <?php if ($data['privUser']->hasPrivilege('8') == true && $data['organ']->id == 3 && !empty($data['organ']->is_active)) : ?>
      <button id="deactivatePrivacy" type="button" class="btn btn-danger">Deaktivieren</button>
    <?php endif; ?>

    <?php /* ################### ORGAN ID:3 - ACTIVATE #################### */ ?>

    <?php if ($data['privUser']->hasPrivilege('8') == true && $data['organ']->id == 3 && empty($data['organ']->is_active)) : ?>
      <button id="activatePrivacy" type="button" class="btn btn-pe-lightgreen">Aktivieren</button>
    <?php endif; ?>

  <?php else : ?>
    
    <div class="row">
      <div class="col-12">
      <h1 class="mb-3">Stabsfunktionen <a href="<?php echo URLROOT; ?>/organs/addsub/<?php echo $data['organ']->id; ?>" class="btn btn-pe-lightgreen">Stabsfunktionen bearbeiten</a></h1>

      <div class="row">

      <?php foreach ($data['suborgans'] as $key => $organ) : ?>

      
        <div class="col-md-6 mb-3">
          <div class="bg-light p-4">
            <h3><?php echo $organ['name']; ?></h3>
            <ul class="list-group">
              <?php if (count($organ['staff']) > 0) : ?>
                <?php foreach ($organ['staff'] as $staff) : ?>
                  <a href="<?php echo URLROOT; ?>/users/show/<?php echo $staff->id; ?>" class="list-group-item list-group-item-action"><?php echo $staff->firstname .' '. $staff->lastname; ?></a>
                <?php endforeach; ?>
              <?php else : ?>
                <p>In diesem Organ sind momentan keine Nutzer der Mitarbeiterebene zugeordnet.</p>
              <?php endif; ?>
            </ul>
          </div>
        </div>

      <?php endforeach; ?>

      </div>

      </div> <!-- end of col-12 -->

    </div> <!-- end of row -->

  <?php endif; ?>


<?php require APPROOT . '/views/inc/footer.php'; ?>
