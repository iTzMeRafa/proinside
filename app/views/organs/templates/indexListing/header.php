<div class="row mb-5">
    <div class="col-12">
        <h1 class="h1 btn btn-pe-lightgreen"><i class="fas fa-sitemap mr-2"></i>Organigramm</h1>
        <a href="<?php echo URLROOT; ?>/organs/organigram" class="h1 btn btn-pe-lightgreen"><i class="fas fa-eye mr-2"></i> Organigramm ansehen </a>
        <a href="<?php echo URLROOT; ?>/organs/add" class="h1 btn btn-pe-lightgreen"><i class="fas fa-pencil-alt mr-2"></i> Neues Organ erstellen </a>
    </div>
</div>