
<!-- Sortable Table -->
<div class="col-md-12">
    <table id="organsListing" class="table table-striped table-bordered" style="width: 100%">
        <thead>
            <tr style="background-color: #0B676E; color: white; border: none;">
                <th style="border: none;">ID</th>
                <th style="border: none;">Name</th>
                <th style="border: none;">Kürzel</th>
                <th style="border: none;">Ebene</th>
            </tr>
        </thead>
        <tbody>

        <?php foreach ($data['organs'] as $organ) : ?>
            <tr>
                <td><a href="<?php echo URLROOT; ?>/organs/show/<?= $organ->id; ?>"><?= $organ->id; ?></a></td>
                <td><a href="<?php echo URLROOT; ?>/organs/show/<?= $organ->id; ?>"><?= $organ->name; ?></a></td>
                <td><?= $organ->abbreviation; ?></td>
                <td><?= $organ->level_id; ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->
