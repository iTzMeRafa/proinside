<!-- Führung/Mitarbeiter Buttons -->
<?php if (!empty($dataStaffFunction->management_level)) : ?>

    <button class="btn btn-white w-100" type="button" data-toggle="collapse" data-target="#org-<?php echo $dataStaffFunction->id; ?>-management-collapse" aria-expanded="false" aria-controls="organ-<?php echo $organ->id; ?>-collapse">
        Führungsebene ansehen
    </button>
    </p>
    <div class="collapse" id="org-<?php echo $dataStaffFunction->id; ?>-management-collapse">
        <div class="card card-body">
            <ul class="list-group">

                <?php foreach ($dataStaffFunction->management_level as $user) : ?>
                    <a href="<?php echo URLROOT; ?>/users/show/<?php echo $user->id; ?>" class="list-group-item list-group-item-action"><?php echo substr($user->firstname, 0, 1).'. '.$user->lastname; ?></a>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>

<?php endif; ?>
<?php if (!empty($dataStaffFunction->staff)) : ?>

    <button class="btn btn-white w-100 mt-2" type="button" data-toggle="collapse" data-target="#org-<?php echo $dataStaffFunction->id; ?>-staff-collapse" aria-expanded="false" aria-controls="organ-<?php echo $dataStaffFunction->id; ?>-collapse">
        Mitarbeiter ansehen
    </button>
    </p>
    <div class="collapse" id="org-<?php echo $dataStaffFunction->id; ?>-staff-collapse">
        <div class="card card-body">
            <ul class="list-group">

                <?php foreach ($dataStaffFunction->staff as $user) : ?>
                    <a href="<?php echo URLROOT; ?>/users/show/<?php echo $user->id; ?>" class="list-group-item list-group-item-action"><?php echo substr($user->firstname, 0, 1).'. '.$user->lastname; ?></a>
                <?php endforeach; ?>

            </ul>
        </div>
    </div>

<?php endif; ?>
<!-- /Mitarbeiter/Führung Buttons -->