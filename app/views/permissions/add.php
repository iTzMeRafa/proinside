<?php require APPROOT . '/views/inc/header.php'; ?>
<section class="permissionsAdd">

    <!-- Flashes -->
    <div class="row">
        <div class="col-md-12">
            <?php displayFlash('no_permisssion'); ?>
        </div>
    </div>

    <!-- Headline -->
    <div class="row">
        <div class="col-md-12 mb-3">
            <h1>Neue Rolle erstellen:</h1>
        </div>
    </div>

    <!-- Form -->
    <div class="row">
        <form class="col-12" action="<?php echo URLROOT; ?>/permissions/add" method="post">

            <!-- Name, Description -->
            <div class="row">
                <div class="col-md-6 mb-3">
                    <p class="h5">Name der Rolle: <sup>*</sup></p>
                    <input name="name" type="text"
                           class="form-control <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['name']; ?>" placeholder="Name">
                    <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>
                </div>
                <div class="col-md-6 mb-3">
                    <p class="h5">Beschreibung:</p>
                    <textarea name="description" class="form-control" rows="3"
                              cols="80"><?php echo $data['description']; ?></textarea>
                </div>
            </div>

            <!-- Headline 2 -->
            <div class="row">
                <div class="col-12 my-2">
                    <p class="h2">Berechtigungen:</p>
                </div>
            </div>

            <!-- File Acess -->
            <div class="row">
                <div class="col-md-12 mb-5">
                    <p class="h4 my-3"><i class="fas fa-folder mr-2"></i>Ordner Zutrittsberechtigung</p>
                    <button type="button" id="open_folder_perm_modal" class="btn btn-pe-darkgreen"
                            data-toggle="modal" data-target="#folder_perm_modal">Zutrittsberechtigung individuell
                        festlegen
                    </button>
                    <input id="folderAccessPerms" name="folderAccessPerms" type="hidden"
                           value="<?php echo $data['folderAccessPerms']; ?>">
                    <input id="globalAccess" name="globalAccess" type="hidden"
                           value="<?php echo $data['globalAccessPerms']; ?>">
                    <ul id="accessList" class="list-group list-group-flush">

                    </ul>
                </div>
            </div>

            <!-- Processes, Tasks -->
            <div class="row">

                <!-- Processes -->
                <div class="col-md-6">
                    <p class="h4 my-3"><i class="fas fa-book mr-2"></i>Prozess-Matrizen</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="createProcessMatrixes" value="22" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['createProcessMatrixes']) ? 'checked' : ''; ?>>Prozess-Matrizen
                            erstellen
                        </li>
                        <li class="list-group-item">
                            <input name="approveProcessMatrixes" value="29" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['approveProcessMatrixes']) ? 'checked' : ''; ?>>Prozess-Matrizen
                            freigeben
                        </li>
                        <li class="list-group-item">
                            <input name="editProcessMatrixes" value="23" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['editProcessMatrixes']) ? 'checked' : ''; ?>>Prozess-Matrizen
                            bearbeiten
                        </li>
                        <li class="list-group-item">
                            <input name="deleteProcessMatrixes" value="24" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['deleteProcessMatrixes']) ? 'checked' : ''; ?>>Prozess-Matrizen
                            löschen
                        </li>
                    </ul>
                </div>

                <!-- Tasks -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-tasks mr-2"></i>Aufgaben</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessTaskSection" value="9" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessTaskSection']) ? 'checked' : ''; ?>>Aufgaben-Sektion
                            sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input name="addTasks" value="1" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['addTasks']) ? 'checked' : ''; ?>>Aufgaben erstellen
                        </li>
                        <li class="list-group-item">
                            <input name="editTasks" value="2" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['editTasks']) ? 'checked' : ''; ?>>Aufgaben bearbeiten
                        </li>
                        <li class="list-group-item">
                            <input name="deleteTasks" value="3" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['deleteTasks']) ? 'checked' : ''; ?>>Aufgaben löschen
                        </li>
                        <li class="list-group-item">
                            <input name="checkTasks" value="15" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['checkTasks']) ? 'checked' : ''; ?>>Aufgaben checken
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Organs, Permissions -->
            <div class="row">

                <!-- Organs -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-sitemap mr-2"></i>Organe</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessOrganSection" value="10" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessOrganSection']) ? 'checked' : ''; ?>>Organigram-Sektion
                            sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input name="addOrgans" value="6" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['addOrgans']) ? 'checked' : ''; ?>>Organe erstellen
                        </li>
                        <li class="list-group-item">
                            <input name="editOrgans" value="7" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['editOrgans']) ? 'checked' : ''; ?>>Organe bearbeiten
                        </li>
                        <li class="list-group-item">
                            <input name="deleteOrgans" value="8" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['deleteOrgans']) ? 'checked' : ''; ?>>Organe löschen
                        </li>
                    </ul>
                </div>

                <!-- Permissions -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-gavel mr-2"></i>Rechte</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessPermissionSection" value="14" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessPermissionSection']) ? 'checked' : ''; ?>>Rechte-Sektion
                            sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input name="assignRoles" value="25" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['assignRoles']) ? 'checked' : ''; ?>>Rechte zuteilen
                        </li>
                        <li class="list-group-item">
                            <input name="addRole" value="26" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['addRole']) ? 'checked' : ''; ?>>Rechte hinzufügen
                        </li>
                        <li class="list-group-item">
                            <input name="editRole" value="27" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['editRole']) ? 'checked' : ''; ?>>Rechte bearbeiten
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Folders, Business Models -->
            <div class="row">

                <!-- Folders -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-folder mr-2"></i>Dateimanager</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessDatamanager" value="13" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessDatamanager']) ? 'checked' : ''; ?>>Dateimanager
                            sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input name="createFolders" value="16" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['createFolders']) ? 'checked' : ''; ?>>Ordner
                            erstellen
                        </li>
                        <li class="list-group-item">
                            <input name="deleteFolders" value="17" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['deleteFolders']) ? 'checked' : ''; ?>>Ordner
                            löschen
                        </li>
                        <li class="list-group-item">
                            <input name="renameFolders" value="36" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['renameFolders']) ? 'checked' : ''; ?>>Ordner umbenennen
                        </li>
                        <li class="list-group-item">
                            <input name="uploadFiles" value="18" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['uploadFiles']) ? 'checked' : ''; ?>>Dateien
                            hochladen
                        </li>
                        <li class="list-group-item">
                            <input name="approveFiles" value="19" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['approveFiles']) ? 'checked' : ''; ?>>Dateien
                            freigeben
                        </li>
                        <li class="list-group-item">
                            <input name="deleteFiles" value="20" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['deleteFiles']) ? 'checked' : ''; ?>>Dateien
                            löschen
                        </li>
                        <li class="list-group-item">
                            <input name="renameFiles" value="28" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['renameFiles']) ? 'checked' : ''; ?>>Dateien
                            umbenennen
                        </li>
                    </ul>
                </div>

                <!-- Business Model -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-table mr-2"></i>Unternehmensmodell</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessDatamanager" value="30" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessBusinessModel']) ? 'checked' : ''; ?>>Unternehmensmodell
                            sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input name="createFolders" value="31" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['editBusinessModel']) ? 'checked' : ''; ?>>Unternehmensmodell
                            bearbeiten / erstellen
                        </li>
                        <li class="list-group-item">
                            <input name="deleteFolders" value="32" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['accessArchivedBusinessModel']) ? 'checked' : ''; ?>>Archivierte Unternehmensmodelle ansehen
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Users, Reports -->
            <div class="row">

                <!-- Users -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-users mr-2"></i>Mitarbeiter</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessUserSection" value="11" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessUserSection']) ? 'checked' : ''; ?>>Mitarbeiter-Sektion
                            sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input name="registerUsers" value="21" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['registerUsers']) ? 'checked' : ''; ?>>Mitarbeiter
                            registrieren
                        </li>
                    </ul>
                </div>

                <!-- Reports -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-eye mr-2"></i>Reports</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessReportSection" value="12" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessReportSection']) ? 'checked' : ''; ?>>Report-Sektion
                            sehen und betreten
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Methods, Qualifications -->
            <div class="row">

                <!-- Methods -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-thumbtack mr-2"></i>Maßnahmen</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input name="accessMethods" value="33" class="selectbox mr-3"
                                   type="checkbox" <?php echo ($data['accessMethods']) ? 'checked' : ''; ?>>Maßnahmen-Sektion
                            sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input name="editMethods" value="34" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['editMethods']) ? 'checked' : ''; ?>>Maßnahmen bearbeiten
                        </li>
                        <li class="list-group-item">
                            <input name="createMethods" value="35" class="selectbox ml-4 mr-3"
                                   type="checkbox" <?php echo ($data['createMethods']) ? 'checked' : ''; ?>>Maßnahmen erstellen
                        </li>
                    </ul>
                </div>

                <!-- Qualifications -->
                <div class="col-md-6 mb-5">
                    <p class="h4 my-3"><i class="fas fa-thumbtack mr-2"></i>Kompetenzen</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <input
                                name="accessQualifications"
                                value="37"
                                class="selectbox mr-3"
                                type="checkbox" <?php echo ($data['accessQualifications']) ? 'checked' : ''; ?>
                            />
                                Kompetenzen-Sektion sehen und betreten
                        </li>
                        <li class="list-group-item">
                            <input
                                name="editQualifications"
                                value="38"
                                class="selectbox ml-4 mr-3"
                                type="checkbox" <?php echo ($data['editQualifications']) ? 'checked' : ''; ?>
                            />
                                Kompetenzen bearbeiten
                        </li>
                        <li class="list-group-item">
                            <input
                                name="createQualifications"
                                value="39"
                                class="selectbox ml-4 mr-3"
                                type="checkbox" <?php echo ($data['createQualifications']) ? 'checked' : ''; ?>
                            />
                                Kompetenzen erstellen
                        </li>
                        <li class="list-group-item">
                            <input
                                    name="assignQualifications"
                                    value="40"
                                    class="selectbox ml-4 mr-3"
                                    type="checkbox" <?php echo ($data['assignQualifications']) ? 'checked' : ''; ?>
                            />
                            Kompetenzen zuweisen
                        </li>
                        <li class="list-group-item">
                            <input
                                    name="deleteQualifications"
                                    value="41"
                                    class="selectbox ml-4 mr-3"
                                    type="checkbox" <?php echo ($data['deleteQualifications']) ? 'checked' : ''; ?>
                            />
                            Kompetenzen löschen
                        </li>
                        <li class="list-group-item">
                            <input
                                    name="renameQualifications"
                                    value="42"
                                    class="selectbox ml-4 mr-3"
                                    type="checkbox" <?php echo ($data['renameQualifications']) ? 'checked' : ''; ?>
                            />
                            Kompetenzen umbenennen
                        </li>
                    </ul>
                </div>

            </div>

            <!-- Create Button -->
            <div class="row">
                <div class="col-md-12">
                    <input id="submit" type="submit" class="btn btn-pe-darkgreen float-right mt-5" value="Erstellen">
                </div>
            </div>

            </div>
        </form>
    </div>

</section>

<!-- Modal -->
<div class="modal fade" id="folder_perm_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Zutrittsberechtigungen für Ordner</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="folderList">
                    <div class="form-check">
                        <input id="global-check" class="form-check-input" type="checkbox" value="global">
                        <label class="form-check-label">Alle Ordner</label>
                    </div>
                    <?php foreach ($data['folders'] as $folder) : ?>
                        <div class="form-check">
                            <input id="folder<?php echo $folder->id; ?>" class="form-check-input" type="checkbox"
                                   value="<?php echo $folder->id; ?>" name="<?php echo $folder->folder_name; ?>"
                                   data-name="<?php echo $folder->folder_name; ?>">
                            <label class="form-check-label"><?php echo $folder->folder_name; ?></label>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-pe-darkgreen">Speichern</button>
            </div>
        </div>
    </div>
</div><!-- end of modal -->

<?php require APPROOT . '/views/inc/footer.php'; ?>
