<?php require APPROOT . '/views/inc/header.php'; ?>

  <div class="row mb-3">
    <div class="col-md-12">
      <!-- Display flash message -->
      <?php displayFlash('no_permisssion'); ?>
      <?php displayFlash('assign_message'); ?>
    </div>
    <div class="col-md-6">
      <h1 class="btn btn-pe-lightgreen">Rechtevergabe</h1>
    </div>
    <div class="col-md-6">
        <a href="javascript:history.go(-1)" class="btn btn-primary float-right">
        <i class="fa fa-chevron-left"></i> Zurück
      </a>
    </div>
  </div><!-- end of row -->
  
  <form action="<?php echo URLROOT; ?>/permissions/assign" method="post" class="row">
    <div class="col-12">
      <h2 class="h4 d-block">Nutzer</h2>
    </div>

    <?php foreach ($data['users'] as $key => $user) : ?>
        <div class="col-md-3 border">
          <p><?php echo $user->firstname .' '. $user->lastname; ?></p>
        </div>

        <div class="col-md-2">
          <button type="button" class="open_perm_roles_modal btn btn-pe-darkgreen" data-toggle="modal" data-target="#perm_roles_modal" data-id="<?php echo $user->id; ?>">Rechte zuordnen</button>
          <input class="hidden" name="user-<?php echo $user->id; ?>" type="hidden" value="<?php echo $user->role_ids; ?>" data-role_names="<?php echo (!empty($user->role_names)) ? $user->role_names : ''; ?>">
        </div>

        <div class="rolesUI col-md-7 border">
          <p>
            <?php if (!empty($user->roles)) : ?>
              <?php echo $user->role_names; ?>
            <?php endif; ?>
          </p>
        </div>
    <?php endforeach; ?>

    <input name="submit" class="btn btn-pe-darkgreen mt-2 float-right" type="submit" value="Speichern">

  </form><!-- end of row -->

  

  <!-- Button trigger modal -->
  

  <!-- Modal -->
  <div class="modal fade" id="perm_roles_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalScrollableTitle">Rechtsrollen</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="listOfRoles">
            <?php foreach ($data['roles'] as $role) : ?>
              <div class="form-check">
                <input id="role<?php echo $role->role_id; ?>" class="form-check-input" type="checkbox" value="<?php echo $role->role_id; ?>" name="<?php echo $role->role_id; ?>" data-name="<?php echo $role->role_name; ?>">
                <label class="form-check-label"><?php echo $role->role_name; ?></label>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
          <button type="button" class="saveChanges btn btn-pe-darkgreen">Aktualisieren</button>
        </div>
      </div>
    </div>
  </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
