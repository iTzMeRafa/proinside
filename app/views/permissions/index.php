<?php require APPROOT . '/views/inc/header.php'; ?>
  <div class="row mb-3">
    <div class="col-md-12">
      <!-- Display flash message -->
      <?php displayFlash('no_permisssion'); ?>
      <?php displayFlash('perm_message'); ?>
    </div>
    <div class="col-md-6">
      <h1 class="btn btn-pe-lightgreen"><i class="fas fa-gavel mr-2"></i>Rechte</h1>
      <p>Die Rechtevergabe funktioniert über Rollen, welche unterschiedliche Berechtigungen haben und flexibel erstellt werden können. Nutzern werden mit den gewünschten Rollen verknüpft und erhalten dadurch die der Rolle entsprechenden Berechtigungen</p>
    </div>
    <div class="col-md-6">
      <a href="<?php echo URLROOT; ?>/permissions/add" class="btn btn-pe-lightgreen float-right">
        <i class="fas fa-pencil-alt"></i> Neue Rolle erstellen
      </a>
      <a href="<?php echo URLROOT; ?>/permissions/assign" class="btn btn-pe-lightgreen float-right">
        <i class="fas fa-users"></i> Rollen zuordnen
      </a>
    </div>
  </div><!-- end of row -->

  <div class="row">
  
    <?php foreach ($data['roles'] as $key => $role) : ?>
      <div class="col-md-8 bg-light mb-2">
        <h4 class="my-1"><?php echo $role['name']; ?></h2>
      </div>
      <div class="col-md-4 mb-2">
        <a href="<?php echo URLROOT; ?>/permissions/show/<?php echo $key; ?>" class="btn 
        btn-pe-darkgreen ml-auto w-100">Berechtigungen ansehen</a>
      </div>
    <?php endforeach; ?>

  </div><!-- end of row -->

<?php require APPROOT . '/views/inc/footer.php'; ?>
