<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="row mb-3">
    <div class="col-md-12">
        <!-- Display flash message -->
        <?php displayFlash('no_permisssion'); ?>
        <?php displayFlash('perm_message'); ?>
    </div>

    <div class="col-12">
        <a href="javascript:history.go(-1)" class="btn btn-pe-lightgreen float-right">
            <i class="fas fa-chevron-left mr-2"></i> Zurück
        </a>

        <?php if ($data['id'] != 1 && $data['id'] != 2) : ?>
            <a href="<?php echo URLROOT; ?>/permissions/edit/<?php echo $data['id']; ?>"
               class="btn btn-pe-lightgreen float-right">
                <i class="fas fa-pencil-alt mr-2"></i> Rolle bearbeiten
            </a>
        <?php endif; ?>

    </div>

    <div class="col-md-12 mb-3">
        <h1>Rolle: <?php echo $data['name']; ?></h1>
    </div>

    <div class="col-12">
        <div class="row">

            <div class="col-md-6 mb-3">
                <p class="h5">Beschreibung:</p>
                <p name="description"><?php echo $data['description']; ?></p>
            </div>

            <div class="col-12 my-2">
                <p class="h2">Berechtigungen:</p>
            </div>

            <div class="col-md-12 mb-5">
                <div class="col-md-6">
                    <p class="h4 my-3"><i class="fas fa-copy mr-2"></i>Dokumente: Ordnerzugangsrechte</p>

                    <?php if ($data['folderAccessPerms'] == 'global') : ?>
                        <div class="alert alert-success">
                            Alle Ordner
                        </div>
                    <?php elseif (!empty($data['folderAccessPerms']) && $data['folderAccessPerms'] != 'global') : ?>
                        <ul class="list-group list-group-flush">

                            <?php foreach ($data['folderAccessPerms'] as $folder) : ?>
                                <li class="list-group-item">
                                    <?php echo $folder->folder_name; ?><br>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php else : ?>
                        <p><i class="fas fa-times mr-2 text-danger"></i> Bisher wurden keine Ordnerzugangsrechte
                            vergeben.</p>
                    <?php endif; ?>
                    </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-book mr-2"></i>Prozess-Matrizen</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?php if ($data['createProcessMatrixes']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Prozess-Matrizen erstellen
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['approveProcessMatrixes']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Prozess-Matrizen freigeben
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['editProcessMatrixes']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Prozess-Matrizen bearbeiten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['deleteProcessMatrixes']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Prozess-Matrizen löschen
                    </li>
                </ul>
            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-tasks mr-2"></i>Aufgaben</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?php if ($data['accessTaskSection']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Aufgaben-Sektion sehen und betreten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['addTasks']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Aufgaben erstellen
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['editTasks']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Aufgaben bearbeiten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['deleteTasks']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Aufgaben löschen
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['checkTasks']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Aufgaben checken
                    </li>
                </ul>
            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-sitemap mr-2"></i>Organe</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?php if ($data['accessOrganSection']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Organigram-Sektion sehen und betreten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['addOrgans']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Organe erstellen
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['editOrgans']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Organe bearbeiten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['deleteOrgans']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Organe löschen
                    </li>
                </ul>
            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-gavel mr-2"></i>Rechte</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?php if ($data['accessPermissionSection']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Rechte-Sektion sehen und betreten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['assignRoles']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Rechte zuteilen
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['addRole']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Rechte hinzufügen
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['editRole']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Rechte bearbeiten
                    </li>
                </ul>
            </div>

            <div class="col-12">
                <div class="row">

                    <div class="col-md-6 mb-5">
                        <p class="h4 my-3"><i class="fas fa-folder mr-2"></i>Dateimanager</p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <?php if ($data['accessDatamanager']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Dateimanager sehen und betreten
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['createFolders']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Ordner erstellen
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['deleteFolders']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Ordner löschen
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['renameFolders']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Ordner umbenennen
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['uploadFiles']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Dateien hochladen
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['approveFiles']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Dateien freigeben
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['deleteFiles']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Dateien löschen
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['renameFiles']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Dateien umbenennen
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-6 mb-5">
                        <p class="h4 my-3"><i class="fas fa-table mr-2"></i>Unternehmensmodell</p>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">
                                <?php if ($data['accessBusinessModel']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Unternehmensmodell sehen und betreten
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['editBusinessModel']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Unternehmensmodell bearbeiten / erstellen
                            </li>
                            <li class="list-group-item">
                                <?php if ($data['accessArchivedBusinessModel']) : ?>
                                    <i class="fas fa-check mr-2 color-darkgreen"></i>
                                <?php else : ?>
                                    <i class="fas fa-times mr-2 text-danger"></i>
                                <?php endif; ?>
                                Archivierte Unternehmensmodelle ansehen
                            </li>
                        </ul>
                    </div>



                </div>

            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-users mr-2"></i>Mitarbeiter</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?php if ($data['accessUserSection']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Mitarbeiter-Sektion sehen und betreten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['registerUsers']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Mitarbeiter registrieren
                    </li>
                </ul>
            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-eye mr-2"></i>Reports</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?php if ($data['accessReportSection']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Report-Sektion sehen und betreten
                    </li>
                </ul>
            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-thumbtack mr-2"></i>Maßnahmen</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?=
                            $data['accessMethods']
                                ? '<i class="fas fa-check mr-2 color-darkgreen"></i>'
                                : '<i class="fas fa-times mr-2 text-danger"></i>';
                        ?>
                        Maßnahme-Sektion sehen und betreten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['editMethods']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Maßnahme bearbeiten
                    </li>
                    <li class="list-group-item">
                        <?php if ($data['createMethods']) : ?>
                            <i class="fas fa-check mr-2 color-darkgreen"></i>
                        <?php else : ?>
                            <i class="fas fa-times mr-2 text-danger"></i>
                        <?php endif; ?>
                        Maßnahme erstellen
                    </li>
                </ul>
            </div>

            <div class="col-md-6 mb-5">
                <p class="h4 my-3"><i class="fas fa-ribbon mr-2"></i>Kompetenzen</p>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <?=
                        $data['accessQualifications']
                            ? '<i class="fas fa-check mr-2 color-darkgreen"></i>'
                            : '<i class="fas fa-times mr-2 text-danger"></i>';
                        ?>
                        Kompetenzen-Sekion sehen und betreten
                    </li>
                    <li class="list-group-item">
                        <?=
                        $data['editQualifications']
                            ? '<i class="fas fa-check mr-2 color-darkgreen"></i>'
                            : '<i class="fas fa-times mr-2 text-danger"></i>';
                        ?>
                        Kompetenzen bearbeiten
                    </li>
                    <li class="list-group-item">
                        <?=
                        $data['createQualifications']
                            ? '<i class="fas fa-check mr-2 color-darkgreen"></i>'
                            : '<i class="fas fa-times mr-2 text-danger"></i>';
                        ?>
                        Kompetenzen erstellen
                    </li>
                    <li class="list-group-item">
                        <?=
                        $data['assignQualifications']
                            ? '<i class="fas fa-check mr-2 color-darkgreen"></i>'
                            : '<i class="fas fa-times mr-2 text-danger"></i>';
                        ?>
                        Kompetenzen zuweisen
                    </li>
                    <li class="list-group-item">
                        <?=
                        $data['deleteQualifications']
                            ? '<i class="fas fa-check mr-2 color-darkgreen"></i>'
                            : '<i class="fas fa-times mr-2 text-danger"></i>';
                        ?>
                        Kompetenzen löschen
                    </li>
                    <li class="list-group-item">
                        <?=
                        $data['renameQualifications']
                            ? '<i class="fas fa-check mr-2 color-darkgreen"></i>'
                            : '<i class="fas fa-times mr-2 text-danger"></i>';
                        ?>
                        Kompetenzen umbenennen
                    </li>
                </ul>
            </div>

        </div>
    </div>

</div><!-- end of row -->

<!-- Modal -->
<div class="modal fade" id="folder_perm_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Rechtsrollen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="folderList">
                    <?php foreach ($data['folders'] as $folder) : ?>
                        <div class="form-check">
                            <input id="folder<?php echo $folder->id; ?>" class="form-check-input" type="checkbox"
                                   value="<?php echo $folder->id; ?>" name="<?php echo $folder->folder_name; ?>">
                            <label class="form-check-label"><?php echo $folder->folder_name; ?></label>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-pe-darkgreen">Aktualisieren</button>
            </div>
        </div>
    </div>
</div><!-- end of modal -->

<?php require APPROOT . '/views/inc/footer.php'; ?>
