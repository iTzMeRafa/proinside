<?php require APPROOT . '/views/inc/header.php'; ?>
  <a href="<?php echo URLROOT; ?>/posts" class="btn btn-light"><i class="fa fa-backward"></i> Back</a>
  <br>
  <h1><?php echo $data['title']; ?></h1>
  <div class="bg-secondary text-white p-2 mb-3">
    Written by <?php echo $data['user_name']; ?> on <?php echo $data['created_at']; ?>
  </div>
  <p><?php echo $data['body']; ?></p>

  <?php if ($data['user_id'] == $_SESSION['user_id']) : ?>
    <hr>
    <a href="<?php echo URLROOT; ?>/posts/edit/<?php echo $data['id']; ?>" class="btn btn-dark">Bearbeiten</a>

    <form class="pull-right" action="<?php echo URLROOT; ?>/posts/delete/<?php echo $data['id']; ?>" method="post">
      <input type="submit" class="btn btn-danger" value="Löschen">
    </form>
  <?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
