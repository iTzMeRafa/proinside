<?php require APPROOT . '/views/inc/header.php'; ?>
<a href="javascript:history.go(-1)" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

  <div class="row">
    <div class="col-12">
      <h1 class="h4">Neuen Prozess erstellen</h1>
      <p>Füllen Sie die nachfolgenden Felder aus, um eine neue Prozess-Matrix zu erstellen</p>
    </div>

    <?php if (isset($data['error'])) : ?>
  
      <div class="alert alert-danger alert-dismissible fade show col-12" role="alert">
        Bitte stellen Sie sicher, dass der zu erstellende Prozess einen Namen sowie einen Zielordner hat.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

    <?php endif; ?>

    <div class="col-12">
      <form id="process_form" action="<?php echo URLROOT; ?>/processes/add" method="post">
      
      <div class="row mb-3">

        <div class="col-md-6 mb-3">
          <h2 class="h6">Prozessname:</h2>
          <input name="process_name" type="text" class="form-control" placeholder="Prozessname">
        </div>
        <div class="col-md-6 mb-3">
          <h2 class="h6">Zielordner:</h2>
          <select name="target_folder" class="form-control">
            <option value="">Zielordner auswählen</option>
            <?php foreach ($data['folders'] as $folder) : ?>
              <option value="<?php echo $folder->id; ?>" <?php echo ($data['target_id'] == $folder->id) ? 'selected' : ''; ?>><?php echo $folder->folder_name; ?></option>

              <?php if (isset($folder->sub_directories)) : ?>
              <?php foreach ($folder->sub_directories as $subFolder) : ?>
                <option value="<?php echo $subFolder->id; ?>" <?php echo ($data['target_id'] == $subFolder->id) ? 'selected' : ''; ?>><?php echo $subFolder->folder_name; ?></option>
              <?php endforeach; ?>
              <?php endif; ?>
              
            <?php endforeach; ?>
          </select>
        </div>

      </div>

      <p class="h4 mb-4">Prozessaufgaben</p>

      <div id="matrix" class="d-flex mb-3">

        <div id="remove-area" class="mr-3">

          <?php if (!empty($data['tasks'])) : ?>

            <?php foreach ($data['tasks'] as $key => $task) : ?>

              <div class="remove-button d-flex align-items-center px-3 mb-3" data-task="<?php echo $task['number']; ?>">
                <i class="fas fa-times"></i>
              </div>

            <?php endforeach; ?>

          <?php else : ?>

            <div class="remove-button d-flex align-items-center px-3 mb-3" data-task="1">
              <i class="fas fa-times"></i>
            </div>

          <?php endif; ?>

            
        </div>

        <div class="scrolling-wrapper">
          <!-- Legend -->
          <div id="column_info" class="d-flex mb-3">
            <div class="legend art task_number"><p class="h6 mb-0">Nr.</p></div>
            <div class="legend art input"><p class="h6 mb-0">Input</p></div>
            <div class="legend art input_source"><p class="h6 mb-0">Inputquelle</p></div>
            <div class="legend art category"><p class="h6 mb-0">Kategorie</p></div>
            <div class="legend art task_name"><p class="h6 mb-0">Aufgabe</p></div>
            <div class="legend art output"><p class="h6 mb-0">Output</p></div>
            <div class="legend art output_target"><p class="h6 mb-0">Outputziel</p></div>
            <div class="legend art risk"><p class="h6 mb-0">Risiko</p></div>
            <div class="legend art status"><p class="h6 mb-0">Status</p></div>
            <div class="legend art roles" data-user-count="<?php echo $data['user_count']; ?>">
              <div class="d-flex">
                <?php foreach ($data['organs'] as $organ) : ?>
                  <div class="organ">
                      
                      <button type="button" class="btn btn-pe-darkgreen w-100" data-toggle="tooltip" data-placement="top" title="<?php echo $organ['name']; ?>">
                        <i class="organ-name fas fa-sitemap"></i>
                      </button>

                      <p class="mb-0 d-none"><?php echo $organ['name']; ?></p>
                    <div class="users d-flex">
                      <?php foreach ($organ['users'] as $user): ?>
                        <button type="button" class="btn btn-pe-darkgreen user px-2 mb-0" data-toggle="tooltip" data-placement="top" title="<?php echo $user->firstname.' '.$user->lastname; ?>">
                          <?php echo $user->abbreviation; ?>
                        </button>
                        
                        <input name="legend-<?php echo $organ['organ_id']; ?>_<?php echo $user->id; ?>" type="hidden" value="<?php echo $organ['organ_id']; ?>">
                      <?php endforeach; ?>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
            <div class="legend art iso_value"><p class="h6 mb-0">ISO-Bezug</p></div>
            <div class="legend art check_intervals"><p class="h6 mb-0">Wiederkehrende Check-Intervalle</p></div>
            <div class="legend art tags"><p class="h6 mb-0">Schlagwörter</p></div>
          </div>

          <?php if (!empty($data['tasks'])) : ?>

            <?php foreach ($data['tasks'] as $key => $task) : ?>

              <div id="task-<?php echo $task['number']; ?>" class="set d-flex mb-3">

                <div class="art task_number">
                  <p class="mb-0 pl-3">A<?php echo $task['number']; ?></p>
                  <input name="<?php echo $task['number']; ?>-number" type="hidden" value="<?php echo $task['number']; ?>">
                </div>
                <div class="art input">
                  <input name="<?php echo $task['number']; ?>-input" type="text" class="form-control" value="<?php echo $task['input']; ?>" placeholder="Input">
                </div>

                <div class="art input_source">
                  <input name="<?php echo $task['number']; ?>-input_source" type="text" class="form-control" value="<?php echo $task['input_source']; ?>" placeholder="Inputquelle">
                </div>

                <div class="art category">
                  <select name="<?php echo $task['number']; ?>-category" class="form-control">
                    <option value="unausgewählt">Kategorie auswählen</option>
                    <option <?php echo ($task['category'] == 'Auftragswelt') ? 'selected' : ''; ?>>Auftragswelt</option>
                    <option <?php echo ($task['category'] == 'Strukturwelt') ? 'selected' : ''; ?>>Strukturwelt</option>
                    <option <?php echo ($task['category'] == 'Zielwelt') ? 'selected' : ''; ?>>Zielwelt</option>
                    <option <?php echo ($task['category'] == 'Rechtswelt') ? 'selected' : ''; ?>>Rechtswelt</option>
                    <option <?php echo ($task['category'] == 'Maßnahmenswelt') ? 'selected' : ''; ?>>Maßnahmenswelt</option>
                  </select>
                </div>

                <div class="art task_name">
                  <input name="<?php echo $task['number']; ?>-name" type="text" class="form-control" value="<?php echo $task['name']; ?>" placeholder="Name">
                </div>

                <div class="art output">
                  <input name="<?php echo $task['number']; ?>-output" type="text" class="form-control" value="<?php echo $task['output']; ?>" placeholder="Output">
                </div>

                <div class="art output_target">
                  <input name="<?php echo $task['number']; ?>-output_target" type="text" class="form-control" value="<?php echo $task['output_target']; ?>" placeholder="Outputziel">
                </div>

                <div class="art risk">
                  <select name="<?php echo $task['number']; ?>-risk" class="form-control">
                    <option value="unausgewählt">Risiko auswählen</option>
                    <option <?php echo ($task['risk'] == 'Keins') ? 'selected' : ''; ?>>Keins</option>
                    <option <?php echo ($task['risk'] == 'Gering') ? 'selected' : ''; ?>>Gering</option>
                    <option <?php echo ($task['risk'] == 'Mittel') ? 'selected' : ''; ?>>Mittel</option>
                    <option <?php echo ($task['risk'] == 'Hoch') ? 'selected' : ''; ?>>Hoch</option>
                  </select>
                </div>

                <div class="art status">
                  <select name="<?php echo $task['number']; ?>-status" class="form-control">
                    <option value="unausgewählt">Status auswählen</option>
                    <option <?php echo ($task['status'] == 'Plan') ? 'selected' : ''; ?>>Plan</option>
                    <option <?php echo ($task['status'] == 'Do') ? 'selected' : ''; ?>>Do</option>
                    <option <?php echo ($task['status'] == 'Check') ? 'selected' : ''; ?>>Check</option>
                    <option <?php echo ($task['status'] == 'Act') ? 'selected' : ''; ?>>Act</option>
                  </select>
                </div>

                <div class="art roles">
                  <?php for ($x = 1; $x <= $data['user_count'] ; $x++) :?>
                      <div class="select-user d-flex flex-column justify-content-center">
                        <select name="<?php echo $task['number']; ?>-user-select-<?php echo $x ?>" class="form-control">
                          <option value="empty"></option>
                          <option <?php echo ($task['user-select-'.$x] == 'responsible') ? 'selected' : ''; ?> value="responsible">V</option>
                          <option <?php echo ($task['user-select-'.$x] == 'concerned') ? 'selected' : ''; ?> value="concerned">Z</option>
                          <option <?php echo ($task['user-select-'.$x] == 'contributing') ? 'selected' : ''; ?> value="contributing">M</option>
                          <option <?php echo ($task['user-select-'.$x] == 'information') ? 'selected' : ''; ?> value="information">I</option>
                        </select>
                      </div>
                  <?php endfor; ?>
                </div> 

                <div class="art iso_value">
                  <input name="<?php echo $task['number']; ?>-iso_value" type="text" class="form-control" value="<?php echo $task['iso_value']; ?>" placeholder="ISO-Bezug">
                </div>

                

              </div>

            <?php endforeach; ?>

          <?php else : ?>

            <div id="task-1" class="set d-flex mb-3">

              <div class="art task_number">
                <p class="mb-0 pl-3">A1</p>
                <input name="1-number" type="hidden" value="1">
              </div>
              <div class="art input">
                <input name="1-input" type="text" class="form-control" value="" placeholder="Input">
              </div>

              <div class="art input_source">
                <input name="1-input_source" type="text" class="form-control" value="" placeholder="Inputquelle">
              </div>

              <div class="art category">
                <select name="1-category" class="form-control">
                  <option value="unausgewählt" selected>Kategorie auswählen</option>
                  <option>Auftragswelt</option>
                  <option>Strukturwelt</option>
                  <option>Zielwelt</option>
                  <option>Rechtswelt</option>
                  <option>Maßnahmenswelt</option>
                </select>
              </div>

              <div class="art task_name">
                <input name="1-name" type="text" class="form-control" value="" placeholder="Name">
              </div>

              <div class="art output">
                <input name="1-output" type="text" class="form-control" value="" placeholder="Output">
              </div>

              <div class="art output_target">
                <input name="1-output_target" type="text" class="form-control" value="" placeholder="Outputziel">
              </div>

              <div class="art risk">
                <select name="1-risk" class="form-control">
                  <option value="unausgewählt">Risiko auswählen</option>
                  <option>Keins</option>
                  <option>Gering</option>
                  <option>Mittel</option>
                  <option>Hoch</option>
                </select>
              </div>

              <div class="art status">
                <select name="1-status" class="form-control">
                  <option value="unausgewählt">Status auswählen</option>
                  <option>Plan</option>
                  <option>Do</option>
                  <option>Check</option>
                  <option>Act</option>
                </select>
              </div>

              <div class="art roles">
                <?php for ($x = 0; $x < $data['user_count'] ; $x++) :?>
                    <div class="select-user d-flex flex-column justify-content-center">
                      <select name="1-user-select-<?= $x+1 ?>" class="form-control">
                        <option value="empty" selected></option>
                        <option value="responsible">V</option>
                        <option value="concerned">Z</option>
                        <option value="contributing">M</option>
                        <option value="information">I</option>
                      </select>
                    </div>
                <?php endfor; ?>
              </div> 

              <div class="art iso_value">
                <input name="1-iso_value" type="text" class="form-control" value="" placeholder="ISO-Bezug">
              </div>

              <div class="art check_intervals d-flex">
                <p class="1-check mb-0 ml-2"></p>
                <button type="button" class="open_checks btn btn-pe-darkgreen d-block ml-auto" data-task="1" data-toggle="modal" data-target="#check_intervals_modal">
                <i class="fas fa-edit"></i></button>
                <input name="1-ref-date" class="ref-date" type="hidden">
                <input name="1-int-count" class="int-count" type="hidden">
                <input name="1-int-format" class="int-format" type="hidden">
                <input name="1-remind-before" class="remind-before" type="hidden">
                <input name="1-remind-after" class="remind-after" type="hidden">
                <input name="1-remind-at" class="remind-at" type="hidden">
              </div>
              
              <div class="art tags d-flex">
                <button type="button" class="tags-tooltip btn btn-light w-100" data-toggle="tooltip" data-placement="top" title="Keine Tags vergeben">
                  Keine Tags hinzugefügt
                </button>
                <button type="button" class="open_tags btn btn-pe-darkgreen d-block ml-auto" data-task="1" data-toggle="modal" data-target="#tags_modal">
                <i class="fas fa-edit"></i></button>
                <input name="1-tags" class="tags-string" type="hidden">
              </div>

            </div>

          <?php endif; ?>





          
        </div><!-- end of #scrolling-wrapper -->

      </div><!-- end of #matrix -->
        
        <!--<button id="addTask" type="button" class="btn btn-pe-lightgreen">Aufgabe zum Prozess hinzufügen</button> -->
        
        
        <div class="row">
          <div class="col-md-12 mt-2 d-flex">
            <p class="border bg-pe-lightgreen text-white py-2 px-3 mb-0 mr-2">Neue Prozessaufgabe</p>
            <select id="selectTarget" class="form-control w-25 mr-2">
              <option value="beginning">am Tabellenanfang</option>
              <option value="task-1" selected>nach A1</option>
            </select>
            <button id="addAfter" type="button" class="btn btn-pe-lightgreen">einfügen</button>
          </div>
        </div>
        
        <input id="submit" type="submit" class="btn btn-pe-darkgreen float-right" value="Erstellen">
      </form>
    </div>

    <!-- Modal: Tags -->
    <div id="tags_modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-mg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Schlagwörter</h5>
          </div>
          <div class="modal-body">
            <p>Schlagwörter per Komma einloggen.</p>
            <div class="tags-input w-100" data-name="tags-input">
              <input type="text" class="mainInput">
              <input name="tags" type="hidden" class="hiddenInput" value="">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="close-modal btn btn-secondary" data-dismiss="modal">Schließen</button>
            <button type="button" class="saveChanges btn btn-pe-darkgreen">Änderungen speichern</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal: Check Intervals -->
    <div id="check_intervals_modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Check-Intervalle & Erinnerungsfunktion</h5>
          </div>
          <div class="modal-body">
            <div class="row">
              
              <div class="col-md-6">
                <p class="h5 mb-2 ">Check-Intervalle:</p>
                <input id="activateIntervals" name="activateIntervals" class="mr-2" type="checkbox" value="none">
                <label for="activateIntervals" class="form-check-label mb-3">Check-Intervalle aktivieren</label>

                <fieldset id="intervalSet" disabled>
                  <p class="h6 mb-2">Referenzdatum:</p>
                  <div class="form-row">
                    <div class="col-4">
                      <label for="reference_day">Tag</label>
                      <input id="reference_day" type="number" class="form-control " min="1" max="31" value="">
                    </div>
                    <div class="col-4">
                      <label for="reference_month">Monat</label>
                      <input id="reference_month" type="number" class="form-control " min="1" max="12" value="">
                    </div>
                    <div class="col-4">
                      <label for="reference_year">Jahr</label>
                      <input id="reference_year" type="number" class="form-control " min="1900" max="3000" value="">
                    </div>
                    <span style="" class="invalid-feedback"></span>
                  </div>

                  <p class="h6 my-2">Rhythmus:</p>
                  <input id="interval_count" name="interval_count" type="number" class="form-control mb-2" value="" placeholder="Intervallzahl auswählen" min="1">

                  <select id="interval_format" name="interval_format" class="form-control ">
                    <option value="">Intervallformat auswählen</option>
                    <option>Tages-Rhythmus</option>
                    <option>Wochen-Rhythmus</option>
                    <option>Monats-Rhythmus</option>
                    <option>Jahres-Rhythmus</option>
                  </select>

                </fieldset>
              </div>  
              
              <div class="col-md-6">

                <p class="h5 mb-2">Erinnerungsfunktion:</p>
                <input id="activate_reminders" class="mr-2" name="activate_reminders" type="checkbox" value="activated">
                <label for="activate_reminders" class="form-check-label">Erinnerungen aktivieren</label>

                <fieldset id="reminderSet" disabled>
                  <div class="form-row">
                    <div class="col-2 my-3">
                      <input id="remind_before" name="remind_before" type="number" class="form-control " min="0" max="100" value="">
                    </div>  
                    <div class="col-10 my-3">
                      <p class="mb-0">Tage vor dem Stichtag erinnern</p>
                    </div>
                    
                    <div class="col-2 pt-4">
                      <input id="remind_at" name="remind_at" type="checkbox" class="mt-1 " value="activated">
                    </div>  
                    <div class="col-10 my-3">
                      <label for="remind_at" class="ml-2 form-check-label">Zum Stichtag erinnern</label>
                    </div>
                    
                    <div class="col-2 my-3">
                      <input id="remind_after" name="remind_after" type="number" class="form-control " min="0" max="100" value="">
                    </div>  
                    <div class="col-10 my-3">
                      <label for="remind_after" class="form-check-label" min="1" max="100">Tage nach Stichtag erinnern</label>
                    </div>
                  </div>
                </fieldset>

              </div>

            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="close-modal btn btn-secondary" data-dismiss="modal">Schließen</button>
            <button type="button" class="saveChanges btn btn-pe-darkgreen">Änderungen speichern</button>
          </div>
        </div>
      </div>
    </div>



    <!-- Modal: Form Submit -->
    <div id="submit_modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Dateibrowser</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Klicken Sie auf einen Ordner um diesen zu öffen. Um einen Ordner oder eine Datei auszuwählen, makieren Sie bitte die jeweilige Checkbox auf der rechten Seite.</p>
            <div id="folders"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Weiter bearbeiten</button>
            <button id="submitProcess" type="button" class="btn btn-pe-darkgreen">Prozessmatrix erstellen</button>
          </div>
        </div>
      </div>
    </div>

  </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
