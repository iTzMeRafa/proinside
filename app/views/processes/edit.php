<?php require APPROOT . '/views/inc/header.php'; ?>
  <a href="<?php echo URLROOT; ?>/processes/show/<?php echo $data['id']; ?>" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

  <div class="row">
    <div class="col-12">
      <h1 class="h4">Nicht-freigegebenen Prozess bearbeiten.</h1>
      <p>Füllen Sie die nachfolgenden Felder aus, um den Prozess zu bearbeiten.</p>
    </div>

    <?php if (isset($data['error'])) : ?>
  
      <div class="alert alert-danger alert-dismissible fade show col-12" role="alert">
        Bitte stellen Sie sicher, dass der zu erstellende Prozess einen Namen sowie einen Zielordner hat.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

    <?php endif; ?>

    <div class="col-12">
      <form id="process_form" action="<?php echo URLROOT; ?>/processes/edit/<?php echo $data['id']; ?>" method="post">
      
      <div class="row mb-3">

        <div class="col-md-6 mb-3">
          <h2 class="h6">Prozessname:</h2>
          <input name="process_name" type="text" class="form-control" placeholder="Prozessname" value="<?php echo $data['process']->process_name; ?>">
        </div>
        <div class="col-md-6 mb-3">
          <h2 class="h6">Zielordner:</h2>
          <select name="target_folder" class="form-control">
            <option value="">Zielordner auswählen</option>
            <?php foreach ($data['folders'] as $folder) : ?>
              <option value="<?php echo $folder->id; ?>" <?php echo ($data['target_id'] == $folder->id) ? 'selected' : ''; ?>><?php echo $folder->folder_name; ?></option>

              <?php if (isset($folder->sub_directories)) : ?>
              <?php foreach ($folder->sub_directories as $subFolder) : ?>
                <option value="<?php echo $subFolder->id; ?>" <?php echo ($data['target_id'] == $subFolder->id) ? 'selected' : ''; ?>><?php echo $subFolder->folder_name; ?></option>
              <?php endforeach; ?>
              <?php endif; ?>
              
            <?php endforeach; ?>
          </select>
        </div>

      </div>

      <p class="h4 mb-4">Prozessaufgaben</p>

      <div id="matrix" class="d-flex mb-3">

        <div id="remove-area" class="mr-3">

          <?php if (!empty($data['tasks'])) : ?>

            <?php foreach ($data['tasks'] as $key => $task) : ?>
              <div class="remove-button d-flex align-items-center px-3 mb-3" data-task="<?php echo $key+1; ?>">
                <i class="fas fa-times"></i>
              </div>

            <?php endforeach; ?>

          <?php else : ?>

            <div class="remove-button d-flex align-items-center px-3 mb-3" data-task="1">
              <i class="fas fa-times"></i>
            </div>

          <?php endif; ?>

            
        </div>

        <div class="scrolling-wrapper">
          <!-- Legend -->
          <div id="column_info" class="d-flex mb-3">
            <div class="legend art task_number"><p class="h6 mb-0">Nr.</p></div>
            <div class="legend art input"><p class="h6 mb-0">Input</p></div>
            <div class="legend art input_source"><p class="h6 mb-0">Inputquelle</p></div>
            <div class="legend art category"><p class="h6 mb-0">Kategorie</p></div>
            <div class="legend art task_name"><p class="h6 mb-0">Aufgabe</p></div>
            <div class="legend art output"><p class="h6 mb-0">Output</p></div>
            <div class="legend art output_target"><p class="h6 mb-0">Outputziel</p></div>
            <div class="legend art risk"><p class="h6 mb-0">Risiko</p></div>
            <div class="legend art status"><p class="h6 mb-0">Status</p></div>
            <div class="legend art required_knowledge"><p class="h6 mb-0">erforderliches Wissen</p></div>
            <div class="legend art roles" data-user-count="<?php echo $data['user_count']; ?>">
              <div class="d-flex">
                <?php foreach ($data['organs'] as $organ) : ?>
                  <div class="organ">
                      
                      <button type="button" class="btn btn-pe-darkgreen w-100" data-toggle="tooltip" data-placement="top" title="<?php echo $organ['name']; ?>">
                        <i class="organ-name fas fa-sitemap"></i>
                      </button>

                      <p class="mb-0 d-none"><?php echo $organ['name']; ?></p>
                    <div class="users d-flex">
                      <?php foreach ($organ['users'] as $user): ?>
                        <p class="user px-2 mb-0" data-id="<?php echo $user->id; ?>" data-organ-id="<?php echo $organ['organ_id']; ?>"><?php echo $user->abbreviation; ?></p>
                        <input name="legend-<?php echo $organ['organ_id']; ?>_<?php echo $user->id; ?>" type="hidden" value="<?php echo $organ['organ_id']; ?>">
                      <?php endforeach; ?>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
            <div class="legend art iso_value"><p class="h6 mb-0">ISO-Bezug</p></div>
            <!--<div class="legend art tags"><p class="h6 mb-0">Schlagwörter</p></div>-->
          </div>
          <!-- Legend -->

          <?php if (!empty($data['tasks'])) : ?>
            
            <?php foreach ($data['tasks'] as $key => $task) : ?>
                
              <div id="task-<?php echo $key+1; ?>" class="set d-flex mb-3">

                <div class="art task_number">
                  <p class="mb-0 pl-3">A<?php echo $key+1; ?></p>
                  <input class="number" name="<?php echo $key+1; ?>-number" type="hidden" value="<?php echo $key+1; ?>">
                  <input class="existing-id" name="<?php echo $key+1; ?>-existing_id" type="hidden" value="<?php echo $task->id; ?>">
                </div>
                <div class="art input">
                  <input name="<?php echo $key+1; ?>-input" type="text" class="form-control" value="<?php echo $task->input; ?>" placeholder="Input">
                </div>

                <div class="art input_source">
                  <input name="<?php echo $key+1; ?>-input_source" type="text" class="form-control" value="<?php echo $task->input_source; ?>" placeholder="Inputquelle">
                </div>

                <div class="art category">
                  <select name="<?php echo $key+1; ?>-category" class="form-control">
                    <option value="unausgewählt">Kategorie auswählen</option>
                    <option <?php echo ($task->category == 'Auftragswelt') ? 'selected' : ''; ?>>Auftragswelt</option>
                    <option <?php echo ($task->category == 'Strukturwelt') ? 'selected' : ''; ?>>Strukturwelt</option>
                    <option <?php echo ($task->category == 'Zielwelt') ? 'selected' : ''; ?>>Zielwelt</option>
                    <option <?php echo ($task->category == 'Rechtswelt') ? 'selected' : ''; ?>>Rechtswelt</option>
                    <option <?php echo ($task->category == 'Maßnahmenswelt') ? 'selected' : ''; ?>>Maßnahmenswelt</option>
                  </select>
                </div>

                <div class="art task_name">
                  <input name="<?php echo $key+1; ?>-name" type="text" class="form-control" value="<?php echo $task->name; ?>" placeholder="Name">
                </div>

                <div class="art output">
                  <input name="<?php echo $key+1; ?>-output" type="text" class="form-control" value="<?php echo $task->output; ?>" placeholder="Output">
                </div>

                <div class="art output_target">
                  <input name="<?php echo $key+1; ?>-output_target" type="text" class="form-control" value="<?php echo $task->output_target; ?>" placeholder="Outputziel">
                </div>

                <div class="art risk">
                  <select name="<?php echo $key+1; ?>-risk" class="form-control">
                    <option value="unausgewählt">Risiko auswählen</option>
                    <option <?php echo ($task->risk == 'Keins') ? 'selected' : ''; ?>>Keins</option>
                    <option <?php echo ($task->risk == 'Gering') ? 'selected' : ''; ?>>Gering</option>
                    <option <?php echo ($task->risk == 'Mittel') ? 'selected' : ''; ?>>Mittel</option>
                    <option <?php echo ($task->risk == 'Hoch') ? 'selected' : ''; ?>>Hoch</option>
                  </select>
                </div>

                <div class="art status">
                  <select name="<?php echo $key+1; ?>-status" class="form-control">
                    <option value="unausgewählt">Status auswählen</option>
                    <option <?php echo ($task->status == 'Plan') ? 'selected' : ''; ?>>Plan</option>
                    <option <?php echo ($task->status == 'Do') ? 'selected' : ''; ?>>Do</option>
                    <option <?php echo ($task->status == 'Check') ? 'selected' : ''; ?>>Check</option>
                    <option <?php echo ($task->status == 'Act') ? 'selected' : ''; ?>>Act</option>
                  </select>
                </div>

                <div class="art required_knowledge">
                  <input name="<?php echo $key+1; ?>-required_knowledge" type="text" class="form-control" placeholder="erfordl. Wissen" value="<?php echo $task->required_knowledge; ?>">
                </div>

                <div class="art roles">

                  <?php $counter = 1;
                        foreach ($task->roles_ordered as $role_key => $role) : ?>
                    
                    <div class="select-user d-flex flex-column justify-content-center align-items-center <?php echo $role; ?>">
                      <select name="<?php echo $key+1; ?>-user-select-<?php echo $counter ?>" class="form-control">
                        <option value="empty"></option>
                        <option <?php echo ($role == 'responsible') ? 'selected' : ''; ?> value="responsible">V</option>
                        <option <?php echo ($role == 'concerned') ? 'selected' : ''; ?> value="concerned">Z</option>
                        <option <?php echo ($role == 'contributing') ? 'selected' : ''; ?> value="contributing">M</option>
                        <option <?php echo ($role == 'information') ? 'selected' : ''; ?> value="information">I</option>
                      </select>
                    </div>
                  <?php $counter++;
                        endforeach; ?>
                </div> 

                <div class="art iso_value">
                  <input name="<?php echo $key+1; ?>-iso_value" type="text" class="form-control" value="<?php echo $task->iso_value; ?>" placeholder="ISO-Bezug">
                </div>

              </div>

            <?php endforeach; ?>

          <?php endif; ?>

          
        </div><!-- end of #scrolling-wrapper -->

      </div><!-- end of #matrix -->
        <!--
        <button id="addTask" type="button" class="btn btn-pe-lightgreen">Aufgabe zum Prozess hinzufügen</button> -->

        <div class="row">
          <div class="col-md-12 mt-2 d-flex">
            <p class="border bg-pe-lightgreen text-white py-2 px-3 mb-0 mr-2">Neue Prozessaufgabe</p>
            <select id="selectTarget" class="form-control w-25 mr-2">
              <option value="beginning">am Tabellenanfang</option>
              <?php if (!empty($data['tasks'])) : ?>
                <?php foreach ($data['tasks'] as $index => $task) : ?>
                  
                  <?php if ($index + 1 == count($data['tasks'])) : ?>
                      <option value="task-<?php echo  $index+1; ?>" selected>nach A<?php echo  $index+1; ?></option>
                  <?php else : ?>
                      <option value="task-<?php echo  $index+1; ?>">nach A<?php echo  $index+1; ?></option>
                  <?php endif; ?>

                <?php endforeach; ?>
              <?php endif; ?>
            </select>
            <button id="addAfter" type="button" class="btn btn-pe-lightgreen">einfügen</button>
          </div>
        </div>
        
        <input id="submit" type="submit" class="btn btn-pe-darkgreen float-right" value="Erstellen">
      </form>
    </div>



    <!-- Modal: Form Submit -->
    <div id="submit_modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Dateibrowser</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Klicken Sie auf einen Ordner um diesen zu öffen. Um einen Ordner oder eine Datei auszuwählen, makieren Sie bitte die jeweilige Checkbox auf der rechten Seite.</p>
            <div id="folders"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Weiter bearbeiten</button>
            <button id="submitProcess" type="button" class="btn btn-pe-darkgreen">Prozessmatrix erstellen</button>
          </div>
        </div>
      </div>
    </div>

  </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
