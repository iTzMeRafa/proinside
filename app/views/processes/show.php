<?php require APPROOT . '/views/inc/header.php'; ?>
  <?php displayFlash('process_message'); ?>
  <?php displayFlash('no_permisssion'); ?>
<a href="javascript:history.go(-1)" class="btn btn-light mb-3"><i class="fa fa-chevron-left"></i> Zurück</a>

  <?php if ($data['privUser']->hasPrivilege('23') && $data['is_archived'] != 'true') : ?>
    <a href="<?php echo URLROOT; ?>/processes/recycle/<?php echo $data['id']; ?>" class="btn btn-dark float-right"><i class="fa fa-edit mr-2"></i>Als Vorlage nutzen</a>
  <?php endif; ?>  

  <div class="row">

    <div class="col-12">
      
      <div class="row mb-3">

        <?php if ($data['is_archived'] == 'true') : ?>
          <div class="col-12">
            <div id="approval" class="alert alert-danger" role="alert">
              <i class="fas fa-archive mr-2"></i>Dieser Prozess ist inaktiv und befindet sich im Archiv
            </div>
          </div>
        <?php endif; ?>

        <?php if ($data['privUser']->hasPrivilege('29') && $data['is_approved'] == 'false' && $data['is_archived'] != 'true') : ?>
          <div class="col-12 mb-3">
            <div id="approval" class="alert alert-success" role="alert">
              <h4 class="alert-heading">Freigabe erforderlich!</h4>
              <p>Dieser Prozess ist noch nicht freigeben und daher inaktiv. Bitte schauen Sie sie sich den Prozess genau an und geben ihn frei oder melden die Fehler bei einem dafür zuständigen Mitarbeiter.</p>
              <hr>
              <div class="d-flex justify-content-end">
                <?php if ($data['privUser']->hasPrivilege('23') && $data['is_archived'] != 'true') : ?>
                  <a href="<?php echo URLROOT; ?>/processes/edit/<?php echo $data['id']; ?>" class="btn btn-dark mr-2"><i class="fa fa-edit mr-2"></i>Bearbeiten</a>
                <?php endif; ?>
                <a href="<?php echo URLROOT; ?>/processes/approve/<?php echo $data['id']; ?>" class="btn btn-pe-lightgreen"><i class="fas fa-check mr-2"></i>Prozess freigeben</a>
                
              </div>
            </div>
          </div>
        <?php endif; ?>

        <div class="col-md-6 mb-3">
          <p class="h4"><i class="fas fa-folder"></i> <?php echo $data['target_folder']; ?></p>
        </div>

        <div class="col-md-6 mb-3">
          <h1 class="h4">Prozess: <?php echo $data['process_name']; ?></h1>
        </div>
        

      </div>

      <p class="h4 mb-4">Prozessaufgaben</p>

      <div id="matrix" class="d-flex mb-3">

        <div id="remove-area" class="mr-3">
          <?php if ($data['tasks']) : ?>
          <?php foreach ($data['tasks'] as $key => $task) : ?>
              
              <a href="<?php echo URLROOT; ?>/tasks/show/<?php echo $task->id; ?>" class="task_no d-flex align-items-center px-3 mb-3">
                <p class="m-0 text-white mr-2">A<?php echo $key+1; ?></p>
                <i class="fas fa-external-link-alt text-white"></i>
              </a>

          <?php endforeach; ?>
          <?php endif; ?>

        </div>

        <div class="scrolling-wrapper">
          <div id="column_info" class="d-flex mb-3">
            <div class="legend art input"><p class="h6 mb-0 mx-3">Input</p></div>
            <div class="legend art input_source"><p class="h6 mb-0">Inputquelle</p></div>
            <div class="legend art category"><p class="h6 mb-0">Kategorie</p></div>
            <div class="legend art task_name"><p class="h6 mb-0">Aufgabe</p></div>
            <div class="legend art output"><p class="h6 mb-0">Output</p></div>
            <div class="legend art output_target"><p class="h6 mb-0">Outputziel</p></div>
            <div class="legend art risk"><p class="h6 mb-0">Risiko</p></div>
            <div class="legend art status"><p class="h6 mb-0">Status</p></div>
            <div class="legend art roles" data-user-count="<?php echo $data['user_count']; ?>">
              <div class="d-flex">
                <?php foreach ($data['organs'] as $organ) : ?>
                  <div class="organ">
                      
                      <button type="button" class="btn btn-pe-darkgreen w-100" data-toggle="tooltip" data-placement="top" title="<?php echo $organ['name']; ?>">
                        <i class="organ-name fas fa-sitemap"></i>
                      </button>

                      <p class="mb-0 d-none"><?php echo $organ['name']; ?></p>
                    <div class="users d-flex">
                      <?php foreach ($organ['users'] as $user): ?>
                        <button type="button" class="btn btn-pe-darkgreen user px-2 mb-0" data-toggle="tooltip" data-placement="top" title="<?php echo $user->firstname.' '.$user->lastname; ?>">
                          <?php echo $user->abbreviation; ?>
                        </button>

                        <input name="legend-<?php echo $organ['organ_id']; ?>_<?php echo $user->id; ?>" type="hidden" value="<?php echo $organ['organ_id']; ?>">
                      <?php endforeach; ?>
                    </div>
                  </div>
                <?php endforeach; ?>
              </div>
            </div>
            <div class="legend art iso_value"><p class="h6 mb-0">ISO-Bezug</p></div>
            <!--<div class="legend art tags"><p class="h6 mb-0">Schlagwörter</p></div>-->
          </div>

          <?php if (!empty($data['tasks'])) : ?>
          
            <?php foreach ($data['tasks'] as $task_number => $task) : ?>

              <div id="task-<?php echo $task_number; ?>" class="set d-flex mb-3">

                <div class="art input">
                  <p class="mb-0 mx-3"><?php echo $task->input; ?></p>
                </div>

                <div class="art input_source">
                  <p class="mb-0 mx-3"><?php echo $task->input_source; ?></p>
                </div>

                <div class="art category">
                  
                  <?php if ($task->category == 'unausgewählt' || $task->category == 'NULL') : ?>
                    <p class="mb-0 mx-3">/</p>
                  <?php else : ?>
                    <p class="mb-0 mx-3"><?php echo $task->category; ?></p>
                  <?php endif; ?>

                </div>

                <div class="art task_name">
                  <p class="mb-0 mx-3"><?php echo $task->name; ?></p>
                </div>

                <div class="art output">
                  <p class="mb-0 mx-3"><?php echo $task->output; ?></p>
                </div>

                <div class="art output_target">
                  <p class="mb-0 mx-3"><?php echo $task->output_target; ?></p>
                </div>

                <div class="art risk">

                  <?php if ($task->risk == 'unausgewählt' || $task->category == 'NULL') : ?>
                    <p class="mb-0 mx-3">/</p>
                  <?php else : ?>
                    <p class="mb-0 mx-3"><?php echo $task->risk; ?></p>
                  <?php endif; ?>

                </div>

                <div class="art status">

                  <?php if ($task->status == 'unausgewählt' || $task->category == 'NULL') : ?>
                    <p class="mb-0 mx-3">/</p>
                  <?php else : ?>
                    <p class="mb-0 mx-3"><?php echo $task->status; ?></p>
                  <?php endif; ?>

                </div>

                <div class="art roles">
                  <?php foreach ($task->roles_ordered as $role_key => $role) : ?>
                    <div class="select-user d-flex flex-column justify-content-center align-items-center <?php echo $role; ?>">
                      <p class="mb-0">
                        <?php echo ($role == 'responsible') ? 'V' : ''; ?>
                        <?php echo ($role == 'concerned') ? 'Z' : ''; ?>
                        <?php echo ($role == 'contributing') ? 'M' : ''; ?>
                        <?php echo ($role == 'information') ? 'I' : ''; ?>
                      </p>
                    </div>
                  <?php endforeach; ?>
                </div> 

                <div class="art iso_value">
                  <p class="mb-0 mx-3"><?php echo $task->iso_value; ?></p>
                </div>

              </div>

            <?php endforeach; ?>

          <?php endif; ?>

          
        </div><!-- end of #scrolling-wrapper -->

      </div><!-- end of #matrix -->

      <?php if ($data['privUser']->hasPrivilege('24') && $data['is_archived'] != 'true') : ?>
        <button id="archive_process" class="btn btn-danger float-right" data-folder-id="<?php echo $data['folder_id']; ?>" data-process-id="<?php echo $data['id']; ?>">Löschen (Archivieren)</button>
      <?php endif; ?>  
        
        
    </div>



    <!-- Modal: Form Submit -->
    <div id="submit_modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Dateibrowser</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Klicken Sie auf einen Ordner um diesen zu öffen. Um einen Ordner oder eine Datei auszuwählen, makieren Sie bitte die jeweilige Checkbox auf der rechten Seite.</p>
            <div id="folders"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Weiter bearbeiten</button>
            <button id="submitProcess" type="button" class="btn btn-pe-darkgreen">Prozessmatrix erstellen</button>
          </div>
        </div>
      </div>
    </div>

  </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
