
<?php require APPROOT . '/views/inc/header.php'; ?>

<?php
    $pageTitle = 'Erstellen'
?>

<?php require APPROOT . '/views/qualifications/templates/title.php'; ?>

<?php require APPROOT . '/views/qualifications/templates/navigation.php'; ?>

<?php require APPROOT . '/views/qualifications/templates/createForm.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
