
<?php require APPROOT . '/views/inc/header.php'; ?>

<?php
$pageTitle = 'Ansehen'
?>

    <?php require APPROOT . '/views/qualifications/templates/title.php'; ?>

    <?php require APPROOT . '/views/qualifications/templates/navigation.php'; ?>

    <?php displayFlash('deleteQualificationState'); ?>
    <?php displayFlash('renameQualificationState'); ?>
    <?php displayFlash('assignQualificationState'); ?>


    <?php require APPROOT . '/views/qualifications/templates/qualificationsList.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>
