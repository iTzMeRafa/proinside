<div class="row">
    <div class="col-md-6 offset-md-3">

        <?php displayFlash('createQualificationState'); ?>

        <form method="post" action="<?= URLROOT; ?>/QualificationsCreate/post">
            <div class="form-group">
                <label for="qualificationName">Name der Kompetenz</label>
                <input
                        type="text"
                        name="qualificationName"
                        class="form-control"
                        id="qualificationName"
                        placeholder="Name"
                />
            </div>
            <div class="form-group">
                <label for="qualificationExpireTime">Verfällt in </label>
                <input
                        type="number"
                        name="qualificationExpireTime"
                        class="form-control"
                        aria-describedby="qualificationExpireTimeHelp"
                        id="qualificationExpireTime"
                        placeholder="Tage..."
                />
                <small id="qualificationExpireTimeHelp" class="form-text text-muted">Nichts oder '0' eintragen, wenn diese Kompetenz nicht erneuert werden muss.</small>
            </div>
            <button type="submit" name="qualificationCreate" class="btn btn-primary float-right">Erstellen</button>
        </form>

    </div>
</div>

