<!-- Sortable Table -->
<div class="row">
    <div class="col-md-12">
        <table id="qualificationsList" class="table table-striped table-bordered" style="width: 100%">
            <thead>
            <tr style="background-color: #0B676E; color: white; border: none;">
                <th style="border: none;">ID</th>
                <th style="border: none;">Name</th>
                <th style="border: none;">Verfällt nach</th>
                <th style="border: none;">Aktionen</th>
                <th style="border: none;">Hinzugefügt Am</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($data['qualifications'] as $qualification) : ?>
                <tr>
                    <td>
                        <a href="<?= URLROOT; ?>/Qualifications/show/<?= $qualification->id; ?>">
                            <?= $qualification->id; ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?= URLROOT; ?>/Qualifications/show/<?= $qualification->id; ?>">
                            <?= $qualification->name; ?>
                        </a>
                    </td>
                    <td><?= $qualification->expire_in_days; ?> Tagen</td>
                    <td>
                        <div class="btn-group" role="group" aria-label="Basic example">

                            <!-- Delete Button -->
                            <button
                                    type="button"
                                    data-toggle="modal"
                                    data-target="#qualificationDeleteModal<?= $qualification->id; ?>"
                                    class="btn btn-danger"
                            >
                                <i class="fa fa-trash mr-2"></i> Löschen
                            </button>

                            <!-- Assign Qualifications Button -->
                            <button
                                    type="button"
                                    data-toggle="modal"
                                    data-target="#qualificationAssignModal<?= $qualification->id; ?>"
                                    class="btn btn-pe-lightgreen"
                            >
                                <i class="fas fa-plus-square mr-2"></i> Mitarbeitern Zuweisen
                            </button>

                            <!-- Rename Qualification Button -->
                            <button
                                    type="button"
                                    data-toggle="modal"
                                    data-target="#qualificationRenameModal<?= $qualification->id; ?>"
                                    class="btn btn-pe-darkgreen"
                            >
                                <i class="fas fa-pen-square mr-2"></i> Umbenennen
                            </button>

                        </div>
                    </td>
                    <td><?= $qualification->created_at; ?></td>

                </tr>

                <!-- Modals for each entry with correct values -->
                <?php require APPROOT . '/views/qualifications/templates/qualificationsModals.php'; ?>

            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
<!-- /Sortable Table -->