<!-- Qualification Delete Modal -->
<div id="qualificationDeleteModal<?= $qualification->id; ?>" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Kompetenz Löschen: <strong><?= $qualification->name; ?></strong></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>
                    Sind Sie sicher, dass Sie die Kompetenz "<?= $qualification->name; ?>" löschen wollen? <br />
                    Dieser Vorgang kann nicht rückgängig gemacht werden, auch verlieren alle Mitarbeiter diese Kompetenz sofern sie besteht.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <a href="<?= URLROOT; ?>/QualificationsDelete/index/<?= $qualification->id; ?>" type="button" class="saveChanges btn btn-danger">Löschen</a>
            </div>
        </div>
    </div>
</div>

<!-- Qualification Rename Modal -->
<div id="qualificationRenameModal<?= $qualification->id; ?>" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <form id="qualificationsRenameForm<?= $qualification->id; ?>" method="POST" action="<?= URLROOT; ?>/QualificationsRename">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Kompetenz Umbenennen: <strong><?= $qualification->name; ?></strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" class="form-control" name="renamedQualification" value="<?= $qualification->name; ?>" placeholder="<?= $qualification->name; ?>" />
                    <input type="hidden" name="qualificationId" value="<?= $qualification->id; ?>" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="submit" name="renameQualificationSubmit" class="saveChanges btn btn-primary">Umbenennen</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Qualification Assign Modal -->
<div id="qualificationAssignModal<?= $qualification->id; ?>" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <form id="qualificationsAssignForm<?= $qualification->id; ?>" method="POST" action="<?= URLROOT; ?>/QualificationsAssign">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Kompetenz Zuweisen: <strong><?= $qualification->name; ?></strong></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <!-- Userlist -->
                    <div class="modal-list mt-3" id="qualificationsAssignUserList">
                        <?php if (!empty($data['organs'])) : ?>
                            <div class="accordion" id="userListingAccordion">
                                <?php foreach ($data['organs'] as $key => $organ) : ?>
                                    <div class="card">
                                        <div class="card-header" id="heading<?= $key; ?>">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link w-100 text-left" type="button" data-toggle="collapse" data-target="#userListing<?= $key; ?>" aria-expanded="<?= $key == 0 ? 'true' : ''; ?>" aria-controls="userListing<?= $key; ?>">
                                                    <?php echo $organ->name; ?>
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="userListing<?= $key; ?>" class="collapse <?= $key == 0 ? 'show' : ''; ?>" aria-labelledby="heading<?= $key; ?>" data-parent="#userListingAccordion">
                                            <div class="card-body">
                                                <?php if (!empty($organ->id == 2)) : ?>
                                                    <?php foreach ($organ->suborgans as $suborgan) : ?>
                                                        <?php foreach ($suborgan->users as $user) : ?>
                                                            <div class="form-check">
                                                                <input class="form-check-input"
                                                                       type="checkbox"
                                                                       value="<?php echo $user->id.'_'.$organ->id.'_'.$suborgan->id; ?>"
                                                                       name="qualificationUserList[]"
                                                                       data-name="<?php echo $user->firstname.' '.$user->lastname; ?>"
                                                                       data-organ="<?php echo $suborgan->name;
                                                                       ?>"
                                                                />
                                                                <label class="form-check-label" for="qualificationUserList[]"><?php echo $user->firstname.' '.$user->lastname.' ('.$suborgan->name.')'; ?></label>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    <?php foreach ($organ->users as $user) : ?>
                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id.'_'.$organ->id; ?>"
                                                                   name="qualificationUserList[]"
                                                                   data-name="<?php echo $user->firstname.' '.$user->lastname; ?>"
                                                                   data-organ="<?php echo $organ->name;
                                                                   ?>"
                                                            />
                                                            <label class="form-check-label" for="qualificationUserList[]"><?php echo $user->firstname.' '.$user->lastname; ?></label>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <!-- /Userlist -->
                    
                    <input type="hidden" name="qualificationId" value="<?= $qualification->id; ?>" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="submit" name="assignQualificationSubmit" class="saveChanges btn btn-primary">Zuweisen</button>
                </div>
            </div>
        </form>
    </div>
</div>


