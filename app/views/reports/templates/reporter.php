<form method="post" action="<?= URLROOT; ?>/ReportsCreate/post">

    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-body">

                    <!-- Inner Card -->
                    <div class="row mb-5">
                        <div class="col-md-12">
                            <h3>Auswählen aus Tabelle:</h3>
                            <select name="databaseTable" class="form-control">
                                <option value="users">Mitarbeiter</option>
                                <option value="tasks">Aufgaben</option>
                                <option value="methods">Maßnahmen</option>
                            </select>
                        </div>
                    </div>

                    <!-- Inputs for: `Mitarbeiter` -->
                    <div class="row mb-5">
                        <div class="col-md-12">
                            <h3>Geben Sie die Werte ein nach dem gesucht werden soll:</h3>
                            <!-- User Firstname -->
                            <div class="form-group">
                                <label for="firstnameLable">Vorname</label>
                                <input type="text" name="reportFirstname" class="form-control" id="firstnameReport" aria-describedby="firstnameLable" placeholder="Vorname">
                            </div>

                            <!-- User Lastname -->
                            <div class="form-group">
                                <label for="lastnameLable">Nachname</label>
                                <input type="text" name="reportLastname" class="form-control" id="lastnameReport" aria-describedby="lastnameLable" placeholder="Nachname">
                            </div>

                            <!-- User Email -->
                            <div class="form-group">
                                <label for="emailLable">E-Mail</label>
                                <input type="text" name="reportEmail" class="form-control" id="emailReport" aria-describedby="emailLable" placeholder="E-Mail">
                            </div>

                            <!-- User abbrev -->
                            <div class="form-group">
                                <label for="abbrevLable">Kürzel</label>
                                <input type="text" name="reportAbbrev" class="form-control" id="abbrevReport" aria-describedby="abbrevLable" placeholder="Kürzel">
                            </div>

                            <!-- Register Date -->
                            <div class="row mb-5">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="registerFromLable">Registriert seit:</label>
                                        <input type="text" name="reportRegisterFrom" class="form-control" id="userRegistrationFrom" aria-describedby="registerFromLable" placeholder="Registriert seit">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="registerToLable">Registriert Bis:</label>
                                        <input type="text" name="reportRegisterTo" class="form-control" id="userRegistrationTo" aria-describedby="registerToLable" placeholder="Registriert bis">
                                    </div>
                                </div>
                            </div>

                            <!-- Generate Report -->
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" name="reportCreate" class="btn btn-pe-darkgreen float-right">Report erstellen</button>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>

</form>
