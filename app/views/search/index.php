<?php require APPROOT . '/views/inc/header.php'; ?>

<div class="row mb-3">
    <div class="col-md-6">
        <h1 class="btn btn-pe-lightgreen"><i class="fas fa-search mr-2"></i>Suchergebnisse</h1>
    </div>
    <div class="col-12 mt-2 mt-4">

        <!-- Query -->
        <h3><?= $data['searchData']['countResults']; ?> Suchergebnisse für "<?= $data['query']; ?>"</h3>

        <!-- Navigation -->
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Aufgaben</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Maßnahmen</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Dokumente</a>
            </div>
        </nav>

        <!-- Contents -->
        <div class="tab-content" id="nav-tabContent">

            <!-- Tasks -->
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <?php require APPROOT . '/views/search/templates/tasks.php'; ?>
            </div>

            <!-- Methods -->
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <?php require APPROOT . '/views/search/templates/methods.php'; ?>
            </div>

            <!-- Documents -->
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                <?php require APPROOT . '/views/search/templates/documents.php'; ?>
            </div>

        </div>









    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
