<!-- Sortable Table for DOCUMENTS -->
<?php if ($data['searchData']['documents'] == null): ?>
    <?php displayFlash('no_permisssionDocuments'); ?>
<?php else: ?>
    <div class="col-md-12 mt-5">
        <table id="globalSearchDocuments" class="table table-striped table-bordered" style="width: 100%">
            <thead>
            <tr style="background-color: #0B676E; color: white; border: none;">
                <th style="border: none;">ID</th>
                <th style="border: none;">Dateiname</th>
                <th style="border: none;">Tags</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($data['searchData']['documents'] as $document) : ?>
                <tr>
                    <td><a href="<?php echo URLROOT; ?>/listings/index/<?= $document->parent_id; ?>"><?= $document->id; ?></a></td>
                    <td><a href="<?php echo URLROOT; ?>/listings/index/<?= $document->parent_id; ?>"><?= $document->file_name; ?></a></td>
                    <td><?= $document->tags; ?></td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
<?php endif; ?>
<!-- /Sortable Table -->