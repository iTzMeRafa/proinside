<!-- Sortable Table for TASKS -->
<?php if ($data['searchData']['tasks'] == null): ?>
    <?php displayFlash('no_permisssionTask'); ?>
<?php else: ?>
    <div class="col-md-12 mt-5">
        <table id="globalSearchTasks" class="table table-striped table-bordered" style="width: 100%">
            <thead>
            <tr style="background-color: #0B676E; color: white; border: none;">
                <th style="border: none;">ID</th>
                <th style="border: none;">Name</th>
                <th style="border: none;">Kategorie</th>
                <th style="border: none;">Unternehmensbereich</th>
                <th style="border: none;">Status</th>
                <th style="border: none;">Check Intervall</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($data['searchData']['tasks'] as $task) : ?>
                <tr>
                    <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task->taskId; ?>">A<?= $task->taskId; ?></a></td>
                    <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task->taskId; ?>"><?= $task->name; ?></a></td>
                    <td><?= $task->category; ?></td>
                    <td><?= $task->organname; ?></td>
                    <td><?= $task->status; ?></td>
                    <td><?= $task->check_intervals; ?></td>
                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
<?php endif; ?>
<!-- /Sortable Table -->