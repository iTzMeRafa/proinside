<?php require APPROOT . '/views/inc/header.php'; ?>
<a href="<?php echo URLROOT . '/tasks/show/' . $data['id'] ?>'" class="btn btn-light mb-3"><i
            class="fa fa-chevron-left"></i> Zurück</a>

<div class="row">
    <div class="col-12">
        <h1 class="h2">Aufgabe A<?php echo $data['id']; ?> bearbeiten</h1>
        <p>Füllen Sie die nachfolgenden Felder aus, um die Aufgabe zu bearbeiten</p>
    </div>

    <div class="col-12">
        <form action="<?php echo URLROOT; ?>/tasks/edit/<?php echo $data['id']; ?>" method="post">
            <div class="row">

                <div class="form-group col-md-4">
                    <p class="h6">Name: <sup>*</sup></p>
                    <input name="name" type="text"
                           class="form-control <?php echo (!empty($data['name_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['name']; ?>" placeholder="Name">
                    <span class="invalid-feedback"><?php echo $data['name_err']; ?></span>

                    <p class="h6 mt-3">Unternehmensbereich:</p>
                    <select id="department" name="department"
                            class="form-control <?php echo (!empty($data['department_err'])) ? 'is-invalid' : ''; ?>">
                        <option value="">Unternehmensbereich auswählen</option>
                        <?php foreach ($data['organs'] as $organ) : ?>
                            <option value="<?php echo $organ->id ?>" <?php echo $data['department'] == $organ->id ? 'selected' : ''; ?>><?php echo $organ->name; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <span class="invalid-feedback"><?php echo $data['department_err']; ?></span>
                </div>

                <div class="form-group col-md-8">
                    <p class="h4">Beschreibung:</p>
                    <textarea name="description" class="form-control" rows="5"
                              cols="80"><?php echo $data['description']; ?></textarea>
                </div>

                <div class="form-group col-md-3">
                    <p class="h6">Input: <sup>*</sup></p>
                    <input name="input" type="text"
                           class="form-control <?php echo (!empty($data['input_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['input']; ?>" placeholder="Input">
                    <span class="invalid-feedback"><?php echo $data['input_err']; ?></span>
                </div>
                <div class="form-group col-md-3">
                    <p class="h6">Inputquelle: <sup>*</sup></p>
                    <input name="input_source" type="text"
                           class="form-control <?php echo (!empty($data['input_source_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['input_source']; ?>" placeholder="Inputquelle">
                    <span class="invalid-feedback"><?php echo $data['input_source_err']; ?></span>
                </div>
                <div class="form-group col-md-3">
                    <p class="h6">Output: <sup>*</sup></p>
                    <input name="output" type="text"
                           class="form-control <?php echo (!empty($data['output_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['output']; ?>" placeholder="Output">
                    <span class="invalid-feedback"><?php echo $data['output_err']; ?></span>
                </div>
                <div class="form-group col-md-3">
                    <p class="h6">Outputziel: <sup>*</sup></p>
                    <input name="output_target" type="text"
                           class="form-control <?php echo (!empty($data['output_target_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['output_target']; ?>" placeholder="Outputziel">
                    <span class="invalid-feedback"><?php echo $data['output_target_err']; ?></span>
                </div>


                <div class="form-group col-md-4">
                    <h2 class="h6">Kategorie: <sup>*</sup></h2>
                    <select name="category"
                            class="form-control <?php echo (!empty($data['category_err'])) ? 'is-invalid' : ''; ?>">
                        <option value="" selected>Kategorie auswählen</option>
                        <option <?php echo $data['category'] == 'Auftragswelt' ? 'selected' : ''; ?>>Auftragswelt
                        </option>
                        <option <?php echo $data['category'] == 'Strukturwelt' ? 'selected' : ''; ?>>Strukturwelt
                        </option>
                        <option <?php echo $data['category'] == 'Zielwelt' ? 'selected' : ''; ?>>Zielwelt</option>
                        <option <?php echo $data['category'] == 'Rechtswelt' ? 'selected' : ''; ?>>Rechtswelt</option>
                        <option <?php echo $data['category'] == 'Maßnahmenswelt' ? 'selected' : ''; ?>>Maßnahmenswelt
                        </option>
                    </select>
                    <span class="invalid-feedback"><?php echo $data['category_err']; ?></span>
                </div>
                <div class="form-group col-md-4">
                    <h2 class="h6">Risikoeinstufung: <sup>*</sup></h2>
                    <select name="risk"
                            class="form-control <?php echo (!empty($data['risk_err'])) ? 'is-invalid' : ''; ?>">
                        <option value="" selected>Risiko auswählen</option>
                        <option <?php echo $data['risk'] == 'Keins' ? 'selected' : ''; ?>>Keins</option>
                        <option <?php echo $data['risk'] == 'Gering' ? 'selected' : ''; ?>>Gering</option>
                        <option <?php echo $data['risk'] == 'Mittel' ? 'selected' : ''; ?>>Mittel</option>
                        <option <?php echo $data['risk'] == 'Hoch' ? 'selected' : ''; ?>>Hoch</option>
                    </select>

                    <span class="invalid-feedback"><?php echo $data['risk_err']; ?></span>
                </div>

                <div class="form-group col-md-4">
                    <h2 class="h6">Status: <sup>*</sup></h2>
                    <select name="status"
                            class="form-control <?php echo (!empty($data['status_err'])) ? 'is-invalid' : ''; ?>">
                        <option value="">Status auswählen</option>
                        <option <?php echo $data['status'] == 'Plan' ? 'selected' : ''; ?>>Plan</option>
                        <option <?php echo $data['status'] == 'Do' ? 'selected' : ''; ?>>Do</option>
                        <option <?php echo $data['status'] == 'Check' ? 'selected' : ''; ?>>Check</option>
                        <option <?php echo $data['status'] == 'Act' ? 'selected' : ''; ?>>Act</option>
                    </select>
                    <span class="invalid-feedback"><?php echo $data['status_err']; ?></span>
                </div>

                <!-- <?php /*
          <div class="col-12 my-3" id="accordionExample">
            <div class="card">
              <div class="card-header bg-pe-darkgreen rounded-0" id="headingOne">
                <h2 class="mb-0">
                  <button class="btn btn-link btn-pe-darkgreen" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Funktionszuteilung: <sup>*</sup>
                  </button>
                </h2>
              </div>

              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="row mx-0 mt-2">
                  <div class="col-md-3">
                    <button id="open_roles_responsible" type="button" class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal" data-target="#roles_responsible_modal">Verantwortlichkeiten definieren</button>
                    <input id="roles_responsible" name="roles_responsible" type="hidden" class="form-control" value="<?php echo $data['roles_responsible_id']; ?>">
                    <ul id="responsibleUI" class="list-group">
                      <?php if( $data['roles_responsible'] ) : ?>

                        <?php foreach($data['roles_responsible'] as $role) : ?>
                          <li class="list-group-item"><?php echo $role->firstname.' '.$role->lastname; ?></li>
                        <?php endforeach; ?>

                      <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                      <?php endif; ?>
                    </ul>
                  </div>

                  <div class="col-md-3">
                    <button id="open_roles_concerned" type="button" class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal" data-target="#roles_concerned_modal">Zuständigkeiten definieren</button>
                    <input id="roles_concerned" name="roles_concerned" type="hidden" class="form-control" value="<?php echo $data['roles_concerned_id']; ?>">
                    <ul id="concernedUI" class="list-group">
                      <?php if( $data['roles_concerned'] ) : ?>

                        <?php foreach($data['roles_concerned'] as $role) : ?>
                          <li class="list-group-item"><?php echo $role->firstname.' '.$role->lastname; ?></li>
                        <?php endforeach; ?>

                      <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                      <?php endif; ?>
                    </ul>
                  </div>

                  <div class="col-md-3">
                    <button id="open_roles_contributing" type="button" class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal" data-target="#roles_contributing_modal">Mitwirkungen definieren</button>
                    <input id="roles_contributing" name="roles_contributing" type="hidden" class="form-control" value="<?php echo $data['roles_contributing_id']; ?>">
                    <ul id="contributingUI" class="list-group">
                      <?php if( $data['roles_contributing'] ) : ?>

                        <?php foreach($data['roles_contributing'] as $role) : ?>
                          <li class="list-group-item"><?php echo $role->firstname.' '.$role->lastname; ?></li>
                        <?php endforeach; ?>

                      <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                      <?php endif; ?>
                    </ul>
                  </div>

                  <div class="col-md-3">
                    <button id="open_roles_information" type="button" class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal" data-target="#roles_information_modal">Information definieren</button>
                    <input id="roles_information" name="roles_information" type="hidden" class="form-control" value="<?php echo $data['roles_information_id']; ?>">
                    <ul id="informationUI" class="list-group">
                      <?php if( $data['roles_information'] ) : ?>

                        <?php foreach($data['roles_information'] as $role) : ?>
                          <li class="list-group-item"><?php echo $role->firstname.' '.$role->lastname; ?></li>
                        <?php endforeach; ?>

                      <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                      <?php endif; ?>
                    </ul>
                  </div>
                </div>

              </div>
            </div>
          </div>
          */ ?>
          -->


                <div class="col-md-12 ">
                    <p class="h4 px-3 py-2 bg-pe-darkgreen text-white">Funktionszuteilung: <sup>*</sup></p>
                    <div class="row">

                        <div class="col-md-3">
                            <button id="open_roles_responsible" type="button"
                                    class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                    data-target="#roles_responsible_modal">Verantwortlichkeiten definieren
                            </button>
                            <input id="roles_responsible" name="roles_responsible" type="hidden" class="form-control"
                                   value="<?php echo $data['roles_responsible_id']; ?>">
                            <ul id="responsibleUI" class="list-group">
                                <?php
                                $roleResponsible = $data['roles_responsible'];
                                if ($roleResponsible && array_key_exists('user', (array)$roleResponsible[0]) && array_key_exists('organ', (array)$roleResponsible[0])) :
                                    ?>

                                    <?php foreach ($roleResponsible as $role) : ?>

                                    <?php if (!empty($role['suborgan'])) : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['suborgan']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php else : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['organ']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php endif; ?>

                                <?php endforeach; ?>


                                <?php else : ?>
                                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine
                                        Zuteilung
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <button id="open_roles_concerned" type="button"
                                    class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                    data-target="#roles_concerned_modal">Zuständigkeiten definieren
                            </button>
                            <input id="roles_concerned" name="roles_concerned" type="hidden" class="form-control"
                                   value="<?php echo $data['roles_concerned_id']; ?>">
                            <ul id="concernedUI" class="list-group">
                                <?php
                                $roleConcerned = $data['roles_concerned'];
                                if ($roleConcerned && array_key_exists('user', (array)$roleConcerned[0]) && array_key_exists('organ', (array)$roleConcerned[0])) :
                                    ?>

                                    <?php foreach ($data['roles_concerned'] as $role) : ?>

                                    <?php if (!empty($role['suborgan'])) : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['suborgan']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php else : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['organ']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                                <?php else : ?>
                                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine
                                        Zuteilung
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <button id="open_roles_contributing" type="button"
                                    class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                    data-target="#roles_contributing_modal">Mitwirkungen definieren
                            </button>
                            <input id="roles_contributing" name="roles_contributing" type="hidden" class="form-control"
                                   value="<?php echo $data['roles_contributing_id']; ?>">
                            <ul id="contributingUI" class="list-group">
                                <?php
                                $roleContributing = $data['roles_contributing'];
                                if ($roleContributing && array_key_exists('user', (array)$roleContributing[0]) && array_key_exists('organ', (array)$roleContributing[0])) :
                                    ?>

                                    <?php foreach ($roleContributing as $role) : ?>

                                    <?php if (!empty($role['suborgan'])) : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['suborgan']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php else : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['organ']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                                <?php else : ?>
                                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine
                                        Zuteilung
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <div class="col-md-3">
                            <button id="open_roles_information" type="button"
                                    class="btn btn-pe-darkgreen d-block w-100 mb-2" data-toggle="modal"
                                    data-target="#roles_information_modal">Information definieren
                            </button>
                            <input id="roles_information" name="roles_information" type="hidden" class="form-control"
                                   value="<?php echo $data['roles_information_id']; ?>">
                            <ul id="informationUI" class="list-group">
                                <?php
                                $roleInformation = $data['roles_information'];
                                if ($roleInformation && array_key_exists('user', (array)$roleInformation[0]) && array_key_exists('organ', (array)$roleInformation[0])) :
                                    ?>

                                    <?php foreach ($data['roles_information'] as $role) : ?>

                                    <?php if (!empty($role['suborgan'])) : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id . '_' . $role['suborgan']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['suborgan']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php else : ?>

                                        <li class="list-group-item d-flex"
                                            data-id="<?php echo $role['user']->id . '_' . $role['organ']->id; ?>">
                                            <p class="mb-0">
                                                <strong><?php echo $role['user']->firstname . ' ' . $role['user']->lastname; ?></strong>
                                                (<?php echo $role['organ']->name; ?>)
                                            </p>
                                            <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
                                        </li>

                                    <?php endif; ?>

                                <?php endforeach; ?>

                                <?php else : ?>
                                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine
                                        Zuteilung
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 mt-3">
                    <h2 class="h6">ISO-Normen:</h2>
                    <select name="iso[]" id="selectIso" class="form-control" multiple required>
                        <option value="none">keine</option>
                        <?php foreach ($data['isoList'] as $iso): ?>
                            <option
                                    value="<?= $iso->id; ?>"
                                    <?=
                                    in_array($iso->norm, explode(', ', $data['taskIsos']))
                                        ? 'selected'
                                        : '';
                                    ?>
                            >
                                <?= $iso->norm; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="form-group col-md-12 my-3">
                    <div class="row">

                        <div class="col-md-6">
                            <h2 class="h6">Erforderliche Kompetenzen:</h2>

                            <select name="qualification[]" id="selectQualification" class="form-control" multiple required>
                                <option value="none">keine</option>
                                <?php foreach ($data['qualificationList'] as $qualification): ?>
                                    <option
                                            value="<?= $qualification->id; ?>"
                                            <?=
                                            in_array($qualification->name, explode(', ', $data['taskQualifications']))
                                                ? 'selected'
                                                : '';
                                            ?>
                                    >
                                        <?= $qualification->name; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <div id="stored_knowledge" class="col-md-6">

                            <input id="required_knowledge" name="required_knowledge" type="hidden" class="form-control"
                                   value="<?php echo $data['required_knowledge']; ?>">
                            <input id="required_knowledge_folders" name="required_knowledge_folders" type="hidden"
                                   class="form-control" value="<?php echo $data['required_knowledge_folders']; ?>">
                            <span class="invalid-feedback"><?php echo $data['required_knowledge_err']; ?></span>
                            </button>

                            <p class="h5">Zugeteiltes Wissen
                                <button id="open_knowledge_browser" type="button" class="btn btn-pe-darkgreen ml-2"
                                        data-toggle="modal" data-target="#knowledge_browser">
                                    Dateien anhängen
                                </button>
                            </p>
                            <ul class="list-group">
                                <?php if (!empty($data['required_knowledge_UI'])) : ?>

                                    <?php foreach ($data['required_knowledge_UI'] as $item) : ?>
                                        <li class="list-group-item list-group-item-secondary d-flex align-items-center file"
                                            data-path="<?php echo $item['path']; ?>">
                                            <i class="fas fa-file mr-2"></i>
                                            <?php echo $item['name']; ?>
                                            <i class="close-btn fas fa-times ml-auto"></i>
                                        </li>
                                    <?php endforeach; ?>

                                <?php endif; ?>

                                <?php if (!empty($data['required_knowledge_folders_UI'])) : ?>

                                    <?php foreach ($data['required_knowledge_folders_UI'] as $item) : ?>
                                        <li class="list-group-item list-group-item-secondary d-flex align-items-center folder"
                                            data-path="<?php echo $item['folderPath']; ?>">
                                            <i class="fas fa-folder mr-2"></i>
                                            <?php echo $item['folderName']; ?>
                                            <i class="close-btn fas fa-times ml-auto"></i>
                                        </li>
                                    <?php endforeach; ?>

                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="form-group col-md-12 mt-4">
                    <div class="row">

                        <div class="col-md-6">
                            <p class="h4">Erforderliche Ressourcen:</p>
                            <textarea name="required_resources_text" id="required_resources_text"
                                      class="form-control mb-3" cols="30"
                                      rows="3"><?php echo $data['required_resources_text']; ?></textarea>
                        </div>

                        <div id="stored_resources" class="col-md-6">

                            <input id="required_resources" name="required_resources" type="hidden" class="form-control"
                                   value="<?php echo $data['required_resources']; ?>">
                            <input id="required_resources_folders" name="required_resources_folders" type="hidden"
                                   class="form-control" value="<?php echo $data['required_resources_folders']; ?>">
                            <span class="invalid-feedback"><?php echo $data['required_resources_err']; ?></span>


                            <p class="h5">Zugeteilte Ressourcen
                                <button id="open_resource_browser" type="button" class="btn btn-pe-darkgreen ml-2"
                                        data-toggle="modal" data-target="#resource_browser">
                                    Dateien anhängen
                                </button>
                            </p>
                            <ul class="list-group">
                                <?php if (!empty($data['required_resources_UI'])) : ?>

                                    <?php foreach ($data['required_resources_UI'] as $item) : ?>
                                        <li class="list-group-item list-group-item-secondary d-flex align-items-center file"
                                            data-path="<?php echo $item['path']; ?>">
                                            <i class="fas fa-file mr-2"></i>
                                            <?php echo $item['name']; ?>
                                            <i class="close-btn fas fa-times ml-auto"></i>
                                        </li>
                                    <?php endforeach; ?>

                                <?php endif; ?>

                                <?php if (!empty($data['required_resources_folders_UI'])) : ?>

                                    <?php foreach ($data['required_resources_folders_UI'] as $item) : ?>
                                        <li class="list-group-item list-group-item-secondary d-flex align-items-center folder"
                                            data-path="<?php echo $item['folderPath']; ?>">
                                            <i class="fas fa-folder mr-2"></i>
                                            <?php echo $item['folderName']; ?>
                                            <i class="close-btn fas fa-times ml-auto"></i>
                                        </li>
                                    <?php endforeach; ?>

                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>


                <div class="form-group col-md-12 mt-3">
                    <div class="row">
                        <div class="col-12">
                            <p class="h4 px-3 py-2 bg-pe-darkgreen text-white">Wiederkehrende Check-Intervalle:
                                <sup>*</sup></p>
                        </div>

                    </div>
                </div>


                <div class="form-group col-md-12">
                    <div class="row">

                        <div class="col-md-4">
                            <h2 class="h5">Wiederkehrende Check-Intervalle:</h2>
                            <div class="form-check pl-0">
                                <input id="activate_intervals" name="activate_intervals" type="checkbox" value="none"
                                       class="mr-1" <?php echo ($data['activate_intervals']) ? 'checked' : ''; ?>>
                                <label for="activate_intervals" class="form-check-label">Check-Intervalle
                                    aktivieren</label>
                            </div>

                            <fieldset id="set_intervals" class="mt-3">
                                <label for="reference_date">Referenzdatum:</label>
                                <div class="form-row">
                                    <div class="col-4">
                                        <label for="reference_day">Tag</label>
                                        <input name="reference_day" type="number"
                                               class="form-control <?php echo (!empty($data['reference_date_err'])) ? 'is-invalid' : ''; ?>"
                                               min="1" max="31" value="<?php echo $data['reference_day']; ?>">
                                    </div>
                                    <div class="col-4">
                                        <label for="reference_month">Monat</label>
                                        <input name="reference_month" type="number"
                                               class="form-control <?php echo (!empty($data['reference_date_err'])) ? 'is-invalid' : ''; ?>"
                                               min="1" max="12" value="<?php echo $data['reference_month']; ?>">
                                    </div>
                                    <div class="col-4">
                                        <label for="reference_year">Jahr</label>
                                        <input name="reference_year" type="number"
                                               class="form-control <?php echo (!empty($data['reference_date_err'])) ? 'is-invalid' : ''; ?>"
                                               min="1900" max="3000" value="<?php echo $data['reference_year']; ?>">
                                    </div>
                                    <span class="invalid-feedback"><?php echo $data['reference_date_err']; ?></span>
                                </div>

                                <label class="mt-2"> Intervallzahl</label>
                                <input id="interval_count" name="interval_count" type="number" min="1" max="365"
                                       class="form-control mb-2 <?php echo (!empty($data['check_intervals_err'])) ? 'is-invalid' : ''; ?>"
                                       value="<?php echo $data['interval_count']; ?>"
                                       placeholder="Intervallzahl auswählen">

                                <select id="interval_format" name="interval_format"
                                        class="form-control <?php echo (!empty($data['check_intervals_err'])) ? 'is-invalid' : ''; ?>">
                                    <option value="empty">Intervallformat auswählen</option>
                                    <option <?php echo $data['interval_format'] == 'Tages-Rhythmus' ? 'selected' : ''; ?>>
                                        Tages-Rhythmus
                                    </option>
                                    <option <?php echo $data['interval_format'] == 'Wochen-Rhythmus' ? 'selected' : ''; ?>>
                                        Wochen-Rhythmus
                                    </option>
                                    <option <?php echo $data['interval_format'] == 'Monats-Rhythmus' ? 'selected' : ''; ?>>
                                        Monats-Rhythmus
                                    </option>
                                    <option <?php echo $data['interval_format'] == 'Jahres-Rhythmus' ? 'selected' : ''; ?>>
                                        Jahres-Rhythmus
                                    </option>
                                </select>
                                <span class="invalid-feedback"><?php echo $data['check_intervals_err']; ?></span>
                            </fieldset>
                        </div>

                        <div class="col-md-8">
                            <h2 class="h5">Erinnerungsfunktion</h2>
                            <p><strong>Vorraussetzung:</strong> ein Check-Intervall muss definiert sein.</p>
                            <input id="activate_reminders" name="activate_reminders" type="checkbox"
                                   value="activated" <?php echo ($data['reminders_activated']) ? 'checked' : ''; ?>>
                            <label for="activate_reminders" class="form-check-label">Erinnerungen aktivieren</label>

                            <fieldset id="set_reminders"
                                      class="mt-2 <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                      disabled="disabled">
                                <div class="form-row">
                                    <div class="col-2 my-3">
                                        <input id="remind_before" name="remind_before" type="number"
                                               class="form-control <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                               min='0' max='100' value="<?php echo $data['remind_before']; ?>">
                                    </div>
                                    <div class="col-10 my-3">
                                        <label for="remind_before" class="form-check-label">Tage vor Stichtag
                                            erinnern</label>
                                    </div>

                                    <div class="col-md-12">
                                        <input id="remind_at" name="remind_at" type="checkbox"
                                               class="mt-1 <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                               value="activated" <?php echo ($data['remind_at']) ? 'checked' : ''; ?>>
                                        <label for="remind_at" class="ml-2 form-check-label">Zum Stichtag
                                            erinnern</label>
                                    </div>

                                    <div class="col-2 my-3">
                                        <input id="remind_after" name="remind_after" type="number"
                                               class="form-control <?php echo (!empty($data['reminders_err'])) ? 'is-invalid' : ''; ?>"
                                               min='0' max='100' value="<?php echo $data['remind_after']; ?>">
                                    </div>
                                    <div class="col-10 my-3">
                                        <label for="remind_after" class="form-check-label" min='1' max='100'>Tage nach
                                            Stichtag erinnern</label>
                                    </div>
                                </div>
                                <span class="invalid-feedback"><?php echo $data['reminders_err']; ?></span>
                            </fieldset>
                        </div>


                        <div class="form-group col-md-4 mt-3">
                            <p class="h5">bestehende Kompetenzen:</p>
                            <input name="existing_compentencies" type="text"
                                   class="form-control <?php echo (!empty($data['existing_compentencies_err'])) ? 'is-invalid' : ''; ?>"
                                   value="<?php echo $data['existing_compentencies']; ?>"
                                   placeholder="bestehende Kompetenzen">
                            <span class="invalid-feedback"><?php echo $data['existing_compentencies_err']; ?></span>
                        </div>
                        <div class="form-group col-md-4 mt-3">
                            <p class="h5">erforderlicher Weiterbildungsbedarf:</p>
                            <input name="required_education" type="text"
                                   class="form-control <?php echo (!empty($data['required_education_err'])) ? 'is-invalid' : ''; ?>"
                                   value="<?php echo $data['required_education']; ?>"
                                   placeholder="erforderlicher Weiterbildungsbedarf">
                            <span class="invalid-feedback"><?php echo $data['required_education_err']; ?></span>
                        </div>

                        <div class="form-group col-md-4 mt-3">
                            <p class="h5">Schlagwörter:</p>
                            <p>Schlagwörter bitte mit Komma trennen.</p>
                            <div class="tags-input" data-name="tags-input">
                                <?php foreach ($data['tags_array'] as $tag): ?>
                                    <?php if (!empty($tag)) : ?>
                                        <span class="tag"><?php echo $tag; ?> <i class="close fas fa-times"></i></span>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                <input type="text" class="mainInput">
                                <input name="tags" type="hidden" class="hiddenInput"
                                       value="<?php echo $data['taglist']; ?>">
                            </div>
                        </div>

                        <!--<?php /*
                        <div class="col-md-12">
                            <h2 class="h4 px-3 py-2 bg-pe-darkgreen text-white">Flexibles Abfragesystem</h2>
                            <h5>Angewendete Fragen</h5>
                            <input id="flexible_questions" name="flexible_questions" type="hidden"
                                   value="<?php echo $data['flexible_questions']; ?>">
                            <div id="added_questions">
                                <ul class="list-group my-3">

                                    <?php if (!empty($data['flexible_questions_UI'])) : ?>

                                        <?php foreach ($data['flexible_questions_UI'] as $question) : ?>

                                            <li class="list-group-item list-group-item-secondary d-flex align-items-center folder"
                                                data-qstring="<?php echo $question['string_UI']; ?>">
                                                <p class="mb-0">
                                                    <strong>Frage:</strong> <?php echo $question['question']; ?></p>
                                                <p class="mb-0 ml-2">
                                                    <strong>Antwortmöglichkeiten:</strong> <?php echo $question['answers_UI']; ?>
                                                </p>
                                                <i class="close-btn fas fa-times ml-auto"></i>
                                            </li>

                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                </ul>
                                <button id="open_question_modal" type="button" class="btn btn-pe-lightgreen"
                                        data-toggle="modal" data-target="#question_modal">
                                    Abfrage hinzufügen
                                </button>
                                </button>
                            </div>
                        </div>
                        */?>-->

                    </div> <!-- row -->
                    <input type="submit" class="btn btn-pe-darkgreen mt-3 float-right" value="Aktualisieren">
        </form>
    </div>


    <!--<?php /*
    <!-- Modal: Flexible Questions -->
    <div id="question_modal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Flexibles Abfragesystem</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <label for="question">Frage:</label>
                    <input id="question" name="question" type="text" class="form-control" value=""
                           placeholder="Bsp: Willst du geschult werden?">

                    <p class="h5 mt-3">Antwortmöglichkeiten auswählen</p>

                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="yes_no_checkbox">
                        <label class="form-check-label" for="yes_no_checkbox">Ja oder Nein</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="notify_checkbox">
                        <label class="form-check-label" for="notify_checkbox">Benachrichtigung von Mitarbeitern mit
                            Verfassungsmöglichkeit einer Nachricht</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="checkbox" id="remind_checkbox">
                        <label class="form-check-label" for="remind_checkbox">Erinnerungsfunktion mit Datums- bzw.
                            Intervallauswahl</label>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>
    */?>-->

    <!-- Modal: Knowledge Browser -->
    <div id="knowledge_browser" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Dateibrowser</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="folders"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Resource Browser -->
    <div id="resource_browser" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-dialog-centered modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Dateibrowser</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="folders"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Roles - Responsible  -->
    <div id="roles_responsible_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nutzer der Rolle "Verantwortlichkeit" bearbeiten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                    <div class="modal-list">

                        <?php if (!empty($data['organs'])) : ?>

                            <div class="accordion" id="accordionExample">

                                <?php foreach ($data['organs'] as $organ) : ?>

                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link w-100 text-left" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#responsible-<?php echo $organ->id; ?>"
                                                        aria-expanded="true"
                                                        aria-controls="responsible-<?php echo $organ->id; ?>">
                                                    <?php echo $organ->name; ?>
                                                </button>
                                            </h2>
                                        </div>

                                        <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                             aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">

                                                <?php /* ############################################### */ ?>
                                                <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                                <?php if (!empty($organ->id == 2)) : ?>

                                                    <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                        <?php foreach ($suborgan->users as $user) : ?>

                                                            <div class="form-check">
                                                                <input class="form-check-input"
                                                                       type="checkbox"
                                                                       value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                       name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                       data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                       data-organ="<?php echo $suborgan->name;
                                                                       ?>">
                                                                <label class="form-check-label"
                                                                       for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                            </div>

                                                        <?php endforeach; ?>

                                                    <?php endforeach; ?>

                                                <?php else : ?>

                                                    <?php /* ############################################### */ ?>
                                                    <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                    <?php foreach ($organ->users as $user) : ?>
                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $organ->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endif; ?>


                                            </div>
                                        </div>
                                    </div><!-- end of card -->

                                <?php endforeach; ?>

                            </div><!-- end of accordion -->

                        <?php endif; ?>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Roles - Concerned -->
    <div id="roles_concerned_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nutzer der Rolle "Zuständigkeit" bearbeiten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                    <div class="modal-list">

                        <?php if (!empty($data['organs'])) : ?>

                            <div class="accordion" id="accordionExample">

                                <?php foreach ($data['organs'] as $organ) : ?>

                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link w-100 text-left" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#responsible-<?php echo $organ->id; ?>"
                                                        aria-expanded="true"
                                                        aria-controls="responsible-<?php echo $organ->id; ?>">
                                                    <?php echo $organ->name; ?>
                                                </button>
                                            </h2>
                                        </div>

                                        <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                             aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">

                                                <?php /* ############################################### */ ?>
                                                <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                                <?php if (!empty($organ->id == 2)) : ?>

                                                    <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                        <?php foreach ($suborgan->users as $user) : ?>

                                                            <div class="form-check">
                                                                <input class="form-check-input"
                                                                       type="checkbox"
                                                                       value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                       name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                       data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                       data-organ="<?php echo $suborgan->name;
                                                                       ?>">
                                                                <label class="form-check-label"
                                                                       for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                            </div>

                                                        <?php endforeach; ?>

                                                    <?php endforeach; ?>

                                                <?php else : ?>

                                                    <?php /* ############################################### */ ?>
                                                    <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                    <?php foreach ($organ->users as $user) : ?>
                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $organ->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endif; ?>


                                            </div>
                                        </div>
                                    </div><!-- end of card -->

                                <?php endforeach; ?>

                            </div><!-- end of accordion -->

                        <?php endif; ?>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Roles - Contributing -->
    <div id="roles_contributing_modal" class="modal fade" tabindex="-1" role="dialog"
         aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nutzer der Rolle "Mitwirkung" bearbeiten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                    <div class="modal-list">

                        <?php if (!empty($data['organs'])) : ?>

                            <div class="accordion" id="accordionExample">

                                <?php foreach ($data['organs'] as $organ) : ?>

                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link w-100 text-left" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#responsible-<?php echo $organ->id; ?>"
                                                        aria-expanded="true"
                                                        aria-controls="responsible-<?php echo $organ->id; ?>">
                                                    <?php echo $organ->name; ?>
                                                </button>
                                            </h2>
                                        </div>

                                        <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                             aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">

                                                <?php /* ############################################### */ ?>
                                                <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                                <?php if (!empty($organ->id == 2)) : ?>

                                                    <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                        <?php foreach ($suborgan->users as $user) : ?>

                                                            <div class="form-check">
                                                                <input class="form-check-input"
                                                                       type="checkbox"
                                                                       value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                       name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                       data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                       data-organ="<?php echo $suborgan->name;
                                                                       ?>">
                                                                <label class="form-check-label"
                                                                       for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                            </div>

                                                        <?php endforeach; ?>

                                                    <?php endforeach; ?>

                                                <?php else : ?>

                                                    <?php /* ############################################### */ ?>
                                                    <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                    <?php foreach ($organ->users as $user) : ?>
                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $organ->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endif; ?>


                                            </div>
                                        </div>
                                    </div><!-- end of card -->

                                <?php endforeach; ?>

                            </div><!-- end of accordion -->

                        <?php endif; ?>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal: Roles - Information -->
    <div id="roles_information_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Nutzer der Rolle "Information" bearbeiten</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
                    <div class="modal-list">

                        <?php if (!empty($data['organs'])) : ?>

                            <div class="accordion" id="accordionExample">

                                <?php foreach ($data['organs'] as $organ) : ?>

                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link w-100 text-left" type="button"
                                                        data-toggle="collapse"
                                                        data-target="#responsible-<?php echo $organ->id; ?>"
                                                        aria-expanded="true"
                                                        aria-controls="responsible-<?php echo $organ->id; ?>">
                                                    <?php echo $organ->name; ?>
                                                </button>
                                            </h2>
                                        </div>

                                        <div id="responsible-<?php echo $organ->id; ?>" class="collapse"
                                             aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">

                                                <?php /* ############################################### */ ?>
                                                <?php /* ############### STAFF FUNCTIONS ############### */ ?>

                                                <?php if (!empty($organ->id == 2)) : ?>

                                                    <?php foreach ($organ->suborgans as $suborgan) : ?>

                                                        <?php foreach ($suborgan->users as $user) : ?>

                                                            <div class="form-check">
                                                                <input class="form-check-input"
                                                                       type="checkbox"
                                                                       value="<?php echo $user->id . '_' . $organ->id . '_' . $suborgan->id; ?>"
                                                                       name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                       data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                       data-organ="<?php echo $suborgan->name;
                                                                       ?>">
                                                                <label class="form-check-label"
                                                                       for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname . ' (' . $suborgan->name . ')'; ?></label>
                                                            </div>

                                                        <?php endforeach; ?>

                                                    <?php endforeach; ?>

                                                <?php else : ?>

                                                    <?php /* ############################################### */ ?>
                                                    <?php /* ################ REGULAR ORGANS ############### */ ?>

                                                    <?php foreach ($organ->users as $user) : ?>
                                                        <div class="form-check">
                                                            <input class="form-check-input"
                                                                   type="checkbox"
                                                                   value="<?php echo $user->id . '_' . $organ->id; ?>"
                                                                   name="<?php echo $user->lastname . '' . $user->id; ?>"
                                                                   data-name="<?php echo $user->firstname . ' ' . $user->lastname; ?>"
                                                                   data-organ="<?php echo $organ->name;
                                                                   ?>">
                                                            <label class="form-check-label"
                                                                   for="<?php echo $user->lastname . '' . $id; ?>"><?php echo $user->firstname . ' ' . $user->lastname; ?></label>
                                                        </div>

                                                    <?php endforeach; ?>

                                                <?php endif; ?>


                                            </div>
                                        </div>
                                    </div><!-- end of card -->

                                <?php endforeach; ?>

                            </div><!-- end of accordion -->

                        <?php endif; ?>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
                </div>
            </div>
        </div>
    </div>

</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
