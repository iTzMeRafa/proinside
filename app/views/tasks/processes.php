<?php require APPROOT . '/views/inc/header.php'; ?>
<?php displayFlash('task_message'); ?>
<?php displayFlash('no_permisssion'); ?>
<div class="row mb-3">

    <div class="col-md-6">
        <h1 class="btn btn-pe-lightgreen"><i class="fas fa-tasks mr-2"></i>Einzelaufgaben</h1>
    </div>
    <div class="col-md-6">
        <a href="<?php echo URLROOT; ?>/tasks/add" class="btn btn-pe-lightgreen float-right">
            <i class="fas fa-pencil-alt mr-2"></i> Neue Einzelaufgabe erstellen
        </a>
    </div>
    <div class="col-12 mt-2 mb-3">
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/tasks">Aktiv</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/tasks/archived">Archiviert</a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" href="<?php echo URLROOT; ?>/tasks/processes">Prozessaufgaben</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/tasks/single">Einzelaufgaben</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo URLROOT; ?>/tasks/all">Alle</a>
            </li>
        </ul>
    </div>

    <!-- Sortable Table -->
    <div class="col-md-12">
        <table id="taskRechtswelt" class="table table-striped table-bordered" style="width: 100%">
            <thead>
            <tr style="background-color: #0B676E; color: white; border: none;">
                <th style="border: none;">ID</th>
                <th style="border: none;">Name</th>
                <th style="border: none;">Zugehörigkeit</th>
                <th style="border: none;">Kategorie</th>
                <th style="border: none;">Unternehmensbereich</th>
                <th style="border: none;">Status</th>
                <th style="border: none;">Check Intervall</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($data['tasks'] as $task) : ?>
                <tr>
                    <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task->taskId; ?>">A<?= $task->taskId; ?></a></td>
                    <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task->taskId; ?>"><?= $task->taskName; ?></a></td>
                    <td><?= $task->process_name === null ? 'Einzelaufgabe' : '<a href="'.URLROOT.'/processes/show/'.$task->process_id.'">'.$task->process_name.'</a>'; ?></td>
                    <td><?= $task->category; ?></td>
                    <td><?= $task->organname; ?></td>
                    <td><?= $task->status; ?></td>
                    <td><?= $task->check_intervals; ?></td>

                </tr>
            <?php endforeach; ?>

            </tbody>
        </table>
    </div>
    <!-- /Sortable Table -->

</div>



<?php require APPROOT . '/views/inc/footer.php'; ?>
