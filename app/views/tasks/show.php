<?php require APPROOT . '/views/inc/header.php'; ?>

    <?php require APPROOT . '/views/tasks/show/header.php'; ?>

    <?php displayFlash('task_message'); ?>
    <?php displayFlash('no_permisssion'); ?>

    <?php require APPROOT . '/views/tasks/show/alerts.php'; ?>

    <div class="card">
        <?php require APPROOT . '/views/tasks/show/cardHeader.php'; ?>
        <?php require APPROOT . '/views/tasks/show/cardBody.php'; ?>
    </div>


<?php require APPROOT . '/views/inc/footer.php'; ?>
