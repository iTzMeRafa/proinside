<!-- Task is not accepted yet -->
<?php if ($data['task']->is_approved == 'false' && $data['task']->is_singletask == 'false') : ?>
    <div class="alert alert-warning" role="alert">
        Diese Prozessaufgabe ist noch nicht freigegeben.
    </div>
<?php endif; ?>

<!-- Task assign not accepted yet -->
<?php if ($data['non_confirming_users']) : ?>
    <?php foreach ($data['non_confirming_users'] as $user) : ?>
        <?php if ($user['user_id'] == $_SESSION['user_id']) : ?>
            <div class="alert alert-warning" role="alert">
                <h4 class="alert-heading">Unakzeptierte Zuteilung!</h4>
                <p>Sie habe Ihre Zuteilung zu dieser Aufgabe in der Rolle <strong>"<?php echo $user['role']; ?>"</strong> noch nicht akzeptiert. Bitte schauen Sie sich die Aufgabe an und entscheiden sich, ob Sie diese akzeptieren wollen.</p>
                <hr>
                <button data-user="<?php echo $user['user_id']; ?>" data-task="<?php echo $data['task']->id; ?>" data-role="<?php echo $user['role_js']; ?>" type="button" class="confirm_assignment btn btn-success"><i class="fas fa-check"></i> Aufgabe akzeptieren</button>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
<?php endif; ?>

<!-- Escalation -->
<?php if ($data['checkdate'] != false && $data['next_checkdate']['isWhen'] == 'past' && $data['task']->is_archived != 'true') : ?>
    <div class="alert alert-danger" role="alert">
        <h4 class="alert-heading">Aufgaben Check überfällig!</h4>
        <p>Der Check dieser Aufgabe ist <?php echo $data['next_checkdate']['msg']; ?> überfällig.<br> Das planmäßige Checkdatum war am <?php echo $data['checkdate']['day'] .'. '. $data['checkdate']['month'] .' '. $data['checkdate']['year']; ?></p>
    </div>
<?php endif; ?>

<!-- Task must be checked today -->
<?php if ($data['checkdate'] != false && $data['privUser']->hasPrivilege('15') && $data['task']->is_archived != 'true') : ?>
    <?php if ($data['next_checkdate']['isWhen'] == 'past' || $data['next_checkdate']['isWhen'] == 'today') : ?>
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Diese Aufgabe muss heute gecheckt werden.</h4>
            <p>Bitte schauen Sie sich die Aufgabe genau an und schreiben einen angemessenen Kommentar zu Ihrem Check.</p>
            <div class="form-group">
                <textarea id="check_comment" class="form-control <?php echo (!empty($data['check_intervals_err'])) ? 'is-invalid' : ''; ?>" rows="3"></textarea>
                <span class="invalid-feedback"><?php echo $data['check_comment_err']; ?></span>
            </div>
            <button id="check_task" data-user="<?php echo $_SESSION['user_id']; ?>" data-task="<?php echo $data['task']->id; ?>" data-check-date="<?php echo $data['task']->check_date; ?>" data-format="<?php echo $data['task']->interval_format; ?>" data-count="<?php echo $data['task']->interval_count; ?>" type="button" class="btn btn-success"><i class="fas fa-check"></i> Check bestätigen</button>
        </div>
    <?php endif; ?>
<?php endif; ?>

<!-- Task archived -->
<?php if ($data['task']->is_archived == 'true') : ?>
    <div class="alert alert-danger" role="alert">
        Die Aufgabe ist inaktiv und befindet sich im Archiv.
    </div>
<?php endif; ?>

<!-- Next Check Reminder Date -->
<?php if ($data['checkdate'] != false && $data['next_checkdate']['isWhen'] == 'future') : ?>
    <div class="alert alert-success" role="alert">
        Der nächste Check dieser Aufgabe steht <?php echo $data['next_checkdate']['msg']; ?> an.<br> Checkdatum: <?php echo $data['checkdate']['day'] .'. '. $data['checkdate']['month'] .' '. $data['checkdate']['year']; ?>
        <div class="mt-2">
            <p class="mb-0"><strong>Erinnerungen:</strong>
                <?php echo ($data['task']->remind_before != null) ? $data['task']->remind_before.' Tage im Vorraus,' : ''; ?>
                <?php echo ($data['task']->remind_at != null) ? ' am Stichtag,' : ''; ?>
                <?php echo ($data['task']->remind_after != null) ? $data['task']->remind_after.' Tage im Nachhinein' : ''; ?>
            </p>
        </div>
    </div>
<?php endif; ?>
