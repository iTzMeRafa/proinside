<?php
    if (
        $data['privUser']->hasPrivilege('3') == true &&
        $data['task']->is_archived != 'true' &&
        $data['task']->is_singletask == 'true'
    ) : ?>
    <form class="float-right" action="<?php echo URLROOT; ?>/tasks/delete/<?php echo $data['task']->id; ?>" method="post">
        <input type="submit" class="btn btn-danger" value="Löschen 	&#40;Archivieren&#41;">
    </form>
<?php endif; ?>