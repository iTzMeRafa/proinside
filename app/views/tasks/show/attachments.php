<h5>Anhänge und bereitgestelltes Wissen</h5>
<div class="row mb-4">

    <div class="col-md-6">
        <?php require APPROOT . '/views/tasks/show/required_knowledge.php'; ?>

    </div>
    <div class="col-md-6">
        <?php require APPROOT . '/views/tasks/show/required_ressources.php'; ?>
    </div>

</div>