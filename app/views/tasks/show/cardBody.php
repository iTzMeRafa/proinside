<div class="card-body">

    <?php require APPROOT . '/views/tasks/show/meta.php'; ?>

    <?php require APPROOT . '/views/tasks/show/infoBoxes.php'; ?>

    <?php require APPROOT . '/views/tasks/show/roles.php'; ?>

    <?php require APPROOT . '/views/tasks/show/attachments.php'; ?>

    <?php require APPROOT . '/views/tasks/show/checks.php'; ?>

</div>