<h5 class="card-header" style="display: flex; align-items: center;">
    <div class="col-md-8">
        <?= 'A'.$data['task']->id.': '.$data['task']->name; ?>
    </div>
    <div class="col-md-4">
                <span class="float-right ml-3">
                    <?php require APPROOT . '/views/tasks/show/processPart.php'; ?>
                </span>
    </div>
</h5>