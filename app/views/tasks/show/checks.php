<h5>Checks</h5>
<div class="row mt-3">
    <div class="col-md-12">
        <?php if (!empty($data['checks'])) : ?>
            <?php foreach ($data['checks'] as $check) : ?>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $check['checked_at']['day'] .'. '. $check['checked_at']['month'] .' '. $check['checked_at']['year'] .' um '. $check['checked_at']['hour'] .':'. $check['checked_at']['min'] .' Uhr'; ?></h5>
                        <h6 class="card-subtitle mb-2 text-muted">Gecheckt von
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $check['user']->id; ?>" class="text-success"><?php echo $check['user']->firstname .' '. $check['user']->name; ?></a>
                        </h6>
                        <p class="card-text"><?php echo $check['comment']; ?></p>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p><i class="fas fa-exclamation-triangle"></i> Es wurden bisher noch keine Checks durchgeführt.</p>
        <?php endif; ?>
    </div>
</div>