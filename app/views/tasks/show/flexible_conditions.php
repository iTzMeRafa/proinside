<!--<?php /*
    <div class="col-md-12 mt-3">
        <h2>Flexible Abfragen</h2>
        <?php if (!empty($data['questions'])) : ?>
            <?php foreach ($data['questions'] as $question) : ?>
                <div class="alert alert-secondary" role="alert">
                    <p class="h4"><?php echo $question->question; ?></p>

                    <?php if ($question->yes_no == 'true') : ?>
                        <div class="yes_no answer mt-3">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="question<?php echo $question->id; ?>" id="question<?php echo $question->id; ?>yes" value="true">
                                <label class="form-check-label" for="question<?php echo $question->id; ?>yes">Ja</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="question<?php echo $question->id; ?>" id="question<?php echo $question->id; ?>no" value="false">
                                <label class="form-check-label" for="question<?php echo $question->id; ?>no">Nein</label>
                            </div>
                            <span class="yesno-error d-block"></span>
                        </div>
                    <?php endif; ?>

                    <?php if ($question->notify == 'true') : ?>
                        <div class="notify answer mt-3">
                            <p class="h6">Welche Mitarbeiter möchten Sie benachrichtigen?</p>
                            <button id="open_coworkers_modal" type="button" class="coworkers-btn btn btn-primary my-3" data-toggle="modal" data-target="#coworkers_browser" data-id="<?php echo $question->id; ?>">Mitarbeiter auswählen</button>
                            <span class="coworkers-error d-block"></span>
                            <div class="stored-coworkers">
                                <input id="notifyUsersQ-<?php echo $question->id; ?>" class="userIds" type="hidden">
                                <ul id="q-<?php echo $question->id; ?>-notifyUsers" class="list-group my-3">
                                </ul>
                            </div>
                            <p class="h6">Ihre Nachricht</p>
                            <textarea class="message form-control" id="textarea-q-<?php echo $question->id; ?>" rows="3"></textarea>
                            <span class="msg-error d-block"></span>
                        </div>
                    <?php endif; ?>

                    <?php if ($question->remind == 'true') : ?>
                        <div class="remind answer mt-3">
                            <p class="h6">Zu welchem Datum möchten Sie eine Erinnerung schalten?</p>
                            <div class="remind-date form-row">
                                <div class="col-4">
                                    <label for="remind_day">Tag</label>
                                    <input name="remind_day" type="number" class="remind_day form-control" min="1" max="31" value="">
                                </div>
                                <div class="col-4">
                                    <label for="remind_month">Monat</label>
                                    <input name="remind_month" type="number" class="remind_month form-control" min="1" max="12" value="">
                                </div>
                                <div class="col-4">
                                    <label for="remind_year">Jahr</label>
                                    <input name="remind_year" type="number" class="remind_year form-control" min="2019" max="3000" value="">
                                </div>
                            </div>
                            <span class="remind-error d-block"></span>
                        </div>
                    <?php endif; ?>
                    <button type="button" class="submit-answer btn btn-outline-info mt-3">Antwort absenden</button>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p><i class="fas fa-exclamation-triangle"></i> Es wurden keine individuellen Fragen gestellt.</p>
        <?php endif; ?>
    </div>
    */ ?>-->