<!-- Title -->
<div class="pageTitle flexCenterContainer">
    <div class="pageTitle__title ">
        <i class="fas fa-tasks mr-2"></i> Aufgabe: Ansehen
    </div>
</div>

<!-- Navigation -->
<div class="row mb-5">
    <div class="col-12">

        <a href="javascript:history.go(-1)" class="h1 btn btn-pe-lightgreen">
            <i class="fa fa-chevron-left mr-2"></i> Zurück
        </a>

        <a href="<?php echo URLROOT; ?>/tasks/edit/<?php echo $data['task']->id; ?>" class="h1 btn btn-pe-lightgreen">
            <i class="fa fa-edit mr-2"></i> Bearbeiten
        </a>

    </div>
</div>