<h5>Aufgabeninfos</h5>
<div class="row mb-3">

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Input</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->input; ?></li>
            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Output</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->output; ?></li>
            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Kategorie</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->category; ?></li>
            </ul>
        </div>
    </div>

</div>

<div class="row mb-3">

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Inputquelle</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->input_source; ?></li>
            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Outputziel</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->output_target; ?></li>
            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Risikoeinstufung</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->risk; ?></li>
            </ul>
        </div>
    </div>

</div>

<div class="row mb-3">

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Status</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->status; ?></li>
            </ul>
        </div>
    </div>

    <?php if ($data['task']->is_singletask == 'true') : ?>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Unternehmensbereich</div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item"><?= $data['task']->department; ?></li>
                </ul>
            </div>
        </div>
    <?php endif; ?>

</div>

<div class="row mb-3">

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Wiederkehrende <br />Check-Intervalle</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <?php if (empty($data['task']->check_intervals) || $data['task']->check_intervals == 'Kein Check Intervall') : ?>
                    Kein Check Intervall
                    <?php else: ?>
                        im <?= $data['task']->check_intervals; ?> Referenzdatum <?= $data['reference_date']; ?>
                    <?php endif; ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Bestehende <br />Kompetenzen</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <?= $data['task']->existing_compentencies ?: 'Keine'; ?>
                </li>
            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="card">
            <div class="card-header">Erforderlicher <br />Weiterbildungsbedarf</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><?= $data['task']->required_education ?: 'Keine'; ?></li>
            </ul>
        </div>
    </div>

</div>
<div class="mb-5"></div>