<?php if (!empty($data['task']->isos)) : ?>
    <?php foreach (explode(', ', $data['task']->isos) as $iso) : ?>
        <?php echo '<span class="badge badge-secondary mx-1">'.$iso.'</span>'; ?>
    <?php endforeach; ?>
<?php else : ?>
    Keine ISO-Normen angegeben.
<?php endif; ?>