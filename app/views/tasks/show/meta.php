<div class="row mb-4">
    <div class="col-md-6">
        <?php require APPROOT . '/views/tasks/show/taskDescription.php'; ?>
    </div>
    <div class="col-md-6">
        <table class="table table-hover">
            <tbody>
            <tr>
                <th scope="row">Schlagwörter</th>
                <td>
                    <?php require APPROOT . '/views/tasks/show/tags.php'; ?>
                </td>
            </tr>
            <tr>
                <th scope="row">Kompetenzen</th>
                <td>
                    <?php require APPROOT . '/views/tasks/show/qualifications.php'; ?>
                </td>
            </tr>
            <tr>
                <th scope="row">ISO-Normen</th>
                <td>
                    <?php require APPROOT . '/views/tasks/show/isos.php'; ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>