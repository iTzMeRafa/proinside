<!-- Modal: Coworkers Browser -->
<div id="coworkers_browser" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Zugeteilte Mitarbeiter auswählen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-group">
                    <?php if ($data['roles_responsible']) : ?>
                        <li class="list-group-item active">Rolle: Verantwortlich</li>
                        <?php foreach ($data['roles_responsible'] as $role) : ?>
                            <li class="list-group-item">
                                <?php echo $role->firstname.' '.$role->name; ?>
                                <input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
                            </li>
                        <?php endforeach; ?>

                    <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                    <?php endif; ?>

                    <?php if ($data['roles_concerned']) : ?>
                        <li class="list-group-item active">Rolle: Zuständig</li>
                        <?php foreach ($data['roles_concerned'] as $role) : ?>
                            <li class="list-group-item">
                                <?php echo $role->firstname.' '.$role->name; ?>
                                <input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
                            </li>
                        <?php endforeach; ?>

                    <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                    <?php endif; ?>

                    <?php if ($data['roles_contributing']) : ?>
                        <li class="list-group-item active">Rolle: Mitwirkend</li>
                        <?php foreach ($data['roles_contributing'] as $role) : ?>
                            <li class="list-group-item">
                                <?php echo $role->firstname.' '.$role->name; ?>
                                <input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
                            </li>
                        <?php endforeach; ?>

                    <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                    <?php endif; ?>

                    <?php if ($data['roles_information']) : ?>
                        <li class="list-group-item active">Rolle: Information</li>
                        <?php foreach ($data['roles_information'] as $role) : ?>
                            <li class="list-group-item">
                                <?php echo $role->firstname.' '.$role->name; ?>
                                <input class="float-right" type="checkbox" value="<?php echo $role->id; ?>">
                            </li>
                        <?php endforeach; ?>

                    <?php else : ?>
                        <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                <button type="button" class="saveChanges btn btn-primary">Änderungen speichern</button>
            </div>
        </div>
    </div>
</div>