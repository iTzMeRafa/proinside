<!-- Teil des Prozesses -->
<?php if (!empty($data['task']->process_id) && $data['task']->is_singletask == false && !empty($data['process'])) : ?>
    <a
        href="<?= URLROOT; ?>/processes/show/<?php echo $data['task']->process_id; ?>"
        class="btn btn-pe-darkgreen ml-2"
    >
        <?= $data['process']->id.' - '.$data['process']->process_name; ?>
    </a>
<?php endif; ?>