<?php if (!empty($data['task']->qualifications)) : ?>
    <?php foreach (explode(', ', $data['task']->qualifications) as $qualification) : ?>
        <?php echo '<span class="badge badge-secondary mx-1">'.$qualification.'</span>'; ?>
    <?php endforeach; ?>
<?php else : ?>
    Keine Kompetenzen erforderlich.
<?php endif; ?>