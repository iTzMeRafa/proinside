<h6>Erforderliche Ressourcen</h6>

<?php if (!empty($data['task']->required_resources_text)) : ?>
    <div class="card">
        <div class="card-body">
            <?= $data['task']->required_resources_text; ?>
        </div>
    </div>
<?php endif; ?>

<?php if (!empty($data['required_resources']) || !empty($data['required_resources_folders'])) : ?>

    <?php if (!empty($data['required_resources'])) : ?>
        <h6 class="h6 d-block mt-3">Dateien</h6>
        <ul class="list-group">

            <?php foreach ($data['required_resources'] as $resource) : ?>
                <a href="<?php echo URLROOT.''.$resource['path']; ?>" class="list-group-item list-group-item-action" target="_blank">
                    <i class="fa fa-file"></i>
                    <?php echo $resource['name']; ?>
                </a>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <?php if (!empty($data['required_resources_folders'])) : ?>
        <h6 class="h4 d-block mt-3">Ordner</h6>
        <div class="row">

            <?php foreach ($data['required_resources_folders'] as $resource) : ?>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo $resource['folderName']; ?></h5>
                        </div>
                        <ul class="list-group list-group-flush">

                            <?php foreach ($resource['files'] as $file) : ?>
                                <a href="<?php echo URLROOT .'/'. $file['file_path']; ?>" class="list-group-item list-group-item-action" target="_blank"><i class="fa fa-file"></i> <?php echo $file['file_name']; ?></a>
                            <?php endforeach; ?>

                        </ul>
                    </div>
                </div>

            <?php endforeach; ?>
        </div>

    <?php endif; ?>

<?php else : ?>
    <p><i class="fas fa-exclamation-triangle"></i> Es wurden keine Dateien angehangen.</p>
<?php endif; ?>