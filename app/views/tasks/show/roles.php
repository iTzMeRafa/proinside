<h5>Funktionszuteilung</h5>
<div class="row mb-4">

    <!-- Verantwortlich -->
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">Verantwortlich</div>
            <ul class="list-group list-group-flush">
                <?php if ($data['roles_responsible']) : ?>

                    <?php foreach ($data['roles_responsible'] as $item) : ?>
                        <?php if ($item['user']) : ?>
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>" class="list-group-item list-group-item-action">

                                <?php if (!empty($item['user'])) : ?>
                                    <p class="mb-0"><strong><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></strong></p>
                                <?php endif; ?>
                                <?php if (!empty($item['suborgan'])) : ?>
                                    <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                <?php else : ?>
                                    <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>

                <?php else : ?>
                    <li class="list-group-item">
                        Keine Zuteilung
                    </li>
                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <!-- Zuständig -->
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">Zuständig</div>
            <ul class="list-group list-group-flush">
                <?php if ($data['roles_concerned']) : ?>

                    <?php foreach ($data['roles_concerned'] as $item) : ?>
                        <?php if ($item['user']) : ?>
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>" class="list-group-item list-group-item-action">

                                <?php if (!empty($item['user'])) : ?>
                                    <p class="mb-0"><strong><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></strong></p>
                                <?php endif; ?>
                                <?php if (!empty($item['suborgan'])) : ?>
                                    <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                <?php else : ?>
                                    <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>

                <?php else : ?>
                    <li class="list-group-item">
                        Keine Zuteilung
                    </li>
                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <!-- Mitwirkend -->
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">Mitwirkend</div>
            <ul class="list-group list-group-flush">
                <?php if ($data['roles_contributing']) : ?>

                    <?php foreach ($data['roles_contributing'] as $item) : ?>
                        <?php if ($item['user']) : ?>
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>" class="list-group-item list-group-item-action">

                                <?php if (!empty($item['user'])) : ?>
                                    <p class="mb-0"><strong><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></strong></p>
                                <?php endif; ?>
                                <?php if (!empty($item['suborgan'])) : ?>
                                    <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                <?php else : ?>
                                    <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>

                <?php else : ?>
                    <li class="list-group-item">
                        Keine Zuteilung
                    </li>
                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

    <!-- Informationen -->
    <div class="col-md-3">
        <div class="card">
            <div class="card-header">Informationen</div>
            <ul class="list-group list-group-flush">
                <?php if ($data['roles_information']) : ?>

                    <?php foreach ($data['roles_information'] as $item) : ?>
                        <?php if ($item['user']) : ?>
                            <a href="<?php echo URLROOT; ?>/users/show/<?php echo $item['user']->id; ?>" class="list-group-item list-group-item-action">

                                <?php if (!empty($item['user'])) : ?>
                                    <p class="mb-0"><strong><?php echo $item['user']->firstname.' '.$item['user']->lastname; ?></strong></p>
                                <?php endif; ?>
                                <?php if (!empty($item['suborgan'])) : ?>
                                    <p class="mb-0">( <?php echo $item['suborgan']->name; ?> )</p>
                                <?php else : ?>
                                    <p class="mb-0">( <?php echo $item['organ']->name; ?> )</p>
                                <?php endif; ?>
                            </a>
                        <?php endif; ?>
                    <?php endforeach; ?>

                <?php else : ?>
                    <li class="list-group-item">
                        Keine Zuteilung
                    </li>
                    <li class="list-group-item"><i class="fas fa-exclamation-triangle"></i> Keine Zuteilung</li>
                <?php endif; ?>
            </ul>
        </div>
    </div>

</div>
