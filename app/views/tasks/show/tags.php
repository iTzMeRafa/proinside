<?php if (!empty($data['tags']) && !empty($data['tags'][0])): ?>
    <?php foreach ($data['tags'] as $tag) : ?>
        <?= '<span class="badge badge-secondary mx-1">'.$tag.'</span>'; ?>
    <?php endforeach; ?>
<?php else : ?>
    Keine Tags hinzugefügt.
<?php endif; ?>