<?php require APPROOT . '/views/inc/header.php'; ?>
<?php displayFlash('user_message'); ?>
<?php displayFlash('no_permisssion'); ?>

<!-- Escalation Methods -->
<?php //require APPROOT . '/views/users/templates/escalationMethods.php';?>

<div class="row mb-3">

    <div class="col-md-12">

        <div class="row">

            <div class="col-md-6">
                <!-- Escalation Tasks -->
                <?php require APPROOT . '/views/users/templates/escalationTasks.php'; ?>

                <!-- Reminders -->
                <?php require APPROOT . '/views/users/templates/reminders.php'; ?>

                <!-- Unreleased Files -->
                <?php require APPROOT . '/views/users/templates/unreleasedFiles.php'; ?>

                <!-- Approved Files -->
                <?php require APPROOT . '/views/users/templates/approvedFiles.php'; ?>
            </div>

            <div class="col-md-6">
                <!-- Unaccepted Task -->
                <?php require APPROOT . '/views/users/templates/unacceptedTask.php'; ?>

                <!-- Unaccepted Methods -->
                <?php require APPROOT . '/views/users/templates/unacceptedMethods.php'; ?>

                <!-- Unread Files -->
                <?php require APPROOT . '/views/users/templates/unreadFiles.php'; ?>
            </div>
        </div>

        <!-- Todolist -->
        <?php require APPROOT . '/views/users/templates/todo.php'; ?>

        <!-- Tasklisting -->
        <?php require APPROOT . '/views/users/templates/taskListings/index.php'; ?>

        <!-- Methodslist -->
        <?php require APPROOT . '/views/users/templates/methodsListing.php'; ?>

    </div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
