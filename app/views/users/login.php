<?php require APPROOT . '/views/inc/header.php'; ?>

    <div class="col-md-6 col-xl-3 mx-auto">
      <div class="card card-body bg-light mt-5">
          <!-- Display flash message -->
          <?php displayFlash('forgotpass_success'); ?>
          <?php displayFlash('forgotpass_failure'); ?>
          <?php displayFlash('resetpass_success'); ?>
          <h2>Login</h2>
          <form class="" action="<?php echo URLROOT; ?>/users/login" method="post">
            <div class="form-group">
              <label for="email">E-Mail: <sup>*</sup></label>
              <input type="text" name="email" class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['email']; ?>">
              <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
            </div>
            <div class="form-group">
              <label for="password">Passwort: <sup>*</sup></label>
              <input type="password" name="password" class="form-control form-control-lg <?php echo (!empty($data['password_err'])) ? 'is-invalid' : ''; ?>" value="<?php echo $data['password']; ?>">
              <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
            </div>
            <div class="row">
              <div class="col">
                <a href="<?php echo URLROOT; ?>/users/forgotpass" class="btn btn-light btn-block">Passwort vergessen?</a>
              </div>
              <div class="col">
                <input type="submit" value="Einloggen" class="btn btn-pe-darkgreen btn-block">
              </div>
            </div>
          </form>
      </div>
    </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
