<?php require APPROOT . '/views/inc/header.php'; ?>

<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2>Account anlegen</h2>
            <form class="" action="<?php echo URLROOT; ?>/users/register" method="post">
                <div class="form-group">
                    <label for="name">Vorname: <sup>*</sup></label>
                    <input type="text" name="firstname"
                           class="form-control form-control-lg <?php echo (!empty($data['firstname_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['firstname']; ?>">
                    <span class="invalid-feedback"><?php echo $data['firstname_err']; ?></span>
                </div>
                <div class="form-group">
                    <label for="name">Nachname: <sup>*</sup></label>
                    <input type="text" name="lastname"
                           class="form-control form-control-lg <?php echo (!empty($data['lastname_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['lastname']; ?>">
                    <span class="invalid-feedback"><?php echo $data['lastname_err']; ?></span>
                </div>
                <div class="form-group">
                    <label for="email">E-Mail: <sup>*</sup></label>
                    <input type="email" name="email"
                           class="form-control form-control-lg <?php echo (!empty($data['email_err'])) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $data['email']; ?>">
                    <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                    <p>Zu dieser E-Mail Adresse werden die Zugangsdaten geschickt</p>
                </div>
                <div class="form-group">
                    <label for="registerUserRole">Rollenzuweisung</label>
                    <select class="form-control" id="registerUserRole" name="userPermissionRole">
                        <?php foreach ($data['roles'] as $role): ?>
                            <option value="<?= $role->role_id; ?>">
                                <?= $role->role_name; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="row">
                    <div class="col">
                        <input type="submit" value="Registrieren" class="btn btn-pe-darkgreen btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
