<?php require APPROOT . '/views/inc/header.php'; ?>
<?php displayFlash('user_message'); ?>

    <!-- User Info -->
    <?php require APPROOT . '/views/users/templates/userInfo/index.php'; ?>

    <!-- Assignments -->
    <?php require APPROOT . '/views/users/templates/assignments/index.php'; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>

