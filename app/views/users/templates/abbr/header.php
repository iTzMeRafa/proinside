
<div class="row mb-3">
    <div class="col-md-6">
        <h1 class="h1 btn btn-pe-lightgreen"><i class="fas fa-users mr-2"></i>Kürzelvergabe</h1>
        <button type="submit" value="Speichern" class="h1 btn btn-pe-lightgreen"><i class="fas fa-save mr-2"></i> Speichern </button>
    </div>
    <div class="col-md-6 mb-4">
        <a href="javascript:history.go(-1)" class="btn btn-pe-darkgreen float-right">
            Zurück
        </a>
    </div>
</div>