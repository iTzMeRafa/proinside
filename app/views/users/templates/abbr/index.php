
<form action="<?php echo URLROOT; ?>/users/abbr" method="post">

    <!-- Display Flashes -->
    <?php require APPROOT . '/views/users/templates/abbr/displayFlashes.php'; ?>

    <!-- Header -->
    <?php require APPROOT . '/views/users/templates/abbr/header.php'; ?>

    <!-- UserList -->
    <?php require APPROOT . '/views/users/templates/abbr/userList.php'; ?>

</form>
