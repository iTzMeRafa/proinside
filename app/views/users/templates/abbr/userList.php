<!-- Sortable Table -->
<div class="col-md-12">
    <table id="userAbbr" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none; width: 30%;">Kürzel</th>
            <th style="border: none; width: 70%;">Mitarbeiter</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data['users'] as $user) : ?>
            <tr>
                <td style="width: 30%;">
                    <input
                        type="text"
                        name="<?php echo $user->id; ?>-abbreviation"
                        value="<?php echo $user->abbreviation; ?>"
                        class="form-control"
                    />
                </td style="width: 70%;">
                <td><?= $user->firstname . ' ' . $user->lastname; ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->