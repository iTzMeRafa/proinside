<?php if ($data['privUser']->hasPrivilege('19') == true) : ?>
    <?php if ($data['approvedFilesByUser']) : ?>
        <section id="approvedFilesByUser" class="mt-2">

            <button class="btn btn-info w-100 text-white" type="button" data-toggle="collapse" data-target="#approvedFilesByUserCollapse" aria-expanded="false" aria-controls="approvedFilesByUserCollapse">
                <?php if (count((array)$data['approvedFilesByUser']) > 1) : ?>
                    Sie haben <?php echo count((array)$data['approvedFilesByUser']); ?> Dateien freigegeben
                <?php else : ?>
                    Sie haben eine Datei freigegeben
                <?php endif; ?>
            </button>

            <div class="collapse" id="approvedFilesByUserCollapse">
                <div class="card card-body">

                    <?php foreach ($data['approvedFilesByUser'] as $approvedFile) : ?>
                        <?php if (!empty($approvedFile->users['unacceptedUser'])): ?>
                            <div class="row alert alert-info" role="alert">

                                <div class="col-md-12">
                                    Die Datei <strong><?= $approvedFile->file_name; ?></strong> (freigegeben am: <?= $approvedFile->approved_at; ?>): <br /> <br />

                                    <strong>Akzeptiert von:</strong> <br />
                                    <?= implode(', ', $approvedFile->users['acceptedUser']); ?>

                                        <br /> <br />

                                    <strong>Nocht nicht akzeptiert von:</strong> <br />
                                    <?= implode(', ', $approvedFile->users['unacceptedUser']); ?>
                                </div>

                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>

        </section>
    <?php endif;?>
<?php endif;?>