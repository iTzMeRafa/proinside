<!-- Sortable Table -->
<div class="mt-3">
    <table id="userAcceptedMethods" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">ID</th>
            <th style="border: none;">Name</th>
            <th style="border: none;">Status</th>
            <th style="border: none;">Check Intervall</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data['methods'] as $method) : ?>

            <tr>
                <td><a href="<?= URLROOT; ?>/methods/show/<?= $method->id; ?>">M<?php echo $method->id; ?></a></td>
                <td><a href="<?= URLROOT; ?>/methods/show/<?= $method->id; ?>"><?php echo $method->name; ?></a></td>
                <td><?= $method->status; ?></td>
                <td><?= $method->check_intervals; ?></td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->