<!-- Sortable Table -->
<div class="mt-3">
    <table id="userAcceptedTasks" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">ID</th>
            <th style="border: none;">Name</th>
            <th style="border: none;">Status</th>
            <th style="border: none;">ISO</th>
            <th style="border: none;">Check Intervall</th>
            <th style="border: none;">Benötigte Kompetenz</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data['tasks'] as $task) : ?>
            <?php if (is_null($task->id)): continue; endif; ?>
            <tr>
                <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task->id; ?>">A<?= $task->id; ?></a></td>
                <td><a href="<?= URLROOT; ?>/tasks/show/<?= $task->id; ?>"><?php echo $task->name; ?></a></td>
                <td><?= $task->status; ?></td>
                <td>ISO NEU HIER</td>
                <td><?= $task->check_intervals; ?></td>
                <td>
                    <?php foreach ($task->qualifications as $qualification): ?>
                        <?php
                        $passedDaysHavingQualification = differenceBetweenNowAndDate($qualification->assigned_at)->days;
                        $allowedDaysHavingQualification = $qualification->expire_in_days;
                        $qualificationStatus =
                            $passedDaysHavingQualification > $allowedDaysHavingQualification
                                ? 'danger'
                                : 'success';

                        $qualificationHref =
                            $qualificationStatus === 'danger'
                                ? URLROOT.'/methods/add'
                                : '#';
                        ?>
                    <a href="<?= $qualificationHref; ?>">
                        <span class="badge badge-<?= $qualificationStatus; ?>">
                            <?=
                              empty($qualification)
                                    ? 'Keine'
                                    : $qualification->name;
                            ?>
                        </span>
                    </a>
                    <?php endforeach; ?>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->