<div class="col-md-12">

    <!-- Navigation -->
    <nav class="mt-5">
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-acceptedTasks-tab" data-toggle="tab" href="#nav-acceptedTasks" role="tab" aria-controls="nav-acceptedTasks" aria-selected="true">Akzeptierte Aufgaben</a>
            <a class="nav-item nav-link" id="nav-unacceptedTasks-tab" data-toggle="tab" href="#nav-unacceptedTasks" role="tab" aria-controls="nav-unacceptedTasks" aria-selected="false">Unakzeptierte Aufgaben</a>
            <a class="nav-item nav-link" id="nav-acceptedMethods-tab" data-toggle="tab" href="#nav-acceptedMethods" role="tab" aria-controls="nav-acceptedMethods" aria-selected="false">Akzeptierte Maßnahmen</a>
            <a class="nav-item nav-link" id="nav-unacceptedMethods-tab" data-toggle="tab" href="#nav-unacceptedMethods" role="tab" aria-controls="nav-unacceptedMethods" aria-selected="false">Unakzeptierte Maßnahmen</a>
        </div>
    </nav>

    <!-- Contents -->
    <div class="tab-content" id="nav-tabContent">

        <!-- Angebotswelt -->
        <div class="tab-pane fade show active" id="nav-acceptedTasks" role="tabpanel" aria-labelledby="nav-acceptedTasks-tab">
            <?php require APPROOT . '/views/users/templates/assignments/acceptedTasks.php'; ?>
        </div>

        <!-- Strukturwelt -->
        <div class="tab-pane fade" id="nav-unacceptedTasks" role="tabpanel" aria-labelledby="nav-unacceptedTasks-tab">
            <?php require APPROOT . '/views/users/templates/assignments/unacceptedTasks.php'; ?>
        </div>

        <!-- Zielwelt -->
        <div class="tab-pane fade" id="nav-acceptedMethods" role="tabpanel" aria-labelledby="nav-acceptedMethods-tab">
            <?php require APPROOT . '/views/users/templates/assignments/acceptedMethods.php'; ?>
        </div>

        <!-- Rechtswelt -->
        <div class="tab-pane fade" id="nav-unacceptedMethods" role="tabpanel" aria-labelledby="nav-unacceptedMethods-tab">
            <?php require APPROOT . '/views/users/templates/assignments/unacceptedMethods.php'; ?>
        </div>

    </div>
</div>
