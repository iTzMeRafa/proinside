<!-- Sortable Table -->
<div class="mt-3">
    <table id="userUnacceptedMethods" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">ID</th>
            <th style="border: none;">Name</th>
            <th style="border: none;">Rolle</th>
            <th style="border: none;">Datum</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data['unaccepted_methods'] as $method) : ?>

            <tr>
                <td><a href="<?= URLROOT; ?>/methods/show/<?= $method['method']->id; ?>">M<?php echo $method['method']->id; ?></a></td>
                <td><a href="<?= URLROOT; ?>/methods/show/<?= $method['method']->id; ?>"><?php echo $method['method']->name; ?></a></td>
                <td><?= $method['role']; ?></td>
                <td>Seit dem <?= $method['date_time']['day'] . '. ' . $method['date_time']['month'] . ' ' . $method['date_time']['year'] . ' um ' . $method['date_time']['hour'] . ':' . $method['date_time']['min'] . ' Uhr'; ?></td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->