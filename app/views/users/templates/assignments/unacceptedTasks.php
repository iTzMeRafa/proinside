<!-- Sortable Table -->
<div class="mt-3">
    <table id="userUnacceptedTasks" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">ID</th>
            <th style="border: none;">Name</th>
            <th style="border: none;">Rolle</th>
            <th style="border: none;">Datum</th>
            <th style="border: none;">Benötigte Kompetenz</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data['unaccepted_tasks'] as $task) : ?>
            <tr>
                <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task['task']->id; ?>">A<?= $task['task']->id; ?></a></td>
                <td><a href="<?= URLROOT; ?>/tasks/show/<?= $task['task']->id; ?>"><?php echo $task['task']->name; ?></a></td>
                <td><?= $task['role']; ?></td>
                <td>Seit dem <?php echo $task['date_time']['day'] . '. ' . $task['date_time']['month'] . ' ' . $task['date_time']['year'] . ' um ' . $task['date_time']['hour'] . ':' . $task['date_time']['min'] . ' Uhr'; ?></td>
                <td>
                    <?php foreach ($task['task']->qualifications as $qualification): ?>
                        <?php
                        $passedDaysHavingQualification = differenceBetweenNowAndDate($qualification->assigned_at)->days;
                        $allowedDaysHavingQualification = $qualification->expire_in_days;
                        $qualificationStatus =
                            $passedDaysHavingQualification > $allowedDaysHavingQualification
                                ? 'danger'
                                : 'success';

                        $qualificationHref =
                            $qualificationStatus === 'danger'
                                ? URLROOT.'/methods/add'
                                : '#';
                        ?>
                        <a href="<?= $qualificationHref; ?>">
                        <span class="badge badge-<?= $qualificationStatus; ?>">
                            <?=
                            empty($qualification)
                                ? 'Keine'
                                : $qualification->name;
                            ?>
                        </span>
                        </a>
                    <?php endforeach; ?>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->