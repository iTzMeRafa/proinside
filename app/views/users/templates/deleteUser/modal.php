<!-- Modal: Delete User -->
<form method="POST" action="<?= URLROOT; ?>/UserDelete/index/<?= $data['user']->id; ?>" enctype="multipart/form-data">
    <div class="modal fade" id="delete_user" tabindex="-1" role="dialog" aria-labelledby="delete_user_label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nutzer entfernen</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Sind Sie sicher, dass die dieses Mitarbeiterprofil entfernen möchten? <br /> <br />
                    Dieser Mitarbeiter wird aus allen zugeteilten Aufgaben und Maßnahmen entfernt, ebenso ist eine Widerherstellung der Daten nicht möglich.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Schließen</button>
                    <button type="submit" class="btn btn-primary">Nutzer entfernen</button>
                </div>
            </div>
        </div>
    </div>
</form>