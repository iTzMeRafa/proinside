<?php if ($data['escalationsMethods']) : ?>
    <section id="escalationsMethods" class="col-6 mt-2">



        <button class="btn btn-danger w-100 text-white" type="button" data-toggle="collapse" data-target="#escalationsMethodsCollapse" aria-expanded="false" aria-controls="escalationsMethodsCollapse">
            <?php if (count((array)$data['escalationsMethods']) > 1) : ?>
                Sie haben <?= count((array)$data['escalationsMethods']); ?> Maßnahmen-Eskalationen
            <?php else : ?>
                Sie haben eine Maßnahmen-Eskalation
            <?php endif; ?>
        </button>

        <div class="collapse" id="escalationsMethodsCollapse">
            <div class="card card-body">

                <?php foreach ($data['escalationsMethods'] as $escalationMethods) : ?>
                    <div class="alert alert-danger col-12" role="alert">
                        <strong>Eskalation:</strong> Der Check der Maßnahme <strong>"M<?php echo intval($escalationMethods->id); ?>: <?php echo $escalationMethods->name; ?>"</strong> ist <?php echo $escalationMethods->nextCheckdate['msg']; ?> überfällig.
                        <a href="<?php echo URLROOT; ?>/tasks/show/<?php echo intval($escalationMethods->id); ?>" class="btn btn-pe-darkgreen float-right" target="_blank">Jetzt Ansehen</a>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>


    </section>
<?php endif;?>