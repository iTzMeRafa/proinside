<?php if ($data['escalationsTasks']) : ?>
    <section id="escalationsTasks" class="mt-2">

        <button class="btn btn-danger w-100 text-white" type="button" data-toggle="collapse" data-target="#escalationsTasksCollapse" aria-expanded="false" aria-controls="escalationsTasksCollapse">
            <?php if (count((array)$data['escalationsTasks']) > 1) : ?>
                Sie haben <?= count((array)$data['escalationsTasks']); ?> Aufgaben-Eskalationen
            <?php else : ?>
                Sie haben eine Aufgaben-Eskalation
            <?php endif; ?>
        </button>

        <div class="collapse" id="escalationsTasksCollapse">
            <div class="card card-body">

                <?php foreach ($data['escalationsTasks'] as $escalationTasks) : ?>
                    <div class="alert alert-danger col-12" role="alert">
                        <strong>Eskalation:</strong> Der Check der Aufgabe <strong>"A<?php echo intval($escalationTasks->task->id); ?>: <?php echo $escalationTasks->task->name; ?>"</strong> ist <?php echo $escalationTasks->nextCheckdate['msg']; ?> überfällig.
                        <a href="<?php echo URLROOT; ?>/tasks/show/<?php echo intval($escalationTasks->task_id); ?>" class="btn btn-pe-darkgreen float-right" target="_blank">Jetzt Ansehen</a>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </section>
<?php endif;?>