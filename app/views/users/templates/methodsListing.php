<div class="row mt-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Maßnahmen</h5>
                <h6 class="card-subtitle mb-2 text-muted">Alle dir zugewiesenen Maßnahmen</h6>
                <div class="card-text">

                    <!-- Sortable Table -->
                    <div class="mt-5">
                        <table id="methodsActiveTable" class="table table-striped table-bordered" style="width: 100%">
                            <thead>
                            <tr style="background-color: #0B676E; color: white; border: none;">
                                <th style="border: none;">ID</th>
                                <th style="border: none;">Bezeichnung</th>
                                <th style="border: none;">Auslösendes Ereignis</th>
                                <th style="border: none;">Startdatum</th>
                                <th style="border: none;">Zu Prozess</th>
                                <th style="border: none;">Priorität</th>
                                <th style="border: none;">Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($data['accepted_methods'] as $method): ?>
                                <tr>
                                    <td><a href="<?php echo URLROOT; ?>/methods/show/<?php echo $method->methodId; ?>">M<?= $method->methodId; ?></a></td>
                                    <td><a href="<?php echo URLROOT; ?>/methods/show/<?php echo $method->methodId; ?>"><?php echo $method->name; ?></a></td>
                                    <td>
                                        <?php
                                        switch ($method->occurrence) {
                                            case 1:
                                                echo 'Arbeitsschutz';
                                                break;
                                            case 2:
                                                echo 'Interne Audits';
                                                break;
                                            case 3:
                                                echo 'Externe Audits';
                                                break;
                                            case 4:
                                                echo 'Sonstiges';
                                                break;
                                            case 5:
                                                echo 'Abgelaufene Kompetenz';
                                                break;
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        Start Datum: <?= $method->reference_date; ?>
                                        <br />
                                        <?php
                                        if (isset($method->startDate_msg) && $method->startDate_msg['isWhen'] == 'future') : ?>
                                            Die Maßnahme fängt <strong><?= $method->startDate_msg['msg']; ?></strong> an. <br />
                                            <small class="text-muted">(<?= $method->startDate_msg['diffInDays']; ?> Tage ab heute)</small>
                                        <?php endif; ?>

                                        <?php
                                        if (isset($method->startDate_msg) && $method->startDate_msg['isWhen'] == 'past') : ?>
                                            Die Maßnahme ist <strong><?= $method->startDate_msg['msg']; ?></strong> überfällig. <br />
                                            <small class="text-muted">(Seit <?= $method->startDate_msg['diffInDays']; ?> Tagen)</small>
                                        <?php endif; ?>
                                    </td>
                                    <td><a href="<?= URLROOT; ?>/processes/show/<?= $method->process_id; ?>"><?= $method->process_name; ?></a></td>
                                    <td>
                                        <?php
                                        switch ($method->priority) {
                                            case 0:
                                                echo 'Keine Priorität festgelegt';
                                                break;
                                            case 1:
                                                echo '1 - Wichtig / Dringend';
                                                break;
                                            case 2:
                                                echo '2 - Nicht Wichtig / Dringend';
                                                break;
                                            case 3:
                                                echo '3 - Wichtig / Nicht Dringend';
                                                break;
                                            case 4:
                                                echo '4 - Nicht Wichtig / Nicht Dringend';
                                                break;
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        switch ($method->status) {
                                            case 'nichtBegonnen':
                                                echo 'Nicht Begonnen';
                                                break;
                                            case 'inBearbeitung':
                                                echo 'In Bearbeitung';
                                                break;
                                            case 'umgesetzt':
                                                echo 'Umgesetzt';
                                                break;
                                            case 'wirksamkeitGeprüft':
                                                echo 'Wirksamkeit geprüft';
                                                break;
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /Sortable Table -->

                </div>
            </div>
        </div>
    </div>
</div>

