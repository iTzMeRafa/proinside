<?php if ($data['reminders']) : ?>
    <section id="reminders" class="mt-2">

        <button class="btn btn-info w-100 text-white" type="button" data-toggle="collapse" data-target="#reminderCollapse" aria-expanded="false" aria-controls="reminderCollapse">
            <?php if (count((array)$data['reminders']) > 1) : ?>
                Sie haben <?php echo count((array)$data['reminders']); ?> Check Erinnerungen
            <?php else : ?>
                Sie haben eine Check Erinnerung
            <?php endif; ?>
        </button>

        <div class="collapse" id="reminderCollapse">
            <div class="card card-body">

                <?php foreach ($data['reminders'] as $reminder) : ?>

                    <div class="row alert alert-info" role="alert">

                        <div class="col-xs-8 col-md-8">


                            Der Check der Aufgabe <strong>"A<?php echo $reminder->task_id; ?>: <?php echo $reminder->name; ?>"</strong>

                            <?php if ($reminder->reminder_type == 'after') : ?>
                                ist <?php echo $reminder->nextCheckdate['msg']; ?> überfällig.

                            <?php elseif ($reminder->reminder_type == 'before') : ?>
                                ist <?php echo $reminder->nextCheckdate['msg']; ?> fällig.

                            <?php elseif ($reminder->reminder_type == 'at') : ?>
                                ist heute fällig.
                            <?php endif; ?>

                        </div>

                        <div class="col-xs-4 col-md-4 d-flex justify-content-end">
                            <a href="<?php echo URLROOT; ?>/tasks/show/<?php echo $reminder->task_id; ?>" class="btn btn-pe-darkgreen mr-2" target="_blank">Jetzt Ansehen</a>
                            <!--<button class="deactivate-reminder btn btn-pe-lightgreen" data="">Löschen</button>-->
                        </div>




                    </div>

                <?php endforeach; ?>
            </div>
        </div>

    </section>
<?php endif;?>