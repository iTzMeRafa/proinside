<div class="row mt-4">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Meine Aufgaben</h5>
                <h6 class="card-subtitle mb-2 text-muted">Alle dir zugewiesenen Aufgaben</h6>

                <!-- Glossar -->
                <h6 class="card-subtitle mb-2 text-muted">
                    <div class="riskShowBox--outer">
                        <div class="riskShowBox risk-Gering"></div> = Gering
                        <div class="riskShowBox risk-Mittel ml-3"></div> = Mittel
                        <div class="riskShowBox risk-Hoch ml-3"></div> = Hoch
                    </div>
                </h6>
                <div class="card-text">

                    <?php if ($data['accepted_tasks']) : ?>

                    <!-- Navigation -->
                    <nav class="mt-5">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-auftragswelt-tab" data-toggle="tab" href="#nav-auftragswelt" role="tab" aria-controls="nav-auftragswelt" aria-selected="true">Auftragswelt</a>
                            <a class="nav-item nav-link" id="nav-strukturwelt-tab" data-toggle="tab" href="#nav-strukturwelt" role="tab" aria-controls="nav-strukturwelt" aria-selected="false">Strukturwelt</a>
                            <a class="nav-item nav-link" id="nav-zielwelt-tab" data-toggle="tab" href="#nav-zielwelt" role="tab" aria-controls="nav-zielwelt" aria-selected="false">Zielwelt</a>
                            <a class="nav-item nav-link" id="nav-rechtswelt-tab" data-toggle="tab" href="#nav-rechtswelt" role="tab" aria-controls="nav-rechtswelt" aria-selected="false">Rechtswelt</a>
                        </div>
                    </nav>

                        <!-- Contents -->
                    <div class="tab-content" id="nav-tabContent">

                        <!-- Angebotswelt -->
                        <div class="tab-pane fade show active" id="nav-auftragswelt" role="tabpanel" aria-labelledby="nav-auftragswelt-tab">
                            <?php require APPROOT . '/views/users/templates/taskListings/angebotswelt.php'; ?>
                        </div>

                        <!-- Strukturwelt -->
                        <div class="tab-pane fade" id="nav-strukturwelt" role="tabpanel" aria-labelledby="nav-strukturwelt-tab">
                            <?php require APPROOT . '/views/users/templates/taskListings/strukturwelt.php'; ?>
                        </div>

                        <!-- Zielwelt -->
                        <div class="tab-pane fade" id="nav-zielwelt" role="tabpanel" aria-labelledby="nav-zielwelt-tab">
                            <?php require APPROOT . '/views/users/templates/taskListings/zielwelt.php'; ?>
                        </div>

                        <!-- Rechtswelt -->
                        <div class="tab-pane fade" id="nav-rechtswelt" role="tabpanel" aria-labelledby="nav-rechtswelt-tab">
                            <?php require APPROOT . '/views/users/templates/taskListings/rechtswelt.php'; ?>
                        </div>

                    </div>

                    <?php else :?>
                        <p>Dem Nutzer sind keine Aufgaben zugeteilt.</p>
                    <?php endif;?>

                </div>
            </div>
        </div>
    </div>
</div>
