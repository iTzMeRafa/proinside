<!-- Sortable Table -->
<div class="mt-3">
    <table id="taskRechtswelt" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">ID</th>
            <th style="border: none;">Zugehörigkeit</th>
            <th style="border: none;">Name</th>
            <th style="border: none;">Status</th>
            <th style="border: none;">Rolle</th>
            <th style="border: none;">Check Intervall</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data['rechtswelt'] as $task) : ?>
            <tr class="risk-<?= $task->risk; ?>">
                <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task->id; ?>">A<?= $task->id; ?></a></td>
                <td><?= $task->process_name === null ? 'Einzelaufgabe' : '<a href="'.URLROOT.'/processes/show/'.$task->process_id.'">'.$task->process_name.'</a>'; ?></td>
                <td><a href="<?php echo URLROOT; ?>/tasks/show/<?= $task->id; ?>"><?= $task->name; ?></a></td>
                <td><?= $task->status; ?></td>
                <td><?= $task->roles; ?></td>
                <td><?= $task->check_intervals; ?></td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->