<div class="row mt-4">
    <div class="col-md-6">
        <div id="todo">
            <div class="card">
                <div class="ui-output card-body">

                    <h5 class="card-title">Ihre persönliche To-dos Liste</h5>
                    <p>Ihr ausschließlich für Sie sichtbarer Ort für kleinere Aufgaben.</p>
                </div>
                <ul id="todo_list" class="list-group list-group-flush">
                    <?php if ($data['todo_tasks']) : ?>
                        <?php foreach ($data['todo_tasks'] as $task) : ?>
                            <li class="list-group-item">
                                <?php echo $task; ?>
                                <span class="remove-task float-right"><i class="fas fa-times"></i></span>
                            </li>
                        <?php endforeach; ?>
                    <?php endif;?>
                </ul>
                <div class="card-body">
                    <input type="text" id="todo_input" name="todo_input" class="form-control">
                    <button type="button" id="add_todo" class="btn btn-pe-darkgreen mt-2 d-block w-100">Hinzufügen</button>
                </div>
            </div>
        </div>
    </div>
</div>
