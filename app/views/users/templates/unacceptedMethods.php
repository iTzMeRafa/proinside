<?php if ($data['unaccepted_methods']['methods']) : ?>
    <section id="unanswered_methods" class="mt-2">

        <button class="btn btn-warning w-100 text-white" type="button" data-toggle="collapse" data-target="#collapseMethods" aria-expanded="false" aria-controls="collapseMethods">
            <?php if ($data['unaccepted_methods']['numOfMethods'] > 1) : ?>
                Sie haben <?php echo $data['unaccepted_methods']['numOfMethods']; ?> unbeantwortete Maßnahmen
            <?php else : ?>
                Sie haben eine unbeantwortete Maßnahme
            <?php endif; ?>
        </button>

        <div class="collapse" id="collapseMethods">
            <div class="card card-body">
                <?php foreach ($data['unaccepted_methods']['methods'] as $methodObj) : ?>


                    <div class="alert alert-warning col-12" role="alert">
                        Sie wurden am <?php echo $methodObj['date']; ?> um <?php echo $methodObj['time']; ?> Uhr zur Maßnahme <strong>"A<?php echo $methodObj['method']->id; ?>: <?php echo $methodObj['method']->name; ?>"</strong> in der Rolle "<?php echo $methodObj['role']; ?>" zugeteilt.
                        <a href="<?php echo URLROOT; ?>/methods/show/<?php echo $methodObj['method']->id; ?>" class="btn btn-pe-darkgreen float-right" target="_blank">Jetzt Ansehen</a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </section>
<?php endif;?>