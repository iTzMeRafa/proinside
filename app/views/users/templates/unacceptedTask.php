<?php if ($data['unaccepted_tasks']['tasks']) : ?>
    <section id="unanswered_tasks" class="mt-2">

        <button class="btn btn-warning w-100 text-white" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            <?php if ($data['unaccepted_tasks']['numOfTasks'] > 1) : ?>
                Sie haben <?php echo $data['unaccepted_tasks']['numOfTasks']; ?> unbeantwortete Aufgabenzuteilung
            <?php else : ?>
                Sie haben eine unbeantwortete Aufgabenzuteilung
            <?php endif; ?>
        </button>

        <div class="collapse" id="collapseExample">
            <div class="card card-body">
                <?php foreach ($data['unaccepted_tasks']['tasks'] as $taskObj) : ?>


                    <div class="alert alert-warning col-12" role="alert">
                        Sie wurden am <?php echo $taskObj['date']; ?> um <?php echo $taskObj['time']; ?> Uhr zur Aufgabe <strong>"A<?php echo $taskObj['task']->id; ?>: <?php echo $taskObj['task']->name; ?>"</strong> in der Rolle "<?php echo $taskObj['role']; ?>" zugeteilt.
                        <a href="<?php echo URLROOT; ?>/tasks/show/<?php echo $taskObj['task']->id; ?>" class="btn btn-pe-darkgreen float-right" target="_blank">Jetzt Ansehen</a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </section>
<?php endif;?>