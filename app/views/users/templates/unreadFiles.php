<?php if ($data['unreadData']) : ?>
    <section id="unread_data" class="mt-2">

        <button class="btn btn-success w-100 text-white" type="button" data-toggle="collapse" data-target="#collapseUnreadData" aria-expanded="false" aria-controls="collapseUnreadData">
            <?php if (count($data['unreadData']) > 1) : ?>
                Sie haben <?= count($data['unreadData']); ?> ungelesene Dateien
            <?php else : ?>
                Sie haben eine ungelesene Datei
            <?php endif; ?>
        </button>

        <div class="collapse" id="collapseUnreadData">
            <div class="card card-body">
                <?php foreach ($data['unreadData'] as $unreadData) : ?>


                    <div class="alert alert-success col-12" role="alert">
                        Bitte lesen und akzeptieren Sie die Datei <strong><?= $unreadData->file_name; ?></strong> (genehmigt am: <?= $unreadData->approved_at; ?>)
                        <a href="<?php echo URLROOT; ?>/Listings/index/<?= $unreadData->parent_id; ?>" class="btn btn-pe-darkgreen float-right" target="_blank"> Jetzt Ansehen </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

    </section>
<?php endif;?>