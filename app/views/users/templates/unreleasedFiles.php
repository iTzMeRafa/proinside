<?php if ($data['privUser']->hasPrivilege('19') == true) : ?>
    <?php if ($data['unapproved_data']) : ?>
        <section id="unreleasedFiles" class="mt-2">

            <button class="btn btn-success w-100 text-white" type="button" data-toggle="collapse" data-target="#unreleasedCollapse" aria-expanded="false" aria-controls="unreleasedCollapse">
                <?php if (count((array)$data['unapproved_data']) > 1) : ?>
                    Sie haben <?php echo count((array)$data['unapproved_data']); ?> Dateien zum Freigeben
                <?php else : ?>
                    Sie haben eine Datei zum Freigeben
                <?php endif; ?>
            </button>

            <div class="collapse" id="unreleasedCollapse">
                <div class="card card-body">

                    <?php foreach ($data['unapproved_data'] as $unreleased) : ?>

                        <div class="row alert alert-success" role="alert">

                            <div class="col-xs-8 col-md-8">
                                Die Datei <strong><?= $unreleased->target_name; ?></strong> (<?= $unreleased->timestamp; ?>) muss noch freigegeben werden.
                            </div>

                            <div class="col-xs-4 col-md-4 d-flex justify-content-end">
                                <a href="<?php echo URLROOT; ?>/folders/jump/<?= $unreleased->parent_id; ?>" class="btn btn-pe-darkgreen mr-2" target="_blank">Jetzt Ansehen</a>
                            </div>




                        </div>

                    <?php endforeach; ?>
                </div>
            </div>

        </section>
    <?php endif;?>
<?php endif;?>