<div class="row mb-3">
    <div class="col-md-6">
        <h1 class="h1 btn btn-pe-lightgreen">
            <i class="fas fa-user mr-2"></i><?= $data['user']->firstname . ' ' . $data['user']->lastname; ?>
        </h1>
        <a href="mailto:<?php echo $data['user']->email; ?>" class="h1 btn btn-dark"><i class="fas fa-envelope mr-2"></i> E-Mail schreiben</a>

        <!-- Delete User -->
        <?php require APPROOT . '/views/users/templates/deleteUser/index.php'; ?>
    </div>
    <div class="col-md-6 mb-4">
        <a href="javascript:history.go(-1)" class="btn btn-light float-right">
            <i class="fa fa-chevron-left mr-2"></i>Zurück
        </a>
    </div>
</div>