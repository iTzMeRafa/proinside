<div class="col-12 my-2">
    <?php if ($data['roles']) : ?>
        <?php foreach ($data['roles'] as $key => $role) : ?>
            <span class="badge badge-primary"><?php echo $key; ?></span>
        <?php endforeach; ?>
    <?php else : ?>
        <p>Dem Nutzer sind keine Rechte zugeteilt.</p>
    <?php endif; ?>

</div>