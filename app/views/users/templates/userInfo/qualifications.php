

<div class="col-12 my-2">
    <?php if ($data['qualifications']) : ?>
        <?php foreach ($data['qualifications'] as $qualification) : ?>

            <?php
                $passedDaysHavingQualification = differenceBetweenNowAndDate($qualification->assigned_at)->days;
                $allowedDaysHavingQualification = $qualification->expire_in_days;
                $qualificationStatus =
                    $passedDaysHavingQualification > $allowedDaysHavingQualification
                        ? 'danger'
                        : 'success';
            ?>



                <a href="<?= URLROOT; ?>/Qualifications/show/<?= $qualification->id; ?>">
                    <span class="badge badge-<?= $qualificationStatus; ?>">
                      <?= $qualification->name; ?>
                    </span>
                </a>
        <?php endforeach; ?>
    <?php else : ?>
        <p>Der Mitarbeiter verfügt über keine Kompetenzen</p>
    <?php endif; ?>
</div>