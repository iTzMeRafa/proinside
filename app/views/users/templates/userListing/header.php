<div class="row mb-3">
    <div class="col-md-12">

        <!-- Display flash message -->
        <?php displayFlash('register_success'); ?>
        <?php displayFlash('register_email_success'); ?>
        <?php displayFlash('register_email_failure'); ?>
        <?php displayFlash('user_message'); ?>
        <?php displayFlash('no_permisssion'); ?>
    </div>

    <div class="col-md-6">
        <h1 class="h1 btn btn-pe-lightgreen">
            <i class="fas fa-users mr-2"></i>Mitarbeiter
        </h1>
    </div>

    <div class="col-md-6">
        <a href="<?php echo URLROOT; ?>/users/register" class="btn btn-pe-lightgreen float-right">
            <i class="fas fa-pencil-alt ml-2"></i> Nutzer hinzufügen
        </a>
        <a href="<?php echo URLROOT; ?>/users/abbr" class="btn btn-pe-darkgreen float-right">
            Kürzel vergeben
        </a>

    </div>
</div>