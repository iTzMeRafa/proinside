<!-- Sortable Table -->
<div class="mt-5">
    <table id="userUnacceptedTasks" class="table table-striped table-bordered" style="width: 100%">
        <thead>
        <tr style="background-color: #0B676E; color: white; border: none;">
            <th style="border: none;">Name</th>
            <th style="border: none;">E-Mail</th>
            <th style="border: none;">Kürzel</th>
            <th style="border: none;">Aktion</th>
        </tr>
        </thead>
        <tbody>

        <?php foreach ($data['users'] as $user) : ?>

            <tr>
                <td><?= $user->firstname . ' ' . $user->lastname; ?></a></td>
                <td><i class="fas fa-envelope mr-2"></i><?= $user->email; ?></td>
                <td><?= $user->abbreviation; ?></td>
                <td>
                    <a href="<?= URLROOT; ?>/users/show/<?= $user->id; ?>" class="btn btn-pe-darkgreen w-100">Zum Nutzer</a>
                </td>

            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
</div>
<!-- /Sortable Table -->
