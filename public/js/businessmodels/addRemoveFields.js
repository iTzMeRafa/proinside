const listTab1 = document.getElementById("list-tab1");
const listTab1Button = document.getElementById("addListTab1List");

const listTab2 = document.getElementById("list-tab2");
const listTab2Button = document.getElementById("addListTab2List");

const listTab3 = document.getElementById("list-tab3");
const listTab3Button = document.getElementById("addListTab3List");

const listTab4 = document.getElementById("list-tab4");
const listTab4Button = document.getElementById("addListTab4List");


// Topic 1
if (listTab1Button) {
    listTab1Button.addEventListener("click", (event) => {
        event.preventDefault();

        listTab1.append(createListTab());

        function createListTab() {
            // Create a tag
            const aAtt = document.createElement("a");
            aAtt.setAttribute("class", "list-group-item list-group-item-action");
            aAtt.setAttribute("id", "list-append");
            aAtt.setAttribute("data-toggle", "list");
            aAtt.setAttribute("role", "tab");
            aAtt.setAttribute("aria-controls", "list-append");

            // Create input tag
            const aInp = document.createElement("input");
            aInp.setAttribute("type", "text");
            aInp.setAttribute("style", "width: 50%");
            aInp.setAttribute("name", "topic1subtopics[]");

            aAtt.appendChild(aInp);

            return aAtt;
        }
    });
}

// Topic 2
if (listTab2Button) {
    listTab2Button.addEventListener("click", (event) => {
        event.preventDefault();

        listTab2.append(createListTab());

        function createListTab() {
            // Create a tag
            const aAtt = document.createElement("a");
            aAtt.setAttribute("class", "list-group-item list-group-item-action");
            aAtt.setAttribute("id", "list-append");
            aAtt.setAttribute("data-toggle", "list");
            aAtt.setAttribute("role", "tab");
            aAtt.setAttribute("aria-controls", "list-append");

            // Create input tag
            const aInp = document.createElement("input");
            aInp.setAttribute("type", "text");
            aInp.setAttribute("style", "width: 50%");
            aInp.setAttribute("name", "topic2subtopics[]");

            aAtt.appendChild(aInp);

            return aAtt;
        }
    });
}

// Topic 3
if (listTab3Button) {
    listTab3Button.addEventListener("click", (event) => {
        event.preventDefault();

        listTab3.append(createListTab());

        function createListTab() {
            // Create a tag
            const aAtt = document.createElement("a");
            aAtt.setAttribute("class", "list-group-item list-group-item-action");
            aAtt.setAttribute("id", "list-append");
            aAtt.setAttribute("data-toggle", "list");
            aAtt.setAttribute("role", "tab");
            aAtt.setAttribute("aria-controls", "list-append");

            // Create input tag
            const aInp = document.createElement("input");
            aInp.setAttribute("type", "text");
            aInp.setAttribute("style", "width: 50%");
            aInp.setAttribute("name", "topic3subtopics[]");

            aAtt.appendChild(aInp);

            return aAtt;
        }
    });
}

// Topic 4
if (listTab4Button) {
    listTab4Button.addEventListener("click", (event) => {
        event.preventDefault();

        listTab4.append(createListTab());

        function createListTab() {
            // Create a tag
            const aAtt = document.createElement("a");
            aAtt.setAttribute("class", "list-group-item list-group-item-action");
            aAtt.setAttribute("id", "list-append");
            aAtt.setAttribute("data-toggle", "list");
            aAtt.setAttribute("role", "tab");
            aAtt.setAttribute("aria-controls", "list-append");

            // Create input tag
            const aInp = document.createElement("input");
            aInp.setAttribute("type", "text");
            aInp.setAttribute("style", "width: 50%");
            aInp.setAttribute("name", "topic4subtopics[]");

            aAtt.appendChild(aInp);

            return aAtt;
        }
    });
}