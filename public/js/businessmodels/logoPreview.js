// Preview Upload Image
const businessLogoInput = document.getElementById("businessLogoInput");
const businessLogoPreview = document.getElementById("businessLogoPreview");

if (businessLogoInput) {
  businessLogoInput.addEventListener("change", (event) => {
    event.preventDefault();
    businessLogoPreview.src = URL.createObjectURL(event.target.files[0]);
  });
}
