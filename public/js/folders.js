/**
 * FOLDERS.JS
 * --------------------------------------------------------------------------------------------
 * This file is for everything to manage and handle the filemanager.
 * Logic to move into folders, load folder lists and make ajax calls to do server-side actions.
 *
 * @type {HTMLElement}
 * @return void
 */


// Initializations
let folderTableEl = document.getElementById("folder_table");
let currentPath = '../uploads/files';
let currentDirectory = currentPath.substring(currentPath.lastIndexOf('/') + 1, currentPath.length);
let directoryId = !!folderTableEl ? folderTableEl.dataset.startdir : 0;
let directoryName = !!folderTableEl ? folderTableEl.dataset.startdirname : null;

$(document).ready(function () {
  // Autocalls the folder load function
  if ($('body').hasClass('folders')) {
    load_folder_list();
  }


  /**
   * LOAD FOLDER LIST
   * Function to load the folder list by path
   *
   * @action Ajax call
   * @return void
   */
  function load_folder_list() {
    /*
     * Check if user jumped into dir by url
     * Show Process & Infomaterial Buttons
     */
    if (parseInt(folderTableEl.dataset.startdir) !== 0) {
      let infoMaterialLink = $('#show_infomaterial').attr('href');
      let infoMaterialIndex = infoMaterialLink.lastIndexOf('/');
      let updatedInfoMaterialLink = infoMaterialLink.substr(0, infoMaterialIndex + 1) + folderTableEl.dataset.startdir;
      $('#show_infomaterial').attr('href', updatedInfoMaterialLink)

      $('#create_process').removeClass('d-none');
      $('#show_infomaterial').removeClass('d-none');
    }
    $.ajax({
      method: 'POST',
      url: urlroot + 'php/action_new.php',
      data: {
        action: 'fetch',
        targetId: directoryId,
        folderName: directoryName,
        parentId: 0,
        parentPath: currentPath
      },
      success: function (data) {
        $('#folder_table').html(data);
        $('#currentDirectory').text(!!directoryName ? directoryName : '-');
      }
    });
  }

  /**
   * Reloads the current page
   */
  function reloadPage() {
    location.reload();
  }

  /**
   * NAVIGATE INTO FOLDER
   * Navigates into the clicked folder and makes a ajax call to get the folders
   *
   * TODO: does not effect when jumping into a folder by url  '/jump/<id>'
   * @action Ajax call
   * @return void
   */
  $(document).on('click', '.directory', function () {

    // path, dir id, parent id, parent path of clicked folder
    let path = $(this).data('path');
    let targetId = $(this).data('id');
    let parentId = $(this).data('parent');
    let parentPath = path.substring(0, path.lastIndexOf('/'));

    $.ajax({
      method: 'POST',
      url: urlroot + 'php/action_new.php',
      data:
          {
            action: 'fetch',
            targetId: targetId,
            parentId: parentId,
            parentPath: parentPath
          },
      success: function (data) {
        // Update folders list with new
        $('#folder_table').html(data);

        // overwrite global init data id, path, name
        directoryId = targetId;
        currentPath = path;
        currentDirectory = currentPath.substring(currentPath.lastIndexOf('/') + 1, currentPath.length);

        // Change dir title
        if (currentDirectory == 'files') {
          $('#currentDirectory').text('Hauptordner');

        } else {
          $('#currentDirectory').text(currentDirectory);
          $('#subtopicOfTopicName').text(currentDirectory);
        }

        // Show Create Process, show infomaterial buttons
        if (directoryId != 0) {

          // Update button links with correct directory id
          let processLink = $('#create_process').attr('href');
          let infoMaterialLink = $('#show_infomaterial').attr('href');

          // Get the last index of a slash
          let processIndex = processLink.lastIndexOf('/');
          let infoMaterialIndex = infoMaterialLink.lastIndexOf('/');

          // Store the link with the current directoryId
          let updatedProcessLink = processLink.substr(0, processIndex + 1) + directoryId;
          let updatedInfoMaterialLink = infoMaterialLink.substr(0, infoMaterialIndex + 1) + directoryId;

          // Update the actual DOM element with the updated link
          $('#create_process').attr('href', updatedProcessLink);
          $('#show_infomaterial').attr('href', updatedInfoMaterialLink)

          // Display the "create_process" button
          $('#create_process').removeClass('d-none');
          $('#show_infomaterial').removeClass('d-none');

        } else {
          // hide buttons for root dir
          $('#create_process').addClass('d-none');
          $('#show_infomaterial').addClass('d-none');
        }
      }
    });
  });

  /**
   * Check for action parameter
   * These parameter will simulate a behavior on the listing
   *
   * @return void
   */
  const actionParameter = findGetParameter("action");
  if (actionParameter) {

    // Simulates the click on archive folder, needs timeout (hacky) because the element has to load first
    // TODO: find a guaranteed way to wait till the el is loaded
    if (actionParameter === 'archiveClick') {
      setTimeout(function () {
        const archiveEl = document.getElementById("archiveFolder");
        archiveEl.click();
      }, 500)
    }

    if (actionParameter == 'openUploadModal') {
      $('#uploadModal').modal('show');
    }
  }


  /**
   * NAVIGATE INTO ARCHIVE
   * Navigates into clicked archive folder
   *
   * @action Ajax call
   * @return void
   */
  $(document).on('click', '.archive', function () {

    // Path, dir id, parent id, isRoot bool of clicked folder (archive)
    let path = $(this).data('path');
    let targetId = $(this).data('id');
    let parentId = $(this).data('parent');
    let parentPath = path.substring(0, path.lastIndexOf('/'));
    let isRootArchive = 'false';

    /* Hide add buttons, since archive is not allowed to */
    $('#upload_data').addClass('d-none');
    $('#create_folder').addClass('d-none');

    if ($(this).hasClass('main')) {
      isRootArchive = 'true';
    }

    $.ajax({
      method: 'POST',
      url: urlroot + 'php/action_new.php',
      data:
          {
            action: 'fetch_archive',
            targetId: targetId,
            parentId: parentId,
            parentPath: parentPath,
            isRootArchive: isRootArchive
          },
      success: function (data) {
        // update the filemanager with new content
        $('#folder_table').html(data);

        // overwrite global init data path, name
        currentPath = path;
        currentDirectory = currentPath.substring(currentPath.lastIndexOf('/') + 1, currentPath.length);

        // Change Directory title
        if (currentDirectory == 'files') {
          $('#currentDirectory').text('Hauptordner');
        } else {
          $('#currentDirectory').text(currentDirectory);
        }
      }
    });
  });

  /**
   * CLICK BACK IN ARCHIVE
   * Click on back in archive folder
   * navigates to parent folder
   */
  $(document).on('click', '.previous-archive', function () {

    // path, dir id, parent id, parent path, is root of clicked folder (back)
    let path = $(this).data('path');
    let targetId = $(this).data('id');
    let parentId = $(this).data('parent');
    let parentPath = path.substring(0, path.lastIndexOf('/'));
    let isRootArchive = 'false';

    if ($(this).hasClass('main')) {
      isRootArchive = 'true';
    }

    $.ajax({
      method: 'POST',
      url: urlroot + 'php/action_new.php',
      data:
          {
            action: 'fetch_archive',
            targetId: targetId,
            parentId: parentId,
            parentPath: parentPath,
            isRootArchive: isRootArchive
          },
      success: function (data) {
        // Update file manager list
        $('#folder_table').html(data);

        // overwrite global init data path, name
        currentPath = path;
        currentDirectory = currentPath.substring(currentPath.lastIndexOf('/') + 1, currentPath.length);

        // Update folder title
        if (currentDirectory == 'files') {
          $('#currentDirectory').text('Hauptordner');
        } else {
          $('#currentDirectory').text(currentDirectory);
        }
      }
    });
  });

  /**
   * CREATE FOLDER MODAL
   * Create folder button click
   * Handles settings the texts and sort dropdown for the modal
   *
   * @action Ajax call
   * @return void
   */
  $('#create_folder').click(function () {

    // get folderlist el, folder id, parent id
    const currentFolderEl = document.getElementsByClassName("directory col-md-9")[0];
    const targetId = $(this).data('id');
    let currentFolderID = 0;
    if (currentFolderEl) {
      currentFolderID = currentFolderEl.dataset.parent;
    }

    // Set values to modal elements and show it
    $('#action').val('create');
    $('#folder_name').val('');
    $('#folder_button').val('Erstellen');
    $('#old_name').val('');
    $('#change_title').text('Ordner erstellen');
    $('#folderModal').modal('show');

    $.ajax({
      method: 'POST',
      url: urlroot + 'php/action_new.php',
      data: {
        action: 'getFolderList',
        targetId: currentFolderID,
      },
      success: function (data, status, request) {
        const folders = JSON.parse(data);
        const sortSelectEl = document.getElementById("sortPositionFolder");

        // sets the sort select options dropdown;
        // removes all childs first (previous ones) and add current folder list as options
        if (sortSelectEl) {

          while (sortSelectEl.firstChild) {
            sortSelectEl.removeChild(sortSelectEl.firstChild);
          }

          folders.map((folder) => {
            const option = document.createElement("option");
            option.value = folder.sortID;
            option.text = 'unter ' + folder.folder_name;
            sortSelectEl.add(option);
          });
        }
      },
      error: (error) => {
        console.log(error);
      }
    });
  });

  /**
   * CREATE FOLDER
   * Click on "Erstellen" for new folder
   * This actually creates the folder and saves all data in the db via ajax
   *
   * @action Ajax call
   * @return void
   */
  $('#folder_button').click(function () {

    // Get all input values of the form for the folder
    let folder_name = $('#folder_name').val();
    let action = $('#action').val();
    let old_name = $('#old_name').val();
    let folder_id = $('#folder_id').val();
    let istopicfolder = $('#istopicFolder').val();
    let issubtopicfolder = $('#issubtopicFolder').val();
    let businessModelType = $('input[name=businessModelType]:checked').val();
    let sortFolderID = $('input[name=businessModelType]:checked').val();
    let sortPositionFolderEL = document.getElementById("sortPositionFolder");
    let sortAfterFolderID;
    if (sortPositionFolderEL) {
      sortAfterFolderID = sortPositionFolderEL.value;
    }
    console.log(sortAfterFolderID);

    if (folder_name != '') {
      $.ajax({
        url: urlroot + 'php/action_new.php',
        method: "POST",
        data: {
          folder_name: folder_name,
          old_name: old_name,
          action: action,
          isTopic: istopicfolder,
          isSubtopic: issubtopicfolder,
          currentDirectory: currentPath,
          targetId: directoryId,
          folderId: folder_id,
          businessModelType: businessModelType,
          sortAfterFolderID: sortAfterFolderID,
        },
        success: function (data) {
          $('#folderModal').modal('hide');
          load_folder_list();
          $('#alertbox').append(data);
        }
      });

    } else {

      // Notification for empty folder name
      let folderAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"> Bitte geben Sie einen Ordnernamen ein. <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      $('#alertbox').append(folderAlert);
    }
  });

  /**
   * SHOW INFO MODAL
   * Click on show info on folder list item
   * This shows a modal of all informations of the clicked folder
   *
   * @return void
   */
  $(document).on('click', '.info', function () {

    // get filename, tags and splits the tags into array
    let fileName = $(this).data('name');
    let tags = $(this).data('tags');
    let fileApprovedBy = $(this).data('file_info_approved_by');
    let fileApprovedAt = $(this).data('file_info_approved_at');
    tags = tags.split(',');

    // update filename and display all tags in a badge
    $('#file_info_name').text(fileName);
    $('#file_info_approved_by').text(fileApprovedBy);
    $('#file_info_approved_at').text(fileApprovedAt);
    $.each(tags, function (index, value) {
      let item = `<span class="badge badge-secondary mr-1">${value}</span>`;
      $('#tag-list').append(item);
    });

    // Display the modal
    $('#infoModal').modal('show');
  });

  // Removes all tags on closing the modal
  $(document).on('click', '#infoModal .closeInfo', function () {
    $('#tag-list').children().remove();
  });

  /**
   * RENAME FILE/FOLDER MODAL
   * Sets up and saves data for the rename file/folder modal
   */
  $(document).on('click', '.update', function () {

    // old file/folder name, determine if file or folder
    let oldName = $(this).data('name');
    let object = $(this).parent().parent().parent().siblings('.col-md-9');

    // Logic for renaming files
    if (object.hasClass('file')) {

      // get filename, extension, extension length, file id
      let split = oldName.split(".");
      let extension = split.pop();
      let extLength = extension.length + 1;
      let nameNoExt = oldName.substr(0, oldName.length - extLength);
      let fileId = object.data('id');

      // Sets modal hidden input values
      $('#old_file_name').val(oldName);
      $('#file_id').val(fileId);
      $('#file_extension').val(extension);

      // Change the ui of modal
      $('#file_name').val(nameNoExt);

      // show the modal
      $('#fileModal').modal('show');

      // Logic for renaming folders
    } else if (object.hasClass('directory')) {

      let folderID = object.data('id');
      let isTopic = object.data('istopic');
      let isSubtopic = object.data('issubtopic');
      const currentFolderEl = document.getElementsByClassName("directory col-md-9")[0];
      let currentFolderID = 0;
      if (currentFolderEl) {
        currentFolderID = currentFolderEl.dataset.parent;
      }

      // Sets modal hidden input values
      $('#old_name').val(oldName);
      $('#folder_name').val(oldName);
      $('#action').val("change_folder_name");
      $('#folder_id').val(folderID);
      $('#istopicFolder').val(isTopic);
      $('#issubtopicFolder').val(isSubtopic);

      // Change the UI of modal
      $('#folder_button').val('Änderungen speichern');
      $('#folderModal #change_title').text('Ordnern anpassen');

      // initializes the sort dropdown
      $.ajax({
        method: 'POST',
        url: urlroot + 'php/action_new.php',
        data: {
          action: 'getFolderList',
          targetId: currentFolderID,
        },
        success: function (data, status, request) {
          const folders = JSON.parse(data);
          const sortSelectEl = document.getElementById("sortPositionFolder");

          // sets the sort select options dropdown;
          // removes all childs first (previous ones) and add current folder list as options
          if (sortSelectEl) {

            while (sortSelectEl.firstChild) {
              sortSelectEl.removeChild(sortSelectEl.firstChild);
            }

            folders.map((folder) => {
              const option = document.createElement("option");
              option.value = folder.sortID;
              option.text = 'unter ' + folder.folder_name;
              sortSelectEl.add(option);
            });
          }
        },
        error: (error) => {
          console.log(error);
        }
      });

      // Show the modal
      $('#folderModal').modal('show');


      /*
       * IMPORTANT:
       * The rest of this request is processed via the create folder function's AJAX Request.
       */
    }
  });

  /**
   * RENAME FILE/FOLDER
   * This actually renames the files/folders
   *
   * @action Ajax call
   * @return void
   */
  $('#file_button').click(function () {

    // Get the required values from the inputs
    let file_name = $('#file_name').val();
    let old_name = $('#old_file_name').val();
    let extension = $('#file_extension').val();
    let fileId = $('#file_id').val();

    if (file_name != '') {
      $.ajax({
        url: urlroot + 'php/action_new.php',
        method: "POST",
        data: {
          file_name: file_name,
          old_name: old_name,
          file_ext: extension,
          action: 'change_file_name',
          currentDirectory: currentPath,
          fileId: fileId
        },
        success: function (data) {
          $('#fileModal').modal('hide');
          load_folder_list(currentPath);
          $('#alertbox').append(data);
        }
      });

    } else {

      // Notification for empty filename
      let fileAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"> Bitte geben Sie einen Dateinamen ein. <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      $('#alertbox').append(fileAlert);
    }
  });

  /**
   * CHECKBOX TOGGLE
   * Checks if the checkbox is active/inactive and toggles the user list
   *
   * @return void
   */
  $('#showUserList').click(function () {
    $("#fileUploadUserList").toggle(this.checked);
  });

  /**
   * UPLOAD DATA
   * Uploads data into the current folder
   *
   * @action Ajax call
   * @return void | boolean
   */
  $(document).on('click', '#upload_data', function () {

    // Prevent uploading in root directory
    if (directoryId == 0) {

      // Notification for uploading in root directory
      const dirError = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Sie können keine Dateien in den Hauptordner laden.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      $('h1').before(dirError);

    } else {

      // get folder path, folder id
      $('#hidden_folder_path').val(currentPath);
      $('#hidden_parent_id').val(directoryId);

      // Show the Upload Modal
      $('#uploadModal').modal('show');

      // Show filename on custom file input
      $('#upload_file').on('change', function () {
        const fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
      })
    }
  });

  $('#upload_form').on('submit', function () {
    $.ajax({
      url: urlroot + 'php/upload.php',
      method: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      success: function (data) {

        // get previous id, path and current file input
        let parentId = $('.previous').data('id');
        let parentPath = $('.previous').data('path');
        let fileInput = $('#upload_file');

        // Fetches the folderslist, basically a reload
        $.ajax({
          method: 'POST',
          url: urlroot + 'php/action_new.php',
          data: {
            action: 'fetch',
            targetId: directoryId,
            parentId: parentId,
            parentPath: parentPath
          },
          success: function (data) {
            $('#folder_table').html(data);
          }
        });

        // Clears the data and closes modal
        $('h1').before(data);
        $('#uploadModal').modal('hide');
        $('#uploadModal .tags-input .tag').remove();
        fileInput.replaceWith(fileInput.val('').clone(true));
      }
    });
    return false;
  });

  /*
   * Stores and handles the tags of the uploaded file
   */
  let tags = [];

  // Check if tags already exists to store them as an array
  if ($('.hiddenInput').val() != '') {
    const storedTags = $('.hiddenInput').val().split(',');
    storedTags.forEach(function (st) {
      tags.push(st);
    });
  }

  // Handle key input events to split visually the tags by entering ',' (comma)
  $('.mainInput').on('keyup', function () {
    let enteredTags = $('.mainInput').val().split(',');
    if (enteredTags.length > 1) {
      enteredTags.forEach(function (t) {
        let filteredTag = filterTag(t);

        // check for duplicate tags and prevent them
        if (filteredTag.length > 0 && tags.indexOf(filteredTag) == -1) {
          addTag(filteredTag);
        }
      });

      // clear for next input
      $('.mainInput').val('');
    }
  });

  /**
   * REMOVES TAG
   * Handles click on (x) removing a tag in the tag field
   */
  $(document).on('click', '.close', function () {

    // get tag as string
    let tag = $(this).parent().text();

    // remove the tag from array, ui
    removeTag(tags.indexOf(tag));
    $(this).parent().remove();
  });


  /**
   * ADD TAG HELPER FUNCTION
   * adds a new tag as html span element at the beginning of the main tag input
   * refreshes the tag list
   *
   * @param text
   * @return void
   */
  function addTag(text) {
    $('.mainInput').before(`<span class="tag">${text} <i class="close fas fa-times"></i></span>`);
    tags.push(text);
    refreshTags();
  }

  /**
   * REMOVE TAG HELPER FUNCTION
   * removes a tag from the array
   * refreshes the tag list
   *
   * @param index
   * @return void
   */
  function removeTag(index) {
    let tag = tags[index];
    tags.splice(index, 1);
    refreshTags();
  }

  /**
   * REFRESH TAG HELPER FUNCTION
   * Refreshes the tags list by storing all current added tags and rejoining them
   *
   * @return void
   */
  function refreshTags() {
    let tagsList = [];
    tags.forEach(function (t) {
      tagsList.push(t);
    });
    $('.hiddenInput').val(tagsList.join());
  }

  /**
   * FILTER TAG HELPER FUNCTION
   * String replaces tags regex
   * prevent whitespaces and replaces them with dashes
   *
   * @param tag
   * @returns {string}
   */
  function filterTag(tag) {
    return tag.replace(/[^\w -]/g, '').trim().replace(/\W+/g, '-');
  }

  /**
   * FOLDER DELETE
   * Handles deleting folders of the folderslist and also updates the database entry
   *
   * @action Ajax call
   * @return void
   */
  $(document).on('click', '.delete', function () {
    // get folder name, folder id, parent id, istopic bool, issubtopic bool, list object
    let folder_name = $(this).data("name");
    let object = $(this).parent().parent().parent().siblings('.col-md-9');
    let folder_id = object.data("id");
    let parentId = object.data("parent");
    let isTopic = object.data("istopic");
    let isSubtopic = object.data("issubtopic");

    // Add alert notification to modal
    let alert = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="fas fa-exclamation-triangle"></i> Der ausgewählter Ordner enthält möglicherweise Dateien!</div>';
    if ($('#confirmModal .modal-body .alert').length == 0) {
      $('#confirmModal .h6').after(alert);
    }

    // update modal texts
    $('#confirmModal .h6').text('Sind Sie sicher, dass Sie diesen Ordner löschen möchten?');
    $('#confirmModal .data').html(`Ausgewählter Ordner: <strong id="selectedFile">${folder_name}</strong>`);

    // Show the modal
    $('#confirmModal').modal('show');


    // Listening for the notification accept click to actually delete the folder
    $('#acceptDel').on('click', function () {

      $.ajax({
        url: urlroot + 'php/action_new.php',
        method: "POST",
        data:
            {
              folder_name: folder_name,
              currentPath: currentPath,
              action: 'delete',
              folderId: folder_id,
              parentId: parentId,
              isTopic: isTopic,
              isSubtopic: isSubtopic,
            },
        success: function (data) {
          //reloadPage();
          load_folder_list(currentPath);
          $('#alertbox').append(data);
          $('#confirmModal').modal('hide');
        }
      });
    });
  });

  /**
   * DELETE FILES
   * Handles deleting files in a folder
   *
   * @action Ajax call
   * @return void
   */
  $(document).on('click', '.delete_file', function () {
    // get filename, path, id
    let fileName = $(this).data("name");
    let filePath = `${currentPath}/${fileName}`;
    let fileId = $(this).parent().parent().parent().siblings('.col-md-6').data('id');

    // Disables alert notification if still showing
    if ($('#confirmModal .modal-body .alert').length) {
      $('#confirmModal .modal-body .alert').remove();
    }

    // Change the modal text
    $('#confirmModal .h6').text('Sind Sie sicher, dass Sie diese Datei löschen möchten?');
    $('#confirmModal .data').html(`Ausgewählte Datei: <strong id="selectedFile">${fileName}</strong>`);

    // Show the modal
    $('#confirmModal').modal('show');

    // Listening for the notification accept click to actually delete the file
    $('#acceptDel').on('click', function () {


      $.ajax({
        url: urlroot + 'php/action_new.php',
        method: "POST",
        data:
            {
              action: 'archive_file',
              filePath: filePath,
              fileId: fileId,
            },
        success: function (data) {
          //reloadPage();
          $('#alertbox').append(data);
          $('#confirmModal').modal('hide');
          load_folder_list();
        }
      });
    });
  });

  /**
   * APPROVE FILE
   * Updates a unapproved file to be approved
   *
   * @action Ajax call
   * @return void
   */
  $(document).on('click', '.approve_file', function () {
    // Get the id
    targetId = $(this).parent().siblings('.unapproved').data('id');

    $.ajax({
      url: urlroot + 'php/action_new.php',
      method: "POST",
      data:
          {
            action: 'approve_file',
            targetId: targetId
          },
      success: function (data) {
        $('#alertbox').append(data);
        load_folder_list();
      }
    });
  });

  /**
   * FILE NOTIFICATION CLICK
   * On button click to store the file id
   */
  $(document).on('click', '#approve_file_notificationButton', function () {

    // Get the data id, userlist
    const notifiedUsersListEl = document.getElementById("notifiedUsersList");
    const targetId = $(this).parent().siblings('.unapproved').data('id');
    const userlist = $(this).data('userlist');
    const userlistArr = userlist.split(",").map(function(item) {
      return item.trim();
    });

    // Loop over the checkboxes of the userlist and check them when already selected
    $("input[name='notificationUserList[]']").each(function (index, obj) {
      obj.checked = userlistArr.includes(obj.value);
    });

    // Display comma seperated list of all user names that are selected
    $.ajax({
      url: urlroot + 'php/action_new.php',
      method: "POST",
      data:
          {
            action: 'getUsernamesByIDs',
            userlist: userlist,
          },
      success: function (data) {
        while (notifiedUsersListEl.firstChild) {
          notifiedUsersListEl.removeChild(notifiedUsersListEl.firstChild);
        }
        const userNamesArr = JSON.parse(data);
        const userNameString = userNamesArr.join(', ');
        const selectedUsersText = document.createTextNode("Ausgewählte Mitarbeiter: ");
        const selectedUsersList = document.createTextNode(userNameString);
        const breakEl = document.createElement("br");
        notifiedUsersListEl.appendChild(selectedUsersText);
        notifiedUsersListEl.appendChild(breakEl);
        notifiedUsersListEl.appendChild(selectedUsersList);
      }
    });

    $("#fileNotificationID").val(targetId);
  });

  /**
   * UPDATE FILE NOTIFICATION USERS
   * On Form submit
   */
  $('#user_notification_form').on('submit', function () {
    $.ajax({
      url: urlroot + 'php/action_new.php',
      method: "POST",
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData: false,
      success: function (data) {
        if (data === "1") {
          $('#fileUserNotificationListAlert').fadeIn().delay(2000).fadeOut();
        } else {
          $('#fileUserNotificationListAlertDanger').fadeIn().delay(2000).fadeOut();
        }

        // get previous id, path and current file input
        let parentId = $('.previous').data('id');
        let parentPath = $('.previous').data('path');

        // Fetches the folderslist, basically a reload
        $.ajax({
          method: 'POST',
          url: urlroot + 'php/action_new.php',
          data: {
            action: 'fetch',
            targetId: directoryId,
            parentId: parentId,
            parentPath: parentPath
          },
          success: function (data) {
            $('#folder_table').html(data);
          }
        });
      }
    });
    return false;
  });

});

