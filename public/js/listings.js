$(document).ready(function(){

	/* ############################### */
	/* #########  LISTINGS   ######### */
	/* ############################### */
	
	if( $('body').hasClass('listings') ){

		console.log('has listings');

		// Listen for click on file in the sidebar
		$('.showDataListGroup').on('click', function(){
			console.log("sidebar clicked");
			// Get the file path
			$path = $(this).data('path');
			$contentType = $(this).data('contenttype');
			console.log($path);

			// Check if path is filled
			if($path == 'empty' || $contentType === 'Content-Type: text/html; charset=UTF-8>'){
				// No further action
				console.log("Empty or not found so do nothin'");
				$('#mainAreaFileNotFoundHint').removeClass("d-none");
				$('#mainArea iframe').attr('src', '');
			} else {
				$('#mainAreaFileNotFoundHint').addClass("d-none");
				// Make the iframe display the file
				$('#mainArea iframe').attr('src', $path);
			}
		});

		/* ###################################### */
		/* ############ SUB FOLDERS ############# */

		// Listen for click on expand sub folders button
		$('.expand-sub').on('click', function(){
			// Reveal the sub folders
			$(this).parent().siblings('.sub-folders').toggleClass('d-none');
		});
		
		// If a sub folder link is .active
		// Reveal the sub folders
		$('.sub-link.active').parent().parent().parent().removeClass('d-none');
		//make sure the parent is .active as well
		$('.sub-link.active').parent().parent().parent().siblings('.d-flex').children('.main-link').addClass('active');

		





	} // listings
}); // document.ready