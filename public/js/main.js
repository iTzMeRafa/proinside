$(document).ready(function(){

/* ########################################################### */
/* ##################### SEARCH FUNCTION ##################### */
/* ########################################################### */

$('#search-field').on('focus', function(){

    console.log('FOCUS');
    
    //$('#search_results_modal').modal('show');
});

$('#search-field').on('focusout', function(){

  console.log('FOCUS OUT');
  //$('#search_results_modal').modal('hide');
  

});

$('#search-field').on('keyup', function(){

  // Get the value
  let input = $(this).val();
  if(input != '') {
    console.log(input);
    
    /*
    // Make the ajax request 
    $.ajax({
      url: '../php/search.php',
        method: 'POST',
        data: {
          input: input
        },
        dataType: "text",
        success: function(data){
          console.log(data);
        }
    }); // AJAX
    */
  }

});



/* #################################################### */
/* ################ DASHBOARD VIEW #################### */
/* #################################################### */

if($('body').hasClass('dashboard')){

  /* #################################################### */
  /* ##################### ADD TODO ##################### */
  /* #################################################### */

  $('#add_todo').on('click', function(){
    // Get the input element
    let taskInput = $('#todo_input'); 

    // Get the input value
    let task = taskInput.val();
    task = task.trim();
    
    // Check if the submitted input value is at least 5 characters long
    if(task.length < 5){
      // LESS THAN 5 CHARS
      ui_message('Eine Aufgabe muss mindestens 5 Zeichen lang sein.', 'danger');

    } else {
      // MORE THAN 5 CHARS 

      // Init variable
      let taskArray = [];

      $('#todo_list li').each(function() {

        currentTask = $(this).text();
        currentTask = currentTask.trim();

        taskArray.push(currentTask);
      });
      taskArray.push(task);

      console.log(taskArray);

      // Clear the input 
      taskInput.val('');

      // Create a listItem with the task's text
      let taskLI = `<li class="list-group-item">${task}<span class="remove-task float-right"><i class="fas fa-times"></i></span></li>`;
      
      // Make the ajax request 
      $.ajax({
          url: urlroot + 'php/todo_list.php',
          method: 'POST',
          data: {
            action: 'add_task',
            taskArray: taskArray
          },
          success: function(data){
            
            // Check if it was successful
            if(data == 'SUCCESS'){
              $('#todo_list').append(taskLI);
              ui_message('Aufgabe erfolgreich hinzugefügt.', 'success');
            } else {
              ui_message('Die Aufgabe konnte nicht hinzugefügt werden.', 'danger');
            }

          }
      }); // AJAX
      
    }
  });

  /* #################################################### */
  /* ################### REMOVE TODO #################### */
  /* #################################################### */

  $(document).on('click', '.remove-task', function(){

    const target = $(this).parent();

    // Init variable
    let taskArray = [];

    // For each li in the todo list
    $('#todo_list li').each(function() {

      if($(this).text() == target.text()){
        return true; // continue in JQuery
      }

      // Get the text and store it in a variable
      currentTask = $(this).text();
      // Remove all whitespace from the variable
      currentTask = currentTask.trim();
      // Push it to taskArray
      taskArray.push(currentTask);
    });
    
    // Make the ajax request 
    $.ajax({
        url: urlroot + 'php/todo_list.php',
        method: 'POST',
        data: {
          action: 'remove_task',
          tasks: taskArray
        },
        success: function(data){
          console.log(data);
          // Check if it was successful
          if(data == 'SUCCESS'){
            // Remove the task from the DOM
            // For each li in the todo list
            $('#todo_list li').each(function() {
              if($(this).text() == target.text()){
                $(this).remove();
              }
            });
            // Give the user a feedback alert
            ui_message('Aufgabe wurde erfolgreich entfernt.', 'success');
          } else {
            ui_message('Die Aufgabe konnte nicht hinzugefügt werden.', 'danger');
          }
        }
    }); // AJAX


  });

  $('.deactivate-reminder').on('click', function(){

    console.log('DEACTIVE REMINDER');
  });






  /* ##################################### */
  /* ####### UI MESSAGE FUNCTION ######### */

  function ui_message($message, $class) {

    // Prepare the alert element
    let output = `<div class="alert alert-${$class}" role="alert">${$message}</div>`;

    // Add the alert to the DOM
    $('#todo .ui-output').prepend(output);

    // Remove the alert after 2.5s
    setTimeout(function(){
      $('#todo .ui-output .alert:last-of-type').remove();
    }, 2500);
  }




} // dashboard view





}); // Document.ready
