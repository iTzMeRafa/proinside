 $(document).ready(function(){

/* ############################### */
/* ########  METHODS: ADD   ######## */
/* ############################### */

if($('body').hasClass('addMethod')){
  

  /* ##################################### */
  /* ################ TAGS ############### */
  /* ##################################### */

  // Init tags variable to store tags in for later
  let tags = [];

  // Check if the hiddenInput already has values (on view: edit-errors)
  if($('.hiddenInput').val() != ''){
    // get them (as string), then split them into an array and store it
    const storedTags = $('.hiddenInput').val().split(',');
    storedTags.forEach(function(st){
      tags.push(st);
    });
  }

  // Check for a keyup event on the mainInput
  $('.mainInput').on('keyup', function(){
    // if a comma character is typed split the value of mainInput into an array of tag and comma
    let enteredTags = $('.mainInput').val().split(',');
    // Check if a tag is entered (so, tag + comma = 2)
    if(enteredTags.length > 1){
      enteredTags.forEach(function(t){
        // filter the tag using the filterTag function
        let filteredTag = filterTag(t);
        // Check if the filtered tag is there and does not already occur in the tags array
        if(filteredTag.length > 0 && tags.indexOf(filteredTag) == -1 ){
          // Add it to the tags array
          addTag(filteredTag);
        }
      });
      // Clear the mainInput field for the next input
      $('.mainInput').val('');
    }
  });

  // Check for a click event on the close button of a tag
  $(document).on('click', '.close', function(){
    // Get the tag as a string
    let tag = $(this).parent().text();
    // Remove it from the tags array
    removeTag(tags.indexOf(tag));
    // Remove the tag from the UI
    $(this).parent().remove();
  });


  function addTag(text){
    // Add the text as a tag right before the mainInput
    $('.mainInput').before(`<span class="tag">${text} <i class="close fas fa-times"></i></span>`);
    // push the text to the tags array
    tags.push(text);
    // refresh the tags in the hiddenInput
    refreshTags();
  }

  function removeTag(index){
    // store the tag in a variable
    let tag = tags[index];
    // remove the tag from the tags array
    tags.splice(index, 1);
    // refresh the tags in the tags array
    refreshTags();
  }

  function refreshTags(){
    // Init tagsList array
    let tagsList = [];
    // Loop through the tags array and store each value in the tagsList array
    tags.forEach(function(t){
      tagsList.push(t);
    });
    // Join the tags of tagsList into a string and add it to hiddenInput
    $('.hiddenInput').val(tagsList.join());
  }

  function filterTag(tag){
    // Remove all characters except whitespace, but replaces whitespace with dashes
    return tag.replace(/[^\w -]/g, '').trim().replace(/\W+/g, '-');
  }


  /* ##################################### */
  /* ############# REMINDERS ############# */
  /* ##################################### */
  
  // Check if the noInterval Checkbox is checked (for error reloads)
  if($('#activate_reminders').prop('checked')){
    // Disable the corresponding fieldset
    $('#set_reminders').attr('disabled', false);
  }

  // Listen for click event on noInterval Checkbox
  $('#activate_reminders').on('click', function(){
    // If it is checked
    if(this.checked ){
      // Disable the corresponding fieldset
      $('#set_reminders').attr('disabled', false);
    }

    // If it is unchecked
    if(!(this.checked)){
      // Reenable the corresponding fieldset
      $('#set_reminders').attr('disabled', true);
    }
  });


  /* /////////////////////////// */
  /* ########################### */
  /* ########## ROLES ########## */
  /* ########################### */
  /* /////////////////////////// */

  /* #################################################################### */
  /* ########################## FUNCTIONS ############################### */
  /* #################################################################### */

  /* #################################################################### */
  /* ################## REMOVE ID FROM HIDDEN INPUT ##################### */

  function removeIdFromHiddenInput( $hiddenInput, id ){
    // Get the id-string from hiddenInput und turn it into ann array
    let storedIds = $hiddenInput.val().split(',');
    // Get the index of (the current) id
    let index = storedIds.indexOf(id);

    // If the id is within stored ids
    if( index != -1 ){
      // Remove it from the array
      storedIds.splice(index, 1);
    }

    // Update the hiddenInput value
    $hiddenInput.val( storedIds.toString() );

  }
  

  /* ####################################### */
  /* ########## RESPONSIBLE USERS ########## */

  // Check for button click event
  $('#open_roles_responsible').on('click', function(){

    console.log('RESPONSIBLE');

    /* ######################################################################### */
    /* ############# CHECK ALL CHECKBOXES OF USERS IN HIDDEN INPUT ############# */

    // Uncheck all checkboxes first
    $('#roles_responsible_modal .form-check-input').prop("checked", false);

    // Check if the hidden Input "roles_responsible" is not empty
    if($('#roles_responsible').val() != '' && $('#roles_responsible').val() != null){
      // Get the users from the hidden input
      let storedUsers = $('#roles_responsible').val().split(',');

      setTimeout(function(){
        $('#roles_responsible_modal .form-check-input').each(function(){
          // Store the currentCheckboxValue in a variable
          let currentCheckboxValue = $(this).val();
          // Check if the currentCheckboxValue is within the storedUsers array
          if(storedUsers.indexOf(currentCheckboxValue) != -1){
            // Validated -> check the current Checkbox
            $(this).prop("checked", true);
          }
        });
      }, 900);
    }

    /* ############################################ */
    /* ############### SAVE CHANGES ############### */

    // Check for click event on save changes button
    $('#roles_responsible_modal .saveChanges').on('click', function(){
      console.log('RESPONSIBLE: SAVED');

      // Init array 
      let idArray = [];

      // Empty the list in the UI
      $('#responsibleUI').empty();

      $('#roles_responsible_modal .form-check-input:checked').each(function(){
        console.log( $(this) );

        // Get the value and username (id) of the checkbox
        let value = $(this).val();
        let name = $(this).data('name');
        let organ = $(this).data('organ');

        // Construct list item for output
        let userLI = `
        <li class="list-group-item d-flex" data-id="${value}">
          <p class="mb-0">
            <strong>${name}</strong> (${organ})
          </p>
          <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
        </li>`;
        // Add the list item to the DOM
        $('#responsibleUI').append(userLI);

        // Push the id/value to the idArray
        idArray.push( value );

      });

      // Update the hidden Input
      $('#roles_responsible').val( idArray.toString() );

      // Hide the modal
      $('#roles_responsible_modal').modal('hide');      
    });

  });

  /* ########################################################### */
  /* ################# REMOVE RESPONSIBLE USER ################# */

  $('#responsibleUI').on('click', '.close-btn', function(){
    // Get the uiContainer
    let id = $(this).parent().data('id');

    removeIdFromHiddenInput( $('#roles_responsible'), id );

    // Remove the DOM element
    $(this).parent().remove();

  });

  



  /* ##################################### */
  /* ########## CONCERNED USERS ########## */

  // Check for button click event
  $('#open_roles_concerned').on('click', function(){

    /* ######################################################################### */
    /* ############# CHECK ALL CHECKBOXES OF USERS IN HIDDEN INPUT ############# */

    // Uncheck all checkboxes first
    $('#roles_concerned_modal .form-check-input').prop("checked", false);

    // Check if the hidden Input "roles_concerned" is not empty
    if($('#roles_concerned').val() != '' && $('#roles_concerned').val() != null){
      // Get the users from the hidden input
      let storedUsers = $('#roles_concerned').val().split(',');

      setTimeout(function(){
        $('#roles_concerned_modal .form-check-input').each(function(){
          // Store the currentCheckboxValue in a variable
          let currentCheckboxValue = $(this).val();
          // Check if the currentCheckboxValue is within the storedUsers array
          if(storedUsers.indexOf(currentCheckboxValue) != -1){
            // Validated -> check the current Checkbox
            $(this).prop("checked", true);
          }
        });
      }, 900);
    }

    /* ############################################ */
    /* ############### SAVE CHANGES ############### */

    // Check for click event on save changes button
    $('#roles_concerned_modal .saveChanges').on('click', function(){
      console.log('RESPONSIBLE: SAVED');

      // Init array 
      let idArray = [];

      // Empty the list in the UI
      $('#concernedUI').empty();

      $('#roles_concerned_modal .form-check-input:checked').each(function(){
        console.log( $(this) );

        // Get the value and username (id) of the checkbox
        let value = $(this).val();
        let name = $(this).data('name');
        let organ = $(this).data('organ');

        // Construct list item for output
        let userLI = `
        <li class="list-group-item d-flex" data-id="${value}">
          <p class="mb-0">
            <strong>${name}</strong> (${organ})
          </p>
          <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
        </li>`;
        // Add the list item to the DOM
        $('#concernedUI').append(userLI);

        // Push the id/value to the idArray
        idArray.push( value );

      });

      // Update the hidden Input
      $('#roles_concerned').val( idArray.toString() );

      // Hide the modal
      $('#roles_concerned_modal').modal('hide');      
    });

  });

  /* ########################################################### */
  /* ################## REMOVE CONCERNED USER ################## */

  $('#concernedUI').on('click', '.close-btn', function(){
    // Get the uiContainer
    let id = $(this).parent().data('id');

    removeIdFromHiddenInput( $('#roles_concerned'), id );

    // Remove the DOM element
    $(this).parent().remove();

  });





  /* ######################################## */
  /* ########## CONTRIBUTING USERS ########## */

  // Check for button click event
  $('#open_roles_contributing').on('click', function(){

    console.log('CONTRIBUTING');

    /* ######################################################################### */
    /* ############# CHECK ALL CHECKBOXES OF USERS IN HIDDEN INPUT ############# */

    // Uncheck all checkboxes first
    $('#roles_contributing_modal .form-check-input').prop("checked", false);

    // Check if the hidden Input "roles_responsible" is not empty
    if($('#roles_contributing').val() != '' && $('#roles_contributing').val() != null){
      // Get the users from the hidden input
      let storedUsers = $('#roles_contributing').val().split(',');

      setTimeout(function(){
        $('#roles_contributing_modal .form-check-input').each(function(){
          // Store the currentCheckboxValue in a variable
          let currentCheckboxValue = $(this).val();
          // Check if the currentCheckboxValue is within the storedUsers array
          if(storedUsers.indexOf(currentCheckboxValue) != -1){
            // Validated -> check the current Checkbox
            $(this).prop("checked", true);
          }
        });
      }, 900);
    }

    /* ############################################ */
    /* ############### SAVE CHANGES ############### */

    // Check for click event on save changes button
    $('#roles_contributing_modal .saveChanges').on('click', function(){

      // Init array 
      let idArray = [];

      // Empty the list in the UI
      $('#contributingUI').empty();

      $('#roles_contributing_modal .form-check-input:checked').each(function(){
        console.log( $(this) );

        // Get the value and username (id) of the checkbox
        let value = $(this).val();
        let name = $(this).data('name');
        let organ = $(this).data('organ');

        // Construct list item for output
        let userLI = `
        <li class="list-group-item d-flex" data-id="${value}">
          <p class="mb-0">
            <strong>${name}</strong> (${organ})
          </p>
          <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
        </li>`;
        // Add the list item to the DOM
        $('#contributingUI').append(userLI);

        // Push the id/value to the idArray
        idArray.push( value );

      });

      // Update the hidden Input
      $('#roles_contributing').val( idArray.toString() );

      // Hide the modal
      $('#roles_contributing_modal').modal('hide');     
    });

  });

  /* ############################################################ */
  /* ################# REMOVE CONTRIBUTING USER ################# */

  $('#contributingUI').on('click', '.close-btn', function(){
    // Get the uiContainer
    let id = $(this).parent().data('id');

    removeIdFromHiddenInput( $('#roles_contributing'), id );

    // Remove the DOM element
    $(this).parent().remove();

  });





  /* ####################################### */
  /* ########## INFORMATION USERS ########## */

  // Check for button click event
  $('#open_roles_information').on('click', function(){

    console.log('INFORMATION');

    /* ######################################################################### */
    /* ############# CHECK ALL CHECKBOXES OF USERS IN HIDDEN INPUT ############# */

    // Uncheck all checkboxes first
    $('#roles_information_modal .form-check-input').prop("checked", false);

    // Check if the hidden Input "roles_responsible" is not empty
    if($('#roles_information').val() != '' && $('#roles_information').val() != null){
      // Get the users from the hidden input
      let storedUsers = $('#roles_information').val().split(',');

      setTimeout(function(){
        $('#roles_information_modal .form-check-input').each(function(){
          // Store the currentCheckboxValue in a variable
          let currentCheckboxValue = $(this).val();
          // Check if the currentCheckboxValue is within the storedUsers array
          if(storedUsers.indexOf(currentCheckboxValue) != -1){
            // Validated -> check the current Checkbox
            $(this).prop("checked", true);
          }
        });
      }, 900);
    }

    /* ############################################ */
    /* ############### SAVE CHANGES ############### */

    // Check for click event on save changes button
    $('#roles_information_modal .saveChanges').on('click', function(){
      console.log('INFORMATION: SAVED');

      // Init array 
      let idArray = [];

      // Empty the list in the UI
      $('#informationUI').empty();

      $('#roles_information_modal .form-check-input:checked').each(function(){
        console.log( $(this) );

        // Get the value and username (id) of the checkbox
        let value = $(this).val();
        let name = $(this).data('name');
        let organ = $(this).data('organ');

        // Construct list item for output
        let userLI = `
        <li class="list-group-item d-flex" data-id="${value}">
          <p class="mb-0">
            <strong>${name}</strong> (${organ})
          </p>
          <i class="close-btn fas fa-times ml-auto align-self-center"></i></li>
        </li>`;
        // Add the list item to the DOM
        $('#informationUI').append(userLI);

        // Push the id/value to the idArray
        idArray.push( value );

      });

      console.log(idArray);

      // Update the hidden Input
      $('#roles_information').val( idArray.toString() );

      // Hide the modal
      $('#roles_information_modal').modal('hide');      
    });

  });

  /* ############################################################ */
  /* ################## REMOVE INFORMATION USER ################# */

  $('#informationUI').on('click', '.close-btn', function(){
    // Get the uiContainer
    let id = $(this).parent().data('id');

    removeIdFromHiddenInput( $('#roles_information'), id );

    // Remove the DOM element
    $(this).parent().remove();

  });

  /* ######################################## */
  /* ########## REQUIRED KNOWLEDGE ########## */
  /* ######################################## */


  // Check for click event
  $('#open_knowledge_browser').on('click', function(){
    // Make an AJAX request to fetch a select list of the databrowser
    $.ajax({
      method: 'POST',
      url: urlroot + 'php/fetch-selectlist_new.php',
      data:{
        action: 'fetch_select_list',
        targetId: 0
      },
      success: function(data) {
        // Display the selectlist
        $('#knowledge_browser #folders').html(data);
      }
    });
  });

  // Navigate inside the data browser
  $('#knowledge_browser').on('click', '.directory', function(){
    // Get the targetId
    let targetId = $(this).data('id');
    // Get the parentId
    let parentId = $(this).data('parent');
    // Make AJAX request to get the updated folder structure
    $.ajax({
      method: 'POST',
      url: urlroot + 'php/fetch-selectlist_new.php',
      data:
      {
        action: 'fetch_select_list',
        targetId: targetId,
        parentId: parentId
      },
      success: function(data) {
        // Display the updated selectlist
        $('#knowledge_browser #folders').html(data);
      }
    });

  });

  // Select Files and Folders
  // listen for a change event on the checkboxes

  // Init resourcesArray to store the resources in
  let knowledgeArray = [];

  function processKnowledgeObject($object, $remove = false) {

    // Get the required data from the html elements
    let knowledgeObject = $($object).parent().siblings('.col-md-9');
    let knowledgeId = knowledgeObject.data('id');
    let knowledgeName = knowledgeObject.text();
    // Init resource variable
    let knowledge;

    // Determine the type of knowledge (folder or file)
    if(knowledgeObject.hasClass('file')){
      knowledge = `<li class="list-group-item list-group-item-secondary d-flex align-items-center file" data-id="${knowledgeId}"><i class="fas fa-file mr-2"></i>${knowledgeName}<i class="close-btn fas fa-times ml-auto"></i></li>`;
    } else {
      knowledge = `<li class="list-group-item list-group-item-secondary d-flex align-items-center folder" data-id="${knowledgeId}"><i class="fas fa-folder mr-2"></i> ${knowledgeName}<i class="close-btn fas fa-times ml-auto"></i></li>`;
    }

    if($remove){
      // Get the index of the knowledge item in the knowledgeArray
      let index = knowledgeArray.indexOf(knowledge);
      // Remove the knowledge item from the knowledgeArray
      knowledgeArray.splice(index, 1);
    } else {
      // Add the knowledge item to the knowledgeArray
      knowledgeArray.push(knowledge);
    }
  }

  $('#knowledge_browser').on('change', '.selectbox', function(){
    // If the selectbox is checked
    if(this.checked){
      processKnowledgeObject(this);
    }

    // If the selectbox is unchecked
    if(!(this.checked)){
      processKnowledgeObject(this, true);
    }
  });

  $('#knowledge_browser .saveChanges').on('click', function(){
    // Loop through the knowledgeArray and add each resource to the DOM
    knowledgeArray.forEach(function(knowledge){
      $('#stored_knowledge ul').append(knowledge);
    });
    // Close the modal
    $('#knowledge_browser').modal('hide');
    // Empty the resourcesArray
    knowledgeArray = [];

    saveKnowledge();

  });

  $('#stored_knowledge').on('click', '.close-btn', function(){
    // Remove the DOM element
    $(this).parent().remove();
    saveKnowledge();

  });

  function saveKnowledge(){
    // Init storedPaths variables
    let storedFileIds= '';
    let storedFolderIds = '';

    // Loop through all list items in the DOM
    $('#stored_knowledge .list-group-item').each(function(){ 

      // Get the id
      let id = $(this).data('id');

      // If the current item holds the link to a single file
      if($(this).hasClass('file')){
        // Add the path to the storedFilePaths variable and seperate it by '|'
        storedFileIds += `${id}|`;
      } else {
        // Add the path to the storedFolderPaths variable and seperate it by '|'
        storedFolderIds += `${id}|`;
      }   
    });

    // Put storedPaths as a value in the hidden input
    $('#required_knowledge').val(storedFileIds);
    $('#required_knowledge_folders').val(storedFolderIds);
  }



  /* ######################################## */
  /* ########## REQUIRED RESOURCES ########## */
  /* ######################################## */


  // Check for click event
  $('#open_resource_browser').on('click', function(){
    // Make an AJAX request to fetch a select list of the databrowser
    $.ajax({
      method: 'POST',
      url: urlroot + 'php/fetch-selectlist_new.php',
      data:{
        action: 'fetch_select_list',
        targetId: 0
      },
      success: function(data) {
        // Display the selectlist
        $('#resource_browser #folders').html(data);
      }
    });
  });

  // Navigate inside the data browser
  $('#resource_browser').on('click', '.directory', function(){
    // Get the targetId
    let targetId = $(this).data('id');
    // Get the parentId
    let parentId = $(this).data('parent');
    // Make AJAX request to get the updated folder structure
    $.ajax({
      method: 'POST',
      url: urlroot + 'php/fetch-selectlist_new.php',
      data:
      {
        action: 'fetch_select_list',
        targetId: targetId,
        parentId: parentId
      },
      success: function(data) {
        // Display the updated selectlist
        $('#resource_browser #folders').html(data);
      }
    });

  });

  // Select Files and Folders
  // listen for a change event on the checkboxes

  // Init resourcesArray to store the resources in
  let resourcesArray = [];

  function processResourceObject($object, $remove = false) {
    // Get the required data from the html elements
    let resourceObject = $($object).parent().siblings('.col-md-9');
    let resourceId = resourceObject.data('id');
    let resourceName = resourceObject.text();
    // Init resource variable
    let resource;

    // Determine the type of resource (folder or file)
    if(resourceObject.hasClass('file')){
      resource = `<li class="list-group-item list-group-item-secondary d-flex align-items-center file" data-id="${resourceId}"><i class="fas fa-file mr-2"></i>${resourceName}<i class="close-btn fas fa-times ml-auto"></i></li>`;
    } else {
      resource = `<li class="list-group-item list-group-item-secondary d-flex align-items-center folder" data-id="${resourceId}"><i class="fas fa-folder mr-2"></i> ${resourceName}<i class="close-btn fas fa-times ml-auto"></i></li>`;
    }

    if($remove){
      // Get the index of the resource item in the resourcesArray
      let index = knowledgeArray.indexOf(resource);
      // Remove the resource item from the resourcesArray
      resourcesArray.splice(index, 1);
    } else {
      // Add the resource item to the resourcesArray
      resourcesArray.push(resource);
    }
  }

  $('#resource_browser').on('change', '.selectbox', function(){
    // If the selectbox is checked
    if(this.checked){
      processResourceObject(this);
    }

    // If the selectbox is unchecked
    if(!(this.checked)){
      processResourceObject(this, true);
    }
  });

  $('#resource_browser .saveChanges').on('click', function(){
    // Loop through the resourcesArray and add each resource to the DOM
    resourcesArray.forEach(function(resource){
      $('#stored_resources ul').append(resource);
    });
    // Close the modal
    $('#resource_browser').modal('hide');
    // Empty the resourcesArray
    resourcesArray = [];

    saveResources();
  });

  $('#stored_resources').on('click', '.close-btn', function(){
    // Remove the DOM element
    $(this).parent().remove();
    saveResources();

  });

  function saveResources(){
    // Init storedPaths variables
    let storedFileIds = '';
    let storedFolderIds = '';

    // Loop through all list items in the DOM
    $('#stored_resources .list-group-item').each(function(){

      // Get the id
      let id = $(this).data('id');

      // If the current item holds the link to a single file
      if($(this).hasClass('file')){
        // Add the path to the storedFilePaths variable and seperate it by '|'
        storedFileIds += `${id}|`;
      } else {
        // Add the path to the storedFolderPaths variable and seperate it by '|'
        storedFolderIds += `${id}|`;
      }
    });

    // Put storedPaths as a values in the hidden inputs
    $('#required_resources').val(storedFileIds);
    $('#required_resources_folders').val(storedFolderIds);
  }

  /* ############################################# */
  /* ############## SAVE QUESTION ################ */

  $('#question_modal .saveChanges').click(function(){

    // Init variables
    let question;
    let yesNo = false;
    let notify = false;
    let remind = false;
    let output = '';

    // Get Question
    question = $('#question').val().trim();
    
    // Get Values if respective checkbox is checked
    if($('#yes_no_checkbox').is(':checked')) {
      yesNo = true;
      output += 'Ja/Nein, ';
    }
    if($('#notify_checkbox').is(':checked')) {
      notify = true;
      output += 'Mitarbeiterbenachrichtigung, ';
    }
    if($('#remind_checkbox').is(':checked')) {
      remind = true;
      output += 'Erinnerung, ';
    }

    // Slice the last comma off the string
    output = output.slice(0, -2);

    // Prepare current Question 
    let currentQuestion = `${question}/!/YN:${yesNo}/!/N:${notify}/!/R:${remind}`;

    // Prepare DOM Element 
    let questionLI = `<li class="list-group-item list-group-item-secondary d-flex align-items-center folder" data-qstring="${currentQuestion}"><p class="mb-0"><strong>Frage:</strong> ${question}</p><p class="mb-0 ml-2"><strong>Antwortmöglichkeiten:</strong> ${output}</p><i class="close-btn fas fa-times ml-auto"></i></li>`;

    // Clear the modal values
    $('#question').val('');
    $('#yes_no_checkbox').prop('checked', false);
    $('#notify_checkbox').prop('checked', false);
    $('#remind_checkbox').prop('checked', false);

    // Get stored questions from hidden Input
    let storedQuestions = $('#flexible_questions').val();

    // Concat currentQuestion to storedQuestions
    storedQuestions += `${currentQuestion}||`;
    // Add it back 
    $('#flexible_questions').val(storedQuestions);

    // Add question element to the UI
    $('#added_questions ul').append(questionLI);
    // Close modal
    $('#question_modal').modal('hide');


  });

  /* ############################################# */
  /* ########### REMOVE ADDED QUESTION ########### */

  $(document).on('click', '#added_questions li .close-btn', function(){

    // Get the current question
    let questionString = $(this).parent().data('qstring');
    // Add the '||' characters as seperators
    questionString += '||';
    // Get the stored questions string from Input
    let storedQuestions = $('#flexible_questions').val();

    // Remove the current question from $storedQuestions
    let newValue = storedQuestions.replace(questionString, '');
    // Replace the Input value with newValue
    $('#flexible_questions').val(newValue);

    // Remove the LI from the DOM
    $(this).parent().remove();
  });

} // addMethod








}); // Document.ready
