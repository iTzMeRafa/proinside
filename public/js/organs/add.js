$(document).ready(function(){

/* ############################### */
/* ########  ORGANS: ADD   ####### */
/* ############################### */

if($('body').hasClass('addOrgan')){

  /* ####################################################### */
  /* ################# SELECT MANAGEMENT  ################## */

  // Check for button click event
  $('#open_management_modal').on('click', function(){

    // Get DOM elements 
    let $hiddenInput = $('#management_level');
    let $ui_List = $('#managementUI');
    let $ui_list_items = $('#managementUI').children();

    // Init variables
    let managementUsers = [];
    let managementNames = [];
    
    ////////////////////////////////
      // GET THE USERS via AJAX
    ///////////////////////////////

    // Make an ajax request to get a select list of users
    $.ajax({
      url: urlroot + 'php/organs.php',
      method: 'POST',
      data:{
        action: 'getAllUsers'
      },
      success: function(data){
        $('#management_modal .modal-list').html(data);
      }
    });

    ////////////////////////////////////
      // GET VALUE FROM HIDDEN INPUT
    ///////////////////////////////////

    // Check if the hidden Input "management_level" is not empty
    if( $hiddenInput.val() != '' && $hiddenInput.val() != null ){
      // Get the users from the hidden input
      let storedUsers = $hiddenInput.val().split(',');
      // Add the array of stored users to managementUsers
      managementUsers = storedUsers;
      // Fill the managementNames array with the names of the LIs in the UI
      if(managementNames.length == 0){
        $ui_list_items.each(function(){
          // Get the name
          let name = $(this).text();
          // Push it to the array
          managementNames.push(name);
        });
      }

      console.log(managementNames);

      setTimeout(function(){
        $('#management_modal .form-check-input').each(function(){
          // Store the currentCheckboxValue in a variable
          let currentCheckboxValue = $(this).val();
          // Check if the currentCheckboxValue is within the storedUsers array
          if(storedUsers.indexOf(currentCheckboxValue) != -1){
            // Validated -> check the current Checkbox
            $(this).prop("checked", true);
          }
        });
      }, 1000);
    }



    // Dynamic content inside the modal --> event delegation to catch the event
    // Check for a checkbox change event
    $('#management_modal').on('change', '.form-check-input', function(){
      // Check if the checkbox is checked
      if(this.checked){
        // Get the value of the checkbox
        let value = $(this).val();
        let name = $(this).data('name');
        // Add it to the users array
        if(managementUsers.indexOf(value) == -1){
          managementUsers.push(value);
        }
        // Add it to the names array if not already in it
        if(managementNames.indexOf(name) == -1){
          managementNames.push(name);
        }
        console.log(managementNames);
        console.log(managementUsers);
      }

      // Check if the checkbox is unchecked
      if(!(this.checked)){
        // Get the value
        let value = $(this).val();
        let name = $(this).data('name');
        // Find the value's index in the users array
        let index = managementUsers.indexOf(value);
        let nameIndex = managementNames.indexOf(name);
        // Remove the value from the users array
        if(index != -1){
          managementUsers.splice(index, 1);
        }
        // Remove the name from the names array
        if(nameIndex != -1){
          managementNames.splice(nameIndex, 1);
        }
        console.log(managementNames);
        console.log(managementUsers);
      }

    });

    /* #################################################### */
    /* ################# SAVE CHANGES  #################### */

    // Check for click event on save changes button
    $('#management_modal #saveChanges').on('click', function(){
      console.log('SAVED');
      // Store the value of the users array in the management_level hidden input as a string
      $hiddenInput.val(managementUsers.join());

      // Empty the list in the UI
      $ui_List.empty();
      // Loop through the names array
      $.each(managementNames, function(index, value){
        // Get the userId from the managementUsers array with the same index as in managementNames
        let userId = managementUsers[index];

        // Prepare the List Item with the userId and userName
        let userLI = `<li class="list-group-item d-flex align-items-center" data-id="${userId}">${value}<i class="close-btn fas fa-times ml-auto"></i></li>`;
        
        // Add the list item to the DOM/UI
        $ui_List.append(userLI);
      });

      // Clear variables 
      staffUsers = [];
      staffNames = [];

      // Hide the modal
      $('#management_modal').modal('hide');
    });

  });


  /* ########################################################## */
  /* ################# REMOVE MANAGEMENT USER ################# */

  $('#managementUI').on('click', '.close-btn', function(){
    // Get the uiContainer
    let $uiContainer = $('#managementUI');
    let $hiddenInput =  $('#management_level');

    // Remove the list-item element
    $(this).parent().remove();
    saveManagers($uiContainer, $hiddenInput);

  });

  


  /* ######################################################## */
  /* ################### SELECT STAFF  ###################### */  

  // Check for button click event

  $('#open_staff_modal').on('click', function(){

    // Get required DOM elements 
    $hiddenInput = $('#staff');
    $ui_List = $('#staffUI');
    $ui_list_items = $('#staffUI').children();

    // Init variables
    let staffUsers = [];
    let staffNames = [];

    ////////////////////////////////
      // GET THE USERS via AJAX
    ///////////////////////////////

    // Make an ajax request to get a select list of users
    $.ajax({
      url: urlroot + 'php/organs.php',
      method: 'POST',
      data:{
        action: 'getAllUsers'
      },
      success: function(data){
        $('#staff_modal .modal-list').html(data);
      }
    });

    /////////////////////////////////////
        // GET VALUE FROM HIDDEN INPUT
    /////////////////////////////////////

    // Check if the hidden Input "staff" is not empty
    if( $hiddenInput.val() != '' && $hiddenInput.val() != null){

      // Get the users from the hidden input
      let storedUsers = $hiddenInput.val().split(',');
      // Add the array of stored users to staffUsers
      staffUsers = storedUsers;
      // Fill the staffNames array with the names of the LIs in the UI
      if(staffNames.length == 0){
        $ui_list_items.each(function(){
          // Get the name
          let name = $(this).text();
          // Push it to the array
          staffNames.push(name);
        });
      }

      console.log(staffNames);
      console.log(staffUsers);

      setTimeout(function(){
        $('#staff_modal .form-check-input').each(function(){
          // Store the currentCheckboxValue in a variable
          let currentCheckboxValue = $(this).val();
          // Check if the currentCheckboxValue is within the storedUsers array
          if(staffUsers.indexOf(currentCheckboxValue) != -1){
            // Validated -> check the current Checkbox
            $(this).prop("checked", true);
          }
        });
      }, 1000);
    }

    // Dynamic content inside the modal --> event delegation to catch the event
    // Check for a checkbox change event
    $('#staff_modal').on('change', '.form-check-input', function(){
      // Check if the checkbox is checked
      if(this.checked){
        // Get the value of the checkbox
        let value = $(this).val();
        let name = $(this).data('name');
        // Add it to the users array
        if(staffUsers.indexOf(value) == -1){
          staffUsers.push(value);
        }
        // Add it to the names array if not already in it
        if(staffNames.indexOf(name) == -1){
          staffNames.push(name);
        }
        console.log(staffUsers);
        console.log(staffNames);
      }

      // Check if the checkbox is unchecked
      if(!(this.checked)){
        // Get the value
        let value = $(this).val();
        let name = $(this).data('name');
        // Find the value's index in the users array
        let index = staffUsers.indexOf(value);
        let nameIndex = staffNames.indexOf(name);
        // Remove the value from the users array
        if(index != -1){
          staffUsers.splice(index, 1);
        }
        // Remove the name from the names array
        if(nameIndex != -1){
          staffNames.splice(nameIndex, 1);
        }
        console.log(staffUsers);
        console.log(staffNames);
      }

    });

    /* ################################################ */
    /* ################# SAVE CHANGES ################# */

    // Check for click event on save changes button
    $('#staff_modal #saveChanges').on('click', function(){
      console.log('SAVED');
      // Store the value of the users array in the management_level hidden input as a string
      $hiddenInput.val(staffUsers.join());


      // Empty the list in the UI
      $ui_List.empty();
      // Loop through the names array
      $.each(staffNames, function(index, value){
        // Get the userId from the managementUsers array with the same index as in managementNames
        let userId = staffUsers[index];

        // Prepare the List Item with the userId and userName
        let userLI = `<li class="list-group-item d-flex align-items-center" data-id="${userId}">${value}<i class="close-btn fas fa-times ml-auto"></i></li>`;
        
        // Add the list item to the DOM/UI
        $ui_List.append(userLI);
      });

      console.log($ui_List);

      // Clear variables 
      staffUsers = [];
      staffNames = [];
      
      // Hide the modal
      $('#staff_modal').modal('hide');
    });

  });

  /* ##################################################### */
  /* ################# REMOVE STAFF USER ################# */

  $('#staffUI').on('click', '.close-btn', function(){
    // Get the uiContainer
    let $uiContainer = $('#staffUI');
    let $hiddenInput =  $('#staff');

    // Remove the DOM element
    $(this).parent().remove();
    saveStaff($uiContainer, $hiddenInput);

  });


  /* ######################################################################## */
  /* ############################# FUNCTIONS  ############################### */
  /* ######################################################################## */

  /* #################################################### ##### */
  /* #################### SAVE MANAGERS  ###################### */

  function saveManagers($uiContainer, $hiddenInput){
    // Get the list
    let $list = $uiContainer.children('.list-group-item');
    console.log($list);

    // Init storedPaths variables
    let storedUserIds = '';

    // Loop through all list items in the DOM
    $list.each(function(){

      // Get the id
      let id = $(this).data('id');

      // Add the path to the storedUserIds variable and seperate it by '|'
      storedUserIds += `${id},`;
    });

    // Remove the last comma from $storeUserIds
    storedUserIds = storedUserIds.substr(0, storedUserIds.length -1 );

    // Put storedUserIds as values in the hidden input
    $hiddenInput.val(storedUserIds);
  }

  /* ####################################################### */
  /* #################### SAVE STAFF  ###################### */

  function saveStaff($uiContainer, $hiddenInput){
    // Get the list
    let $list = $uiContainer.children('.list-group-item');

    // Init storedPaths variables
    let storedUserIds = '';

    // Loop through all list items in the DOM
    $list.each(function(){

      // Get the id
      let id = $(this).data('id');

      // Add the path to the storedUserIds variable and seperate it by '|'
      storedUserIds += `${id},`;
    });

    // Remove the last comma from $storeUserIds
    storedUserIds = storedUserIds.substr(0, storedUserIds.length -1 );

    console.log(storedUserIds);

    // Put storedUserIds as values in the hidden input
    $hiddenInput.val(storedUserIds);
  }


 


  


} // addOrgan
}); // Document.ready
