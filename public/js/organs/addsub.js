$(document).ready(function(){

  // REGULAR SUBORGANS
  let is_staff_function = false

  // CHECK IF STAFF FUNCTIONS ARE BEING EDITED
  if( $('#staff-functions').length ){
    is_staff_function = true
  }


    /* ########################################### */
    /* ############## ADD NEW SUBORGAN ########### */

    $('#open_suborgan_modal').on('click', function(){
      // Make sure value of existing class is empty
      $('#existing_id').val('');

      // Reset the modal to default
      $('#sub_name').val('');
      $('#sub_abbr').val('');
      $('#user_list .form-check-input').prop('checked', false);
      // Remove alert if existing
      $('#suborgan_modal .row').children('.alert').remove();

      if( is_staff_function ){
        // STAFF FUNCTIONS
        // Change the modal title and button text 
        $('#suborgan_modal .modal-title').text('Stabsfuntion hinzufügen');
        $('#saveChanges').text('Stabsfuntion hinzufügen');
      } else {
        // REGULAR SUBORGANS
        // Change the modal title and button text 
        $('#suborgan_modal .modal-title').text('Suborgan hinzufügen');
        $('#saveChanges').text('Organ hinzufügen');
      }

      
    });


    /* ############################################## */
    /* ############## MODAL: SAVE CHANGES ########### */

    $('#suborgan_modal #saveChanges').on('click', function(){

      
      let idArr = [];
      let nextId;

      $('#added_suborgans .suborgan').each(function(){
        // Get the id of the current suborgan
        let id = $(this).attr('id');
        // Remove the first 4 characters = only the number
        id = id.substring(4);
        // Add it to the id Array
        idArr.push(id);
      });

      if( idArr.length != 0 ){
        // Sort the numbers in the array in descending order
        idArr.sort(function(a, b){return b-a});    
        // Store the next free id 
        nextId =  parseInt(idArr[0])+1;
      } else {
        nextId = 1;
      }

      ////////////////////////////
        // GET VALUES
      ////////////////////////////

      // Get the name and the abbreviation
      let sub_name = $('#sub_name').val().trim();
      let abbreviation = $('#sub_abbr').val();

      // Init variable
      let assigned_users = '';
      let user_ids = '';

      // Loop through all the worker-checkboxes
      $('#user_list .form-check-input').each(function(){
        
        // If the current checkbox is checked
        if(this.checked){
          // Get name and value and user id      
          let name = $(this).next('.form-check-label').text();

          user_ids += $(this).val()+',';

          // Create a list item DOM Element and add it to the assigned_user string
          assigned_users += `<li class="list-group-item">${name}</li>`;
        }
        
      });

      // Remove the last comma from the user_ids
      user_ids = user_ids.substring(0, user_ids.length - 1);

      // VALIDATE COMPLETENESS 
      // Check if there is a suborgan name and at least one user assigned
      if( sub_name == '' ||  user_ids < 1 ){
        
        // INVALID
        // Display error message
        // Make sure to only display one error alert and not more
        if( $('#suborgan_modal .row').children('.alert').length == 0 ){
          // Prepare alert
          let alert = `<div class="alert alert-danger alert-dismissible fade show w-100 mx-2" role="alert"> Um ein Suborgan zu erstellen wird ein Name und mindestens ein Mitarbeiter benötigt <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>`;
          // Add the alert to the DOM
          $('#suborgan_modal .row').prepend(alert);
        }
        
      } else {
        // VALID

        // Check if the current organ is already existing
        if( $('#existing_id').val() != '' ){
          // EXISTING
          // Get the id of the existing DOM Element
          let existingId = $('#existing_id').val();

          // Update the properties
          $(`#${existingId} .nameUI`).text(sub_name);
          $(`#${existingId} .name`).val(sub_name);
          $(`#${existingId} .abbrUI`).text(abbreviation);
          $(`#${existingId} .abbreviation`).val(abbreviation);
          $(`#${existingId} .workersUI`).html(`<li class="list-group-item active">Mitarbeiter</li>${assigned_users}`);
          $(`#${existingId} .workers`).val(user_ids);


        } else {
          // NEW
          // Create the new DOM Element
          let new_suborgan = `<div id="sub-${nextId}" class="suborgan col-md-4 mb-3 d-flex flex-column">
                                <div class="border p-3 d-flex flex-column">
                                  <span class="remove_organ ml-auto"><i class="fas fa-times"></i></span>
                                  <p class="h5">Name:</p>
                                  <p class="h5"><span class="nameUI font-weight-normal">${sub_name}</span></p>
                                  <input class="name" name="${nextId}-name" type="hidden" value="${sub_name}">
                                  <p class="h5">Kürzel:</p>
                                  <p class="h5"><span class="abbrUI font-weight-normal">${abbreviation}</span></p>
                                  <input class="abbreviation" name="${nextId}-abbreviation" type="hidden" value="${abbreviation}">
                                  <ul class="workersUI list-group mt-2">
                                    <li class="list-group-item active">Mitarbeiter</li>
                                    ${assigned_users}
                                  </ul>
                                  <input class="workers" name="${nextId}-workers" type="hidden" value="${user_ids}">
                                  <button type="button" class="editSubOrgan btn btn-pe-darkgreen d-block w-100 mt-3" data-toggle="modal" data-target="#suborgan_modal">Suborgan bearbeiten</button>
                                </div>
                              </div>`;
        
          // Add the new suborgan to the DOM
          $('#added_suborgans').append(new_suborgan);
        }   
      

        // Reset the modal to default
        $('#sub_name').val('');
        $('#sub_abbr').val('');
        $('#user_list .form-check-input').prop('checked', false);
        // Remove alert if existing
        $('#suborgan_modal .row').children('.alert').remove();

        // Make sure a maximum of eight staff functions can be added
        if( is_staff_function && $('.suborgan').length <= 8 ){
          // Remove the 'add staff functions'-button
          $('#open_suborgan_modal').removeClass('d-block');
          $('#open_suborgan_modal').addClass('d-none');
        }

        // Hide the modal
        $('#suborgan_modal').modal('hide');
      }
        
    });



    /* ########################################## */
    /* ############## REMOVE SUBORGAN ########### */

    // Listen for click on remove organ button
    $(document).on('click', '.remove_organ', function(){
      // Get the whole Suborgan DOM Element
      let $parent = $(this).parent().parent('.suborgan');

      if( is_staff_function && $('.suborgan').length <= 8 ){
        // Remove the 'add staff functions'-button
        $('#open_suborgan_modal').removeClass('d-none');
        $('#open_suborgan_modal').addClass('d-block');
      }

      // Remove it from the DOM
      $parent.remove();
      
    });

    /* ######################################### */
    /* ############## EDIT SUBORGAN ############ */

    $(document).on('click', '.editSubOrgan', function(){
      
      // Get all the required organ properties
      let name = $(this).siblings('.name').val();
      let abbreviation = $(this).siblings('.abbreviation').val();
      let workers = $(this).siblings('.workers').val();
      // Turn the string of workers into an array of user ids
      let users = workers.split(',');

      /* ###### PARENT ID ####### */
      // Get all the classes of the existing suborgan element
      let parentId = $(this).parent().parent('.suborgan').attr('id');
    
      /* ######## WITHIN THE MODAL ########## */
      
      // Display the data in the modal

      $('#existing_id').val(parentId);
      $('#sub_name').val(name);
      $('#sub_abbr').val(abbreviation);
      
      // Loop through all the worker-checkboxes in the modal
      $('#user_list .form-check-input').each(function(){
        // Get the current inputs user id
        let userId = $(this).val();
        // Get the index of the current user id in the "users" array
        let index = users.indexOf(userId);
        
        // Check if the user is in the "users" array
        if(index != -1){
          // PRESENT
          // check the checkbox
          $(this).prop('checked', true);
        } else {
          // ABSENT
          // uncheck the checkbox
          $(this).prop('checked', false);
        }
        
      });

      // Change the modal title and button text
      $('#suborgan_modal .modal-title').text('Suborgan bearbeiten');
      $('#saveChanges').text('Organ aktualisieren');    

    });





});