$(document).ready(function () {

  /**
   * Listens on the show manager button-click for organs/suborgans
   * Gets the managers-list by data attribute and displays them
   */
  $('#showSubOrganManagersButton, #showOrganManagersButton').click(function (e) {
    // Stop parent node from collapsing the childs in organigram
    e.stopPropagation();
    e.preventDefault();

    // Initialize elements and data
    const managersList    = $(this).data('managers');
    const managersListArr = managersList.split(', ');
    const managerListEl   = document.getElementById('managersList');
    const ul              = document.createElement('ul');
    managerListEl.innerHTML = '';
    ul.className = 'list-group';

    // Build the list group if the managers list is not empty
    if (managersList) {
      managersListArr.map((name) => {
        const li              = document.createElement('li');
        const managerTextNode = document.createTextNode(name);
        li.className          = 'list-group-item';
        li.appendChild(managerTextNode);
        ul.appendChild(li);
      });

      managerListEl.appendChild(ul);
    }

    // Show modal
    $('#showManagersModal').modal('show')
  })


});