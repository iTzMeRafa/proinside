$(document).ready(function(){

  /* ############################################ */
  /* ########### DEACTIVATE PRIVACY ############# */

  $(document).on('click', '#deactivatePrivacy', function(){

    // Make an ajax request to get a select list of users
    $.ajax({
      url: urlroot + 'php/organs.php',
      method: 'POST',
      data:{
        action: 'deactivatePrivacy'
      },
      success: function(data){
        $('#main').prepend(data);
        $("#deactivatePrivacy").replaceWith( '<button id="activatePrivacy" type="button" class="btn btn-pe-lightgreen">Aktivieren</button>' );
      }
    });

  });

  /* ########################################## */
  /* ########### ACTIVATE PRIVACY ############# */

  $(document).on('click', '#activatePrivacy', function(){

    // Make an ajax request to get a select list of users
    $.ajax({
      url: urlroot + 'php/organs.php',
      method: 'POST',
      data:{
        action: 'activatePrivacy'
      },
      success: function(data){
        $('#main').prepend(data);
        $("#activatePrivacy").replaceWith( '<button id="deactivatePrivacy" type="button" class="btn btn-danger">Deaktivieren</button>' );
      }
    });

  });







});