$(document).ready(function(){

  /* ################################ */
  /* ########  PERMS : ADD   ######## */
  /* ################################ */

  /* ################################################## */
  /* ############# OPEN FOLDER PERM MODAL ############# */ 

  // Check for button click event
  $('#open_folder_perm_modal').on('click', function(){

    // Get the hidden Input for the folder access perms
    let $input = $('#folderAccessPerms');
    // Get the hidden Input for globalAccess 
    let $globalInput = $('#globalAccess');

    // Get the folder ids from the hidden Input as a string
    let folder_ids = $input.val();
    let hasGlobalAccess = $globalInput.val();

    let folder_id_array = [];
    let folder_name_array = [];
    

    // Check if folder_ids is more than one number
    if(folder_ids.length > 2 ){
      // MORE THAN ONE
      // split the string and add every number to the array
      folder_id_array = folder_ids.split(',');
    } else if(folder_ids.length != 0) {
      // ONLY ONE;
      // push the single number to the array
      folder_id_array.push(folder_ids);
    }      
    
    console.log(folder_id_array);
    console.log(folder_name_array);

    // Check the corresponding checkboxes of the roles that are assigned to the user
    $('#listOfRoles .form-check-input').each(function(){
      let value = $(this).val();

      if(role_id_array.indexOf(value) != -1){
        $(this).prop('checked', true);
      } else {
        $(this).prop('checked', false);
      }
    });

    // Check the corresponding checkboxes of the roles that are assigned to the user
    $('#folderList .form-check-input').each(function(){
      let value = $(this).val();

      if(folder_id_array.indexOf(value) != -1){
        $(this).prop('checked', true);
      } else {
        $(this).prop('checked', false);
      }
    });

    // If global Access is selected
    if( hasGlobalAccess == 'true' ){
      console.log('TRUE GLOBAL');
      // Disable all checkboxes
      $('#folderList .form-check-input').attr('disabled', true);
      // Enable the global Access checkbox
      $('#global-check').attr('disabled', false);
      // Check the corresponding checkbox in the modal
      $('#global-check').prop('checked', true);
      
    }

    /* ################################################# */
    /* ############# CHECKBOX CHANGE EVENT ############# */ 

    $('#folderList').on('change', '.form-check-input', function(){

      // Get the value and rolename (id) of the checkbox
      let value = $(this).val();
      let name = $(this).data('name');       

      // Check if the checkbox is checked
      if(this.checked){
        
        // If global access is targeted
        if( value == 'global' ){
          // GLOBAL TARGETED

          // Set all other checkboxes to invalid
          $('#folderList .form-check-input').attr('disabled', true);
          $(this).attr('disabled', false);

          globalAccess = true;
        } else {
          // SPECIFIC FOLDER TARGETED

          // Add it to the users array if not already in it
          if(folder_id_array.indexOf(value) == -1){
            folder_id_array.push(value);
          }
          // Add it to the names array if not already in it
          if(folder_name_array.indexOf(name) == -1){
            folder_name_array.push(name);
          }
  
          console.log(folder_id_array);
          console.log(folder_name_array);
        }
      }

      // Check if the checkbox is unchecked
      if(!(this.checked)){
        
        // If global access is targeted
        if( value == 'global' ){
          // GLOBAL TARGETED

          // Set all other checkboxes to valid again
          $('#folderList .form-check-input').attr('disabled', false);

          globalAccess = false;
        } else {
          // SPECIFIC FOLDER TARGETED

          // Find the value's index in the users array
          let index = folder_id_array.indexOf(value);
          let nameIndex = folder_name_array.indexOf(name);
          // Remove the value from the users array
          if(index != -1){
            folder_id_array.splice(index, 1);
          }
          // Remove the name from the names array
          if(nameIndex != -1){
            folder_name_array.splice(nameIndex, 1);
          }

          console.log(folder_id_array);
          console.log(folder_name_array);
        }
      }

    });

    /* ################################################# */
    /* ############### SAVE CHANGES EVENT ############## */ 

    // Check for click event on save changes button
    $('#folder_perm_modal .saveChanges').on('click', function(){
      console.log('SAVED');

      // Check if Global Access has been selected
      if( globalAccess == true ){
        // GLOBAL ACCESS
        // Add the respective HTML elements to the UI
        $('#accessList').html('<li class="list-group-item">Alle Ordner</li>');
        // Set globalInput to true
        $globalInput.val('true');
      } else {
        // RESTRICTED ACCESS
        // Set globalInput to false
        $globalInput.val('false');

        // Turn the ids into a comma seperated string and store it in the hidden input
        $input.val( folder_id_array.join() );

        // Turn the role names array into a comma seperated string and store it in a variable
        let output = '';

        // Loop through all the names of the selected folders
        $.each(folder_name_array, function(index, value){
          // Add the respective UI element to the output variable
          output += `<li class="list-group-item">${value}</li>`;
        });

        // Add the output to the DOM
        $('#accessList').html(output);

      }
      
      $('#folder_perm_modal').modal('hide');
    });

  });
});