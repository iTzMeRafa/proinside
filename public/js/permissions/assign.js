$(document).ready(function(){

  /* ############################### */
  /* ########  PERMS : ASSIGN   ######## */
  /* ############################### */
  
  if($('body').hasClass('permissions-assign')){


    // Check for button click event
    $('.open_perm_roles_modal').on('click', function(){

      // Get the button
      let $button = $(this);
      // Get the hidden input
      let $hiddenInput = $(this).siblings('.hidden');

      console.log($button);

      // Get user id
      let user_id = $(this).data('id');
      // Get user role_ids
      let role_ids = $hiddenInput.val();
      console.log(role_ids);
      // Init role_names array
      let role_names = $hiddenInput.attr('data-role_names');
      console.log(role_names);
      

      
      // Check if role_ids is more than one number
      if(role_ids.length > 2 ){
        // MORE THAN ONE
        var role_id_array = role_ids.split(',');
        var role_name_array = role_names.split(', ');
      } else if(role_ids.length != 0) {
        // ONLY ONE
        var role_id_array = [];
        role_id_array.push(`${role_ids}`);
        var role_name_array = [];
        role_name_array.push(`${role_names}`);
      } else {
        // EMPTY
        var role_id_array = [];
        var role_name_array = [];
      }
      

      console.log(role_id_array);
      console.log(role_name_array);

      // Check the corresponding checkboxes of the roles that are assigned to the user
      $('#listOfRoles .form-check-input').each(function(){
        let value = $(this).val();

        if(role_id_array.indexOf(value) != -1){
          $(this).prop('checked', true);
        } else {
          $(this).prop('checked', false);
        }
      });




      $('#listOfRoles').on('change', '.form-check-input', function(){

        // Get the value and rolename (id) of the checkbox
        let value = $(this).val();
        let name = $(this).data('name');

        // Check if the checkbox is checked
        if(this.checked){
  
          // Add it to the users array if not already in it
          if(role_id_array.indexOf(value) == -1){
            role_id_array.push(value);
          }
          // Add it to the names array if not already in it
          if(role_name_array.indexOf(name) == -1){
            role_name_array.push(name);
          }
  
          console.log(role_id_array);
          console.log(role_name_array);
        }
  
        // Check if the checkbox is unchecked
        if(!(this.checked)){
          // Find the value's index in the users array
          let index = role_id_array.indexOf(value);
          let nameIndex = role_name_array.indexOf(name);
          // Remove the value from the users array
          if(index != -1){
            role_id_array.splice(index, 1);
          }
          // Remove the name from the names array
          if(nameIndex != -1){
            role_name_array.splice(nameIndex, 1);
          }

          console.log(role_id_array);
          console.log(role_name_array);
        }

      });

      // Check for click event on save changes button
      $('#perm_roles_modal .saveChanges').on('click', function(){
        console.log('SAVED');
        
        // Turn the ids into a comma seperated string and store it in the hidden input
        $hiddenInput.val( role_id_array.join() );

        // Turn the role names array into a comma seperated string and store it in a variable
        let output = role_name_array.join(', ');        

        // Store the role names (output) in the hidden input data attribute
        $hiddenInput.attr('data-role_names', output );

        // Output the role names in the UI
        $button.parent().next('.rolesUI').children('p').html(output);
        console.log($button.parent().next('.rolesUI'));

        $('#perm_roles_modal').modal('hide');
      });



      /*
      // Make an ajax request to get a select list of roles
      $.ajax({
        url: '../php/fetch_roles.php',
        method: 'POST',
        data:{
          action: 'fetch_roles',
        },
        success: function(data){
          $('#perm_roles_modal .modal-body').html(data);
        }
      });
      */
    });
  }
});