$(document).ready(function(){

  // Enable tooltips
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  // Update column widths on load
  updateColumnWidths();

  /* ###################################################### */
  /* ################### GET ROLES ARRAY ################## */

  // Get the number of users for the assignment of roles
  let userCount = $('.organ .user').length;

  // Create rolesArray for output
  let rolesArray = [];
  
  // Loop through all .users
  $('.organ .user').each(function(){
    // Get the user id and organ id for each user
    let userId = $(this).data('id');
    let organId = $(this).data('organ-id');

    // Push values to the rolesArray as an object
    rolesArray.push(
      {
        userId: userId,
        organId: organId
      }
    );

  });

  /* ############################################################# */
  /* ################### ADD TASK AFTER TASK ##################### */
  /* ############################################################# */
  
  $('#addAfter').on('click', function(){

    // Get the taskId of
    let taskId = $('#selectTarget option:selected').val();
    let newTaskNum;
    let newTaskId;

    // Check if the beginning is selected 
    if( taskId == 'beginning'){
      // BEGINNING 
      newTaskNum = 1;
      // set the newTaskId to "task-1"
      newTaskId = 'task-1';

      // Create the new row element for the DOM
      let newRow = createNewTaskSet(newTaskNum, rolesArray );
      // Add it to the beginning right after the legend
      $(`#column_info`).after(newRow);
    } else {
      // REST
      // filter the task number from taskId
      // then turn the task number into an integer
      // add 1 to it to get the next task number
      newTaskNum = parseInt( taskId.substring(5) ) + 1
      newTaskId = `task-${newTaskNum}`;

      // Create the new row element for the DOM
      let newRow = createNewTaskSet(newTaskNum, rolesArray );
      // Add it to its respective place
      $(`#${taskId}`).after(newRow);
    }

    

    // Add remove button  
    $(`#remove-area`).append(`<div class="remove-button d-flex align-items-center px-3 mb-3" data-task="1">
                                <i class="fas fa-times"></i>
                              </div>`);

    updateProcessRows();
    updateRemoveButtons();
    updateSelectOptions();
    updateColumnWidths();

  });


  /* ##################################################### */
  /* ################### REMOVE TASK ##################### */
  /* ##################################################### */

  // Listen for click on the remove buttons
  // Use "document" as the first selector because the buttons are dynamic elements
  $(document).on('click', '.remove-button', function(){
    
    // Get the corresponding task number
    let taskNo = $(this).data('task');

    // Remove the corresponding task set from the DOM
    $(`#task-${taskNo}`).remove();
    // Remove the button itself
    $(this).remove();


    updateProcessRows();
    updateRemoveButtons();
    updateSelectOptions();
    updateColumnWidths();

  });


  /* ############################################################# */
  /* ##################### CHECK INTERVALS ####################### */
  /* ############################################################# */

  // Get relevant modal elements
  const $row = $('#check_intervals_modal .modal-body .row');
  const $activateIntervals = $('#activateIntervals');
  const $refDay =  $('#reference_day');
  const $refMonth =  $('#reference_month');
  const $refYear =  $('#reference_year');
  const $intCount =  $('#interval_count');
  const $intFormat =  $('#interval_format');
  const $activateReminders = $('#activate_reminders');
  const $remindBefore = $('#remind_before');
  const $remindAfter = $('#remind_after');
  const $remindAt = $('#remind_at');

  $(document).on('click', '.open_checks', function(){

    // Add the activated class to current .open_checks button
    $(this).addClass('activated');

    
    let $inputRefDate = $('.open_checks.activated').siblings('.ref-date');
    let $inputIntCount = $('.open_checks.activated').siblings('.int-count');
    let $inputIntFormat = $('.open_checks.activated').siblings('.int-format');
    let $inputRemindBefore = $('.open_checks.activated').siblings('.remind-before');
    let $inputRemindAfter = $('.open_checks.activated').siblings('.remind-after');
    let $inputRemindAt = $('.open_checks.activated').siblings('.remind-at');
    
    // Get UI Output
    let $uiOutput = $('.open_checks.activated').siblings('p');
    
    /* ################################################################################# */
    /* ##################### FILL MODAL WITH HIDDEN INPUT VALUES ####################### */

    if( $inputRefDate.val() != '' ){
      // Split the exisiting refDate into a array 
      let existingRefDate = $inputRefDate.val().split('-');
      
      // Add the values to the modal
      $refDay.val( existingRefDate[0] );
      $refMonth.val( existingRefDate[1] );
      $refYear.val( existingRefDate[2] );

      // Check the checkbox
      $activateIntervals.prop('checked', true);
      // Enable the interval fieldset
      $('#intervalSet').attr('disabled', false);
    } else {
      // Get the current date
      let today = new Date();
      // Populate the modal with the current date
      $refDay.val( today.getDate() );
      $refMonth.val( today.getMonth()+1 );
      $refYear.val( today.getFullYear() );
    }

    if( $inputIntCount.val() != '' ){
      $intCount.val( $inputIntCount.val() );
    }

    if( $inputIntFormat.val() != '' ){
      $intFormat.val( $inputIntFormat.val() );
    }

    // Remind Before
    if( $inputRemindBefore.val() != '' && $inputRemindBefore.val() != 0 ){
      // Fill the modal input
      $remindBefore.val( $inputRemindBefore.val() );
      // Check the activate reminders
      $activateReminders.prop('checked', true);
      // Enable the reminder set
      $('#reminderSet').attr('disabled', false);
    }

    // Remind After
    if( $inputRemindAfter.val() != '' && $inputRemindAfter.val() != 0 ){
      $remindAfter.val( $inputRemindAfter.val() );
      $activateReminders.prop('checked', true);
      $('#reminderSet').attr('disabled', false);
    }

    // Remind At
    if( $inputRemindAt.val() == 'true' ){
      $remindAt.prop('checked', true);
      $activateReminders.prop('checked', true);
      $('#reminderSet').attr('disabled', false);
    } 

    /* ################################################################ */
    /* ##################### ACTIVATE INTERVALS ####################### */

    // Listen for click event on noInterval Checkbox
    $('#activateIntervals').on('click', function(){

      // If it is checked
      if(this.checked){
        // Enable the corresponding fieldset
        $('#intervalSet').attr('disabled', false);
      }

      // If it is unchecked
      if(!(this.checked)){
        // Disable the corresponding fieldset
        $('#intervalSet').attr('disabled', true);

        // Remove all error statuses
        $refDay.removeClass('is-invalid');
        $refMonth.removeClass('is-invalid');
        $refYear.removeClass('is-invalid');

        // Disable the whole reminders set
        $('#activate_reminders').prop('checked', false);
        $('#reminderSet').attr('disabled', true);
      }
    });

    /* ################################################################ */
    /* ##################### ACTIVATE REMINDERS ####################### */

    // Listen for click event on noInterval Checkbox
    $('#activate_reminders').on('click', function(){

      // If it is checked
      if(this.checked){
        

        if( $('#activateIntervals').prop('checked') == true ){
          // CHECKED
          // Disable the corresponding fieldset
          $('#reminderSet').attr('disabled', false);
        } else {
          // Check if the remind alert is not already showing
          if( $('.alert.remind-err').length == 0 ){
            // Output an error notification
            $row.prepend(`<div class="col-12 remind-err alert alert-danger">Für Nutzung der Erinnerungsfunktion wird die Aktivierung der Check-Intervalle benötigt.<button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button></div>`);
          }
          $('#activate_reminders').prop('checked', false);
        }
      }

      // If it is unchecked
      if(!(this.checked)){
        // Disable the corresponding fieldset
        $('#reminderSet').attr('disabled', true);
      }

    });
  });


    /* ########################################################### */
    /* ##################### SAVE CHANGES ######################## */

    // Listen for saveChanges click
    $('#check_intervals_modal').on('click', '.saveChanges', function(){


      console.log('SAVE');

      // Get the activation statuses of check intervals and reminders
      activateIntervals = $('#activateIntervals').prop('checked');
      activateReminders = $('#activate_reminders').prop('checked');
      
      // Check if "check intervals" are activated
      if( activateIntervals ){

        console.log('ACTIVATE INTERVALS');

        

        // ACTIVATED
        // Get reference date values
        let refDay =  $refDay.val();
        let refMonth =  $refMonth.val();
        let refYear =  $refYear.val();
        // Get Rhythm values
        let intCount =  $('#interval_count').val();
        let intFormat =  $('#interval_format').val();

        let remindBefore;
        let remindAfter;
        let remindAt;


        if( activateReminders ){
          
          // Get the reminders values
          remindBefore = $('#remind_before').val();
          remindAfter = $('#remind_after').val();
          remindAt = $('#remind_at').prop('checked');

        } else {

          // Get the reminders values
          remindBefore = '';
          remindAfter = '';
          remindAt = '';
        }

        

        storeCheckInterval( refDay, refMonth, refYear , intCount, intFormat, remindBefore, remindAfter, remindAt);

        // Reset the modal
        clearCheckModal();
        
        // Remove the unique activated class
        $('.open_checks.activated').removeClass('activated');

        // Hide the modal
        $('#check_intervals_modal').modal('hide');

      } else {

        console.log('NOT ACTIVATED');

        console.log(activateIntervals);

        // EMPTY
        // Set the corresponding fields to invalid
        $('#reference_day').addClass('is-invalid');
        $('#reference_month').addClass('is-invalid');
        $('#reference_year').addClass('is-invalid');
        $('#interval_count').addClass('is-invalid');
        $('#interval_format').addClass('is-invalid');

        // Check if the empty alert is not already showing
        if( $('.alert.empty-err').length == 0 ){
          // Output an error notification
          $row.prepend(`<div class="col-12 empty-err alert alert-danger">Bitte füllen Sie alle Felder der Check Intervalle aus.<button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button></div>`);
        }
        

      }
    });

    /* ########################################################### */
    /* ###################### CLOSE MODAL ######################## */


    $('#check_intervals_modal .close-modal').on('click', function(){

      // Remove the unique activated class
      $('.open_checks.activated').removeClass('activated');

      // Reset the modal
      clearCheckModal();

    });


    function clearCheckModal(){

      // Reset the modal

      $activateIntervals.prop('checked', false);
      // Disable the corresponding fieldset
      $('#intervalSet').attr('disabled', true);
      $refDay.val('');
      $refMonth.val('');
      $refYear.val('');
      $intCount.val('');
      $intFormat.val('');

      $activateReminders.prop('checked', false);
      // Disable the corresponding fieldset
      $('#reminderSet').attr('disabled', true);
      $remindBefore.val('');
      $remindAfter.val('');
      $remindAt.prop('checked', false);


      /* ########### REMOVE ALL ERRORS ############ */

      // Remove all alerts
      $('#check_intervals_modal .alert').remove();

      // Set the corresponding fields to invalid
      $('#reference_day').removeClass('is-invalid');
      $('#reference_month').removeClass('is-invalid');
      $('#reference_year').removeClass('is-invalid');
      $('#interval_count').removeClass('is-invalid');
      $('#interval_format').removeClass('is-invalid');

    }




    function storeCheckInterval( refDay, refMonth, refYear, intCount, intFormat, remindBefore, remindAfter, remindAt ){

      // Get hidden Inputs
    
      let $inputRefDate = $('.open_checks.activated').siblings('.ref-date');
      let $inputIntCount = $('.open_checks.activated').siblings('.int-count');
      let $inputIntFormat = $('.open_checks.activated').siblings('.int-format');
      let $inputRemindBefore = $('.open_checks.activated').siblings('.remind-before');
      let $inputRemindAfter = $('.open_checks.activated').siblings('.remind-after');
      let $inputRemindAt = $('.open_checks.activated').siblings('.remind-at');
      
      // Get UI Output
      let $uiOutput = $('.open_checks.activated').siblings('p');

      /* ######################################### */
      /* ############ STORING DATA ############### */

      if( refDay != '' && refMonth != '' && refYear != '' && intCount != '' && intCount != 0 && intFormat != '' ){

        // Construct the reference date
        let referenceDate = `${refYear}-${refMonth}-${refDay}`;

        // Store the data in their respective hidden Inputs in the DOM
        $inputRefDate.val(referenceDate);
        $inputIntCount.val(intCount);
        $inputIntFormat.val(intFormat);

        // Check if at least one reminder value is present
        if( remindBefore > 0 || remindAfter > 0 || remindAt == true ){
          // VALIDATED
        
          // Store the data in their respective hidden Inputs in the DOM
          $inputRemindBefore.val(remindBefore);
          $inputRemindAfter.val(remindAfter);
          $inputRemindAt.val(remindAt);

          $uiOutput.html(`${intCount} ${intFormat}, <i class="fas fa-bell"></i>`);

        } else {
          
          $uiOutput.html(`${intCount} ${intFormat}, <i class="fas fa-bell-slash"></i>`);
        } 

      }
      
    } 




  

  /* ############################################################# */
  /* ########################### TAGS ############################ */
  /* ############################################################# */

  // Get modal inputs
  const $hiddenInput = $(`#tags_modal .hiddenInput`);
  const $mainInput = $(`#tags_modal .mainInput`);
  const $inputWrapper = $(`#tags_modal .tags-input`);

  
  /* #################################################### */
  /* ################### OPEN MODAL ##################### */

  // Init tags variable to store tags in for later
  let tags = [];

  $(document).on('click', '.open_tags', function(){

    // Add activated class to the current .open_tags button
    $(this).addClass('activated');

    let $rowInput = $('.open_tags.activated').siblings('.tags-string');
    let $uiOutput = $('.open_tags.activated').siblings('.tags-tooltip');

    console.log($rowInput);


    console.log('TAGS');

    if( $rowInput.val() != '' ){
      $hiddenInput.val( $rowInput.val() );
    }

    

    

    /* ##################################################### */
    /* ################# BASIC TAG FUNCTION ################ */

    

    // Check if the hiddenInput already has values (on view: edit-errors)
    if( $hiddenInput.val() != '' ){
      console.log('TEST');
      console.log($hiddenInput.val());

      // get them (as string), then split them into an array and store it
      const storedTags = $hiddenInput.val().split(',');
      storedTags.forEach(function(text){
        addTag(text);
      });

      
    }

    // Check for a keyup event on the mainInput
    $mainInput.on('keyup', function(e){

      // if a comma character is typed split the value of mainInput into an array of tag and comma
      let enteredTags = $mainInput.val().split(',');
      // Check if a tag is entered (so, tag + comma = 2)
      if(enteredTags.length > 1){
        enteredTags.forEach(function(t){
          // filter the tag using the filterTag function
          let filteredTag = filterTag(t);
          // Check if the filtered tag is there and does not already occur in the tags array
          if(filteredTag.length > 0 && tags.indexOf(filteredTag) == -1 ){
            // Add it to the tags array
            addTag(filteredTag);
          }
        });
        // Clear the mainInput field for the next input
        $mainInput.val('');
      }
    });

    // Check for a click event on the close button of a tag
    $(document).on('click', '.close', function(){
      // Get the tag as a string
      let tag = $(this).parent().text();
      // Remove it from the tags array
      if( tags.indexOf(tag) != '-1'){
        removeTag(tags.indexOf(tag));
        // Remove the tag from the UI
        $(this).parent().remove();
      }
    });


    ////////////////// FUNCTIONS //////////////////

    function addTag(text){
      // Add the text as a tag right before the mainInput
      $mainInput.before(`<span class="tag">${text} <i class="close fas fa-times"></i></span>`);
      // push the text to the tags array
      tags.push(text);
      // refresh the tags in the hiddenInput
      refreshTags();
    }

    function removeTag(index){
      // store the tag in a variable
      let tag = tags[index];
      // remove the tag from the tags array
      tags.splice(index, 1);
      // refresh the tags in the tags array
      refreshTags();
    }

    function refreshTags(){
      // Init tagsList array
      let tagsList = [];
      // Loop through the tags array and store each value in the tagsList array
      tags.forEach(function(t){
        tagsList.push(t);
      });
      // Join the tags of tagsList into a string and add it to hiddenInput
      $hiddenInput.val(tagsList.join());
    }

    function filterTag(tag){
      // Remove all characters except whitespace, but replaces whitespace with dashes
      return tag.replace(/[^\w -]/g, '').trim().replace(/\W+/g, '-');
    }

  });

    /* ##################################################### */
    /* ##################################################### */

    /* ##################################################### */
    /* ################### SAVE CHANGES #################### */

    // Listen for saveChanges click
    $('#tags_modal').on('click', '.saveChanges', function(){
      console.log('SAVE TAGS');

      let $rowInput = $('.open_tags.activated').siblings('.tags-string');
      let $uiOutput = $('.open_tags.activated').siblings('.tags-tooltip');

      // Get the tagsString from the hidden Input 
      let tagsString = $hiddenInput.val();

      if( tagsString != '' ){

        // Store the tagsString in the respective input in the process row
        $rowInput.val(tagsString);

        // Display the values in the UI
        $uiOutput.html('<i class="fas fa-tags"></i>');
        $uiOutput.attr('data-original-title', tagsString);
        $uiOutput.attr('title', tagsString);

      } else {

        // Display the an is_empty message in the UI
        $uiOutput.html('Keine Tags hinzugefügt');

        // Store the empty values in UI and hidden Input
        $rowInput.val('');
        $uiOutput.attr('data-original-title', tagsString);
        $uiOutput.attr('title', tagsString);

      }

      


      // Reset tags
      $('#tags_modal .tag').remove();
      $hiddenInput.val('');
      tags = [];

      // Remove the activated class from the current .open_tags button
      $('.open_tags.activated').removeClass('activated');


      $('#tags_modal').modal('hide');
    });

    /* ##################################################### */
    /* #################### CLOSE MODAL #################### */


    // Listen for close-modal click
    $('#tags_modal').on('click', '.close-modal', function(){

      // Reset tags
      $('#tags_modal .tag').remove();
      $hiddenInput.val('');
      tags = [];

      // Remove the activated class from the current .open_tags button
      $('.open_tags.activated').removeClass('activated');

    });



    

  



  /* ############################################################## */
  /* ########################## FUNCTIONS ######################### */
  /* ############################################################## */



  /* ///////////////////////////////////////////////////////////////// */
  /* //////////////////// UPDATE SELECT OPTIONS ////////////////////// */

  function updateSelectOptions(){

    let numOfTasks;
    let selectContent = `<option value="beginning">am Tabellenanfang</option>`;

    // Loop through all task sets
    $('.scrolling-wrapper .set').each(function(index){
      // Get the current task number
      numOfTasks = index+1;
      // Get the setId of the current task
      let setId = $(this).attr('id');
      
      // Check if this is the last iteration
      if( index+1 == $('.scrolling-wrapper .set').length ){
        // Add the html element to the selectContent variable and make it selected
        selectContent += `<option value="${setId}" selected>nach A${numOfTasks}</option>`;
      } else {
        // Add the html element to the selectContent variable (but not selected)
        selectContent += `<option value="${setId}">nach A${numOfTasks}</option>`;
      }
      
    });

    // Update the select field 
    $('#selectTarget').html(selectContent);

  }

  /* /////////////////////////////////////////////////////////////// */
  /* //////////////////// UPDATE PROCESS ROWS ////////////////////// */

  function updateProcessRows(){

    // Loop through all task sets
    $('.scrolling-wrapper .set').each(function(mainIndex){
        // Update the id
        $(this).attr('id', `task-${mainIndex+1}`) 
        // Update task_number
        $(this).children('.task_number').children('p').text(`A${mainIndex + 1}`);
        $(this).children('.task_number').children('input').attr('name', `${mainIndex + 1}-number`);
        $(this).children('.task_number').children('input').val(mainIndex + 1);
        // Update input 
        $(this).children('.input').children('input').attr('name', `${mainIndex + 1}-input`);
        // Update input_source
        $(this).children('.input_source').children('input').attr('name', `${mainIndex + 1}-input_source`);
        // Update category
        $(this).children('.category').children('select').attr('name', `${mainIndex + 1}-category`);
        // Update task_name
        $(this).children('.task_name').children('input').attr('name', `${mainIndex + 1}-name`);
        // Update output
        $(this).children('.output').children('input').attr('name', `${mainIndex + 1}-output`);
        // Update output_target
        $(this).children('.output_target').children('input').attr('name', `${mainIndex + 1}-output_target`);
        // Update risk
        $(this).children('.risk').children('select').attr('name', `${mainIndex + 1}-risk`);
        // Update status
        $(this).children('.status').children('select').attr('name', `${mainIndex + 1}-status`);
        // Update required_knowledge
        $(this).children('.required_knowledge').children('input').attr('name', `${mainIndex + 1}-required_knowledge`);

        // Update roles
        $(this).children('.roles') .children().each(function(index){
          $(this).children('select').attr('name', `${mainIndex+ 1}-user-select-${index + 1}`);
        });

        // Update iso_value
        $(this).children('.iso_value').children('input').attr('name', `${mainIndex + 1}-iso_value`);
    });

  }

  /* /////////////////////////////////////////////////////////////// */
  /* /////////////////// UPDATE REMOVE BUTTONS ///////////////////// */

  function updateRemoveButtons(){
    // Loop through all remove buttons
    $('#remove-area').children().each(function(index){
      $(this).attr('data-task', index + 1 );
    });
  }

  /* ///////////////////////////////////////////////////////////////// */
  /* /////////////////// CREATE REMOVE BUTTON //////////////////////// */

  function createNewRemoveButton( taskNo ){

    let removeBtn = `
    <div class="remove-button d-flex align-items-center px-3 mb-3" data-task="${taskNo}">
      <i class="fas fa-times"></i>
    </div>`;

    return removeBtn;
  }


  /* ///////////////////////////////////////////////////////////////// */
  /* /////////////////// CREATE NEW TASK SET ROW ///////////////////// */

  function createNewTaskSet(taskNo, rolesArray){

    let task_number = `
      <div class="art task_number">
        <p class="mb-0 pl-3">A${taskNo}</p>
        <input name="${taskNo}-number" type="hidden" value="${taskNo}">
      </div>`;
    
    let input = `
      <div class="art input">
        <input name="${taskNo}-input" type="text" class="form-control" value="" placeholder="Input">
      </div>`;
    
    let input_source = `
      <div class="art input_source">
        <input name="${taskNo}-input_source" type="text" class="form-control" value="" placeholder="Inputquelle">
      </div>`;

    let category = `
      <div class="art category">
        <select name="${taskNo}-category" class="form-control">
          <option value="empty" selected>Kategorie auswählen</option>
          <option>Auftragswelt</option>
          <option>Strukturwelt</option>
          <option>Zielwelt</option>
          <option>Rechtswelt</option>
          <option>Maßnahmenswelt</option>
        </select>
      </div>`;

    let task_name = `
      <div class="art task_name">
        <input name="${taskNo}-name" type="text" class="form-control" value="" placeholder="Name">
      </div>`;

    let output = `
      <div class="art output">
        <input name="${taskNo}-output" type="text" class="form-control" value="" placeholder="Output">
      </div>`;

    let output_target = `
      <div class="art output_target">
        <input name="${taskNo}-output_target" type="text" class="form-control" value="" placeholder="Outputziel">
      </div>`;

    let risk = `
      <div class="art risk">
        <select name="${taskNo}-risk" class="form-control">
          <option value="empty" selected>Risiko auswählen</option>
          <option>Keins</option>
          <option>Gering</option>
          <option>Mittel</option>
          <option>Hoch</option>
        </select>
      </div>
    `;

    let status = `
      <div class="art status">
        <select name="${taskNo}-status" class="form-control">
          <option value="empty">Status auswählen</option>
          <option>Plan</option>
          <option>Do</option>
          <option>Check</option>
          <option>Act</option>
        </select>
      </div>
    `;

    let roles = `
      <div class="art roles">`;

    let i;
    for (i = 0; i < rolesArray.length ; i++) { 
      roles += `<div class="select-user d-flex flex-column justify-content-center">
                  <select name="${taskNo}-user-select-${i+1}" class="form-control">
                    <option value="empty" selected></option>
                    <option value="responsible">V</option>
                    <option value="concerned">Z</option>
                    <option value="contributing">M</option>
                    <option value="information">I</option>
                  </select>
                </div>`;
    }
    roles += `</div>`;

    let iso_value = `
      <div class="art iso_value">
        <input name="${taskNo}-iso_value" type="text" class="form-control" value="" placeholder="ISO-Bezug">
      </div>
    `;

    let check_intervals = `
      <div class="art check_intervals d-flex">
        <p class="${taskNo}-check mb-0 ml-2"></p>
        <button type="button" class="open_checks btn btn-pe-darkgreen d-block ml-auto" data-task="${taskNo}" data-toggle="modal" data-target="#check_intervals_modal">
        <i class="fas fa-edit"></i></button>
        <input name="${taskNo}-ref-date" class="ref-date" type="hidden">
        <input name="${taskNo}-int-count" class="int-count" type="hidden">
        <input name="${taskNo}-int-format" class="int-format" type="hidden">
        <input name="${taskNo}-remind-before" class="remind-before" type="hidden">
        <input name="${taskNo}-remind-after" class="remind-after" type="hidden">
        <input name="${taskNo}-remind-at" class="remind-at" type="hidden">
      </div>
    `;

    let tags = `
      <div class="art tags d-flex">
        <button type="button" class="tags-tooltip btn btn-light w-100" data-toggle="tooltip" data-placement="top" title data-original-title="Keine Tags vergeben">
          Keine Tags hinzugefügt
        </button>
        <button type="button" class="open_tags btn btn-pe-darkgreen d-block ml-auto" data-task="${taskNo}" data-toggle="modal" data-target="#tags_modal">
        <i class="fas fa-edit"></i></button>
        <input name="${taskNo}-tags" class="tags-string" type="hidden">
      </div>
    `;
              
              

    let newRow = `
      <div id="task-${taskNo}" class="set d-flex mb-3">
        ${task_number}
        ${input}
        ${input_source}
        ${category}
        ${task_name}
        ${output}
        ${output_target}
        ${risk}
        ${status}
        ${roles}
        ${iso_value}
        ${check_intervals}
        ${tags}
      </div>
    `;

    return newRow;
  }


  /* ############################################################################ */
  /* ############## ALLIGN COLUMNS BASED ON WIDTH OF WIDEST COLUMN ############## */
  /* ############################################################################ */

  function updateColumnWidths(){
    // Update the column widths

    calcColumnWidth( 'input' );
    calcColumnWidth( 'input_source' );
    calcColumnWidth( 'task_name' );
    calcColumnWidth( 'output' );
    calcColumnWidth( 'output_target' );
    calcColumnWidth( 'iso_value' ); 
  };



  
  function calcColumnWidth( $class ){

    // Init max width variables
    let maxWidth = 0;

    // Loop through all columns of the selected class
    $(`.scrolling-wrapper .${$class}`).each(function(){

      // Get the current column's width
      let currentWidth = $(this).css('width');
      currentWidth = currentWidth.substring(0, currentWidth.length - 2);


      //console.log( $(this) );
      //console.log(currentWidth);

      // Compare current width with max width
      if( currentWidth > maxWidth ){
        maxWidth =  currentWidth;
      };

    });

    //console.log(maxWidth);

    // Set the max width plus extra space on all columns of selected class

    $(`.scrolling-wrapper .${$class}`).css('min-width', `${maxWidth}px`);
  }
  
   




});