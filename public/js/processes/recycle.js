$(document).ready(function(){

  // Enable tooltips
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  // Update column widths on load
  updateColumnWidths();

  /* ###################################################### */
  /* ################### GET ROLES ARRAY ################## */

  // Get the number of users for the assignment of roles
  let userCount = $('.organ .user').length;

  // Create rolesArray for output
  let rolesArray = [];
  
  // Loop through all .users
  $('.organ .user').each(function(){
    // Get the user id and organ id for each user
    let userId = $(this).data('id');
    let organId = $(this).data('organ-id');

    // Push values to the rolesArray as an object
    rolesArray.push(
      {
        userId: userId,
        organId: organId
      }
    );
  });

  /* ############################################################# */
  /* ################### ADD TASK AFTER TASK ##################### */
  /* ############################################################# */
  
  $('#addAfter').on('click', function(){

    // Get the taskId of
    let taskId = $('#selectTarget option:selected').val();
    let newTaskNum;
    let newTaskId;

    // Check if the beginning is selected 
    if( taskId == 'beginning'){
      // BEGINNING 
      newTaskNum = 1;
      // set the newTaskId to "task-1"
      newTaskId = 'task-1';

      // Create the new row element for the DOM
      let newRow = createNewTaskSet(newTaskNum, rolesArray );
      // Add it to the beginning right after the legend
      $(`#column_info`).after(newRow);
    } else {
      // REST
      // filter the task number from taskId
      // then turn the task number into an integer
      // add 1 to it to get the next task number
      newTaskNum = parseInt( taskId.substring(5) ) + 1
      newTaskId = `task-${newTaskNum}`;

      // Create the new row element for the DOM
      let newRow = createNewTaskSet(newTaskNum, rolesArray );
      // Add it to its respective place
      $(`#${taskId}`).after(newRow);
    }

    // Add remove button  
    $(`#remove-area`).append(`<div class="remove-button d-flex align-items-center px-3 mb-3" data-task="1">
                                <i class="fas fa-times"></i>
                              </div>`);

    updateProcessRows();
    updateRemoveButtons();
    updateSelectOptions();
    updateColumnWidths();

  });


  /* ##################################################### */
  /* ################### REMOVE TASK ##################### */
  /* ##################################################### */

  // Listen for click on the remove buttons
  // Use "document" as the first selector because the buttons are dynamic elements
  $(document).on('click', '.remove-button', function(){
    
    // Get the corresponding task number
    let taskNo = $(this).data('task');

    // Remove the corresponding task set from the DOM
    $(`#task-${taskNo}`).remove();
    // Remove the button itself
    $(this).remove();


    updateProcessRows();
    updateRemoveButtons();
    updateSelectOptions();
    updateColumnWidths();

  });


  /* ############################################################## */
  /* ########################## FUNCTIONS ######################### */
  /* ############################################################## */



  /* ///////////////////////////////////////////////////////////////// */
  /* //////////////////// UPDATE SELECT OPTIONS ////////////////////// */

  function updateSelectOptions(){

    let numOfTasks;
    let selectContent = `<option value="beginning">am Tabellenanfang</option>`;

    // Loop through all task sets
    $('.scrolling-wrapper .set').each(function(index){
      // Get the current task number
      numOfTasks = index+1;
      // Get the setId of the current task
      let setId = $(this).attr('id');

      // Check if this is the last iteration
      if( index+1 == $('.scrolling-wrapper .set').length ){
        // Add the html element to the selectContent variable and make it selected
        selectContent += `<option value="${setId}" selected>nach A${numOfTasks}</option>`;
      } else {
        // Add the html element to the selectContent variable (but not selected)
        selectContent += `<option value="${setId}">nach A${numOfTasks}</option>`;
      }
      
    });

    // Update the select field 
    $('#selectTarget').html(selectContent);

  }

  /* /////////////////////////////////////////////////////////////// */
  /* //////////////////// UPDATE PROCESS ROWS ////////////////////// */

  function updateProcessRows(){

    // Loop through all task sets
    $('.scrolling-wrapper .set').each(function(mainIndex){
        // Update the id
        $(this).attr('id', `task-${mainIndex+1}`) 
        // Update task_number
          $(this).children('.task_number').children('p').text(`A${mainIndex + 1}`);
          $(this).children('.task_number').children('.number').attr('name', `${mainIndex + 1}-number`);
          $(this).children('.task_number').children('.number').val(mainIndex + 1);
        // Update input 
        $(this).children('.input').children('input').attr('name', `${mainIndex + 1}-input`);
        // Update input_source
        $(this).children('.input_source').children('input').attr('name', `${mainIndex + 1}-input_source`);
        // Update category
        $(this).children('.category').children('select').attr('name', `${mainIndex + 1}-category`);
        // Update task_name
        $(this).children('.task_name').children('input').attr('name', `${mainIndex + 1}-name`);
        // Update output
        $(this).children('.output').children('input').attr('name', `${mainIndex + 1}-output`);
        // Update output_target
        $(this).children('.output_target').children('input').attr('name', `${mainIndex + 1}-output_target`);
        // Update risk
        $(this).children('.risk').children('select').attr('name', `${mainIndex + 1}-risk`);
        // Update status
        $(this).children('.status').children('select').attr('name', `${mainIndex + 1}-status`);
        // Update required_knowledge
        $(this).children('.required_knowledge').children('input').attr('name', `${mainIndex + 1}-required_knowledge`);

        // Update roles
        $(this).children('.roles') .children().each(function(index){
          $(this).children('select').attr('name', `${mainIndex+ 1}-user-select-${index + 1}`);
        });

        // Update iso_value
        $(this).children('.iso_value').children('input').attr('name', `${mainIndex + 1}-iso_value`);
    });

  }

  /* /////////////////////////////////////////////////////////////// */
  /* /////////////////// UPDATE REMOVE BUTTONS ///////////////////// */

  function updateRemoveButtons(){
    // Loop through all remove buttons
    $('#remove-area').children().each(function(index){
      $(this).attr('data-task', index + 1 );
    });
  }

  /* ///////////////////////////////////////////////////////////////// */
  /* /////////////////// CREATE REMOVE BUTTON //////////////////////// */

  function createNewRemoveButton( taskNo ){

    let removeBtn = `
    <div class="remove-button d-flex align-items-center px-3 mb-3" data-task="${taskNo}">
      <i class="fas fa-times"></i>
    </div>`;

    return removeBtn;
  }
  
  /* ///////////////////////////////////////////////////////////////// */
  /* /////////////////// CREATE NEW TASK SET ROW ///////////////////// */

  function createNewTaskSet(taskNo, rolesArray){

    let task_number = `
      <div class="art task_number">
        <p class="mb-0 pl-3">A${taskNo}</p>
        <input name="${taskNo}-number" type="hidden" value="${taskNo}">
      </div>`;
    
    let input = `
      <div class="art input">
        <input name="${taskNo}-input" type="text" class="form-control" value="" placeholder="Input">
      </div>`;
    
    let input_source = `
      <div class="art input_source">
        <input name="${taskNo}-input_source" type="text" class="form-control" value="" placeholder="Inputquelle">
      </div>`;

    let category = `
      <div class="art category">
        <select name="${taskNo}-category" class="form-control">
          <option value="empty" selected>Kategorie auswählen</option>
          <option>Auftragswelt</option>
          <option>Strukturwelt</option>
          <option>Zielwelt</option>
          <option>Rechtswelt</option>
          <option>Maßnahmenswelt</option>
        </select>
      </div>`;

    let task_name = `
      <div class="art task_name">
        <input name="${taskNo}-name" type="text" class="form-control" value="" placeholder="Name">
      </div>`;

    let output = `
      <div class="art output">
        <input name="${taskNo}-output" type="text" class="form-control" value="" placeholder="Output">
      </div>`;

    let output_target = `
      <div class="art output_target">
        <input name="${taskNo}-output_target" type="text" class="form-control" value="" placeholder="Outputziel">
      </div>`;

    let risk = `
      <div class="art risk">
        <select name="${taskNo}-risk" class="form-control">
          <option value="empty" selected>Risiko auswählen</option>
          <option>Keins</option>
          <option>Gering</option>
          <option>Mittel</option>
          <option>Hoch</option>
        </select>
      </div>
    `;

    let status = `
      <div class="art status">
        <select name="${taskNo}-status" class="form-control">
          <option value="empty">Status auswählen</option>
          <option>Plan</option>
          <option>Do</option>
          <option>Check</option>
          <option>Act</option>
        </select>
      </div>
    `;

    let required_knowledge = `
      <div class="art required_knowledge">
        <input name="${taskNo}-required_knowledge" type="text" class="form-control" placeholder="erfordl. Wissen">
      </div>`;

    let roles = `
      <div class="art roles">`;

    let i;
    for (i = 0; i < rolesArray.length ; i++) { 
      roles += `<div class="select-user d-flex flex-column justify-content-center">
                  <select name="${taskNo}-user-select-${i+1}" class="form-control">
                    <option value="empty" selected></option>
                    <option value="responsible">V</option>
                    <option value="concerned">Z</option>
                    <option value="contributing">M</option>
                    <option value="information">I</option>
                  </select>
                </div>`;
    }
    roles += `</div>`;

    let iso_value = `
      <div class="art iso_value">
        <input name="${taskNo}-iso_value" type="text" class="form-control" value="" placeholder="ISO-Bezug">
      </div>
    `;

    let newRow = `
      <div id="task-${taskNo}" class="set d-flex mb-3">
        ${task_number}
        ${input}
        ${input_source}
        ${category}
        ${task_name}
        ${output}
        ${output_target}
        ${risk}
        ${status}
        ${required_knowledge}
        ${roles}
        ${iso_value}
      </div>
    `;

    return newRow;
  }

  /* ############################################################################ */
  /* ############## ALLIGN COLUMNS BASED ON WIDTH OF WIDEST COLUMN ############## */
  /* ############################################################################ */

  function updateColumnWidths(){
    // Update the column widths

    calcColumnWidth( 'input' );
    calcColumnWidth( 'input_source' );
    calcColumnWidth( 'task_name' );
    calcColumnWidth( 'output' );
    calcColumnWidth( 'output_target' );
    calcColumnWidth( 'iso_value' ); 
  };



  
  function calcColumnWidth( $class ){

    // Init max width variables
    let maxWidth = 0;

    // Loop through all columns of the selected class
    $(`.scrolling-wrapper .${$class}`).each(function(){

      // Get the current column's width
      let currentWidth = $(this).css('width');
      currentWidth = currentWidth.substring(0, currentWidth.length - 2);


      //console.log( $(this) );
      //console.log(currentWidth);

      // Compare current width with max width
      if( currentWidth > maxWidth ){
        maxWidth =  currentWidth;
      };

    });

    //console.log(maxWidth);

    // Set the max width plus extra space on all columns of selected class

    $(`.scrolling-wrapper .${$class}`).css('min-width', `${maxWidth}px`);
  }
   




});