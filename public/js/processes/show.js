$(document).ready(function(){

  /* ############## SHOW TOOLTIPS FOR ORGANS ############## */
  
  // Enable tooltips
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })


  /* ############################################# */
  /* ############## APPROVE PROCESS ############## */

  $('#approve_process').on('click', function(){

    let processId = $(this).data('id');
    let taskIds = $(this).data('tasks');

    
    $.ajax({
      url: urlroot + 'php/processes.php',
      method: 'POST',
      data:{
        action: 'approve_process',
        processId: processId,
        tasks: taskIds
      },
      success: function(data){
        // Remove the approval window
        $('#approval').remove();
        // Reload the page 
        location.reload();
        // Add the output alert to the top of the window
        $('#main').prepend(data);
        
      }
    }); 
    

  });


  /* ############################################# */
  /* ############## ARCHIVE PROCESS ############## */

  $('#archive_process').on('click', function(){

    let folderId = $(this).data('folder-id'); 
    let processId = $(this).data('process-id');

    
    $.ajax({
      url: urlroot + 'php/processes.php',
      method: 'POST',
      data:{
        action: 'archive_process',
        folderId: folderId,
        processId: processId
      },
      success: function(data){
        // Add the output alert to the top of the window
        $('#main').prepend(data);
      }
    }); 
    

  });


  /* ############## ALLIGN COLUMNS BASED ON WIDTH OF WIDEST COLUMN ############## */

  calcColumnWidth( 'input' );
  calcColumnWidth( 'input_source' );
  calcColumnWidth( 'task_name' );
  calcColumnWidth( 'output' );
  calcColumnWidth( 'output_target' );
  calcColumnWidth( 'iso_value' );


  
  function calcColumnWidth( $class ){

    let maxWidth = 0;

    $(`.scrolling-wrapper .${$class}`).each(function(){

      let currentWidth = $(this).width();

      if( currentWidth > maxWidth ){
        maxWidth =  currentWidth;
      };

    });

    console.log( maxWidth + 20 );

    $(`.scrolling-wrapper .${$class}`).css('min-width', `${maxWidth +60}px`);

  }

  






});