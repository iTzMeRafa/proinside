$(document).ready(function() {
    $('#methodsActiveTable').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 100,
    } );
} );

$(document).ready(function() {
    $('#businessModelsArchived').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskAngebotswelt').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskStrukturwelt').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskZielwelt').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskRechtswelt').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskActive').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskArchived').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskProcess').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#singleTask').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#taskAll').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#globalSearchTasks').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#globalSearchMethods').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#globalSearchDocuments').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#reportsList').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#userAcceptedTasks').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#userUnacceptedTasks').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#userAcceptedMethods').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#userUnacceptedMethods').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#userListing').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#userAbbr').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#organsListing').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#qualificationsList').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#qualificationUserList').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );

$(document).ready(function() {
    $('#backupList').DataTable( {
        "language": {
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/German.json"
        },
        "pageLength": 25,
    } );
} );


