$(document).ready(function(){

  /* ################################ */
  /* ######### TASKS: SHOW  ######### */
  /* ################################ */
  
  if($('body').hasClass('showTask')){

    /* ###################################################### */
    /* ############## ACCEPT ASSIGNMENT TO TASK ############# */

    $('.confirm_assignment').on('click', function(){
      console.log('TEST');
      // Get the values from element
      $alert = $(this).parent();
      $userId = $(this).data('user');
      $taskId = $(this).data('task');
      $userRole = $(this).data('role');
      
      // Make AJAX request
      $.ajax({
        url: urlroot + 'php/confirm-task.php',
        method: 'POST',
        data: {
          action: 'confirmTask',
          user_id: $userId,
          task_id: $taskId,
          user_role: $userRole
        },
        success: function(data){
          $alert.before(data);
          $alert.remove();
        }
      }); // ajax
    }); // click

    /* ###################################################### */
    /* ####################### CHECK TASK ################### */

    $('#check_task').on('click', function(){
      // Get the values from element
      // Alert Element
      $alert = $(this).parent();
      // User ID
      $userId = $(this).data('user');
      // Task ID
      $taskId = $(this).data('task');
      // Check Comment
      $comment = $('#check_comment').val();
      // Reference Date
      $checkDate = $(this).data('check-date');
      // Interval Count
      $intCount = $(this).data('count');
      // Interval Format
      $intFormat = $(this).data('format');

      // Check if there is a comment 
      if($comment){
        // SUCCESS
        // Make AJAX request
        $.ajax({
          url: urlroot + 'php/confirm-task.php',
          method: 'POST',
          data: {
            action: 'checkTask',
            user_id: $userId,
            task_id: $taskId,
            comment: $comment,
            checkDate: $checkDate,
            intCount: $intCount,
            intFormat: $intFormat
          },
          success: function(data){
            $alert.before(data);
            $alert.remove();
          }
        }); // ajax

      } else {
        // NO COMMENT
        // Get the necessary elements
        $errOutput = ($('#check_comment').siblings('.invalid-feedback'));
        $textArea = $('#check_comment');
        // Display errors in the view
        $textArea.addClass('is-invalid');
        $errOutput.text('Bitte schreiben Sie ein beschreibenden Kommentar zu Ihrem Check.');

      }
      
    }); // click






    /* ####### OPEN MODAL ###### */

    $('#open_coworkers_modal').on('click', function(){
      // Get the question id & the saved userIds
      let questionId = $(this).data('id');
      let idsFromUI = $(this).siblings('.stored-coworkers').children('.userIds').val();

      // Init arrays for storage
      let storedUserIds = [];
      let storedUserNames = [];

      // Make sure there are saved user ids
      if(idsFromUI != ''){
        // Split the strign of ids into an array
        idsFromUI = idsFromUI.split(",");

        // Loop through all list items in the modal
        $('#coworkers_browser .modal-body li').each(function(){
          // Get the current li's input id
          let currentId = $(this).children('input').val();          
          // Get the index of it in idsFromUI
          let currentIndex = idsFromUI.indexOf(currentId);

          
          // If it is in the array 
          if(currentIndex != -1){
            // Check the checkbox
            $(this).children('input').prop('checked', true);
            // Get the correspondig name
            let name = $.trim($(this).text());


            // Add the id to storedUserIds
            if(storedUserIds.indexOf(currentId) == -1){
              // Add the id to storedUserIds
              storedUserIds.push(currentId);
              // Add the name to storedUserNames
              storedUserNames.push(name);
            }

          }
        });

        console.log(storedUserIds);
        console.log(storedUserNames);
      }



      

      /* ####### CHECKBOX CHECKED ###### */

      $('#coworkers_browser').on('change', 'input:checkbox', function(){
        // Check if the checkbox is checked
        if(this.checked){
          // Get the id & name
          let userId = $(this).val();
          let userName = $.trim($(this).parent().text());

          // Add the userId to storedUserIds if not already in it
          if(storedUserIds.indexOf(userId) == -1){
            storedUserIds.push(userId);
            // Add the userName to storedUserNames
            storedUserNames.push(userName);
          }

          // Loop through all list items
          $('#coworkers_browser .modal-body li').each(function(){
            // Check for same user in different role
            if( $(this).children('input').val() == userId ){
              // Check its checkbox as well
              $(this).children('input').prop('checked', true);
            }
          });
        }

        // Check if the checkbox is unchecked
        if(!(this.checked)){
          // Get the id & name
          let userId = $(this).val();
          let userName = $.trim($(this).parent().text());

          let idIndex = storedUserIds.indexOf(userId);
          let nameIndex = storedUserNames.indexOf(userName);
    
          // Remove userId from storedUserIds if in it
          if(idIndex != -1){
            storedUserIds.splice(idIndex, 1);
          }
          // Remove userName from storedUserNames if in it
          if(nameIndex != -1){
            storedUserNames.splice(nameIndex, 1);
          }

          // Loop through all list items
          $('#coworkers_browser .modal-body li').each(function(){
            // Check for same user in different role
            if( $(this).children('input').val() == userId ){
              // Uncheck its checkbox as well
              $(this).children('input').prop('checked', false);
            }
          });
        }
      });

      /* ####### SAVE CHANGES ###### */

      $('#coworkers_browser .saveChanges').on('click', function(){

        // Empty the UI
        $(`#q-${questionId}-notifyUsers`).empty();

        // Loop through the array of ids
        $.each(storedUserIds, function(index, value){
          // Get the corresponding name from the other array
          let name = storedUserNames[index];
          // Construct a list item for the UI with the values
          let li = `<li class="list-group-item" data-id="${value}">${name}<span class="remove-user float-right"><i class="fas fa-times"></i></span></li>`;
          // Add it to the UI
          $(`#q-${questionId}-notifyUsers`).append(li);
        });

        $(`#notifyUsersQ-${questionId}`).val(storedUserIds.join());

        // Uncheck all checkboxes 
        $('#coworkers_browser .modal-body li').children('input').prop('checked', false);
        // Close the modal
        $('#coworkers_browser').modal('hide');

      });


    });

    /* ####### REMOVE INDIVIDUAL USER VIA UI ###### */

    $(document).on('click', '.remove-user', function(){
      // Get the li's id
      let currentId = $(this).parent().data('id').toString();

      // Get the hidden input element
      let hiddenInput = $(this).parent().parent().siblings('.userIds');
      // Get the value of the hiddenInput 
      let hiddenInputString = hiddenInput.val();
      // split it into an array of ids
      let idArray = hiddenInputString.split(",")

      // Get the index of the current id within the array of ids
      console.log(idArray);
      let index = idArray.indexOf(currentId);

      console.log(index);

      // If the id is within the array remove it
      if(index != -1){
        idArray.splice(index, 1);
      }
      // Turn the ids array into a comma seperated string and add it back to the hidden Input
      hiddenInput.val(idArray.join());

      // Remove the corresponding UI Element
      $(this).parent().remove();
    });


    /* ######## SUBMIT ANSWER ######## */

    $(document).on('click', '.submit-answer', function(){
      
      // Get the different answer siblings
      let answers = $(this).siblings('.answer');
      // Init output variable
      let output = [];

      // Loop through all answers
      $.each(answers, function(index, value){

        // Is "Yes or No" Answer

        if($(this).hasClass('yes_no')){
          // Get the two radio buttons
          let radionButtons = $(this).children().children('.form-check-input');

          // If "yes"-button is checked
          if(radionButtons[0].checked){
            // Remove error message and class
            $(this).removeClass('invalid');
            $(this).find('.yesno-error').text('');

            console.log('YES');
          }
          // If "no"-button is checked
          else if(radionButtons[1].checked){
            // Remove error message and class
            $(this).removeClass('invalid');
            $(this).find('.yesno-error').text('');

            console.log('NO');
          }
          // If nothing is checked
          else {
            // Create error message
            let yesnoErr = 'Bitte wählen Sie ein der beiden Antwortmöglichkeiten aus.';
            $(this).addClass('invalid');
            $(this).find('.yesno-error').text(yesnoErr);
          }
        }
        
        // Is "Notify" Answer

        if($(this).hasClass('notify')){
          // Get the userIds from hidden Input as a string
          let userIds = $(this).find('.userIds').val();
          // Convert userIds to array 
          let userIdArray =  userIds.split(',');
          // Get the value of the textarea 
          let message = $(this).find('.message').val().trim();

          if(userIdArray != ''){
            // FILLED
            // Remove error message and class
            $(this).find('.coworkers-btn').removeClass('invalid');
            $(this).children('.coworkers-error').text('');

            console.log(userIdArray);

          } else {
            // EMPTY
            // Create error message
            let idErr = 'Bitte wählen Sie mindestens einen zubenachrichtigten Mitarbeiter aus.';
            $(this).find('.coworkers-btn').addClass('invalid');
            $(this).children('.coworkers-error').text(idErr);
          }

          if(message != ''){
            // FILLED
            // Remove error message and class
            $(this).find('.msg-btn').removeClass('invalid');
            $(this).children('.msg-error').text('');

            if(message.length > 20){
              // LONG ENOUGH
              // Remove error message and class
              $(this).find('.msg-btn').removeClass('invalid');
              $(this).children('.msg-error').text('');

              console.log(message);

            } else {
              // TOO SHORT
              // Create error message
              let msgErr = 'Bitte verfassen Sie eine Nachricht mit mindestens 20 Zeichen.';
              $(this).find('.message').addClass('invalid');
              $(this).children('.msg-error').text(msgErr);
            }

          } else {
            // NO MESSAGE
            // Create error message
            let msgErr = 'Bitte verfassen Sie eine angemessene Nachricht.';
            $(this).find('.message').addClass('invalid');
            $(this).children('.msg-error').text(msgErr);
          }
          
        } 

        // Is "Remind" Answer

        if($(this).hasClass('remind')){
          // Get the three columns with the date values
          let remindDay = $(this).find('.remind_day').val();
          let remindMonth = $(this).find('.remind_month').val();
          let remindYear = $(this).find('.remind_year').val();

          if(remindDay && remindMonth && remindYear){
            // Remove error message and class
            $(this).removeClass('invalid');
            $(this).find('.remind-error').text('');

            // Assemble the remind date
            let remindDate = new Date(`${remindYear}, ${remindMonth}, ${remindDay}`);
            // Parse it (for comparison)
            let parsedRD = Date.parse(remindDate);
            // Parse the current date
            let today = Date.parse(new Date()); 

            // Check ff the parsed remindDate is not in the future
            if(parsedRD < today) {
              // NOT IN FUTURE
              // Create error message
              let remindErr = 'Bitte wählen Sie ein Erinnerungsdatum in der Zukunft aus.';
              $(this).addClass('invalid');
              $(this).find('.remind-error').text(remindErr);
            } else {
              // IN FUTURE
              console.log(remindDate);
            }

          
          } 
          // If one or more fields are empty/unset
          else {
            // Create error message
            let remindErr = 'Bitte wählen Sie ein vollständiges Erinnerungsdatum aus.';
            $(this).find('.remind-date').addClass('invalid');
            $(this).find('.remind-error').text(remindErr)
          }
          

        }
        

      });

      
    });
  

  

  


  } // showTask

   
}); // document.ready