<?php
// require the main file for config variables
require_once '../../app/bootstrap.php' ;
// Require the archive file
require_once '../../app/models/data.php';
// instantiate the data model class
$dataModel = new Data;




if(isset($_POST['action'])){

  if($_POST['action'] == 'fetch'){

    // Set up the required font-awesome css-classes
    $fileTypes = [

      // Archives
      '7z'    => 'fa-file-archive',
      'bz'    => 'fa-file-archive',
      'gz'    => 'fa-file-archive',
      'rar'   => 'fa-file-archive',
      'tar'   => 'fa-file-archive',
      'zip'   => 'fa-file-archive',

      // Text
      'cfg'   => 'fa-file-alt',
      'ini'   => 'fa-file-alt',
      'log'   => 'fa-file-alt',
      'md'    => 'fa-file-alt',
      'rtf'   => 'fa-file-alt',
      'txt'   => 'fa-file-alt',

      // Video
      'avi'   => 'fa-file-video',
      'flv'   => 'fa-file-video',
      'mkv'   => 'fa-file-video',
      'mov'   => 'fa-file-video',
      'mp4'   => 'fa-file-video',
      'mpg'   => 'fa-file-video',
      'ogv'   => 'fa-file-video',
      'webm'  => 'fa-file-video',
      'wmv'   => 'fa-file-video',
      'swf'   => 'fa-file-video',

      // Images
      'bmp'   => 'fa-file-image',
      'gif'   => 'fa-file-image',
      'jpg'   => 'fa-file-image',
      'jpeg'  => 'fa-file-image',
      'png'   => 'fa-file-image',
      'psd'   => 'fa-file-image',
      'tga'   => 'fa-file-image',
      'tif'   => 'fa-file-image',

      // Audio
      'aac'   => 'fa-file-audio',
      'flac'  => 'fa-file-audio',
      'mid'   => 'fa-file-audio',
      'midi'  => 'fa-file-audio',
      'mp3'   => 'fa-file-audio',
      'ogg'   => 'fa-file-audio',
      'wma'   => 'fa-file-audio',
      'wav'   => 'fa-file-audio',

      // Documents
      'csv'   => 'fa-file-csv',
      'doc'   => 'fa-file-word',
      'docx'  => 'fa-file-word',
      'odt'   => 'fa-file-alt',
      'ppt'   => 'fa-powerpoint',
      'pdf'   => 'fa-file-pdf',
      'xls'   => 'fa-file-alt',
      'xlsx'  => 'fa-file-excel',
      // Blank
      'blank' => 'fa-file'
    ];

    $rootDirectory = '../uploads/files';

    // if there is a new directory transmitted set the current directory to the new one
    if(!empty($_POST['newDirectory'])){
      $currentDirectory = $_POST['newDirectory'];
    } else {
      // Set the current directory to the root Directory
      $currentDirectory = $rootDirectory;
    }
    // Scan the directory and return the containing files/folders in an array
    $files = scandir($currentDirectory);


    // Read files/folders from the directory
    foreach ($files as $key => $file) {

      $realPath = '';
      $fileName = '';
      $iconClass = '';


      if ($file != '.') {

        // Get the files relative path
        $relativePath = $currentDirectory . '/' . $file;

        if (substr($relativePath, 0, 2) == './') {
            $relativePath = substr($relativePath, 2);
        }

        // Don't check parent dir if we're in the root dir
        if ($currentDirectory == $rootDirectory && $file == '..'){

            continue;

        } else {

          // Get files absolute path
          $realPath = realpath($relativePath);

          // Get file name
          $fileName = pathinfo($realPath, PATHINFO_FILENAME);


          // Determine file type by extension
          // Check if path is a directory
          if (is_dir($realPath)) {
              // Add folder icon class
              $iconClass = 'fa-folder';

          } else {
              // Get file extension
              $fileExt = strtolower(pathinfo($realPath, PATHINFO_EXTENSION));

              if (isset($fileTypes[$fileExt])) {
                  $iconClass = $fileTypes[$fileExt];
              } else {
                  $iconClass = $fileTypes['blank'];
              }
          }
        }

        if ($file == '..') {

            if ($currentDirectory != '.' && $currentDirectory != 'uploads/files') {
                // Get parent directory path
                $pathArray = explode('/', $relativePath);
                unset($pathArray[count($pathArray)-1]);
                unset($pathArray[count($pathArray)-1]);
                $directoryPath = implode('/', $pathArray);

                /*
                if (!empty($directoryPath)) {
                    $directoryPath = '?dir=' . rawurlencode($directoryPath);
                }
                */

                // Add file info to the array
                $directoryArray['..'] = array(
                    'name' => '',
                    'file_path'  => $directoryPath,
                    'url_path'   => URLROOT.$directoryPath,
                    'icon_class' => 'fa-level-up-alt'
                );
            }
            continue;
        }

        $directoryArray[$key] = array(
          'name' => $file,
          'file_path' => $relativePath,
          'url_path'   => $realPath,
          'icon_class' => $iconClass
        );

      }

    }

    $data = [
      'entries' => $directoryArray,
      'scandir' => $files,
      'current' => $currentDirectory
    ];

    $output = '
      <div id="file-tree">
        <div class="row bg-light mb-3">
          <div class="col-md-9">
            <p>Inhalt</p>
          </div>
          <div class="col-md-2">
            <p>Dateien</p>
          </div>
        </div>
    ';

    if(count($data['entries']) > 0){

      foreach($data['entries'] as $entry){

        $name = $entry['name'];
        $url_path = $entry['url_path'];
        $file_path = $entry['file_path'];
        $icon_class = $entry['icon_class'];

        // Add the row as a wrapper
        $output .= '<div class="row bg-light py-2 mb-3">';

        // Init dir variable
        $dirClass = '';
        // Check if the $_POST variable is set
        if(isset($_POST['dirType'])) {
          // Check if dirType is archive
          if($_POST['dirType'] == 'archive'){
            // Set the dirClass to archive
            $dirClass = 'archive';
          }
        }

        // Check if the current directory is the parent directory
        if($icon_class == 'fa-level-up-alt'){

          // Check if the parent directory is an archive
          if($file_path != $rootDirectory && $dirClass == 'archive'){
            // IS ARCHIVE
            $output .= '
              <div class="archive directory col-md-12" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> .. Zurück</div>
            </div>';            
          } else {
            // IS ORDINARY PARENT DIRECTORY
            $output .= '
              <div class="directory col-md-12" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> .. Zurück</div>
            </div>';
          }

          // Check if the current directory is a folder
        } elseif($icon_class == 'fa-folder'){

          // Check if the current directory is an archive
          if($name == 'Archiv' || $dirClass == 'archive'){
            // IS ARCHIVE
            $output .= '
              <div class="archive directory col-md-9" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> '.$name.'</div>
              <div class="col-md-1">'.(count(scandir($url_path)) - 2).'</div>
              <div class="col-md-2"></div>
            </div>';

          } else {
            // IS ORDINARY DIRECTORY
            $output .= '
              <div class="directory col-md-9" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> '.$name.'</div>
              <div class="col-md-1">'.(count(scandir($url_path)) - 2).'</div>
              <div class="col-md-2">
                <div class="dropdown">
                  <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Bearbeiten
                  </button>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <button type="button" name="update" data-name="'.$name.'" class="dropdown-item update btn btn-warning btn-xs">Umbenennen</button>
                    <button type="button" name="delete" data-name="'.$name.'" class="dropdown-item delete btn btn-danger btn-xs">Löschen</button>
                  </div>
                </div>
              </div>
            </div>
              ';
          }
          // The current iteration is a file
        } else {
          // Check if the current file is in the archive folder
          if($dirClass == 'archive'){
            // IS IN ARCHIVE
            $output .= '
                <div class="archive file col-md-9" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> '.$name.'</div>
                <div class="col-md-1">-</div>
                <div class="col-md-2"></div>
              </div>
            ';

          } else {
            // IS REGULAR FILE
            $output .= '
                <div class="file col-md-9" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> '.$name.'</div>
                <div class="col-md-1">-</div>
                <div class="col-md-2">
                  <div class="dropdown">
                    <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Bearbeiten
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                      <button type="button" name="update" data-name="'.$name.'" class="dropdown-item update btn btn-warning btn-xs">Umbenennen</button>
                      <button type="button" name="delete_file" data-name="'.$name.'" class="dropdown-item delete_file btn btn-danger btn-xs">Löschen</button>
                    </div>
                  </div>
                </div>
              </div>
            ';
          }
        }
      }

    } else {
      // Closing wrapper
      $output .= '<div class="row">
          <div class="col-md-12">Kein Ordner gefunden</td>
      </div>';
    }
    // Another closing tag
    $output .= '</div>';
    echo $output;
  }

  /* ########################################################## */
  /* ################### AJAX: CREATE FOLDER ################## */
  /* ########################################################## */

  if($_POST["action"] == "create") {
    // Get the directory path an store it in a variable
    $dirPath = $_POST['currentDirectory'] .'/'. $_POST["folder_name"];
    // Check if folder already exists in the root directory
    if(!file_exists($dirPath)){
      // Create a new folder in the root directory
      mkdir($dirPath, 0777, true);

      // Prepare data for model
      $data = [
        'folder_name' => $_POST["folder_name"],
        'folder_path' => $dirPath
      ];


      if($dataModel->addFolder($data)){
        // SUCCESS
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Der Ordner wurde erstellt.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      } else {
        // ERROR
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Der Ordner wurde erstellt, konnte aber nicht zur Datenbank hinzugefügt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      }
    }
    else
    {
      // Otherwise echo an error message
      echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
              Ein Ordner mit diesem Namen existier bereits.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    }
  }

  /* ########################################################## */
  /* ############### AJAX: CHANGE FILE NAME ################### */
  /* ########################################################## */

  if($_POST["action"] == "change_file_name")
  {
    // Store the values with the directory path in variables
    $oldFilePath = $_POST['currentDirectory'] .'/'. $_POST["old_name"];
    $noExtPath = $_POST['currentDirectory'] .'/'. $_POST["file_name"];
    $fileExtension = '.'.$_POST['file_ext'];
    // Construct the new file path with extension
    $newFilePath = $noExtPath.$fileExtension;

    // Check if folder already exists in the directory
    if(!file_exists($newFilePath)){
      // Rename the folder and echo out a success message
      if(rename($oldFilePath, $newFilePath)){
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
                Die Datei wurde umbenannt.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>';
      } else {
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            Ein Fehler ist aufgetreten. Bitte versuchen Sie es nochmal oder kontaktieren Sie den Support.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>';
      }
    } else {
      // Otherwise echo an error message
      echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
              Eine Datei mit diesem Namen existiert bereits.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
    }
  }

  function archiveFile($path) {
    // Store the old path
    $oldPath = $path;
    // Remove the root folder from the filepath of the file to be archived
    $noRootPath = substr( $path, 17, strlen($path) );

    /* ############# EXTENSION ############## */
    // Get the file extension from the string
    $extension = explode(".", $noRootPath);
    $extension = array_pop($extension);   
    // Get the number of charactes of $extension and add the number 1 (for the dot) to it
    $length = 1+(strlen($extension));
    // Remove the file extension from $fileName
    $dirAndFile = substr( $noRootPath, 0, strlen($noRootPath)-$length);
    
    // Create a new variable to log current date and time
    $currentDate = date("d.m.Y");
    $currentTime = date("H:i");
    // Construct the final path for the archive folder
    $archivePath = '../uploads/files/Archiv/'.$dirAndFile.'_archived_'.$currentDate.'_'.$currentTime.'.'.$extension;
    

    /* ############# PREVIOUS DIRECTORIES ############## */
    // Split the path into parts (for each directory) and store them in an array
    $dirParts = explode('/', $noRootPath);
    // Remove the last array element (the actual file)
    $fileName = array_pop($dirParts);

    // Construct the start path for the if-file-exists foreach-loop
    $checkPath = '../uploads/files/Archiv/';

    // Loop through the $dirParts array and check if the dir already exisist within the archive directory
    foreach($dirParts as $dir) {
      // For each iteration add one part to $checkPath 
      $checkPath .= $dir; 
      
      // Check if this path does not exists
      if(!file_exists($checkPath)){
        // NO EXISTENT
        // Create it
        mkdir($checkPath, 0777, true);
        // Concatenate the slash for the next round
        $checkPath .= '/';
      } else {
        // DIR EXISTENT -> continue to the next dir 
        // Concatenate the slash for the next round
        $checkPath .= '/';
        continue;
      }
    }

    // Init log variable
    $log = '';

    // Check if the file exists in the archive directory
    if(!file_exists($archivePath)){
      // Move the file to the archive directory
      if(rename($oldPath, $archivePath)) {
        $log = 'SUCCESS'; 
        
      } else {
        $log = 'ERROR';
      }
    } else {
      $log = 'EXISTS';
    }

    return $output = [
      'log' => $log,
      'archivePath' => $archivePath,
      'fileName' => $fileName
    ];


  }

  /* ########################################################## */
  /* ############ AJAX: DELETE AND ARCHIVE FILE ############### */
  /* ########################################################## */

  // Init Database Object
  // Source: app/libraries/database.php
  $db = new Database;

  // check if the action set to remove_file
  if($_POST["action"] == "archive_file") {

    // Archive the file
    $result = archiveFile($_POST['filePath']);

    // Output the respective message 
    if($result['log'] == 'SUCCESS') {

      // Prepare data for the model
      $data = [
        'target_name' => $result['fileName'],
        'path' => $result['archivePath'],
        'user_id' => $_SESSION['user_id']
      ];
  
      // Check if the the database request was successful
      if($dataModel->archive($data)){
        // SUCCESS
        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Die Datei wurde archiviert.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      } else {
        // ERROR
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Die Datei wurde archiviert. Es konnte jedoch kein Datenbankeintrag erstellt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewähren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      } 

    } elseif($result['log'] == 'ERROR') {
      echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Ein Fehler ist aufgetreten. Bitte versuchen Sie es nochmal oder kontaktieren Sie den Support.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    } elseif($result['log'] == 'EXISTS') {
      echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Eine Datei mit dem selben Namen und Pfad existiert bereits im Archiv.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }
  }

  /* ########################################################## */
  /* ################### AJAX: DELETE FOLDER ################## */
  /* ########################################################## */

  // check if the action set to delete
  if($_POST["action"] == "delete"){

    
    $folderName = $_POST["folder_name"];
    $folderPath = $_POST["currentPath"] . '/' . $_POST["folder_name"];
    $files = scandir($folderPath);

    // ARCHIVING

    // Init output variable for feedback
    $output = '';
    // Set the error variable to false by default
    $hasError = false;
    // Init array for additional folders contained inside the main one
    $folders = [];


    foreach($files as $file) {
      // Skip current folder and parent folder paths
      if($file === '.' || $file === '..') {
        continue;
      } else {

        // Get the files relative path
        $filePath = $folderPath .'/'. $file;
        // Get files absolute path
        $realPath = realpath($filePath);

        // Check if current "file" is a directory
        if(is_dir($realPath)) {
          $hasError = true;
          $output = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Der zu archivierende Ordner enthält weitere Unterordner. Bitte archivieren Sie zunächst alle beinhalteten Unterordner bevor Sie diesen Ordner archivieren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
          // Stop the foreach loop
          break;
        }

        // Archive the file and store the feedback
        $result = archiveFile($filePath);

        // Check the result the current archiving process
        if($result['log'] == 'SUCCESS'){
          // FILE => archived (renamed and moved to the respective archive folder)
          // Add respective feedback to $logArr
          $output .= '<div class="alert alert-success alert-dismissible fade show" role="alert">Die Datei '.$file.' wurde archiviert.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

        } elseif($result['log'] == 'ERROR') {
          // FILE => NOT archived
          // Add respective feedback to $logArr
          $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Bei der Archivierung der Datei '.$file.' ist ein Fehler aufgetreten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
          $hasError = true;

        } else {
          // FILE => already exists
          // Add respective feedback to $logArr
          $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Eine Datei mit dem Namen '.$file.' existiert bereits im entsprechenden Archivordner.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
          $hasError = true;
        }
      }
    }

    if(!$hasError){
      // Check if the removal of the folder was successful
      if(rmdir($folderPath)) {
        // SUCCESS

        // Prepare data for model
        $data = [
          'target_name' => $folderName,
          'path' => $folderPath,
          'user_id' => $_SESSION['user_id']
        ];
        
        // Check if the the database request was successful
        if($dataModel->archive($data)){
          // SUCCESS
          echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Der Ordner wurde gelöscht und alle beinhalteten Dateien archiviert.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        } else {
          // ERROR
          echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Der Ordner wurde gelöscht und alle beinhalteten Dateien archiviert. Es konnte jedoch kein Datenbankeintrag erstellt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewähren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        }
      }
    } else {
      echo $output;
    }
    
  }

}
