<?php

require_once '../../app/bootstrap.php' ;

/* ################################### */
/* ######## Fetch Select List ######## */
/* ################################### */


if($_POST['action'] == 'fetch_select_list'){

  // Set up the required font-awesome css-classes
  $fileTypes = [

    // Archives
    '7z'    => 'fa-file-archive',
    'bz'    => 'fa-file-archive',
    'gz'    => 'fa-file-archive',
    'rar'   => 'fa-file-archive',
    'tar'   => 'fa-file-archive',
    'zip'   => 'fa-file-archive',

    // Text
    'cfg'   => 'fa-file-alt',
    'ini'   => 'fa-file-alt',
    'log'   => 'fa-file-alt',
    'md'    => 'fa-file-alt',
    'rtf'   => 'fa-file-alt',
    'txt'   => 'fa-file-alt',

    // Video
    'avi'   => 'fa-file-video',
    'flv'   => 'fa-file-video',
    'mkv'   => 'fa-file-video',
    'mov'   => 'fa-file-video',
    'mp4'   => 'fa-file-video',
    'mpg'   => 'fa-file-video',
    'ogv'   => 'fa-file-video',
    'webm'  => 'fa-file-video',
    'wmv'   => 'fa-file-video',
    'swf'   => 'fa-file-video',

    // Images
    'bmp'   => 'fa-file-image',
    'gif'   => 'fa-file-image',
    'jpg'   => 'fa-file-image',
    'jpeg'  => 'fa-file-image',
    'png'   => 'fa-file-image',
    'psd'   => 'fa-file-image',
    'tga'   => 'fa-file-image',
    'tif'   => 'fa-file-image',

    // Audio
    'aac'   => 'fa-file-audio',
    'flac'  => 'fa-file-audio',
    'mid'   => 'fa-file-audio',
    'midi'  => 'fa-file-audio',
    'mp3'   => 'fa-file-audio',
    'ogg'   => 'fa-file-audio',
    'wma'   => 'fa-file-audio',
    'wav'   => 'fa-file-audio',

    // Documents
    'csv'   => 'fa-file-csv',
    'doc'   => 'fa-file-word',
    'docx'  => 'fa-file-word',
    'odt'   => 'fa-file-alt',
    'ppt'   => 'fa-powerpoint',
    'pdf'   => 'fa-file-pdf',
    'xls'   => 'fa-file-alt',
    'xlsx'  => 'fa-file-excel',
    // Blank
    'blank' => 'fa-file'
  ];



  $rootDirectory = '../uploads/files';

  // if there is a new directory transmitted set the current directory to the new one
  if(!empty($_POST['newDirectory'])){
    $currentDirectory = $_POST['newDirectory'];
  } else {
    // Set the current directory to the root Directory
    $currentDirectory = $rootDirectory;
  }
  // Scan the directory and return the containing files/folders in an array
  $files = scandir($currentDirectory);



  // Read files/folders from the directory
  foreach ($files as $key => $file) {

    $realPath = '';
    $fileName = '';
    $iconClass = '';


    if ($file != '.') {

      // Get the files relative path
      $relativePath = $currentDirectory . '/' . $file;

      if (substr($relativePath, 0, 2) == './') {
          $relativePath = substr($relativePath, 2);
      }

      // Don't check parent dir if we're in the root dir
      if ($currentDirectory == $rootDirectory && $file == '..'){

          continue;

      } else {

        // Get files absolute path
        $realPath = realpath($relativePath);

        // Get file name
        $fileName = pathinfo($realPath, PATHINFO_FILENAME);


        // Determine file type by extension
        // Check if path is a directory
        if (is_dir($realPath)) {
            // Add folder icon class
            $iconClass = 'fa-folder';

        } else {
            // Get file extension
            $fileExt = strtolower(pathinfo($realPath, PATHINFO_EXTENSION));

            if (isset($fileTypes[$fileExt])) {
                $iconClass = $fileTypes[$fileExt];
            } else {
                $iconClass = $fileTypes['blank'];
            }
        }
      }

      if ($file == '..') {

          if ($currentDirectory != '.' && $currentDirectory != 'uploads/files') {
              // Get parent directory path
              $pathArray = explode('/', $relativePath);
              unset($pathArray[count($pathArray)-1]);
              unset($pathArray[count($pathArray)-1]);
              $directoryPath = implode('/', $pathArray);

              // Add file info to the array
              $directoryArray['..'] = array(
                  'name' => '',
                  'file_path'  => $directoryPath,
                  'url_path'   => URLROOT .'/pages' . $directoryPath,
                  'icon_class' => 'fa-level-up-alt'
              );
          }
          continue;
      }

      $directoryArray[$key] = array(
        'name' => $file,
        'file_path' => $relativePath,
        'url_path'   => $realPath,
        'icon_class' => $iconClass
      );

    }

  }



  $data = [
    'entries' => $directoryArray,
    'scandir' => $files,
    'current' => $currentDirectory
  ];





  $output = '
    <div id="file-tree">
      <div class="row bg-light mb-3">
        <div class="col-md-9 offset-md-1">
          <p>Inhalt</p>
        </div>
        <div class="col-md-2">
          <p>Dateien</p>
        </div>
      </div>
  ';

  if(count($data['entries']) > 0){

    foreach($data['entries'] as $entry){

      $name = $entry['name'];
      $url_path = $entry['url_path'];
      $file_path = $entry['file_path'];
      $icon_class = $entry['icon_class'];

      // Add the row as a wrapper
      $output .= '<div class="row bg-light py-2 mb-3">';

      // Check if the current entry is '..' (one folder up)
      if($icon_class == 'fa-level-up-alt'){
        // Add the respective code to the output
        $output .= '
            <div class="directory col-md-11 offset-md-1" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> ..</div>
          </div>';
      } elseif($icon_class == 'fa-folder'){
        // If the current entry is a folder add the respective code to the output
        $output .= '
          <div class="col-md-1"><input class="selectbox" type="checkbox"></div>
          <div class="directory col-md-9" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> '.$name.'</div>
          <div class="col-md-2">'.(count(scandir($url_path)) - 2).'</div>
        </div>
          ';
      } else {
        $output .= '
            <div class="col-md-1"><input class="selectbox" type="checkbox"></div>
            <div class="file col-md-9" data-path="'.$file_path.'"><i class="fa '.$icon_class.'"></i> '.$name.'</div>
            <div class="col-md-2">-</div>
          </div>
        ';
      }
    }

  } else {
    $output .= '<div class="row">
        <div class="col-md-12">Kein Ordner gefunden</td>
    </div>';
  }
  $output .= '</div>';
  echo $output;
}
