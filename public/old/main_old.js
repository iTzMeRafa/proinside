// Init current Path and current Directory variable

let currentPath = '../uploads/files';
// Get the currentDirectory from the currentPath
let currentDirectory = currentPath.substring(currentPath.lastIndexOf('/') + 1, currentPath.length);

$(document).ready(function(){


  // Check if we are on the correct view 
  if($('body').hasClass('folders')){
    // autoload the directory
    load_folder_list();
  }

  // Load Folder List Function

  function load_folder_list(){
    $.ajax({
      method: 'POST',
      url: urlroot + 'php/action_new.php',
      data:{
        action: 'fetch',
        newDirectory: currentPath
      },
      success: function(data) {
        $('#folder_table').html(data);
      }
    });
  }

  /* ####################################################### */
  /* ########## Navigate Into Folder Function ############## */
  /* ####################################################### */

  $(document).on('click', '.directory', function(){
    // Get the folder name
    var path = $(this).data('path');

    if($(this).hasClass('archive')){
      var dirType = 'archive'
    } else {
      var dirType = 'directory'
    }

    $.ajax({
      method: 'POST',
      url: 'php/action.php',
      data:
      {
        action: 'fetch',
        newDirectory: path,
        dirType: dirType
      },
      success: function(data) {
        // Update the data
        $('#folder_table').html(data);
        // Update the current path variable
        currentPath = path;
        // Get the name of the current directory from the currentPath variable
        currentDirectory = currentPath.substring(currentPath.lastIndexOf('/') + 1, currentPath.length);
        // Check if the currentDirectory is the root folder
        if(currentDirectory == 'files'){
          // Change the name in the UI to "Hauptordner"
          $('#currentDirectory').text('Hauptordner');
        } else {
          // Change the name in the UI to the currentDirectory
          $('#currentDirectory').text(currentDirectory);
        }
      }
    });

  });

  /* #################################################### */
  /* ############## Create Folder Function ############## */
  /* #################################################### */

  $('#create_folder').click(function(){
    // Set the attributes accordingly (to create a folder)
    $('#action').val('create');
    // Empty the folder name value
    $('#folder_name').val('');
    // Set the button text
    $('#folder_button').val('Erstellen');
    // Empty the old name value (this field is for change folder name)
    $('#old_name').val('');
    // Change the title to fit the method
    $('#change_title').text('Ordner erstellen');
    // Display the modal
    $('#folderModal').modal('show');
  });

  $('#folder_button').click(function(){
    // Get the required values from the inputs
    var folder_name = $('#folder_name').val();
    var action = $('#action').val();
    var old_name = $('#old_name').val();

    // Make an ajax request if a folder name is entered
    if(folder_name != ''){
      $.ajax({
        url:"php/action.php",
        method: "POST",
        data:{
          folder_name: folder_name,
          old_name: old_name,
          action: action,
          currentDirectory: currentPath
        },
        success: function(data){
          $('#folderModal').modal('hide');
          load_folder_list(currentPath);
          $('#alertbox').append(data);
        }
      });
    } else {
      // remind the user to enter a folder name
      let folderAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"> Bitte geben Sie einen Ordnernamen ein. <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
      $('#alertbox').append(folderAlert);
    }
  });


/* #################################################### */
/* ############ Update File/Folder Button ############# */
/* #################################################### */

$(document).on('click', '.update', function(){

  // Store the old folder name in a variable and in hidden input "old_name"
  let oldName = $(this).data('name');
  

  // Determine if it is a file or a folder by checking the classes of "col-md-9"
  // Get the DOM Element
  $object = $(this).parent().parent().parent().siblings('.col-md-9');

  // Check the class
  if($object.hasClass('file')){
    // IS FILE

    // Split the string by every dot and store all parts in an array
    let split = oldName.split(".");
    // Get the last item from the array
    let extension = split.pop();
    // Get the char-length of the extension and add 1 for the dot to it
    let extLength = extension.length+1;
    // Subtract extLength from the name
    let nameNoExt = oldName.substr(0, oldName.length-extLength);


    // Add the old name to hidden Input
    $('#old_file_name').val(oldName);
    // Add the extension to hidden Input
    $('#file_extension').val(extension);
    // Display the current name (without Extension) in UI
    $('#file_name').val(nameNoExt);
    // Show the modal
    $('#fileModal').modal('show');

  } else if($object.hasClass('directory')){
    // IS FOLDER/DIRECTORY

    // Add the old name to hidden Input
    $('#old_name').val(oldName);
    // Give the user the current name via the "folder_name" input in the UI
    $('#folder_name').val(oldName);
    // Change the AJAX action data to "change"
    $('#action').val("change_folder_name");

    // Change the UI texts according to the action
    $('#folder_button').val('Umbenennen');
    $('#change_title').text("Ordnernamen ändern");

    // Show the modal
    $('#folderModal').modal('show');
    // The rest of this request is processed via the create folder function's AJAX Request
  }
  
});

/* #################################################### */
/* ################# Update File Name ################# */
/* #################################################### */

$('#file_button').click(function(){

  // Get the required values from the inputs
  var file_name = $('#file_name').val();
  var old_name = $('#old_file_name').val();
  var extension = $('#file_extension').val();

  // Make an ajax request if a file name is entered
  if(file_name != ''){
    $.ajax({
      url:"php/action.php",
      method: "POST",
      data:{
        file_name: file_name,
        old_name: old_name,
        file_ext: extension,
        action: 'change_file_name',
        currentDirectory: currentPath
      },
      success: function(data){
        $('#fileModal').modal('hide');
        load_folder_list(currentPath);
        $('#alertbox').append(data);
      }
    });
  } else {
    // remind the user to enter a folder name
    let fileAlert = '<div class="alert alert-warning alert-dismissible fade show" role="alert"> Bitte geben Sie einen Dateinamen ein. <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    $('#alertbox').append(fileAlert);
  }
});




/* #################################################### */
/* ###################### Upload ###################### */
/* #################################################### */

$(document).on('click', '#upload_data', function(){
  // Store the folder path in a variable and the hidden input "hidden_folder_name"

  $('#hidden_folder_path').val(currentPath);
  // Show the Upload Modal
  $('#uploadModal').modal('show');
});

$('#upload_form').on('submit', function(){
  $.ajax({
    url:"php/upload.php",
    method: "POST",
    data: new FormData(this),
    contentType: false,
    cache: false,
    processData: false,
    success: function(data)
    {

      $.ajax({
        method: 'POST',
        url: 'php/action.php',
        data:{
          action: 'fetch',
          newDirectory: currentPath
        },
        success: function(data) {
          $('#folder_table').html(data);
        }
      });

      $('h1').before(data);
      $('#uploadModal').modal('hide');
    }
  });
  return false;
});

/* #################################################### */
/* ################### Delete Folder ################## */
/* #################################################### */

$(document).on('click', '.delete', function(){
  // Get the folder name from data variable
  var folder_name = $(this).data("name");
  // Get the number of files inside from nearby column
  var numOfFiles = $(this).parent().parent().parent().siblings('.col-md-1').text();

  if(numOfFiles > 0){
    var alert = '<div class="alert alert-danger alert-dismissible fade show" role="alert"><i class="fas fa-exclamation-triangle"></i> Der ausgewählter Ordner enthält Dateien!</div>';
    // If this alert is not already there, add it.
    if($('#confirmModal .modal-body .alert').length == 0){
      $('#confirmModal .h6').after(alert);
    }
    
  }

  // Change the modal text
  $('#confirmModal .h6').text('Sind Sie sicher, dass Sie diesen Ordner löschen möchten?');
  $('#confirmModal .data').html(`Ausgewählter Ordner: <strong id="selectedFile">${folder_name}</strong>`);

  // Show the modal
  $('#confirmModal').modal('show');


  // Listen for acceptance
  $('#acceptDel').on('click', function(){

    $.ajax({
      url: "php/action.php",
      method: "POST",
      data:
      {
        folder_name: folder_name,
        currentPath: currentPath,
        action: 'delete'
      },
      success: function(data){
        load_folder_list(currentPath);
        $('#alertbox').append(data);
        $('#confirmModal').modal('hide');
      }
    }); // AJAX

  });
});

/* #################################################### */
/* #################### Delete File ################### */
/* #################################################### */

$(document).on('click', '.delete_file', function(){
  var fileName = $(this).data("name");
  var filePath = `${currentPath}/${fileName}`;

  // Remove the alert (from delete folder function) if it exists
  if($('#confirmModal .modal-body .alert').length){
    $('#confirmModal .modal-body .alert').remove();
  }

  // Change the modal text
  $('#confirmModal .h6').text('Sind Sie sicher, dass Sie diese Datei löschen möchten?');
  $('#confirmModal .data').html(`Ausgewählte Datei: <strong id="selectedFile">${fileName}</strong>`);


  // Show the modal
  $('#confirmModal').modal('show');

  // Listen for acceptance
  $('#acceptDel').on('click', function(){

    $.ajax({
      url: "php/action.php",
      method: "POST",
      data:
      {
        action: 'archive_file',
        filePath: filePath
      },
      success: function(data)
      {
        $('#alertbox').append(data);
        $('#confirmModal').modal('hide');
        load_folder_list();
      }
    });
  }); // AJAX

});

/* ############## SHOW TASK ################## */


if($('body').hasClass('showTask')){

  /* ###################################################### */
  /* ############## ACCEPT ASSIGNMENT TO TASK ############# */

  $('.confirm_assignment').on('click', function(){
    // Get the values from element
    $alert = $(this).parent();
    $userId = $(this).data('user');
    $taskId = $(this).data('task');
    $userRole = $(this).data('role');
    
    // Make AJAX request
    $.ajax({
      url: '../../php/confirm-task.php',
      method: 'POST',
      data: {
        action: 'confirmTask',
        user_id: $userId,
        task_id: $taskId,
        user_role: $userRole
      },
      success: function(data){
        $alert.before(data);
        $alert.remove();
      }
    }); // ajax
  }); // click

  $('#check_task').on('click', function(){
    // Get the values from element
    // Alert Element
    $alert = $(this).parent();
    // User ID
    $userId = $(this).data('user');
    // Task ID
    $taskId = $(this).data('task');
    // Check Comment
    $comment = $('#check_comment').val();
    // Reference Date
    $checkDate = $(this).data('check-date');
    // Interval Count
    $intCount = $(this).data('count');
    // Interval Format
    $intFormat = $(this).data('format');

    console.log($checkDate);
    console.log($intCount);
    console.log($intFormat);

    // Check if there is a comment 
    if($comment){
      // SUCCESS
      // Make AJAX request
      $.ajax({
        url: '../../php/confirm-task.php',
        method: 'POST',
        data: {
          action: 'checkTask',
          user_id: $userId,
          task_id: $taskId,
          comment: $comment,
          checkDate: $checkDate,
          intCount: $intCount,
          intFormat: $intFormat
        },
        success: function(data){
          $alert.before(data);
          $alert.remove();
        }
      }); // ajax

    } else {
      // NO COMMENT
      // Get the necessary elements
      $errOutput = ($('#check_comment').siblings('.invalid-feedback'));
      $textArea = $('#check_comment');
      // Display errors in the view
      $textArea.addClass('is-invalid');
      $errOutput.text('Bitte schreiben Sie ein beschreibenden Kommentar zu Ihrem Check.');

    }
    
  }); // click

} // showTask



}); // Document.ready
