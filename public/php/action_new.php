<?php
// require the main file for config variables
require_once '../../app/bootstrap.php';
// Require the archive file
require_once '../../app/models/Data.php';
// Require the process file
require_once '../../app/models/Process.php';
// Require the business model file
require_once '../../app/models/BusinessModel.php';
// Require the business model file
require_once '../../app/models/User.php';
// instantiate the data model class
$dataModel = new Data;
// instantiate the process model class
$processModel = new Process;
// instantiate the business model class
$businessModel = new BusinessModel;
// instantiate the user model class
$userModel = new User();


// Get the current user's permissions
$privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

if (isset($_POST['action'])) {

    /* ########################################################## */
    /* ########################## FETCH ######################### */
    /* ########################################################## */

    if ($_POST['action'] == 'fetch') {

        $output = '
      <div id="file-tree">
        <div class="row bg-light mb-3">
          <div class="col-md-12">
            <p>Inhalt</p>
          </div>
        </div>
    ';

        /* ################################################# */
        /* ########### BACK TO PARENT DIRECTORY ############ */
        /* ################################################# */

        // Check if the target directory is not the root
        if ($_POST['targetId'] != 0) {
            // If the parent id is not the root
            if ($_POST['parentId'] != 0) {
                // Get the parent folder by its ID
                $ref = $dataModel->getFolderById($_POST['parentId']);
                // Extract the parent_id and store it in $insert as a data-parent html part
                $insert = 'data-parent="' . $ref->parent_id . '"';
            } else {
                // create an empty insert variable
                $insert = '';
            }

            // Add the row as a wrapper
            $output .= '<div class="row bg-light py-2 mb-3">';

            // Add the 'back to parent' div
            $output .= '<div class="directory previous col-md-12" data-id="' . $_POST['parentId'] . '" data-path="' . $_POST['parentPath'] . '" ' . $insert . '"><i class="fa fa-level-up-alt"></i> .. Zurück</div></div>';
        }

        /* ########################################## */
        /* ############### PROCESSES ################ */
        /* ########################################## */

        $processes = $processModel->getProcessByFolderId($_POST['targetId']);

        if (!empty($processes)) {
            foreach ($processes as $process) {

                if ($process->is_approved != 'false') {
                    $output .= '<div class="row bg-pe-darkgreen mb-3">
                        <a href="' . URLROOT . '/processes/show/' . $process->id . '" class="process col py-3 text-dark"><i class="fas fa-book mr-2"></i> Prozess: ' . $process->process_name . '</a>
                      </div>';
                } else {
                    $output .= '<div class="row bg-warning mb-3">
                        <a href="' . URLROOT . '/processes/show/' . $process->id . '" class="process-unapproved col-md-9 py-3 text-dark"><i class="fas fa-book mr-2"></i> Prozess: ' . $process->process_name . '</a>
                        <div class="col-md-3 py-3">
                          <i class="fas fa-lock mr-2"></i> noch nicht freigegeben
                        </div>
                      </div>';
                }
            }

        }


        /* ########################################## */
        /* ########### UNAPPRROVED FILES ############ */
        /* ########################################## */

        // GET THE UNAPPROVED FILES CONTAINED IN THE FOLDER
        $pendingFiles = $dataModel->getUnapprovedDataByFolderId($_POST['targetId']);

        foreach ($pendingFiles as $file) {

            // Add the row as a wrapper
            $output .= '<div class="row bg-warning py-2 mb-3">';

            $output .= '<div class="file unapproved col-md-9" data-id="' . $file->id . '" data-path="' . $file->path . '">
                    <i class="fa fa-file mr-2"></i> ' . $file->target_name . '</div>
                  <div class="col-md-3"><i class="fas fa-lock mr-2"></i> noch nicht freigegeben</div>';

            // Validate 'approve_file' privilege
            if ($privUser->hasPrivilege('19') == true) {
                $output .=
                    '<div class="col-md-12 mt-3">
                    <button type="button" class="approve_file btn btn-success mr-3">Freigeben</button>';

                if (!empty($file->userNotificationList)) {
                    $output .= '<button type="button" id="approve_file_notificationButton" data-toggle="modal" data-userList="' . $file->userNotificationList . '" data-target="#approve_file_notification" class="btn btn-info mr-3">Benachrichtigung</button>';
                }

                $output .= '</div>';

            }
            $output .= '</div>';

            // Close the row
            $output .= '</div>';

        }


        /* ######################################## */
        /* ########### APPRROVED FILES ############ */
        /* ######################################## */

        // GET THE APPROVED FILES CONTAINED IN THE FOLDER
        $files = $dataModel->getDataByFolderId($_POST['targetId']);

        foreach ($files as $file) {
            $unacceptedFileUserList = $dataModel->getUnacceptedUsersByDataId($file->id);
            $userList = '';
            $userList = !empty($unacceptedFileUserList)
                ? '<strong>Noch nicht akzeptiert:</strong> <br />' . implode(', ', $unacceptedFileUserList)
                : '';

            // Add the row as a wrapper
            $output .= '<div class="row bg-light py-2 mb-3">';

            $output .= '<div class="file col-md-6 flexCenterContainer" data-id="' . $file->id . '" data-path="' . $file->file_path . '">
                    <i class="fa fa-file mr-2"></i> ' . $file->file_name . '</div>
                    <div class="col-md-4">'.$userList.'</div>
                    <div class="col-md-2 flexCenterContainer">
                      <div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Bearbeiten
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                          <button type="button" name="info" data-name="' . $file->file_name . '" data-name="' . $file->id . '" data-tags="' . $file->tags . '" data-file_info_approved_by="'. mergeFullName($file->firstname, $file->lastname) .'" data-file_info_approved_at="'. $file->approved_at .'" class="dropdown-item info btn btn-warning btn-xs">Info</button>
                          <a href="' . URLROOT . substr($file->file_path, 2) . '" download class="dropdown-item btn-xs">Download</a>';

            // Validate 'rename_files' privilege
            if ($privUser->hasPrivilege('28') == true) {
                // Add the button to the dropdown
                $output .=
                    '<button type="button" name="update"    data-name="' . $file->file_name . '" data-name="' . $file->id . '" class="dropdown-item update btn btn-warning btn-xs">Umbenennen</button>';
            }

            // Validate 'delete_files' privilege
            if ($privUser->hasPrivilege('20') == true) {
                // Add the button to the dropdown
                $output .=
                    '<button type="button" name="delete_file" data-name="' . $file->file_name . '" data-name="' . $file->id . '" class="dropdown-item delete_file btn btn-danger btn-xs">Löschen</button>';
            }
            $output .=
                '</div>
                      </div>
                  </div>';

            // Close the row
            $output .= '</div>';

        }

        /* ######################################## */
        /* ############### FOLDERS ################ */
        /* ######################################## */

        // Get the folder by its parent id
        $folders = $dataModel->getFolderByParentId($_POST['targetId']);

        foreach ($folders as $folder) {


            // Check if the current iteration is the archive
            if ($folder->folder_name == 'Archiv') {
                // Add the row as a wrapper
                $output .= '<div class="row" style="visibility: hidden; margin-bottom: -25px;">';

                $output .= '<div id="archiveFolder" class="main archive col-md-9" data-id="' . $folder->id . '" data-path="' . $folder->folder_path . '" data-parent="' . $folder->parent_id . '" style="visibility: hidden;">
                                <i class="fas fa-archive mr-2"></i> ' . $folder->folder_name . '</div>
                              <div class="col-md-1"></div>
                              <div class="col-md-2"></div>';
            } else {

                // Add the row as a wrapper
                $output .= '<div class="row bg-light py-2 mb-3">';

                $output .= '<div class="directory col-md-9" data-istopic="' . $folder->isTopic . '" data-issubtopic="' . $folder->isSubtopic . '" data-id="' . $folder->id . '" data-path="' . $folder->folder_path . '" data-parent="' . $folder->parent_id . '">
                      <i class="fa fa-folder mr-2"></i> ' . $folder->folder_name . '</div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2">';


                $output .=
                    '<div class="dropdown">
                        <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Bearbeiten
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">';

                // Validate 'delete_folders' privilege
                if ($privUser->hasPrivilege('17') == true) {
                    $output .= '<button type="button" name="delete" data-name="' . $folder->folder_name . '" class="dropdown-item delete btn btn-danger btn-xs">Löschen</button>';
                }

                // Validate 'renameFolders' privilege
                if ($privUser->hasPrivilege('36') == true) {
                    // Add the button to the dropdown
                    $output .=
                        '<button type="button" name="update" data-istopic="' . $folder->isTopic . '" data-issubtopic="' . $folder->isSubtopic . '" data-name="' . $folder->folder_name . '" data-id="' . $folder->id . '" class="dropdown-item update btn btn-warning btn-xs">Umbenennen</button>';
                }


                $output .= '</div></div>
                    </div>';
            }

            // Close the row
            $output .= '</div>';
        }


        $output .= '</div>';

        echo $output;
    }


    /* ########################################################## */
    /* ##################### FETCH ARCHIVE ###################### */
    /* ########################################################## */

    if ($_POST['action'] == 'getFolderList') {
        echo json_encode($dataModel->getFolderByParentId($_POST['targetId']));
    }

    if ($_POST['action'] == 'fetch_archive') {

        $output = '
      <div id="file-tree">
        <div class="row bg-light mb-3">
          <div class="col-md-12">
            <p>Inhalt</p>
          </div>
        </div>
    ';

        if ($_POST['isRootArchive'] == 'true') {
            // IS ARCHIVE ROOT
            $folders = $dataModel->getArchiveFoldersByParentId(0);

            // Add the row as a wrapper
            $output .= '<div class="row bg-light py-2 mb-3">
                      <div class="directory previous col-md-12" data-id="0" data-path="../uploads/files">
                        <i class="fa fa-level-up-alt"></i> .. Zurück
                      </div>
                    </div>';

        } else {
            // IS NOT ARCHIVE ROOT
            $folders = $dataModel->getArchiveFoldersByParentId($_POST['targetId']);

            // Check if the target is the root of the archive by checking the targetId
            if ($_POST['targetId'] == 0) {
                // IS ARCHIVE ROOT
                // Add custom back to parent element to link to the root of the data browser
                $output .= '<div class="row bg-light py-2 mb-3">
                      <div class="directory previous col-md-12" data-id="0" data-path="../uploads/files">
                        <i class="fa fa-level-up-alt"></i> .. Zurück
                      </div>
                    </div>';
            } else {
                // IS NOT ARCHIVE ROOT
                // Get the parent folder by its ID
                $ref = $dataModel->getArchiveFolderById(isset($_POST['parentId']) ? $_POST['parentId'] : 0);
                // Extract the parent_id and store it in $insert as a data-parent html part
                $insert = 'data-parent="' . isset($ref->parent_id) && is_object($ref) ? $ref->parent_id : 0 . '"';


                // Add the $insert to the back to parent element
                $parentID = isset($_POST['parentId']) ? $_POST['parentId'] : 0;

                $output .= '<div class="row bg-light py-2 mb-3">
                      <div class="previous-archive col-md-12" data-id="' . $parentID . '" data-path="' . $_POST['parentPath'] . '" ' . $insert . '>
                        <i class="fa fa-level-up-alt"></i> .. Zurück
                      </div>
                    </div>';
            }
        }

        /* ####################################################### */
        /* ################ GET ARCHIVED FOLDERS ################# */

        foreach ($folders as $folder) {

            // Add the row as a wrapper
            $output .= '<div class="row bg-light py-2 mb-3">';

            $output .= '<div class="archive col-md-9" data-id="' . $folder->id . '" data-path="' . $folder->folder_path . '" data-parent="' . $folder->parent_id . '">
                      <i class="fas fa-archive"></i> ' . $folder->folder_name . '</div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2"></div>
                  </div>';
        }

        /* ####################################################### */
        /* ################# GET ARCHIVED FILES ################## */

        $files = $dataModel->getArchivedDataByParentId($_POST['targetId']);

        foreach ($files as $file) {

            // Add the row as a wrapper
            $output .= '<div class="row bg-light py-2 mb-3">';

            $output .= '<div class="file col-md-9" data-id="' . $file->id . '" data-path="' . $file->file_path . '" data-parent="' . $file->parent_id . '">
                      <i class="fa fa-file mr-2"></i> ' . $file->file_name . '</div>
                    <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <div class="dropdown">
                                <button class="btn btn-light dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Bearbeiten
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <button type="button" name="info" data-name="' . $file->file_name . '" data-name="' . $file->id . '" data-tags="' . $file->tags . '" class="dropdown-item info btn btn-warning btn-xs">Info</button>
                                    <a href="' . URLROOT . substr($file->file_path, 2) . '" download class="dropdown-item btn-xs">Download</a>
                                </div>
                            </div>
                        </div>
                    </div>';
        }

        /* ####################################################### */
        /* ############### GET ARCHIVED PROCESSES ################ */

        $processes = $processModel->getArchivedProcessesByFolderId($_POST['targetId']);

        foreach ($processes as $process) {
            // Add the row as a wrapper
            $output .= '<div class="row bg-pe-darkgreen py-2 mb-3">';

            $output .= '<a href="' . URLROOT . '/processes/show/' . $process->id . '" class="process col-md-9 text-white">
                      <i class="fa fa-book mr-2"></i>Prozess: ' . $process->process_name . '</div>
                    <div class="col-md-1"></div>
                    <div class="col-md-2"></div>
                  </a>';
        }


        echo $output .= '</div>';

    }

    /* ########################################################## */
    /* ################### AJAX: CREATE FOLDER ################## */
    /* ########################################################## */

    if ($_POST["action"] == "create") {

        // Get the directory path an store it in a variable
        $dirPath = $_POST['currentDirectory'] . '/' . $_POST["folder_name"];
        // Check if folder already exists in the root directory
        if (!file_exists($dirPath)) {

            // Create a new folder in the root directory
            mkdir($dirPath, 0777, true);

            // Prepare data for model
            $data = [
                'folder_name' => $_POST["folder_name"],
                'folder_path' => $dirPath,
                'parent_id' => $_POST["targetId"],
                'businessModelType' => $_POST['businessModelType'],
                'sortAfterFolderID' => $_POST['sortAfterFolderID'],
            ];


            if ($dataModel->addFolder($data)) {
                if ($data['businessModelType'] == 'isTopic') {
                    // Insert New Topic
                    $businessModel->insertTopic($data['folder_name'], $businessModel->getIdOfActiveBusinessModel()->id, $dataModel->getIdOfNewestFolder()->id);

                } else if ($data['businessModelType'] == 'isSubtopic') {
                    // Insert New Subtopic
                    $businessModel->insertSubtopic($data['folder_name'], $businessModel->getTopicIdByFolderId($data['parent_id'])->id, $dataModel->getIdOfNewestFolder()->id);
                }

                // SUCCESS
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Der Ordner wurde erstellt.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } else {
                // ERROR
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Der Ordner wurde erstellt, konnte aber nicht zur Datenbank hinzugefügt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
        } else {
            // Otherwise echo an error message
            echo '<div class="alert alert-warning alert-dismissible fade show" role="alert">
              Ein Ordner mit diesem Namen existiert bereits.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
        }
    }

    /* ########################################################## */
    /* ################ AJAX: CHANGE FOLDER NAME ################ */
    /* ########################################################## */

    if ($_POST["action"] == "change_folder_name") {
        // Store the name values with the directory path in variables
        $oldDirPath = $_POST['currentDirectory'] . '/' . $_POST["old_name"];
        $newDirPath = $_POST['currentDirectory'] . '/' . $_POST["folder_name"];

        $dataModel->updateFoldersSortID($_POST['folderId'], $_POST['sortAfterFolderID']);

        // Check if folder already exists in the directory
        if (!file_exists($newDirPath)) {
            // Rename the folder and echo out a success message
            if (rename($oldDirPath, $newDirPath)) {

                $businessModel->updateFolderName($_POST['folder_name'], $_POST['folderId']);
                if ($_POST['isTopic']) {
                    $businessModel->updateActiveTopic($_POST['folder_name'], $_POST['folderId']);
                } else if ($_POST['isSubtopic']) {
                    $businessModel->updateActiveSubtopic($_POST['folder_name'], $_POST['folderId']);
                }

                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">
              Der Ordner wurde erfolgreich angepasst.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
            }
        } else {
            // Otherwise echo an error message
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
              Ein Ordner mit diesem Namen existiert bereits.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
        }
    }

    /* ########################################################## */
    /* ############## AJAX: GET USERNAMES BY IDS ################ */
    /* ########################################################## */

    if ($_POST["action"] == "getUsernamesByIDs") {
        $userlist = $_POST['userlist'] ?? '';
        $usersArr = removeOrgansFromUserCommaSeperatedList($userlist);
        $userIDsCommaSeperated = implode(',', $usersArr);
        $users = $userModel->getUsersByCommaSeperatedIds($userIDsCommaSeperated);

        $usernames = [];
        foreach ($users as $key => $user) {
            $usernames[$key] = $user->firstname . ' ' . $user->lastname;
        }
        echo json_encode($usernames);
    }

    /* ########################################################## */
    /* ################### AJAX: APPROVE FILE ################### */
    /* ########################################################## */

    if ($_POST["action"] == "approve_file") {

        // Validate 'approve_file' privilege
        if ($privUser->hasPrivilege('19') == true) {
            // HAS PERMISSION

            // Get the target file as an object
            $file = $dataModel->getUnapprovedFileById($_POST["targetId"]);

            // Prepare data for model
            $data = [
                'id' => $_POST["targetId"],
                'file_name' => $file->target_name,
                'file_path' => $file->path,
                'extension' => $file->extension,
                'tags' => $file->tags,
                'parent_id' => $file->parent_id,
                'approved_by' => $_SESSION['user_id']
            ];

            // Check if the file was approved
            $newDataId = $dataModel->approveData($data);

            if (!empty($file->userNotificationList)) {
                // Send mail to users: they have to accept and read the file
                $userIDsArr = removeOrgansFromUserCommaSeperatedList($file->userNotificationList);
                $userIDsCommaSeperated = implode(',', $userIDsArr);
                $userEmails = $userModel->getUsersByCommaSeperatedIds($userIDsCommaSeperated);

                foreach ($userEmails as $user) {
                    $dataModel->addReadData($user->id, $newDataId, 0);
                    Mailer::sendApprovedFileMail($user->email, $user->firstname, $user->lastname, $data['file_name'], $data['parent_id']);
                }
            }

            // SUCCESS
            // Check if the file was set inactive
            if ($dataModel->setUnapprovedFileInactive($_POST["targetId"])) {
                // INACTIVE
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Die Datei wurde freigegeben.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } else {
                // ERROR
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Die Datei wurde freigegeben. Allerdings ist ein Fehler in der Datenbank entstanden. Bitte kontaktieren Sie den Support um einen einwandfreien Ablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
        } else {
            // NO PERMISSION
            // echo an error message
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Sie sind nicht dazu berechtigt Dateien freizugeben.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        }
    }

    /* ########################################################## */
    /* ############### AJAX: CHANGE FILE NAME ################### */
    /* ########################################################## */

    if ($_POST["action"] == "change_file_name") {
        // Store the values with the directory path in variables
        $oldFilePath = $_POST['currentDirectory'] . '/' . $_POST["old_name"];
        $noExtPath = $_POST['currentDirectory'] . '/' . $_POST["file_name"];
        $fileExtension = '.' . $_POST['file_ext'];
        // Construct the new file path with extension
        $newFilePath = $noExtPath . $fileExtension;

        // Check if folder already exists in the directory
        if (!file_exists($newFilePath)) {
            // Rename the folder and echo out a success message
            if (rename($oldFilePath, $newFilePath)) {
                // SUCCESS
                // Prepare data for model
                $data = [
                    'id' => $_POST['fileId'],
                    'file_name' => $_POST["file_name"] . $fileExtension,
                    'file_path' => $newFilePath
                ];

                // Check if the database update request was successful
                if ($dataModel->changeFileName($data)) {
                    $dataModel->addReportOnFileRenaming($_SESSION['user_id'], $_POST['fileId']);
                    // SUCCESS
                    echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Die Datei wurde umbenannt.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                } else {
                    // ERROR
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Die Datei wurde erfolgreich umbenannt. Allerdings ist dabei ein Fehler in der Datenbank entstanden. Bitte kontaktieren Sie den Support um einen einwandfreiene Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                }
            } else {
                // ERROR
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Ein Fehler ist aufgetreten. Bitte versuchen Sie es nochmal oder kontaktieren Sie den Support.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
        } else {
            // Otherwise echo an error message
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">
              Eine Datei mit diesem Namen existiert bereits.
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>';
        }
    }

    function archiveFile($path, $dataModel)
    {
        // Store the old path
        $oldPath = $path;
        // Remove the root folder from the filepath of the file to be archived
        $noRootPath = substr($path, 17, strlen($path));

        /* ############# EXTENSION ############## */
        // Get the file extension from the string
        $extension = explode(".", $noRootPath);
        $extension = array_pop($extension);
        // Get the number of charactes of $extension and add the number 1 (for the dot) to it
        $length = 1 + (strlen($extension));
        // Remove the file extension from $fileName
        $dirAndFile = substr($noRootPath, 0, strlen($noRootPath) - $length);

        $archivePath = '../uploads/files/Archiv/' . $dirAndFile . '_archived_' . '.' . $extension;


        /* ############# PREVIOUS DIRECTORIES ############## */
        // Split the path into parts (for each directory) and store them in an array
        $dirParts = explode('/', $noRootPath);
        // Remove the last array element (the actual file)
        $fileName = array_pop($dirParts);

        // Construct the start path for the if-file-exists foreach-loop
        $checkPath = '../uploads/files/Archiv/';

        // Init Error Variable
        $errorMsg = '';

        // Loop through the $dirParts array and check if the dir already exisist within the archive directory
        foreach ($dirParts as $dir) {
            // For each iteration add one part to $checkPath
            $checkPath .= $dir;

            // Check if this path does not exists
            if (!file_exists($checkPath)) {
                // NO EXISTENT
                // Create it
                mkdir($checkPath, 0777, true);

                // Get the active folder from DB
                $folderObject = $dataModel->getFolderByName($dir);

                // Prepare data for Archive Folder Creation
                $data = [
                    'id' => $folderObject->id,
                    'parent_id' => $folderObject->parent_id ?? 0,
                    'folder_name' => $folderObject->folder_name ?? 'unbekannt',
                    'folder_path' => $checkPath
                ];

                // Create archive folder in DB and check if it was successful
                if ($dataModel->createArchiveFolder($data)) {

                } else {
                    $errorMsg .= 'Beim spiegeln der Ordnerstruktur ist im Archivierungsprozess ein Fehler entstanden. Ordner: ' . $checkPath . ' .';
                }

                // Concatenate the slash for the next round
                $checkPath .= '/';
            } else {
                // DIR EXISTENT -> continue to the next dir
                // Concatenate the slash for the next round
                $checkPath .= '/';
                continue;
            }
        }

        // Init log variable
        $log = '';

        // Check if the file exists in the archive directory
        if (!file_exists($archivePath)) {
            // Move the file to the archive directory
            if (rename($oldPath, $archivePath)) {
                $log = 'SUCCESS';

            } else {
                $log = 'ERROR';
            }
        } else {
            $log = 'EXISTS';
        }

        return $output = [
            'log' => $log,
            'error' => $errorMsg,
            'archivePath' => $archivePath,
            'fileName' => $fileName
        ];
    }

    /* ########################################################## */
    /* ############## AJAX: DELETE AND ARCHIVE FILE ############# */
    /* ########################################################## */

    // check if the action set to remove_file
    if ($_POST["action"] == "archive_file") {

        // Validate delete privilege
        if ($privUser->hasPrivilege('20') != true) {
            // NO PERMISSION
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Sie nicht dazu berechtigt Dateien zu löschen bzw. zu archivieren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        } else {
            // PERMISSION

            // Archive the file
            $result = archiveFile($_POST['filePath'], $dataModel);


            // Get the active file from DB
            $activeFile = $dataModel->getFileById($_POST['fileId']);

            // Output the respective message
            if ($result['log'] == 'SUCCESS' && empty($result['error'])) {

                // Prepare data for the model
                $data = [
                    'file_name' => $result['fileName'],
                    'file_path' => $result['archivePath'],
                    'parent_id' => $activeFile->parent_id
                ];

                // Check if the the database request was successful
                if ($dataModel->archiveFile($data)) {
                    // SUCCESS
                    // Check if the removal of the file from approved_data was successful
                    if ($dataModel->removeFile($_POST['fileId'])) {
                        // SUCCESS
                        echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Die Datei wurde archiviert.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    } else {
                        // ERROR
                        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Die Datei wurde archiviert. Die Datei konnte jedoch nicht inaktiv gesetzt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewähren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    }
                } else {
                    // ERROR
                    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Die Datei wurde archiviert. Es konnte jedoch kein Datenbankeintrag erstellt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewähren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                }


            } elseif ($result['log'] == 'ERROR') {
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Ein Fehler ist aufgetreten. Bitte versuchen Sie es nochmal oder kontaktieren Sie den Support.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } elseif ($result['log'] == 'EXISTS') {
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Eine Datei mit dem selben Namen und Pfad existiert bereits im Archiv.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } else {
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . $result['error'] . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }
        }

    }

    /* ########################################################## */
    /* ################### AJAX: DELETE FOLDER ################## */
    /* ########################################################## */

    // check if the action set to delete
    if ($_POST["action"] == "delete") {

        $folderID = $_POST['folderId'];
        $folderName = $_POST["folder_name"];
        $folderPath = $_POST["currentPath"] . '/' . $_POST["folder_name"];
        $folderIsTopic = $_POST['isTopic'];
        $folderIsSubtopic = $_POST['isSubtopic'];
        $files = scandir($folderPath);

        // ARCHIVING
        // Init output variable for feedback
        $output = '';
        // Set the error variable to false by default
        $hasError = false;
        // Init array for additional folders contained inside the main one
        $folders = [];


        foreach ($files as $file) {
            // Skip current folder and parent folder paths
            if ($file === '.' || $file === '..') {
                continue;
            } else {

                // Get the files relative path
                $filePath = $folderPath . '/' . $file;

                //var_dump($filePath);
                // Get files absolute path
                $realPath = realpath($filePath);

                // Check if current "file" is a directory
                if (is_dir($realPath)) {
                    $hasError = true;
                    // Set the output variable
                    $output = '<div class="alert alert-danger alert-dismissible fade show" role="alert">Der zu archivierende Ordner enthält weitere Unterordner. Bitte archivieren Sie zunächst alle beinhalteten Unterordner bevor Sie diesen Ordner archivieren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    // Stop the foreach loop
                    break;
                }

                // Archive the file and store the feedback
                $result = archiveFile($filePath, $dataModel);

                // Check the result the current archiving process
                if ($result['log'] == 'SUCCESS') {
                    // FILE => archived (renamed and moved to the respective archive folder)

                    // Get the file from the DB
                    $fileObj = $dataModel->getFileByPath($filePath);

                    /**
                     * Get the correct path for archived file to store in db
                     */
                    $oldPath = $fileObj->file_path;
                    $path = $fileObj->file_path;
                    $noRootPath = substr($path, 17, strlen($path));

                    /* ############# EXTENSION ############## */
                    // Get the file extension from the string
                    $extension = explode(".", $noRootPath);
                    $extension = array_pop($extension);
                    // Get the number of charactes of $extension and add the number 1 (for the dot) to it
                    $length = 1 + (strlen($extension));
                    // Remove the file extension from $fileName
                    $dirAndFile = substr($noRootPath, 0, strlen($noRootPath) - $length);


                    $archivePath = '../uploads/files/Archiv/' . $dirAndFile . '_archived_' . '.' . $extension;

                    // Prepare data for the model
                    $data = [
                        'file_name' => $fileObj->file_name,
                        'file_path' => $archivePath,
                        'parent_id' => $fileObj->parent_id
                    ];

                    // Make the archiveFile request and check if it is successful
                    if ($dataModel->archiveFile($data)) {

                        // SUCCESS
                        // Add respective feedback to $output
                        $output .= '<div class="alert alert-success alert-dismissible fade show" role="alert">Die Datei ' . $file . ' wurde archiviert. ' . $data['file_path'] . ' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    } else {
                        // ERROR
                        $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Bei der Archivierung der Datei ' . $file . ' ist ein Datenbankfehler aufgetreten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    }
                } elseif ($result['log'] == 'ERROR') {
                    // FILE => NOT archived
                    // Add respective feedback to $output
                    $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Bei der Archivierung der Datei ' . $file . ' ist ein Fehler aufgetreten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    $hasError = true;

                } else {
                    // FILE => already exists
                    // Add respective feedback to $output
                    $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Eine Datei mit dem Namen ' . $file . ' existiert bereits im entsprechenden Archivordner.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    $hasError = true;
                }
            }
        }

        if ($hasError == false) {
            // Check if the removal of the folder was successful
            if (rmdir($folderPath)) {
                // FOLDER REMOVED
                // Check if the data of the folder was removed from the DB
                if ($dataModel->removeDataByFolderId($folderID)) {

                    // Delete Topics || Subtopics when deleting the folder for the business model
                    if ((int)$folderIsTopic == 1) {
                        $businessModel->deleteTopicByFolderID($folderID);
                    } else if ((int)$folderIsSubtopic == 1) {
                        $businessModel->deleteSubtopicByFolderID($folderID);
                    }

                    // SUCCESSFULLY DELETED
                    // Check if the folder was removed from the DB
                    if ($dataModel->removeFolderById($folderID)) {
                        // REMOVED
                        $output .= '<div class="alert alert-success alert-dismissible fade show" role="alert">Der Ordner wurde gelöscht und alle beinhalteten Dateien archiviert.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        echo $output;
                    } else {
                        // ERROR: NOT REMOVED
                        $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Der Ordner wurde gelöscht und alle beinhalteten Dateien archiviert. Es ist jedoch ein Fehler bei der Aktualisierung der Datenbank aufgetreten. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewähren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                        echo $output;
                    }

                } else {
                    // UNSUCCESSFULLY DELETED
                    $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Der Ordner wurde gelöscht und alle beinhalteten Dateien archiviert. Es konnte jedoch kein Datenbankeintrag erstellt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewähren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    echo $output;
                }

            } else {
                // ERROR: FOLDER NOT REMOVED
                $output .= '<div class="alert alert-danger alert-dismissible fade show" role="alert">Ein Fehler ist aufgetreten. Der Ordner konnte nicht gelöscht werden.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                echo $output;
            }
        } else {
            echo $output;
        }
    } // ACTION: DELETE


    if ($_POST['action'] == "fileUserNotificationChange") {
        if (isset($_POST['notificationUserList']) && !empty($_POST['notificationUserList'])) {
            $fileID = $_POST['fileID'];
            $userListArr = $_POST['notificationUserList'];
            $userNotificationList = implode(', ', $userListArr);

            echo $dataModel->updateUnapprovedData($userNotificationList, $fileID);
        }
    }

}