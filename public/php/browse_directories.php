<?php
// require the main file for config variables
require_once '../../app/bootstrap.php' ;
// Require the archive file
require_once '../../app/models/Data.php';
// Require the process file
require_once '../../app/models/Process.php';
// instantiate the data model class
$dataModel = new Data;
// instantiate the process model class
$processModel = new Process;

/* ################################### */
/* ######## Fetch Select List ######## */
/* ################################### */


if($_POST['action'] == 'fetch_select_list'){

  /* ########################################################## */
  /* ########################## FETCH ######################### */
  /* ########################################################## */ 
    
    $output = '
      <div id="file-tree" class="container">
        <div class="row bg-light mb-3">
          <div class="col-md-12">
            <p>Inhalt</p>
          </div>
        </div>
    ';

    /* ################################################# */
    /* ########### BACK TO PARENT DIRECTORY ############ */
    /* ################################################# */

    // Check if the target directory is not the root
    if($_POST['targetId'] != 0 ) {

      // If the parent id is not the root 
      if($_POST['parentId'] != 0) {
        // Get the parent folder by its ID
        $ref = $dataModel->getFolderById($_POST['parentId']);
        // Extract the parent_id and store it in $insert as a data-parent html part
        $insert = 'data-parent="'.$ref->parent_id.'"';
      } else {
        // create an empty insert variable
        $insert = '';
      }
    
      // Add the row as a wrapper
      $output .= '<div class="row bg-light py-2 mb-3">';

      // Add the 'back to parent' div 
      $output .= '<div class="directory previous col-md-12" data-id="'.$_POST['parentId'].'" '.$insert.'"><i class="fa fa-level-up-alt"></i> .. Zurück</div></div>';
    }

    /* ######################################## */
    /* ########### APPRROVED FILES ############ */
    /* ######################################## */

    // GET THE APPROVED FILES CONTAINED IN THE FOLDER
    $files = $dataModel->getDataByFolderId($_POST['targetId']);
    
    foreach($files as $file){

      // Add the row as a wrapper
      $output .= '<div class="row bg-light py-2 mb-3">';

      $output .= '<div class="file col-md-11" data-id="'.$file->id.'" data-path="'.$file->file_path.'">
                    <i class="fa fa-file"></i> '.$file->file_name.'</div>
                    <div class="col-md-1">
                      <button type="button" class="select select-file btn btn-pe-darkgreen"><i class="fas fa-plus"></i></button>
                    </div>';

      // Close the row
      $output .= '</div>';

    }

    /* ######################################## */
    /* ############## PROCESSES ############### */
    /* ######################################## */

    // GET THE APPROVED PROCESSES CONTAINED IN THE FOLDER
    $processes = $processModel->getProcessByFolderId($_POST['targetId']);

    //die(var_dump($processes));
    
    if(!empty($processes)){
      foreach($processes as $process){

        if($process->is_approved != 'false'){
          // Add the row as a wrapper
          $output .= '<div class="row bg-pe-darkgreen py-2 mb-3">';

          $output .= '<div class="process col-md-11" data-process-id="'.$process->id.'">
                        <i class="fas fa-book mr-2"></i>Prozess: '.$process->process_name.'</div>
                        <div class="col-md-1">
                          <button type="button" class="select select-process btn btn-light"><i class="fas fa-plus"></i></button>
                        </div>';

          // Close the row
          $output .= '</div>';
        }

      }
    }

    /* ######################################## */
    /* ############### FOLDERS ################ */
    /* ######################################## */
    
    // Get the folder by its parent id
    $folders = $dataModel->getFolderByParentId($_POST['targetId']);

    foreach($folders as $folder){

      // Skip the archive folder
      if($folder->folder_name == 'Archiv'){
        continue;
      }

      // Add the row as a wrapper
      $output .= '<div class="row bg-light py-2 mb-3">';

      $output .= '<div class="directory col-md-11" data-id="'.$folder->id.'" data-path="'.$folder->folder_path.'" data-parent="'.$folder->parent_id.'">
                    <i class="fa fa-folder"></i> '.$folder->folder_name.'</div>
                  <div class="col-md-1">
                    <button type="button" class="select select-folder btn btn-pe-darkgreen"><i class="fas fa-plus"></i></button>
                  </div>';
      

      // Close the row
      $output .= '</div>';
    }
    


    echo $output .= '</div>';
}