<?php

require_once '../../app/bootstrap.php';

// Init Database Object
// Source: app/libraries/database.php
$db = new Database;

// Check for a post request with 'action'
if (isset($_POST['action'])) {

    /* ####################################### */
    /* ############ CONFIRM METHOD ############# */
    /* ####################################### */

    if ($_POST['action'] == 'confirmMethod') {

        // CREATE A NEW INSERT FOR THE ACCEPTED METHODS AND ADD THE VALUES
        // basically move the unaccepted method to the accepted methods
        // Make the PDO database request
        $db->query('INSERT into accepted_methods (method_id, user_id, role) VALUES (:method_id, :user_id, :role) ');

        // Bind values
        $db->bind(':method_id', $_POST['method_id']);
        $db->bind(':user_id', $_POST['user_id']);
        $db->bind(':role', $_POST['user_role']);

        // Execute and check if it worked
        if ($db->execute()) {
            // SUCCESS
            // Delete the unaccepted method from the database
            $db->query('DELETE FROM unaccepted_methods WHERE method_id = :method_id AND user_id = :user_id AND role = :role');
            // Bind values
            $db->bind(':method_id', $_POST['method_id']);
            $db->bind(':user_id', $_POST['user_id']);
            $db->bind(':role', $_POST['user_role']);

            // Execute and check if it worked
            if ($db->execute()) {
                // SUCCESS
                $output = '<div class="alert alert-success" role="alert">Die Maßnahme wurde erfolgreich akzeptiert.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                echo $output;
            } else {
                // FAILURE
                $output = '<div class="alert alert-danger" role="alert">Die Maßnahme konnte nicht vollständig akzeptiert werden, da die Unakzeptanz nicht aufgehoben wurde. Bitte kontaktieren die Entwickler, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                echo $output;
            }

        } else {
            // FAILURE
            $output = '<div class="alert alert-danger" role="alert">Die Maßnahme konnte nicht akzeptiert werden. Bitte versuchen Sie es erneut oder kontaktieren die Entwickler.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            echo $output;
        }
    } // action = confirmMethod

    /* ##################################### */
    /* ############ CHECK METHOD ############# */
    /* ##################################### */

    if ($_POST['action'] == 'checkMethod') {

        // CREATE A NEW INSERT INTO CHECKS AND ADD THE METHOD VALUES
        // Make the PDO database request
        $db->query('INSERT into checks_methods (method_id, user_id, checked_at, comment) VALUES (:method_id, :user_id, NOW(), :comment) ');

        // Bind values
        $db->bind(':method_id', $_POST['method_id']);
        $db->bind(':user_id', $_POST['user_id']);
        $db->bind(':comment', $_POST['comment']);

        // Execute
        $last_id = $db->executeAndGetId();

        // Check if the submit was successful by checking if there is a last id
        if ($last_id != false) {

            /*
             * Clear "roles" for the method in db, so the method is assign to no one
             */
            $db->query('UPDATE methods SET roles_responsible = :roles_responsible, roles_concerned = :roles_concerned, roles_contributing = :roles_contributing, roles_information = :roles_information WHERE id = :id');
            $db->bind(':roles_responsible', '');
            $db->bind(':roles_concerned', '');
            $db->bind(':roles_contributing', '');
            $db->bind(':roles_information', '');
            $db->bind(':id', $_POST['method_id']);
            $db->execute();

            /*
             * Remove row in 'accepted_methods' in db
             */
            $db->query('DELETE FROM accepted_methods WHERE method_id = :methodID AND user_id = :userID');
            $db->bind(':methodID',  $_POST['method_id']);
            $db->bind(':userID', $_POST['user_id']);
            $db->execute();

            /*
             * Add a report for checking the method
             */
            $report_type = 'method_report_check';

            $db->query('INSERT into reports (report_date, report_type, method_id, user_id, check_id) VALUES (NOW(), :report_type, :method_id, :user_id, :check_id) ');
            $db->bind(':report_type', $report_type);
            $db->bind(':method_id', $_POST['method_id']);
            $db->bind(':user_id', $_POST['user_id']);
            $db->bind(':check_id', $last_id);

            // Check if the submit was successful
            if ($db->execute()) {

                /**
                 * Sets check date to reference_date + check interval,
                 * but if the reference_date is already in the past (escalation) than the check box will always pop up
                 * FIX: uses current time + check interval
                 * e.g 1 day interval, checking today so I get notified tomorrow
                 *
                 * old code: $timestamp = strtotime($_POST['checkDate']);
                 */
                $timestamp = getCurrentTimestamp();
                // Calculte the checkdate
                $check_date_array = calculateCheckdate($timestamp, $_POST['intCount'], $_POST['intFormat']);
                // Construct the date format for the DB
                $check_date = $check_date_array['year'] . '-' . $check_date_array['month'] . '-' . $check_date_array['day'];

                // Create a new insert into reports
                $db->query('UPDATE methods SET reference_date = :reference_date, check_date = :check_date WHERE id = :id');

                // Bind values
                $db->bind(':id', $_POST['method_id']);
                $db->bind(':reference_date', $_POST['checkDate']);
                $db->bind(':check_date', $check_date);

                // Check if the submit was successful
                if ($db->execute()) {
                    // SUCCESS
                    $output = '<div class="alert alert-success" role="alert">Die Maßnahme wurde erfolgreich gecheckt. Außerdem wurde das Checkdatum basierend auf dem bestehenden Intervall aktualisiert.<br><a class="mt-2 btn btn-success" href="' . URLROOT . '/methods/show/' . $_POST['method_id'] . '" role="button">Verstanden</a></div>';
                    echo $output;
                } else {
                    // FAILURE
                    $output = '<div class="alert alert-danger" role="alert">Die Maßnahme konnte nicht vollständig gecheckt werden, da das Checkdatum nicht aktualisiert werden konnte. Bitte kontaktieren Sie die Entwickler, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    echo $output;
                }
            } else {
                // FAILURE
                $output = '<div class="alert alert-danger" role="alert">Die Maßnahme konnte nicht vollständig gecheckt werden, da kein abschließender Report erstellt werden konnte. Bitte kontaktieren Sie die Entwickler, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                echo $output;
            }
        } else {
            // FAILURE
            $output = '<div class="alert alert-danger" role="alert">Die Maßnahme konnte nicht akzeptiert werden. Bitte versuchen Sie es erneut oder kontaktieren die Entwickler.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            echo $output;
        }
    } // action = checkMethod


} // post request with action