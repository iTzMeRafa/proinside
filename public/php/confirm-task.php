<?php require_once '../../app/bootstrap.php';

$db = new Database;

if (isset($_POST['action'])) {

    /**
     * Fetches and handles the HTTP Post of confirming a task (accepting a task)
     * TL;DR moves the unaccepted_tasks to accepted_tasks
     * @return void
     */
    if ($_POST['action'] == 'confirmTask') {
        $db->query('INSERT into accepted_tasks (task_id, user_id, role) VALUES (:task_id, :user_id, :role) ');
        $db->bind(':task_id', $_POST['task_id']);
        $db->bind(':user_id', $_POST['user_id']);
        $db->bind(':role', $_POST['user_role']);

        // Insert: successful
        if ($db->execute()) {
            $db->query('DELETE FROM unaccepted_tasks WHERE task_id = :task_id AND user_id = :user_id AND role = :role');
            $db->bind(':task_id', $_POST['task_id']);
            $db->bind(':user_id', $_POST['user_id']);
            $db->bind(':role', $_POST['user_role']);

            // Delete: return message on status
            echo $db->execute()
                ? '<div class="alert alert-success" role="alert">Die Aufgabe wurde erfolgreich akzeptiert.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>'
                : '<div class="alert alert-danger" role="alert">Die Aufgabe konnte nicht vollständig akzeptiert werden, da die Unakzeptanz nicht aufgehoben wurde. Bitte kontaktieren die Entwickler, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';

        } else {
            // Insert: failure
            echo '<div class="alert alert-danger" role="alert">Die Aufgabe konnte nicht akzeptiert werden. Bitte versuchen Sie es erneut oder kontaktieren die Entwickler.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        }
    }

    /**
     * Create the saved check and save a report for it into 'reports'
     * Also handles the next check date and updates the task
     * @return void
     */
    if ($_POST['action'] == 'checkTask') {

        $db->query('INSERT into checks_tasks (task_id, user_id, comment) VALUES (:task_id, :user_id, :comment) ');
        $db->bind(':task_id', $_POST['task_id']);
        $db->bind(':user_id', $_POST['user_id']);
        $db->bind(':comment', $_POST['comment']);

        $last_id = $db->executeAndGetId();

        // Check if the submit was successful by checking if there is a last id
        if ($last_id != false) {

            /*
             * Clear "roles" for the task in db, so the task is assign to no one
             */
            $db->query('UPDATE tasks SET roles_responsible = :roles_responsible, roles_concerned = :roles_concerned, roles_contributing = :roles_contributing, roles_information = :roles_information WHERE id = :id');
            $db->bind(':roles_responsible', '');
            $db->bind(':roles_concerned', '');
            $db->bind(':roles_contributing', '');
            $db->bind(':roles_information', '');
            $db->bind(':id', $_POST['task_id']);
            $db->execute();

            /*
             * Remove row in 'accepted_tasks' in db
             */
            $db->query('DELETE FROM accepted_tasks WHERE task_id = :taskID AND user_id = :userID');
            $db->bind(':taskID',  $_POST['task_id']);
            $db->bind(':userID', $_POST['user_id']);
            $db->execute();

            /*
             * Add a report for checking the task
             */
            $report_type = 'task_report_check';

            $db->query('INSERT into reports (report_date, report_type, task_id, user_id, check_id) VALUES (NOW(), :report_type, :task_id, :user_id, :check_id) ');
            $db->bind(':report_type', $report_type);
            $db->bind(':task_id', $_POST['task_id']);
            $db->bind(':user_id', $_POST['user_id']);
            $db->bind(':check_id', $last_id);

            // Check if the submit was successful
            if ($db->execute()) {

                /**
                 * Sets check date to reference_date + check interval,
                 * but if the reference_date is already in the past (escalation) than the check box will always pop up
                 * FIX: uses current time + check interval
                 * e.g 1 day interval, checking today so I get notified tomorrow
                 *
                 * old code: $timestamp = strtotime($_POST['checkDate']);
                 */
                $timestamp = getCurrentTimestamp();
                // Calculte the checkdate
                $check_date_array = calculateCheckdate($timestamp, $_POST['intCount'], $_POST['intFormat']);
                // Construct the date format for the DB
                $check_date = $check_date_array['year'] . '-' . $check_date_array['month'] . '-' . $check_date_array['day'];

                // Create a new insert into reports
                $db->query('UPDATE tasks SET reference_date = :reference_date, check_date = :check_date WHERE id = :id');

                // Bind values
                $db->bind(':reference_date', $_POST['checkDate']);
                $db->bind(':check_date', $check_date);
                $db->bind(':id', $_POST['task_id']);

                // Check if the submit was successful
                if ($db->execute()) {
                    // SUCCESS
                    $output = '<div class="alert alert-success" role="alert">Die Aufgabe wurde erfolgreich gecheckt und erledigt. <br><a class="mt-2 btn btn-success" href="' . URLROOT . '/tasks/show/' . $_POST['task_id'] . '" role="button">Verstanden</a></div>';
                    echo $output;
                } else {
                    // FAILURE
                    $output = '<div class="alert alert-danger" role="alert">Die Aufgabe konnte nicht vollständig gecheckt werden, da das Checkdatum nicht aktualisiert werden konnte. Bitte kontaktieren Sie die Entwickler, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                    echo $output;
                }
            } else {
                // FAILURE
                $output = '<div class="alert alert-danger" role="alert">Die Aufgabe konnte nicht vollständig gecheckt werden, da kein abschließender Report erstellt werden konnte. Bitte kontaktieren Sie die Entwickler, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
                echo $output;
            }
        } else {
            // FAILURE
            $output = '<div class="alert alert-danger" role="alert">Die Aufgabe konnte nicht akzeptiert werden. Bitte versuchen Sie es erneut oder kontaktieren die Entwickler.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            echo $output;
        }
    }


}