<?php

  require_once '../../app/bootstrap.php' ;
  // Require the organ model file
  require_once '../../app/models/Organ.php';
  // Require the user model file
  require_once '../../app/models/User.php';
  // Instantiate the organ model class
  $organModel = new Organ;
  // Instantiate the organ model class
  $userModel = new User;

  // Init Database Object
  // Source: app/libraries/database.php
  $db = new Database;

  // Check for a post request with 'action'
  if(isset($_POST['action'])){

    /* ################################## */
    /* ######### GET ALL USERS ########## */
    /* ################################## */

    if($_POST['action'] == 'getAllUsers'){
      // Get all users
      $users = $userModel->getUsers();
      // Prepare output variable
      $output = '<div class="taskList">';

      foreach ($users as $user) {

        $id = $user->id;
        $firstname = $user->firstname;
        $lastname = $user->lastname;

        $output .='
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="'.$id.'" name="'.$lastname.''.$id.'" data-name="'.$firstname.' '.$lastname.'">
              <label class="form-check-label" for="'.$lastname.''.$id.'">'.$firstname.' '.$lastname.'</label>
            </div>
            ';
      }
      $output .= '</div>';
      echo $output;
    }

    /* ######################################## */
    /* ######### GET ORGAN-USER LIST ########## */
    /* ######################################## */

    if($_POST['action'] == 'getOrganUserList'){

      // Prepare output variable
      $output = '<div class="userList">';

      $organs = $organModel->getOrgansWithoutUser();


      foreach ( $organs as $organ ){

        $output .= '<p class="h5 mt-2">'.$organ->name.'</p>';


        $managers = explode( ',', $organ->management_level );
        $staff = explode( ',', $organ->staff );

        $users = array_merge($managers, $staff);
        $users = array_unique($users);

        // Turn each user_id into an user object
        foreach ( $users as $user_id ){
          // Get the user
          $user = $userModel->getUserById($user_id);

          if( !empty($user) ){
            // FILLED
            // Add Checkbox to $ouput
            $output .= '<div class="form-check">
                          <input class="form-check-input" type="checkbox" value="'.$user->id.'" name="'.$user->lastname.''.$user->id.'" data-name="'.$user->firstname.' '.$user->lastname.'">
                          <label class="form-check-label" for="'.$user->lastname.''.$id.'">'.$user->firstname.' '.$user->lastname.'</label>
                        </div>';
          }
        }
      }

      

      // Close the div 
      $output .= '</div>';

      echo $output;

    }


    /* ############################## */
    /* ######### GET USERS ########## */
    /* ############################## */

    if($_POST['action'] == 'getUsers'){

      // Check if subOrganId is not empty
      if($_POST['subOrganId'] != 'empty'){
        // NOT EMPTY
        // Get the sub-organ
        $organ = $organModel->getSubOrganById($_POST['subOrganId']);
      } else {
        // EMPTY
        // Get the main-organ
        $organ = $organModel->getOrganById($_POST['organId']);
      }

      $managers = $organ->management_level;
      $staff = $organ->staff;

      $managerArr = explode(',', $managers);
      $staffArr = explode(',', $staff);

      if (!empty($managerArr) && !empty($staffArr)) {
        // BOTH FILLED
        // Merge arrays 
        $userIds = array_merge($managerArr, $staffArr);
        // Filter values
        $userIds = array_unique($userIds);

      } elseif(!empty($managerArr) && empty($staffArr)) {
        // ONLY MANAGERS
        // Set userIds to $managerArr
        $userIds = $managerArr;
      } elseif(empty($managerArr) && !empty($staffArr)) {
        // ONLY STAFF
        // Set userIds to $staffArr
        $userIds = $staffArr;
      } else {
        // BOTH EMPTY
        $userIds = '';
      }

      if(!empty($userIds)){
        // USERS FOUND
        // Init var
        $users = [];

        foreach ($userIds as $userId) {
          // Get the user (object) from database
          $user = $userModel->getUserById($userId);
          // Push the user to the $users array
          $users[] = $user;
        }     
        
        $output = '
          <p>Setzen Sie einen entsprechenden Haken bei einem Nutzer um in aus- oder abzuwählen.</p>
          <div class="modal-list">
          <div class="userList">';

        foreach ($users as $user) {

          if(empty($user)){
            continue;
          }

          $id = $user->id;
          $firstname = $user->firstname;
          $lastname = $user->lastname;

          $output .='
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="'.$id.'" name="'.$lastname.''.$id.'" data-name="'.$firstname.' '.$lastname.'">
                <label class="form-check-label" for="'.$lastname.''.$id.'">'.$firstname.' '.$lastname.'</label>
              </div>';
        }
        $output .= '</div></div>';
        echo $output;
      } else {
        // NO USERS FOUND
        $output = '
          <p>Es wurden keine Mitarbeiter in diesem (Unter-)Organ gefunden.</p>';
      }
      
    }

    /* ############################## */
    /* ########## GET TASKS ######### */
    /* ############################## */

    if($_POST['action'] == 'getTasks'){

      // Make the PDO database request
      // Order the results ascending by name
      $db->query('SELECT * FROM tasks ORDER BY name ASC');

      // Get the results and store them into $users
      $tasks = $db->resultSet();


      $output = '<div class="taskList">';

      foreach ($tasks as $task) {

        $id = $task->id;
        $lastname = $task->lastname;

        $output .='
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="' .$id. '" id="'.$name.'">
              <label class="form-check-label" for="'.$name.'">' .$name. '</label>
            </div>
            ';
      }
      $output .= '</div>';
      echo $output;
    }

    /* ############################## */
    /* ######## GET METHODS ######### */
    /* ############################## */

    if($_POST['action'] == 'getMethods'){

      // Make the PDO database request
      // Order the results ascending by name
      $db->query('SELECT * FROM methods ORDER BY name ASC');

      // Get the results and store them into $users
      $methods = $db->resultSet();


      $output = '<div class="taskList">';

      foreach ($methods as $method) {

        $id = $method->id;
        $name = $method->name;

        $output .= '
            <div class="form-check">
              <input class="form-check-input" type="checkbox" value="' .$id. '" id="'.$name.'">
              <label class="form-check-label" for="'.$name.'">' .$name. '</label>
            </div>
            ';
      }
      $output .= '</div>';
      echo $output;
    }

    /* ################################## */
    /* ######### GET SUB ORGANS ######### */
    /* ################################## */

    if($_POST['action'] == 'getSubOrgans'){

      // Get suborgans 
      $subOrgans = $organModel->getSubOrgansByParentId($_POST['parentId']);

      if(!empty($subOrgans)){

        $output = '<option value="empty">Suborgan auswählen</option>';
        $test = 'pepe';

        foreach ($subOrgans as $organ) {
          $output .= '<option value="'.$organ->id.'">'.$organ->name.'</option>';
        }
        echo $output;

      } else {
        $output = '<option value="empty">Keine Suborgane vorhanden.</option>';
        echo $output;
      }
    }

    /* ############################################# */
    /* ######### DEACTIVATE PRIVACY ORGAN ########## */
    /* ############################################# */

    if($_POST['action'] == 'deactivatePrivacy'){

      $data = [
        'id' => 3,
        'is_active' => NULL 
      ];

      if( $organModel->updateOrganActiveStatus($data) ){
        // DEACTIVATION SUCCESSFUL
        $output = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Das Organ wurde erfolgreich deaktiviert.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>';
      }

      echo $output;
    }

    /* ############################################# */
    /* ########## ACTIVATE PRIVACY ORGAN ########### */
    /* ############################################# */

    if($_POST['action'] == 'activatePrivacy'){

      $data = [
        'id' => 3,
        'is_active' => true 
      ];

      if( $organModel->updateOrganActiveStatus($data) ){
        // ACTIVATION SUCCESSFUL
        $output = '<div class="alert alert-success alert-dismissible fade show" role="alert">
                    Das Organ wurde erfolgreich aktiviert.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>';
      }

      echo $output;
    }


  }
