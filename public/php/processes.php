<?php
// require the main file for config variables
require_once '../../app/bootstrap.php' ;
// Require the archive file
require_once '../../app/models/Data.php';
// Require the process file
require_once '../../app/models/Process.php';
// Require the process file
require_once '../../app/models/Task.php';
// Require the process file
require_once '../../app/models/Reminder.php';
// instantiate the data model class
$dataModel = new Data;
// instantiate the process model class
$processModel = new Process;
// instantiate the task model class
$taskModel = new Task;
// instantiate the reminder model class
$reminderModel = new Reminder;

// Get the current user's permissions
$privUser = PrivilegedUser::getByEmail($_SESSION['user_email']);

if(isset($_POST['action'])){

  /* ########################################################## */
  /* ##################### APPROVE PROCESS #################### */
  /* ########################################################## */

  if($_POST['action'] == 'approve_process'){

    //////////////////////////////////
      // APPROVE ALL ASSIGNED TASKS
    //////////////////////////////////

    // Init log variable for errors
    $log = '';

    // Make sure $_POST['tasks] is not empty
    if( !empty($_POST['tasks']) ){
      // NOT EMPTY
      // Explode string of ids into array of ids
      $task_ids = explode(',', $_POST['tasks']);

      // Loop through the array
      foreach( $task_ids as $task_id ){
        // Check if successful and log errors if not
        if( $taskModel->approveTask($task_id) == false ) {
          $log .= "Die Aufgabe A$task_id des Prozesses ". $_POST['processId'] . " konnte nicht freigegeben werden.";
        } else {
          // APPROVED 
          
        }
      }
      
    }

    //////////////////////////////////
      // APPROVE THE PROCESS
    //////////////////////////////////

    // Check if all the tasks have been approved
    if(empty($log)){
      // ALL TASKS APPROVED

      if( $processModel->approveProcess($_POST['processId']) ) {
        // APPROVAL: SUCCESS
        // Log the approval and check if that is successful
        if( $processModel->logApprovedProcess($_POST['processId']) ) {
          // LOG: SUCCESS
          $output = '<div class="alert alert-success" role="alert">
                      Der Prozess wurde erfolgreich freigegeben und ist nun aktiv.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
        } else {
          // LOG: FAILURE
          $output = '<div class="alert alert-danger" role="alert">
          Es ist ein Fehler aufgetreten. Der Prozess wurde erfolgreich freigegeben. Allerdings konnte kein Datenbank-Log erstellt werden.
          Bitte kontaktieren Sie den Support, um den Fehler zu melden und einen einwandfreien Betriebsablauf zu gewährleisten.
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>';
        }
        
      } else {
        // APPROVAL: FAILURE
        $output = '<div class="alert alert-danger" role="alert">
                      Es ist ein Fehler aufgetreten. Der Prozess konnte nicht freigegeben werden.
                      Bitte kontaktieren Sie den Support, um den Fehler zu melden und schnellstmöglich zu beheben.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
      }
  
      echo $output;

    } else {
      // NOT ALL TASKS APPROVED
      $output = '<div class="alert alert-danger" role="alert">
                  Es ist ein Fehler aufgetreten. Der Prozess konnte nicht vollständig freigegeben werden.
                  Bitte kontaktieren Sie den Support, um den Fehler zu melden und schnellstmöglich zu beheben.
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>';
    }
    
    

  }

  /* ########################################################## */
  /* #################### ARCHIVE PROCESS ##################### */
  /* ########################################################## */

  if($_POST['action'] == 'archive_process'){

    // Validate delete privilege
    if($privUser->hasPrivilege('24') != true) {
      // NO PERMISSION
      // Echo out an error message
      echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Sie nicht dazu berechtigt Prozesse zu löschen bzw. zu archivieren.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    } else {
      // HAS PERMISSION

      // Get the folder where the process is located
      $folder = $dataModel->getFolderById($_POST['folderId']);
      // Store the path 
      $path = $folder->folder_path;
      // Remove the root folder from the filepath of the file to be archived
      $noRootPath = substr( $path, 17, strlen($path) );
      // Construct the archive Path = the final destination
      $archivePath = '../uploads/files/Archiv/'.$noRootPath;

      /* ############# PREVIOUS DIRECTORIES ############## */
      // Split the path into parts (for each directory) and store them in an array
      $dirParts = explode('/', $noRootPath);
      // Construct the start path for the if-file-exists foreach-loop
      $checkPath = '../uploads/files/Archiv/';

      // Init Error Variable
      $errorMsg = '';

      
      // Loop through the $dirParts array and check if the dir already exisist within the archive directory
      foreach($dirParts as $dir) {
        // For each iteration add one part to $checkPath 
        $checkPath .= $dir;
        
        // Check if this path does not exists
        if(!file_exists($checkPath)){
          // NO EXISTENT
          // Create it
          mkdir($checkPath, 0777, true);

          // Get the active folder from DB
          $folderObject = $dataModel->getFolderByName($dir);

          // Prepare data for Archive Folder Creation
          $data = [
            'id' => $folderObject->id,
            'parent_id' => $folderObject->parent_id,
            'folder_name' => $folderObject->folder_name,
            'folder_path' => $checkPath
          ];

          // Create archive folder in DB and check if it was successful
          if($dataModel->createArchiveFolder($data)){
            // SUCCESS

          } else {
            $errorMsg .= 'Beim spiegeln der Ordnerstruktur ist im Archivierungsprozess ein Fehler entstanden. Ordner: '.$checkPath.' .';
          }

          // Concatenate the slash for the next round
          $checkPath .= '/';
        } else {
          // DIR EXISTENT -> continue to the next dir 
          // Concatenate the slash for the next round
          $checkPath .= '/';
          continue;
        }
      }

      // Check if the directory exists in the archive directory
      if(file_exists($archivePath)){
        // EXISTS
        // Archive the process
        if($processModel->archiveProcess($_POST['processId'])) {
          // SUCCESSFULLY ARCHIVED
          $output = '<div class="alert alert-success" role="alert">
                      Der Prozess wurde erfolgreich archiviert und ist nun über den Archivordner auffindbar.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
        } else {
          // FAILURE
          $output = '<div class="alert alert-darnger" role="alert">
                      Ein Fehler ist aufgetreten. Der Prozess konnte nicht archiviert werden.
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
        }
      }

      echo $output;
    }

    /* ################################################### */
    /* ########## ARCHIVE ALL CONTAINED TASKS ############ */

    // Get the process
    $process = $processModel->getProcessById($_POST['processId']);
    // Turn string of task-ids into array of task-ids
    $taskIds = explode(',', $process->task_ids);

    $log = [];

    foreach ($taskIds as $key => $taskId){
      // Archive the task
      if( $taskModel->archiveTask($taskId) != true ){
        $log[] = "Die Aufgabe A$taskId konnte aufgrund eines Fehlers nicht archiviert werden.";
      } else {
        // TASK ARCHIVED
        // Remove all connected reminders
        //$reminderModel->deactivateRemindersByTaskId($taskId);
      }

      
    }   





    
  }

}