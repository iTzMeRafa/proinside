<?php
// require the main file for config variables
require_once '../../app/bootstrap.php' ;
// Require the archive file
require_once '../../app/models/Task.php';
require_once '../../app/models/Data.php';
// instantiate the data model class
$taskModel = new Task;
$dataModel = new Data;


// Make sure there is an input string
if(isset($_POST['input'])){

  // Get all tags by the input string
  $tags = $dataModel->getTag($_POST['input']);

  $taskArr = [];
  $fileArr = [];

  
  foreach( $tags as $item ){

    if($item->task_id){
      $task =  $taskModel->getAnyTaskById($item->task_id);
    }
    if($item->file_id){
      $file =  $dataModel->getFileById($item->file_id);
    }

    $taskArr[] = $task;
    $fileArr[] = $file;

  }

  




  /* TAGS müssen umstrukturiert werden sodass sie eine eigene Reihe in der DB haben. */

}