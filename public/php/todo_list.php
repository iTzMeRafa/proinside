<?php
// require the main file for config variables
require_once '../../app/bootstrap.php' ;
// Require the archive file
require_once '../../app/models/Data.php';
// instantiate the data model class
$dataModel = new Data;




if(isset($_POST['action'])){

  /* ############################### */
  /* ########### ADD TASK ########## */
  /* ############################### */

  if($_POST['action'] == 'add_task'){

    // Store the taskArray in a variable
    $tasksArray = $_POST['taskArray'];
    // Init taskString variable for the model
    $tasksString = '';

    // Loop through the tasksArray and add each task to the taskString variable
    foreach($tasksArray as $task){
      $tasksString .= $task.'||';
    }

    // Get the users list
    $list = $dataModel->getTodoListByUserId($_SESSION['user_id']);
    
    // Check if this user has not created a todo list yet
    if(empty($list)){
      // NO LIST YET
      // Check if it was created
      if($dataModel->createTodoList($tasksString)){
        // SUCCESS
        echo 'SUCCESS';

      } else {
        // ERRROR
        echo 'ERROR';
      }
    } else {
      // LIST ALREADY CREATED
      // Check if it was updated
      if($dataModel->saveTodoList($tasksString)){
       // SUCCESS
       echo 'SUCCESS';

      } else {
        // ERRROR
        echo 'ERROR';
      }
    }
 
  } // add_task

  /* ################################## */
  /* ########### REMOVE TASK ########## */
  /* ################################## */

  if($_POST['action'] == 'remove_task'){

    if(!empty($_POST['tasks'])) {
      $tasksString = implode("||", $_POST['tasks']);
    } else {
      $tasksString = '';
    }

    // Check if the todolist was updated
    if($dataModel->saveTodoList($tasksString)){
      // SUCCESS
      echo 'SUCCESS';
     } else {
       // ERRROR
       echo 'ERROR';
     }


  } // Remove task


} // isset action