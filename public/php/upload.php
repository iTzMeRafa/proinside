<?php

/***
 * UPLOAD.PHP
 * ----------------------------------------------------------------------------------
 * This file handles the upload of files in the file manager.
 * Validating: Fetches file, check for allowed extensions, check for allowed filesize
 * Action: Move file to directory, upload data to database
 *
 * @type {Upload}
 * @return {alert}
 */
/* Requires and objects needed for this file */
require_once '../../app/bootstrap.php';
require_once '../../app/models/Data.php';
$dataModel = new Data;

/* global configs */
$rootDirectory      = './uploads/files';
$max_size           = 1000 * 1024 * 6; // 6 MB
$allowed_extensions = [
    'png', 'jpg', 'jpeg', 'gif',
    'doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'odt', 'ppt', 'pot', 'pps', 'xls', 'xlsx', 'xlt',
    'pdf', 'txt', 'csv'
];
$userNotificationList = '';

/**
 * FETCH Notification User List
 * Check if notification is enabled and prepare the user ids for later database insert
 *
 * @return void
 */
if (isset($_POST['notificationUserList']) && !empty($_POST['notificationUserList'])) {
    $userListArr = $_POST['notificationUserList'];
    $userNotificationList = implode(', ', $userListArr);
}

/**
 * FETCH UPLOAD
 * Check if file has at least 10 bytes so it does exist and is not empty
 *
 * @return void
 */
if ($_FILES['upload_file']['size'] > 10) {

    /*
     * Get File Infos
     */
    $filename = pathinfo($_FILES['upload_file']['name'], PATHINFO_FILENAME);
    $extension = strtolower(pathinfo($_FILES['upload_file']['name'], PATHINFO_EXTENSION));
    $new_path = $_POST["hidden_folder_path"] . '/' . $filename . '.' . $extension;


    /*
     * Validation
     */
    if (!in_array($extension, $allowed_extensions)) {
        $data['upload_file_err'] = 'Die hochgeladene Dateiformat ist ungültig';
    }

    if ($_FILES['upload_file']['size'] > $max_size) {
        $data['upload_file_err'] = 'Die hochgeladene Datei ist zu groß. Maximal 6 MB sind erlaubt.';
    }

    // Add a new filename if filename already exists
    if (file_exists($new_path)) { // if file exists add a number to the filename
        $id = 1;
        do {
            $new_path = $_POST["hidden_folder_path"] . '/' . $filename . '_' . $id . '.' . $extension;
            $id++;
        } while (file_exists($new_path));
    }

    $filenamePlusExtension = substr($new_path, strlen($_POST["hidden_folder_path"]) + 1);

    /**
     * UPLOAD AND INSERT
     * check if no errors occurred and move the file to dir
     *
     * @action {move_uploaded_file}
     * @return {alert}
     */
    if (empty($data['upload_file_err'])) {

        if (move_uploaded_file($_FILES["upload_file"]["tmp_name"], $new_path)) {

            $data = [
                'target_name' => $filenamePlusExtension,
                'path' => $new_path,
                'extension' => $extension,
                'tags' => $_POST["tags"],
                'user_id' => $_SESSION['user_id'],
                'parent_id' => $_POST["hidden_parent_id"],
                'userNotificationList' => $userNotificationList,
            ];

          /* Add to DB */
            if ($dataModel->addUnapprovedData($data)) {
                /* On all success */
                echo '<div class="alert alert-success alert-dismissible fade show" role="alert">Die Datei wurde erfolgreich hochgeladen und muss zur Nutzung von einem berechtigten Mitarbeiter freigegeben werden.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            } else {
                /* on database insert failure */
                echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Die Datei wurde erfolgreich hochgeladen. Allerdings konnte kein Datenbankeintrag erstellt werden. Bitte kontaktieren Sie den Support, um einen einwandfreien Betriebsablauf zu gewährleisten.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
            }

        } else {
            /* move_uploaded_file failure */
            echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Ein Fehler is aufgetreten! Bitte versuchen Sie es nochmal oder kontaktieren Sie den Support.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
        }

    } else {
        /* validation error */
        echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">' . $data['upload_file_err'] . '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
    }

} else {
    /* no file submitted, file larger than 8 MB (upload_max_filesize, post_max_size in php.ini) */
    echo '<div class="alert alert-danger alert-dismissible fade show" role="alert">Es wurde keine Datei zum Hochladen ausgewählt. Oder die Datei überschreitet die max. Uploadgröße von 6 MB.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>';
}
